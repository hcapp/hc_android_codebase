/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.scansee.hubregion;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.scansee.taylor";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "Taylor";
  public static final int VERSION_CODE = 4;
  public static final String VERSION_NAME = "2.8.8";
  // Fields from build type: debug
  public static final String ENVIRONMENT = "Production";
  public static final String SERVER_PATH = "/HubCiti2.8.7/";
}
