/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.client.android;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.camera.CameraManager;
import com.google.zxing.client.android.history.HistoryItem;
import com.google.zxing.client.android.history.HistoryManager;
import com.google.zxing.client.android.result.ResultHandler;
import com.google.zxing.client.android.result.ResultHandlerFactory;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.HubCityLogger;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.RetailerActivity;
import com.scansee.android.RetailerCurrentsalesActivity;
import com.scansee.android.RetailerGiveAwayActivity;
import com.scansee.android.RetailerProductMediaActivity;
import com.scansee.android.ScanNowResultActivity;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.android.ScanSeeVideoActivity;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

//import com.scansee.android.activities.FindActivity;
//import com.scansee.android.activities.HotDealsActivity;
//import com.scansee.android.activities.RetailerActivity;
//import com.scansee.android.activities.RetailerCurrentsalesActivity;
//import com.scansee.android.activities.RetailerGiveAwayActivity;
//import com.scansee.android.activities.RetailerProductMediaActivity;
//import com.scansee.android.activities.ScanNowResultActivity;
//import com.scansee.android.activities.ScanSeeHome;
//import com.scansee.android.activities.ScanSeeLocListener;
//import com.scansee.android.activities.ScanSeeVideoActivity;
//import com.scansee.android.activities.ScanseeBrowserActivity;

//import com.scansee.android.adapters.FindCategoryListAdapter;
//import com.scansee.android.utils.CommonConstants;
//import com.scansee.android.utils.ScanSeeLogger;
//import com.scansee.android.utils.UserFunctions;

/**
 * This activity opens the camera and does the actual scanning on a background
 * thread. It draws a viewfinder to help the user place the barcode correctly,
 * shows feedback as the image processing is happening, and then overlays the
 * results when a scan is successful.
 * 
 * @author dswitkin@google.com (Daniel Switkin)
 * @author Sean Owen edited version
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public final class ScanNowActivity extends CustomTitleBar implements
		SurfaceHolder.Callback, OnClickListener {

	private static final String TAG = ScanNowActivity.class.getSimpleName();
	private static final long BULK_MODE_SCAN_DELAY_MS = 1000L;
	private static final int HISTORY_REQUEST_CODE = 0x0000bacc;

	private Handler mHandler;
	private CameraManager cameraManager;
	private CaptureActivityHandler handler;
	private Result savedResultToShow;
	private ViewfinderView viewfinderView;
	private LinearLayout scanArea;
	private Result lastResult;
	private boolean hasSurface;
	private IntentSource source;
	private Collection<BarcodeFormat> decodeFormats;
	private String characterSet;
	private HistoryManager historyManager;
	private InactivityTimer inactivityTimer;
	private BeepManager beepManager;
	private boolean isTorched = false;
	private HashMap<String, String> findData = new HashMap<>();

	private ImageView scanseeAnimation;
	String moduleID = "3";
	String userId;
	private String mItemId;
	private LooperThread looperThread = new LooperThread();
	SharedPreferences settings;

	ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public LinearLayout getScanArea() {
		return scanArea;
	}

	public Handler getHandler() {
		return handler;
	}

	CameraManager getCameraManager() {
		return cameraManager;
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
			mItemId = getIntent().getExtras().getString(
					Constants.MENU_ITEM_ID_INTENT_EXTRA);

			Log.i(Constants.MENU_ITEM_ID_INTENT_EXTRA, "mItemId :" + mItemId);
		}
		GPSUpdate();

		new GetMainMenuId().execute();

	}

	private void GPSUpdate() {
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		String provider = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (provider.contains("gps")) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());

			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			String latitude;
			String longitude;
			if (locNew != null) {

				latitude = String.valueOf(locNew.getLatitude());
				longitude = String.valueOf(locNew.getLongitude());

				HubCityLogger.d("Latitude and Longitude", "new location : lat "
						+ latitude + "lon " + longitude);
			} else {
				// N/W Tower Info Start
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					latitude = String.valueOf(locNew.getLatitude());
					longitude = String.valueOf(locNew.getLongitude());
				} else {
					// Comment
					latitude = CommonConstants.LATITUDE;
					longitude = CommonConstants.LONGITUDE;
					// **********************
				}
				// N/W Tower Info end

			}
			new GetMainMenuId().execute();
		} else {
			new GetZipcode().execute();
		}
	}

	JSONObject jsonObject = null;

	private class GetZipcode extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {

				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();

				String urlParameters = mUrlRequestParams.getUserZipcode();
				String fetchuserlocationpoints = Properties.url_local_server
						+ Properties.hubciti_version
						+ "thislocation/fetchuserlocationpoints?userId=";

				jsonObject = mServerConnections.getJSONFromUrl(false,
						fetchuserlocationpoints + urlParameters, null);
				HubCityLogger.d("Zipcode response", jsonObject.toString());
				if (jsonObject != null) {
					String postalCode = null;
					if (jsonObject.getString("postalCode") != null) {
						postalCode = jsonObject.getString("postalCode");
					} else {
						postalCode = null;
					}
				}
				result = "true";
			} catch (Exception e) {
				result = "true";
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			HubCityLogger.d("progress-GetLocationFromZipcode",
					" post execute async");

			if ("true".equals(result)) {
				new GetMainMenuId().execute();
			} else {
				Toast.makeText(getBaseContext(), "Error Getting Location",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	public class GetMainMenuId extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;

		// private ProgressDialog mDialog;
		// Comment
		// UserFunctions functions = new UserFunctions();
		// *****************
		/*
		 * @Override protected void onPreExecute() {
		 * ScanSeeLogger.d("progress-loadMenu", " pre execute async"); mDialog =
		 * ProgressDialog.show(ScanNowActivity.this, "",
		 * "Loading Menu.. Please Wait.. ", true); }
		 */

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {
				// Comment
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();
				String get_mainmenu_id = Properties.url_local_server
						+ Properties.hubciti_version
						+ "firstuse/utgetmainmenuid";
				String urlParameters = mUrlRequestParams.getMainMenuId(mItemId);
				jsonObject = mServerConnections.getUrlPostResponse(
						get_mainmenu_id, urlParameters, true);
				// ************************
				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.TAG_RESULTSET_MAINMENUID);
					// *************************
					/*
					 * for (int arrayCount = 0; arrayCount < jsonArray.length();
					 * arrayCount++) {
					 */
					findData = new HashMap<>();
					/*
					 * responseMenuObject = jsonArray
					 * .getJSONObject(arrayCount);
					 */
					// Comment
					findData.put(CommonConstants.TAG_MAINMENUID, jsonObject
							.getString(CommonConstants.TAG_MAINMENUID));
					Log.d(TAG, "MAINMENUID in Scan now"
							+ findData.get(CommonConstants.TAG_MAINMENUID));
					SharedPreferences preferences = HubCityContext
							.getHubCityContext()
							.getSharedPreferences(
									Constants.PREFERENCE_HUB_CITY,
									Context.MODE_PRIVATE);
					SharedPreferences.Editor e = preferences.edit().putString(
							Constants.PREFERENCE_KEY_MAIN_MENU_ID,
							findData.get(CommonConstants.TAG_MAINMENUID));
					e.apply();
					// ************************************

					result = "true";

				}
			} catch (Exception e) {
				// //e.printstacktrace();
			}
			Log.d(TAG, result);
			return result;
		}

		/*
		 * @Override protected void onPostExecute(String result) {
		 * ScanSeeLogger.d("progress-GetZipcode", " post execute async");
		 * //mDialog.dismiss(); if ("true".equals(result) ) {
		 * System.out.println(findData.get(CommonConstants.TAG_MAINMENUID)); }
		 * else { System.out.println("Error calling webservice");
		 * 
		 * } }
		 */

	}

	@Override
	protected void onResume() {
		super.onResume();
		try {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			setContentView(R.layout.capture);
			(findViewById(R.id.bottom_bar)).setBackgroundColor(Color.parseColor(Constants.getNavigationBarValues("titleBkGrdColor")));
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText(R.string.title_scan_now);

			// right_button.setText("Main Menu");
			leftTitleImage.setVisibility(View.GONE);

			hasSurface = false;
			historyManager = new HistoryManager(this);
			historyManager.trimHistory();
			inactivityTimer = new InactivityTimer(this);
			beepManager = new BeepManager(this);

			scanseeAnimation = (ImageView) findViewById(R.id.scannow_animo);
			PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
			// CameraManager must be initialized here, not in onCreate(). This is
			// necessary because we don't
			// want to open the camera driver and measure the screen size if we're
			// going to show the help on
			// first launch. That led to bugs where the scanning rectangle was the
			// wrong size and partially
			// off screen.
			// Edit: added layout where the screen should be displayed
			scanArea = (LinearLayout) findViewById(R.id.scan_surface_rect);
			cameraManager = new CameraManager(getApplication(), this);
			viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
			viewfinderView.setCameraManager(cameraManager);
			Button torch = (Button) (findViewById(R.id.scan_now_history_light));
			torch.setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
			torch.setOnClickListener(this);
			Button searchNav = (Button) (findViewById(R.id.scan_now_history_toggle));
			searchNav.setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
			searchNav.setOnClickListener(this);
			handler = null;
			lastResult = null;

			resetStatusView();

			SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
			SurfaceHolder surfaceHolder = surfaceView.getHolder();
			try {
                if (hasSurface) {
                    // The activity was paused but not stopped, so the surface still
                    // exists. Therefore
                    // surfaceCreated() won't be called, so init the camera here.
                    initCamera(surfaceHolder);
                } else {
                    // Install the callback and wait for surfaceCreated() to init
                    // the
                    // camera.
                    surfaceHolder.addCallback(this);
                    surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                }
            } catch (Exception e) {
                new AlertDialog.Builder(this)
                        .setTitle("Alert")
                        .setMessage("Scan Feature is not available for your Device")
                        .setPositiveButton("OK", null).show();
            }
			beepManager.updatePrefs();

			inactivityTimer.onResume();

			Intent intent = getIntent();

			SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(this);

			source = IntentSource.NONE;
			decodeFormats = null;
			characterSet = null;

			if (intent != null) {

                String action = intent.getAction();
                String dataString = intent.getDataString();

                if (Intents.Scan.ACTION.equals(action)) {

                    // Scan the formats the intent requested, and return the result
                    // to the calling activity.
                    source = IntentSource.NATIVE_APP_INTENT;
                    decodeFormats = DecodeFormatManager.parseDecodeFormats(intent);

                    if (intent.hasExtra(Intents.Scan.WIDTH)
                            && intent.hasExtra(Intents.Scan.HEIGHT)) {
                        int width = intent.getIntExtra(Intents.Scan.WIDTH, 0);
                        int height = intent.getIntExtra(Intents.Scan.HEIGHT, 0);
                        if (width > 0 && height > 0) {
                            cameraManager.setManualFramingRect(width, height);
                        }
                    }

                    String customPromptMessage = intent
                            .getStringExtra(Intents.Scan.PROMPT_MESSAGE);
                }

                characterSet = intent.getStringExtra(Intents.Scan.CHARACTER_SET);

            }
			if (looperThread != null && !looperThread.isAlive()) {
                looperThread.start();
            }

			//user for hamburger in modules
			drawerIcon.setVisibility(View.VISIBLE);
			drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			CustomNavigation customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
			callSideMenuApi(customNaviagation);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPause() {
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}
		if (looperThread != null && looperThread.isAlive()) {
			looperThread.interrupt();
		}
		cameraManager.setTorch(false);
//		torch.setBackgroundResource(R.drawable.light_button_up);
		inactivityTimer.onPause();
		cameraManager.closeDriver();
		if (!hasSurface) {
			SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
			SurfaceHolder surfaceHolder = surfaceView.getHolder();
			surfaceHolder.removeCallback(this);
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		inactivityTimer.shutdown();
		if (mHandler != null) {
			mHandler.getLooper().quit();
		}
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			if (source == IntentSource.NATIVE_APP_INTENT) {
				setResult(RESULT_CANCELED);
				finish();
				return true;
			}
			if ((source == IntentSource.NONE || source == IntentSource.ZXING_LINK)
					&& lastResult != null) {
				restartPreviewAfterDelay(0L);
				return true;
			}
			break;
		case KeyEvent.KEYCODE_FOCUS:
		case KeyEvent.KEYCODE_CAMERA:
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (resultCode == RESULT_OK) {
			if (requestCode == HISTORY_REQUEST_CODE) {
				int itemNumber = intent.getIntExtra(
						Intents.History.ITEM_NUMBER, -1);
				if (itemNumber >= 0) {

					HistoryItem historyItem = historyManager
							.buildHistoryItem(itemNumber);
					decodeOrStoreSavedBitmap(null, historyItem.getResult());
				}
			}
		}

		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}
	}

	private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
		// Bitmap isn't used yet -- will be used soon
		if (handler == null) {
			savedResultToShow = result;
		} else {
			if (result != null) {
				savedResultToShow = result;
			}
			if (savedResultToShow != null) {
				Message message = Message.obtain(handler,
						R.id.decode_succeeded, savedResultToShow);
				handler.sendMessage(message);
			}
			savedResultToShow = null;
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (holder == null) {
			HubCityLogger.e(TAG,
					"*** WARNING *** surfaceCreated() gave us a null surface!");
		}
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	/**
	 * A valid barcode has been found, so give an indication of success and show
	 * the results.
	 * 
	 * @param rawResult
	 *            The contents of the barcode.
	 * @param barcode
	 *            A greyscale bitmap of the camera data which was decoded.
	 */

	public void handleDecode(Result rawResult, Bitmap barcode) {
		inactivityTimer.onActivity();
		lastResult = rawResult;
		ResultHandler resultHandler = ResultHandlerFactory.makeResultHandler(
				this, rawResult);

		boolean fromLiveScan = barcode != null;
		if (fromLiveScan) {
			historyManager.addHistoryItem(rawResult, resultHandler);
			// Then not from history, so beep/vibrate and we have an image to
			// draw on
			beepManager.playBeepSoundAndVibrate();
			drawResultPoints(barcode, rawResult);
		}

		switch (source) {
		case NONE:
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(this);
			if (fromLiveScan
					&& prefs.getBoolean(PreferencesActivity.KEY_BULK_MODE,
							false)) {
				String message = getResources().getString(
						R.string.msg_bulk_mode_scanned)
						+ " (" + rawResult.getText() + ')';
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				// Wait a moment or else it will scan the same barcode
				// continuously about 3 times
				restartPreviewAfterDelay(BULK_MODE_SCAN_DELAY_MS);
			} else {
				handleDecodeInternally(rawResult, resultHandler, barcode);
			}
			break;
		}
	}

	/**
	 * Superimpose a line for 1D or dots for 2D to highlight the key features of
	 * the barcode.
	 * 
	 * @param barcode
	 *            A bitmap of the captured image.
	 * @param rawResult
	 *            The decoded results which contains the points to draw.
	 */
	private void drawResultPoints(Bitmap barcode, Result rawResult) {
		ResultPoint[] points = rawResult.getResultPoints();
		if (points != null && points.length > 0) {
			Canvas canvas = new Canvas(barcode);
			Paint paint = new Paint();
			paint.setColor(getResources().getColor(R.color.result_points));
			if (points.length == 2) {
				paint.setStrokeWidth(4.0f);
				drawLine(canvas, paint, points[0], points[1]);
			} else if (points.length == 4
					&& (rawResult.getBarcodeFormat() == BarcodeFormat.UPC_A || rawResult
							.getBarcodeFormat() == BarcodeFormat.EAN_13)) {
				// Hacky special case -- draw two lines, for the barcode and
				// metadata
				drawLine(canvas, paint, points[0], points[1]);
				drawLine(canvas, paint, points[2], points[3]);
			} else {
				paint.setStrokeWidth(10.0f);
				for (ResultPoint point : points) {
					canvas.drawPoint(point.getX(), point.getY(), paint);
				}
			}
		}
	}

	private static void drawLine(Canvas canvas, Paint paint, ResultPoint a,
			ResultPoint b) {
		canvas.drawLine(a.getX(), a.getY(), b.getX(), b.getY(), paint);
	}

	// Put up our own UI for how to handle the decoded contents.
	private void handleDecodeInternally(Result rawResult,
			ResultHandler resultHandler, Bitmap barcode) {
		// Comment
		String mainmenuId = findData.get(CommonConstants.TAG_MAINMENUID);
		// ******************************
		// statusView.setVisibility(View.GONE);
		viewfinderView.setVisibility(View.GONE);
		HubCityLogger.d(ScanNowActivity.class.getName(), "displayContents "
				+ resultHandler.getDisplayContents()
				+ " resultHandler.getType().toString() "
				+ resultHandler.getType().toString()
				+ " rawResult.getBarcodeFormat().toString() "
				+ rawResult.getBarcodeFormat().toString());

		// Toast.makeText(getApplicationContext(), "displayContents "
		// + resultHandler.getDisplayContents()
		// + " resultHandler.getType().toString() "
		// + resultHandler.getType().toString()
		// + " rawResult.getBarcodeFormat().toString() "
		// + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_LONG).show();

		String scanResponse = resultHandler.getDisplayContents().toString();
		if (scanResponse != null) {
			String scanResponseSub = scanResponse.substring(0, 4);
			String lastFour = null;
			if (scanResponse.length() >= 4) {
				lastFour = scanResponse.substring(scanResponse.length() - 4,
						scanResponse.length());
			}
			HubCityLogger.d(TAG, scanResponseSub + " scanResponse"
					+ " last four " + lastFour);
			if (lastFour != null
					&& (lastFour.equalsIgnoreCase(".mp4")
							|| lastFour.equalsIgnoreCase(".3gp") || lastFour
								.equalsIgnoreCase(".m4v"))) {
				// Comment
				Toast.makeText(getApplicationContext(),
						"ScanSeeVideoActivity.class", Toast.LENGTH_LONG).show();

				Intent intent = new Intent(ScanNowActivity.this,
						ScanSeeVideoActivity.class);
				intent.putExtra(
						RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
						scanResponse);
				startActivity(intent);
				// **************************
			} else if (scanResponseSub.contains("www")
					|| scanResponseSub.contains("htt")
					|| scanResponseSub.contains("key1=")) {
				String[] prodParam = scanResponse.split("key1=");
				if (scanResponse.contains(Properties.url_ssqr_server)) {
					if ((prodParam.length == 2)
							&& (!(scanResponse.contains("key3")))) {
						// https://www.scansee.net/SSQR/qr/2300.htm?key1=1063774&key2=1412
						if (prodParam[0].contains("SSQR/qr/2300.htm")) {

							// Comment
							Intent browseIntent = new Intent(
									ScanNowActivity.this,
									RetailerGiveAwayActivity.class);
							// ***************
							Toast.makeText(getApplicationContext(),
									"RetailerGiveAwayActivity.class",
									Toast.LENGTH_LONG).show();

							// new GetMainMenuId().execute();
							// System.out.println("MAINMENUID"
							// +findData.get(CommonConstants.TAG_MAINMENUID));

							// Comment
							String url = scanResponse + "&fromSS=Yes"
									+ "&mainmenuid=" + mainmenuId
									+ "&scantypeid=1";
							browseIntent.putExtra(CommonConstants.URL, url);
							startActivity(browseIntent);
							// **********************

						} else {

							String[] retailParam = prodParam[1].split("&key2=");
							if (retailParam.length >= 2) {
								HubCityLogger.d(TAG, retailParam[0]
										+ "retailer id");
								HubCityLogger.d(TAG, retailParam[1]
										+ "Location id");

								// Toast.makeText(getApplicationContext(),
								// "RetailerActivity.class",
								// Toast.LENGTH_LONG).show();
								Intent navIntent = new Intent(
										ScanNowActivity.this,
										RetailerActivity.class);
								navIntent.putExtra(
										CommonConstants.TAG_RETAIL_ID,
										retailParam[0]);
								navIntent.putExtra(
										CommonConstants.TAG_RETAILE_LOCATIONID,
										retailParam[1]);
								navIntent.putExtra(Constants.TAG_SCAN_TYPE_ID,
										"1");

								navIntent.putExtra("ScanNow", true);
								startActivityForResult(navIntent,
										Constants.STARTVALUE);
								// **************************************************************

							} else {
								HubCityLogger.d(TAG, prodParam[1] + "prod id");

								// Comment
								Toast.makeText(getApplicationContext(),
										"RetailerCurrentsalesActivity.class",
										Toast.LENGTH_LONG).show();
								Intent navIntent = new Intent(
										ScanNowActivity.this,
										RetailerCurrentsalesActivity.class);
								navIntent
										.putExtra(
												CommonConstants.TAG_CURRENTSALES_PRODUCTID,
												prodParam[1]);
								navIntent
										.putExtra(
												CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
												"");

								navIntent.putExtra("ScanNow", true);
								startActivityForResult(navIntent,
										Constants.STARTVALUE);

								// ************************************

							}
						}

					} else {
						Intent browseIntent = new Intent(ScanNowActivity.this,
								ScanseeBrowserActivity.class);
						browseIntent.putExtra(CommonConstants.URL,
								resultHandler.getDisplayContents());
						startActivityForResult(browseIntent,
								Constants.STARTVALUE);
					}
				} else {

					Intent browseIntent = new Intent(ScanNowActivity.this,
							ScanseeBrowserActivity.class);
					browseIntent.putExtra(CommonConstants.URL,
							resultHandler.getDisplayContents());
					startActivityForResult(browseIntent, Constants.STARTVALUE);
				}
			} else {

				// Comment
				// Toast.makeText(getApplicationContext(),
				// "ScanNowResultActivity.class", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(ScanNowActivity.this,
						ScanNowResultActivity.class);

				if (getIntent().hasExtra("favorites")) {
					intent.putExtra("favorites", true);
				}
				if (getIntent().hasExtra("shoppinglist")) {
					intent.putExtra("shoppinglist", true);
				}
				if (resultHandler.getDisplayContents() != null)
					intent.putExtra("barcode",
							resultHandler.getDisplayContents());
				if (resultHandler.getType() != null)
					intent.putExtra("type", resultHandler.getType().toString());
				if (rawResult.getBarcodeFormat() != null)
					intent.putExtra("format", rawResult.getBarcodeFormat()
							.toString());
				startActivityForResult(intent, Constants.STARTVALUE);
				finish();
				// ******************************
			}
		}
		//
		// ImageView barcodeImageView = (ImageView)
		// findViewById(R.id.barcode_image_view);
		// if (barcode == null) {
		// barcodeImageView.setImageBitmap(BitmapFactory.decodeResource(
		// getResources(), R.drawable.launcher_icon));
		// } else {
		// barcodeImageView.setImageBitmap(barcode);
		// }
		//
		// TextView formatTextView = (TextView)
		// findViewById(R.id.format_text_view);
		// formatTextView.setText(rawResult.getBarcodeFormat().toString());
		//
		// TextView typeTextView = (TextView) findViewById(R.id.type_text_view);
		// typeTextView.setText(resultHandler.getType().toString());
		//
		// DateFormat formatter =
		// DateFormat.getDateTimeInstance(DateFormat.SHORT,
		// DateFormat.SHORT);
		// String formattedTime = formatter.format(new Date(rawResult
		// .getTimestamp()));
		// TextView timeTextView = (TextView) findViewById(R.id.time_text_view);
		// timeTextView.setText(formattedTime);
		//
		// TextView metaTextView = (TextView) findViewById(R.id.meta_text_view);
		// View metaTextViewLabel = findViewById(R.id.meta_text_view_label);
		// metaTextView.setVisibility(View.GONE);
		// metaTextViewLabel.setVisibility(View.GONE);
		// Map<ResultMetadataType, Object> metadata = rawResult
		// .getResultMetadata();
		// if (metadata != null) {
		// StringBuilder metadataText = new StringBuilder(20);
		// for (Map.Entry<ResultMetadataType, Object> entry : metadata
		// .entrySet()) {
		// if (DISPLAYABLE_METADATA_TYPES.contains(entry.getKey())) {
		// metadataText.append(entry.getValue()).append('\n');
		// }
		// }
		// if (metadataText.length() > 0) {
		// metadataText.setLength(metadataText.length() - 1);
		// metaTextView.setText(metadataText);
		// metaTextView.setVisibility(View.VISIBLE);
		// metaTextViewLabel.setVisibility(View.VISIBLE);
		// }
		// }

		// TextView contentsTextView = (TextView)
		// findViewById(R.id.contents_text_view);
		// CharSequence displayContents = resultHandler.getDisplayContents();
		// contentsTextView.setText(displayContents);
		// // Crudely scale betweeen 22 and 32 -- bigger font for shorter text
		// int scaledSize = Math.max(22, 32 - displayContents.length() / 4);
		// contentsTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, scaledSize);

		// TextView supplementTextView = (TextView)
		// findViewById(R.id.contents_supplement_text_view);
		// supplementTextView.setText("");
		// supplementTextView.setOnClickListener(null);
		// if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
		// PreferencesActivity.KEY_SUPPLEMENTAL, true)) {
		// SupplementalInfoRetriever.maybeInvokeRetrieval(supplementTextView,
		// resultHandler.getResult(), historyManager, this);
		// }
		//
		// int buttonCount = resultHandler.getButtonCount();
		// ViewGroup buttonView = (ViewGroup)
		// findViewById(R.id.result_button_view);
		// buttonView.requestFocus();
		// for (int x = 0; x < ResultHandler.MAX_BUTTON_COUNT; x++) {
		// TextView button = (TextView) buttonView.getChildAt(x);
		// if (x < buttonCount) {
		// button.setVisibility(View.VISIBLE);
		// button.setText(resultHandler.getButtonText(x));
		// button.setOnClickListener(new ResultButtonListener(
		// resultHandler, x));
		// } else {
		// button.setVisibility(View.GONE);
		// }
		// }

	}

	private void initCamera(SurfaceHolder surfaceHolder) {
		if (surfaceHolder == null) {
			throw new IllegalStateException("No SurfaceHolder provided");
		}
		if (cameraManager.isOpen()) {
			HubCityLogger
					.w(TAG,
							"initCamera() while already open -- late SurfaceView callback?");
			return;
		}
		try {
			cameraManager.openDriver(surfaceHolder);
			// Creating the handler starts the preview, which can also throw a
			// RuntimeException.
			if (handler == null) {
				handler = new CaptureActivityHandler(this, decodeFormats,
						characterSet, cameraManager);
			}
			decodeOrStoreSavedBitmap(null, null);
		} catch (IOException ioe) {
			HubCityLogger.w(TAG, ioe);
			displayFrameworkBugMessageAndExit();
		} catch (RuntimeException e) {
			// Barcode Scanner has seen crashes in the wild of this variety:
			// java.?lang.?RuntimeException: Fail to connect to camera service
			HubCityLogger.w(TAG, "Unexpected error initializing camera", e);
			displayFrameworkBugMessageAndExit();
		}
	}

	private void displayFrameworkBugMessageAndExit() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.app_name));
		builder.setMessage(getString(R.string.msg_camera_framework_bug));
		builder.setPositiveButton(R.string.button_ok, null);
		builder.setOnCancelListener(new FinishListener(this));
		builder.show();
	}

	public void restartPreviewAfterDelay(long delayMS) {
		if (handler != null) {
			handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
		}
		resetStatusView();
	}

	private void resetStatusView() {
		// statusView.setText(R.string.msg_default_status);
		// statusView.setVisibility(View.VISIBLE);
		viewfinderView.setVisibility(View.VISIBLE);
		lastResult = null;
	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.scan_now_history_light:
			isTorched = !isTorched;
			cameraManager.setTorch(isTorched);
//			if (isTorched) {
//				torch.setBackgroundResource(R.drawable.light_btn_down);
//				// torch.setBackgroundColor(Color.WHITE);
//				// torch.setTextColor(Color.BLACK);
//			} else {
//				torch.setBackgroundResource(R.drawable.light_button_up);
//				// torch.setBackgroundColor(Color.BLACK);
//				// torch.setTextColor(Color.WHITE);
//			}
			break;
		case R.id.scan_now_history_toggle:

			// Comment
			Intent intent = new Intent(ScanNowActivity.this,
					ScanNowResultActivity.class);
			intent.putExtra("barcode", "");
			intent.putExtra("type", "");
			intent.putExtra("format", "");
			startActivity(intent);
			this.finish();
			break;
		// *********************************
		default:
			break;
		}

	}

	private class LooperThread extends Thread {
		@Override
		public void run() {
			Looper.prepare();

			try {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim1);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim2);

					}
				});

				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim3);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim4);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim5);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim6);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim7);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim8);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation.setImageResource(R.drawable.scananim9);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation
								.setImageResource(R.drawable.scananim10);

					}
				});
				Thread.sleep(50);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						scanseeAnimation
								.setImageResource(R.drawable.scananim11);

					}
				});
				Thread.sleep(800);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// //////e.printstacktrace();
			}
			looperThread = new LooperThread();
			looperThread.start();
			Looper.loop();
		}

	}
}
