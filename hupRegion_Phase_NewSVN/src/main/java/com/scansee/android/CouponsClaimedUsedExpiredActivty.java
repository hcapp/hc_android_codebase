package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UpdateZipCode;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

public class CouponsClaimedUsedExpiredActivty extends CustomTitleBar implements
		OnClickListener {

	public ArrayList<HashMap<String, String>> allcouponsList = new ArrayList<>();
	private HashMap<String, String> allcouponsData = new HashMap<>();
	protected ListView couponsListView;
	protected ListView loyaltyListView;
	ProgressBar locationProgress;
	Activity activity;
	FindCoupons findCoupons = new FindCoupons();
	boolean firstRequest = true;
	ImageView next;
	protected CouponClaimRedeemExpiredListAdapter couponsListAdapter;
	String userID;
	protected String type = "";
	String clrC = "0";
	String clrL = "0";
	String clrR = "0";
	String lowerLimit = "0";
	String refType = "";
	String refLowerLt = "";
	String couponId = "";
	int minProdId = 0;
	String added;
	Spinner category;
	RadioGroup couponsearch;
	String rowNum;
	RelativeLayout nocouponBackground = null;
	SpinnerObject spinnerObject;
	SpinnerObject[] categoriesList = null;
	protected boolean isCliped = false;
	boolean all = false;
	protected boolean isDelete = false;
	TextView couponStrip;
	ImageButton nextPageBtn, prevPageBtn;
	View paginator;
	String categoryID = null;
	EditText searchText;
	String searchKey = null;
	String selectedCategory;
	String retailerId = "0";
	ViewFlipper findFlipper;
	Button searchButton, closeButton;
	RadioButton loyalty, coupon;
	LinearLayout searchDialog;
	int lastvisitId = 0;
	int zipCode = 0;
	String latitude = null;
	String longitude = null;

	int maxCount = 0;
	int maxRowNum = 0;
	RadioButton radioLoc, radioProd;
	RadioGroup radioFindTab;
	String claimRedeemExpiredValue;
	Button searchCancelBtn;
	SharedPreferences settings = null;

	boolean isFirst, isAlreadyLoading;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.coupons_used_claimed_expired);

		try {
			radioFindTab = (RadioGroup) findViewById(R.id.coupon_radio);
			radioLoc = (RadioButton) findViewById(R.id.coupon_radio_location);
			radioLoc.setChecked(true);

			radioProd = (RadioButton) findViewById(R.id.coupon_radio_product);
			if (getIntent().hasExtra(CommonConstants.TAB_SELECTED)) {
                claimRedeemExpiredValue = getIntent().getExtras().getString(
                        CommonConstants.TAB_SELECTED);
            }
			String typeSelected = "location";
			if (getIntent().hasExtra(CommonConstants.TAB_SELECT_BASE)) {
                typeSelected = getIntent().getExtras().getString(
                        CommonConstants.TAB_SELECT_BASE);
            }

			type = claimRedeemExpiredValue;

			getGPSValues();
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText(claimRedeemExpiredValue);

			leftTitleImage.setVisibility(View.GONE);

			searchCancelBtn = (Button) findViewById(R.id.couponsdeals_cancel);
			searchCancelBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    searchText.setText("");
                    searchKey = "";
                    hideKeyBoard();
                    hideKeyboardItem();
                }
            });

			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    hideKeyboardItem();
                    cancelAsyncTask();
                    finish();
                }
            });

			radioFindTab.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup arg0, int id) {

                    isShowdialog = false;
                    searchText.setText("");
                    lastvisitId = 0;
                    findViewById(R.id.no_coupons_found).setVisibility(View.GONE);

                    if (radioLoc.isChecked()) {
                        radioLoc.setSelected(true);
                        radioProd.setSelected(false);
                    } else if (radioProd.isChecked()) {
                        radioLoc.setSelected(false);
                        radioProd.setSelected(true);
                    }

                    clearData();

                    callFindCoupons();

                }
            });
			searchText = (EditText) findViewById(R.id.coupons_search_text);
//			category = (Spinner) findViewById(R.id.categories);
			searchText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
			searchText.setOnEditorActionListener(new OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView textView, int actionId,
                        KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        searchKey = searchText.getText().toString().trim();
                        lastvisitId = 0;
                        allcouponsList = new ArrayList<>();
                        callFindCoupons();
                    }
                    hideKeyboardItem();
                    return true;
                }
            });

			couponsListView = (ListView) findViewById(R.id.coupons_list);

			paginator = getLayoutInflater().inflate(R.layout.coupon_paginator,
                    couponsListView, false);
			prevPageBtn = (ImageButton) paginator
                    .findViewById(R.id.hotdeals_prev_page);
			nextPageBtn = (ImageButton) paginator
                    .findViewById(R.id.hotdeals_next_page);
			prevPageBtn.setOnClickListener(this);
			nextPageBtn.setOnClickListener(this);

			couponStrip = (TextView) findViewById(R.id.coupons_list_name);
			searchKey = searchText.getText().toString();

			couponsListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {

                    try {
                        HashMap<String, String> selectedData = allcouponsList
                                .get(position);
                        String clickedItem = selectedData.get("itemName");
                        if (isDelete) {
                            isDelete = false;
                            view.findViewById(R.id.coupon_delete).setAnimation(
                                    SlideAnimation.outToRightAnimation());
                            view.findViewById(R.id.coupon_delete).setVisibility(
                                    View.GONE);
                            Parcelable state = couponsListView
                                    .onSaveInstanceState();
                            if (radioLoc.isSelected()) {
                                couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
                                        CouponsClaimedUsedExpiredActivty.this,
                                        allcouponsList, true);
                            } else {
                                couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
                                        CouponsClaimedUsedExpiredActivty.this,
                                        allcouponsList);
                            }
                            couponsListView.setAdapter(couponsListAdapter);
                            couponsListView.onRestoreInstanceState(state);

                        } else {
                            if (!"viewMore".equalsIgnoreCase(clickedItem)) {

                                Intent navIntent = new Intent(
                                        CouponsClaimedUsedExpiredActivty.this,
                                        CouponsDetailActivity.class);
                                if ("coupons".equalsIgnoreCase(clickedItem)) {
                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSCOUPONNAME,
													selectedData
															.get(CommonConstants.ALLCOUPONSCOUPONNAME));
                                    navIntent.putExtra("type", type);
                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSCOUPONID,
													selectedData
															.get(CommonConstants.ALLCOUPONSCOUPONID));
                                    if (radioLoc.isChecked()) {
                                        navIntent.putExtra(
                                                CommonConstants.TAB_SELECTED,
                                                CommonConstants.TAB_LOCATION);
                                    } else {
                                        navIntent.putExtra(
                                                CommonConstants.TAB_SELECTED,
                                                CommonConstants.TAB_PRODUCT);
                                    }
                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSDESCRIPTION,
													selectedData
															.get(CommonConstants.ALLCOUPONSDESCRIPTION));
                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSIMAGEPATH,
													selectedData
															.get(CommonConstants.ALLCOUPONSIMAGEPATH));
                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSSTARTDATE,
													selectedData
															.get(CommonConstants.ALLCOUPONSSTARTDATE));
                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSEXPIRATIONDATE,
													selectedData
															.get(CommonConstants.ALLCOUPONSEXPIRATIONDATE));
                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT,
													selectedData
															.get(CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT));

                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSVIEWABLE,
													selectedData
															.get(CommonConstants.ALLCOUPONSVIEWABLE));
                                    navIntent
                                            .putExtra(
													CommonConstants.ALLCOUPONSURL,
													selectedData
															.get(CommonConstants.ALLCOUPONSURL));
                                    navIntent.putExtra("type", type);

                                    startActivityForResult(navIntent, 100);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            });

			lowerLimit = "0";
			searchKey = null;
			categoryID = null;

			clearData();

			if (radioLoc.isChecked() && "location".equals(typeSelected)) {
                radioLoc.performClick();
            } else if (radioProd.isChecked() && "product".equals(typeSelected)) {
                radioProd.performClick();
            } else {
                if ("product".equals(typeSelected)) {
                    radioProd.setChecked(true);
                } else {
                    radioLoc.setChecked(true);
                }
            }

			callFindCoupons();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void viewMore() {
		isAlreadyLoading = true;
		allcouponsList.remove(allcouponsList.size() - 1);
		lowerLimit = allcouponsList.get(allcouponsList.size() - 1).get(rowNum);
		callFindCoupons();
	}

	@Override
	protected void onResume() {
		hideKeyBoard();
		hideKeyboardItem();
		super.onResume();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && data.hasExtra("isCliped")
				&& data.hasExtra("isUsed") && data.hasExtra("couponId")) {

			String tabSelected = null;
			if (data.hasExtra(CommonConstants.TAB_SELECTED)) {
				tabSelected = data.getStringExtra(CommonConstants.TAB_SELECTED);
			}

			String couponId = data.getExtras().getString("couponId");

			if (data.getExtras().getBoolean("isCliped")) {
				for (int count = 0; count < allcouponsList.size(); count++) {
					String couponID = allcouponsList.get(count).get(
							CommonConstants.ALLCOUPONSCOUPONID);
					if (couponID != null && couponID.equalsIgnoreCase(couponId)) {
						allcouponsList.get(count).put("claimFlag", "1");
					}
				}
				Parcelable state = couponsListView.onSaveInstanceState();
				if (radioLoc.isSelected()) {
					couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
							CouponsClaimedUsedExpiredActivty.this,
							allcouponsList, true);
				} else {
					couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
							CouponsClaimedUsedExpiredActivty.this,
							allcouponsList);
				}

				couponsListView.setAdapter(couponsListAdapter);
				couponsListAdapter.notifyDataSetChanged();
				couponsListView.onRestoreInstanceState(state);
			}
			if (data.getExtras().getBoolean("isUsed")) {

				if (tabSelected != null) {

					if (tabSelected.equals(CommonConstants.TAB_LOCATION)) {
						radioLoc.setSelected(true);
						lastvisitId = 0;

					} else if (tabSelected.equals(CommonConstants.TAB_PRODUCT)) {
						radioProd.setChecked(true);
						lastvisitId = 0;

					}

					clearData();
					callFindCoupons();
				}

			}

			if (!data.getExtras().getBoolean("isUsed")
					&& !data.getExtras().getBoolean("isCliped")) {
				if (tabSelected != null) {
					if (tabSelected.equals(CommonConstants.TAB_LOCATION)) {
						radioLoc.setSelected(true);
						lastvisitId = 0;

					} else if (tabSelected.equals(CommonConstants.TAB_PRODUCT)) {
						radioProd.setSelected(true);
						lastvisitId = 0;
					}
				}

				clearData();

				callFindCoupons();
			}
		}
	}

	private void clearData() {
		couponsListView.setAdapter(null);
		allcouponsList = new ArrayList<>();
		allcouponsData = new HashMap<>();
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	boolean nextPage = false;
	boolean isShowdialog;

	private class FindCoupons extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;

		HashMap<String, String> hotData = null;
		private ProgressDialog mDialog;

		UrlRequestParams objUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			if (!isShowdialog) {
				isShowdialog = true;
				mDialog = ProgressDialog.show(
						CouponsClaimedUsedExpiredActivty.this, "",
						Constants.DIALOG_MESSAGE, true, true);
				mDialog.setCancelable(false);
			}
		}

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			minProdId = lastvisitId;

			/*if (radioProd.isChecked()) {
				result = parseClaimedExpiredUsedbyProd();

			} else if (radioLoc.isChecked()) {
				result = parseJSONClaimUsedExpByLoc();

			}*/
			return result;
		}

		public boolean isJSONArray(JSONObject jsonObject, String value) {
			boolean isArray = false;

			JSONObject isJSONObject = jsonObject.optJSONObject(value);
			if (isJSONObject == null) {
				JSONArray isJSONArray = jsonObject.optJSONArray(value);
				if (isJSONArray != null) {
					isArray = true;
				}
			}

			return isArray;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				mDialog.dismiss();
				searchKey = null;
				findViewById(R.id.no_coupons_found).setVisibility(View.INVISIBLE);
				if (couponsListView != null) {
                    couponsListView.removeFooterView(paginator);
                }
				if (radioLoc.isChecked()) {
                    if ("true".equals(result)) {
                        Parcelable state = couponsListView.onSaveInstanceState();

                        couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
                                CouponsClaimedUsedExpiredActivty.this,
                                allcouponsList, true);
                        couponsListView.setAdapter(couponsListAdapter);
                        couponsListView.onRestoreInstanceState(state);
                        firstRequest = false;
                        isAlreadyLoading = false;
                    } else if ("false".equals(result) && firstRequest) {
                        firstRequest = false;
                        isAlreadyLoading = false;
                        radioProd.setChecked(true);
                    } else {
                        if (!"".equals(searchText.getText().toString())) {
                            searchKey = "";
                            searchText.setText("");
                        }
                        if (couponsListView != null) {
                            couponsListView.removeFooterView(paginator);
                        }
                        couponsListView.setAdapter(null);
                        isAlreadyLoading = false;
                        if (type.equalsIgnoreCase(CommonConstants.COUPON_TYPE_MY)
                                || type.equalsIgnoreCase(CommonConstants.COUPON_TYPE_USED)) {
                            couponsListView.removeFooterView(paginator);
                            findViewById(R.id.no_coupons_found).setVisibility(
                                    View.VISIBLE);

                        } else if (type
                                .equalsIgnoreCase(CommonConstants.COUPON_TYPE_EXPIRED)
                                || type.equalsIgnoreCase(CommonConstants.COUPON_TYPE_ALL)) {

                            findViewById(R.id.no_coupons_found).setVisibility(
                                    View.VISIBLE);
                        }

                    }
                } else if (radioProd.isChecked()) {
                    if ("true".equals(result)) {
                        Parcelable state = couponsListView.onSaveInstanceState();

                        couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
                                CouponsClaimedUsedExpiredActivty.this,
                                allcouponsList);
                        couponsListView.setAdapter(couponsListAdapter);
                        couponsListView.onRestoreInstanceState(state);
                        isAlreadyLoading = false;
                    } else {
                        isAlreadyLoading = false;
                        if (!"".equals(searchText.getText().toString())) {
                            searchKey = "";
                            searchText.setText("");
                        }
                        if (couponsListView != null) {
                            couponsListView.removeFooterView(paginator);
                        }
                        couponsListView.setAdapter(null);
                        if (type.equalsIgnoreCase(CommonConstants.COUPON_TYPE_MY)
                                || type.equalsIgnoreCase(CommonConstants.COUPON_TYPE_USED)) {
                            findViewById(R.id.no_coupons_found).setVisibility(
                                    View.VISIBLE);

                        } else if (type
                                .equalsIgnoreCase(CommonConstants.COUPON_TYPE_EXPIRED)
                                || type.equalsIgnoreCase(CommonConstants.COUPON_TYPE_ALL)) {
                            findViewById(R.id.no_coupons_found).setVisibility(
                                    View.VISIBLE);
                        }

                    }
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public String parseClaimedExpiredUsedbyProd() {

			String result = "false";
			try {
				minProdId = lastvisitId;
				refType = type;
				refLowerLt = lowerLimit;

				String urlParameters = objUrlRequestParams
						.getCoupnClaimRedeemExpired(type,
								String.valueOf(lastvisitId), searchKey);
				String url_get_clm_use_exp_coup_by_prod = Properties.url_local_server
						+ Properties.hubciti_version + "gallery/gallcoupbyprod";
				jsonObject = mServerConnections.getUrlPostResponse(
						url_get_clm_use_exp_coup_by_prod, urlParameters, true);

				if (jsonObject != null) {
					try {
						nextPage = "1".equalsIgnoreCase(jsonObject.getJSONObject(
								"CouponsDetails").getString("nextPageFlag"));
					} catch (Exception e) {
						e.printStackTrace();
						if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
								"response").getString("responseCode"))) {
							result = jsonObject.getJSONObject("response")
									.getString("responseText");

						}
					}
					JSONObject jsonCoupons = null;
					if (jsonObject.has("CouponsDetails")) {
						jsonCoupons = jsonObject
								.getJSONObject("CouponsDetails");
					} else if (jsonObject.has("CouponDetails")) {
						jsonCoupons = jsonObject.getJSONObject("CouponDetails");
					}

					JSONArray jsonCategoryListArray;
					maxCount = jsonCoupons.getInt("maxCnt");
					maxRowNum = jsonCoupons.getInt("maxRowNum");

					boolean isCategoryArray = isJSONArray(
							jsonCoupons.getJSONObject("categoryInfoList"),
							"CategoryInfo");
					if (isCategoryArray) {
						jsonCategoryListArray = jsonCoupons.getJSONObject(
								"categoryInfoList")
								.getJSONArray("CategoryInfo");
						for (int index = 0; index < jsonCategoryListArray
								.length(); index++) {
							JSONObject jsonCategory = jsonCategoryListArray
									.getJSONObject(index);
							allcouponsData = new HashMap<>();
							allcouponsData.put("itemName", "category");
							if (jsonCategory.has("categoryName")) {
								allcouponsData
										.put(CommonConstants.ALLCOUPONSCOUPONCATNAME,
												jsonCategory
														.getString("categoryName"));
							}
							if (jsonCategory.has("categoryId")) {
								allcouponsData.put(
										CommonConstants.ALLCOUPONSCATID,
										jsonCategory.getString("categoryId"));
							}
							allcouponsList.add(allcouponsData);
							boolean isCouponDetailsArray = isJSONArray(
									jsonCategory
											.getJSONObject("couponDetailsList"),
									"CouponDetails");
							if (isCouponDetailsArray) {
								JSONArray jsonCouponListArray = jsonCategory
										.getJSONObject("couponDetailsList")
										.getJSONArray("CouponDetails");
								for (int couIndex = 0; couIndex < jsonCouponListArray
										.length(); couIndex++) {
									JSONObject jsonCoupon = jsonCouponListArray
											.getJSONObject(couIndex);
									if (jsonCoupon != null) {
										allcouponsData = new HashMap<>();
										allcouponsData.put("itemName",
												"coupons");
										allcouponsData
												.put(CommonConstants.ALLCOUPONSCATID,
														jsonCategory
																.getString("categoryId"));
										if (jsonCoupon.has("couponName")) {
											allcouponsData
													.put("couponName",
															jsonCoupon
																	.getString("couponName"));
										}
										if (jsonCoupon
												.has("couponDiscountAmount")) {
											allcouponsData
													.put("couponDiscountAmount",
															jsonCoupon
																	.getString("couponDiscountAmount"));
										}
										if (jsonCoupon.has("couponURL")) {
											allcouponsData
													.put("couponURL",
															jsonCoupon
																	.getString("couponURL"));
										}
										if (jsonCoupon.has("couponExpireDate")) {
											allcouponsData
													.put("couponExpireDate",
															jsonCoupon
																	.getString("couponExpireDate"));
										}
										if (jsonCoupon.has("coupDesc")) {
											allcouponsData
													.put("coupDesc",
															jsonCoupon
																	.getString("coupDesc"));
										}
										if (jsonCoupon.has("couponDiscountPct")) {
											allcouponsData
													.put("couponDiscountPct",
															jsonCoupon
																	.getString("couponDiscountPct"));
										}
										if (jsonCoupon.has("couponStartDate")) {
											allcouponsData
													.put("couponStartDate",
															jsonCoupon
																	.getString("couponStartDate"));
										}
										if (jsonCoupon.has("couponImagePath")) {
											allcouponsData
													.put("couponImagePath",
															jsonCoupon
																	.getString("couponImagePath"));
										}

										if (jsonCoupon.has("couponNewFlag")) {
											allcouponsData.put(
													"couponNewFlag",
													jsonCoupon
															.getInt("newFlag")
															+ "");
										}
										if (jsonCoupon.has("couponId")) {
											allcouponsData.put(
													"couponId",
													jsonCoupon
															.getInt("couponId")
															+ "");
										}
										if (jsonCoupon.has("usedFlag")) {
											allcouponsData.put(
													"couponUsed",
													jsonCoupon
															.getInt("usedFlag")
															+ "");
										}
										if (jsonCoupon.has("couponListId")) {
											allcouponsData
													.put("couponListId",
															jsonCoupon
																	.getInt("userCoupGallId")
																	+ "");
										}
										if (jsonCoupon.has("redeemFlag")) {
											allcouponsData
													.put("redeemFlag",
															jsonCoupon
																	.getInt("redeemFlag")
																	+ "");
										}
										if (jsonCoupon.has("claimFlag")) {
											allcouponsData
													.put("claimFlag",
															jsonCoupon
																	.getInt("claimFlag")
																	+ "");
										}
										if (jsonCoupon.has("coupDistance")) {
											allcouponsData
													.put("coupDistance",
															jsonCoupon
																	.getDouble("distance")
																	+ "");
										}
										allcouponsList.add(allcouponsData);
									}
								}
							} else {
								JSONObject jsonCoupon = jsonCategory
										.getJSONObject("couponDetailsList")
										.getJSONObject("CouponDetails");
								if (jsonCoupon != null) {
									allcouponsData = new HashMap<>();
									allcouponsData.put("itemName", "coupons");
									if (jsonCoupon.has("couponName")) {
										allcouponsData
												.put("couponName",
														jsonCoupon
																.getString("couponName"));
									}
									if (jsonCoupon.has("couponDiscountAmount")) {
										allcouponsData
												.put("couponDiscountAmount",
														jsonCoupon
																.getString("couponDiscountAmount"));
									}
									if (jsonCoupon.has("couponURL")) {
										allcouponsData
												.put("couponURL", jsonCoupon
														.getString("couponURL"));
									}
									if (jsonCoupon.has("couponExpireDate")) {
										allcouponsData
												.put("couponExpireDate",
														jsonCoupon
																.getString("couponExpireDate"));
									}
									if (jsonCoupon.has("coupDesc")) {
										allcouponsData.put("coupDesc",
												jsonCoupon
														.getString("coupDesc"));
									}
									if (jsonCoupon.has("couponDiscountPct")) {
										allcouponsData
												.put("couponDiscountPct",
														jsonCoupon
																.getString("couponDiscountPct"));
									}
									if (jsonCoupon.has("couponStartDate")) {
										allcouponsData
												.put("couponStartDate",
														jsonCoupon
																.getString("couponStartDate"));
									}
									if (jsonCoupon.has("couponImagePath")) {
										allcouponsData
												.put("couponImagePath",
														jsonCoupon
																.getString("couponImagePath"));
									}

									if (jsonCoupon.has("couponNewFlag")) {
										allcouponsData.put("couponNewFlag",
												jsonCoupon.getInt("newFlag")
														+ "");
									}
									if (jsonCoupon.has("couponId")) {
										allcouponsData.put("couponId",
												jsonCoupon.getInt("couponId")
														+ "");
									}
									if (jsonCoupon.has("usedFlag")) {
										allcouponsData.put("couponUsed",
												jsonCoupon.getInt("usedFlag")
														+ "");
									}
									if (jsonCoupon.has("userCoupGallId")) {
										allcouponsData
												.put("couponListId",
														jsonCoupon
																.getInt("userCoupGallId")
																+ "");
									}
									if (jsonCoupon.has("redeemFlag")) {
										allcouponsData.put("redeemFlag",
												jsonCoupon.getInt("redeemFlag")
														+ "");
									}
									if (jsonCoupon.has("claimFlag")) {
										allcouponsData.put("claimFlag",
												jsonCoupon.getInt("claimFlag")
														+ "");
									}
									if (jsonCoupon.has("coupDistance")) {
										allcouponsData.put(
												"coupDistance",
												jsonCoupon
														.getDouble("distance")
														+ "");
									}
									allcouponsList.add(allcouponsData);
								}
							}
						}

					} else {

						JSONObject jsonCategory = jsonCoupons.getJSONObject(
								"categoryInfoList").getJSONObject(
								"CategoryInfo");
						allcouponsData = new HashMap<>();
						allcouponsData.put("itemName", "category");
						if (jsonCategory.has("categoryName")) {
							allcouponsData.put(
									CommonConstants.ALLCOUPONSCOUPONCATNAME,
									jsonCategory.getString("categoryName"));
						}
						if (jsonCategory.has("categoryId")) {
							allcouponsData.put(CommonConstants.ALLCOUPONSCATID,
									jsonCategory.getString("categoryId"));
						}
						allcouponsList.add(allcouponsData);
						boolean isCouponDetailsArray = isJSONArray(
								jsonCategory.getJSONObject("couponDetailsList"),
								"CouponDetails");
						if (isCouponDetailsArray) {
							JSONArray jsonCouponListArray = jsonCategory
									.getJSONObject("couponDetailsList")
									.getJSONArray("CouponDetails");
							for (int couIndex = 0; couIndex < jsonCouponListArray
									.length(); couIndex++) {
								JSONObject jsonCoupon = jsonCouponListArray
										.getJSONObject(couIndex);
								if (jsonCoupon != null) {
									allcouponsData = new HashMap<>();
									allcouponsData.put("itemName", "coupons");
									if (jsonCoupon.has("couponName")) {
										allcouponsData
												.put("couponName",
														jsonCoupon
																.getString("couponName"));
									}
									if (jsonCoupon.has("couponDiscountAmount")) {
										allcouponsData
												.put("couponDiscountAmount",
														jsonCoupon
																.getString("couponDiscountAmount"));
									}
									if (jsonCoupon.has("couponURL")) {
										allcouponsData
												.put("couponURL", jsonCoupon
														.getString("couponURL"));
									}
									if (jsonCoupon.has("couponExpireDate")) {
										allcouponsData
												.put("couponExpireDate",
														jsonCoupon
																.getString("couponExpireDate"));
									}
									if (jsonCoupon.has("coupDesc")) {
										allcouponsData.put("coupDesc",
												jsonCoupon
														.getString("coupDesc"));
									}
									if (jsonCoupon.has("couponDiscountPct")) {
										allcouponsData
												.put("couponDiscountPct",
														jsonCoupon
																.getString("couponDiscountPct"));
									}
									if (jsonCoupon.has("couponStartDate")) {
										allcouponsData
												.put("couponStartDate",
														jsonCoupon
																.getString("couponStartDate"));
									}
									if (jsonCoupon.has("couponImagePath")) {
										allcouponsData
												.put("couponImagePath",
														jsonCoupon
																.getString("couponImagePath"));
									}

									if (jsonCoupon.has("couponNewFlag")) {
										allcouponsData.put("couponNewFlag",
												jsonCoupon.getInt("newFlag")
														+ "");
									}
									if (jsonCoupon.has("couponId")) {
										allcouponsData.put("couponId",
												jsonCoupon.getInt("couponId")
														+ "");
									}
									if (jsonCoupon.has("usedFlag")) {
										allcouponsData.put("couponUsed",
												jsonCoupon.getInt("usedFlag")
														+ "");
									}
									if (jsonCoupon.has("userCoupGallId")) {
										allcouponsData
												.put("couponListId",
														jsonCoupon
																.getInt("userCoupGallId")
																+ "");
									}
									if (jsonCoupon.has("redeemFlag")) {
										allcouponsData.put("redeemFlag",
												jsonCoupon.getInt("redeemFlag")
														+ "");
									}
									if (jsonCoupon.has("claimFlag")) {
										allcouponsData.put("claimFlag",
												jsonCoupon.getInt("claimFlag")
														+ "");
									}
									if (jsonCoupon.has("coupDistance")) {
										allcouponsData.put(
												"coupDistance",
												jsonCoupon
														.getDouble("distance")
														+ "");
									}
									allcouponsList.add(allcouponsData);
								}
							}
						} else {
							JSONObject jsonCoupon = jsonCategory.getJSONObject(
									"couponDetailsList").getJSONObject(
									"CouponDetails");
							if (jsonCoupon != null) {
								allcouponsData = new HashMap<>();
								allcouponsData.put("itemName", "coupons");
								if (jsonCoupon.has("couponName")) {
									allcouponsData.put("couponName",
											jsonCoupon.getString("couponName"));
								}
								if (jsonCoupon.has("couponDiscountAmount")) {
									allcouponsData
											.put("couponDiscountAmount",
													jsonCoupon
															.getString("couponDiscountAmount"));
								}
								if (jsonCoupon.has("couponURL")) {
									allcouponsData.put("couponURL",
											jsonCoupon.getString("couponURL"));
								}
								if (jsonCoupon.has("couponExpireDate")) {
									allcouponsData
											.put("couponExpireDate",
													jsonCoupon
															.getString("couponExpireDate"));
								}
								if (jsonCoupon.has("coupDesc")) {
									allcouponsData.put("coupDesc",
											jsonCoupon.getString("coupDesc"));
								}
								if (jsonCoupon.has("couponDiscountPct")) {
									allcouponsData
											.put("couponDiscountPct",
													jsonCoupon
															.getString("couponDiscountPct"));
								}
								if (jsonCoupon.has("couponStartDate")) {
									allcouponsData
											.put("couponStartDate",
													jsonCoupon
															.getString("couponStartDate"));
								}
								if (jsonCoupon.has("couponImagePath")) {
									allcouponsData
											.put("couponImagePath",
													jsonCoupon
															.getString("couponImagePath"));
								}

								if (jsonCoupon.has("couponNewFlag")) {
									allcouponsData.put("couponNewFlag",
											jsonCoupon.getInt("newFlag") + "");
								}
								if (jsonCoupon.has("couponId")) {
									allcouponsData.put("couponId",
											jsonCoupon.getInt("couponId") + "");
								}
								if (jsonCoupon.has("usedFlag")) {
									allcouponsData.put("couponUsed",
											jsonCoupon.getInt("usedFlag") + "");
								}
								if (jsonCoupon.has("userCoupGallId")) {
									allcouponsData.put("couponListId",
											jsonCoupon.getInt("userCoupGallId")
													+ "");
								}
								if (jsonCoupon.has("redeemFlag")) {
									allcouponsData.put("redeemFlag",
											jsonCoupon.getInt("redeemFlag")
													+ "");
								}
								if (jsonCoupon.has("claimFlag")) {
									allcouponsData
											.put("claimFlag",
													jsonCoupon
															.getInt("claimFlag")
															+ "");
								}
								if (jsonCoupon.has("coupDistance")) {
									allcouponsData.put("coupDistance",
											jsonCoupon.getDouble("distance")
													+ "");
								}
								allcouponsList.add(allcouponsData);
							}
						}

					}
					if (maxCount > lastvisitId) {
						lastvisitId = maxRowNum;
					}
					if (nextPage) {
						// nextPage = false;
						hotData = new HashMap<>();
						hotData.put("itemName", "viewMore");
						hotData.put(
								CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
								maxRowNum + "");
						allcouponsList.add(hotData);
					}
					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;

		}

		public String parseJSONClaimUsedExpByLoc() {

			minProdId = lastvisitId;
			String result = "false";
			try {

				refType = type;
				refLowerLt = lowerLimit;

				String urlParameters = objUrlRequestParams
						.getCoupnClaimRedeemExpired(type,
								String.valueOf(lastvisitId), searchKey);
				String url_get_clm_use_exp_coup_by_loc = Properties.url_local_server
						+ Properties.hubciti_version + "gallery/gallcoupbyloc";
				jsonObject = mServerConnections.getUrlPostResponse(
						url_get_clm_use_exp_coup_by_loc, urlParameters, true);

				if (jsonObject != null) {

					try {
						nextPage = "1".equalsIgnoreCase(jsonObject.getJSONObject(
								"CouponsDetails").getString("nextPageFlag"));
					} catch (Exception e) {
						e.printStackTrace();
						if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
								"response").getString("responseCode"))) {
							result = jsonObject.getJSONObject("response")
									.getString("responseText");
							result = "false";
							return result;
						}
					}

					JSONObject jsonCoupons = null;
					if (jsonObject.has("CouponsDetails")) {
						jsonCoupons = jsonObject
								.getJSONObject("CouponsDetails");
					} else if (jsonObject.has("CouponDetails")) {
						jsonCoupons = jsonObject.getJSONObject("CouponDetails");
					}

					if (jsonCoupons.has("maxCnt")) {
						maxCount = jsonCoupons.getInt("maxCnt");
					}

					if (jsonCoupons.has("maxRowNum")) {
						maxRowNum = jsonCoupons.getInt("maxRowNum");
					}

					JSONObject jsonretDetailsList;
					JSONArray jsonretDetailsListArray;

					boolean isRetailersArray = isJSONArray(
							jsonCoupons.getJSONObject("retDetailsList"),
							"RetailersDetails");
					if (isRetailersArray) {
						jsonretDetailsListArray = jsonCoupons.getJSONObject(
								"retDetailsList").getJSONArray(
								"RetailersDetails");

						for (int index = 0; index < jsonretDetailsListArray
								.length(); index++) {
							JSONObject jsonRetdetailsObj = jsonretDetailsListArray
									.getJSONObject(index).getJSONObject(
											"retDetailsList");
							int retId = jsonretDetailsListArray.getJSONObject(
									index).getInt("retId");
							String retname = jsonretDetailsListArray
									.getJSONObject(index).getString("retName");
							allcouponsData = new HashMap<>();
							allcouponsData.put("itemName", "category");
							if (jsonretDetailsListArray.getJSONObject(index)
									.has("retId")) {
								allcouponsData.put(
										CommonConstants.ALLCOUPONSCATID, retId
												+ "");
							}
							allcouponsData.put("cateName", retname);
							allcouponsList.add(allcouponsData);

							JSONArray jsonRetailerArray;
							boolean isRetailerjsonArray = isJSONArray(
									jsonRetdetailsObj, "RetailersDetails");
							if (isRetailerjsonArray) {
								jsonRetailerArray = jsonRetdetailsObj
										.getJSONArray("RetailersDetails");
								for (int retIndex = 0; retIndex < jsonRetailerArray
										.length(); retIndex++) {
									JSONObject jsonRetailer = jsonRetailerArray
											.getJSONObject(retIndex);

									allcouponsData = new HashMap<>();
									allcouponsData.put("itemName", "location");
									if (jsonRetailer.has("postalCode")) {
										allcouponsData.put(
												"postalCode",
												jsonRetailer
														.getInt("postalCode")
														+ "");
									}
									if (jsonRetailer.has("minRetDist")) {
										allcouponsData
												.put("minRetDist",
														jsonRetailer
																.getDouble("minRetDist")
																+ "");
									}
									if (jsonRetailer.has("retailerAddress")) {
										allcouponsData
												.put(CommonConstants.ALLCOUPONSLOCATION,
														jsonRetailer
																.getString("retailerAddress"));
									}
									if (jsonRetailer.has("retLocId")) {
										allcouponsData.put("retLocId",
												jsonRetailer.getInt("retLocId")
														+ "");
									}
									if (jsonRetailer.has("city")) {
										allcouponsData.put("city",
												jsonRetailer.getString("city"));
									}
									allcouponsList.add(allcouponsData);

									boolean isArrayCouponDetails = isJSONArray(
											jsonRetailer
													.getJSONObject("couponDetailsList"),
											"CouponDetails");
									JSONArray jsonCouponDetailsListArray;
									if (isArrayCouponDetails) {
										jsonCouponDetailsListArray = jsonRetailer
												.getJSONObject(
														"couponDetailsList")
												.getJSONArray("CouponDetails");
										for (int couponIndx = 0; couponIndx < jsonCouponDetailsListArray
												.length(); couponIndx++) {
											JSONObject jsonCoupon = jsonCouponDetailsListArray
													.getJSONObject(couponIndx);
											if (jsonCoupon != null) {
												allcouponsData = new HashMap<>();
												allcouponsData.put("itemName",
														"coupons");
												allcouponsData
														.put(CommonConstants.ALLCOUPONSCATID,
																jsonretDetailsListArray
																		.getJSONObject(
																				index)
																		.getInt("retId")
																		+ "");
												if (jsonCoupon
														.has("couponName")) {
													allcouponsData
															.put("couponName",
																	jsonCoupon
																			.getString("couponName"));
												}
												if (jsonCoupon
														.has("couponDiscountAmount")) {
													allcouponsData
															.put("couponDiscountAmount",
																	jsonCoupon
																			.getString("couponDiscountAmount"));
												}
												if (jsonCoupon.has("couponURL")) {
													allcouponsData
															.put("couponURL",
																	jsonCoupon
																			.getString("couponURL"));
												}
												if (jsonCoupon
														.has("couponExpireDate")) {
													allcouponsData
															.put("couponExpireDate",
																	jsonCoupon
																			.getString("couponExpireDate"));
												}
												if (jsonCoupon.has("coupDesc")) {
													allcouponsData
															.put("coupDesc",
																	jsonCoupon
																			.getString("coupDesc"));
												}
												if (jsonCoupon
														.has("couponDiscountPct")) {
													allcouponsData
															.put("couponDiscountPct",
																	jsonCoupon
																			.getString("couponDiscountPct"));
												}
												if (jsonCoupon
														.has("couponStartDate")) {
													allcouponsData
															.put("couponStartDate",
																	jsonCoupon
																			.getString("couponStartDate"));
												}
												if (jsonCoupon
														.has("couponImagePath")) {
													allcouponsData
															.put("couponImagePath",
																	jsonCoupon
																			.getString("couponImagePath"));
												}

												if (jsonCoupon
														.has("couponNewFlag")) {
													allcouponsData
															.put(CommonConstants.ALLCOUPONSNEWFLAG,
																	jsonCoupon
																			.getInt("newFlag")
																			+ "");
												}
												if (jsonCoupon.has("couponId")) {
													allcouponsData
															.put("couponId",
																	jsonCoupon
																			.getInt("couponId")
																			+ "");
												}
												if (jsonCoupon.has("used")) {
													allcouponsData
															.put("couponUsed",
																	jsonCoupon
																			.getInt("used")
																			+ "");
												}
												if (jsonCoupon
														.has("couponListId")) {
													allcouponsData
															.put("couponListId",
																	jsonCoupon
																			.getInt("couponListId")
																			+ "");
												}
												if (jsonCoupon
														.has("redeemFlag")) {
													allcouponsData
															.put("redeemFlag",
																	jsonCoupon
																			.getInt("redeemFlag")
																			+ "");
												}
												if (jsonCoupon.has("claimFlag")) {
													allcouponsData
															.put("claimFlag",
																	jsonCoupon
																			.getInt("claimFlag")
																			+ "");
												}
												if (jsonCoupon
														.has("coupDistance")) {
													allcouponsData
															.put("coupDistance",
																	jsonCoupon
																			.getDouble("distance")
																			+ "");
												}
												allcouponsList
														.add(allcouponsData);
											}
										}
									} else {
										JSONObject jsonCoupon = jsonRetailer
												.getJSONObject(
														"couponDetailsList")
												.getJSONObject("CouponDetails");
										if (jsonCoupon != null) {
											allcouponsData = new HashMap<>();
											allcouponsData.put("itemName",
													"coupons");
											if (jsonCoupon.has("couponName")) {
												allcouponsData
														.put("couponName",
																jsonCoupon
																		.getString("couponName"));
											}
											if (jsonCoupon
													.has("couponDiscountAmount")) {
												allcouponsData
														.put("couponDiscountAmount",
																jsonCoupon
																		.getString("couponDiscountAmount"));
											}
											if (jsonCoupon.has("couponURL")) {
												allcouponsData
														.put("couponURL",
																jsonCoupon
																		.getString("couponURL"));
											}
											if (jsonCoupon
													.has("couponExpireDate")) {
												allcouponsData
														.put("couponExpireDate",
																jsonCoupon
																		.getString("couponExpireDate"));
											}
											if (jsonCoupon.has("coupDesc")) {
												allcouponsData
														.put("coupDesc",
																jsonCoupon
																		.getString("coupDesc"));
											}
											if (jsonCoupon
													.has("couponDiscountPct")) {
												allcouponsData
														.put("couponDiscountPct",
																jsonCoupon
																		.getString("couponDiscountPct"));
											}
											if (jsonCoupon
													.has("couponStartDate")) {
												allcouponsData
														.put("couponStartDate",
																jsonCoupon
																		.getString("couponStartDate"));
											}
											if (jsonCoupon
													.has("couponImagePath")) {
												allcouponsData
														.put("couponImagePath",
																jsonCoupon
																		.getString("couponImagePath"));
											}

											if (jsonCoupon.has("couponNewFlag")) {
												allcouponsData
														.put(CommonConstants.ALLCOUPONSNEWFLAG,
																jsonCoupon
																		.getInt("newFlag")
																		+ "");
											}
											if (jsonCoupon.has("couponId")) {
												allcouponsData
														.put("couponId",
																jsonCoupon
																		.getInt("couponId")
																		+ "");
											}
											if (jsonCoupon.has("couponUsed")) {
												allcouponsData.put(
														"couponUsed",
														jsonCoupon
																.getInt("used")
																+ "");
											}
											if (jsonCoupon.has("couponListId")) {
												allcouponsData
														.put("couponListId",
																jsonCoupon
																		.getInt("couponListId")
																		+ "");
											}
											if (jsonCoupon.has("redeemFlag")) {
												allcouponsData
														.put("redeemFlag",
																jsonCoupon
																		.getInt("redeemFlag")
																		+ "");
											}
											if (jsonCoupon.has("claimFlag")) {
												allcouponsData
														.put("claimFlag",
																jsonCoupon
																		.getInt("claimFlag")
																		+ "");
											}
											if (jsonCoupon.has("coupDistance")) {
												allcouponsData
														.put("coupDistance",
																jsonCoupon
																		.getDouble("distance")
																		+ "");
											}
											allcouponsList.add(allcouponsData);
										}

									}
								}

							} else {

								JSONObject jsonRetailer = jsonRetdetailsObj
										.getJSONObject("RetailersDetails");
								allcouponsData = new HashMap<>();
								allcouponsData.put("itemName", "location");
								if (jsonRetailer.has("postalCode")) {
									allcouponsData.put("postalCode",
											jsonRetailer.getInt("postalCode")
													+ "");
								}
								if (jsonRetailer.has("minRetDist")) {
									allcouponsData.put(
											"minRetDist",
											jsonRetailer
													.getDouble("minRetDist")
													+ "");
								}
								if (jsonRetailer.has("retailerAddress")) {
									allcouponsData
											.put(CommonConstants.ALLCOUPONSLOCATION,
													jsonRetailer
															.getString("retailerAddress"));
								}
								if (jsonRetailer.has("retLocId")) {
									allcouponsData.put("retLocId",
											jsonRetailer.getInt("retLocId")
													+ "");
								}
								if (jsonRetailer.has("city")) {
									allcouponsData.put("city",
											jsonRetailer.getString("city"));
								}
								allcouponsList.add(allcouponsData);

								boolean isArrayCouponDetails = isJSONArray(
										jsonRetailer
												.getJSONObject("couponDetailsList"),
										"CouponDetails");
								JSONArray jsonCouponDetailsListArray;
								if (isArrayCouponDetails) {
									jsonCouponDetailsListArray = jsonRetailer
											.getJSONObject("couponDetailsList")
											.getJSONArray("CouponDetails");
									for (int couponIndx = 0; couponIndx < jsonCouponDetailsListArray
											.length(); couponIndx++) {
										JSONObject jsonCoupon = jsonCouponDetailsListArray
												.getJSONObject(couponIndx);
										if (jsonCoupon != null) {
											allcouponsData = new HashMap<>();
											allcouponsData.put("itemName",
													"coupons");
											if (jsonCoupon.has("couponName")) {
												allcouponsData
														.put("couponName",
																jsonCoupon
																		.getString("couponName"));
											}
											if (jsonCoupon
													.has("couponDiscountAmount")) {
												allcouponsData
														.put("couponDiscountAmount",
																jsonCoupon
																		.getString("couponDiscountAmount"));
											}
											if (jsonCoupon.has("couponURL")) {
												allcouponsData
														.put("couponURL",
																jsonCoupon
																		.getString("couponURL"));
											}
											if (jsonCoupon
													.has("couponExpireDate")) {
												allcouponsData
														.put("couponExpireDate",
																jsonCoupon
																		.getString("couponExpireDate"));
											}
											if (jsonCoupon.has("coupDesc")) {
												allcouponsData
														.put("coupDesc",
																jsonCoupon
																		.getString("coupDesc"));
											}
											if (jsonCoupon
													.has("couponDiscountPct")) {
												allcouponsData
														.put("couponDiscountPct",
																jsonCoupon
																		.getString("couponDiscountPct"));
											}
											if (jsonCoupon
													.has("couponStartDate")) {
												allcouponsData
														.put("couponStartDate",
																jsonCoupon
																		.getString("couponStartDate"));
											}
											if (jsonCoupon
													.has("couponImagePath")) {
												allcouponsData
														.put("couponImagePath",
																jsonCoupon
																		.getString("couponImagePath"));
											}

											if (jsonCoupon.has("couponNewFlag")) {
												allcouponsData
														.put(CommonConstants.ALLCOUPONSNEWFLAG,
																jsonCoupon
																		.getInt("newFlag")
																		+ "");
											}
											if (jsonCoupon.has("couponId")) {
												allcouponsData
														.put("couponId",
																jsonCoupon
																		.getInt("couponId")
																		+ "");
											}
											if (jsonCoupon.has("couponUsed")) {
												allcouponsData.put(
														"couponUsed",
														jsonCoupon
																.getInt("used")
																+ "");
											}
											if (jsonCoupon.has("couponListId")) {
												allcouponsData
														.put("couponListId",
																jsonCoupon
																		.getInt("couponListId")
																		+ "");
											}
											if (jsonCoupon.has("redeemFlag")) {
												allcouponsData
														.put("redeemFlag",
																jsonCoupon
																		.getInt("redeemFlag")
																		+ "");
											}
											if (jsonCoupon.has("claimFlag")) {
												allcouponsData
														.put("claimFlag",
																jsonCoupon
																		.getInt("claimFlag")
																		+ "");
											}
											if (jsonCoupon.has("coupDistance")) {
												allcouponsData
														.put("coupDistance",
																jsonCoupon
																		.getDouble("distance")
																		+ "");
											}
											allcouponsList.add(allcouponsData);
										}
									}
								} else {
									JSONObject jsonCoupon = jsonRetailer
											.getJSONObject("couponDetailsList")
											.getJSONObject("CouponDetails");
									if (jsonCoupon != null) {
										allcouponsData = new HashMap<>();
										allcouponsData.put("itemName",
												"coupons");
										if (jsonCoupon.has("couponName")) {
											allcouponsData
													.put("couponName",
															jsonCoupon
																	.getString("couponName"));
										}
										if (jsonCoupon
												.has("couponDiscountAmount")) {
											allcouponsData
													.put("couponDiscountAmount",
															jsonCoupon
																	.getString("couponDiscountAmount"));
										}
										if (jsonCoupon.has("couponURL")) {
											allcouponsData
													.put("couponURL",
															jsonCoupon
																	.getString("couponURL"));
										}
										if (jsonCoupon.has("couponExpireDate")) {
											allcouponsData
													.put("couponExpireDate",
															jsonCoupon
																	.getString("couponExpireDate"));
										}
										if (jsonCoupon.has("coupDesc")) {
											allcouponsData
													.put("coupDesc",
															jsonCoupon
																	.getString("coupDesc"));
										}
										if (jsonCoupon.has("couponDiscountPct")) {
											allcouponsData
													.put("couponDiscountPct",
															jsonCoupon
																	.getString("couponDiscountPct"));
										}
										if (jsonCoupon.has("couponStartDate")) {
											allcouponsData
													.put("couponStartDate",
															jsonCoupon
																	.getString("couponStartDate"));
										}
										if (jsonCoupon.has("couponImagePath")) {
											allcouponsData
													.put("couponImagePath",
															jsonCoupon
																	.getString("couponImagePath"));
										}

										if (jsonCoupon.has("couponNewFlag")) {
											allcouponsData
													.put(CommonConstants.ALLCOUPONSNEWFLAG,
															jsonCoupon
																	.getInt("newFlag")
																	+ "");
										}
										if (jsonCoupon.has("couponId")) {
											allcouponsData.put(
													"couponId",
													jsonCoupon
															.getInt("couponId")
															+ "");
										}
										if (jsonCoupon.has("couponUsed")) {
											allcouponsData.put("couponUsed",
													jsonCoupon.getInt("used")
															+ "");
										}
										if (jsonCoupon.has("couponListId")) {
											allcouponsData
													.put("couponListId",
															jsonCoupon
																	.getInt("couponListId")
																	+ "");
										}
										if (jsonCoupon.has("redeemFlag")) {
											allcouponsData
													.put("redeemFlag",
															jsonCoupon
																	.getInt("redeemFlag")
																	+ "");
										}
										if (jsonCoupon.has("claimFlag")) {
											allcouponsData
													.put("claimFlag",
															jsonCoupon
																	.getInt("claimFlag")
																	+ "");
										}
										if (jsonCoupon.has("coupDistance")) {
											allcouponsData
													.put("coupDistance",
															jsonCoupon
																	.getDouble("distance")
																	+ "");
										}
										allcouponsList.add(allcouponsData);
									}

								}
							}
						}
					} else {
						jsonretDetailsList = jsonCoupons.getJSONObject(
								"retDetailsList").getJSONObject(
								"RetailersDetails");

						JSONObject jsonRetdetailsObj = jsonretDetailsList
								.getJSONObject("retDetailsList");
						allcouponsData = new HashMap<>();
						int retId = jsonretDetailsList.getInt("retId");
						String retname = jsonretDetailsList
								.getString("retName");

						allcouponsData.put("itemName", "category");
						allcouponsData.put(CommonConstants.ALLCOUPONSCATID,
								retId + "");
						allcouponsData.put("cateName", retname);
						allcouponsList.add(allcouponsData);

						boolean isArrayjsonRetailer = isJSONArray(
								jsonRetdetailsObj, "RetailersDetails");
						if (isArrayjsonRetailer) {
							JSONArray jsonRetailerArray = jsonRetdetailsObj
									.getJSONArray("RetailersDetails");
							for (int retIndex = 0; retIndex < jsonRetailerArray
									.length(); retIndex++) {
								JSONObject jsonRetailer = jsonRetailerArray
										.getJSONObject(retIndex);

								allcouponsData = new HashMap<>();
								allcouponsData.put("itemName", "location");
								if (jsonRetailer.has("postalCode")) {
									allcouponsData.put("postalCode",
											jsonRetailer.getInt("postalCode")
													+ "");
								}
								if (jsonRetailer.has("minRetDist")) {
									allcouponsData.put(
											"minRetDist",
											jsonRetailer
													.getDouble("minRetDist")
													+ "");
								}
								if (jsonRetailer.has("retailerAddress")) {
									allcouponsData
											.put(CommonConstants.ALLCOUPONSLOCATION,
													jsonRetailer
															.getString("retailerAddress"));
								}
								if (jsonRetailer.has("retLocId")) {
									allcouponsData.put("retLocId",
											jsonRetailer.getInt("retLocId")
													+ "");
								}
								if (jsonRetailer.has("city")) {
									allcouponsData.put("city",
											jsonRetailer.getString("city"));
								}
								allcouponsList.add(allcouponsData);

								boolean isArrayCouponDetails = isJSONArray(
										jsonRetailer
												.getJSONObject("couponDetailsList"),
										"CouponDetails");
								JSONArray jsonCouponDetailsListArray;
								if (isArrayCouponDetails) {
									jsonCouponDetailsListArray = jsonRetailer
											.getJSONObject("couponDetailsList")
											.getJSONArray("CouponDetails");
									for (int couponIndx = 0; couponIndx < jsonCouponDetailsListArray
											.length(); couponIndx++) {
										JSONObject jsonCoupon = jsonCouponDetailsListArray
												.getJSONObject(couponIndx);

										if (jsonCoupon != null) {
											allcouponsData = new HashMap<>();
											allcouponsData.put("itemName",
													"coupons");
											if (jsonCoupon.has("couponName")) {
												allcouponsData
														.put("couponName",
																jsonCoupon
																		.getString("couponName"));
											}
											if (jsonCoupon
													.has("couponDiscountAmount")) {
												allcouponsData
														.put("couponDiscountAmount",
																jsonCoupon
																		.getString("couponDiscountAmount"));
											}
											if (jsonCoupon.has("couponURL")) {
												allcouponsData
														.put("couponURL",
																jsonCoupon
																		.getString("couponURL"));
											}
											if (jsonCoupon
													.has("couponExpireDate")) {
												allcouponsData
														.put("couponExpireDate",
																jsonCoupon
																		.getString("couponExpireDate"));
											}
											if (jsonCoupon.has("coupDesc")) {
												allcouponsData
														.put("coupDesc",
																jsonCoupon
																		.getString("coupDesc"));
											}
											if (jsonCoupon
													.has("couponDiscountPct")) {
												allcouponsData
														.put("couponDiscountPct",
																jsonCoupon
																		.getString("couponDiscountPct"));
											}
											if (jsonCoupon
													.has("couponStartDate")) {
												allcouponsData
														.put("couponStartDate",
																jsonCoupon
																		.getString("couponStartDate"));
											}
											if (jsonCoupon
													.has("couponImagePath")) {
												allcouponsData
														.put("couponImagePath",
																jsonCoupon
																		.getString("couponImagePath"));
											}

											if (jsonCoupon.has("couponNewFlag")) {
												allcouponsData
														.put(CommonConstants.ALLCOUPONSNEWFLAG,
																jsonCoupon
																		.getInt("newFlag")
																		+ "");
											}
											if (jsonCoupon.has("couponId")) {
												allcouponsData
														.put("couponId",
																jsonCoupon
																		.getInt("couponId")
																		+ "");
											}
											if (jsonCoupon.has("couponUsed")) {
												allcouponsData.put(
														"couponUsed",
														jsonCoupon
																.getInt("used")
																+ "");
											}
											if (jsonCoupon.has("couponListId")) {
												allcouponsData
														.put("couponListId",
																jsonCoupon
																		.getInt("couponListId")
																		+ "");
											}
											if (jsonCoupon.has("redeemFlag")) {
												allcouponsData
														.put("redeemFlag",
																jsonCoupon
																		.getInt("redeemFlag")
																		+ "");
											}
											if (jsonCoupon.has("claimFlag")) {
												allcouponsData
														.put("claimFlag",
																jsonCoupon
																		.getInt("claimFlag")
																		+ "");
											}
											if (jsonCoupon.has("coupDistance")) {
												allcouponsData
														.put("coupDistance",
																jsonCoupon
																		.getDouble("distance")
																		+ "");
											}
											allcouponsList.add(allcouponsData);
										}

									}
								} else {
									JSONObject jsonCoupon = jsonRetailer
											.getJSONObject("couponDetailsList")
											.getJSONObject("CouponDetails");

									if (jsonCoupon != null) {
										allcouponsData = new HashMap<>();
										allcouponsData.put("itemName",
												"coupons");
										if (jsonCoupon.has("couponName")) {
											allcouponsData
													.put("couponName",
															jsonCoupon
																	.getString("couponName"));
										}
										if (jsonCoupon
												.has("couponDiscountAmount")) {
											allcouponsData
													.put("couponDiscountAmount",
															jsonCoupon
																	.getString("couponDiscountAmount"));
										}
										if (jsonCoupon.has("couponURL")) {
											allcouponsData
													.put("couponURL",
															jsonCoupon
																	.getString("couponURL"));
										}
										if (jsonCoupon.has("couponExpireDate")) {
											allcouponsData
													.put("couponExpireDate",
															jsonCoupon
																	.getString("couponExpireDate"));
										}
										if (jsonCoupon.has("coupDesc")) {
											allcouponsData
													.put("coupDesc",
															jsonCoupon
																	.getString("coupDesc"));
										}
										if (jsonCoupon.has("couponDiscountPct")) {
											allcouponsData
													.put("couponDiscountPct",
															jsonCoupon
																	.getString("couponDiscountPct"));
										}
										if (jsonCoupon.has("couponStartDate")) {
											allcouponsData
													.put("couponStartDate",
															jsonCoupon
																	.getString("couponStartDate"));
										}
										if (jsonCoupon.has("couponImagePath")) {
											allcouponsData
													.put("couponImagePath",
															jsonCoupon
																	.getString("couponImagePath"));
										}

										if (jsonCoupon.has("couponNewFlag")) {
											allcouponsData
													.put(CommonConstants.ALLCOUPONSNEWFLAG,
															jsonCoupon
																	.getInt("newFlag")
																	+ "");
										}
										if (jsonCoupon.has("couponId")) {
											allcouponsData.put(
													"couponId",
													jsonCoupon
															.getInt("couponId")
															+ "");
										}
										if (jsonCoupon.has("used")) {
											allcouponsData.put("couponUsed",
													jsonCoupon.getInt("used")
															+ "");
										}
										if (jsonCoupon.has("couponListId")) {
											allcouponsData
													.put("couponListId",
															jsonCoupon
																	.getInt("couponListId")
																	+ "");
										}
										if (jsonCoupon.has("redeemFlag")) {
											allcouponsData
													.put("redeemFlag",
															jsonCoupon
																	.getInt("redeemFlag")
																	+ "");
										}
										if (jsonCoupon.has("claimFlag")) {
											allcouponsData
													.put("claimFlag",
															jsonCoupon
																	.getInt("claimFlag")
																	+ "");
										}
										if (jsonCoupon.has("coupDistance")) {
											allcouponsData
													.put("coupDistance",
															jsonCoupon
																	.getDouble("distance")
																	+ "");
										}
										allcouponsList.add(allcouponsData);
									}

								}

							}

						} else {

							JSONObject jsonRetailer = jsonRetdetailsObj
									.getJSONObject("RetailersDetails");

							boolean isArrayCouponDetails = isJSONArray(
									jsonRetailer
											.getJSONObject("couponDetailsList"),
									"CouponDetails");
							JSONArray jsonCouponDetailsListArray;
							if (isArrayCouponDetails) {
								jsonCouponDetailsListArray = jsonRetailer
										.getJSONObject("couponDetailsList")
										.getJSONArray("CouponDetails");
								for (int couponIndx = 0; couponIndx < jsonCouponDetailsListArray
										.length(); couponIndx++) {
									JSONObject jsonCoupon = jsonCouponDetailsListArray
											.getJSONObject(couponIndx);

									if (jsonCoupon != null) {
										allcouponsData = new HashMap<>();
										allcouponsData.put("itemName",
												"coupons");
										if (jsonCoupon.has("couponName")) {
											allcouponsData
													.put("couponName",
															jsonCoupon
																	.getString("couponName"));
										}
										if (jsonCoupon
												.has("couponDiscountAmount")) {
											allcouponsData
													.put("couponDiscountAmount",
															jsonCoupon
																	.getString("couponDiscountAmount"));
										}
										if (jsonCoupon.has("couponURL")) {
											allcouponsData
													.put("couponURL",
															jsonCoupon
																	.getString("couponURL"));
										}
										if (jsonCoupon.has("couponExpireDate")) {
											allcouponsData
													.put("couponExpireDate",
															jsonCoupon
																	.getString("couponExpireDate"));
										}
										if (jsonCoupon.has("coupDesc")) {
											allcouponsData
													.put("coupDesc",
															jsonCoupon
																	.getString("coupDesc"));
										}
										if (jsonCoupon.has("couponDiscountPct")) {
											allcouponsData
													.put("couponDiscountPct",
															jsonCoupon
																	.getString("couponDiscountPct"));
										}
										if (jsonCoupon.has("couponStartDate")) {
											allcouponsData
													.put("couponStartDate",
															jsonCoupon
																	.getString("couponStartDate"));
										}
										if (jsonCoupon.has("couponImagePath")) {
											allcouponsData
													.put("couponImagePath",
															jsonCoupon
																	.getString("couponImagePath"));
										}

										if (jsonCoupon.has("couponNewFlag")) {
											allcouponsData
													.put(CommonConstants.ALLCOUPONSNEWFLAG,
															jsonCoupon
																	.getInt("newFlag")
																	+ "");
										}
										if (jsonCoupon.has("couponId")) {
											allcouponsData.put(
													"couponId",
													jsonCoupon
															.getInt("couponId")
															+ "");
										}
										if (jsonCoupon.has("couponUsed")) {
											allcouponsData.put("couponUsed",
													jsonCoupon.getInt("used")
															+ "");
										}
										if (jsonCoupon.has("couponListId")) {
											allcouponsData
													.put("couponListId",
															jsonCoupon
																	.getInt("couponListId")
																	+ "");
										}
										if (jsonCoupon.has("redeemFlag")) {
											allcouponsData
													.put("redeemFlag",
															jsonCoupon
																	.getInt("redeemFlag")
																	+ "");
										}
										if (jsonCoupon.has("claimFlag")) {
											allcouponsData
													.put("claimFlag",
															jsonCoupon
																	.getInt("claimFlag")
																	+ "");
										}
										if (jsonCoupon.has("coupDistance")) {
											allcouponsData
													.put("coupDistance",
															jsonCoupon
																	.getDouble("distance")
																	+ "");
										}
										allcouponsList.add(allcouponsData);
									}

								}
							} else {
								JSONObject jsonCoupon = jsonRetailer
										.getJSONObject("couponDetailsList")
										.getJSONObject("CouponDetails");

								if (jsonCoupon != null) {
									allcouponsData = new HashMap<>();
									allcouponsData.put("itemName", "coupons");
									if (jsonCoupon.has("couponName")) {
										allcouponsData
												.put("couponName",
														jsonCoupon
																.getString("couponName"));
									}
									if (jsonCoupon.has("couponDiscountAmount")) {
										allcouponsData
												.put("couponDiscountAmount",
														jsonCoupon
																.getString("couponDiscountAmount"));
									}
									if (jsonCoupon.has("couponURL")) {
										allcouponsData
												.put("couponURL", jsonCoupon
														.getString("couponURL"));
									}
									if (jsonCoupon.has("couponExpireDate")) {
										allcouponsData
												.put("couponExpireDate",
														jsonCoupon
																.getString("couponExpireDate"));
									}
									if (jsonCoupon.has("coupDesc")) {
										allcouponsData.put("coupDesc",
												jsonCoupon
														.getString("coupDesc"));
									}
									if (jsonCoupon.has("couponDiscountPct")) {
										allcouponsData
												.put("couponDiscountPct",
														jsonCoupon
																.getString("couponDiscountPct"));
									}
									if (jsonCoupon.has("couponStartDate")) {
										allcouponsData
												.put("couponStartDate",
														jsonCoupon
																.getString("couponStartDate"));
									}
									if (jsonCoupon.has("couponImagePath")) {
										allcouponsData
												.put("couponImagePath",
														jsonCoupon
																.getString("couponImagePath"));
									}

									if (jsonCoupon.has("couponNewFlag")) {
										allcouponsData
												.put(CommonConstants.ALLCOUPONSNEWFLAG,
														jsonCoupon
																.getInt("newFlag")
																+ "");
									}
									if (jsonCoupon.has("couponId")) {
										allcouponsData.put("couponId",
												jsonCoupon.getInt("couponId")
														+ "");
									}
									if (jsonCoupon.has("used")) {
										allcouponsData.put("couponUsed",
												jsonCoupon.getInt("used") + "");
									}
									if (jsonCoupon.has("couponListId")) {
										allcouponsData.put(
												"couponListId",
												jsonCoupon
														.getInt("couponListId")
														+ "");
									}
									if (jsonCoupon.has("redeemFlag")) {
										allcouponsData.put("redeemFlag",
												jsonCoupon.getInt("redeemFlag")
														+ "");
									}
									if (jsonCoupon.has("claimFlag")) {
										allcouponsData.put("claimFlag",
												jsonCoupon.getInt("claimFlag")
														+ "");
									}
									if (jsonCoupon.has("coupDistance")) {
										allcouponsData.put(
												"coupDistance",
												jsonCoupon
														.getDouble("distance")
														+ "");
									}
									allcouponsList.add(allcouponsData);
								}

							}
						}
					}

				}

				if (nextPage) {

					if (maxCount > lastvisitId) {
						lastvisitId = maxRowNum;
					}

					// nextPage = false;
					hotData = new HashMap<>();
					hotData.put("itemName", "viewMore");
					hotData.put(
							CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
							maxRowNum + "");
					allcouponsList.add(hotData);
				}
				result = "true";

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;

		}

	}

	public void deleteItem(String coupon) {
		type = refType;
		lowerLimit = refLowerLt;
		couponId = coupon;
		if (type.equalsIgnoreCase(CommonConstants.COUPON_TYPE_USED)) {
			for (int count = 0; count < allcouponsList.size(); count++) {
				String couponID = allcouponsList.get(count).get(
						CommonConstants.ALLCOUPONSCOUPONID);
				if (couponID != null && couponID.equalsIgnoreCase(couponId)) {
					couponId = allcouponsList.get(count).get(
							CommonConstants.ALLCOUPONSUSEDCOUPONID);
					break;
				}
			}
		}
		new DeleteCoupon().execute();
	}

	private class DeleteCoupon extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(
					CouponsClaimedUsedExpiredActivty.this, "",
					Constants.DIALOG_MESSAGE, true, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			try {
				result = jsonObject.getJSONObject("response").getString(
						"responseCode");
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();
			if ("10000".equals(result)) {
				findCouponsCall();
			}
		}
	}

	public void addCoupon(String coupon) {
		type = refType;
		lowerLimit = refLowerLt;
		couponId = coupon;

		if (type.equalsIgnoreCase(CommonConstants.COUPON_TYPE_ALL)) {
			for (int count = 0; count < allcouponsList.size(); count++) {
				String couponID = allcouponsList.get(count).get(
						CommonConstants.ALLCOUPONSCOUPONID);

				if (couponID != null && couponID.equalsIgnoreCase(couponId)) {
					couponId = allcouponsList.get(count).get(
							CommonConstants.ALLCOUPONSCOUPONID);
					break;
				}
			}
		}
		new ClaimCoupon().execute();
	}

	public class ClaimCoupon extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		UrlRequestParams objUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(
					CouponsClaimedUsedExpiredActivty.this, "",
					Constants.DIALOG_MESSAGE);
			mDialog.setCancelable(true);
		}

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {
				String urlParameters = objUrlRequestParams
						.getClaimCoupon(couponId);
				String url_add_to_redeem_coupon = Properties.url_local_server
						+ Properties.hubciti_version + "gallery/addCoupon";
				jsonObject = mServerConnections
						.getUrlPostResponse(url_add_to_redeem_coupon,
								urlParameters, true);

				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.TAG_COUPONREDEEM_RESULTSET);

					result = jsonObject
							.getString(CommonConstants.TAG_COUPONREDEEM_RESPONSE_CODE);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();
			if ("10000".equalsIgnoreCase(result)) {
				findCouponsCall();
				isCliped = true;

			} else {
				isCliped = false;
				Toast.makeText(getApplicationContext(),
						"Please try again later.", Toast.LENGTH_SHORT).show();

			}
		}

	}

	private void hideKeyBoard() {
		CouponsClaimedUsedExpiredActivty.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private void hideKeyboardItem() {
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		/*case R.id.close_button:
			searchDialog.setVisibility(View.INVISIBLE);
			hideKeyBoard();
			hideKeyboardItem();
			break;*/

		case R.id.hotdeals_prev_page:
			lastvisitId = 0 > (minProdId - Constants.RANGE) ? 0 : minProdId
					- Constants.RANGE;
			callFindCoupons();
			break;
		case R.id.hotdeals_next_page:
			callFindCoupons();
			break;

		default:
			break;
		}

	}

	@SuppressWarnings("unused")
	private void selfSearch(String type, int id) {

		this.type = type;
		lowerLimit = "0";
		searchKey = null;
		categoryID = null;
		lastvisitId = 0;

		callFindCoupons();
	}

	LocationManager locationManager;
	String strZipcode = null;

	private Dialog createDialogToUpdateZip() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Update Zip code");
		builder.setMessage("Please enter Zip code:");

		// Use an EditText view to get user input.
		final EditText input = new EditText(this);
		builder.setView(input);

		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				strZipcode = value;
				new UpdateZipCode(getApplicationContext(), UrlRequestParams
						.getUid(), value).execute();

				if (strZipcode != null) {
					if (strZipcode.length() > 0) {
						Constants.setZipCode(strZipcode);
						couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
								CouponsClaimedUsedExpiredActivty.this,
								new ArrayList<HashMap<String, String>>());
						couponsListView.setAdapter(couponsListAdapter);
						callFindCoupons();
					}
				}
			}
		});

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						radioProd.setChecked(true);
						hideKeyBoard();
						hideKeyboardItem();
						return;
					}
				});

		return builder.create();
	}

	private void findCouponsCall() {
		/*
		 * if (AsyncTask.Status.RUNNING == findCoupons.getStatus()) {
		 * findCoupons.cancel(true); }
		 */
		firstRequest = true;
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!gpsEnabled) {
			CommonConstants.LATITUDE = null;
			CommonConstants.LONGITUDE = null;
		}

		if (locationManager.getAllProviders().contains("gps") && !gpsEnabled
				&& "".equals(Constants.getZipCode())) {
			createDialogToUpdateZip().show();
		}
		if (locationManager.getAllProviders().contains("gps") && gpsEnabled) {
			getGPSValues();
			findCoupons = new FindCoupons();
			couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
					CouponsClaimedUsedExpiredActivty.this,
					new ArrayList<HashMap<String, String>>());
			couponsListView.setAdapter(couponsListAdapter);

		} else if (!"".equals(Constants.getZipCode())) {
			couponsListAdapter = new CouponClaimRedeemExpiredListAdapter(
					CouponsClaimedUsedExpiredActivty.this,
					new ArrayList<HashMap<String, String>>());
			couponsListView.setAdapter(couponsListAdapter);

		}

	}

	public void getGPSValues() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (locationManager.getAllProviders().contains("gps") && gpsEnabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (locNew != null) {

				CommonConstants.LATITUDE = String.valueOf(locNew.getLatitude());
				CommonConstants.LONGITUDE = String.valueOf(locNew
						.getLongitude());

			} else {
				// N/W Tower Info Start
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					CommonConstants.LATITUDE = String.valueOf(locNew
							.getLatitude());
					CommonConstants.LONGITUDE = String.valueOf(locNew
							.getLongitude());
				}
			}
		}
	}

	public class SpinnerObject {
		protected String id = "";
		protected String name = "";
		protected String abbr = "";

		public SpinnerObject(String string, String name) {
			this.id = string;
			this.name = name;
		}

		public SpinnerObject(String string, String name, String abbr) {
			this.id = string;
			this.name = name;
			this.abbr = abbr;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	private void callFindCoupons() {
		if (isFirst) {
			findCoupons = new FindCoupons();
			findCoupons.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;
		}

		if (findCoupons != null) {
			if (!findCoupons.isCancelled()) {
				findCoupons.cancel(true);

			}

			findCoupons = null;

			findCoupons = new FindCoupons();
			findCoupons.execute();
		}
	}

	private void cancelAsyncTask() {
		if (findCoupons != null && !findCoupons.isCancelled()) {
			findCoupons.cancel(true);
		}

		findCoupons = null;

		Intent intent = new Intent();
		intent.setClass(this, DealsGallery.class);
		intent.putExtra("type", getIntent().getExtras().getString("type"));
		intent.putExtra("isSubMenu",
				getIntent().getExtras().getBoolean("isSubMenu"));

		String mItemId = getIntent().getExtras().getString(
				Constants.MENU_ITEM_ID_INTENT_EXTRA);
		String mBottomId = getIntent().getExtras().getString(
				Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);

		if (mItemId != null && !"".equals(mItemId)) {
			intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
		} else if (mBottomId != null && !"".equals(mBottomId)) {
			intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mBottomId);
		}

		startActivity(intent);
		finish();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cancelAsyncTask();
	}
}