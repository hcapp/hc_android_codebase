package com.scansee.android;

import java.util.ArrayList;

import com.hubcity.android.businessObjects.FilterOptionBO;

public interface OnCreateFilterChildView {
	public void setFilterOptionsForChildView(
			ArrayList<FilterOptionBO> arrFilterOptionBOs, int groupPosition,
			String whichFilter);
}
