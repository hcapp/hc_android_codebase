package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.scansee.hubregion.R;

public class HotDealsActivity extends CustomTitleBar implements
		OnClickListener, OnCheckedChangeListener {

	private final static int REQUEST_CODE_CITY = 101;
	private final static int REQUEST_CODE_PREF = 102;
	protected ListView hotdealsListView;
	protected boolean isDelete = false;

	static EditText search_hotdeals;
	String apiPartnerName = "", categoryName = "";

	UrlRequestParams objUrlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	ArrayList<HashMap<String, String>> hotdealsList = null;
	ArrayList<ArrayList<HashMap<String, String>>> mainList = new ArrayList<>();

	HotDealsAroundYouListAdapter hotdealsListAdapter;
	LocationManager locationManager;

	ImageButton nextPageBtn, prevPageBtn;
	Button cancel;
	View paginator;
	HotDeals hotDealsAsyncTask;

	// For Bottom Buttons
	LinearLayout linearLayout = null;
	BottomButtons bb = null;
	Activity activity;
	boolean hasBottomBtns = false;
	boolean enableBottomButton = true;

	String userId, category, latitude = null, longitude = null,
			accZipcode = null, cityId = null;
	String searchkey;

	String hotdealID = "";
	String hDInterested = "";
	String mItemId = "", mBottomId = "";

	String selectedTab = "local";
	int previousPosition = 0;
	int lastvisitProdId = 0, minProdId = 0;

	int maxRowNum = 0;
	int localItemCount = 0;
	int nationItemCount = 0;

	boolean isFirst, isAlreadyLoaded, isFirstLoad;
	View moreResultsView;
	RadioGroup radioDealTab;
	RadioButton radioLocal, radioNationwide;
	private CustomNavigation customNaviagation;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hotdeals_getdeals);

		try {
			CommonConstants.hamburgerIsFirst = true;
			isFirst = true;
			isFirstLoad = true;
			title.setText("Deals");
			leftTitleImage.setVisibility(View.GONE);

			activity = HotDealsActivity.this;
			// Initiating Bottom button class
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "");

			linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
			linearLayout.setVisibility(View.INVISIBLE);

			radioDealTab = (RadioGroup) findViewById(R.id.hotdeal_radio);
			radioLocal = (RadioButton) findViewById(R.id.hotdeal_radio_local);
			radioNationwide = (RadioButton) findViewById(R.id.hotdeal_radio_nationwide);

			radioLocal.setSelected(true);
			selectedTab = "local";

			radioDealTab.setOnCheckedChangeListener(this);
			hideKeyBoard();

			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                            Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }

			if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                            Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
                mBottomId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }

			findViewById(R.id.hotdeals_cancel).setOnClickListener(this);

			hotdealsList = new ArrayList<>();
			search_hotdeals = (EditText) findViewById(R.id.hotdeals_category_edit_text);
			category = "0";

			paginator = getLayoutInflater().inflate(R.layout.hot_deals_pagination,
                    hotdealsListView, true);

			search_hotdeals.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
			search_hotdeals.setOnEditorActionListener(new OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView textView, int actionId,
                        KeyEvent event) {

                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        hotdealsList = new ArrayList<>();
                        searchkey = search_hotdeals.getText().toString().trim();
                        gpsUpdate();
                        hotdealsListView.setAdapter(null);
                        mainList = new ArrayList<>();
                        isShowLoading = false;
                        callHotDealsAsyncTask();

                        category = "0";
                        lastvisitProdId = 0;
                        cityId = null;
                    }
                    hideKeyBoard();
                    hideKeyboardItem();
                    return true;
                }
            });

			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTask();
                    finish();
                }
            });

			prevPageBtn = (ImageButton) paginator
                    .findViewById(R.id.hotdeals_prev_page);
			nextPageBtn = (ImageButton) paginator
                    .findViewById(R.id.hotdeals_next_page);
			prevPageBtn.setOnClickListener(this);
			nextPageBtn.setOnClickListener(this);
			hotdealsListView = (ListView) findViewById(R.id.hot_deals_listview);
			moreResultsView = getLayoutInflater().inflate(
                    R.layout.listitem_get_retailers_viewmore, hotdealsListView,
                    false);

			final ListItemGesturesHotdeals listItemGestures = new ListItemGesturesHotdeals(
                    this);
			hotdealsListView.setOnTouchListener(listItemGestures);

			hotdealsListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
					try {
						HashMap<String, String> selectedData = new HashMap<>();

						if (radioLocal.isSelected()) {
							selectedData = mainList.get(0).get(position);
							hotdealsList = mainList.get(0);
						}
						if (radioNationwide.isSelected()) {
							selectedData = mainList.get(1).get(position);
							hotdealsList = mainList.get(1);
						}

						// String clickedItem = selectedData.get("itemName");

						if (listItemGestures.swipeDetected()) {

							if (listItemGestures.getAction().equals(
									ListItemGestures.Action.REMOVE)) {
								return;
							}
							if (listItemGestures.getAction().equals(
									ListItemGesturesHotdeals.Action.LR)
									|| listItemGestures.getAction().equals(
									ListItemGesturesHotdeals.Action.RL)) {
								isDelete = true;
								view.findViewById(R.id.hotdeal_delete)
										.setAnimation(
												SlideAnimation
														.inFromRightAnimation());
								view.findViewById(R.id.hotdeal_delete)
										.setVisibility(View.VISIBLE);

							}
						} else if ("details".equalsIgnoreCase(selectedData
								.get("itemName"))) {
							Intent dealDetail = new Intent(HotDealsActivity.this,
									HotDealsDetailsActivity.class);
							dealDetail.putExtra("hotDealId",
									selectedData.get("hotDealId"));
							dealDetail.putExtra("city", selectedData.get("city"));
							dealDetail.putExtra("hDshortDescription",
									selectedData.get("hDshortDescription"));
							dealDetail.putExtra("hotDealName",
									selectedData.get("hotDealName"));

							dealDetail.putExtra("hDPrice",
									selectedData.get("hDPrice"));
							dealDetail.putExtra("hDSalePrice",
									selectedData.get("hDSalePrice"));
							dealDetail.putExtra("hDStartDate",
									selectedData.get("hDStartDate"));
							dealDetail.putExtra("hDEndDate",
									selectedData.get("hDEndDate"));
							dealDetail.putExtra("hDStartDate",
									selectedData.get("hDStartDate"));
							dealDetail.putExtra("hotDealListId",
									selectedData.get("hotDealListId"));
							dealDetail.putExtra(
									Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
							dealDetail.putExtra(
									Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
									mBottomId);
							startActivityForResult(dealDetail, 150);
						} else {

							isDelete = false;
							view.findViewById(R.id.hotdeal_delete).setAnimation(
									SlideAnimation.outToRightAnimation());
							view.findViewById(R.id.hotdeal_delete).setVisibility(
									View.GONE);
							Parcelable state = hotdealsListView
									.onSaveInstanceState();

							if (radioLocal.isSelected()) {
								hotdealsList = mainList.get(0);

							} else if (radioNationwide.isSelected()) {
								hotdealsList = mainList.get(1);
							}

							hotdealsListAdapter = new HotDealsAroundYouListAdapter(
									HotDealsActivity.this, hotdealsList,
									selectedTab);
							hotdealsListView.setAdapter(hotdealsListAdapter);
							hotdealsListView.onRestoreInstanceState(state);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});


			//user for hamburger in modules
			drawerIcon.setVisibility(View.VISIBLE);
			drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void viewMore() {
		try {
			if (radioLocal.isSelected()) {
				hotdealsList = mainList.get(0);
			}
			if (radioNationwide.isSelected()) {
				hotdealsList = mainList.get(1);
			}
			isAlreadyLoaded = true;
			callHotDealsAsyncTask();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void callHotDealsAsyncTask() {
		try {
			previousPosition = hotdealsList.size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (isFirst) {
			hotDealsAsyncTask = new HotDeals();
			hotDealsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;

		}

		if (hotDealsAsyncTask != null) {
			hotDealsAsyncTask.cancel(true);
		}

		hotDealsAsyncTask = null;

		hotDealsAsyncTask = new HotDeals();
		hotDealsAsyncTask.execute();
	}

	private void cancelAsyncTask() {
		if (hotDealsAsyncTask != null && !hotDealsAsyncTask.isCancelled()) {
			hotDealsAsyncTask.cancel(true);
		}

		hotDealsAsyncTask = null;
	}

	private class DeleteHotdeal extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(HotDealsActivity.this, "",
					Constants.DIALOG_MESSAGE, true, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			try {

				String url_remove_hotdeal = Properties.url_local_server
						+ "/HubCithubciti_versions/removehdprod";

				String urlParameters = objUrlRequestParams.getHotdealToDelete(
						hotdealID, hDInterested);
				jsonObject = mServerConnections.getUrlPostResponse(
						url_remove_hotdeal, urlParameters, true);
				if (jsonObject != null) {
					result = jsonObject.getJSONObject("response").getString(
							"responseCode");

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				mDialog.dismiss();
				if ("10000".equals(result)) {
                    if (radioLocal.isSelected()) {
                        hotdealsList = mainList.get(0);
                    }
                    if (radioNationwide.isSelected()) {
                        hotdealsList = mainList.get(1);
                    }

                    for (int count = 0; count < hotdealsList.size(); count++) {
                        String hotDealID = hotdealsList.get(count).get("hotDealId");
                        if (hotDealID != null
                                && hotDealID.equalsIgnoreCase(hotdealID)) {
                            hotdealID = hotdealsList.get(count).get("hotDealId");
                            hotdealID = hotdealsList.get(count).get("hotDealId");

                            boolean prevApiPartner = hotdealsList.get(count - 2)
                                    .containsKey("apiPartnerName");
                            boolean prevCategory = hotdealsList.get(count - 1)
                                    .containsKey("categoryName");
                            boolean nextApiPartner = hotdealsList.get(count + 1)
                                    .containsKey("apiPartnerName");
                            boolean nextCategory = hotdealsList.get(count + 1)
                                    .containsKey("categoryName");
                            hotdealsList.remove(count);
                            if ((prevCategory && nextCategory)
                                    || (prevCategory && nextApiPartner)) {
                                hotdealsList.remove(count - 1);
                                if (prevApiPartner && nextApiPartner) {
                                    hotdealsList.remove(count - 2);
                                }
                            }
                            Parcelable state = hotdealsListView
                                    .onSaveInstanceState();
                            hotdealsListAdapter = new HotDealsAroundYouListAdapter(
                                    HotDealsActivity.this, hotdealsList,
                                    selectedTab);
                            hotdealsListView.setAdapter(hotdealsListAdapter);
                            hotdealsListView.onRestoreInstanceState(state);
                            break;
                        }
                    }

                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}
		if (isFirstLoad)
			refresh();
		isFirstLoad = false;
	}

	public void deleteItem(String hotdeal) {

		hotdealID = hotdeal;

		if (radioLocal.isSelected()) {
			hotdealsList = mainList.get(0);
		}
		if (radioNationwide.isSelected()) {
			hotdealsList = mainList.get(1);
		}

		for (int count = 0; count < hotdealsList.size(); count++) {
			String hotDealID = hotdealsList.get(count).get("hotDealId");
			if (hotDealID != null && hotDealID.equalsIgnoreCase(hotdealID)) {
				hotdealID = hotdealsList.get(count).get("hotDealId");
				hotdealID = hotdealsList.get(count).get("hotDealId");

				break;
			}
		}
		new DeleteHotdeal().execute();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (!"".equals(search_hotdeals.getText())) {
			search_hotdeals.setText("");
			searchkey = "";
		}
	}

	private void hideKeyboardItem() {
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(search_hotdeals.getWindowToken(), 0);
	}

	private void hideKeyBoard() {
		HotDealsActivity.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	@Override
	public void onDestroy() {
		if (hotdealsListView != null) {
			hotdealsListView.setAdapter(null);
		}
		super.onDestroy();
	}

	boolean nextPage;
	boolean isShowLoading;

	private class HotDeals extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		HashMap<String, String> hotData = null, categoryData = null,
				retailerData = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			if (!isShowLoading) {
				isShowLoading = true;
				mDialog = ProgressDialog.show(HotDealsActivity.this, "",
						Constants.DIALOG_MESSAGE, true);
				mDialog.setCancelable(false);
			}
		}

		@SuppressWarnings("static-access")
		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {

				String urlParameters = objUrlRequestParams
						.getHotDealsAroundYou(category, latitude, longitude,
								String.valueOf(lastvisitProdId), cityId,
								searchkey, mItemId, mBottomId, accZipcode);
				String get_hotdeals_aroundyou = Properties.url_local_server
						+ Properties.hubciti_version
						+ "hotdeals/gethotdealprods";
				jsonObject = mServerConnections.getUrlPostResponse(
						get_hotdeals_aroundyou, urlParameters, true);

				if (jsonObject != null) {
					try {
						nextPage = "1".equalsIgnoreCase(jsonObject.getJSONObject(
								"HotDealsListResultSet").getString("nextPage"));
					} catch (Exception e) {
						e.printStackTrace();
						if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
								"response").getString("responseCode"))) {
							result = jsonObject.getJSONObject("response")
									.getString("responseText");
						}
					}

					// If there are bottom buttons
					if (jsonObject.getJSONObject("HotDealsListResultSet").has(
							"bottomBtn")
							&& "1".equals(jsonObject.getJSONObject(
									"HotDealsListResultSet").getString(
									"bottomBtn"))) {

						hasBottomBtns = true;
						JSONObject jsonBBObject = null;

						jsonBBObject = jsonObject.getJSONObject(
								"HotDealsListResultSet").getJSONObject(
								"arBottomBtnList");

						if (jsonBBObject.has("BottomButton")) {

							try {
								ArrayList<BottomButtonBO> bottomButtonList = bb
										.parseForBottomButton(jsonBBObject);
								BottomButtonListSingleton
										.clearBottomButtonListSingleton();
								BottomButtonListSingleton
										.getListBottomButton(bottomButtonList);

							} catch (JSONException e1) {
								e1.printStackTrace();
							}

						}

					} else {
						hasBottomBtns = false;
					}

					try {
						if (jsonObject.getJSONObject("HotDealsListResultSet")
								.has("mainMenuId")) {
							SharedPreferences preferences = HubCityContext
									.getHubCityContext()
									.getSharedPreferences(
											Constants.PREFERENCE_HUB_CITY,
											HubCityContext.getHubCityContext().MODE_PRIVATE);
							SharedPreferences.Editor e = preferences
									.edit()
									.putString(
											Constants.PREFERENCE_KEY_MAIN_MENU_ID,
											jsonObject.getJSONObject(
													"HotDealsListResultSet")
													.getString("mainMenuId"));
							e.apply();

						}
					} catch (Exception e) {
						e.printStackTrace();
						if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
								"response").getString("responseCode"))) {
							result = jsonObject.getJSONObject("response")
									.getString("responseText");
						}
					}

					if (jsonObject.getJSONObject("HotDealsListResultSet").has(
							"maxRowNum")) {
						maxRowNum = jsonObject.getJSONObject(
								"HotDealsListResultSet").getInt("maxRowNum");
					}

					JSONObject jsonHotDealsList = null;
					JSONArray jsonHotDealsArray = new JSONArray();
					JSONArray jsonHotDealsListArray = null;
					boolean isHotdealsArray = false;

					if (jsonObject.has("HotDealsListResultSet")) {
						minProdId = lastvisitProdId;

						try {
							jsonHotDealsArray.put(jsonObject
									.getJSONObject("HotDealsListResultSet")
									.getJSONObject("dealTypeList")
									.getJSONObject("HotDealsListResultSet"));

						} catch (Exception e) {
							e.printStackTrace();
							jsonHotDealsArray = jsonObject
									.getJSONObject("HotDealsListResultSet")
									.getJSONObject("dealTypeList")
									.getJSONArray("HotDealsListResultSet");
						}

						for (int i = 0; i < jsonHotDealsArray.length(); i++) {

							hotdealsList = new ArrayList<>();

							isHotdealsArray = isJSONArray(
									jsonHotDealsArray.getJSONObject(i)
											.getJSONObject("hdAPIResult"),
									"HotDealAPIResultSet");

							if (isHotdealsArray) {
								jsonHotDealsListArray = jsonHotDealsArray
										.getJSONObject(i)
										.getJSONObject("hdAPIResult")
										.getJSONArray("HotDealAPIResultSet");
								for (int dealIndex = 0; dealIndex < jsonHotDealsListArray
										.length(); dealIndex++) {
									String strApiPartnerName = jsonHotDealsListArray
											.getJSONObject(dealIndex)
											.getString("apiPartnerName");
									String strApiPartnerId = jsonHotDealsListArray
											.getJSONObject(dealIndex)
											.getString("apiPartnerId");

									hotData = new HashMap<>();

									retailerData = new HashMap<>();
									if (strApiPartnerName != null
											&& retailerData != null) {
										retailerData.put("itemName",
												"apiPartnerName");
										retailerData.put("apiPartnerName",
												strApiPartnerName);
										retailerData.put("apiPartnerId",
												strApiPartnerId);
									}

									boolean isArrayCatInfoLst = isJSONArray(
											jsonHotDealsListArray
													.getJSONObject(dealIndex),
											"hdCatInfoList");
									JSONObject jsonCatInfoLst = null;
									JSONArray jsonCatInfoLstArray = null;
									if (isArrayCatInfoLst) {
										jsonCatInfoLstArray = jsonHotDealsListArray
												.getJSONObject(dealIndex)
												.getJSONArray("hdCatInfoList");
										for (int catIndex = 0; catIndex < jsonCatInfoLstArray
												.length(); catIndex++) {
											if (!apiPartnerName
													.equals(strApiPartnerName)) {
												hotdealsList.add(retailerData);
												apiPartnerName = strApiPartnerName;
											}

											categoryData = new HashMap<>();
											categoryData.put("itemName",
													"categoryName");
											categoryData
													.put("categoryId",
															jsonCatInfoLstArray
																	.getJSONObject(
																			catIndex)
																	.getString(
																			"categoryId"));
											categoryData
													.put("categoryName",
															jsonCatInfoLstArray
																	.getJSONObject(
																			catIndex)
																	.getString(
																			"categoryName"));
											if (!categoryName
													.equals(jsonCatInfoLstArray
															.getJSONObject(
																	catIndex)
															.getString(
																	"categoryName"))) {
												categoryName = jsonCatInfoLstArray
														.getJSONObject(catIndex)
														.getString(
																"categoryName");
												hotdealsList.add(categoryData);
											}

											boolean isDealDetailsArray = isJSONArray(
													jsonCatInfoLstArray
															.getJSONObject(catIndex),
													"hotDealsDetailsArrayLst");
											if (!isDealDetailsArray) {
												JSONArray hotDealsDetailsArray = null;
												JSONObject hotDealsDetails = null;
												boolean isHotDealsDetailsArray = isJSONArray(
														jsonCatInfoLstArray
																.getJSONObject(
																		catIndex)
																.getJSONObject(
																		"hotDealsDetailsArrayLst"),
														"HotDealsDetails");
												if (isHotDealsDetailsArray) {
													hotDealsDetailsArray = jsonCatInfoLstArray
															.getJSONObject(
																	catIndex)
															.getJSONObject(
																	"hotDealsDetailsArrayLst")
															.getJSONArray(
																	"HotDealsDetails");
													for (int dIndex = 0; dIndex < hotDealsDetailsArray
															.length(); dIndex++) {
														hotdealsList
																.add(getJSONValues(hotDealsDetailsArray
																		.getJSONObject(dIndex)));
													}
												} else {
													hotDealsDetails = jsonCatInfoLstArray
															.getJSONObject(
																	catIndex)
															.getJSONObject(
																	"hotDealsDetailsArrayLst")
															.getJSONObject(
																	"HotDealsDetails");
													hotdealsList
															.add(getJSONValues(hotDealsDetails));
												}
											}
										}
									} else {

										boolean isCatInfoArray = isJSONArray(
												jsonHotDealsListArray
														.getJSONObject(
																dealIndex)
														.getJSONObject(
																"hdCatInfoList"),
												"HotDealsCategoryInfo");

										if (isCatInfoArray) {
											jsonCatInfoLstArray = jsonHotDealsListArray
													.getJSONObject(dealIndex)
													.getJSONObject(
															"hdCatInfoList")
													.getJSONArray(
															"HotDealsCategoryInfo");
											for (int dInx = 0; dInx < jsonCatInfoLstArray
													.length(); dInx++) {
												if (!apiPartnerName
														.equals(strApiPartnerName)) {
													hotdealsList
															.add(retailerData);
													apiPartnerName = strApiPartnerName;
												}
												categoryData = new HashMap<>();
												categoryData.put("itemName",
														"categoryName");
												categoryData
														.put("categoryId",
																jsonCatInfoLstArray
																		.getJSONObject(
																				dInx)
																		.getString(
																				"categoryId"));
												categoryData
														.put("categoryName",
																jsonCatInfoLstArray
																		.getJSONObject(
																				dInx)
																		.getString(
																				"categoryName"));
												if (!categoryName
														.equals(jsonCatInfoLstArray
																.getJSONObject(
																		dInx)
																.getString(
																		"categoryName"))) {
													categoryName = jsonCatInfoLstArray
															.getJSONObject(dInx)
															.getString(
																	"categoryName");
													hotdealsList
															.add(categoryData);
												}
												boolean isDealDetailsArray = isJSONArray(
														jsonCatInfoLstArray
																.getJSONObject(dInx),
														"hotDealsDetailsArrayLst");
												if (!isDealDetailsArray) {
													JSONArray hotDealsDetailsArray = null;
													JSONObject hotDealsDetails = null;
													boolean isHotDealsDetailsArray = isJSONArray(
															jsonCatInfoLstArray
																	.getJSONObject(
																			dInx)
																	.getJSONObject(
																			"hotDealsDetailsArrayLst"),
															"HotDealsDetails");
													if (isHotDealsDetailsArray) {
														hotDealsDetailsArray = jsonCatInfoLstArray
																.getJSONObject(
																		dInx)
																.getJSONObject(
																		"hotDealsDetailsArrayLst")
																.getJSONArray(
																		"HotDealsDetails");
														for (int dIndex = 0; dIndex < hotDealsDetailsArray
																.length(); dIndex++) {
															hotdealsList
																	.add(getJSONValues(hotDealsDetailsArray
																			.getJSONObject(dIndex)));
														}
													} else {
														hotDealsDetails = jsonCatInfoLstArray
																.getJSONObject(
																		dInx)
																.getJSONObject(
																		"hotDealsDetailsArrayLst")
																.getJSONObject(
																		"HotDealsDetails");
														hotdealsList
																.add(getJSONValues(hotDealsDetails));
													}
												}
											}
										} else {
											jsonCatInfoLst = jsonHotDealsListArray
													.getJSONObject(dealIndex)
													.getJSONObject(
															"hdCatInfoList")
													.getJSONObject(
															"HotDealsCategoryInfo");
											if (!apiPartnerName
													.equals(strApiPartnerName)) {
												hotdealsList.add(retailerData);
												apiPartnerName = strApiPartnerName;
											}
											categoryData = new HashMap<>();
											categoryData.put("itemName",
													"categoryName");
											categoryData
													.put("categoryId",
															jsonCatInfoLst
																	.getString("categoryId"));
											categoryData
													.put("categoryName",
															jsonCatInfoLst
																	.getString("categoryName"));
											if (!categoryName
													.equals(jsonCatInfoLst
															.getString("categoryName"))) {
												categoryName = jsonCatInfoLst
														.getString("categoryName");
												hotdealsList.add(categoryData);
											}
											boolean isDealDetailsArray = isJSONArray(
													jsonCatInfoLst,
													"hotDealsDetailsArrayLst");
											if (!isDealDetailsArray) {
												JSONArray hotDealsDetailsArray = null;
												JSONObject hotDealsDetails = null;
												boolean isHotDealsDetailsArray = isJSONArray(
														jsonCatInfoLst
																.getJSONObject("hotDealsDetailsArrayLst"),
														"HotDealsDetails");
												if (isHotDealsDetailsArray) {
													hotDealsDetailsArray = jsonCatInfoLst
															.getJSONObject(
																	"hotDealsDetailsArrayLst")
															.getJSONArray(
																	"HotDealsDetails");
													for (int dIndex = 0; dIndex < hotDealsDetailsArray
															.length(); dIndex++) {
														hotdealsList
																.add(getJSONValues(hotDealsDetailsArray
																		.getJSONObject(dIndex)));
													}
												} else {
													hotDealsDetails = jsonCatInfoLst
															.getJSONObject(
																	"hotDealsDetailsArrayLst")
															.getJSONObject(
																	"HotDealsDetails");
													hotdealsList
															.add(getJSONValues(hotDealsDetails));
												}
											}
										}
									}
								}
							} else {
								jsonHotDealsList = jsonHotDealsArray
										.getJSONObject(i)
										.getJSONObject("hdAPIResult")
										.getJSONObject("HotDealAPIResultSet");
								String strApiPartnerName = jsonHotDealsList
										.getString("apiPartnerName");
								String strApiPartnerId = jsonHotDealsList
										.getString("apiPartnerId");

								hotData = new HashMap<>();
								retailerData = new HashMap<>();
								if (strApiPartnerName != null
										&& retailerData != null) {
									retailerData.put("itemName",
											"apiPartnerName");
									retailerData.put("apiPartnerName",
											strApiPartnerName);
									retailerData.put("apiPartnerId",
											strApiPartnerId);
								}
								boolean isArrayCatInfoLst = isJSONArray(
										jsonHotDealsList, "hdCatInfoList");
								JSONObject jsonCatInfoLst = null;
								JSONArray jsonCatInfoLstArray = null;
								if (isArrayCatInfoLst) {
									jsonCatInfoLstArray = jsonHotDealsList
											.getJSONArray("hdCatInfoList");
									for (int catIndex = 0; catIndex < jsonCatInfoLstArray
											.length(); catIndex++) {
										if (!apiPartnerName
												.equals(strApiPartnerName)) {
											hotdealsList.add(retailerData);
											apiPartnerName = strApiPartnerName;
										}
										categoryData = new HashMap<>();
										categoryData.put("itemName",
												"categoryName");
										categoryData
												.put("categoryId",
														jsonCatInfoLstArray
																.getJSONObject(
																		catIndex)
																.getString(
																		"categoryId"));
										categoryData
												.put("categoryName",
														jsonCatInfoLstArray
																.getJSONObject(
																		catIndex)
																.getString(
																		"categoryName"));
										if (!categoryName
												.equals(jsonCatInfoLstArray
														.getJSONObject(catIndex)
														.getString(
																"categoryName"))) {
											categoryName = jsonCatInfoLstArray
													.getJSONObject(catIndex)
													.getString("categoryName");
											hotdealsList.add(categoryData);
										}
										boolean isDealDetailsArray = isJSONArray(
												jsonCatInfoLstArray
														.getJSONObject(catIndex),
												"hotDealsDetailsArrayLst");
										if (!isDealDetailsArray) {
											JSONArray hotDealsDetailsArray = null;
											JSONObject hotDealsDetails = null;
											boolean isHotDealsDetailsArray = isJSONArray(
													jsonCatInfoLstArray
															.getJSONObject(
																	catIndex)
															.getJSONObject(
																	"hotDealsDetailsArrayLst"),
													"HotDealsDetails");
											if (isHotDealsDetailsArray) {
												hotDealsDetailsArray = jsonCatInfoLstArray
														.getJSONObject(catIndex)
														.getJSONObject(
																"hotDealsDetailsArrayLst")
														.getJSONArray(
																"HotDealsDetails");
												for (int dIndex = 0; dIndex < hotDealsDetailsArray
														.length(); dIndex++) {
													hotdealsList
															.add(getJSONValues(hotDealsDetailsArray
																	.getJSONObject(dIndex)));
												}
											} else {
												hotDealsDetails = jsonCatInfoLstArray
														.getJSONObject(catIndex)
														.getJSONObject(
																"hotDealsDetailsArrayLst")
														.getJSONObject(
																"HotDealsDetails");
												hotdealsList
														.add(getJSONValues(hotDealsDetails));
											}
										}
									}
								} else {

									jsonCatInfoLst = jsonHotDealsList
											.getJSONObject("hdCatInfoList");
									boolean isArrayHotDeals = isJSONArray(
											jsonCatInfoLst,
											"HotDealsCategoryInfo");
									JSONArray jsonCategoryListArray = null;
									JSONObject jsonCategoryList = null;

									if (isArrayHotDeals) {
										jsonCategoryListArray = jsonCatInfoLst
												.getJSONArray("HotDealsCategoryInfo");
										for (int categoryIndx = 0; categoryIndx < jsonCategoryListArray
												.length(); categoryIndx++) {
											if (!apiPartnerName
													.equals(strApiPartnerName)) {
												hotdealsList.add(retailerData);
												apiPartnerName = strApiPartnerName;
											}
											categoryData = new HashMap<>();
											categoryData.put("itemName",
													"categoryName");
											categoryData
													.put("categoryId",
															jsonCategoryListArray
																	.getJSONObject(
																			categoryIndx)
																	.getString(
																			"categoryId"));
											categoryData
													.put("categoryName",
															jsonCategoryListArray
																	.getJSONObject(
																			categoryIndx)
																	.getString(
																			"categoryName"));
											if (!categoryName
													.equals(jsonCategoryListArray
															.getJSONObject(
																	categoryIndx)
															.getString(
																	"categoryName"))) {
												categoryName = jsonCategoryListArray
														.getJSONObject(
																categoryIndx)
														.getString(
																"categoryName");
												hotdealsList.add(categoryData);
											}

											JSONArray hotDealsDetailsArray = null;
											JSONObject hotDealsDetails = null;
											boolean isHotDealsDetailsArray = isJSONArray(
													jsonCategoryListArray
															.getJSONObject(
																	categoryIndx)
															.getJSONObject(
																	"hotDealsDetailsArrayLst"),
													"HotDealsDetails");
											if (isHotDealsDetailsArray) {
												hotDealsDetailsArray = jsonCategoryListArray
														.getJSONObject(
																categoryIndx)
														.getJSONObject(
																"hotDealsDetailsArrayLst")
														.getJSONArray(
																"HotDealsDetails");
												for (int dIndex = 0; dIndex < hotDealsDetailsArray
														.length(); dIndex++) {
													hotdealsList
															.add(getJSONValues(hotDealsDetailsArray
																	.getJSONObject(dIndex)));
												}
											} else {
												hotDealsDetails = jsonCategoryListArray
														.getJSONObject(
																categoryIndx)
														.getJSONObject(
																"hotDealsDetailsArrayLst")
														.getJSONObject(
																"HotDealsDetails");
												hotdealsList
														.add(getJSONValues(hotDealsDetails));
											}
										}

									} else {
										jsonCategoryList = jsonCatInfoLst
												.getJSONObject("HotDealsCategoryInfo");
										if (!apiPartnerName
												.equals(strApiPartnerName)) {
											hotdealsList.add(retailerData);
											apiPartnerName = strApiPartnerName;
										}
										categoryData = new HashMap<>();
										categoryData.put("itemName",
												"categoryName");
										categoryData
												.put("categoryId",
														jsonCategoryList
																.getString("categoryId"));
										categoryData
												.put("categoryName",
														jsonCategoryList
																.getString("categoryName"));
										if (!categoryName
												.equals(jsonCategoryList
														.getString("categoryName"))) {
											categoryName = jsonCategoryList
													.getString("categoryName");
											hotdealsList.add(categoryData);
										}
										boolean isDealDetailsArray = isJSONArray(
												jsonCategoryList,
												"hotDealsDetailsArrayLst");
										if (!isDealDetailsArray) {
											JSONArray hotDealsDetailsArray = null;
											JSONObject hotDealsDetails = null;
											boolean isHotDealsDetailsArray = isJSONArray(
													jsonCategoryList
															.getJSONObject("hotDealsDetailsArrayLst"),
													"HotDealsDetails");
											if (isHotDealsDetailsArray) {
												hotDealsDetailsArray = jsonCategoryList
														.getJSONObject(
																"hotDealsDetailsArrayLst")
														.getJSONArray(
																"HotDealsDetails");
												for (int dIndex = 0; dIndex < hotDealsDetailsArray
														.length(); dIndex++) {
													hotdealsList
															.add(getJSONValues(hotDealsDetailsArray
																	.getJSONObject(dIndex)));
												}
											} else {
												hotDealsDetails = jsonCategoryList
														.getJSONObject(
																"hotDealsDetailsArrayLst")
														.getJSONObject(
																"HotDealsDetails");
												hotdealsList
														.add(getJSONValues(hotDealsDetails));
											}
										}

									}
								}

							}

							if (nextPage) {
								lastvisitProdId = maxRowNum;
								// hotData = new HashMap<String, String>();
								// hotData.put("itemName", "viewMore");
								// hotData.put(
								// CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
								// maxRowNum + "");
								// hotdealsList.add(hotData);

							}

							if (jsonHotDealsArray.length() == 1
									&& jsonHotDealsArray.getJSONObject(i).has(
											"dealType")) {
								if ("Local"
										.equalsIgnoreCase(jsonHotDealsArray
												.getJSONObject(i).getString(
														"dealType"))) {
									for (int j = 0; j < hotdealsList.size(); j++) {
										if (isAlreadyLoaded)
											mainList.get(0).add(
													hotdealsList.get(j));
										else {
											mainList.add(hotdealsList);
											mainList.add(new ArrayList<HashMap<String, String>>());
										}
									}
									// mainList.add(hotdealsList);
									// mainList.add(new
									// ArrayList<HashMap<String, String>>());

								} else if ("Nationwide"
										.equalsIgnoreCase(jsonHotDealsArray
												.getJSONObject(i).getString(
														"dealType"))) {
									for (int j = 0; j < hotdealsList.size(); j++) {
										if (isAlreadyLoaded)
											mainList.get(1).add(
													hotdealsList.get(j));
										else {
											mainList.add(new ArrayList<HashMap<String, String>>());
											mainList.add(hotdealsList);
										}
									}

								}

							} else {
								if (i == 0) {
									apiPartnerName = "";
									categoryName = "";
								}
								mainList.add(hotdealsList);
								for (int index = 0; index < hotdealsList.size(); index++) {
									if ("Local"
											.equalsIgnoreCase(jsonHotDealsArray
													.getJSONObject(i)
													.getString("dealType"))) {
										if (hotdealsList.get(index)
												.get("itemName")
												.equalsIgnoreCase("details")) {
											localItemCount = localItemCount + 1;
										}
									} else if ("Nationwide"
											.equalsIgnoreCase(jsonHotDealsArray
													.getJSONObject(i)
													.getString("dealType"))) {
										if (hotdealsList.get(index)
												.get("itemName")
												.equalsIgnoreCase("details")) {
											nationItemCount = nationItemCount + 1;
										}
									}
								}
							}

						}

						// if (nextPage) {
						// nextPage = false;
						// }

						result = "true";
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		public boolean isJSONArray(JSONObject jsonObject, String value) {
			boolean isArray = false;

			JSONObject isJSONObject = jsonObject.optJSONObject(value);
			if (isJSONObject == null) {
				JSONArray isJSONArray = jsonObject.optJSONArray(value);
				if (isJSONArray != null) {
					isArray = true;
				}
			}

			return isArray;
		}

		public HashMap<String, String> getJSONValues(JSONObject jsonHotDeal) {
			hotData = new HashMap<>();
			hotData.put("itemName", "details");
			try {
				hotData.put("hDSalePrice", jsonHotDeal.getString("hDSalePrice"));
				hotData.put("hDshortDescription",
						jsonHotDeal.getString("hDshortDescription"));
				hotData.put("hdURL", jsonHotDeal.getString("hdURL"));
				hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
				hotData.put("newFlag", jsonHotDeal.getString("newFlag"));
				hotData.put("hotDealImagePath",
						jsonHotDeal.getString("hotDealImagePath"));
				hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
				hotData.put("hDStartDate", jsonHotDeal.getString("hDStartDate"));
				hotData.put("hDPrice", jsonHotDeal.getString("hDPrice"));
				hotData.put("rowNumber", jsonHotDeal.getString("rowNumber"));
				hotData.put("hotDealId", jsonHotDeal.getString("hotDealId"));
				hotData.put("city", jsonHotDeal.getString("city"));
				hotData.put("hDDiscountPct",
						jsonHotDeal.getString("hDDiscountPct"));
				hotData.put("hotDealListId",
						jsonHotDeal.getString("hotDealListId"));
				hotData.put("hotDealName", jsonHotDeal.getString("hotDealName"));
				hotData.put("extFlag", jsonHotDeal.getString("extFlag"));
				if (Integer.valueOf(hotData.get("rowNumber")) > lastvisitProdId) {
					lastvisitProdId = Integer.valueOf(hotData.get("rowNumber"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return hotData;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				search_hotdeals.setText(null);
				findViewById(R.id.no_coupons_found).setVisibility(View.INVISIBLE);

				try {
                    isAlreadyLoaded = false;
                    if ("true".equals(result)) {

                        try {
                            if (nextPage) {

                                try {
                                    moreResultsView.setVisibility(View.VISIBLE);
                                    hotdealsListView
                                            .removeFooterView(moreResultsView);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                                // moreInfo.setVisibility(View.VISIBLE);

                                hotdealsListView.addFooterView(moreResultsView);

                            } else {
                                hotdealsListView.removeFooterView(moreResultsView);
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if ("local".equals(selectedTab)) {
                            radioLocal.setSelected(true);
                            radioNationwide.setSelected(false);

                        } else if ("nation".equals(selectedTab)) {
                            radioLocal.setSelected(false);
                            radioNationwide.setSelected(true);
                        }

                        if (radioLocal.isSelected()) {
                            if (!mainList.get(0).isEmpty()) {
                                hotdealsListAdapter = new HotDealsAroundYouListAdapter(
                                        HotDealsActivity.this, mainList.get(0),
                                        selectedTab);
                                hotdealsListView.setAdapter(hotdealsListAdapter);
                                hotdealsListView.setSelection(previousPosition);
                                // hotdealsListView.onRestoreInstanceState(state);
                            } else {
                                findViewById(R.id.no_coupons_found).setVisibility(
                                        View.VISIBLE);
                                ((HotDealsActivity) activity).hotdealsListView
                                        .removeFooterView(((HotDealsActivity) activity).moreResultsView);
                                // radioLocal.setSelected(false);
                                // radioNationwide.setSelected(true);
                                // selectedTab = "nation";

                            }
                        }

                        if (radioNationwide.isSelected()) {
                            if (!mainList.get(1).isEmpty()) {
                                hotdealsListAdapter = new HotDealsAroundYouListAdapter(
                                        HotDealsActivity.this, mainList.get(1),
                                        selectedTab);
                                hotdealsListView.setAdapter(hotdealsListAdapter);
                                hotdealsListView.setSelection(previousPosition);
                                // hotdealsListView.onRestoreInstanceState(state);
                            } else {
                                findViewById(R.id.no_coupons_found).setVisibility(
                                        View.VISIBLE);
                                ((HotDealsActivity) activity).hotdealsListView
                                        .removeFooterView(((HotDealsActivity) activity).moreResultsView);
                            }

                        }

                    } else {
                        hotdealsListView.setAdapter(null);
                        prevPageBtn.setVisibility(View.GONE);
                        nextPageBtn.setVisibility(View.GONE);
                        findViewById(R.id.no_coupons_found).setVisibility(
                                View.VISIBLE);
                        try {
                            ((HotDealsActivity) activity).hotdealsListView
                                    .removeFooterView(((HotDealsActivity) activity).moreResultsView);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (hasBottomBtns && enableBottomButton) {

                        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) hotdealsListView
                                .getLayoutParams();
                        mlp.setMargins(0, 0, 0, 2);

                        bb.createbottomButtontTab(linearLayout, false);
                        enableBottomButton = false;
                    }

                    linearLayout.setVisibility(View.VISIBLE);

                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.hotdeals_cancel:
			hotDealsCancel();
			break;
		case R.id.hotdeals_prev_page:
			hotdealsPrevPage();
			break;
		case R.id.hotdeals_next_page:
			gpsUpdate();
			new CustomImageLoader(getApplicationContext(), "HotDealsImages")
					.clearCache();
			break;
		default:
			break;
		}

	}

	private void hotdealsPrevPage() {
		lastvisitProdId = 0 > (minProdId - 31) ? 0 : minProdId - 31;
		new CustomImageLoader(getApplicationContext(), "HotDealsImages").clearCache();
		gpsUpdate();

	}

	private void hotDealsCancel() {
		if (search_hotdeals != null) {
			search_hotdeals.setText("");
			hideKeyboardItem();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			cancelAsyncTask();
			finish();
		}
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case REQUEST_CODE_CITY:
				requestCodeCity(data);
				break;
			case REQUEST_CODE_PREF:
				refresh();
				break;
			default:
				break;
			}
		}
	}

	private void requestCodeCity(Intent data) {
		if (data.hasExtra("selData") && data.hasExtra("selCityName")) {
			isShowLoading = false;
			nextPage = false;
			category = "0";
			latitude = null;
			longitude = null;
			lastvisitProdId = 0;
			cityId = data.getExtras().getString("selData");
			searchkey = null;
			callHotDealsAsyncTask();
		}
	}

	@SuppressWarnings("deprecation")
	private void gpsUpdate() {
		latitude = null;
		longitude = null;

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		String provider = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (provider.contains("gps") && gpsEnabled) {

			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (locNew != null) {

				latitude = String.valueOf(locNew.getLatitude());
				longitude = String.valueOf(locNew.getLongitude());

			} else {
				// N/W Tower Info Start
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					latitude = String.valueOf(locNew.getLatitude());
					longitude = String.valueOf(locNew.getLongitude());
				} else {
					accZipcode = Constants.getZipCode();
				}
			}
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cancelAsyncTask();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		if (mainList != null && !mainList.isEmpty()) {
			findViewById(R.id.no_coupons_found).setVisibility(View.GONE);
			// hotdealsListView.addFooterView(moreResultsView);
			if (radioLocal.isChecked()) {
				radioLocal.setSelected(true);
				radioNationwide.setSelected(false);
				selectedTab = "local";
				if (!mainList.get(0).isEmpty()) {
					hotdealsListAdapter = new HotDealsAroundYouListAdapter(
							HotDealsActivity.this, mainList.get(0), selectedTab);
					hotdealsListView.setAdapter(hotdealsListAdapter);

				} else {
					hotdealsListView.setAdapter(null);
					findViewById(R.id.no_coupons_found).setVisibility(
							View.VISIBLE);
					hotdealsListView.removeFooterView(moreResultsView);
				}

			} else if (radioNationwide.isChecked()) {

				radioLocal.setSelected(false);
				radioNationwide.setSelected(true);
				selectedTab = "nation";
				if (!mainList.get(1).isEmpty()) {
					hotdealsListAdapter = new HotDealsAroundYouListAdapter(
							HotDealsActivity.this, mainList.get(1), selectedTab);
					hotdealsListView.setAdapter(hotdealsListAdapter);
				} else {

					hotdealsListView.setAdapter(null);
					findViewById(R.id.no_coupons_found).setVisibility(
							View.VISIBLE);
					hotdealsListView.removeFooterView(moreResultsView);
				}

			}

			// refresh();

		}
	}

	public void refresh() {
		lastvisitProdId = 0;
		searchkey = null;
		category = "0";
		cityId = null;
		apiPartnerName = "";
		categoryName = "";
		hotdealsList = new ArrayList<>();
		mainList = new ArrayList<>();
		hotdealsListView.setAdapter(null);
		try {
			hotdealsListView.removeFooterView(moreResultsView);
		} catch (Exception e) {
			e.printStackTrace();
		}
		isShowLoading = false;
		gpsUpdate();
		callHotDealsAsyncTask();
	}
}
