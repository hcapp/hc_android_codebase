package com.scansee.android;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class LocateOnMapActivity extends CustomTitleBar implements
		OnMarkerClickListener, OnMapClickListener, OnClickListener {
	private GoogleMap googleMap;
	UiSettings mUiSettings;
	private Marker mMarker;
	MarkerOptions markerOptions;
	String zipCode, newZipCode;
	double mapLng;
	double latitude, longitude;
	double mapLat;
	SharedPreferences settings;
	String enterzipCode;
	List<Address> address;
	UrlRequestParams mUrlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.thislocation_google_maps);

		try {
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText("Locate on Map");
			rightImage.setImageBitmap(null);
			rightImage.setBackgroundResource(R.drawable.ic_action_accept);
			leftTitleImage.setVisibility(View.GONE);

			enterzipCode = getIntent().getExtras().getString(
                    CommonConstants.ZIP_CODE);
			settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
			rightImage.setOnClickListener(this);

			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.this_location_map)).getMap();

			if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();

            } else {
                googleMap.setOnMapClickListener(this);
                new GetGeoCode().execute();

            }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * function to load map. If map is not created it will create it for you
	 */
	private void initilizeMap() {

		// create marker
		markerOptions = new MarkerOptions()
				.position(new LatLng(mapLat, mapLng)).title("Locate Map");

		// adding marker
		mMarker = googleMap.addMarker(markerOptions);

		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
				mapLat, mapLng), 14));

		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(getApplicationContext(),
					"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	public class GetGeoCode extends AsyncTask<Void, Void, Boolean> {

		JSONObject jsonObject = null;
		JSONArray jsonArray = null;
		JSONObject resultSet = null;
		JSONArray addressComp = null;
		private ProgressDialog mDialog;
		boolean isZipCodeLocated;

		@Override
		protected void onPostExecute(Boolean result) {
			try {
				mDialog.dismiss();

				if (isZipCodeLocated) {

                    initilizeMap();
                } else {

                    Toast.makeText(getApplicationContext(),
                            "Sorry! unable to locate zipcode", Toast.LENGTH_SHORT)
                            .show();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(LocateOnMapActivity.this, "",
					"Fetching Location Details ", true);
			mDialog.setCancelable(false);
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			try {

				String fetch_latlong = Properties.url_local_server
						+ Properties.hubciti_version
						+ "thislocation/fetchlatlong?zipcode=";

				jsonObject = mServerConnections.getJSONFromUrl(false,
						fetch_latlong + enterzipCode, null).getJSONObject(
						CommonConstants.TAG_LOGIN_RESULTSET);

				String responseCode = jsonObject.getString("responseCode");
				if (null != responseCode && "10000".equals(responseCode)) {
					mapLat = Double.valueOf(jsonObject.getString("Latitude"));
					mapLng = Double.valueOf(jsonObject.getString("Longitude"));

					isZipCodeLocated = true;
				} else {

					isZipCodeLocated = false;
				}

			} catch (Exception e1) {
				e1.printStackTrace();
			}

			return null;

		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.right_button:
			if (GlobalConstants.isFromNewsTemplate){
				Intent intent = null;
				switch (GlobalConstants.className) {
					case Constants.COMBINATION:
						intent = new Intent(LocateOnMapActivity.this, CombinationTemplate.class);
						break;
					case Constants.SCROLLING:
						intent = new Intent(LocateOnMapActivity.this, ScrollingPageActivity.class);
						break;
					case Constants.NEWS_TILE:
						intent = new Intent(LocateOnMapActivity.this, TwoTileNewsTemplateActivity.class);
						break;
				}
				if (intent != null) {
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}
			}else {
				rightBtn();
			}
			break;

		default:
			break;

		}

	}

	private void rightBtn() {
		if (mMarker.isVisible()) {

			Intent getRetailer = new Intent(LocateOnMapActivity.this,
					ThisLocationGetRetailers.class);
			getRetailer.putExtra("mapLat", mapLat);
			getRetailer.putExtra("mapLng", mapLng);
			getRetailer.putExtra(CommonConstants.ZIP_CODE, enterzipCode);
			getRetailer.putExtra("fromLocateMap", "true");
			startActivityForResult(getRetailer, 11);
		}

	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		if (marker.equals(mMarker)) {
			mMarker.remove();
			mMarker.setVisible(false);
		}
		return false;
	}

	@Override
	public void onMapClick(LatLng latLng) {
		mapLat = latLng.latitude;
		mapLng = latLng.longitude;
		if (mMarker != null) {

			mMarker.remove();
		}
		// create marker
		markerOptions = new MarkerOptions().position(latLng)
				.title("Locate Map");
		mMarker = googleMap.addMarker(markerOptions);
		mMarker.setVisible(true);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 11) {
			setResult(10);

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
