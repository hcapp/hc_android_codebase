package com.scansee.android;

import java.util.HashMap;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;

public class RetailerGiveAwayActivity extends Activity implements
		OnClickListener {
	WebView webView;
	String urL;
	ProgressBar webProgress;
	Button button;
	String userId;
	private HashMap<String, String> giveAwayData = new HashMap<>();
	String qrRetCustPageID;

	@SuppressWarnings("deprecation")
	@SuppressLint({ "NewApi", "SetJavaScriptEnabled" })
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		urL = getIntent().getExtras().getString(CommonConstants.URL);
		setContentView(R.layout.giveaway);

		webView = (WebView) findViewById(R.id.scansee_webview);
		button = (Button) findViewById(R.id.enternow);
		button.setOnClickListener(this);
		SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
		userId = settings.getString(CommonConstants.USER_ID, "0");
		String[] qrid = urL.split("&");
		if (qrid.length >= 2) {
			String[] key = qrid[1].split("=");
			if (key.length >= 2) {
				qrRetCustPageID = key[1];
			}
		}
		webView.loadUrl(urL);
		webProgress = (ProgressBar) findViewById(R.id.progressBar_browser);

		webView.getSettings().setJavaScriptEnabled(true);

		webView.getSettings().setPluginState(WebSettings.PluginState.ON);

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			webView.getSettings().setDisplayZoomControls(false);
		}
		webView.getSettings().setBuiltInZoomControls(true);

		webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
		webView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				webProgress.setProgress(newProgress);
				if (newProgress == 100) {
					webProgress.setVisibility(View.GONE);
				} else {
					webProgress.setVisibility(View.VISIBLE);

				}
			}

		});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.enternow:
			new GiveAway().execute();
			break;

		default:
			break;
		}

	}

	private class GiveAway extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {

				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.ADDTOSHOPPINGLISTRESULTSET);
					giveAwayData = new HashMap<>();

					giveAwayData
							.put(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE,
									jsonObject
											.getString(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE));
					giveAwayData.put(CommonConstants.ADDTOSL,
							jsonObject.getString(CommonConstants.ADDTOSL));

					result = "true";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if ("true".equals(result)) {
				Builder findAlert = new AlertDialog.Builder(
						RetailerGiveAwayActivity.this);
				findAlert
						.setMessage(giveAwayData.get(CommonConstants.ADDTOSL))
						.setCancelable(true)
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int whichButton) {
										dialog.cancel();
									}
								});
				findAlert.create().show();
			} else {
				Builder findAlert = new AlertDialog.Builder(
						RetailerGiveAwayActivity.this);
				findAlert
						.setMessage(getString(R.string.find_alert_nozipcode))
						.setCancelable(true)
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int whichButton) {
										dialog.cancel();
									}
								});
				findAlert.create().show();

			}
		}

	}
}
