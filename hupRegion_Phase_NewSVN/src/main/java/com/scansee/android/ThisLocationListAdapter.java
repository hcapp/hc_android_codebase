package com.scansee.android;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class ThisLocationListAdapter extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<RetailerObj> thislocationList;
    private static LayoutInflater inflater = null;
    protected final CustomImageLoader customImageLoader;

    public ThisLocationListAdapter(Activity activity,
                                   ArrayList<RetailerObj> thislocationList) {
        this.activity = activity;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.thislocationList = thislocationList;
        customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
    }

    @Override
    public int getCount() {
        return thislocationList.size();
    }

    @Override
    public Object getItem(int id) {
        return thislocationList.get(id);
    }

    @Override
    public long getItemId(int id) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder = new ViewHolder();
        if (position == thislocationList.size() - 1) {
            if (!((ThisLocationGetRetailers) activity).isAlreadyLoading) {
                if (((ThisLocationGetRetailers) activity).nextPage) {
                    ((ThisLocationGetRetailers) activity).viewMore();
                }
            }
        }
        if (convertView == null) {

            view = inflater.inflate(R.layout.listitem_austin_exp_get_retailers, parent, false);
            viewHolder.retailerImage = (ImageView) view
                    .findViewById(R.id.retailer_image);
            viewHolder.retailerName = (TextView) view
                    .findViewById(R.id.retailer_name);
            viewHolder.distance = (TextView) view
                    .findViewById(R.id.distance);
            viewHolder.retailerSpecials = (ImageView) view
                    .findViewById(R.id.retailer_specials);
            viewHolder.retailerMileAddress = (TextView) view
                    .findViewById(R.id.retailer_address);
            viewHolder.retailerMileAddress2 = (TextView) view
                    .findViewById(R.id.retailer_address2);
            viewHolder.retailerStatus = (TextView) view
                    .findViewById(R.id.retailer_status);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        String retailerName = thislocationList.get(position)
                .getRetailerName();
        String retailerAddress1 = thislocationList.get(position).getRetailAddress1(
        );
        String retailerAddress2 = thislocationList.get(position).getRetailAddress2(
        );
        String saleFlag = thislocationList.get(position).getSaleFlg(
        );
        String logoImagePath = thislocationList.get(position).getLogoImagePath(
        );
        String bannerImagePath = thislocationList.get(position)
                .getBannerAdImagePath();
        String city = thislocationList.get(position)
                .getCity();
        String state = thislocationList.get(position)
                .getState();
        String postalCode = thislocationList.get(position)
                .getPostalCode();
        String locationStatus = thislocationList.get(position)
                .getLocationOpen();


        String distance = thislocationList.get(
                position).getDistance();
        if (retailerName != null && !retailerName.equalsIgnoreCase("")) {
            viewHolder.retailerName.setText(retailerName);
        }
        if (distance != null && !distance.equalsIgnoreCase("")) {
            viewHolder.distance.setText(distance);
        }
        if (locationStatus != null && !locationStatus.isEmpty()&& !locationStatus.equalsIgnoreCase("N/A")) {
            viewHolder.retailerStatus.setText(locationStatus);
        }
        String retailerAddress = "";
        if (retailerAddress1 != null && !retailerAddress1.equals("N/A")) {
            retailerAddress = retailerAddress1;
        }
        if (retailerAddress2 != null && !retailerAddress2.equals("N/A") && !retailerAddress2.equalsIgnoreCase("")) {
            retailerAddress = retailerAddress + ", " + retailerAddress2;
        }
        viewHolder.retailerMileAddress.setText(retailerAddress);


        String address = "";
        if (!"N/A".equals(city) && city != null) {
            address = city;
        }
        if (!"N/A".equals(state) && state != null) {
            if (address.isEmpty())
                address = state;
            else
                address = address + ", " + state;
        }
        if (!"N/A".equals(postalCode) && postalCode != null) {
            if (address.isEmpty())
                address = postalCode;
            else
                address = address + ", " + postalCode;
        }
        viewHolder.retailerMileAddress2.setText(address);


        if ("1".equalsIgnoreCase(saleFlag)) {
            viewHolder.retailerSpecials.setVisibility(View.VISIBLE);
        } else {
            viewHolder.retailerSpecials.setVisibility(View.GONE);
        }

        if (!"N/A".equalsIgnoreCase(logoImagePath)) {
            viewHolder.retailerImage.setTag(logoImagePath);
            customImageLoader.displayImage(logoImagePath, activity,
                    viewHolder.retailerImage);
            customImageLoader.displayImage(bannerImagePath,
                    activity, new ImageView(activity));
        }


        return view;
    }

    public static class ViewHolder {
        ImageView retailerImage;
        TextView retailerName;
        ImageView retailerSpecials;
        TextView retailerMileAddress;
        TextView retailerMileAddress2;
        TextView retailerStatus;
        TextView distance;
    }
}
