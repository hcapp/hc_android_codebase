package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;

public class FindSearchProductListListAdapter extends BaseAdapter
{
	private Activity activity;
	private ArrayList<HashMap<String, String>> findsearchList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	public FindSearchProductListListAdapter(Activity findSearchProductActivity,
			ArrayList<HashMap<String, String>> findsearchList)
	{
		this.activity = findSearchProductActivity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.findsearchList = findsearchList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount()
	{

		return findsearchList.size();
	}

	@Override
	public Object getItem(int id)
	{
		return findsearchList.get(id);
	}

	@Override
	public long getItemId(int id)
	{

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		ViewHolder holder;
		try {
			if (position == findsearchList.size() - 1) {
				if (!((FindActivity) activity).isAlreadyLoading) {
					if (((FindActivity) activity).nextPage) {
						((FindActivity) activity).viewMore();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (convertView == null) {
			view = inflater.inflate(R.layout.listitem_find_product, parent, false);
			holder = new ViewHolder();
			holder.productsearchImage = (ImageView) view
					.findViewById(R.id.findsearch_info_image);
			holder.productsearchName = (TextView) view
					.findViewById(R.id.find_productsearch_name);
			holder.productsearchDescription = (TextView) view
					.findViewById(R.id.find_productsearch_shortdesc);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.productsearchName.setText(findsearchList.get(position)
				.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));

		holder.productsearchDescription.setText(findsearchList.get(
				position).get(
				CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION));

		if (null != findsearchList.get(position).get(
				CommonConstants.FINDPRODUCTSEARCHIMAGEPATH)
				&& !"N/A".equalsIgnoreCase(findsearchList.get(position)
				.get(CommonConstants.FINDPRODUCTSEARCHIMAGEPATH))) {
			holder.productsearchImage.setTag(findsearchList.get(
					position).get(
					CommonConstants.FINDPRODUCTSEARCHIMAGEPATH));
			customImageLoader.displayImage(
					findsearchList.get(position).get(
							CommonConstants.FINDPRODUCTSEARCHIMAGEPATH),
					activity, holder.productsearchImage);
		} else {
			holder.productsearchImage.setImageDrawable(activity.getDrawable(R.drawable
					.ic_empty_icon));
		}

		return view;
	}

	public static class ViewHolder
	{
		protected TextView productsearchDescription;
		protected TextView productsearchName;
		protected ImageView productsearchImage;

	}
}
