package com.scansee.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hubcity.android.businessObjects.MainMenuBO;
import com.scansee.hubregion.R;

import java.util.ArrayList;

/**
 * Created by supriya.m on 9/1/2016.
 */
public class SideMenuAdapter extends BaseAdapter {
    Context context;
    ArrayList<MainMenuBO> arrMenuItem;

    public SideMenuAdapter(Context context, ArrayList<MainMenuBO> arrMenuItem) {
        this.context = context;
        this.arrMenuItem = arrMenuItem;
    }

    @Override
    public int getCount() {
        return arrMenuItem.size();
    }

    @Override
    public Object getItem(int position) {
        return arrMenuItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
                    .LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(
                    R.layout.side_menu_row, viewGroup, false);
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.item_name);
            view.setTag(holder);
        }
        {
            holder = (ViewHolder) view.getTag();
        }
        holder.text.setText(arrMenuItem.get(i).getmItemName());

        System.out.println(arrMenuItem.get(i).getmItemName());
        return view;
    }

    static class ViewHolder {
        TextView text;

    }
}
