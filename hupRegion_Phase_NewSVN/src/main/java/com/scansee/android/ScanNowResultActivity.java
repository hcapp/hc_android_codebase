package com.scansee.android;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.google.zxing.client.android.ScanNowActivity;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.CustomNavigation;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("Convert2Diamond")
public class ScanNowResultActivity extends CustomTitleBar implements
        OnClickListener, TextWatcher {

    SharedPreferences settings = null;
    HashMap<String, String> historyData;
    String pname = null;
    private ScanNowSmartSearch scanNowSmartSearch = new ScanNowSmartSearch();
    private ListView scanNowListView;
    boolean fromshoppinglist = false;
    boolean fromfavorites = false;
    private ArrayList<HashMap<String, String>> smartList, searchList,
            clickList;
    String smartsearchKey, barcode, type, format, parCatId = "0",
            lastVistedProductNo = "0", searchKey;
    private EditText searchEdittext;
    LinearLayout progressLayout;
    private ProgressDialog mDialog;
    String addTo, productId;
    LocationManager locationManager;
    String latitude;
    String longitude;
    boolean isAlreadyLoading;
    private CustomNavigation customNaviagation;

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        title.setSingleLine(false);
        title.setMaxLines(2);
        title.setText(R.string.title_scan_now);

        leftTitleImage.setVisibility(View.GONE);

        rightImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (GlobalConstants.isFromNewsTemplate){
                    Intent intent = null;
                    switch (GlobalConstants.className) {
                        case Constants.COMBINATION:
                            intent = new Intent(ScanNowResultActivity.this, CombinationTemplate.class);
                            break;
                        case Constants.SCROLLING:
                            intent = new Intent(ScanNowResultActivity.this, ScrollingPageActivity.class);
                            break;
                        case Constants.NEWS_TILE:
                            intent = new Intent(ScanNowResultActivity.this, TwoTileNewsTemplateActivity.class);
                            break;
                    }
                    if (intent != null) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }else {
                    closeKeypad(searchEdittext);
                    startMenuActivity();
                }

            }
        });

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scannow_result);
        try {
            CommonConstants.hamburgerIsFirst = true;
            if (getIntent().hasExtra("favorites")) {
                fromfavorites = getIntent().getExtras().getBoolean("favorites");
            }
            if (getIntent().hasExtra("shoppinglist")) {
                fromshoppinglist = getIntent().getExtras().getBoolean(
                        "shoppinglist");
            }
            barcode = getIntent().getExtras().getString("barcode");
            type = getIntent().getExtras().getString("type");
            format = getIntent().getExtras().getString("format");
            (findViewById(R.id.bottom_bar)).setBackgroundColor(Color.parseColor(Constants.getNavigationBarValues("titleBkGrdColor")));
            findViewById(R.id.scan_now_cancelbtn).setOnClickListener(this);
            findViewById(R.id.scan_now_historybtn).setOnClickListener(this);
            findViewById(R.id.scan_now_scanbtn).setOnClickListener(this);
            ((Button) findViewById(R.id.scan_now_historybtn)).setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
            ((Button) findViewById(R.id.scan_now_scanbtn)).setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
            scanNowListView = (ListView) findViewById(R.id.scan_now_search_result);
            progressLayout = (LinearLayout) findViewById(R.id.scan_now_progressbar);
            searchEdittext = (EditText) findViewById(R.id.scan_now_edit_text);
            searchEdittext.addTextChangedListener(this);
            hideKeyboardItem(searchEdittext);
            searchEdittext.setOnEditorActionListener(new OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView textView, int actionId,
                                              KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        lastVistedProductNo = "0";
                        searchList = new ArrayList<>();
                        searchKey = textView.getText().toString().trim();
                        if (AsyncTask.Status.RUNNING == scanNowSmartSearch
                                .getStatus()) {
                            scanNowSmartSearch.cancel(true);
                        }

                        new SearchbyName().execute();
                        hideKeyboardItem(searchEdittext);
                        getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    }

                    return true;
                }

            });
            scanNowListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    try {
                        hideKeyboardItem(searchEdittext);
                        String itemName = clickList.get(position).get("itemName");
                        if ("viewMore".equalsIgnoreCase(itemName)) {

                        } else if ("nameSearch".equalsIgnoreCase(itemName)) {

                            int count = Integer
                                    .valueOf(searchList
                                            .get(position)
                                            .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTCOUNT));

                            if (count > 1) {

                                Intent intent = new Intent(
                                        ScanNowResultActivity.this,
                                        FindGetProductListActivity.class);
                                intent.putExtra(
                                        CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
                                        searchList
                                                .get(position)
                                                .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
                                startActivityForResult(intent, Constants.STARTVALUE);

                            } else {

                                Intent navIntent = new Intent(
                                        ScanNowResultActivity.this,
                                        RetailerCurrentsalesActivity.class);
                                navIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_PRODUCTID,
                                                searchList
                                                        .get(position)
                                                        .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
                                navIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
                                                searchList
                                                        .get(position)
                                                        .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID));
                                navIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
                                                searchList
                                                        .get(position)
                                                        .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));

                                navIntent.putExtra("ScanNow", true);
                                startActivityForResult(navIntent,
                                        Constants.STARTVALUE);
                            }

                        } else if (fromshoppinglist) {
                            productId = searchList.get(position).get(
                                    CommonConstants.FINDPRODUCTSEARCHPRODUCTID);
                            addTo = "List";
                            new AddTo().execute();
                        } else if (fromfavorites) {
                            new AddToFavorites().execute();
                        } else if ("smartSearch".equalsIgnoreCase(itemName)) {
                            Intent intent = new Intent(ScanNowResultActivity.this,
                                    FindGetProductListActivity.class);
                            intent.putExtra(
                                    CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
                                    searchEdittext.getText().toString().trim());
                            intent.putExtra("selectedCat", smartList.get(position)
                                    .get(CommonConstants.FINDSMARTSEARCHCATEGORYID));
                            startActivityForResult(intent, Constants.STARTVALUE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    hideKeyboardItem(searchEdittext);
                    finish();
                }
            });

            if (barcode != null && format != null && barcode.length() > 0
                    && format.length() > 0) {
                format = format.replaceAll("_", "-");
                new BarcodeResult().execute();
            }

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            String provider = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (provider.contains("gps")) {

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                        CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                        new ScanSeeLocListener());
                Location locNew = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (locNew != null) {

                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());

                } else {
                    // N/W Tower Info Start
                    locNew = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (locNew != null) {
                        latitude = String.valueOf(locNew.getLatitude());
                        longitude = String.valueOf(locNew.getLongitude());
                    }
                }
            }
            //user for hamburger in modules
            drawerIcon.setVisibility(View.VISIBLE);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void viewMore() {
        isAlreadyLoading = true;
        searchList.remove(searchList.size() - 1);
        new SearchbyName().execute();
    }

    private void hideKeyboardItem(EditText searchEdittext) {
        InputMethodManager imm = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchEdittext.getWindowToken(), 0);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @SuppressWarnings("Convert2Diamond")
    private class AddToFavorites extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(ScanNowResultActivity.this,
                    Constants.DIALOG_MESSAGE, "");
            mDialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... params) {
            String result = "false";
            try {

                if (jsonObject != null) {
                    jsonObject = jsonObject
                            .getJSONObject(CommonConstants.ADDTOSLRESULTSET);

                    historyData = new HashMap<>();
                    historyData
                            .put(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE,
                                    String.valueOf(jsonObject
                                            .getInt(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE)));
                    historyData.put(CommonConstants.ADDTOSL,
                            jsonObject.getString(CommonConstants.ADDTOSL));

                    result = "true";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            mDialog.dismiss();
            if ("true".equalsIgnoreCase(result)) {

                Builder notificationAlert = new Builder(
                        ScanNowResultActivity.this);
                notificationAlert.setMessage(
                        historyData.get(CommonConstants.ADDTOSHOPPINGLIST))
                        .setPositiveButton(getString(R.string.specials_ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                        finish();
                                    }
                                });

                notificationAlert.create().show();
            } else {
                Builder notificationAlert = new Builder(
                        ScanNowResultActivity.this);
                notificationAlert.setMessage(
                        getString(R.string.network_failure)).setPositiveButton(
                        getString(R.string.specials_ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
                notificationAlert.create().show();
            }
        }
    }

    @SuppressWarnings("Convert2Diamond")
    private class AddTo extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(ScanNowResultActivity.this,
                    Constants.DIALOG_MESSAGE, "");
            mDialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... params) {
            String result = "false";
            try {

                if (jsonObject != null) {
                    jsonObject = jsonObject
                            .getJSONObject(CommonConstants.ADDTOSHOPPINGLISTRESULTSET);

                    historyData = new HashMap<>();
                    historyData
                            .put(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE,
                                    String.valueOf(jsonObject
                                            .getInt(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE)));

                    historyData
                            .put(CommonConstants.ADDTOSHOPPINGLIST,
                                    jsonObject
                                            .getString(CommonConstants.ADDTOSHOPPINGLIST));
                    historyData
                            .put(CommonConstants.ADDTOSHOPPINGLISTUSERPRODUCTIDS,
                                    jsonObject
                                            .getString(CommonConstants.ADDTOSHOPPINGLISTUSERPRODUCTIDS));

                    result = "true";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            mDialog.dismiss();
            if ("10000".equalsIgnoreCase(historyData
                    .get(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE))) {

                Builder notificationAlert = new Builder(
                        ScanNowResultActivity.this);
                notificationAlert.setMessage(
                        historyData.get(CommonConstants.ADDTOSHOPPINGLIST))
                        .setPositiveButton(getString(R.string.specials_ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                        finish();
                                    }
                                });

                notificationAlert.create().show();

            } else if ("10010".equalsIgnoreCase(historyData
                    .get(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE))) {

                Builder notificationAlert = new Builder(
                        ScanNowResultActivity.this);
                notificationAlert.setMessage(
                        historyData.get(CommonConstants.ADDTOSHOPPINGLIST))
                        .setPositiveButton(getString(R.string.specials_ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                        finish();
                                    }
                                });

                notificationAlert.create().show();

            } else {
                Builder notificationAlert = new Builder(
                        ScanNowResultActivity.this);
                notificationAlert.setMessage(
                        getString(R.string.network_failure)).setPositiveButton(
                        getString(R.string.specials_ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });

                notificationAlert.create().show();

            }

        }
    }

    UrlRequestParams objUrlRequestParams = new UrlRequestParams();
    ServerConnections mServerConnections = new ServerConnections();

    @SuppressWarnings("Convert2Diamond")
    private class ScanNowSmartSearch extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            JSONObject jsonObject = null;
            Boolean result = false;
            JSONArray jsonArraySmartsearchData = new JSONArray();
            smartList = new ArrayList<>();
            try {
                String urlParameters = objUrlRequestParams
                        .getFindSmartSearch(smartsearchKey);
                String get_findsmartsearchdata = Properties.url_local_server
                        + Properties.hubciti_version
                        + "scannow/getsmartsearchprods";
                jsonObject = mServerConnections.getUrlPostResponse(
                        get_findsmartsearchdata, urlParameters, true);

                if (jsonObject != null) {
                    try {
                        JSONObject smartSearchObj = jsonObject.getJSONObject(
                                CommonConstants.FINDSMARTSEARCHRESULTSET)
                                .getJSONObject(
                                        CommonConstants.FINDSMARTSEARCHMENU);
                        jsonArraySmartsearchData.put(smartSearchObj);
                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonArraySmartsearchData = jsonObject.getJSONObject(
                                CommonConstants.FINDSMARTSEARCHRESULTSET)
                                .getJSONArray(
                                        CommonConstants.FINDSMARTSEARCHMENU);
                    }

                    for (int arrayCount = 0; arrayCount < jsonArraySmartsearchData
                            .length(); arrayCount++) {
                        HashMap<String, String> scanseeData = new HashMap<>();
                        scanseeData.put("itemName", "smartSearch");
                        scanseeData
                                .put(CommonConstants.FINDSMARTSEARCHCATEGORYNAME,
                                        jsonArraySmartsearchData
                                                .getJSONObject(arrayCount)
                                                .getString(
                                                        CommonConstants.FINDSMARTSEARCHCATEGORYNAME));
                        scanseeData
                                .put(CommonConstants.FINDSMARTSEARCHCATEGORIES,
                                        jsonArraySmartsearchData
                                                .getJSONObject(arrayCount)
                                                .getString(
                                                        CommonConstants.FINDSMARTSEARCHCATEGORIES));
                        scanseeData
                                .put(CommonConstants.FINDSMARTSEARCHCATEGORYID,
                                        jsonArraySmartsearchData
                                                .getJSONObject(arrayCount)
                                                .getString(
                                                        CommonConstants.FINDSMARTSEARCHCATEGORYID));
                        smartList.add(scanseeData);
                    }
                    result = true;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                clickList = smartList;
                scanNowListView.setAdapter(null);
                scanNowListView.setAdapter(new FindSmartSearchListAdapter(
                        ScanNowResultActivity.this, smartList));
            }

        }
    }

    boolean nextPage;
    boolean isShowLoading;

    @SuppressWarnings("Convert2Diamond")
    private class SearchbyName extends AsyncTask<Void, Void, Boolean> {

        JSONObject jsonObject = null;
        JSONObject responseMenuObject = null;
        JSONArray jsonArrayProductDetail = null;
        HashMap<String, String> productData = null;

        // Boolean nextPage = false;

        @Override
        protected void onPreExecute() {
            if (!isShowLoading) {
                isShowLoading = true;
                mDialog = ProgressDialog.show(ScanNowResultActivity.this, "",
                        Constants.DIALOG_MESSAGE, true);
                // nextPage = false;
                mDialog.setCancelable(false);
            }
        }

        @SuppressWarnings("static-access")
        @Override
        protected Boolean doInBackground(Void... voids) {

            Boolean result = false;

            try {
                jsonObject = null;
                SharedPreferences preferences = HubCityContext
                        .getHubCityContext()
                        .getSharedPreferences(Constants.PREFERENCE_HUB_CITY,
                                HubCityContext.getHubCityContext().MODE_PRIVATE);
                String mainMenuId = preferences.getString(
                        Constants.PREFERENCE_KEY_MAIN_MENU_ID, "45");

                String urlParameters = objUrlRequestParams
                        .getFindProductSearchList(searchKey, parCatId,
                                lastVistedProductNo, mainMenuId);
                String get_product_items_list = Properties.url_local_server
                        + Properties.hubciti_version
                        + "scannow/getsmartsearchcount";
                jsonObject = mServerConnections.getUrlPostResponse(
                        get_product_items_list, urlParameters, true);

                if (jsonObject != null) {
                    if ("1".equalsIgnoreCase(jsonObject.getJSONObject(
                            CommonConstants.FINDPRODUCTSEARCHRESULTSET)
                            .getString("nextPage"))) {
                        nextPage = true;
                    } else {
                        nextPage = false;
                    }

                    try {
                        JSONObject productDetail = jsonObject.getJSONObject(
                                CommonConstants.FINDPRODUCTSEARCHRESULTSET)
                                .getJSONObject(
                                        CommonConstants.FINDPRODUCTSEARCHMENU);

                        jsonArrayProductDetail = new JSONArray();
                        jsonArrayProductDetail.put(productDetail);
                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonArrayProductDetail = jsonObject.getJSONObject(
                                CommonConstants.FINDPRODUCTSEARCHRESULTSET)
                                .getJSONArray(
                                        CommonConstants.FINDPRODUCTSEARCHMENU);
                    }

                    for (int arrayCount = 0; arrayCount < jsonArrayProductDetail
                            .length(); arrayCount++) {
                        productData = new HashMap<>();
                        responseMenuObject = jsonArrayProductDetail
                                .getJSONObject(arrayCount);
                        productData.put("itemName", "nameSearch");
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTPRODUCTCOUNT,
                                        jsonObject
                                                .getJSONObject(
                                                        CommonConstants.FINDPRODUCTSEARCHRESULTSET)
                                                .getString(
                                                        CommonConstants.FINDPRODUCTSEARCHPRODUCTPRODUCTCOUNT));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTID,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHIMAGEPATH,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHIMAGEPATH));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTCOUNT,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTCOUNT));
                        lastVistedProductNo = responseMenuObject
                                .getString(CommonConstants.TAG_ROW_NUMBER);
                        searchList.add(productData);
                    }
                    if (nextPage) {
                        productData = new HashMap<>();
                        productData.put("itemName", "viewMore");

                        searchList.add(productData);

                    }
                    result = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mDialog.dismiss();
            isAlreadyLoading = false;
            if (result) {
                clickList = searchList;
                if ("0".equalsIgnoreCase(lastVistedProductNo)) {
                    scanNowListView.setAdapter(null);
                    scanNowListView
                            .setAdapter(new FindSearchProductListAdapter(
                                    ScanNowResultActivity.this, searchList));
                } else {
                    Parcelable state = scanNowListView.onSaveInstanceState();
                    scanNowListView.setAdapter(null);
                    scanNowListView
                            .setAdapter(new FindSearchProductListAdapter(
                                    ScanNowResultActivity.this, searchList));
                    scanNowListView.onRestoreInstanceState(state);
                }

            } else if ("0".equalsIgnoreCase(lastVistedProductNo)) {
                searchList = new ArrayList<>();
                clickList = searchList;
                scanNowListView.setAdapter(null);
                scanNowListView.setAdapter(new FindSearchProductListAdapter(
                        ScanNowResultActivity.this, searchList));

                Builder findAlert = new Builder(
                        ScanNowResultActivity.this);
                findAlert
                        .setMessage("No Products Found")
                        .setCancelable(true)
                        .setPositiveButton(getString(R.string.specials_ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        dialog.cancel();
                                    }
                                });
                findAlert.create().show();
            }

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressWarnings("Convert2Diamond")
    private class BarcodeResult extends AsyncTask<Void, Void, Boolean> {

        JSONObject jsonObject = null;
        JSONObject responseMenuObject = null;
        JSONArray jsonArrayProductDetail = null;
        HashMap<String, String> productBarcode = null;
        private ProgressDialog mDialog;
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(ScanNowResultActivity.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            Boolean result = false;

            try {
                // Comment code is reused once user scan a barcode the response
                // is converted to XML
                String fetch_barcode_prod = Properties.url_local_server
                        + Properties.hubciti_version
                        + "scannow/getprodforbarcode";
                String urlParameters = mUrlRequestParams.fetchBarcodeProd(
                        barcode, format, latitude, longitude);
                jsonObject = mServerConnections.getUrlPostResponse(
                        fetch_barcode_prod, urlParameters, true);

                if (jsonObject != null) {
                    try {
                        JSONObject productDetail = jsonObject
                                .getJSONObject(CommonConstants.FINDPRODUCTSEARCHMENU);

                        jsonArrayProductDetail = new JSONArray();
                        jsonArrayProductDetail.put(productDetail);
                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonArrayProductDetail = jsonObject
                                .getJSONArray(CommonConstants.FINDPRODUCTSEARCHMENU);
                    }

                    for (int arrayCount = 0; arrayCount < jsonArrayProductDetail
                            .length(); arrayCount++) {
                        productBarcode = new HashMap<>();
                        responseMenuObject = jsonArrayProductDetail
                                .getJSONObject(arrayCount);
                        productBarcode.put("itemName", "nameSearch");
                        productBarcode
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTID,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
                        productBarcode
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID));
                        productBarcode
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
                                        responseMenuObject
                                                .getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
                        productBarcode
                                .put(CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER,
                                        responseMenuObject
                                                .getString(CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER));

                        productBarcode
                                .put(CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER,
                                        responseMenuObject
                                                .getString(CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER));
                    }
                    result = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mDialog.dismiss();
            if (result) {
                if (fromfavorites) {
                    productId = productBarcode
                            .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID);
                    pname = productBarcode
                            .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME);
                    new AddToFavorites().execute();
                } else if (fromshoppinglist) {
                    productId = productBarcode
                            .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID);

                    addTo = "List";
                    new AddTo().execute();
                } else {

                    Intent navIntent = new Intent(ScanNowResultActivity.this,
                            RetailerCurrentsalesActivity.class);
                    navIntent
                            .putExtra(
                                    CommonConstants.TAG_CURRENTSALES_PRODUCTID,
                                    productBarcode
                                            .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
                    navIntent
                            .putExtra(
                                    CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
                                    productBarcode
                                            .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID));
                    navIntent
                            .putExtra(
                                    CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
                                    productBarcode
                                            .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
                    navIntent
                            .putExtra(
                                    CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER,
                                    productBarcode
                                            .get(CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER));

                    navIntent.putExtra("ScanNow", true);
                    startActivityForResult(navIntent, Constants.STARTVALUE);
                }
            } else {
                Builder findAlert = new Builder(
                        ScanNowResultActivity.this);
                findAlert
                        .setMessage(
                                "We could not recognize this scan, please search by typing in you request")
                        .setCancelable(true)
                        .setPositiveButton(getString(R.string.button_cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        dialog.cancel();
                                    }
                                });
                findAlert.create().show();
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        String value = s.toString();

        if (value.length() < 4) {
            return;
        }

        if (AsyncTask.Status.RUNNING == scanNowSmartSearch.getStatus()) {
            scanNowSmartSearch.cancel(true);
        }
        scanNowSmartSearch = new ScanNowSmartSearch();
        smartsearchKey = value.trim();
        scanNowSmartSearch.execute();

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scan_now_cancelbtn:
                scanNowCancelbtn();
                break;
            case R.id.scan_now_historybtn:
                Intent intent = new Intent(ScanNowResultActivity.this,
                        ScanNowHistoryActivity.class);
                startActivityForResult(intent, Constants.STARTVALUE);
                break;
            case R.id.scan_now_scanbtn:
                Intent intentScanNow = new Intent(this, ScanNowActivity.class);
                startActivity(intentScanNow);
                this.finish();
                break;
            default:
                break;
        }

    }

    private void scanNowCancelbtn() {
        if (searchEdittext.isFocused()) {
            searchList = null;
            scanNowListView.setAdapter(null);
            searchEdittext.setText(null);
            hideKeyboardItem(searchEdittext);
        }

    }
}
