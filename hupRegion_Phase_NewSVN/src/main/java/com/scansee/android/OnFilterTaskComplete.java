package com.scansee.android;

import java.util.ArrayList;

import com.hubcity.android.businessObjects.FilterArdSortScreenBO;

public interface OnFilterTaskComplete {
	public void setDynamicFilterView(
			ArrayList<FilterArdSortScreenBO> arrSortAndFilter);
}
