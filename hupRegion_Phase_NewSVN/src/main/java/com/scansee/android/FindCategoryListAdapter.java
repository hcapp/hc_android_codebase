package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class FindCategoryListAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<HashMap<String, String>> findList;
    private static LayoutInflater inflater = null;
    protected CustomImageLoader customImageLoader;

    String mColor = "";
    String mFontColor = "";

    public FindCategoryListAdapter(Activity activity,
                                   ArrayList<HashMap<String, String>> findList) {
        this.activity = activity;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.findList = findList;
        customImageLoader = new CustomImageLoader(activity.getApplicationContext(), true);
    }

    public FindCategoryListAdapter(Activity activity,
                                   ArrayList<HashMap<String, String>> findList, String mColor,
                                   String mFontColor, String bottomBtnId) {
        this.activity = activity;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.findList = findList;
        this.mColor = mColor;
        this.mFontColor = mFontColor;
        customImageLoader = new CustomImageLoader(activity.getApplicationContext(),
                true);
    }

    @Override
    public int getCount() {
        return findList.size();
    }

    @Override
    public Object getItem(int id) {
        return findList.get(id);
    }

    @Override
    public long getItemId(int id) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup group) {
        View view = convertView;
        ViewHolder viewHolder;
        if (convertView == null) {

            view = inflater.inflate(R.layout.listitem_find_category, null);
            viewHolder = new ViewHolder();

            viewHolder.mainLayout = (LinearLayout) view.findViewById(R.id.ll);

            viewHolder.name = (TextView) view
                    .findViewById(R.id.find_category_name);
            viewHolder.imagePath = (ImageView) view
                    .findViewById(R.id.find_category_image);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();

        }

        if (!"".equals(mColor) && !"N/A".equalsIgnoreCase(mColor)) {
            viewHolder.mainLayout.setBackgroundColor(Color.parseColor(mColor));
        }

        viewHolder.name.setText(findList.get(position).get(
                FindActivity.TAG_FINDCATEGORY_NAME));

        if (!"".equals(mFontColor) && !"N/A".equalsIgnoreCase(mFontColor)) {
            viewHolder.name.setTextColor(Color.parseColor(mFontColor));
        }

        if (viewHolder.imagePath != null) {

            if (findList.get(position).get(
                    FindActivity.TAG_FINDCATEGORYIMAGE_PATH) != null) {

                viewHolder.imagePath.setTag(findList.get(position).get(
                        FindActivity.TAG_FINDCATEGORYIMAGE_PATH));
                customImageLoader.displayImage(
                        findList.get(position).get(
                                FindActivity.TAG_FINDCATEGORYIMAGE_PATH),
                        activity, viewHolder.imagePath);
            } else {
                viewHolder.imagePath.setImageBitmap(null);
            }
        }

        return view;
    }

    public static class ViewHolder {
        protected LinearLayout mainLayout;
        protected TextView name;
        protected ImageView imagePath;

    }
}
