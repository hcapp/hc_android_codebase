package com.scansee.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("rawtypes")
public class FindRetailerSearchAdapter extends ArrayAdapter
{
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	private FindRetailerSearchActivity activity;
	ArrayList<Item> items = new ArrayList<>();

	@SuppressWarnings("unchecked")
	public FindRetailerSearchAdapter(FindRetailerSearchActivity activity,
			ArrayList<Item> items, String sortBy,
			ArrayList<HashMap<String, String>> findList)
	{
		super(activity, 0, items);

		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// @Rekha START
		this.items = items;

		// @Rekha END

		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount()
	{

		return items.size();

	}

	@Override
	public Object getItem(int id)
	{

		return items.get(id);

	}

	@Override
	public long getItemId(int id)
	{
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		ViewHolder viewHolder;

		/***
		 * If the list item reaches to its last position then it will be called
		 * for next pagination
		 ***/
		if (position == items.size() - 1) {
			if (!((FindRetailerSearchActivity) activity).isAlreadyLoading) {
				if (((FindRetailerSearchActivity) activity).nextPage) {
					((FindRetailerSearchActivity) activity).LoadMore();
				}
			}
		}

		final Item i = items.get(position);
		if (i != null) {

				viewHolder = new ViewHolder();

				FindRetailerSearchEntryItem ei = (FindRetailerSearchEntryItem) i;

				view = inflater.inflate(R.layout.find_location_main_menu, parent, false);
				viewHolder.scanseeName = (TextView) view
						.findViewById(R.id.find_category_name);
				viewHolder.scanseeImage = (ImageView) view
						.findViewById(R.id.retailer_image);
				viewHolder.scanseeAddress = (TextView) view
						.findViewById(R.id.find_category_address);
				viewHolder.scanseeAddress2 = (TextView) view
						.findViewById(R.id.find_category_address2);
				viewHolder.scanseeDistance = (TextView) view
						.findViewById(R.id.distance);
				viewHolder.retailerStatus = (TextView) view
						.findViewById(R.id.find_retailer_status);
				viewHolder.retailerSpecials = (ImageView) view
						.findViewById(R.id.retailer_specials);

				view.setTag(viewHolder);

				viewHolder.scanseeName.setText(ei.retailerName);
				if (!"N/A ".equalsIgnoreCase(ei.logoImagePath)) {
					viewHolder.scanseeImage.setTag(ei.logoImagePath);
					customImageLoader.displayImage(ei.logoImagePath, activity,
							viewHolder.scanseeImage);
				} else {
					viewHolder.scanseeImage.setImageBitmap(null);
				}

				// Adding full Address
				// Changes by Rekha
				StringBuffer address = new StringBuffer();
				if (ei.retailerAddress2 != null && !ei.retailerAddress2.equals("") && !("N/A")
						.equalsIgnoreCase(ei.retailerAddress2)) {
					address.append(ei.retailerAddress1 + ", " + ei
							.retailerAddress2);
				} else {
					address.append(ei.retailerAddress1);
				}
			if (ei.locationOpen != null && !ei.locationOpen.isEmpty() && !ei.locationOpen.equalsIgnoreCase("N/A")) {
				viewHolder.retailerStatus.setText(ei.locationOpen);
			}
				// viewHolder.scanseeDistance.setText(ei.distance + ",");

				if (null != address) {
					viewHolder.scanseeAddress.setText(address);
				}
				viewHolder.scanseeDistance.setText(ei.distance);
				viewHolder.scanseeAddress2.setText(ei.city + ", " + ei.state + ", " + ei
						.postalCode);

				if (ei.saleFlag.equals("true")) {
					viewHolder.retailerSpecials.setVisibility(View.VISIBLE);
				} else {
					viewHolder.retailerSpecials.setVisibility(View.INVISIBLE);
				}


		}

		return view;
	}

	public static class ViewHolder
	{
		protected ImageView scanseeImage;
		protected TextView scanseeAddress;
		protected TextView scanseeAddress2;
		protected TextView scanseeName;
		protected TextView retailerStatus;
		protected TextView scanseeDistance;

		protected ImageView retailerSpecials;

	}
}
