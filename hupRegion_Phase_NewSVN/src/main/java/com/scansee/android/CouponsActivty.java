package com.scansee.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.text.InputFilter;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.com.hubcity.model.CouponMapLocs;
import com.com.hubcity.model.MapModel;
import com.com.hubcity.model.MapObj;
import com.com.hubcity.model.RetailerDetails;
import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CouponsActivty extends CustomTitleBar {


    private SearchView svSearch;
    private Activity mActivity;

    private FrameLayout flFrameLayout;
    private int screenHeight;
    private TextView tvMaps;
    private CustomNavigation customNaviagation;
    public LinearLayout bottomLayout;
    public BottomButtons bb;
    private String latitude;
    private String longitude;
    private String searchKey;
    private boolean isRefresh;
    private HubCityContext mHubCiti;
    private RestClient mRestClient;
    private String radius;
    private final String className = "Coupons";
    private String catIds;
    private String sortColumn = "distance";
    private String cityIds;
    private CouponsFragment couponFrag;
    private boolean isFirst = true;
    public static String postalCode = null;
    public boolean isClaimed;
    boolean flag = false;
    public int bottomButtonHeight;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mycoupons_listview);
        CommonConstants.hamburgerIsFirst = true;

        try {
            mActivity = CouponsActivty.this;
            title.setText(mActivity.getString(R.string.coupons));
            bindView();
            getValue();
            setClickLstener();
            CommonConstants.traverseSearchView(svSearch, mActivity);
            initializeBottomButton();
            //Clearing filter cached values and sending resultset as null to send fresh request every
            // time
            HubCityContext mHubCity = (HubCityContext) getApplicationContext();
            mHubCity.clearArrayAndAllValues(false);
            HubCityContext.isDoneClicked = false;
            HashMap<String, String> gpsValue = CommonConstants.getGpsValue(mActivity);
            latitude = gpsValue.get("latitude");
            longitude = gpsValue.get("longitude");

            isRefresh = true;
            mHubCiti = (HubCityContext) getApplicationContext();
            mHubCiti.setCancelled(false);

            mRestClient = RestClient.getInstance();

            if (((latitude == null && longitude == null) || (latitude.isEmpty() && longitude.isEmpty()))
                    && (Constants.getZipCode() == null ||
                    Constants.getZipCode().isEmpty())) {
                flag = true;
                showZipCodeAlert().show();
            }

            ViewTreeObserver observer = flFrameLayout.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    ViewTreeObserver obs = flFrameLayout.getViewTreeObserver();
                    screenHeight = flFrameLayout.getMeasuredHeight();

                    addFragment();


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFragment() {
        if (!flag) {
            startFragment();
        }

    }


    private void getValue() {
        radius = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).getString
                ("radious", "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);

        }

        if (resultCode == 30001 || resultCode == 2) {
            isRefresh = false;

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRefresh = true;
    }

    private void initializeBottomButton() {
        try {
            //noinspection ConstantConditions
            bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
            bb.setActivityInfo(this, "Coupons");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isClaimed) {
            isFirst = true;
            startFragment();
        }
        if (null != bb) {
            bb.setActivityInfo(this, className);
        }
        if (bb != null) {
            bb.setOptionsValues(getListValues(), null);
        }

        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }
        if (mHubCiti != null && mHubCiti.isCancelled()) {
            return;
        }
        if (!isRefresh) {
            HashMap<String, String> resultSet = mHubCiti.getFilterValues();
            if (resultSet != null && !resultSet.isEmpty()) {
                if (resultSet.containsKey("savedSubCatIds")) {
                    catIds = resultSet.get("savedSubCatIds");
                }
                if (resultSet.containsKey("SortBy")) {
                    sortColumn = resultSet.get("SortBy");
                }
                if (resultSet.containsKey("savedCityIds")) {
                    if (resultSet.get("savedCityIds") != null) {
                        cityIds = resultSet.get("savedCityIds");
                        cityIds = cityIds.replace("All,", "");
                    }
                }
            }
            if (catIds != null && catIds.isEmpty()) {
                catIds = null;
            }
            if (cityIds != null && cityIds.isEmpty()) {
                cityIds = null;
            }
            isFirst = true;
            startFragment();
        }

        isRefresh = false;
    }

    private Dialog showZipCodeAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Please enter Zip code:");

        // Use an EditText view to get user input.
        final EditText input = new EditText(this);
        input.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});

        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                postalCode = input.getText().toString();
                flag = false;
                addFragment();
                return;
            }
        });

        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        flag = false;
                        addFragment();
                        return;
                    }
                });

        return builder.create();
    }

    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();

        if (radius != null && !radius.equals(""))
            values.put("radius", radius);
        else
            values.put("radius", "50");
        values.put("latitude", String.valueOf(latitude));

        values.put("longitude", String.valueOf(longitude));
        values.put("Class", className);
        values.put("srchKey", searchKey);
        return values;
    }

    private void setClickLstener() {
        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override

            public boolean onQueryTextSubmit(String query) {
                isFirst = true;
                searchKey = query;
                if (bb != null) {
                    bb.setOptionsValues(getListValues(), null);
                }
                startFragment();
                svSearch.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0 || newText.length() >= 3) {
                    isFirst = false;
                    searchKey = null;

                    if (newText.length() >= 3) {
                        searchKey = newText;
                    }
                    if (bb != null) {
                        bb.setOptionsValues(getListValues(), null);
                    }
                    startFragment();
                }

                return true;
            }
        });

        tvMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    couponFrag = (CouponsFragment) getSupportFragmentManager().findFragmentById(R.id.coupon_fragment);
                    LinkedList<ArrayList<RetailerDetails>> listItem = couponFrag.listItem;
                    StringBuilder couponIds = new StringBuilder();

                    for (int parentCount = 0; parentCount < listItem.size(); parentCount++) {
                        for (int childCount = 0; childCount < listItem.get(parentCount).size(); childCount++) {
                            couponIds.append(listItem.get(parentCount).get(childCount).getCouponId());
                            couponIds.append(",");
                        }
                    }
                    String couponId = couponIds.toString();
                    if (couponIds.length() != 0) {
                        callMapService(couponId.substring(0, couponId.length() - 1));
                    } else {
                        CommonConstants.displayToast(mActivity, mActivity.getString(R.string.map_norecord));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void callMapService(String couponIds) {
        final ProgressDialog progDialog = new ProgressDialog(mActivity);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage(mActivity.getString(R.string.progress_message));
        progDialog.setCancelable(false);
        progDialog.show();
        MapModel request = UrlRequestParams.sendMapReq(couponIds);
        mRestClient.getMap(request, new Callback<MapObj>() {


            @Override
            public void success(final MapObj mapObj, Response response) {
                try {
                    String responseText = mapObj.getResponseText();
                    if (!responseText.equalsIgnoreCase(mActivity.getString(R.string.success)) && responseText != null) {
                        CommonConstants.displayToast(mActivity, responseText);
                    }
                    hideProgressBar(progDialog);
                    if (mapObj != null && !responseText.equalsIgnoreCase(mActivity.getString(R.string.norecord))) {
                        if (mapObj.getCouponMapLocs() != null) {
                            CommonConstants.startMapScreen(mActivity, getMapDetails(mapObj.getCouponMapLocs()), true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressBar(progDialog);
                if (error.getResponse() != null) {
                    CommonConstants.displayToast(mActivity, error
                            .getResponse().getReason());
                }
            }
        });
    }

    private void hideProgressBar(ProgressDialog progDialog) {
        if (progDialog != null && progDialog.isShowing()) {
            progDialog.dismiss();
        }
    }

    private void startFragment() {
        Fragment fragment = new CouponsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("screenHeight", screenHeight);

        bundle.putString("searchKey", searchKey);
        bundle.putString("latitude", latitude);
        bundle.putString("longitude", longitude);

        //sort and filter param
        bundle.putString("catIds", catIds);
        bundle.putString("sortColumn", sortColumn);
        bundle.putString("cityIds", cityIds);
        bundle.putBoolean("isFirst", isFirst);

        fragment.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.coupon_fragment, fragment).commit();

    }


    private void bindView() {
        svSearch = (SearchView) findViewById(R.id.search);
        flFrameLayout = (FrameLayout) findViewById(R.id.coupon_fragment);
        tvMaps = (TextView) findViewById(R.id.maps);
        //user for hamburger in modules
        drawerIcon.setVisibility(View.VISIBLE);
        leftTitleImage.setVisibility(View.GONE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
        bottomLayout = (LinearLayout) findViewById(R.id.bottom_bar);
        bottomLayout.setVisibility(View.INVISIBLE);
    }


    private ArrayList<RetailerBo> getMapDetails(ArrayList<CouponMapLocs> couponMapLocs) {
        ArrayList<RetailerBo> mapList = new ArrayList<>();
        int size = couponMapLocs.size();
        for (int i = 0; i < size; i++) {
            CouponMapLocs couponList = couponMapLocs.get(i);

            String couponName = couponList.getCouponName();
            String retailAddress = couponList.getLocation();
            String latitude = couponList.getRetLatitude();
            String longitude = couponList.getRetLongitude();
            String couponId = couponList.getCouponId();
            String locationID = couponList.getRetailLocationId();

            RetailerBo retailerObject = new RetailerBo(null, couponId,
                    couponName, locationID, null, retailAddress, null, null,
                    null, null, null, null, latitude,
                    longitude, null);
            mapList.add(retailerObject);

        }

        return mapList;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HubCityContext mHubCity = (HubCityContext) getApplicationContext();
        mHubCity.clearArrayAndAllValues(false);
        HubCityContext.isDoneClicked = false;
    }
}