package com.scansee.android;

import android.os.Parcelable;
import android.view.MotionEvent;
import android.view.View;

public class ListItemGestures implements View.OnTouchListener {
	public enum Action {
		LR, // Left to Right
		RL, // Right to Left
		TB, // Top to bottom
		BT, // Bottom to Top
		NONE, // when no action was detected
		REMOVE
	}

	private ShoppingListActivity activity;
	private static final int MIN_DISTANCE = 150;
	private float downX;
	private float downY;
	private Action mSwipeDetected = Action.NONE;

	public boolean swipeDetected() {
		return mSwipeDetected != Action.NONE;
	}

	public ListItemGestures(ShoppingListActivity activity) {
		this.activity = activity;
	}

	public Action getAction() {
		return mSwipeDetected;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (activity.isDelete) {
			activity.isDelete = false;
			Parcelable state = activity.shoppingListListView
					.onSaveInstanceState();
			activity.shoppingListListAdapter = new ShoppingListListAdapter(
					activity, activity.shoppingList);
			activity.shoppingListListView
					.setAdapter(activity.shoppingListListAdapter);
			activity.shoppingListListView.onRestoreInstanceState(state);
			mSwipeDetected = Action.REMOVE;
			return false;
		}

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			downX = event.getX();
			downY = event.getY();
			mSwipeDetected = Action.NONE;
			return false; // allow other events like Click to be processed
		case MotionEvent.ACTION_UP:
			actionUp(event);
			return false;

		default:
			return false;
		}
	}

	private void actionUp(MotionEvent event) {
		float upX = event.getX();
		float upY = event.getY();

		float deltaX = downX - upX;
		float deltaY = downY - upY;

		// horizontal swipe detection
		if (Math.abs(deltaX) > MIN_DISTANCE) {
			// left or right
			if (deltaX < 0) {
				mSwipeDetected = Action.LR;
				return;
			}
			if (deltaX > 0) {
				mSwipeDetected = Action.RL;
			}
		} else if (Math.abs(deltaY) > MIN_DISTANCE) { // vertical swipe
														// detection
			// top or down
			if (deltaY < 0) {
				mSwipeDetected = Action.TB;
				return;
			}
			if (deltaY > 0) {
				mSwipeDetected = Action.BT;
			}
		}

	}
}
