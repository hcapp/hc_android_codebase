package com.scansee.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by supriya.m on 4/22/2016.
 */
public class GetEventTypeAsyncTask extends AsyncTask<Void, Void, Void> {

    private final boolean showProgress;
    private final String searchKey;
    Context mContext;
    String latitude, longitude, radius, postalCode, mItemId, responseText, lastVisitedNo, cityIds;
    ProgressDialog progressDialog;
    ArrayList<BandSearchListSetGet> arrEvents;
    BottomButtons bb;
    LinearLayout parentLayout;
    boolean hasBottomButtons, enableBottomButtons;
    boolean nextPage;
    private HashMap<String, String> resultSet;

    public GetEventTypeAsyncTask(Context context, String latitude, String longitude,
                                 String radius, String postalCode, String mItemId, BottomButtons bb, LinearLayout
                                         layout, boolean enableBottomButtons, String lastVisitedNo, HashMap<String, String> resultSet, boolean showProgress, String searchKey, String cityIds) {
        this.mContext = context;
        this.latitude = latitude;
        this.longitude = longitude;
        this.mItemId = mItemId;
        this.postalCode = postalCode;
        this.radius = radius;
        this.bb = bb;
        this.parentLayout = layout;
        this.enableBottomButtons = enableBottomButtons;
        this.lastVisitedNo = lastVisitedNo;
        this.resultSet = resultSet;
        this.showProgress = showProgress;
        this.searchKey = searchKey;
//      this.cityIds = cityIds;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgress) {
            progressDialog = new ProgressDialog(mContext, ProgressDialog.THEME_HOLO_DARK);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(Constants.DIALOG_MESSAGE);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        String sortColumn;
        String sortOrder;
        String groupBy;
        sortColumn = "BandName";
        sortOrder = "ASC";
        groupBy = "name";
        String catIds = null;
        if (resultSet != null && !resultSet.isEmpty()) {
            if (resultSet.containsKey("savedSubCatIds")) {
                catIds = resultSet.get("savedSubCatIds");
            }
//            if (resultSet.containsKey("savedCityIds")) {
//                if (resultSet.get("savedCityIds") != null && !resultSet.get("savedCityIds").equals("")) {
//                    cityIds = resultSet.get("savedCityIds");
//                    cityIds = cityIds.replace("All,", "");
//                }
//            }
        }
        String url = Properties.url_local_server
                + Properties.hubciti_version + "band/getbandlist";
        try {

            UrlRequestParams mUrlRequestParams = new UrlRequestParams();
            ServerConnections mServerConnections = new ServerConnections();
            JSONObject jsonUrlParameters = mUrlRequestParams
                    .getEventTypeRequestParam(radius, postalCode, mItemId, sortColumn, sortOrder,
                            groupBy,
                            lastVisitedNo, catIds, cityIds);
            JSONObject jsonObject = mServerConnections.getUrlJsonPostResponse(
                    url, jsonUrlParameters);
            if (jsonObject != null) {

                if (jsonObject.has(
                        "mainMenuId")) {
                    String mainMenuId = jsonObject.getString("mainMenuId");
                    Log.d("mainMenuId : ", mainMenuId);
                    Constants.saveMainMenuId(mainMenuId);
                }

                responseText = jsonObject.optString("responseText");
                if (jsonObject.has("catList")) {
                    JSONArray jsonArray = jsonObject.optJSONArray("catList");
                    arrEvents = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.getJSONObject(i).has("retDetList")) {
                            JSONArray jsonRetArray = jsonArray.getJSONObject(i).getJSONArray
                                    ("retDetList");
                            for (int j = 0; j < jsonRetArray.length(); j++) {
                                BandSearchListSetGet bandSearchListSetGet = new
                                        BandSearchListSetGet();
                                bandSearchListSetGet.setSearchedItemId(jsonRetArray
                                        .getJSONObject(j)
                                        .optString("bandID"));
                                bandSearchListSetGet.setSearchedItemName(jsonRetArray
                                        .getJSONObject(j)
                                        .optString("bandName"));
                                bandSearchListSetGet.setSearchedCatName(jsonRetArray
                                        .getJSONObject(j)
                                        .optString("catName"));
                                HubCityContext.searchedArrSubCatIds.add(jsonRetArray
                                        .getJSONObject(
                                                j).optString("catId"));
                                bandSearchListSetGet.setSearchedImgPath(jsonRetArray
                                        .getJSONObject(j)
                                        .optString
                                                ("bandImgPath"));
                                arrEvents.add(bandSearchListSetGet);
                            }
                        }
                    }
                }
                if (jsonObject.has("bottomBtnList")) {
                    try {
                        ArrayList<BottomButtonBO> bottomButtonList = bb
                                .parseForBottomButton(jsonObject);
                        BottomButtonListSingleton
                                .clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                        hasBottomButtons = true;
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }
                if (jsonObject.optString("nextPage").equals("1")) {
                    if (jsonObject.has("maxRowNum")) {
                        lastVisitedNo = jsonObject.getString("maxRowNum");
                    }
                    nextPage = true;
                } else {
                    nextPage = false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try {
            if (responseText != null && responseText.equals("Success")) {
                //first time
                ((BandsLandingPageActivity) mContext).loadEventList(arrEvents, nextPage,
                        lastVisitedNo);
            } else {
                if (responseText != null) {
                    Toast.makeText(mContext, responseText, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Technical problem.", Toast.LENGTH_SHORT).show();
                }
                arrEvents = new ArrayList<>();
                ((BandsLandingPageActivity) mContext).loadEventList(arrEvents, nextPage, lastVisitedNo);
            }
            if (hasBottomButtons && enableBottomButtons) {
                bb.createbottomButtontTab(parentLayout, false);
                parentLayout.setVisibility(View.VISIBLE);
            }
            if (!hasBottomButtons) {
                parentLayout.setVisibility(View.GONE);
            }
            if (bb != null) {
                bb.setOptionsValues(getListValues(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();

        values.put("Class", "BandLanding");

        values.put("mItemId", mItemId);

        values.put("latitude", String.valueOf(latitude));

        values.put("longitude", String.valueOf(longitude));

        values.put("moduleName", "Band");

        values.put("postalCode", postalCode);

        values.put("srchKey", searchKey);

        return values;
    }

}
