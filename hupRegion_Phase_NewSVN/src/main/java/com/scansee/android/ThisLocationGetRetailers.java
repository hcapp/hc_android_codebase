package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hubcity.android.CustomLayouts.ToggleButtonView;
import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.OptionSortByList;
import com.hubcity.android.screens.RetailerActivity;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class ThisLocationGetRetailers extends CustomTitleBar implements ToggleButtonView.ToggleViewInterface {

    private ArrayList<RetailerObj> retailerList = new ArrayList<>();
    private ListView getRetailersListView;
    private ThisLocationListAdapter getRetailersListAdapter;
    private String preferredRadius;
    private String lastVisitedRecord = "0";
    private boolean gpsEnabled;
    private String latitude = null;
    private String longitude = null;
    private String mItemId;
    private String bottomBtnId;
    private String fromLocateMap = null;
    private String locOnMap = "false";
    private final static String TAG_RESPONE_TEXT = "responseText";
    private AlertDialog.Builder alertDialogBuilder;
    private BottomButtons bb = null;
    private Activity activity;
    private LinearLayout linearLayout = null;
    private View moreResultsView;
    private boolean hasBottomBtns = false;
    private boolean enableBottomButtons = true;
    private String sortColumn = "distance";
    boolean isAlreadyLoading;
    boolean nextPage;
    private boolean isShowLoading;
    private TextView sortText;
    private String requestedTime;
    private final UrlRequestParams mUrlRequestParams = new UrlRequestParams();
    private final ServerConnections mServerConnections = new ServerConnections();
    TextView mLabelDistance, mLabelName;
    private CustomNavigation customNaviagation;
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thislocation_get_retailers);

        try {
            CommonConstants.hamburgerIsFirst = true;
            activity = ThisLocationGetRetailers.this;
            requestedTime = CommonConstants.convertDeviceTimeToUTC();
            OptionSortByList.sortChoice = "Distance";
            setBottomButton();
            bindView();
            setTitleView();
            setVisibility();
            getGpsResponse();
            getIntentData();
            if (fromLocateMap != null && "true".equals(fromLocateMap)) {
                locOnMap = "true";
                gpsEnabled = true;
                new GetRetailers().execute();
            } else {
                refresh();
            }
            setClickListener();

            //user for hamburger in modules
            customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setVisibility() {
//        rightImage.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
        leftTitleImage.setVisibility(View.GONE);
        drawerIcon.setVisibility(View.VISIBLE);
    }

    private void setTitleView() {
        title.setSingleLine(false);
        title.setMaxLines(2);
        title.setText("Choose Your" + "\n" + "Location");
    }

    private void bindView() {
        sortText = (TextView) findViewById(R.id.sort_text);
        linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
        getRetailersListView = (ListView) findViewById(R.id.lst_this_location_get_retailers);
        moreResultsView = getLayoutInflater().inflate(
                R.layout.listitem_get_retailers_viewmore, getRetailersListView,
                false);
        mLabelName = (TextView) this.findViewById(R.id.toggle_name);
        mLabelDistance = (TextView) this.findViewById(R.id.toggle_distance);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    private void getIntentData() {
        if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
            mItemId = getIntent().getExtras().getString(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA);
        }
        if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
            bottomBtnId = getIntent().getExtras().getString(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
        }

        if (getIntent().hasExtra("fromLocateMap")) {

            fromLocateMap = getIntent().getExtras().getString("fromLocateMap");
        }
    }

    private void setClickListener() {
        getRetailersListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                try {

                    RetailerObj selectedData = retailerList
                            .get(position);


                    Intent retailerInfo = new Intent(

                            ThisLocationGetRetailers.this, RetailerActivity.class);
                    retailerInfo
                            .putExtra(
                                    CommonConstants.TAG_RETAIL_ID,
                                    selectedData
                                            .getRetailerId());
                    retailerInfo.putExtra(
                            CommonConstants.TAG_RETAILER_NAME, selectedData
                                    .getRetailerName());
                    retailerInfo
                            .putExtra(
                                    CommonConstants.TAG_RETAILE_LOCATIONID,
                                    selectedData
                                            .getRetailLocationId());
                    retailerInfo
                            .putExtra(
                                    CommonConstants.TAG_RETAIL_LIST_ID,
                                    selectedData
                                            .getRetListId());
                    retailerInfo
                            .putExtra(
                                    CommonConstants.TAG_BANNER_IMAGE_PATH,
                                    selectedData
                                            .getBannerAdImagePath());
                    retailerInfo
                            .putExtra(
                                    CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
                                    selectedData
                                            .getRibbonAdImagePath());
                    retailerInfo.putExtra(
                            CommonConstants.TAG_RIBBON_AD_URL, selectedData
                                    .getRibbonAdURL());
                    retailerInfo.putExtra(CommonConstants.TAG_DISTANCE,
                            selectedData.getDistance());
                    retailerInfo.putExtra("isLocation", true);
                    retailerInfo.putExtra("ZipCode", Constants.getZipCode());

                    startActivityForResult(retailerInfo,
                            Constants.STARTVALUE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        rightImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (GlobalConstants.isFromNewsTemplate) {
                    Intent intent = null;
                    switch (GlobalConstants.className) {
                        case Constants.COMBINATION:
                            intent = new Intent(ThisLocationGetRetailers.this, CombinationTemplate.class);
                            break;
                        case Constants.SCROLLING:
                            intent = new Intent(ThisLocationGetRetailers.this, ScrollingPageActivity.class);
                            break;
                        case Constants.NEWS_TILE:
                            intent = new Intent(ThisLocationGetRetailers.this, TwoTileNewsTemplateActivity.class);
                            break;
                    }
                    if (intent != null) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                } else {
                    if (SortDepttNTypeScreen.subMenuDetailsList != null) {
                        SortDepttNTypeScreen.subMenuDetailsList.clear();
                        SubMenuStack.clearSubMenuStack();
                        SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
                    }
                    LoginScreenViewAsyncTask.bIsLoginFlag = true;
                    callingMainMenu();
                }
            }
        });
        refreshBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                alertDialogBuilder = new AlertDialog.Builder(
                        ThisLocationGetRetailers.this);
                alertDialogBuilder.setTitle("Refresh Your Location?");
                alertDialogBuilder
                        .setMessage("Do you want to refresh your location?");
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                enableBottomButtons = false;
                                retailerList = new ArrayList<>();

                                if (getRetailersListAdapter != null) {
                                    getRetailersListAdapter = new ThisLocationListAdapter(
                                            ThisLocationGetRetailers.this,
                                            retailerList);
                                    getRetailersListView
                                            .setAdapter(getRetailersListAdapter);
                                    getRetailersListAdapter
                                            .notifyDataSetChanged();

                                }
                                refresh();
                            }
                        });
                alertDialogBuilder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialogBuilder.show();
            }
        });

    }

    private void setBottomButton() {
        try {
            // For BottomButtons
            //noinspection ConstantConditions
            bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
            bb.setActivityInfo(activity, "This Location");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getGpsResponse() {
        // GPS check
        final LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gpsStatus = locManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (gpsStatus) {
            refreshBtn.setVisibility(View.VISIBLE);
        } else {
            refreshBtn.setVisibility(View.GONE);
        }

    }

    protected void viewMore() {
        // Added to disable toggle click
        CommonConstants.disableClick = true;
        int size = retailerList.size();
        lastVisitedRecord = retailerList.get(size - 1).getRowNumber(
        );
        enableBottomButtons = false;
        isAlreadyLoading = true;
        new GetRetailers().execute();
    }

    @SuppressWarnings("deprecation")
    private void refresh() {
        lastVisitedRecord = "0";
        nextPage = false;
        retailerList.clear();
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {

            gpsEnabled = true;
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                latitude = String.valueOf(locNew.getLatitude());
                longitude = String.valueOf(locNew.getLongitude());

            } else {
                // N/W Tower Info Start
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());
                } else {
                    latitude = CommonConstants.LATITUDE;
                    longitude = CommonConstants.LONGITUDE;
                }
                // N/W Tower Info end

            }
            new GetRetailers().execute();

        } else {

            gpsEnabled = false;
            new GetRetailers().execute();
        }

    }

    @Override
    public void onDestroy() {
        if (getRetailersListAdapter != null) {
            getRetailersListAdapter.customImageLoader.stopThread();
            getRetailersListView.setAdapter(null);
        }

        super.onDestroy();
    }

    @Override
    public void onToggleClick(String toggleBtnName) {

        enableBottomButtons = false;
        if (toggleBtnName.equalsIgnoreCase("Distance")) {
            OptionSortByList.sortChoice = "Distance";
            sortText.setText(R.string.sort_by_distance);
            isShowLoading = false;
            lastVisitedRecord = "0";
            retailerList = new ArrayList<>();
            getRetailersListAdapter.notifyDataSetChanged();
            sortColumn = "distance";
            setToggleButtonColor();
            new GetRetailers().execute();
        } else if (toggleBtnName.equalsIgnoreCase("Name")) {
            OptionSortByList.sortChoice = "name";
            sortText.setText(R.string.sort_by_name);
            isShowLoading = false;
            lastVisitedRecord = "0";
            retailerList = new ArrayList<>();
            getRetailersListAdapter.notifyDataSetChanged();
            sortColumn = "atoz";
            setToggleButtonColor();
            new GetRetailers().execute();

        } else {
            CommonConstants.startMapScreen(ThisLocationGetRetailers.this, getRetailerDetails(), false);
        }
    }

    private void setToggleButtonColor() {
        if (sortColumn.equalsIgnoreCase("Distance")) {
            if (Build.VERSION.SDK_INT >= 16) {
                mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
                mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
            } else {
                mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
                mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
            }

            mLabelName.setTextColor(getResources().getColor(R.color.white));
            mLabelName.setTypeface(null, Typeface.NORMAL);
            mLabelDistance.setTextColor(getResources().getColor(R.color.black));
            mLabelDistance.setTypeface(null, Typeface.BOLD);
        }
        if (sortColumn.equalsIgnoreCase("Name") || sortColumn.equalsIgnoreCase("atoz")) {
            if (Build.VERSION.SDK_INT >= 16) {
                mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
                mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            } else {
                mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
                mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            }
            mLabelDistance.setTextColor(getResources().getColor(R.color.white));
            mLabelDistance.setTypeface(null, Typeface.NORMAL);
            mLabelName.setTextColor(getResources().getColor(R.color.black));
            mLabelName.setTypeface(null, Typeface.BOLD);
        }
    }

    private class GetRetailers extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;
        JSONObject responseMenuObject = null;
        private ProgressDialog mDialog;
        String responseText = "";

        @Override
        protected void onPreExecute() {
            // Modified-Dileep
            if (!isShowLoading) {
                isShowLoading = true;
                mDialog = ProgressDialog.show(ThisLocationGetRetailers.this,
                        "", Constants.DIALOG_MESSAGE, true);
                mDialog.setCancelable(false);
            }
        }

        @Override
        protected String doInBackground(String... params) {

            String result = "false";
            try {
                String sortOrder = "ASC";
                String get_retail_info = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/getretailersinfojson";

                JSONObject urlParameters = mUrlRequestParams.getRetailersInfoJson(
                        preferredRadius, Constants.getZipCode(), latitude, longitude,
                        lastVisitedRecord, gpsEnabled + "",
                        Constants.getMainMenuId(), locOnMap, sortColumn,
                        sortOrder, bottomBtnId, mItemId, requestedTime);

//                jsonObject = mServerConnections.getUrlPostResponse(
//                        get_retail_info, urlParameters, true);
                jsonObject = mServerConnections.getUrlJsonPostResponse(
                        get_retail_info, urlParameters);

                if (jsonObject != null) {
                    if (jsonObject.has("bottomBtnList")) {
                        hasBottomBtns = true;
                        try {
                            ArrayList<BottomButtonBO> bottomButtonList = bb
                                    .parseForBottomButton(jsonObject);
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(bottomButtonList);

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    } else {
                        hasBottomBtns = false;
                    }


                    responseText = jsonObject.getString(TAG_RESPONE_TEXT);


                    if (!responseText.equalsIgnoreCase("Success")) {
                        return "false";
                    }

                    Object obj = jsonObject.get(CommonConstants.TAG_RETAILER_RESULT);

                    if (obj instanceof JSONArray) {

                        jsonArray = jsonObject.getJSONArray(CommonConstants.TAG_RETAILER_RESULT);


                        for (int arrayCount = 0; arrayCount < jsonArray
                                .length(); arrayCount++) {
                            RetailerObj retailerObj = new RetailerObj();
                            responseMenuObject = jsonArray
                                    .getJSONObject(arrayCount);
                            retailerObj
                                    .setRowNumber(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_ROW_NUMBER));
                            retailerObj
                                    .setCompleteAddress(
                                            responseMenuObject
                                                    .optString("completeAddress"));
                            retailerObj.setLocationOpen(responseMenuObject
                                    .optString("locationOpen"));

                            retailerObj
                                    .setRetailerName(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_RETAILER_NAME));
                            retailerObj
                                    .setRetailerId(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_RETAILER_ID));
                            if (responseMenuObject
                                    .has(CommonConstants.TAG_RETAILER_LOCATION_ID)) {
                                retailerObj
                                        .setRetailLocationId(
                                                responseMenuObject
                                                        .getString(CommonConstants.TAG_RETAILER_LOCATION_ID));
                            }
                            if (responseMenuObject
                                    .has(CommonConstants.TAG_RETAIL_LIST_ID)) {
                                retailerObj
                                        .setRetListId(
                                                responseMenuObject
                                                        .getString(CommonConstants.TAG_RETAIL_LIST_ID));
                            }
                            retailerObj
                                    .setRetailAddress1(
                                            responseMenuObject
                                                    .optString("retailAddress1"));
                            retailerObj
                                    .setRetailAddress2(
                                            responseMenuObject
                                                    .optString("retailAddress2"));
                            retailerObj
                                    .setCity(
                                            responseMenuObject
                                                    .optString("city"));
                            retailerObj
                                    .setState(
                                            responseMenuObject
                                                    .optString("state"));
                            retailerObj
                                    .setPostalCode(
                                            responseMenuObject
                                                    .optString("postalCode"));
                            retailerObj
                                    .setDistance(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_DISTANCE));
                            retailerObj
                                    .setLogoImagePath(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_LOGOIMAGE_PATH));
                            retailerObj
                                    .setRowNumber(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_ROW_NUMBER));
                            retailerObj
                                    .setRibbonAdURL(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_RIBBON_AD_URL));
                            retailerObj
                                    .setRibbonAdImagePath(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_RIBBON_ADIMAGE_PATH));

                            retailerObj
                                    .setBannerAdImagePath(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_BANNER_IMAGE_PATH));

                            retailerObj
                                    .setLatitude(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_RET_LATITIUDE));
                            retailerObj
                                    .setLongitude(
                                            responseMenuObject
                                                    .getString(CommonConstants.TAG_RET_LONGITUDE));
                            if (responseMenuObject
                                    .has(CommonConstants.TAG_SALE_FLG)) {

                                retailerObj
                                        .setSaleFlg(
                                                responseMenuObject
                                                        .getString(CommonConstants.TAG_SALE_FLG));
                            }

                            retailerList.add(retailerObj);
                        }
                    }
                    nextPage = "1".equalsIgnoreCase(jsonObject.getString(
                                    CommonConstants.TAG_RETAILER_NEXTPAGE)
                    );
                    result = "true";

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (mDialog != null)
                    mDialog.dismiss();
                isAlreadyLoading = false;
                if ("true".equals(result)) {
                    try {
                        if (nextPage) {

                            try {
                                getRetailersListView
                                        .removeFooterView(moreResultsView);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                            getRetailersListView.addFooterView(moreResultsView);

                        } else {
                            getRetailersListView.removeFooterView(moreResultsView);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    getRetailersListAdapter = new ThisLocationListAdapter(
                            ThisLocationGetRetailers.this, retailerList);
                    getRetailersListView.setAdapter(getRetailersListAdapter);
                    getRetailersListView.setSelection(Integer
                            .valueOf(lastVisitedRecord));

                    if (bb != null) {
                        bb.setOptionsValues(getListValues(), getRetailerDetails());
                    }

                } else {
                    alertDialogBuilder = new AlertDialog.Builder(
                            ThisLocationGetRetailers.this);

                    alertDialogBuilder.setMessage(responseText);
                    alertDialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    alertDialogBuilder.show();
                }

                try {
                    if (hasBottomBtns && enableBottomButtons) {

                        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) getRetailersListView
                                .getLayoutParams();
                        mlp.setMargins(0, 0, 0, 2);

                        bb.createbottomButtontTab(linearLayout, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Added to disable toggle click
                CommonConstants.disableClick = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();
        values.put("Class", "Whats NearBy");
        values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

        return values;
    }

    private ArrayList<RetailerBo> getRetailerDetails() {
        ArrayList<RetailerBo> mRetailerList = new ArrayList<>();
        int size = retailerList.size();
        for (int i = 0; i < size; i++) {

            String retailerName = retailerList.get(i).getRetailerName(
            );
            String retailAddress = retailerList.get(i).getCompleteAddress();
            String latitude = retailerList.get(i).getLatitude(
            );
            String longitude = retailerList.get(i).getLongitude(
            );
            String retailId = retailerList.get(i).getRetailerId(
            );
            String ribbonAdImagePath = retailerList.get(i).getRibbonAdImagePath(
            );
            String ribbonAdURL = retailerList.get(i).getRibbonAdURL(
            );
            String locationID = retailerList.get(i).getRetailLocationId(
            );
            String retListId = retailerList.get(i).getRetListId(
            );
            String bannerAd = retailerList.get(i).getBannerAdImagePath(
            );
            RetailerBo retailerObject = new RetailerBo(null, retailId,
                    retailerName, locationID, null, retailAddress, null, null,
                    bannerAd, ribbonAdImagePath, ribbonAdURL, null, latitude,
                    longitude, retListId);
            mRetailerList.add(retailerObject);

        }

        return mRetailerList;

    }

    @Override
    protected void onPause() {
        OptionSortByList.isSorted = false;
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            finish();
        }

        if (resultCode == 30001 || resultCode == 2) {
            OptionSortByList.isSorted = true;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }
        try {
            bb.setActivityInfo(activity, "This Location");
            if (bb != null) {
                bb.setOptionsValues(getListValues(), getRetailerDetails());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (OptionSortByList.isSorted) {
            getRetailersListView
                    .removeFooterView(moreResultsView);
            isShowLoading = false;
            retailerList = new ArrayList<>();

            if (getRetailersListAdapter != null) {
                getRetailersListAdapter = new ThisLocationListAdapter(
                        ThisLocationGetRetailers.this, retailerList);
                getRetailersListView.setAdapter(getRetailersListAdapter);
                getRetailersListAdapter.notifyDataSetChanged();

            }

            enableBottomButtons = false;
            OptionSortByList.isSorted = false;
            if ("name".equals(OptionSortByList.sortChoice)) {
                sortColumn = "atoz";
                sortText.setText(R.string.sort_by_name);
            } else if ("Distance".equals(OptionSortByList.sortChoice)) {
                sortColumn = "Distance";
                sortText.setText(R.string.sort_by_distance);
            }
            setToggleButtonColor();
            refresh();


        }

    }
}