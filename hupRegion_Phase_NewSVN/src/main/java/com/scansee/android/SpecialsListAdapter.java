package com.scansee.android;

import java.util.ArrayList;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.android.SpecialsActivity.RetailerGroup;
import com.scansee.hubregion.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SpecialsListAdapter extends BaseAdapter {
	private SpecialsActivity activity;
	private ArrayList<RetailerGroup> retailerGroupList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	public SpecialsListAdapter(Activity activity,
			ArrayList<RetailerGroup> couponsproductsList)
	{
		this.activity = (SpecialsActivity) activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.retailerGroupList = couponsproductsList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return retailerGroupList.size();
	}

	@Override
	public Object getItem(int id) {
		return retailerGroupList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder = null;

		if (position == retailerGroupList.size() - 1) {
			if (!((SpecialsActivity) activity).isAlreadyLoading) {
				if (((SpecialsActivity) activity).nextPage) {
					((SpecialsActivity) activity).viewMore();
				}
			}
		}

		if (convertView == null) {
			view = inflater.inflate(R.layout.listitem_specials_items, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.retailerName = (TextView) view
					.findViewById(R.id.retailer_name);
			viewHolder.retailerAddress = (TextView) view
					.findViewById(R.id.retailer_address);
			viewHolder.retailerImagepath = (ImageView) view
					.findViewById(R.id.retailer_imagepath);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		String distance = retailerGroupList.get(position).distance;
		String retailerName = retailerGroupList.get(position).retailerName;
		String retailAddress = retailerGroupList.get(position).retailAddress;
		String retailerImagePath = retailerGroupList.get(position).retailerImagePath;

		viewHolder.retailerName.setText(retailerName);
		viewHolder.retailerAddress.setText(distance + " " + retailAddress);
		viewHolder.retailerImagepath.setTag(retailerImagePath);
		if (retailerImagePath != null) {
			customImageLoader.displayImage(retailerImagePath, activity,
					viewHolder.retailerImagepath);
		}
		return view;
	}

	public static class ViewHolder {
		protected TextView retailerAddress;
		protected ImageView retailerImagepath;
		protected TextView retailerName;

	}

}
