package com.scansee.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ViewFlipper;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class FindActivity extends CustomTitleBar implements OnClickListener {

    boolean gpsEnabled = false;
    String responseText = " No Response from server";
    private ArrayList<HashMap<String, String>> findList = null,
            findsmartsearchList = null;
    ArrayList<HashMap<String, String>> bottomBtnList = null;

    public static final String TAG_FINDCATEGORY_RESULT_SET_CATLIST = "catList";
    public static final String TAG_FINDCATEGORY_NAME = "CatDisName";
    public static final String TAG_FINDCATEGORY_NAME_ID = "catID";
    public static final String TAG_FINDCATEGORYIMAGE_PATH = "CatImgPth";

    protected LocationManager locationManager;
    boolean flag = false;
    String mItemId = null, bottomBtnId = null;
    ImageButton cancel;
    EditText edittextRetailer;
    String locationText, productText, userId, smartsearchKey, searchkey,
            parCatId = "0", accZipcode = null, lastVistedProductNo = "0";

    ListView findListView, smartsearchListView;
    FindCategoryListAdapter findListAdapter;

    FindSmartSearchListAdapter findsmartsearchListAdapter;
    Activity activity;
    ViewFlipper findFlipper;
    String latitude;
    String longitude;
    boolean isManualSearch = false;

    boolean isFirstTime = true;
    FindSearchProductListListAdapter findproductsearchListAdapter;
    private ArrayList<HashMap<String, String>> findsearchlist = null;
    LinearLayout linearLayout = null;
    BottomButtons bb = null;

    boolean hasBottomBtns = false;
    boolean enableBottomButton = true;
    String mLinkId;

    FindLocation findAsyncTask;
    SmartSearch smartSrcAsyncTask;
    ManualSearch manualSrcAsyncTask;

    boolean isFirst, isAlreadyLoading;
    boolean isSubMenu;

    String mColor = "#FFFFFF";
    String mFontColor = "#000000";
    public boolean nextPage;
    int previousPosition;
    View moreResultsView;
    private CustomNavigation customNaviagation;

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }

        getGPSValues();

        if (bb != null) {
            if (bottomBtnId != null && !bottomBtnId.equals("")) {
                bb.setActivityInfo(activity, "Find-BottomBtn");
            } else {
                bb.setActivityInfo(activity, "Find");
            }
            bb.setOptionsValues(null, null);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_listview);

        try {
            activity = FindActivity.this;
            CommonConstants.hamburgerIsFirst = true;
            isFirst = true;

            linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
            linearLayout.setVisibility(View.INVISIBLE);
            linearLayout.setBackgroundColor(Color.parseColor(mColor));
            try {
                if (getIntent().hasExtra("BgColor")) {
                    if (getIntent().getExtras().getString("BgColor")
                            .contains("http")) {
                        new ImageLoaderAsync(linearLayout).execute(getIntent()
                                .getExtras().getString("BgColor"));
                    } else {
                        linearLayout.setBackgroundColor(Color
                                .parseColor(getIntent().getExtras().getString(
                                        "BgColor")));
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (getIntent().hasExtra("isSubMenu")
                    && getIntent().getExtras().getBoolean("isSubMenu")) {
                isSubMenu = true;
            }

            if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
                if ("".equals(mItemId)) {
                    mItemId = null;
                }
            }
            if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
                bottomBtnId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }
            if (getIntent().hasExtra("mLinkId")) {
                mLinkId = getIntent().getExtras().getString("mLinkId");

            } else if (getIntent().hasExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA)) {
                mLinkId = getIntent().getExtras().getString(
                        Constants.BOTTOM_LINK_ID_INTENT_EXTRA);
            }
            rightImage = (ImageView) findViewById(R.id.right_button);

            if (getIntent().hasExtra("Title")) {
                title.setText(getIntent().getExtras().getString("Title"));
            } else {
                title.setText("Find");
            }

            leftTitleImage.setVisibility(View.INVISIBLE);

            findViewById(R.id.find_cancel).setOnClickListener(this);
            findListView = (ListView) findViewById(R.id.find_listview_location);
            smartsearchListView = (ListView) findViewById(R.id.find_listview_product);
            moreResultsView = getLayoutInflater().inflate(
                    R.layout.listitem_get_retailers_viewmore, smartsearchListView,
                    false);

            // google props
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (GlobalConstants.isFromNewsTemplate) {
                        Intent intent = null;
                        switch (GlobalConstants.className) {

                            case Constants.COMBINATION:
                                intent = new Intent(FindActivity.this, CombinationTemplate.class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(FindActivity.this, ScrollingPageActivity.class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(FindActivity.this, TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    } else {
                        closeKeypad(edittextRetailer);
                        callingMainMenu();
                    }
                }
            });

            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    hideKeyboardItem();
                    cancelAsyncTasks();
                    finish();

                }
            });

            gpsEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (gpsEnabled) {
                refreshBtn.setVisibility(View.VISIBLE);
            } else {
                refreshBtn.setVisibility(View.GONE);
            }


            refreshBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            FindActivity.this);
                    alertDialogBuilder.setTitle("Refresh Your Location?");
                    alertDialogBuilder
                            .setMessage("Do you want to refresh your location?");
                    alertDialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();
                                    enableBottomButton = false;
                                    checkLocService();
                                }
                            });
                    alertDialogBuilder.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialogBuilder.show();
                }

            });

            findsearchlist = new ArrayList<>();

            findFlipper = (ViewFlipper) findViewById(R.id.find_viewflippers);
            findFlipper.setDisplayedChild(0);

            SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
            userId = settings.getString(CommonConstants.USER_ID, "0");

            edittextRetailer = (EditText) findViewById(R.id.find_category_edit_text);
            edittextRetailer.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
            if (bottomBtnId != null && !bottomBtnId.equals("")) {
                try {
                    // Initiating Bottom button class
                    bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
                    bb.setActivityInfo(activity, "Find-BottomBtn");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    // Initiating Bottom button class
                    bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
                    bb.setActivityInfo(activity, "Find");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            edittextRetailer
                    .setOnEditorActionListener(new OnEditorActionListener() {

                        @Override
                        public boolean onEditorAction(TextView textView,
                                                      int actionId, KeyEvent event) {

                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                if (findFlipper.getDisplayedChild() == 0) {
                                    onClick(textView);
                                } else if (findFlipper.getDisplayedChild() == 1) {

                                    lastVistedProductNo = "0";
                                    if (smartSrcAsyncTask != null
                                            && AsyncTask.Status.RUNNING == smartSrcAsyncTask
                                            .getStatus()) {
                                        smartSrcAsyncTask.cancel(true);

                                    }

                                    findsmartsearchList = new ArrayList<>();
                                    findsmartsearchListAdapter = new FindSmartSearchListAdapter(
                                            FindActivity.this, findsmartsearchList);
                                    smartsearchListView
                                            .setAdapter(findsmartsearchListAdapter);
                                    findsmartsearchListAdapter
                                            .notifyDataSetChanged();

                                    if (null != findsearchlist) {

                                        findsearchlist.clear();
                                    }
                                    findproductsearchListAdapter = new
                                            FindSearchProductListListAdapter(
                                            FindActivity.this, findsearchlist);
                                    smartsearchListView
                                            .setAdapter(findproductsearchListAdapter);
                                    Parcelable state = smartsearchListView
                                            .onSaveInstanceState();
                                    smartsearchListView
                                            .setAdapter(findproductsearchListAdapter);
                                    smartsearchListView
                                            .onRestoreInstanceState(state);
                                    smartsearchListView.setSelection(Integer
                                            .valueOf(lastVistedProductNo));
                                    findproductsearchListAdapter
                                            .notifyDataSetChanged();

                                    searchkey = edittextRetailer.getText()
                                            .toString().trim();
                                    isManualSearch = true;

                                    callManualSrcAsyncTask(searchkey);

                                }
                            }
                            hideKeyboardItem();
                            return true;
                        }
                    });
            edittextRetailer.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence sequence, int start,
                                          int before, int count) {
                    String value = sequence.toString();
                    if (findFlipper.getDisplayedChild() == 0) {
                        locationText = value.trim();

                    } else if (findFlipper.getDisplayedChild() == 1) {
                        if (value.length() < 5) {
                            if (smartSrcAsyncTask != null
                                    && AsyncTask.Status.RUNNING == smartSrcAsyncTask
                                    .getStatus()) {
                                smartSrcAsyncTask.cancel(true);
                            }
                            findsmartsearchList = new ArrayList<>();
                            findsmartsearchListAdapter = new FindSmartSearchListAdapter(
                                    FindActivity.this, findsmartsearchList);
                            smartsearchListView
                                    .setAdapter(findsmartsearchListAdapter);
                            findsmartsearchListAdapter.notifyDataSetChanged();

                        } else if (value.length() > 4) {
                            productText = value.trim();
                            smartsearchKey = productText;

                            isManualSearch = false;

                            callSmartSrcAsyncTask(value);
                        }

                    }

                }

                @Override
                public void beforeTextChanged(CharSequence sequence, int start,
                                              int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

            findListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    try {
                        String selectedCat = findList.get(position).get(
                                TAG_FINDCATEGORY_NAME);
                        String selectedCatId = findList.get(position).get(
                                TAG_FINDCATEGORY_NAME_ID);
                        Intent locationIntent = new Intent(FindActivity.this,
                                FindLocationActivity.class);

                        locationIntent.putExtra("mLinkId", mLinkId);
                        locationIntent.putExtra("selectedCat", selectedCat);
                        locationIntent.putExtra("selectedCatId", selectedCatId);
                        locationIntent.putExtra("mItemId", mItemId);
                        locationIntent.putExtra(
                                Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
                        startActivityForResult(locationIntent, Constants.STARTVALUE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            });
            hideKeyBoard();
            checkLocService();

            smartsearchListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    try {
                        Intent locationIntent;
                        @SuppressWarnings("unchecked")
                        HashMap<String, String> selectedItem = (HashMap<String, String>)
                                smartsearchListView
                                        .getAdapter().getItem(position);
                        String itemName = selectedItem.get("itemName");
                        if ("smartSearch".equalsIgnoreCase(itemName)) {
                            String selectedCat = selectedItem
                                    .get(CommonConstants.FINDSMARTSEARCHCATEGORYID);
                            locationIntent = new Intent(FindActivity.this,
                                    FindGetProductListActivity.class);
                            locationIntent.putExtra(
                                    CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
                                    smartsearchKey);
                            locationIntent.putExtra("selectedCat", selectedCat);
                            locationIntent.putExtra("findTitleText", "Find");
                            startActivityForResult(locationIntent,
                                    Constants.STARTVALUE);

                        } else if ("nameSearch".equalsIgnoreCase(itemName)) {
                            int count = Integer.valueOf(selectedItem
                                    .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTPRODUCTCOUNT));
                            if (count > 1) {
                                Intent intent = new Intent(FindActivity.this,
                                        FindGetProductListActivity.class);
                                intent.putExtra(
                                        CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
                                        selectedItem
                                                .get(CommonConstants
                                                        .FINDPRODUCTSEARCHPRODUCTNAME));

                                startActivity(intent);
                            } else {
                                locationIntent = new Intent(FindActivity.this,
                                        RetailerCurrentsalesActivity.class);
                                locationIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_PRODUCTID,
                                                selectedItem
                                                        .get(CommonConstants
                                                                .FINDPRODUCTSEARCHPRODUCTID));
                                locationIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
                                                selectedItem
                                                        .get(CommonConstants
                                                                .FINDPRODUCTSEARCHPRODUCTLISTID));
                                locationIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
                                                selectedItem
                                                        .get(CommonConstants
                                                                .FINDPRODUCTSEARCHPRODUCTNAME));
                                locationIntent.putExtra("Find", true);
                                startActivity(locationIntent);
                            }
                        } else if ("viewMore".equalsIgnoreCase(itemName)) {
                            // findsearchlist.remove(position);
                            // lastVistedProductNo = selectedItem
                            // .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER);
                            // new ManualSearch().execute();
                        } else if ("product".equalsIgnoreCase(itemName)) {

                            Intent navIntent = new Intent(FindActivity.this,
                                    RetailerCurrentsalesActivity.class);

                            navIntent
                                    .putExtra(
                                            CommonConstants.TAG_CURRENTSALES_PRODUCTID,
                                            selectedItem
                                                    .get(CommonConstants
                                                            .FINDPRODUCTSEARCHPRODUCTID));
                            navIntent
                                    .putExtra(
                                            CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
                                            selectedItem
                                                    .get(CommonConstants
                                                            .FINDPRODUCTSEARCHPRODUCTLISTID));
                            navIntent
                                    .putExtra(
                                            CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
                                            selectedItem
                                                    .get(CommonConstants
                                                            .FINDPRODUCTSEARCHPRODUCTNAME));

                            navIntent.putExtra("Find", true);
                            startActivity(navIntent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
//user for hamburger in modules
            drawerIcon.setVisibility(View.VISIBLE);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    class ImageLoaderAsync extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            BitmapDrawable background = new BitmapDrawable(bmImg);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                bmImage.setBackgroundDrawable(background);
            } else {
                bmImage.setBackgroundDrawable(background);
            }

            try {
                if ((this.mDialog != null) && this.mDialog.isShowing()) {
                    this.mDialog.dismiss();
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void viewMore() {
        previousPosition = findsearchlist.size() - 1;
        isAlreadyLoading = true;
        // findsearchlist.remove(findsearchlist.size() - 1);
        lastVistedProductNo = findsearchlist.get(findsearchlist.size() - 1)
                .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER);
        new ManualSearch().execute();
    }

    @SuppressWarnings("deprecation")
    public void getGPSValues() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                latitude = String.valueOf(locNew.getLatitude());
                longitude = String.valueOf(locNew.getLongitude());

            } else {
                // N/W Tower Info Start
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());
                } else {
                    latitude = CommonConstants.LATITUDE;
                    longitude = CommonConstants.LONGITUDE;
                }
            }
        }
    }

    protected void checkLocService() {
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (gpsEnabled) {
                flag = false;
//				accZipcode = null;
                accZipcode = Constants.getZipCode();
                getGPSValues();

            } else if (!"".equals(Constants.getZipCode())) {
                accZipcode = Constants.getZipCode();

            } else {

                new AlertDialog.Builder(this).setTitle("Alert")
                        .setMessage(R.string.find_alert_nozipcode)
                        .setPositiveButton("OK", null).show();

            }
        } else {
            accZipcode = Constants.getZipCode();
        }

        callFindAsyncTask();

    }

    @Override
    public void onDestroy() {
        if (findsmartsearchListAdapter != null) {
            smartsearchListView.setAdapter(null);
        }

        cancelAsyncTasks();

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancelAsyncTasks();
        finish();
    }

    private class FindLocation extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;
        JSONObject responseMenuObject = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(FindActivity.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            findList = new ArrayList<>();
            bottomBtnList = new ArrayList<>();

            String result = "false";
            try {
                String get_findcategory_url = Properties.url_local_server
                        + Properties.hubciti_version + "find/getcategoryjson";

                UrlRequestParams mUrlRequestParams = new UrlRequestParams();
                ServerConnections mServerConnections = new ServerConnections();

                String radious = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).getString
                        ("radious", "");
                String cityIds = Constants.getSortedCityIds(new CityDataHelper().getCitiesList
                        (FindActivity.this));
                JSONObject jsonUrlParameters = mUrlRequestParams
                        .getFindCategoryPartners(mItemId, accZipcode,
                                latitude, longitude, bottomBtnId,
                                isSubMenu, radious, cityIds);
                jsonObject = mServerConnections.getUrlJsonPostResponse(
                        get_findcategory_url, jsonUrlParameters);

                if (jsonObject != null) {

                    responseText = jsonObject.getString(
                            "responseText");

                    if (jsonObject.has("mColor")) {
                        mColor = jsonObject.getString(
                                "mColor");
                    }

                    if (jsonObject.has("mFontColor")) {
                        mFontColor = jsonObject.getString(
                                "mFontColor");
                    }


                    // For BottomButton
                    if (jsonObject.has(
                            "bottomBtnList")) {

                        hasBottomBtns = true;

                        try {
                            if (bb != null) {
                                ArrayList<BottomButtonBO> bottomButtonList = bb
                                        .parseForBottomButton(jsonObject);
                                BottomButtonListSingleton
                                        .clearBottomButtonListSingleton();
                                BottomButtonListSingleton
                                        .getListBottomButton(bottomButtonList);
                            }

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    } else {
                        hasBottomBtns = false;
                    }

                    Object obj = jsonObject.get(TAG_FINDCATEGORY_RESULT_SET_CATLIST);

                    if (obj != null) {
                        HashMap<String, String> findData;
                        if (obj instanceof JSONArray) {
                            jsonArray = jsonObject.getJSONArray
                                    (TAG_FINDCATEGORY_RESULT_SET_CATLIST);

                            for (int arrayCount = 0; arrayCount < jsonArray
                                    .length(); arrayCount++) {
                                findData = new HashMap<>();
                                responseMenuObject = jsonArray
                                        .getJSONObject(arrayCount);
                                findData.put(
                                        TAG_FINDCATEGORY_NAME,
                                        responseMenuObject
                                                .getString(TAG_FINDCATEGORY_NAME));

                                findData.put(
                                        TAG_FINDCATEGORY_NAME_ID,
                                        responseMenuObject
                                                .getString(TAG_FINDCATEGORY_NAME_ID));
                                findData.put(
                                        TAG_FINDCATEGORYIMAGE_PATH,
                                        responseMenuObject
                                                .getString(TAG_FINDCATEGORYIMAGE_PATH));

                                findList.add(findData);
                            }

                        } else if (obj instanceof JSONObject) {
                            findData = new HashMap<>();
                            responseMenuObject = jsonObject.getJSONObject
                                    (TAG_FINDCATEGORY_RESULT_SET_CATLIST);

                            findData.put(
                                    TAG_FINDCATEGORY_NAME,
                                    responseMenuObject
                                            .getString(TAG_FINDCATEGORY_NAME));

                            findData.put(
                                    TAG_FINDCATEGORY_NAME_ID,
                                    responseMenuObject
                                            .getString(TAG_FINDCATEGORY_NAME_ID));
                            findData.put(
                                    TAG_FINDCATEGORYIMAGE_PATH,
                                    responseMenuObject
                                            .getString(TAG_FINDCATEGORYIMAGE_PATH));

                            findList.add(findData);
                        }

                        result = "true";

                    } else {
                        result = "false";
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                if ("true".equals(result)) {

                    findListAdapter = new FindCategoryListAdapter(activity,
                            findList, mColor, mFontColor, bottomBtnId);
                    findListView.setAdapter(null);
                    findListView.setAdapter(findListAdapter);

                } else {

                    try {
                        mDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            FindActivity.this);

                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setMessage(Html.fromHtml(responseText
                            .replace("\\n", "<br/>")));
                    alertDialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialogBuilder.show();
                }

                if (hasBottomBtns && enableBottomButton) {

                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) findListView
                            .getLayoutParams();
                    mlp.setMargins(0, 0, 0, 2);
                    if (bb != null) {
                        bb.createbottomButtontTab(linearLayout, false);
                    }
                }

                linearLayout.setVisibility(View.VISIBLE);
                try {
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void hideKeyBoard() {
        FindActivity.this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void hideKeyboardItem() {
        InputMethodManager imm = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edittextRetailer.getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.find_cancel:
                findCancel();
                break;
            case R.id.find_category_edit_text:
                findCategoryEditText();
                break;

            default:
                break;
        }
    }

    private void findCategoryEditText() {
        hideKeyboardItem();
        if (!edittextRetailer.getText()
                .toString().trim().equals("")) {
            Intent locationIntent = new Intent(FindActivity.this,
                    FindRetailerSearchActivity.class);
            locationIntent.putExtra("retSearchText", edittextRetailer.getText()
                    .toString().trim());
            locationIntent.putExtra("mLinkId", mLinkId);

            locationIntent.putExtra("module", "retSearch");
            locationIntent.putExtra("mItemId", mItemId);
            locationIntent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                    bottomBtnId);
            startActivityForResult(locationIntent, Constants.STARTVALUE);
        }
    }

    private void findCancel() {
        if (edittextRetailer != null) {
            edittextRetailer.setText("");
            hideKeyboardItem();
        }

    }

    private class SmartSearch extends AsyncTask<String, Void, String> {
        UrlRequestParams objUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        @Override
        protected String doInBackground(String... args) {
            // Replace with new code
            findsmartsearchList = new ArrayList<>();
            JSONObject jsonObject;
            JSONArray jsonArraySmartsearchData = new JSONArray();
            try {
                // Replace with new code
                String urlParameters = objUrlRequestParams
                        .getFindSmartSearch(smartsearchKey);
                String get_findsmartsearchdata = Properties.url_local_server
                        + Properties.hubciti_version
                        + "scannow/getsmartsearchprods";
                jsonObject = mServerConnections.getUrlPostResponse(
                        get_findsmartsearchdata, urlParameters, true);

                if (jsonObject != null) {
                    try {
                        JSONObject smartSearchObj = jsonObject.getJSONObject(
                                CommonConstants.FINDSMARTSEARCHRESULTSET)
                                .getJSONObject(
                                        CommonConstants.FINDSMARTSEARCHMENU);
                        jsonArraySmartsearchData.put(smartSearchObj);
                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonArraySmartsearchData = jsonObject.getJSONObject(
                                CommonConstants.FINDSMARTSEARCHRESULTSET)
                                .getJSONArray(
                                        CommonConstants.FINDSMARTSEARCHMENU);
                    }

                    for (int arrayCount = 0; arrayCount < jsonArraySmartsearchData
                            .length(); arrayCount++) {
                        HashMap<String, String> scanseeData = new HashMap<>();
                        scanseeData.put("itemName", "smartSearch");
                        scanseeData
                                .put(CommonConstants.FINDSMARTSEARCHCATEGORYNAME,
                                        jsonArraySmartsearchData
                                                .getJSONObject(arrayCount)
                                                .getString(
                                                        CommonConstants
                                                                .FINDSMARTSEARCHCATEGORYNAME));
                        scanseeData
                                .put(CommonConstants.FINDSMARTSEARCHCATEGORIES,
                                        jsonArraySmartsearchData
                                                .getJSONObject(arrayCount)
                                                .getString(
                                                        CommonConstants
                                                                .FINDSMARTSEARCHCATEGORIES));
                        scanseeData
                                .put(CommonConstants.FINDSMARTSEARCHCATEGORYID,
                                        jsonArraySmartsearchData
                                                .getJSONObject(arrayCount)
                                                .getString(
                                                        CommonConstants
                                                                .FINDSMARTSEARCHCATEGORYID));
                        findsmartsearchList.add(scanseeData);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if (null != smartsearchKey && smartsearchKey.length() < 5) {
                if (null != findsmartsearchList) {

                    findsmartsearchList.clear();
                }
            } else {
                if (findsmartsearchList != null) {
                    if (isFirstTime) {

                        isFirstTime = false;
                    } else {
                        if (!isManualSearch) {
                            findsmartsearchListAdapter = new FindSmartSearchListAdapter(
                                    FindActivity.this, findsmartsearchList);
                            smartsearchListView.setAdapter(null);
                            smartsearchListView
                                    .setAdapter(findsmartsearchListAdapter);

                        }

                    }
                }
            }

        }
    }

    boolean isShowLoading;

    private class ManualSearch extends AsyncTask<String, Void, String> {
        UrlRequestParams objUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog mDialog;
        JSONObject jsonObject = null;
        JSONObject responseMenuObject = null;
        JSONArray jsonArrayProductDetail = null;
        HashMap<String, String> productData = null;

        @Override
        protected void onPreExecute() {
            if (!isShowLoading) {
                isShowLoading = true;
                mDialog = ProgressDialog.show(FindActivity.this, "",
                        Constants.DIALOG_MESSAGE, true);
                mDialog.setCancelable(false);
            }

        }

        @Override
        protected String doInBackground(String... args) {
            String result = "false";

            try {

                String urlParameters = objUrlRequestParams
                        .getFindProductSearchList(searchkey, parCatId,
                                lastVistedProductNo, Constants.getMainMenuId());
                String get_product_items_list = Properties.url_local_server
                        + Properties.hubciti_version
                        + "scannow/getsmartsearchcount";
                jsonObject = mServerConnections.getUrlPostResponse(
                        get_product_items_list, urlParameters, true);

                if (jsonObject != null) {

                    try {
                        JSONObject productDetail = jsonObject.getJSONObject(
                                CommonConstants.FINDPRODUCTSEARCHRESULTSET)
                                .getJSONObject(
                                        CommonConstants.FINDPRODUCTSEARCHMENU);

                        jsonArrayProductDetail = new JSONArray();
                        jsonArrayProductDetail.put(productDetail);
                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonArrayProductDetail = jsonObject.getJSONObject(
                                CommonConstants.FINDPRODUCTSEARCHRESULTSET)
                                .getJSONArray(
                                        CommonConstants.FINDPRODUCTSEARCHMENU);
                    }

                    for (int arrayCount = 0; arrayCount < jsonArrayProductDetail
                            .length(); arrayCount++) {
                        productData = new HashMap<>();
                        responseMenuObject = jsonArrayProductDetail
                                .getJSONObject(arrayCount);

                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTID,
                                        responseMenuObject
                                                .getString(CommonConstants
                                                        .FINDPRODUCTSEARCHPRODUCTID));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID,
                                        responseMenuObject
                                                .getString(CommonConstants
                                                        .FINDPRODUCTSEARCHPRODUCTLISTID));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
                                        responseMenuObject
                                                .getString(CommonConstants
                                                        .FINDPRODUCTSEARCHPRODUCTNAME));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION,
                                        responseMenuObject
                                                .getString(CommonConstants
                                                        .FINDPRODUCTSEARCHPRODUCTDESCRIPTION));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHIMAGEPATH,
                                        responseMenuObject
                                                .getString(CommonConstants
                                                        .FINDPRODUCTSEARCHIMAGEPATH));
                        productData
                                .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
                                        responseMenuObject
                                                .getString(CommonConstants
                                                        .FINDPRODUCTSEARCHPRODUCTROWNUMBER));
                        productData.put("itemName", "product");

                        findsearchlist.add(productData);
                    }
                    // productData = new HashMap<String, String>();
// productData.put("itemName", "viewMore");
// productData
// .put(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
// tempLastVisit);
// findsearchlist.add(productData);
                    nextPage = "1".equalsIgnoreCase(jsonObject
                            .getJSONObject(
                                    CommonConstants.FINDPRODUCTSEARCHRESULTSET)
                            .getString(
                                    CommonConstants.FINDPRODUCTSEARCHPRODUCTNEXTPAGE))
                            && productData != null;
                    result = "true";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if ("true".equals(result)) {
                try {
                    if (nextPage) {

                        try {
                            moreResultsView.setVisibility(View.VISIBLE);
                            smartsearchListView
                                    .removeFooterView(moreResultsView);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        // moreInfo.setVisibility(View.VISIBLE);

                        smartsearchListView.addFooterView(moreResultsView);

                    } else {
                        smartsearchListView.removeFooterView(moreResultsView);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                isAlreadyLoading = false;
                findproductsearchListAdapter = new FindSearchProductListListAdapter(
                        FindActivity.this, findsearchlist);
                smartsearchListView.setAdapter(findproductsearchListAdapter);
                smartsearchListView.setAdapter(findproductsearchListAdapter);
                // smartsearchListView.onRestoreInstanceState(state);
                smartsearchListView.setSelection(previousPosition);

            } else {
                try {
                    smartsearchListView.removeFooterView(moreResultsView);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
                        FindActivity.this);
                notificationAlert.setMessage(getString(R.string.norecord))
                        .setPositiveButton(getString(R.string.specials_ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        edittextRetailer.setText("");
                                        dialog.dismiss();
                                    }
                                });

                notificationAlert.create().show();

            }

            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            cancelAsyncTasks();
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void callFindAsyncTask() {

        if (isFirst) {
            findAsyncTask = new FindLocation();
            findAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            isFirst = false;
            return;
        }

        if (findAsyncTask != null) {
            if (!findAsyncTask.isCancelled()) {
                findAsyncTask.cancel(true);
            }

            findAsyncTask = null;
        }

        findAsyncTask = new FindLocation();
        findAsyncTask.execute();

    }

    private void callSmartSrcAsyncTask(String value) {

        if (smartSrcAsyncTask != null) {
            if (!smartSrcAsyncTask.isCancelled()) {
                smartSrcAsyncTask.cancel(true);
            }

            smartSrcAsyncTask = null;
        }

        smartSrcAsyncTask = new SmartSearch();
        smartSrcAsyncTask.execute(value);

    }

    private void callManualSrcAsyncTask(String searchKey) {

        if (manualSrcAsyncTask != null) {
            if (!manualSrcAsyncTask.isCancelled()) {
                manualSrcAsyncTask.cancel(true);
            }

            manualSrcAsyncTask = null;
        }

        manualSrcAsyncTask = new ManualSearch();
        manualSrcAsyncTask.execute(searchKey);

    }

    private void cancelAsyncTasks() {
        if (findAsyncTask != null && !findAsyncTask.isCancelled()) {
            findAsyncTask.cancel(true);
        }
        findAsyncTask = null;

        if (smartSrcAsyncTask != null && !smartSrcAsyncTask.isCancelled()) {
            smartSrcAsyncTask.cancel(true);
        }
        smartSrcAsyncTask = null;

        if (manualSrcAsyncTask != null && !manualSrcAsyncTask.isCancelled()) {
            manualSrcAsyncTask.cancel(true);
        }
        manualSrcAsyncTask = null;

    }

}
