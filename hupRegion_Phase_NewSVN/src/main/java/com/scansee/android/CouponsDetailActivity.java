package com.scansee.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.AddCouponDetails;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CouponDetails;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.GetAddCouponObject;
import com.hubcity.android.commonUtil.GetCouponDetailsObject;
import com.hubcity.android.commonUtil.GetRedeemObject;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.RedeemGetDetails;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.RetailerActivity;
import com.hubcity.android.screens.ShareInformation;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.CommonMethods;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CouponsDetailActivity extends CustomTitleBar implements
        OnClickListener {

    Activity mCouponsActivity;
    public static final String COUPON_TYPE_ALL = "Allcoups";
    boolean redirect = false;
    private PopupWindow pwindo;
    private PopupWindow locPwindo;
    String couponId = "", couponadded, type, couponname, couponListId;
    String usedFlag, expiredFlag;
    TextView couponName, couponstartdate, couponexpiredate,
            txtTermsnConds, retailerName;
    String termsAndConditionsDesc;
    ProgressBar progressBar;
    TextView txtTermsConditions;
    ImageView couponimagepath;
    CustomImageLoader customImageLoader;
    private WebView webview;
    private ProgressDialog progDialog;
    Button claimBtn, redeemBtn, location;
    String tabSelected = null;
    private RestClient mRestClient;
    boolean isMainmenuBtnClicked = false;
    boolean isCliped = false, isUsed = false;
    protected ArrayList<HashMap<String, String>> couponsproductsList = null;
    protected ArrayList<HashMap<String, String>> couponsLocationsList = null;
    CouponsRetailListAdapter couponsretailListAdapter;
    protected ListView couponsproductListView;
    private ShareInformation shareInfo;
    private boolean isShareTaskCalled;
    String retailerId = "1108557";
    private boolean isCoupon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupon_detail);
        try {
            mCouponsActivity = this;
            getIntentData();
            mRestClient = RestClient.getInstance();
            String strTitle = getIntent().getExtras().getString("couponName");
            title.setSingleLine(false);
            title.setMaxLines(2);
            if (strTitle != null) {
                title.setText(strTitle);
            } else {
                title.setText("Redeem");
            }

            isShareTaskCalled = false;
            leftTitleImage
                    .setBackgroundResource(R.drawable.share_btn_down);
            leftTitleImage
                    .setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (!Constants.GuestLoginId
                                    .equals(UrlRequestParams.getUid()
                                            .trim())) {
                                shareInfo.shareTask(isShareTaskCalled);
                                isShareTaskCalled = true;
                            } else {
                                Constants
                                        .SignUpAlert(CouponsDetailActivity.this);
                                isShareTaskCalled = false;
                            }
                        }
                    });


            shareInfo = new ShareInformation(
                    CouponsDetailActivity.this, "", "",
                    couponId, "Coupons");

            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (GlobalConstants.isFromNewsTemplate) {
                        Intent intent = null;
                        switch (GlobalConstants.className) {
                            case Constants.COMBINATION:
                                intent = new Intent(CouponsDetailActivity.this, CombinationTemplate.class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(CouponsDetailActivity.this, ScrollingPageActivity.class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(CouponsDetailActivity.this, TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    } else {
                        if ("pushnotify".equals(getIntent().getStringExtra("push"))) {
                            if (SortDepttNTypeScreen.subMenuDetailsList != null) {
                                SortDepttNTypeScreen.subMenuDetailsList.clear();
                                SubMenuStack.clearSubMenuStack();
                                SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
                            }
                            LoginScreenViewAsyncTask.bIsLoginFlag = true;
                            callingMainMenu();
                        } else {
                            setResult(Constants.FINISHVALUE);
                            isMainmenuBtnClicked = true;
                            LoginScreenViewAsyncTask.bIsLoginFlag = true;
                            callingMainMenu();
                        }
                    }
                }
            });

            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (SubMenuStack.getSubMenuStack() != null
                            && SubMenuStack.getSubMenuStack().size() > 0) {
                        finish();
                    } else {
                        callingMainMenu();
                    }
                }
            });

            if (type != null && type.equalsIgnoreCase(COUPON_TYPE_ALL)) {
                if ("true".equalsIgnoreCase(couponadded)) {
                    claimBtn.setBackgroundResource(R.drawable.claimcoupon);
                    redeemBtn.setBackgroundResource(R.drawable.redeemcoupon);
                    isCliped = true;
                } else {
                    claimBtn.setBackgroundResource(R.drawable.claimcoupons_down);
                    redeemBtn.setBackgroundResource(R.drawable.redeemcoupon_down);

                }
            }
            findViewById(R.id.coupon_terms_conditions).setOnClickListener(this);
            findViewById(R.id.location).setOnClickListener(this);
            claimBtn = (Button) findViewById(R.id.clip_coupon);

            claimBtn.setOnClickListener(this);
            redeemBtn = (Button) findViewById(R.id.btn_redeem);
            redeemBtn.setOnClickListener(this);
            location = (Button) findViewById(R.id.location);
            webview = (WebView) findViewById(R.id.webView);
            location.setText("Locations");

            if (getIntent().hasExtra(CommonConstants.TAB_SELECTED)) {
                tabSelected = getIntent().getExtras().getString(
                        CommonConstants.TAB_SELECTED);
            }
            couponName = (TextView) findViewById(R.id.coupon_productname);
            progressBar = (ProgressBar) findViewById(R.id.progress);
            couponstartdate = (TextView) findViewById(R.id.coupon_product_startdate);
            couponexpiredate = (TextView) findViewById(R.id.coupon_product_ExpireDate);
            couponimagepath = (ImageView) findViewById(R.id.coupon_info_image);
            showLoading();
            fetchCouponDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent.hasExtra("isCoupon")) {
            isCoupon = intent.getExtras().getBoolean("isCoupon");
        }
        if (intent.hasExtra("couponId")) {
            couponId = intent.getExtras().getString("couponId");
        }
        if (getIntent().hasExtra("couponListId")) {
            couponListId = intent.getExtras().getString("couponListId");
            ;
        }
        if (getIntent().hasExtra("retailerId")) {
            retailerId = intent.getExtras().getString("retailerId");
        }
    }

    @Override
    public void finish() {
        if (!isMainmenuBtnClicked) {
            Intent updateIntent = new Intent();
            updateIntent.putExtra("isCliped", isCliped);
            updateIntent.putExtra("isUsed", isUsed);
            updateIntent.putExtra("couponId", couponId);
            if (tabSelected != null) {
                updateIntent
                        .putExtra(CommonConstants.TAB_SELECTED, tabSelected);
            }
            setResult(RESULT_OK, updateIntent);
        }
        super.finish();
    }

    private void initiateLocationWindow(
            final ArrayList<HashMap<String, String>> listData) {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) CouponsDetailActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater
                    .inflate(
                            R.layout.events_location_popup,
                            (ViewGroup) findViewById(R.id.events_location_popup_element));

            DisplayMetrics metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            layout.setBackgroundColor(Color.WHITE);
            int screenHeight = metrics.heightPixels;
            int scrHeight = screenHeight / 4;

            locPwindo = new PopupWindow(layout, screenWidth - 100, scrHeight * 2,
                    true);
            locPwindo.showAtLocation(layout, Gravity.BOTTOM, 0, 82);

            couponsproductListView = (ListView) layout
                    .findViewById(R.id.events_location_listview);
            TextView textTitle = (TextView) layout.findViewById(R.id.events_location_popup_title);
            textTitle.setText("Locations");

            couponsretailListAdapter = new CouponsRetailListAdapter(
                    CouponsDetailActivity.this, couponsLocationsList);
            couponsproductListView.setAdapter(couponsretailListAdapter);
            couponsproductListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                    if ("product".equals(type)) {

                        Intent navIntent = new Intent(
                                CouponsDetailActivity.this,
                                RetailerCurrentsalesActivity.class);
                        navIntent
                                .putExtra(
                                        CommonConstants.TAG_CURRENTSALES_PRODUCTID,
                                        couponsproductsList
                                                .get(position)
                                                .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
                        navIntent
                                .putExtra(
                                        CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
                                        couponsproductsList
                                                .get(position)
                                                .get(CommonConstants.TAG_CURRENTSALES_PRODUCTNAME));

                        navIntent.putExtra("Coupons", true);
                        startActivityForResult(navIntent, 106);

                    } else if ("location".equals(type)) {

                            Intent intent = new Intent(
                                    CouponsDetailActivity.this,
                                    RetailerActivity.class);

                            if (null != couponsLocationsList.get(position).get(
                                    "retailerId")) {
                                intent.putExtra(
                                        CommonConstants.TAG_RETAIL_ID,
                                        couponsLocationsList.get(position).get(
                                                "retailerId"));
                            }

                            if (null != couponsLocationsList.get(position).get(
                                    "retListId")) {
                                intent.putExtra(
                                        CommonConstants.TAG_RETAIL_LIST_ID,
                                        couponsLocationsList.get(position).get(
                                                "retListId"));
                            }
                            if (null != couponsLocationsList.get(position).get(
                                    "retailLocationId")) {
                                intent.putExtra(
                                        Constants.TAG_RETAILE_LOCATIONID,
                                        couponsLocationsList.get(position).get(
                                                "retailLocationId"));
                            }

                            if (null != couponsLocationsList.get(position).get(
                                    "retailerName")) {
                                intent.putExtra(
                                        Constants.TAG_APPSITE_NAME,
                                        couponsLocationsList.get(position).get(
                                                "retailerName"));
                            }

                            startActivity(intent);


                    }
                }
            });
            btnClosePopup = (Button) layout
                    .findViewById(R.id.btn_location_close_popup);
            btnClosePopup.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    locPwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<RetailerBo> getMapDetails(ArrayList<HashMap<String, String>> couponsLocationsList) {
        ArrayList<RetailerBo> mRetailerList = new ArrayList<>();
        int size = couponsLocationsList.size();
        for (int i = 0; i < size; i++) {
            String retailerName = couponsLocationsList.get(i).get("retailerName");
            String retailAddress = couponsLocationsList.get(i).get("completeAddress");
            String latitude = couponsLocationsList.get(i).get("latitude");
            String longitude = couponsLocationsList.get(i).get("longitude");
            String retailId = couponsLocationsList.get(i).get("retailerId");
            String retListId = couponsLocationsList.get(i).get("retListId");
            String locationID = couponsLocationsList.get(i).get("retailLocationId");
            String bannerAd = couponsLocationsList.get(i).get("logoImagePath");
            RetailerBo retailerObject = new RetailerBo(null, retailId,
                    retailerName, locationID, null, retailAddress, null, null,
                    bannerAd, null, null, null, latitude,
                    longitude, retListId);
            mRetailerList.add(retailerObject);
        }

        return mRetailerList;

    }


    Button btnClosePopup, btnRedeem;

    private void initiatePopupWindow() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) CouponsDetailActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.coupons_screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));

            layout.setBackgroundColor(Color.WHITE);

            DisplayMetrics metrics = new DisplayMetrics();
            CouponsDetailActivity.this.getWindowManager().getDefaultDisplay()
                    .getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            int screenHeight = metrics.heightPixels;
            int scrHeight = screenHeight / 5;
            pwindo = new PopupWindow(layout, screenWidth - 100, scrHeight * 2,
                    false);
            pwindo.showAtLocation(layout, Gravity.BOTTOM, 0, 82);

            btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);
            btnRedeem = (Button) layout.findViewById(R.id.btn_redeem_popup);

            TextView txtView = (TextView) layout
                    .findViewById(R.id.txtTermsConds);
            txtTermsConditions = (TextView) layout
                    .findViewById(R.id.txtTerms);

            txtView.setTextColor(Color.BLACK);
            txtTermsConditions.setTextColor(Color.BLACK);

            txtView.setText("Terms and Conditions");
            txtTermsConditions.setText(termsAndConditionsDesc);

            btnClosePopup.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.location:
                new CouponsLocations().execute();
                break;
            case R.id.coupon_terms_conditions:
                initiatePopupWindow();
                break;
            case R.id.clip_coupon:
                clipCoupon();
                break;
            case R.id.btn_redeem:
                btnRedeem();
                break;
            default:
                break;
        }

    }


    private void clipCoupon() {
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if ("0".equals(usedFlag)) {
                showLoading();
                addCouponDetails();
            }
        } else {
            Constants.SignUpAlert(CouponsDetailActivity.this);
        }
    }

    private void btnRedeem() {
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if ("1".equals(usedFlag)) {
                showLoading();
                fetchRedeemDetails();
            }
        } else {
            Constants.SignUpAlert(CouponsDetailActivity.this);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            this.setResult(Constants.FINISHVALUE);
            isMainmenuBtnClicked = true;
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void addCouponDetails() {
        AddCouponDetails request = UrlRequestParams.getAddCouponsBusiness(couponId);
        mRestClient.getAddCouponDetails(request, new Callback<GetAddCouponObject>() {


            @Override
            public void success(GetAddCouponObject getAddCouponObject, Response response) {

                String responseCode = getAddCouponObject.getResponseCode();
                try {
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (responseCode.equalsIgnoreCase("10000")) {
                    claimBtn.setEnabled(false);
                    redeemBtn.setEnabled(true);
                    isCliped = true;
                    usedFlag = "1";

                } else {
                    isCliped = false;
                    Toast.makeText(getApplicationContext(),
                            "Please try again later.", Toast.LENGTH_SHORT).show();
                    claimBtn.setEnabled(true);
                    redeemBtn.setEnabled(false);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                try {
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    boolean loadingFinished = true;

    public class MyWebClient extends WebViewClient {


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.e("tag", "onReceivedError" + description);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // webProgress.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
            loadingFinished = false;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (webview == null) {
                webview = view;
            }
            // This checking is to load the previous page on back button
            // if (mCurrentUrl != null && url != null && url.equals(mCurrentUrl)) {
            if (url != null) {
                webview.goBack();
                if (url.startsWith("tel:")) {
                    Intent intent = new Intent(
                            Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                } else if (url.startsWith("mailto:")) {
                    Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
                    startActivity(i);
                } else if (url.startsWith("http:") || url.startsWith("https:")) {
                    Intent intent = new Intent(
                            CouponsDetailActivity.this,
                            ScanseeBrowserActivity.class);
                    intent.putExtra(CommonConstants.URL, url);
                    startActivity(intent);
                }
                return true;
            }

            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (!redirect) {
                loadingFinished = true;
            }

            if (!loadingFinished && redirect) {

                redirect = false;
            }

            // webProgress.setVisibility(View.GONE);

        }
    }


    private class CouponsLocations extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;
        JSONObject responseMenuObject = null;
        JSONArray jsonArrayLocationDetail = null;
        private ProgressDialog mDialog;

        UrlRequestParams objUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(CouponsDetailActivity.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            couponsLocationsList = new ArrayList<>();
            String result = "true";
            type = "location";
            try {
                String urlParameters = objUrlRequestParams
                        .getQualifyingCoupDetails(couponId, type, couponListId);

                String url_get_coup_prod_or_loc = Properties.url_local_server
                        + Properties.hubciti_version
                        + "gallery/getcouponproductorlocation";
                jsonObject = mServerConnections.getUrlPostResponse(
                        url_get_coup_prod_or_loc, urlParameters, true);
                if (jsonObject != null) {

                    try {
                        JSONObject productDetail = jsonObject.getJSONObject(
                                "CouponsDetails").getJSONObject(
                                CommonConstants.ALLCOUPONSRETAILERDETAIL);

                        jsonArrayLocationDetail = new JSONArray();
                        jsonArrayLocationDetail.put(productDetail);
                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonArrayLocationDetail = jsonObject.getJSONObject(
                                "CouponsDetails").getJSONArray(
                                CommonConstants.ALLCOUPONSRETAILERDETAIL);
                    }

                    for (int arrayCount = 0; arrayCount < jsonArrayLocationDetail
                            .length(); arrayCount++) {
                        HashMap<String, String> couponsData = new HashMap<>();

                        responseMenuObject = jsonArrayLocationDetail
                                .getJSONObject(arrayCount);

                        if (responseMenuObject.has("retailerName")) {
                            couponsData.put("retailerName", responseMenuObject
                                    .getString("retailerName"));
                        }
                        couponsData.put("latitude", responseMenuObject
                                .optString("latitude"));
                        couponsData.put("longitude", responseMenuObject
                                .optString("longitude"));

                        if (responseMenuObject.has("rowNumber")) {
                            couponsData.put("rowNumber",
                                    responseMenuObject.getString("rowNumber"));
                        }

                        if (responseMenuObject.has("retailerId")) {
                            couponsData.put("retailerId",
                                    responseMenuObject.getString("retailerId"));
                        }

                        if (responseMenuObject.has("retailLocationId")) {
                            couponsData.put("retailLocationId",
                                    responseMenuObject
                                            .getString("retailLocationId"));
                        }

                        if (responseMenuObject.has("retListId")) {
                            couponsData.put("retListId",
                                    responseMenuObject.getString("retListId"));
                        }

                        couponsData
                                .put("completeAddress", responseMenuObject
                                        .getString("completeAddress"));
                        couponsData.put("logoImagePath",
                                responseMenuObject.getString("logoImagePath"));

                        couponsLocationsList.add(couponsData);
                    }
                    result = "true";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            mDialog.dismiss();
            try {
                progDialog.dismiss();

                if ("true".equals(result)) {
                    if(isCoupon) {
                        initiateLocationWindow(couponsLocationsList);
                    }
                    else
                    {
                        CommonConstants.startMapScreen(mCouponsActivity, getMapDetails(couponsLocationsList), false);
                    }

                } else {
                    AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
                            CouponsDetailActivity.this);
                    notificationAlert.setMessage(getString(R.string.norecord))
                            .setPositiveButton(getString(R.string.specials_ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            dialog.cancel();
                                        }
                                    });

                    notificationAlert.create().show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void fetchCouponDetails() {

        CouponDetails request = UrlRequestParams.getCouponsDetails(couponId, couponListId);
        mRestClient.getCouponDetails(request, new Callback<GetCouponDetailsObject>() {

            @Override
            public void success(GetCouponDetailsObject getCouponDetailsObject, Response response) {
                String responseCode = getCouponDetailsObject.getResponseCode();
                try {
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (responseCode.equalsIgnoreCase("10000")) {
                    title.setText(getCouponDetailsObject.getCouponName());
                    couponName.setText(getCouponDetailsObject.getBannerName());
                    couponstartdate.setText(getCouponDetailsObject.getCouponStartDate());
                    termsAndConditionsDesc = getCouponDetailsObject.getTermAndConditions();
                    usedFlag = getCouponDetailsObject.getUsedFlag();
                    expiredFlag = getCouponDetailsObject.getExpireFlag();
                    if (getCouponDetailsObject.getCouponExpireDate() != null) {
                        couponexpiredate.setVisibility(View.VISIBLE);
                        couponexpiredate.setText("Expires On:" + " " + getCouponDetailsObject.getCouponExpireDate());
                    } else {
                        couponexpiredate.setVisibility(View.GONE);
                    }
                    if (getCouponDetailsObject.getCouponImagePath() != null) {

                        new CommonMethods().loadImage(mCouponsActivity, progressBar, getCouponDetailsObject.getCouponImagePath(), couponimagepath);

                    }
                    String description = "<body><center>" + getCouponDetailsObject.getCouponDesc() + "</center></body>";

                    System.out.println("content" + description);


                    WebSettings settings = webview.getSettings();

                    settings.setJavaScriptEnabled(true);

                    webview.setWebViewClient(new MyWebClient());
                    webview.setWebChromeClient(new WebChromeClient() {
                        @Override
                        public void onProgressChanged(WebView view, int newProgress) {
                            super.onProgressChanged(view, newProgress);
                        }
                    });

                    webview.loadDataWithBaseURL("", description, "text/html", "utf-8", "");

                    if (getCouponDetailsObject.getTermAndConditions() == null) {
                        findViewById(R.id.coupon_terms_conditions).setVisibility(
                                View.GONE);
                    } else {
                        txtTermsnConds = (TextView) findViewById(R.id.coupon_terms_conditions);
                        txtTermsnConds.setVisibility(View.VISIBLE);
                        txtTermsnConds.setTypeface(null, Typeface.BOLD);
                        txtTermsnConds.setText("Terms and Conditions");
                    }


                    if (getCouponDetailsObject.getLocatnFlag().equals("1")) {
                        findViewById(R.id.location).setVisibility(View.VISIBLE);
                    } else {
                        findViewById(R.id.location).setVisibility(View.GONE);
                    }

                    if ("0".equals(expiredFlag)) {


                        if (getCouponDetailsObject.getUsedFlag().equals("0")) {
                            claimBtn.setEnabled(true);
                            redeemBtn.setEnabled(false);
                        } else if (getCouponDetailsObject.getUsedFlag().equals("1")) {
                            claimBtn.setEnabled(false);
                            redeemBtn.setEnabled(true);
                        } else if (getCouponDetailsObject.getUsedFlag().equals("2")) {
                            claimBtn.setEnabled(false);
                            redeemBtn.setEnabled(false);
                        }
                    } else if ("1".equals(expiredFlag)) {
                        claimBtn.setEnabled(false);
                        redeemBtn.setEnabled(false);
                        leftTitleImage.setEnabled(true);
                    }

                }

            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void showLoading() {
        progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();
    }

    private void fetchRedeemDetails() {
        RedeemGetDetails request = UrlRequestParams.getRedeemBusiness(couponId);
        mRestClient.getRedeemDetails(request, new Callback<GetRedeemObject>() {

            @Override
            public void success(GetRedeemObject getRedeemObject, Response response) {
                String responseCode = getRedeemObject.getResponseCode();
                try {
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (responseCode.equalsIgnoreCase("10000")) {
                    redeemBtn.setEnabled(false);
                    isUsed = true;
                    CommonConstants.displayToast(mCouponsActivity, getRedeemObject.getResponseText());
                }

            }

            @Override
            public void failure(RetrofitError error) {

                try {
                    if (progDialog != null && progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
