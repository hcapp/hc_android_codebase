package com.scansee.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.scansee.hubregion.R;
import com.google.zxing.client.android.ScanNowActivity;
import com.hubcity.android.commonUtil.Constants;

public class ScanNowSplashActivity extends Activity {
	String mItemId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scannow_splash);

		if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
			mItemId = getIntent().getExtras().getString(
					Constants.MENU_ITEM_ID_INTENT_EXTRA);
		} else if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
			mItemId = getIntent().getExtras().getString(
					Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
		}

		Thread mSplashThread = new Thread() {
			@Override
			public void run() {

				Intent intent = new Intent(ScanNowSplashActivity.this,

						ScanNowActivity.class);
				try {
					synchronized (this) {
						// Wait given period of time or exit on touch
						wait(1000);
					}
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}

				intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
				startActivityForResult(intent, Constants.STARTVALUE);
				finish();

			}
		};
		mSplashThread.start();

	}

	@Override
	protected void onResume() {
		super.onResume();
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.scannow_splash);

	}
}
