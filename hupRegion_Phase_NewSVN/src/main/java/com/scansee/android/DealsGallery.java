package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

/**
 * This Screen contains deals gallery items
 * 
 * @author rekha_p
 * 
 */
public class DealsGallery extends CustomTitleBar {

	private static String returnValue = "sucessfull";

	// List Holding gallery data
	@SuppressWarnings("Convert2Diamond")
	ArrayList<HashMap<String, String>> galleryDataList = new ArrayList<>();

	DealsGalleryAsyncTask galleryAsyncTask;
	ProgressDialog progDialog;

	// For Bottom Buttons
	LinearLayout linearLayout = null;
	BottomButtons bb = null;
	Activity activity;
	boolean hasBottomBtns = false;
	boolean enableBottomButton = true;

	// Lists holding deals Gallery data
	ListView dealsGalleryList;
	DealsGalleryAdapter dealsGalleryAdapter;

	boolean isFirst;
	boolean isSubMenu;

	String type = "";

	// Colors for the List
	String mColor = "";
	String mFontColor = "";
	String mBackGrdColor = "";
	String mItemId = "";
	String mBottomId = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deals_cat);

		try {
			isFirst = true;
			activity = DealsGallery.this;
			// Initiating Bottom button class
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "");

			linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
			linearLayout.setVisibility(View.INVISIBLE);

			if (getIntent().hasExtra("isSubMenu")) {
                isSubMenu = getIntent().getExtras().getBoolean("isSubMenu");
            }

			if (getIntent().hasExtra("type")) {
                type = getIntent().getExtras().getString("type");
            }

			title.setText(type);

			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }

			if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
                mBottomId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }

			dealsGalleryList = (ListView) findViewById(R.id.deals_menu);
			dealsGalleryList.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {
                    selectedOption(position);
                }
            });

			// Back button click action
			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTask();
                    finish();
                }
            });

			callDealsGallery();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * On item click action
	 */
	private void selectedOption(int position) {
		String name = galleryDataList.get(position).get("Name");

		if ("Deals".equalsIgnoreCase(name)) {
			dealsGallery(type);

		} else if ("Coupons".equalsIgnoreCase(name)) {
			couponsGallery(type);
		}

		finish();
	}

	/*
	 * Call to AsyncTask
	 */
	private void callDealsGallery() {
		if (isFirst) {
			galleryAsyncTask = new DealsGalleryAsyncTask();
			galleryAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;
		}

		if (galleryAsyncTask != null) {
			if (!galleryAsyncTask.isCancelled()) {
				galleryAsyncTask.cancel(true);

			}

			galleryAsyncTask = null;

			galleryAsyncTask = new DealsGalleryAsyncTask();
			galleryAsyncTask.execute();
		}
	}

	/*
	 * Cancel AsyncTask on navigating to different screen
	 */
	private void cancelAsyncTask() {
		if (galleryAsyncTask != null && !galleryAsyncTask.isCancelled()) {
			galleryAsyncTask.cancel(true);
		}

		galleryAsyncTask = null;
	}

	/**
	 * Background process to get the Deals gallery items
	 * 
	 * @author rekha_p
	 * 
	 */
	public class DealsGalleryAsyncTask extends AsyncTask<String, Void, String> {
		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();


		String responseText = "";
		String responseCode = "";

		@Override
		protected void onPreExecute() {
			progDialog = new ProgressDialog(DealsGallery.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage(Constants.DIALOG_MESSAGE);
			progDialog.setCancelable(false);
			progDialog.show();

		}

		@SuppressWarnings("Convert2Diamond")
		@Override
		protected String doInBackground(String... params) {
			try {
				
				String deals_gallery_display = Properties.url_local_server
						+ Properties.hubciti_version + "hotdeals/gallerydisplay";
				
				String urlParameters = mUrlRequestParams
						.createDealsGalleryParameter(isSubMenu, type);
				JSONObject jsonResponse = mServerConnections
						.getUrlPostResponse(deals_gallery_display,
								urlParameters, true);

				// If response is Success
				if (jsonResponse != null) {

					JSONObject responseJson;

					// If the response has Deals data
					if (jsonResponse.has("Data")) {

						responseJson = jsonResponse.getJSONObject("Data");

						if (responseJson.has("responseCode")) {
							responseCode = responseJson
									.getString("responseCode");
						}

						if (responseJson.has("responseText")) {
							responseText = responseJson
									.getString("responseText");
						}

						if (responseJson.has("mColor")) {
							mColor = responseJson.getString("mColor");
						}

						if (responseJson.has("mFontColor")) {
							mFontColor = responseJson.getString("mFontColor");
						}

						if (responseJson.has("mBackGrdColor")) {
							mBackGrdColor = responseJson
									.getString("mBackGrdColor");
						}

						// If there are bottom buttons
						if (responseJson.has("bottomBtn")
								&& "1".equals(responseJson
										.getString("bottomBtn"))) {

							hasBottomBtns = true;
							JSONObject jsonBBObject;

							jsonBBObject = responseJson
									.getJSONObject("arBottomBtnList");

							if (jsonBBObject.has("BottomButton")) {

								try {
									ArrayList<BottomButtonBO> bottomButtonList = bb
											.parseForBottomButton(jsonBBObject);
									BottomButtonListSingleton
											.clearBottomButtonListSingleton();
									BottomButtonListSingleton
											.getListBottomButton(bottomButtonList);

								} catch (JSONException e1) {
									e1.printStackTrace();
								}

							}

						} else {
							hasBottomBtns = false;
						}

						// If there are deals list
						if (responseJson.has("dealList")) {
							JSONObject dealsJsonObject = responseJson
									.getJSONObject("dealList");
							JSONArray dealsListJson = new JSONArray();

							try {
								dealsListJson.put(dealsJsonObject
										.getJSONObject("Deal"));
							} catch (Exception e) {
								e.printStackTrace();
								dealsListJson = dealsJsonObject
										.getJSONArray("Deal");
							}

							for (int arrayCount = 0; arrayCount < dealsListJson
									.length(); arrayCount++) {
								HashMap<String, String> dealsData = new HashMap<>();
								if (dealsListJson.getJSONObject(arrayCount)
										.has("Name")) {
									dealsData.put("Name", dealsListJson
											.getJSONObject(arrayCount)
											.getString("Name"));
								}

								if (dealsListJson.getJSONObject(arrayCount)
										.has("imagePath")) {
									dealsData.put("imagePath", dealsListJson
											.getJSONObject(arrayCount)
											.getString("imagePath"));
								}

								if (dealsListJson.getJSONObject(arrayCount)
										.has("flag")) {
									dealsData.put("flag", dealsListJson
											.getJSONObject(arrayCount)
											.getString("flag"));
								}

								galleryDataList.add(dealsData);
							}
						}

					} else {
						// If the response failed
						if (jsonResponse.has("response")) {
							responseJson = jsonResponse
									.getJSONObject("response");
							if (responseJson.has("responseCode")) {
								responseCode = responseJson
										.getString("responseCode");
							}

							if (responseJson.has("responseText")) {
								responseText = responseJson
										.getString("responseText");
							}
						}
					}
				} else {
					returnValue = "UNSUCCESS";
					responseText = "Error";
				}

			} catch (Exception ex) {
				ex.printStackTrace();
				returnValue = "UNSUCCESS";
				return returnValue;
			}

			return returnValue;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				if ("sucessfull".equals(returnValue)) {

                    if (galleryDataList != null && !galleryDataList.isEmpty()) {

                        // if (galleryDataList.size() > 1) {

                        dealsGalleryAdapter = new DealsGalleryAdapter(
                                DealsGallery.this, galleryDataList);
                        dealsGalleryList.setAdapter(dealsGalleryAdapter);

                        // } else {
                        // selectedOption(0);
                        // finish();
                        // }
                    }

                } else {
                    // Display Alert if there are no deals data
                    progDialog.dismiss();

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            DealsGallery.this);

                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setMessage(responseText);
                    alertDialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which) {

                                    dialog.dismiss();
                                    cancelAsyncTask();
                                    finish();
                                }
                            });
                    alertDialogBuilder.show();

                }

				if (hasBottomBtns && enableBottomButton) {

                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) dealsGalleryList
                            .getLayoutParams();
                    mlp.setMargins(0, 0, 0, 2);

                    bb.createbottomButtontTab(linearLayout,false);
                    enableBottomButton = false;
                }

				if (mBackGrdColor != null && !"".equals(mBackGrdColor)
                        && !"N/A".equalsIgnoreCase(mBackGrdColor)) {
                    linearLayout
                            .setBackgroundColor(Color.parseColor(mBackGrdColor));
                } else {
                    linearLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }

				linearLayout.setVisibility(View.VISIBLE);

				if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * Navigates to Deals Used/Expired/Claimed items
	 */
	private void dealsGallery(String type) {

		if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
			Intent navIntent = new Intent(activity,
					HotDealClaimRedeemExpiredActivty.class);
			navIntent.putExtra(CommonConstants.TAB_SELECTED, type);

			if (mItemId != null && !"".equals(mItemId)) {
				navIntent
						.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
			} else if (mBottomId != null && !"".equals(mBottomId)) {
				navIntent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
						mBottomId);
			}

			navIntent.putExtra("isSubmenu", isSubMenu);
			navIntent.putExtra("type", this.type);

			activity.startActivity(navIntent);
		} else {
			Constants.SignUpAlert(DealsGallery.this);
		}

	}

	/*
	 * Navigates to Coupons Claimed/Used/Expired items
	 */
	private void couponsGallery(String type) {

		if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {

			Intent navIntent = new Intent(activity,
					CouponsClaimedUsedExpiredActivty.class);
			navIntent.putExtra(CommonConstants.TAB_SELECTED, type);
			navIntent.putExtra("isSubmenu", isSubMenu);
			navIntent.putExtra("type", this.type);

			if (mItemId != null && !"".equals(mItemId)) {
				navIntent
						.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
			} else if (mBottomId != null && !"".equals(mBottomId)) {
				navIntent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
						mBottomId);
			}

			activity.startActivity(navIntent);

		} else {
			Constants.SignUpAlert(DealsGallery.this);
		}

	}

	// Adapter to deals Gallery items
	class DealsGalleryAdapter extends BaseAdapter {
		Activity mContext;
		ArrayList<HashMap<String, String>> mlist;
		CustomImageLoader customImageLoader;

		DealsGalleryAdapter(Activity context,
				ArrayList<HashMap<String, String>> list) {
			mContext = context;
			mlist = list;
			customImageLoader = new CustomImageLoader(context, false);
		}

		class ViewHolder {
			ImageView icon;
			ImageView disclosureIcon;
			TextView text;
			LinearLayout menuListTemplateParent;
			protected LinearLayout textViewLayout;
		}

		@SuppressLint("ViewHolder")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder mViewHolder;

			String imagePath = mlist.get(position).get("imagePath");
			String listText = mlist.get(position).get("Name");

			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.template_list_menu_row,
					parent, false);

			mViewHolder = new ViewHolder();

			if (listText != null && !"".equals(listText)) {

				mViewHolder.textViewLayout = (LinearLayout) convertView
						.findViewById(R.id.textView_layout);
				mViewHolder.textViewLayout
						.setLayoutParams(new LinearLayout.LayoutParams(
								LayoutParams.MATCH_PARENT,
								LayoutParams.WRAP_CONTENT));

				mViewHolder.icon = (ImageView) convertView
						.findViewById(R.id.imageView_row_icon);
				mViewHolder.icon.setVisibility(View.VISIBLE);

				mViewHolder.text = (TextView) convertView
						.findViewById(R.id.tv_row_header);

				mViewHolder.menuListTemplateParent = (LinearLayout) convertView
						.findViewById(R.id.menu_list_template_parent);

				convertView.setTag(mViewHolder);

				mViewHolder.text.setText(listText);

				if (mFontColor != null && !"".equals(mFontColor)
						&& !"N/A".equalsIgnoreCase(mFontColor)) {

					mViewHolder.text.setTextColor(Color.parseColor(mFontColor));
				} else {
					mViewHolder.text.setTextColor(Color.WHITE);
				}

				if (mColor != null && !"".equals(mColor)
						&& !"N/A".equalsIgnoreCase(mColor)) {
					mViewHolder.menuListTemplateParent.setBackgroundColor(Color
							.parseColor(mColor));

				} else {
					mViewHolder.menuListTemplateParent
							.setBackgroundColor(Color.BLACK);

				}

				if (imagePath != null && !"".equals(imagePath)) {

					customImageLoader.displayImage(imagePath, mContext,
							mViewHolder.icon);

				} else {
					mViewHolder.icon.setVisibility(View.GONE);
				}
			}

			return convertView;
		}

		@Override
		public int getCount() {
			return mlist.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cancelAsyncTask();
	}

	@Override
	protected void onResume() {
		super.onResume();
		try {
			// Add screen name when needed
			bb.setActivityInfo(activity, "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.i("", "OnPause");

		try {

			if (progDialog != null && progDialog.isShowing()) {
				progDialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
