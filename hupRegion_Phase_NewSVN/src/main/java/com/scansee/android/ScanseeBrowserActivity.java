package com.scansee.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.ShareInformation;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class ScanseeBrowserActivity extends CustomTitleBar implements
        OnClickListener {

    ContentAsyncTask contentAsyncTask;
    WebView webView;
    String urL;
    ProgressBar webProgress;
    Button backBtn1, fwdBtn, reloadBtn, stopBtn;
    boolean frommuPdf;
    ShareInformation shareInfo;
    boolean isShareTaskCalled;
    boolean isEventLogistics, isFromSideNav;
    boolean redirect = false;
    boolean canWebviewGoBack;
    private ValueCallback mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE = 1;
    private final static int REQUEST_SELECT_FILE=2;
    private ProgressDialog mDialog;
    private Activity activity;

    String gpsAlertMessage = "Event Logistics uses your Phone's Location Services to locate you " +
            "on" +

            " the map. "
            + "Please turn on Location Services in Settings";
    //Added for File chooser in webview
    boolean isMaps = false;

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            this.setResult(Constants.FINISHVALUE);
            finish();
        }
        if (requestCode == 3000) {

            finish();
        }

        if (requestCode == REQUEST_SELECT_FILE) {
            if (mUploadMessage == null) return;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mUploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
            }
            mUploadMessage = null;
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage)
                return;

            Uri result = data == null || resultCode != RESULT_OK ? null : data
                    .getData();
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint({"SetJavaScriptEnabled", "InlinedApi", "NewApi"})
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = ScanseeBrowserActivity.this;
        try {
            String module = "";
            if (getIntent().hasExtra("module")
                    && getIntent().getExtras().getString("module") != null
                    && !"".equals(getIntent().getExtras().getString("module"))) {

                module = getIntent().getExtras().getString("module");
            } else {
                module = "Anything";
            }

            if (getIntent().hasExtra("isMap")
                    && getIntent().getExtras().getBoolean("isMap")) {
                isMaps = getIntent().getExtras().getBoolean("isMap");
            }
            if (getIntent().hasExtra("isFromSideNav")) {
                isFromSideNav = getIntent().getExtras().getBoolean("isFromSideNav");
            }

            // To Check if the URL is coming from Pdf
            if (getIntent().hasExtra("frommupdfInternal")) {
                frommuPdf = getIntent().getExtras().getBoolean("frommupdfInternal");
            }

            urL = getIntent().getExtras().getString(CommonConstants.URL);
            Log.d("initial urL : ", urL);

            setContentView(R.layout.scansee_browser);

            webView = (WebView) findViewById(R.id.hubciti_webview);
            WebSettings settings = webView.getSettings();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }

            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setJavaScriptEnabled(true);
            //Added  for File chooser to work
            settings.setAllowFileAccess(true);
            if (isMaps) {
                enableGeoLoc(settings);
            }

            // We have used this to resolve redirection issue i.e.If we use, this
            // allow ANY
            // website
            // that takes advantage of DOM storage to use said storage options on
            // the device. It is disabled by default for space
            // savings and security.
            settings.setDomStorageEnabled(true);
            if (frommuPdf) {
                settings.setBuiltInZoomControls(true);
            }
            webView.setWebViewClient(new MyWebClient());
            webProgress = (ProgressBar) findViewById(R.id.progressBar_browser);
            webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onProgressChanged(WebView view, int newProgress) {
                    super.onProgressChanged(view, newProgress);
                    webProgress.setProgress(newProgress);
                }
                //Added for file chooser to work

                protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                    mUploadMessage = uploadMsg;
                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    i.setType("*/*");
                    startActivityForResult(Intent.createChooser(i, "File Chooser"),
                            FILECHOOSER_RESULTCODE);
                }

                // For Android 3.0+
                public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                    mUploadMessage = uploadMsg;
                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.addCategory(Intent.CATEGORY_OPENABLE);
                    i.setType("*/*");
                    ScanseeBrowserActivity.this.startActivityForResult(Intent.createChooser(i, "File Browser"),
                            FILECHOOSER_RESULTCODE);
                }

                //For Android 4.1
                protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType,
                                               String capture) {
                    mUploadMessage = uploadMsg;
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    startActivityForResult(Intent.createChooser(intent, "File Browser"),
                            FILECHOOSER_RESULTCODE);
                }

                // For Lollipop 5.0+ Devices
                public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]>
                        filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                    if (mUploadMessage != null) {
                        mUploadMessage.onReceiveValue(null);
                        mUploadMessage = null;
                    }

                    mUploadMessage = filePathCallback;
                    Intent intent = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        intent = fileChooserParams.createIntent();
                    }
                    try {
                        startActivityForResult(intent, REQUEST_SELECT_FILE);
                    } catch (ActivityNotFoundException e) {
                        mUploadMessage = null;
                        Toast.makeText(getApplicationContext(), "Cannot Open File Chooser", Toast
                                .LENGTH_LONG).show();
                        return false;
                    }
                    return true;
                }
            });
            if (!urL.contains("://")) {
                if (urL.contains("www.")) {
                    urL = "http://" + urL;
                } else {
                    urL = "http://www." + urL;
                }

            }

            // If the url contains play store link then opening the particular app
            if (urL.contains("play.google.com/store/apps/")) {
                String packageName = urL.split("=")[1];
                Constants.openCustomUrl(packageName, ScanseeBrowserActivity.this);
                finish();
            } else {
                webView.loadUrl(urL);
            }
            if (UrlRequestParams.getUid().equals("-1")) {
                leftTitleImage.setVisibility(View.GONE);
                rightImage.setVisibility(View.GONE);
            }

            if (getIntent().hasExtra("isEventLogistics")
                    && getIntent().getExtras().getBoolean("isEventLogistics")) {
                isEventLogistics = true;
                title.setText("Maps / Event Logistics");
                title.setMaxLines(1);
                checkGPSConnection();
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
                }

                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {

                    enableGeoLoc(settings);
                }

            } else {
                isEventLogistics = false;
                title.setText("Details");
                title.setEllipsize(TruncateAt.END);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            if (getIntent().hasExtra("module")
                    && getIntent().getStringExtra("module").equalsIgnoreCase("news")) {
                setColor();
            }
            //Forcing the screen to open in landscape on the basis of below checking
            if (urL.contains("3000.htm") || urL.contains("2410.htm")) {

                Log.d("urL loaded: ", urL);
                LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                }
                if (!gps_enabled) {
                    new AlertDialog.Builder(this).setTitle("")
                            .setMessage(gpsAlertMessage).setPositiveButton("OK", new DialogInterface
                            .OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                            .show();
                }
                enableGeoLoc(settings);
                if (urL.contains("oType=Landscape")) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else if (urL.contains("oType=Portrait")) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }

            }
            if (getIntent().hasExtra(CommonConstants.TAG_PAGE_TITLE)
                    && getIntent().getExtras().getString(
                    CommonConstants.TAG_PAGE_TITLE) != null
                    && !"".equals(getIntent().getExtras().getString(
                    CommonConstants.TAG_PAGE_TITLE))) {
                if (getIntent().getStringExtra(CommonConstants.TAG_PAGE_TITLE)
                        .equals("Forget Password")) {
                    rightImage.setVisibility(View.GONE);
                    settings.setBuiltInZoomControls(true);
                }
                title.setText(getIntent().getStringExtra(
                        CommonConstants.TAG_PAGE_TITLE));
            }

            if (getIntent().hasExtra("isShare")
                    && getIntent().getExtras().getBoolean("isShare")) {

                // Call for Share Information
                shareInfo = new ShareInformation(ScanseeBrowserActivity.this,
                        getIntent().getExtras().getString("retailerId"),
                        getIntent().getExtras().getString("retailLocationId"),
                        getIntent().getExtras().getString("pageId"), module);
                isShareTaskCalled = false;

                leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
                leftTitleImage.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!Constants.GuestLoginId.equals(UrlRequestParams
                                .getUid().trim())) {
                            // CityPreferencesScreen.bIsSendingTimeStamp =
                            // false;
                            shareInfo.shareTask(isShareTaskCalled);
                            isShareTaskCalled = true;
                        } else {
                            Constants.SignUpAlert(ScanseeBrowserActivity.this);
                            isShareTaskCalled = false;
                        }
                    }
                });
            } else {
                leftTitleImage.setVisibility(View.GONE);

            }

            backBtn1 = (Button) findViewById(R.id.browser_btn_back);
            fwdBtn = (Button) findViewById(R.id.browser_btn_foward);
            stopBtn = (Button) findViewById(R.id.browser_btn_stop);
            reloadBtn = (Button) findViewById(R.id.browser_btn_reload);
            backBtn1.setOnClickListener(this);
            fwdBtn.setOnClickListener(this);
            stopBtn.setOnClickListener(this);
            reloadBtn.setOnClickListener(this);

            reloadBtn.setEnabled(true);
            stopBtn.setEnabled(true);
            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (GlobalConstants.isFromNewsTemplate) {
                        Intent intent = null;
                        switch (GlobalConstants.className) {
                            case Constants.COMBINATION:
                                intent = new Intent(ScanseeBrowserActivity.this, CombinationTemplate.class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(ScanseeBrowserActivity.this, ScrollingPageActivity.class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(ScanseeBrowserActivity.this, TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    } else {
                        // is there any way to write here so that i can avoid
                        // putting in each subclass having edittext ?
                        // u got it?
                        // @Ramachandran
                        if (SortDepttNTypeScreen.subMenuDetailsList != null) {
                            SortDepttNTypeScreen.subMenuDetailsList.clear();
                            SubMenuStack.clearSubMenuStack();
                            SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
                        }
                        LoginScreenViewAsyncTask.bIsLoginFlag = true;
                        callingMainMenu();
                    }
                }
            });

            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    WebBackForwardList history = webView.copyBackForwardList();
                    Log.e("tag", "history size :" + history.getSize());
                    int index = -1;
                    String url = null;
                    Log.e("tag", "current index :" + history.getCurrentIndex());
                    if (isFromSideNav) {
                        finish();
                    } else {
                        if (canWebviewGoBack) {
                            webView.goBack();
                        } else {
                            while (webView.canGoBackOrForward(index)) {
                                if (history.getCurrentIndex() == 1) {
                                    if (SubMenuStack.getSubMenuStack() != null
                                            && SubMenuStack.getSubMenuStack().size() > 0) {
                                        finish();
                                    } else {
                                        callingMainMenu();
                                    }
                                } else {
                                    if (!history.getItemAtIndex(history.getCurrentIndex() + index).getUrl().equals("about:blank")) {
                                        webView.goBackOrForward(index);
                                        url = history.getItemAtIndex(-index).getUrl();
                                        Log.e("tag", "first non empty" + url);
                                        break;
                                    }
                                }
                                index--;
                            }
                            // no history found that is not empty
                            if (CommonConstants.isFromSliderScreen) {
                                CommonConstants.isFromSliderScreen = false;
                                finish();
                            } else {
                                if (url == null) {
                                    if (SubMenuStack.getSubMenuStack() != null
                                            && SubMenuStack.getSubMenuStack().size() > 0) {
                                        finish();
                                    } else {
                                        callingMainMenu();
                                    }
                                }
                            }
                        }
                    }
                }
            });
            if (urL == null) {
                // add a error meaasge to show that url link is null or not
                // available

                AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
                        ScanseeBrowserActivity.this);
                notificationAlert.setMessage("Sorry, the Webpage is not Available")
                        .setPositiveButton(getString(R.string.specials_ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                notificationAlert.create().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void setColor()
    {
        Intent intent =  getIntent();
        title.setText(intent.getStringExtra("title"));
        title.setTextColor(Color.parseColor(intent.getStringExtra("textColor")));
        parentLayout.setBackgroundColor(Color.parseColor
                ( intent.getStringExtra("textBgColor")));
        String color =  intent.getStringExtra("backButtonColor");
        backImage.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
        isNonFeed = false;
    }

    private void enableGeoLoc(WebSettings settings) {
        settings.setGeolocationEnabled(true);
        settings.setGeolocationDatabasePath(getFilesDir().getPath());
        settings.setDatabaseEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onGeolocationPermissionsShowPrompt(
                    String origin,
                    GeolocationPermissions.Callback callback) {

                super.onGeolocationPermissionsShowPrompt(origin,
                        callback);
                callback.invoke(origin, true, false);
            }
        });
    }

    @SuppressWarnings("deprecation")
    private void checkGPSConnection() {
        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) {
            new AlertDialog.Builder(this).setTitle("")
                    .setMessage(gpsAlertMessage).setPositiveButton("OK", null)
                    .show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // used this piece of code to resolve click issue when coming back from next screen if there is any hyperlink
        mCurrentUrl = null;
        loadingFinished = true;
        if(mDialog != null && mDialog.isShowing())
        {
            mDialog.cancel();
        }
    }

    private String mCurrentUrl;
    boolean loadingFinished = true;

    public class MyWebClient extends WebViewClient {
        boolean isPDF = false;


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.e("tag", "onReceivedError" + description);
            startPdfScreen(failingUrl);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            webProgress.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
            loadingFinished = false;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (webView == null) {
                webView = view;
            }
            // This checking is to load the previous page on back button
            if (mCurrentUrl != null && url != null && url.equals(mCurrentUrl)) {
                webView.goBack();
                return true;
            }

            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;

            Log.v("", "URL NAME : " + url);
            if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
            } else if (url.startsWith("mailto:")) {
                String emailId = url.substring(url.indexOf(":") + 1,
                        url.length());
                Intent emailIntent = new Intent(
                        Intent.ACTION_SEND);
                emailIntent.setType("message/rfc822");
                emailIntent.putExtra(Intent.EXTRA_EMAIL,
                        new String[]{emailId});
                startActivity(Intent.createChooser(emailIntent, "Email:"));
            } else if (url.startsWith("http:") || url.startsWith("https:")) {
                if (url.endsWith(".pdf") || url.contains("/PDF/")) {

                    Intent intent = new Intent(ScanseeBrowserActivity.this,
                            ScanSeePdfViewerActivity.class);
                    try {
                        intent.putExtra(
                                RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
                                url);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivity(intent);
                    if (redirect) {
                        finish();
                    }
                } else {
                    // If the url contains play store link then opening the particular app
                    if (url.contains("play.google.com/store/apps/")) {
                        String packageName = url.split("=")[1];
                        Constants.openCustomUrl(packageName, ScanseeBrowserActivity.this);
                    } else {
                        if (Properties.isRegionApp) {
                            if (url.contains("brightcove") || isEventLogistics) {
                                setRequestedOrientation(ActivityInfo
                                        .SCREEN_ORIENTATION_UNSPECIFIED);

                            } else {
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                            }

                        }

                        Spanned strHtml = Html.fromHtml(url);

                        if (isMaps) {
                            webView.loadUrl(strHtml.toString());
                        } else {

                            if (!redirect) {
                                checkIfPdf(url);
                            } else {
                                webView.loadUrl(strHtml.toString());

                            }
                        }
                    }
                }

            }
            mCurrentUrl = url;
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (!redirect) {
                loadingFinished = true;
            }
            startPdfScreen(url);
            if (!loadingFinished && redirect) {

                redirect = false;
            }
            canWebviewGoBack = view.canGoBack();
            webProgress.setVisibility(View.GONE);
            backBtn1.setEnabled(view.canGoBack());
            fwdBtn.setEnabled(view.canGoForward());
        }
    }

    private void startPdfScreen(String url) {
        if (url.endsWith(".pdf") || url.contains("/PDF/")) {

            Intent intent = new Intent(ScanseeBrowserActivity.this,
                    ScanSeePdfViewerActivity.class);
            try {
                intent.putExtra(
                        RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
                        url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            startActivity(intent);
            finish();
        }
    }


    public String convertToString(InputStream inputStream) {
        StringBuffer string = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                string.append(line).append("\n");
            }
        } catch (IOException e) {
        }
        return string.toString();
    }

    private void checkIfPdf(String url) {

        if (contentAsyncTask != null) {
            if (!contentAsyncTask.isCancelled()) {
                contentAsyncTask.cancel(true);
            }

            contentAsyncTask = null;
        }

        contentAsyncTask = new ContentAsyncTask();
        contentAsyncTask.execute(url);

    }

    public class ContentAsyncTask extends AsyncTask<String, Void, Boolean> {
        boolean isPDF = false;

        String url = "";

        @Override
        protected void onPreExecute() {
            if (!activity.isFinishing())
            {
                mDialog = ProgressDialog.show(ScanseeBrowserActivity.this, "",
                        "Please Wait..", true);
                mDialog.setCancelable(false);
            }

        }

        @Override
        protected Boolean doInBackground(String... params) {


            url = params[0];

            try {
                URL uRL = new URL(url);
                URLConnection conn = uRL.openConnection();

                Map<String, List<String>> headerFields = conn.getHeaderFields();

                Set<String> headerFieldsSet = headerFields.keySet();
                Iterator<String> hearerFieldsIter = headerFieldsSet.iterator();

                while (hearerFieldsIter.hasNext()) {
                    String headerFieldKey = hearerFieldsIter.next();
                    if ("Content-Type".equalsIgnoreCase(headerFieldKey)) {
                        List<String> headerFieldValue = headerFields
                                .get(headerFieldKey);
                        String value = headerFieldValue.get(0);
                        if (value.startsWith("application/pdf")) {
                            isPDF = true;
                        }

                        break;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return isPDF;

        }

        @Override
        protected void onPostExecute(Boolean result) {
            try {
                mDialog.dismiss();
                super.onPostExecute(result);
                if (result) {
                    webView.loadUrl("https://docs.google.com/viewer?url=" + url);
                } else {
                    webView.loadUrl(url);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        webView = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.browser_btn_back:
                // cancelAsyncTasks();
                if (!isEventLogistics) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                if (isMaps) {
                    finish();
                } else {
                    webView.goBack();
                }
                break;
            case R.id.browser_btn_foward:
                webView.goForward();
                break;

            case R.id.browser_btn_stop:
                // cancelAsyncTasks();
                webView.stopLoading();
                stopBtn.setEnabled(false);
                break;

            case R.id.browser_btn_reload:
                webView.reload();
                stopBtn.setEnabled(true);
                break;

            default:
                break;
        }

    }

}
