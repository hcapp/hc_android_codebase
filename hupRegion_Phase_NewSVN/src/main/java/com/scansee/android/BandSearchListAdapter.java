package com.scansee.android;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.scansee.newsfirst.CommonMethods;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by supriya.m on 4/20/2016.
 */
public class BandSearchListAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<BandSearchListSetGet> arrSearch;
    LayoutInflater inflater;
    boolean isLoadMore;

    public BandSearchListAdapter(Context context, ArrayList<BandSearchListSetGet> arrSearch,
                                 boolean isLoadMore) {
        this.mContext = context;
        this.arrSearch = arrSearch;
        this.isLoadMore = isLoadMore;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrSearch.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


            if (position == arrSearch.size() - 1) {
                if (!((BandsLandingPageActivity) mContext).isAlreadyLoading) {
                    if (((BandsLandingPageActivity) mContext).searchNextPage) {
                        ((BandsLandingPageActivity) mContext).loadMoreDataForSearch();
                    }
                }
            }

        if (isLoadMore) {
            if (arrSearch.get(position).isGroup()) {
                convertView = inflater.inflate(R.layout.events_list_item_section, null);
                TextView sectionView = (TextView) convertView
                        .findViewById(R.id.events_list_item_section_text);
                sectionView.setBackgroundColor(mContext.getResources().getColor(R.color.light_grey));
                sectionView.setGravity(Gravity.LEFT);
                sectionView.setText(arrSearch.get(position).getGroupName());
            } else {
//                if (convertView == null) {
                convertView = inflater.inflate(R.layout.band_listing_item, null);
                TextView bandName = (TextView) convertView.findViewById(R.id
                        .band_listing_item_band_name);
                TextView bandCategoryName = (TextView) convertView.findViewById(R.id
                        .band_listing_item_band_category_name);
                ImageView bandImage = (ImageView) convertView.findViewById(R.id
                        .band_listing_item_image);
//                    convertView.setTag(viewHolderBand);
//                } else {
//                    viewHolderBand = (ViewHolderBand) convertView.getTag();
//                }
                bandName.setText(arrSearch.get(position).getSearchedItemName());
                bandCategoryName.setText(arrSearch.get(position).getSearchedCatName());
                if (arrSearch.get(position).getSearchedImgPath() != null && !arrSearch.get(position)
                        .getSearchedImgPath().equals("")) {
                    Picasso.with(mContext).load(arrSearch.get(position).getSearchedImgPath().replace(" ",
                            "%20")).error(R.drawable.ic_empty_icon)
                            .placeholder(R.drawable.loading_animation)
                            .into(bandImage);
                }
            }
        } else {
            ViewHolder viewHolder;
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.band_listing_item, null);
                    viewHolder = new ViewHolder();
                    viewHolder.searchItemName = (TextView) convertView.findViewById(R.id.band_listing_item_band_name);
                    viewHolder.searchCategoryName = (TextView) convertView.findViewById(R.id.band_listing_item_band_category_name);
                    viewHolder.searchBandImage = (ImageView) convertView.findViewById(R.id.band_listing_item_image);
                    viewHolder.progressBar = (ProgressBar)convertView.findViewById(R.id.progress);

                    convertView.setTag(viewHolder);
                } else {
                    viewHolder = (ViewHolder) convertView.getTag();
                }
                String bandName = arrSearch.get(position).getSearchedItemName();
                String catName = arrSearch.get(position).getSearchedCatName();
                String bandImage = arrSearch.get(position).getSearchedImgPath();

                if (bandName != null && !bandName.equals("")) {
                    viewHolder.searchItemName.setText(bandName);
                }
                if (catName != null && !catName.equals("")) {
                    viewHolder.searchCategoryName.setText(catName);
                }
                if (bandImage != null && !bandImage.equals("")) {
                    bandImage = bandImage.replaceAll(" ", "%20");
                    new CommonMethods().loadImage(mContext, viewHolder.progressBar, bandImage, viewHolder.searchBandImage);
                }

        }
        return convertView;
    }

    public class ViewHolder {
        TextView searchItemName,searchCategoryName;
        ImageView searchBandImage;
        ProgressBar progressBar;
    }

}
