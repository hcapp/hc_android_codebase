package com.scansee.android;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.RetailerBo;

public class RetailerNearbyListAdapter extends BaseAdapter {
	private ArrayList<RetailerBo> retailerList;
	private static LayoutInflater inflater = null;

	public RetailerNearbyListAdapter(ArrayList<RetailerBo> retailerList,
			Activity activity) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.retailerList = retailerList;
	}

	@Override
	public int getCount() {
		return retailerList.size();
	}

	@Override
	public Object getItem(int id) {
		return retailerList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		if (convertView == null) {

			view = inflater.inflate(R.layout.listitem_retailer_nearby, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) view.findViewById(R.id.nearby_name);
			viewHolder.distance = (TextView) view
					.findViewById(R.id.nearby_distance);
			viewHolder.price = (TextView) view.findViewById(R.id.nearby_price);
			viewHolder.power = (TextView) view
					.findViewById(R.id.nearby_powered);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();

		}
		viewHolder.name.setText(retailerList.get(position).getRetailerName());
		viewHolder.distance.setText(retailerList.get(position).getDistance()
				+ " miles");
		if (retailerList.get(position).getProductPrice() != null
				&& !"N/A".equalsIgnoreCase(retailerList.get(position)
						.getProductPrice())) {
			viewHolder.price.setText(retailerList.get(position)
					.getProductPrice());
		} else {
			viewHolder.price.setText("");
		}

		if (retailerList.get(position).getPoweredby() != null
				&& !"N/A".equalsIgnoreCase(retailerList.get(position)
						.getPoweredby())
				&& !"".equals(retailerList.get(position).getPoweredby())) {
			viewHolder.power.setText("Powerd by : "
					+ retailerList.get(position).getPoweredby());
		} else {
			viewHolder.power.setText("");
		}

		return view;
	}

	public static class ViewHolder {
		protected TextView name;
		protected TextView distance;
		protected TextView price;
		protected TextView power;

	}
}
