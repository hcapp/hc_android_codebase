package com.scansee.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.URLUtil;
import android.widget.MediaController;
import android.widget.VideoView;

import com.scansee.hubregion.R;

public class ScanSeeVideoActivity extends Activity {

	VideoView mVideoView;

	@SuppressLint("CutPasteId")
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.videoplay);
		mVideoView = (VideoView) findViewById(R.id.surface_view);
		String path = getIntent().getExtras().getString(
				RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH);

		if (null == path) {
			finish();
		}
		Uri uri = Uri.parse(path);
		VideoView video = (VideoView) findViewById(R.id.surface_view);
		video.setVideoURI(uri);
		video.setMediaController(new MediaController(this));
		video.start();

	}

	@SuppressWarnings("unused")
	private String getDataSource(String path) throws IOException {
		if (!URLUtil.isNetworkUrl(path)) {
			return path;
		} else {
			URL url = new URL(path);
			URLConnection cn = url.openConnection();
			cn.connect();
			InputStream stream = cn.getInputStream();
			if (stream == null) {
				throw new RuntimeException("stream is null");
			}
			File temp = File.createTempFile("mediaplayertmp", "dat");
			temp.deleteOnExit();
			String tempPath = temp.getAbsolutePath();
			FileOutputStream out = new FileOutputStream(temp);
			byte buf[] = new byte[128];
			do {
				int numread = stream.read(buf);
				if (numread <= 0) {
					break;
				}
				out.write(buf, 0, numread);
			} while (true);
			try {
				stream.close();
				out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			return tempPath;
		}
	}

}
