package com.scansee.android;

import com.hubcity.android.screens.EventDetailObj;

/**
 * Created by supriya.m on 4/20/2016.
 */
public class BandSearchListSetGet {
    public String getSearchedItemName() {
        return searchedItemName;
    }

    public void setSearchedItemName(String searchedItemName) {
        this.searchedItemName = searchedItemName;
    }

    public String getSearchedItemId() {
        return searchedItemId;
    }

    public void setSearchedItemId(String searchedItemId) {
        this.searchedItemId = searchedItemId;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setIsGroup(boolean isGroup) {
        this.isGroup = isGroup;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    private String groupName;
    private boolean isGroup;
    private String searchedItemId;
    private String searchedItemName;

    public String getSearchedCatName() {
        return searchedCatName;
    }

    public void setSearchedCatName(String searchedCatName) {
        this.searchedCatName = searchedCatName;
    }

    public String getSearchedImgPath() {
        return searchedImgPath;
    }

    public void setSearchedImgPath(String searchedImgPath) {
        this.searchedImgPath = searchedImgPath;
    }

    private String searchedCatName;
    private String searchedImgPath;

    public EventDetailObj getEventDetailObj() {
        return eventDetailObj;
    }

    public void setEventDetailObj(EventDetailObj eventDetailObj) {
        this.eventDetailObj = eventDetailObj;
    }

    private EventDetailObj eventDetailObj;
}
