package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class HotDealsAroundYouListAdapter extends BaseAdapter implements
		OnClickListener {
	private HotDealsActivity activity;
	private ArrayList<HashMap<String, String>> hotdealsList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;
	String itemName = null;
	HashMap<String, String> hotData = null;
	boolean allowPagination;

	public HotDealsAroundYouListAdapter(HotDealsActivity activity,
			ArrayList<HashMap<String, String>> hotdealsList, String selectedTab) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.hotdealsList = hotdealsList;
		this.activity = activity;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(),
				"HotDealsImages");
		if (selectedTab.equalsIgnoreCase("local")) {
			if (activity.localItemCount >= 10)
				allowPagination = true;
		} else if (selectedTab.equalsIgnoreCase("nation")) {
			if (activity.nationItemCount >= 10)
				allowPagination = true;
		}
	}

	@Override
	public int getCount() {
		return hotdealsList.size();
	}

	@Override
	public Object getItem(int id) {
		return hotdealsList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;

		try {
			if (allowPagination) {
				if (position == hotdealsList.size() - 1) {
					if (!((HotDealsActivity) activity).isAlreadyLoaded) {
						if (((HotDealsActivity) activity).nextPage) {
							((HotDealsActivity) activity).viewMore();
						} else {
							((HotDealsActivity) activity).hotdealsListView
									.removeFooterView(((HotDealsActivity) activity).moreResultsView);
						}
					}
				}
			} else {
				((HotDealsActivity) activity).hotdealsListView
						.removeFooterView(((HotDealsActivity) activity).moreResultsView);
			}
			hotData = hotdealsList.get(position);
			itemName = hotData.get("itemName");

			viewHolder = new ViewHolder();
			if ("categoryName".equalsIgnoreCase(itemName)) {
				view = inflater.inflate(
						R.layout.listitem_hotdeals_categoryname, parent,false);

				viewHolder.categoryName = (TextView) view
						.findViewById(R.id.hotdeals_categoryname);
			} else if ("apiPartnerName".equalsIgnoreCase(itemName)) {
				view = inflater.inflate(
						R.layout.listitem_hotdeals_apipartnername, parent,false);
				viewHolder.apiPartnerName = (TextView) view
						.findViewById(R.id.hotdeals_apipartenername);
			} else if ("details".equalsIgnoreCase(itemName)) {
				view = inflater.inflate(R.layout.listitem_hotdeals_details,
						parent,false);
				viewHolder.hotDealName = (TextView) view
						.findViewById(R.id.hotdeals_details_name);
				viewHolder.hDeleteBtn = (Button) view
						.findViewById(R.id.hotdeal_delete);
				viewHolder.hDeleteBtn.setOnClickListener(this);
				viewHolder.hDPrice = (TextView) view
						.findViewById(R.id.hotdeals_details_price);
				viewHolder.hDSalePrice = (TextView) view
						.findViewById(R.id.hotdeals_details_sales_price);
				viewHolder.hotDealImagePath = (ImageView) view
						.findViewById(R.id.hotdeals_details_image);
				viewHolder.ribbonimage = (ImageView) view
						.findViewById(R.id.hotdeal_ribbon_imagepath);
			}
			view.setTag(viewHolder);
			if ("categoryName".equalsIgnoreCase(itemName)) {
				viewHolder.categoryName.setText(hotData.get("categoryName"));
			} else if ("apiPartnerName".equalsIgnoreCase(itemName)) {
				viewHolder.apiPartnerName
						.setText(hotData.get("apiPartnerName"));
			}

			else if ("details".equalsIgnoreCase(itemName)) {
				viewHolder.hotDealName.setText(hotData.get("hotDealName"));
				if (!"N/A".equalsIgnoreCase(hotData.get("hDPrice"))) {
					if (!"$0.00".equalsIgnoreCase(hotData.get("hDPrice"))) {
						viewHolder.hDPrice.setVisibility(View.VISIBLE);
						viewHolder.hDPrice.setText(hotData.get("hDPrice"));
					} else {
						viewHolder.hDPrice.setText(hotData.get("hDPrice"));
						viewHolder.hDPrice.setVisibility(View.INVISIBLE);
					}
				}
				if (!"$0.00".equalsIgnoreCase(hotData.get("hDSalePrice"))) {
					viewHolder.hDSalePrice.setText(activity
							.getString(R.string.sale_price)
							+ " "
							+ hotData.get("hDSalePrice"));
				} else
					viewHolder.hDSalePrice.setVisibility(View.INVISIBLE);

				viewHolder.hotDealImagePath.setTag(hotData
						.get("hotDealImagePath"));

				try {
					Long hotdealId = Long.parseLong(hotData.get("hotDealId"));
					viewHolder.hDeleteBtn.setTag(hotdealId);

				} catch (NumberFormatException ne) {
					ne.printStackTrace();

				}

				if ("0".equals(hotData.get("newFlag"))) {
					viewHolder.ribbonimage.setVisibility(View.INVISIBLE);
				}

				if (viewHolder.hotDealImagePath != null) {
					customImageLoader.displayImage(hotData.get("hotDealImagePath"),
							activity, viewHolder.hotDealImagePath);
				}

			}
			// else if ("viewMore".equalsIgnoreCase(itemName)) {
			// view = inflater.inflate(
			// R.layout.listitem_get_retailers_viewmore, null);
			//
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}

		return view;
	}

	public static class ViewHolder {

		protected TextView city;
		protected TextView apiPartnerName;
		protected ImageView ribbonimage;
		protected TextView categoryName;
		protected TextView hotDealName;
		protected ImageView hotDealImagePath;
		protected TextView hDPrice;
		protected TextView hDSalePrice;
		protected Button hDeleteBtn;

	}

	@Override
	public void onClick(View v) {
		long currPst = 0;
		switch (v.getId()) {
		case R.id.hotdeal_delete:
			hotDealDelete(v);
			break;
		default:
			break;
		}

	}

	private void hotDealDelete(View v) {
		v.setVisibility(View.GONE);
		activity.isDelete = false;
		long currPst = -1;
		currPst = (Long) v.getTag();
		if (currPst != -1 && activity.hotdealsList != null) {
			activity.deleteItem(currPst + "");
		}

	}

}
