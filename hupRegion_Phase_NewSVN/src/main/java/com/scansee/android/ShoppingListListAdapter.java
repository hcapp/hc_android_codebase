package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class ShoppingListListAdapter extends BaseAdapter implements
		OnClickListener {
	private ShoppingListActivity activity;
	private ArrayList<HashMap<String, String>> shoppingList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;
	String itemName = null;
	int count = 0;
	HashMap<String, String> historyData = null;

	public ShoppingListListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> shoppingList) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.shoppingList = shoppingList;
		this.activity = (ShoppingListActivity) activity;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
		int checked = 0, unchecked = 0;
		for (HashMap<String, String> item : shoppingList) {
			if (item != null) {
				if ("true".equalsIgnoreCase(item.get("selected"))) {
					checked = checked + 1;

				} else if ("false".equalsIgnoreCase(item.get("selected"))) {
					unchecked = unchecked + 1;
				}
			}
		}
		this.activity.itemsChecked.setText(checked
				+ (checked > 1 ? " items" : " item"));
		this.activity.itemsUnchecked.setText(unchecked
				+ (unchecked > 1 ? " items" : " item"));
	}

	@Override
	public int getCount() {
		return shoppingList.size();
	}

	@Override
	public Object getItem(int id) {
		return shoppingList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;

		historyData = shoppingList.get(position);
		itemName = historyData.get("itemName");

		viewHolder = new ViewHolder();
		if ("parentcategoryname".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_scannow_date, parent,false);
			viewHolder.categoryname = (TextView) view
					.findViewById(R.id.dummydata_scannow_date);
		}

		else {
			view = inflater.inflate(R.layout.listitem_shoppinglist, parent,false);
			viewHolder.productName = (TextView) view
					.findViewById(R.id.shoppinglist_productsearch_name);
			viewHolder.productDesc = (TextView) view
					.findViewById(R.id.shoppinglist_productsearch_shortdesc);
			viewHolder.productImagePath = (ImageView) view
					.findViewById(R.id.shoppinglist_image);

			view.findViewById(R.id.shoppinglist_checkbox).setOnClickListener(
					this);
			view.findViewById(R.id.shoppinglist_checkbox).setTag(position);
			view.findViewById(R.id.items_unchecked);
		}
		view.setTag(viewHolder);
		if ("parentcategoryname".equalsIgnoreCase(itemName)) {
			viewHolder.categoryname.setText(historyData
					.get(CommonConstants.SHOPPINGLIST_PARENTCATEGORYNAME));
		} else {

			viewHolder.productName.setText(historyData
					.get(CommonConstants.SHOPPINGLIST_PRODUCTNAME));
			viewHolder.productDesc.setText(historyData
					.get(CommonConstants.SHOPPINGLIST_PRODUCTSHORTDESC));
			viewHolder.productImagePath.setTag(historyData
					.get(CommonConstants.SHOPPINGLIST_PRODUCTIMAGEPATH));
			customImageLoader.displayImage(historyData
					.get(CommonConstants.SHOPPINGLIST_PRODUCTIMAGEPATH),
					activity, viewHolder.productImagePath);
			if ("Green".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_COUPONSTATUS))) {
				view.findViewById(R.id.c_image).setBackgroundResource(
						R.drawable.c_green);
				historyData.put("type", "coupon");
			} else if ("Red".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_COUPONSTATUS))) {
				view.findViewById(R.id.c_image).setBackgroundResource(
						R.drawable.c_red);
				historyData.put("type", "coupon");
			} else if ("Grey".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_COUPONSTATUS))) {
				view.findViewById(R.id.c_image).setBackgroundResource(
						R.drawable.c_grey);
			}

			if ("Green".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_REBATESTATUS))) {
				view.findViewById(R.id.r_image).setBackgroundResource(
						R.drawable.r_green);
				historyData.put("type", "rebate");
			} else if ("Red".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_REBATESTATUS))) {
				view.findViewById(R.id.r_image).setBackgroundResource(
						R.drawable.r_red);
				historyData.put("type", "rebate");
			} else if ("Grey".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_REBATESTATUS))) {
				view.findViewById(R.id.r_image).setBackgroundResource(
						R.drawable.r_grey);
			}

			if ("Green".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_LOYALTYSTATUS))) {
				view.findViewById(R.id.l_image).setBackgroundResource(
						R.drawable.l_green);
				historyData.put("type", "loyalty");
			} else if ("Red".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_LOYALTYSTATUS))) {
				view.findViewById(R.id.l_image).setBackgroundResource(
						R.drawable.l_red);
				historyData.put("type", "loyalty");
			} else if ("Grey".equalsIgnoreCase(shoppingList.get(position).get(
					CommonConstants.SHOPPINGLIST_LOYALTYSTATUS))) {
				view.findViewById(R.id.l_image).setBackgroundResource(
						R.drawable.l_grey);
			}
			if ("false".equalsIgnoreCase(historyData.get("selected"))) {

				view.findViewById(R.id.retail_below).setVisibility(
						View.INVISIBLE);
				((ImageView) view.findViewById(R.id.shoppinglist_checkbox))
						.setImageResource(R.drawable.checkbox_unmarked);

			} else {

				view.findViewById(R.id.retail_below)
						.setVisibility(View.VISIBLE);
				((ImageView) view.findViewById(R.id.shoppinglist_checkbox))
						.setImageResource(R.drawable.checkbox_marked);

			}
			view.findViewById(R.id.shoppinglist_delete)
					.setOnClickListener(this);
			view.findViewById(R.id.shoppinglist_delete).setTag(position);

		}
		return view;
	}

	public static class ViewHolder {

		protected ImageView productImagePath;
		protected TextView productDesc;
		protected TextView categoryname;
		protected TextView productName;

	}

	@Override
	public void onClick(View view) {
		int currPst = 0;
		switch (view.getId()) {
		case R.id.shoppinglist_checkbox:
			shoppinglistCheckbox(view);
			break;
		case R.id.shoppinglist_delete:
			shoppinglistDelete(view);
			break;
		default:
			break;
		}

	}

	private void shoppinglistDelete(View view) {
		view.setVisibility(View.GONE);
		activity.isDelete = false;
		int currPst = -1;
		currPst = (Integer) view.getTag();
		if (currPst != -1 && activity.shoppingList != null) {
			activity.userProdId = activity.shoppingList.get(currPst).get(
					CommonConstants.SHOPPINGLIST_USERPRODUCTID);
			activity.shoppingList.remove(currPst);
			activity.deleteItem();
		}

	}

	private void shoppinglistCheckbox(View view) {
		if (activity.isDelete) {
			activity.isDelete = false;
			Parcelable state = activity.shoppingListListView
					.onSaveInstanceState();
			activity.shoppingListListAdapter = new ShoppingListListAdapter(
					activity, shoppingList);
			activity.shoppingListListView
					.setAdapter(activity.shoppingListListAdapter);
			activity.shoppingListListView.onRestoreInstanceState(state);

			return;
		}
		int currPst = (Integer) view.getTag();
		if (currPst > 0 && activity.shoppingList != null) {
			if ("false".equalsIgnoreCase(shoppingList.get(currPst).get(
					"selected"))) {
				activity.shoppingList.get(currPst).put("selected", "true");
				((ImageView) view.findViewById(R.id.shoppinglist_checkbox))
						.setImageResource(R.drawable.checkbox_marked);
			} else {
				activity.shoppingList.get(currPst).put("selected", "false");
				((ImageView) view.findViewById(R.id.shoppinglist_checkbox))
						.setImageResource(R.drawable.checkbox_unmarked);
			}
			activity.saveHelper(activity.shoppingList.get(currPst));
			Parcelable state = activity.shoppingListListView
					.onSaveInstanceState();
			activity.shoppingListListAdapter = new ShoppingListListAdapter(
					activity, activity.shoppingList);
			activity.shoppingListListView
					.setAdapter(activity.shoppingListListAdapter);
			activity.shoppingListListView.onRestoreInstanceState(state);

		}

	}
}
