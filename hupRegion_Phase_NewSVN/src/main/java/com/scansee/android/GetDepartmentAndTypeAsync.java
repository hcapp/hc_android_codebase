package com.scansee.android;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.hubcity.android.businessObjects.FilterOptionBO;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class GetDepartmentAndTypeAsync extends AsyncTask<Void, Void, Void> {

	Context mContext;
	String fName;
	ArrayList<FilterOptionBO> arrFilterOptionBOs;
	private ProgressDialog mDialog;
	int groupPosition;
	OnCreateFilterChildView filterChildView;

	public GetDepartmentAndTypeAsync(Context context, String fName,
			int groupPosition) {
		this.mContext = context;
		this.fName = fName;
		this.groupPosition = groupPosition;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mDialog = ProgressDialog.show(mContext, "", Constants.DIALOG_MESSAGE,
				true);

		mDialog.setCancelable(false);
	}

	@Override
	protected Void doInBackground(Void... params) {
		JSONObject jsonResponse;
		String urlParameters;
		try {
			String url_citi_dept_and_menu_type = Properties.url_local_server
					+ Properties.hubciti_version + "firstuse/deptandmenutype";
			urlParameters = new UrlRequestParams().getDepttAndType(fName);
			jsonResponse = new ServerConnections().getUrlPostResponse(
					url_citi_dept_and_menu_type, urlParameters, true);
			if (jsonResponse.getJSONObject("Menu").getString("responseCode")
					.equals("10000")) {
				JSONArray jsonArray = null;
				JSONObject jsonObject = null;
				try {
					jsonArray = jsonResponse.getJSONObject("Menu")
							.getJSONObject("mItemList")
							.getJSONArray("MenuItem");
				} catch (Exception e) {
					jsonObject = jsonResponse.getJSONObject("Menu")
							.getJSONObject("mItemList")
							.getJSONObject("MenuItem");
				}
				arrFilterOptionBOs = new ArrayList<>();
				if (jsonArray != null) {
					for (int i = 0; i < jsonArray.length(); i++) {
						FilterOptionBO filterOptionBO = new FilterOptionBO();
						if (fName.equals("dept")) {
							filterOptionBO.setFilterGroupName("Department");
						} else {
							filterOptionBO.setFilterGroupName("Type");
						}
						try {
							filterOptionBO.setFilterId(jsonArray.getJSONObject(
									i).getString("departmentId"));
							filterOptionBO.setFilterName(jsonArray
									.getJSONObject(i).getString(
											"departmentName"));
						} catch (Exception e) {
							filterOptionBO.setFilterId(jsonArray.getJSONObject(
									i).getString("mItemTypeId"));
							filterOptionBO.setFilterName(jsonArray
									.getJSONObject(i)
									.getString("mItemTypeName"));
						}
						arrFilterOptionBOs.add(filterOptionBO);
					}
				} else {
					FilterOptionBO filterOptionBO = new FilterOptionBO();
					if (fName.equals("dept")) {
						filterOptionBO.setFilterGroupName("Department");
					} else {
						filterOptionBO.setFilterGroupName("Type");
					}
					try {
						filterOptionBO.setFilterId(jsonObject
								.getString("departmentId"));
						filterOptionBO.setFilterName(jsonObject
								.getString("departmentName"));
					} catch (Exception e) {
						filterOptionBO.setFilterId(jsonObject
								.getString("mItemTypeId"));
						filterOptionBO.setFilterName(jsonObject
								.getString("mItemTypeName"));
					}
					arrFilterOptionBOs.add(filterOptionBO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (arrFilterOptionBOs != null) {
			try {
				((FilterAndSortScreen) mContext).setFilterOptionsForChildView(
						arrFilterOptionBOs, groupPosition, fName);
			} catch (Exception e) {
				filterChildView.setFilterOptionsForChildView(
						arrFilterOptionBOs, groupPosition, fName);
			}
		} else
			Toast.makeText(mContext, "No Records Found", Toast.LENGTH_SHORT)
					.show();

		mDialog.dismiss();
	}

}
