package com.scansee.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.artifex.mupdflib.AsyncTask;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

import org.json.JSONObject;

/**
 * Created by supriya.m on 7/6/2016.
 */
public class ResetPasswordAsync extends AsyncTask<String, Void, String>
{
	ProgressDialog mDialog;
	Context mContext;
	UrlRequestParams urlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();
	String responseCode, password;

	public ResetPasswordAsync(Context context)
	{
		this.mContext = context;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
		mDialog = ProgressDialog.show(mContext, "",
				"Please Wait..", true);
		mDialog.setCancelable(false);
	}

	@Override
	protected String doInBackground(String... params)
	{
		String responseText = "";
		try {
			String url_update_pwd = Properties.url_local_server
					+ Properties.hubciti_version
					+ "firstuse/v2/resetpassword";
//			String url_update_pwd = "http://10.10.221.225:8080/HubCiti2" +
//					".6/firstuse/v2/resetpassword";
			password = params[0];
			String urlParameters = urlRequestParams
					.createUpdatePasswordUrlParameter(password);
			JSONObject jsonObject = mServerConnections.getUrlPostResponse(
					url_update_pwd, urlParameters, true);
			if (jsonObject != null) {
				responseText = jsonObject.getJSONObject("response")
						.getString("responseText");
				responseCode = jsonObject.getJSONObject("response")
						.getString("responseCode");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseText;
	}

	@Override
	protected void onPostExecute(String responseText)
	{
		super.onPostExecute(responseText);
		try {
			mDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (responseCode.equals("10000")) {
			Constants.setNavigationBarValues(Constants.IS_TEMP_USER, "0");
		}
//		if (responseCode.equals("10000")) {
//			SharedPreferences preferences = mContext.getSharedPreferences(
//					Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
//			SharedPreferences.Editor prefEditor = preferences.edit();
//			prefEditor.putString(Constants.REMEMBER_PASSWORD, password);
//			prefEditor.apply();
//		}
		Toast.makeText(mContext, responseText, Toast.LENGTH_SHORT).show();
	}
}
