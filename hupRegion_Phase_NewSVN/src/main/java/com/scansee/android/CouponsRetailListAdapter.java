package com.scansee.android;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.HashMap;

public class CouponsRetailListAdapter extends BaseAdapter {
	Activity activity;
	private ArrayList<HashMap<String, String>> couponslocationsList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	public CouponsRetailListAdapter(Activity activity,
									ArrayList<HashMap<String, String>> couponsproductsList) {
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.couponslocationsList = couponsproductsList;

		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return couponslocationsList.size();
	}

	@Override
	public Object getItem(int id) {
		return couponslocationsList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		if (convertView == null) {

			view = inflater.inflate(R.layout.listitem_retailer_location, parent, false);
			viewHolder = new ViewHolder();

			viewHolder.couponname = (TextView) view
					.findViewById(R.id.coupon_location_name);
			viewHolder.couponlogo = (ImageView) view
					.findViewById(R.id.coupon_location_logo);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		String retailerDetails = couponslocationsList.get(position).get(
				"retailerName")
				+ "\n"
				+ couponslocationsList.get(position).get("completeAddress");
		viewHolder.couponname.setText(retailerDetails);

		customImageLoader.displayImage(
				couponslocationsList.get(position).get("logoImagePath"),
				activity, viewHolder.couponlogo);


		return view;
	}

	public static class ViewHolder {
		protected TextView couponname;
		protected ImageView couponlogo;

	}

}
