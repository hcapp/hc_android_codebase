package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

public class HotDealClaimRedeemExpiredActivty extends CustomTitleBar implements
		OnClickListener {
	EditText searchHotdeals;
	String mItemId = "", mBottomId = "";
	String claimRedeemExpiredValue;
	ProgressDialog progDialog;
	String searchkey;
	ImageButton nextPageBtnLocation, prevPageBtnLocation;
	String bottomBtnId = null;
	RadioButton radioLoc, radioProd;
	JSONArray jsonCouponsArray;
	protected HotDealClaimRedeemExpiredListAdapter hotDealClaimedRedeemExpiredListAdapter;
	boolean firstRequest = true;
	View paginator, paginatorSearch;
	View moreResultsView;
	Button moreInfo, searchCancelBtn;
	boolean isMainmenuBtnClicked = false;

	int lastvisitProdIdLocation = 0, minProdIdLocation = 0;
	int lastvisitProdIdProducts = 0, minProdIdProducts = 0;

	protected ListView couponsListView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.used_claimed_expired);
		try {
			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                            Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }

			if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                            Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
                mBottomId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }

			if (getIntent().hasExtra(CommonConstants.TAB_SELECTED)) {
                claimRedeemExpiredValue = getIntent().getExtras().getString(
                        CommonConstants.TAB_SELECTED);
            }
			getGPSValues();
			moreResultsView = getLayoutInflater().inflate(
                    R.layout.events_pagination, couponsListView, true);
			moreInfo = (Button) moreResultsView
                    .findViewById(R.id.events_view_more_button);
			moreInfo.setOnClickListener(this);

			couponsListView = (ListView) findViewById(R.id.coupons_list);

			searchHotdeals = (EditText) findViewById(R.id.search_hotdeals);
			radioLoc = (RadioButton) findViewById(R.id.coupon_radio_location);
			radioProd = (RadioButton) findViewById(R.id.coupon_radio_product);

			radioLoc.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!"".equals(searchHotdeals.getText())) {
                        searchHotdeals.setText("");
                        searchkey = "";
                    }

                    lastvisitProdIdLocation = 0;
                    nextPageLocation = false;
                    nextPage = false;
                    isShowdialog = false;
                    hotdealsListbyLocations = new ArrayList<>();
                    new HotDealsClaimedByLocation().execute();
                }
            });

			radioProd.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!"".equals(searchHotdeals.getText())) {
                        searchHotdeals.setText("");
                        searchkey = "";
                    }
                    lastvisitProdIdProducts = 0;
                    nextPageProducts = false;
                    nextPage = false;
                    isShowdialog = false;
                    hotdealsListbyProducts = new ArrayList<>();
                    new HotDealsClaimedByProducts().execute();
                }
            });

			searchCancelBtn = (Button) findViewById(R.id.hotdeals_cancel);
			searchCancelBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    searchHotdeals.setText("");
                    searchkey = "";
                    hideKeyBoard();
                    hideKeyboardItem();
                }
            });

			searchHotdeals.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
			searchHotdeals.setOnEditorActionListener(new OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView textView, int actionId,
                        KeyEvent event) {

                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        searchkey = searchHotdeals.getText().toString().trim();

                        if (radioLoc.isChecked()) {
                            hotdealsListbyLocations.clear();
                            lastvisitProdIdLocation = 0;
                            hotdealsListbyLocations = new ArrayList<>();
                            new HotDealsClaimedByLocation().execute();
                        } else if (radioProd.isChecked()) {
                            hotdealsListbyLocations.clear();
                            lastvisitProdIdProducts = 0;
                            hotdealsListbyProducts = new ArrayList<>();
                            new HotDealsClaimedByProducts().execute();
                        }

                    }
                    hideKeyBoard();
                    hideKeyboardItem();
                    return true;
                }
            });

			new HotDealsClaimedByLocation().execute();

			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText(claimRedeemExpiredValue);

			leftTitleImage.setVisibility(View.GONE);
			leftTitleImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    isMainmenuBtnClicked = true;
                    setResult(Constants.FINISHVALUE);
                    finish();
                }
            });

			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    callIntent();

                }
            });

			couponsListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {
                    try {
                        HashMap<String, String> selectedData = null;
                        if (radioLoc.isChecked()) {
                            selectedData = hotdealsListbyLocations.get(position);
                        }

                        if (radioProd.isChecked()) {
                            selectedData = hotdealsListbyProducts.get(position);
                        }

                        if (selectedData != null
                                && "details".equalsIgnoreCase(selectedData
                                        .get("itemName"))) {
                            Intent dealDetail = new Intent(
                                    HotDealClaimRedeemExpiredActivty.this,
                                    HotDealsDetailsActivity.class);
                            dealDetail.putExtra("hotDealId",
                                    selectedData.get("hotDealId"));
                            dealDetail.putExtra("city", selectedData.get("city"));
                            dealDetail.putExtra("hDshortDescription",
                                    selectedData.get("hDshortDescription"));
                            dealDetail.putExtra("hotDealName",
                                    selectedData.get("hotDealName"));

                            dealDetail.putExtra("hDPrice",
                                    selectedData.get("hDPrice"));
                            dealDetail.putExtra("hDSalePrice",
                                    selectedData.get("hDSalePrice"));
                            dealDetail.putExtra("hDStartDate",
                                    selectedData.get("hDStartDate"));
                            dealDetail.putExtra("hDEndDate",
                                    selectedData.get("hDEndDate"));
                            dealDetail.putExtra("hDStartDate",
                                    selectedData.get("hDStartDate"));
                            dealDetail.putExtra("hotDealListId",
                                    selectedData.get("hotDealListId"));
                            dealDetail.putExtra(
                                    Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
                            dealDetail.putExtra(
                                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                                    mBottomId);
                            startActivity(dealDetail);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

			hotdealsListbyLocations = new ArrayList<>();
			hotdealsListbyProducts = new ArrayList<>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void hideKeyboardItem() {
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(searchHotdeals.getWindowToken(), 0);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {

		case R.id.events_view_more_button:
			viewMoreAction();
			break;

		default:
			break;
		}
	}

	boolean isAlreadyLoading;

	protected void viewMoreAction() {
		isAlreadyLoading = true;
		if (radioLoc.isChecked()) {
			new HotDealsClaimedByLocation().execute();
		} else if (radioProd.isChecked()) {
			new HotDealsClaimedByProducts().execute();
		}
	}

	private void hideKeyBoard() {
		HotDealClaimRedeemExpiredActivty.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	boolean nextPageProducts;
	boolean nextPageLocation;

	@Override
	protected void onResume() {
		hideKeyBoard();
		hideKeyboardItem();
		super.onResume();

	}

	LocationManager locationManager;

	public void getGPSValues() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (locationManager.getAllProviders().contains("gps") && gpsEnabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (locNew != null) {

				CommonConstants.LATITUDE = String.valueOf(locNew.getLatitude());
				CommonConstants.LONGITUDE = String.valueOf(locNew
						.getLongitude());
			} else {
				// N/W Tower Info Start
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					CommonConstants.LATITUDE = String.valueOf(locNew
							.getLatitude());
					CommonConstants.LONGITUDE = String.valueOf(locNew
							.getLongitude());
				}
			}
		}
	}

	boolean isShowdialog;

	public class HotDealsClaimedByLocation extends
			AsyncTask<String, Void, String> {

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();
		JSONObject jsonObject = null;
		HashMap<String, String> categoryData = null;
		HashMap<String, String> retailerData = null;

		String result = "true";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!isShowdialog) {
				isShowdialog = true;
				progDialog = new ProgressDialog(
						HotDealClaimRedeemExpiredActivty.this);
				progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progDialog.setMessage(Constants.DIALOG_MESSAGE);
				progDialog.setCancelable(false);
				progDialog.show();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				progDialog.dismiss();
				isAlreadyLoading = false;
				hotDealClaimedRedeemExpiredListAdapter = new HotDealClaimRedeemExpiredListAdapter(
                        HotDealClaimRedeemExpiredActivty.this,
                        hotdealsListbyLocations);
				couponsListView.setAdapter(hotDealClaimedRedeemExpiredListAdapter);
				findViewById(R.id.no_coupons_found).setVisibility(View.INVISIBLE);
				if ("true".equals(result)) {

                    if (nextPage) {

                        try {
                            couponsListView.removeFooterView(moreResultsView);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        // moreInfo.setVisibility(View.VISIBLE);

                        couponsListView.addFooterView(moreResultsView);

                    } else {
                        couponsListView.removeFooterView(moreResultsView);
                    }
                    couponsListView
                            .setAdapter(hotDealClaimedRedeemExpiredListAdapter);
                    firstRequest = false;
                } else if ("false".equals(result) && firstRequest) {
                    firstRequest = false;
                    radioProd.setChecked(true);
                    new HotDealsClaimedByProducts().execute();
                } else {
                    if (!"".equals(searchHotdeals.getText())) {
                        searchHotdeals.setText("");
                        searchkey = "";
                    }
                    couponsListView.setAdapter(null);
                    findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public boolean isJSONArray(JSONObject jsonObject, String value) {
			boolean isArray = false;

			JSONObject isJSONObject = jsonObject.optJSONObject(value);
			if (isJSONObject == null) {
				JSONArray isJSONArray = jsonObject.optJSONArray(value);
				if (isJSONArray != null) {
					isArray = true;
				}
			}

			return isArray;
		}

		@Override
		protected String doInBackground(String... params) {
			try {

				String urlParameters = mUrlRequestParams
						.getHotDealsByLocationAndProducts(searchkey,
								claimRedeemExpiredValue, mItemId,
								lastvisitProdIdLocation + "", bottomBtnId);
				String get_hotdeals_bylocation = Properties.url_local_server
						+ Properties.hubciti_version + "gallery/gallhdbyloc";
				jsonObject = mServerConnections.getUrlPostResponse(
						get_hotdeals_bylocation, urlParameters, true);
				if (jsonObject != null) {
					try {

						nextPage = "1".equalsIgnoreCase(jsonObject.getJSONObject(
								"HotDealsListResultSet").getString("nextPage"));
					} catch (Exception e) {
						e.printStackTrace();
						if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
								"response").getString("responseCode"))) {
							result = jsonObject.getJSONObject("response")
									.getString("responseText");
							result = "false";
						}
					}

					try {
						if (jsonObject.getJSONObject("HotDealsListResultSet")
								.has("maxRowNum")) {
							if (Integer.valueOf(jsonObject.getJSONObject(
									"HotDealsListResultSet").getString(
									"maxRowNum")) > lastvisitProdIdLocation) {
								lastvisitProdIdLocation = Integer
										.parseInt(jsonObject.getJSONObject(
												"HotDealsListResultSet")
												.getString("maxRowNum"));
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (jsonObject.has("HotDealsListResultSet")) {
						minProdIdLocation = lastvisitProdIdLocation;
						JSONObject jsonHotDealsList = null;
						JSONArray jsonRetDetailsList = new JSONArray();

						try {
							jsonRetDetailsList.put(jsonObject
									.getJSONObject("HotDealsListResultSet")
									.getJSONObject("retDetailsList")
									.getJSONObject("RetailersDetails"));

						} catch (Exception e) {

							jsonRetDetailsList = jsonObject
									.getJSONObject("HotDealsListResultSet")
									.getJSONObject("retDetailsList")
									.getJSONArray("RetailersDetails");
						}

						for (int j = 0; j < jsonRetDetailsList.length(); j++) {

							JSONObject jsonRet = jsonRetDetailsList
									.getJSONObject(j);

							retailerData = new HashMap<>();
							retailerData.put("itemName", "apiPartnerName");
							retailerData.put("apiPartnerName",
									jsonRet.getString("retName"));
							retailerData.put("apiPartnerId",
									jsonRet.getString("retId"));
							hotdealsListbyLocations.add(retailerData);
							try {

								jsonHotDealsList = jsonRet.getJSONObject(
										"retDetailsList").getJSONObject(
										"RetailersDetails");
								categoryData = new HashMap<>();
								categoryData.put("itemName", "categoryName");
								categoryData.put("categoryName",
										jsonHotDealsList
												.getString("retailerAddress"));

								hotdealsListbyLocations.add(categoryData);
								try {
									hotdealsListbyLocations
											.add(getJSONValuesLocation(jsonHotDealsList
													.getJSONObject(
															"hDDetailsList")
													.getJSONObject(
															"HotDealsDetails")));
								} catch (Exception e1) {
									e1.printStackTrace();
									JSONArray jsonRetailerList = jsonHotDealsList
											.getJSONObject("hDDetailsList")
											.getJSONArray("HotDealsDetails");
									for (int i = 0; i < jsonRetailerList
											.length(); i++) {
										JSONObject elem = jsonRetailerList
												.getJSONObject(i);
										hotdealsListbyLocations
												.add(getJSONValuesLocation(elem));
									}
								}
							} catch (Exception ex) {
								ex.printStackTrace();

								JSONArray jsonRetailerArray = jsonRet
										.getJSONObject("retDetailsList")
										.getJSONArray("RetailersDetails");
								for (int i = 0; i < jsonRetailerArray.length(); i++) {
									JSONObject elem = jsonRetailerArray
											.getJSONObject(i);
									categoryData = new HashMap<>();
									categoryData
											.put("itemName", "categoryName");
									categoryData.put("categoryName",
											elem.getString("retailerAddress"));
									hotdealsListbyLocations.add(categoryData);
									try {
										hotdealsListbyLocations
												.add(getJSONValuesLocation(jsonHotDealsList
														.getJSONObject(
																"hDDetailsList")
														.getJSONObject(
																"HotDealsDetails")));
									} catch (Exception ex1) {
										ex1.printStackTrace();
										try {
											JSONArray jsonRetailerList = elem
													.getJSONObject(
															"hDDetailsList")
													.getJSONArray(
															"HotDealsDetails");
											for (int ind = 0; ind < jsonRetailerList
													.length(); ind++) {
												JSONObject elemr = jsonRetailerList
														.getJSONObject(ind);
												hotdealsListbyLocations
														.add(getJSONValuesLocation(elemr));
											}
										} catch (Exception ed) {
											ed.printStackTrace();
											JSONObject jsonRetailerList = elem
													.getJSONObject(
															"hDDetailsList")
													.getJSONObject(
															"HotDealsDetails");
											hotdealsListbyLocations
													.add(getJSONValuesLocation(jsonRetailerList));
										}
									}
								}
							}
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				result = "false";
			}
			return result;
		}
	}

	ArrayList<HashMap<String, String>> hotdealsListbyLocations;
	ArrayList<HashMap<String, String>> hotdealsListbyProducts;

	public HashMap<String, String> getJSONValuesLocation(JSONObject jsonHotDeal) {
		HashMap<String, String> hotData = new HashMap<>();
		hotData.put("itemName", "details");
		try {

			if (jsonHotDeal.has("hDDesc")) {
				hotData.put("hDDesc", jsonHotDeal.getString("hDDesc"));
			}
			if (jsonHotDeal.has("hdURL")) {
				hotData.put("hdURL", jsonHotDeal.getString("hdURL"));
			}
			if (jsonHotDeal.has("hDEndDate")) {
				hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
			}
			if (jsonHotDeal.has("newFlag")) {
				hotData.put("newFlag", jsonHotDeal.getString("newFlag"));
			}
			if (jsonHotDeal.has("hotDealImagePath")) {
				hotData.put("hotDealImagePath",
						jsonHotDeal.getString("hotDealImagePath"));
			}
			if (jsonHotDeal.has("hDEndDate")) {
				hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
			}
			if (jsonHotDeal.has("hDStartDate")) {
				hotData.put("hDStartDate", jsonHotDeal.getString("hDStartDate"));
			}
			if (jsonHotDeal.has("hDDiscountAmount")) {
				hotData.put("hDDiscountAmount",
						jsonHotDeal.getString("hDDiscountAmount"));
			}
			if (jsonHotDeal.has("rowNumber")) {
				hotData.put("rowNumber", jsonHotDeal.getString("rowNumber"));
			}
			if (jsonHotDeal.has("hotDealId")) {
				hotData.put("hotDealId", jsonHotDeal.getString("hotDealId"));
			}
			if (jsonHotDeal.has("city")) {
				hotData.put("city", jsonHotDeal.getString("city"));
			}
			if (jsonHotDeal.has("hDDiscountPct")) {
				hotData.put("hDDiscountPct",
						jsonHotDeal.getString("hDDiscountPct"));
			}
			if (jsonHotDeal.has("hotDealListId")) {
				hotData.put("hotDealListId",
						jsonHotDeal.getString("hotDealListId"));
			}
			if (jsonHotDeal.has("hotDealName")) {
				hotData.put("hotDealName", jsonHotDeal.getString("hotDealName"));
			}
			if (jsonHotDeal.has("extFlag")) {
				hotData.put("extFlag", jsonHotDeal.getString("extFlag"));
			}
			if (jsonHotDeal.has("hDExpDate")) {
				hotData.put("hDExpDate", jsonHotDeal.getString("hDExpDate"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return hotData;
	}

	public HashMap<String, String> getJSONValuesProducts(JSONObject jsonHotDeal) {
		HashMap<String, String> hotData = new HashMap<>();
		hotData.put("itemName", "details");
		try {

			if (jsonHotDeal.has("hDDesc")) {
				hotData.put("hDDesc", jsonHotDeal.getString("hDDesc"));
			}
			if (jsonHotDeal.has("hdURL")) {
				hotData.put("hdURL", jsonHotDeal.getString("hdURL"));
			}
			if (jsonHotDeal.has("hDEndDate")) {
				hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
			}
			if (jsonHotDeal.has("newFlag")) {
				hotData.put("newFlag", jsonHotDeal.getString("newFlag"));
			}
			if (jsonHotDeal.has("hotDealImagePath")) {
				hotData.put("hotDealImagePath",
						jsonHotDeal.getString("hotDealImagePath"));
			}
			if (jsonHotDeal.has("hDEndDate")) {
				hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
			}
			if (jsonHotDeal.has("hDStartDate")) {
				hotData.put("hDStartDate", jsonHotDeal.getString("hDStartDate"));
			}
			if (jsonHotDeal.has("hDDiscountAmount")) {
				hotData.put("hDDiscountAmount",
						jsonHotDeal.getString("hDDiscountAmount"));
			}
			if (jsonHotDeal.has("rowNumber")) {
				hotData.put("rowNumber", jsonHotDeal.getString("rowNumber"));
			}
			if (jsonHotDeal.has("hotDealId")) {
				hotData.put("hotDealId", jsonHotDeal.getString("hotDealId"));
			}
			if (jsonHotDeal.has("city")) {
				hotData.put("city", jsonHotDeal.getString("city"));
			}
			if (jsonHotDeal.has("hDDiscountPct")) {
				hotData.put("hDDiscountPct",
						jsonHotDeal.getString("hDDiscountPct"));
			}
			if (jsonHotDeal.has("hotDealListId")) {
				hotData.put("hotDealListId",
						jsonHotDeal.getString("hotDealListId"));
			}
			if (jsonHotDeal.has("hotDealName")) {
				hotData.put("hotDealName", jsonHotDeal.getString("hotDealName"));
			}
			if (jsonHotDeal.has("extFlag")) {
				hotData.put("extFlag", jsonHotDeal.getString("extFlag"));
			}
			if (jsonHotDeal.has("hDExpDate")) {
				hotData.put("hDExpDate", jsonHotDeal.getString("hDExpDate"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return hotData;
	}

	boolean nextPage = false;

	public class HotDealsClaimedByProducts extends
			AsyncTask<String, Void, String> {
		HashMap<String, String> categoryData = null;
		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();
		JSONObject jsonObject = null;
		String result = "true";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!isShowdialog) {
				isShowdialog = true;
				progDialog = new ProgressDialog(
						HotDealClaimRedeemExpiredActivty.this);
				progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progDialog.setMessage(Constants.DIALOG_MESSAGE);
				progDialog.setCancelable(false);
				progDialog.show();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				progDialog.dismiss();
				isAlreadyLoading = false;
				findViewById(R.id.no_coupons_found).setVisibility(View.INVISIBLE);
				if ("true".equals(result)) {

                    HotDealClaimRedeemExpiredListAdapter hotDealClaimedRedeemExpiredListAdapter = new HotDealClaimRedeemExpiredListAdapter(
                            HotDealClaimRedeemExpiredActivty.this,
                            hotdealsListbyProducts);
                    if (nextPage) {

                        try {
                            couponsListView.removeFooterView(moreResultsView);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        // moreInfo.setVisibility(View.VISIBLE);

                        couponsListView.addFooterView(moreResultsView);

                    } else {
                        couponsListView.removeFooterView(moreResultsView);
                    }
                    couponsListView
                            .setAdapter(hotDealClaimedRedeemExpiredListAdapter);
                } else {
                    if (!"".equals(searchHotdeals.getText())) {
                        searchHotdeals.setText("");
                        searchkey = "";
                    }
                    couponsListView.setAdapter(null);
                    findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
                }
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		public boolean isJSONArray(JSONObject jsonObject, String value) {
			boolean isArray = false;

			JSONObject isJSONObject = jsonObject.optJSONObject(value);
			if (isJSONObject == null) {
				JSONArray isJSONArray = jsonObject.optJSONArray(value);
				if (isJSONArray != null) {
					isArray = true;
				}
			}

			return isArray;
		}

		@Override
		protected String doInBackground(String... params) {
			try {

				String urlParameters = mUrlRequestParams
						.getHotDealsByLocationAndProducts(searchkey,
								claimRedeemExpiredValue, mItemId,
								lastvisitProdIdProducts + "", bottomBtnId);
				String get_hotdeals_byproducts = Properties.url_local_server
						+ Properties.hubciti_version + "gallery/gallhdbyprod";
				jsonObject = mServerConnections.getUrlPostResponse(
						get_hotdeals_byproducts, urlParameters, true);
				if (jsonObject != null) {
					try {

						nextPage = "1".equalsIgnoreCase(jsonObject.getJSONObject(
								"HotDealsListResultSet").getString("nextPage"));
					} catch (Exception e) {
						e.printStackTrace();
						if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
								"response").getString("responseCode"))) {
							result = jsonObject.getJSONObject("response")
									.getString("responseText");
							nextPage = false;
						}
					}

					try {
						if (jsonObject.getJSONObject("HotDealsListResultSet")
								.has("maxRowNum")) {
							if (Integer.valueOf(jsonObject.getJSONObject(
									"HotDealsListResultSet").getString(
									"maxRowNum")) > lastvisitProdIdProducts) {
								lastvisitProdIdProducts = Integer
										.parseInt(jsonObject.getJSONObject(
												"HotDealsListResultSet")
												.getString("maxRowNum"));
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (jsonObject.has("HotDealsListResultSet")) {
						minProdIdProducts = lastvisitProdIdProducts;
						JSONObject jsonHotDealsList = null;
						try {
							jsonHotDealsList = jsonObject
									.getJSONObject("HotDealsListResultSet")
									.getJSONObject("categoryInfoList")
									.getJSONObject("CategoryInfo");
							categoryData = new HashMap<>();
							categoryData.put("itemName", "categoryName");
							categoryData.put("categoryName",
									jsonHotDealsList.getString("categoryName"));
							hotdealsListbyProducts.add(categoryData);
							try {
								hotdealsListbyProducts
										.add(getJSONValuesProducts(jsonHotDealsList
												.getJSONObject("hDDetailsList")
												.getJSONObject(
														"HotDealsDetails")));
							} catch (Exception e) {
								e.printStackTrace();
								JSONArray jsonRetailerList = jsonHotDealsList
										.getJSONObject("hDDetailsList")
										.getJSONArray("HotDealsDetails");
								for (int i = 0; i < jsonRetailerList.length(); i++) {
									JSONObject elem = jsonRetailerList
											.getJSONObject(i);
									hotdealsListbyProducts
											.add(getJSONValuesProducts(elem));
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							JSONArray jsonRetailerArray = jsonObject
									.getJSONObject("HotDealsListResultSet")
									.getJSONObject("categoryInfoList")
									.getJSONArray("CategoryInfo");
							for (int i = 0; i < jsonRetailerArray.length(); i++) {
								JSONObject elem = jsonRetailerArray
										.getJSONObject(i);
								categoryData = new HashMap<>();
								categoryData.put("itemName", "categoryName");
								categoryData.put("categoryName",
										elem.getString("categoryName"));
								hotdealsListbyProducts.add(categoryData);
								try {
									hotdealsListbyProducts
											.add(getJSONValuesProducts(jsonHotDealsList
													.getJSONObject(
															"hDDetailsList")
													.getJSONObject(
															"HotDealsDetails")));
								} catch (Exception ex) {
									ex.printStackTrace();
									try {
										JSONArray jsonRetailerList = elem
												.getJSONObject("hDDetailsList")
												.getJSONArray("HotDealsDetails");
										for (int ind = 0; ind < jsonRetailerList
												.length(); ind++) {
											JSONObject elemr = jsonRetailerList
													.getJSONObject(ind);
											hotdealsListbyProducts
													.add(getJSONValuesProducts(elemr));
										}
									} catch (Exception ed) {
										ed.printStackTrace();
										JSONObject jsonRetailerList = elem
												.getJSONObject("hDDetailsList")
												.getJSONObject(
														"HotDealsDetails");
										hotdealsListbyProducts
												.add(getJSONValuesProducts(jsonRetailerList));
									}
								}

							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				result = "false";
			}
			return result;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		callIntent();
	}

	private void callIntent() {
		Intent intent = new Intent();
		intent.setClass(this, DealsGallery.class);
		intent.putExtra("type", getIntent().getExtras().getString("type"));
		intent.putExtra("isSubMenu",
				getIntent().getExtras().getBoolean("isSubMenu"));

		if (mItemId != null && !"".equals(mItemId)) {
			intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
		} else if (mBottomId != null && !"".equals(mBottomId)) {
			intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mBottomId);
		}

		startActivity(intent);
		finish();

	}
}
