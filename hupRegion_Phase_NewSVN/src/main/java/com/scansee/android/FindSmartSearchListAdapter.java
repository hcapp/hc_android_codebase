package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;

public class FindSmartSearchListAdapter extends BaseAdapter {
	Activity activity;
	private ArrayList<HashMap<String, String>> findsmartsearchList;
	private static LayoutInflater inflater = null;

	public FindSmartSearchListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> findsmartsearchList) {
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.findsmartsearchList = findsmartsearchList;
	}

	@Override
	public int getCount() {
		return findsmartsearchList.size();
	}

	@Override
	public Object getItem(int id) {
		return findsmartsearchList.get(id);
	}

	@Override
	public long getItemId(int id) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (convertView == null){
			view = inflater.inflate(R.layout.listitem_find_smartsearch, parent ,false);
			holder = new ViewHolder();
			holder.searchNameTxt = (TextView) view
					.findViewById(R.id.find_smartsearch_text);
			holder.categoryNameTxt = (TextView) view
					.findViewById(R.id.find_smartsearch_text_bold);
			view.setTag(holder);
		}else{
			holder = (ViewHolder)view.getTag();
		}
		String searchName = findsmartsearchList.get(position).get(
				CommonConstants.FINDSMARTSEARCHCATEGORIES);
		if (searchName != null) {
			String[] searchArray = searchName.replace("|~|", "<br>").split("<br>");
			holder.searchNameTxt.setText(searchArray[0]);
			if (searchArray.length > 1) {
				holder.categoryNameTxt.setText(searchArray[1]);
			}
		}

		return view;
	}

	public static class ViewHolder {
		protected TextView searchNameTxt;
		protected TextView categoryNameTxt;
	}

}
