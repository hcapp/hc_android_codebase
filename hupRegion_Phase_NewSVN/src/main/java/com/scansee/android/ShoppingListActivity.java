package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;

public class ShoppingListActivity extends Activity implements OnClickListener {
	protected ArrayList<HashMap<String, String>> shoppingList = null,
			cartList = null, basketList = null;
	protected ArrayList<HashMap<String, String>> clrList = null;
	protected HashMap<String, String> loginData = null;
	protected ArrayList<HashMap<String, String>> selectedProducts = null;
	protected HashMap<String, String> selectedItem = null;
	String productId;
	protected ListView shoppingListListView;
	TextView unchecked, checked;
	protected ShoppingListListAdapter shoppingListListAdapter;
	CheckBox checkbox;
	String userId;
	ImageView cImage, lImage, rImage;
	protected TextView itemsUnchecked;
	protected TextView itemsChecked;
	int count;
	protected boolean isDelete = false;
	protected String userProdId = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shoppinglist_listview);

		cImage = (ImageView) findViewById(R.id.c_image);
		lImage = (ImageView) findViewById(R.id.l_image);
		rImage = (ImageView) findViewById(R.id.r_image);

		// get extras
		SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
		userId = settings.getString(CommonConstants.USER_ID, "0");

		itemsUnchecked = (TextView) findViewById(R.id.items_unchecked);
		itemsChecked = (TextView) findViewById(R.id.items_checked);
		findViewById(R.id.menu_nav_fav).setOnClickListener(this);
		findViewById(R.id.menu_nav_notes).setOnClickListener(this);
		findViewById(R.id.menu_nav_history).setOnClickListener(this);
		findViewById(R.id.menu_nav_mycoupons).setOnClickListener(this);
		findViewById(R.id.checkout_button).setOnClickListener(this);

		shoppingListListView = (ListView) findViewById(R.id.shoppinglist_listview_list);
		final ListItemGestures listItemGestures = new ListItemGestures(this);
		shoppingListListView.setOnTouchListener(listItemGestures);
		shoppingListListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				HashMap<String, String> selectedData = shoppingList
						.get(position);
				if (isDelete) {
					isDelete = false;
					view.findViewById(R.id.shoppinglist_delete).setAnimation(
							SlideAnimation.outToRightAnimation());
					view.findViewById(R.id.shoppinglist_delete).setVisibility(
							View.GONE);
					Parcelable state = shoppingListListView
							.onSaveInstanceState();
					shoppingListListAdapter = new ShoppingListListAdapter(
							ShoppingListActivity.this, shoppingList);
					shoppingListListView.setAdapter(shoppingListListAdapter);
					shoppingListListView.onRestoreInstanceState(state);

				} else {
					if (listItemGestures.swipeDetected()) {
						if (listItemGestures.getAction().equals(
								ListItemGestures.Action.REMOVE)) {
							return;
						}
						if ((listItemGestures.getAction().equals(
								ListItemGestures.Action.LR) || listItemGestures
								.getAction().equals(ListItemGestures.Action.RL))
								&& "productdetails"
										.equalsIgnoreCase(selectedData
												.get("itemName"))
								&& ("false".equalsIgnoreCase(shoppingList.get(
										position).get("selected")))) {
							isDelete = true;

							view.findViewById(R.id.shoppinglist_delete)
									.setAnimation(
											SlideAnimation
													.inFromRightAnimation());
							view.findViewById(R.id.shoppinglist_delete)
									.setVisibility(View.VISIBLE);

						}
					} else if ("productdetails".equalsIgnoreCase(selectedData
							.get("itemName"))) {
						Intent dealDetail = new Intent(
								ShoppingListActivity.this,
								RetailerCurrentsalesActivity.class);
						dealDetail
								.putExtra(
										CommonConstants.TAG_CURRENTSALES_PRODUCTID,
										selectedData
												.get(CommonConstants.SHOPPINGLIST_PRODUCTID));
						dealDetail
								.putExtra(
										CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
										selectedData
												.get(CommonConstants.SHOPPINGLIST_PRODUCTNAME));

						dealDetail.putExtra("ShoppingList", true);
						startActivity(dealDetail);
					}

				}
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		new GetShoppingList().execute();

	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	public class CheckOut extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(ShoppingListActivity.this,
					Constants.DIALOG_MESSAGE, "Checking out");
			mDialog.setCancelable(true);
		}

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {
				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.TAG_LOGIN_RESULTSET);

					loginData = new HashMap<>();
					loginData
							.put(CommonConstants.TAG_LOGIN_RESPONSE_CODE,
									String.valueOf(jsonObject
											.getInt(CommonConstants.TAG_LOGIN_RESPONSE_CODE)));
					loginData
							.put(CommonConstants.TAG_LOGIN_RESPONSETEXT,
									jsonObject
											.getString(CommonConstants.TAG_LOGIN_RESPONSETEXT));

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();

			if ("10000".equalsIgnoreCase(loginData
					.get(CommonConstants.TAG_LOGIN_RESPONSE_CODE))) {
				new GetShoppingList().execute();
			} else {
				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						ShoppingListActivity.this);
				notificationAlert.setMessage(getString(R.string.invalid_user))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				notificationAlert.create().show();

			}
		}

	}

	private class GetShoppingList extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null, jsonDataObject = null;
		JSONArray jsonCartArray = null, jsonBasketArray = null, jsonProdArray;
		HashMap<String, String> catData = null;
		Set<String> catNames = null;
		ArrayList<HashMap<String, String>> allProds = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(ShoppingListActivity.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {
			shoppingList = new ArrayList<>();
			String result = "false";
			try {
				if (jsonObject != null) {
					jsonCartArray = new JSONArray();
					try {
						jsonDataObject = jsonObject
								.getJSONObject(
										CommonConstants.SHOPPINGLISTRESULTSET)
								.getJSONObject(
										CommonConstants.SHOPPINGLISTRESULTSET_CARTMENU)
								.getJSONObject(
										CommonConstants.SHOPPINGLIST_CATEGORY);
						jsonCartArray.put(jsonDataObject);
					} catch (Exception e) {
						e.printStackTrace();
						try {

							jsonCartArray = jsonObject
									.getJSONObject(
											CommonConstants.SHOPPINGLISTRESULTSET)
									.getJSONObject(
											CommonConstants.SHOPPINGLISTRESULTSET_CARTMENU)
									.getJSONArray(
											CommonConstants.SHOPPINGLIST_CATEGORY);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					jsonBasketArray = new JSONArray();
					try {
						jsonDataObject = jsonObject
								.getJSONObject(
										CommonConstants.SHOPPINGLISTRESULTSET)
								.getJSONObject(
										CommonConstants.SHOPPINGLISTRESULTSET_BASKETMENU)
								.getJSONObject(
										CommonConstants.SHOPPINGLIST_CATEGORY);
						jsonBasketArray.put(jsonDataObject);
					} catch (Exception e) {
						e.printStackTrace();
						try {
							jsonBasketArray = jsonObject
									.getJSONObject(
											CommonConstants.SHOPPINGLISTRESULTSET)
									.getJSONObject(
											CommonConstants.SHOPPINGLISTRESULTSET_BASKETMENU)
									.getJSONArray(
											CommonConstants.SHOPPINGLIST_CATEGORY);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					allProds = new ArrayList<>();
					catNames = new TreeSet<>();
					for (int cartCount = 0; cartCount < jsonCartArray.length(); cartCount++) {
						popuList(jsonCartArray, cartCount, "false");
					}
					for (int basketCount = 0; basketCount < jsonBasketArray
							.length(); basketCount++) {
						popuList(jsonBasketArray, basketCount, "true");
					}
					catNames = new TreeSet<>(catNames);
					shoppingList = new ArrayList<>();
					for (String name : catNames) {
						catData = new HashMap<>();
						catData.put("itemName", "parentcategoryname");
						catData.put(
								CommonConstants.SHOPPINGLIST_PARENTCATEGORYNAME,
								name);
						shoppingList.add(catData);
						for (HashMap<String, String> prodData : allProds) {
							if (name.equalsIgnoreCase(prodData
									.get(CommonConstants.SHOPPINGLIST_PARENTCATEGORYNAME))) {
								shoppingList.add(prodData);
							}

						}

					}
				}
				result = "true";

			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		protected void popuList(JSONArray jsonArray, int cartCount,
				String isAdded) throws JSONException {
			catNames.add(jsonArray.getJSONObject(cartCount).getString(
					CommonConstants.SHOPPINGLIST_PARENTCATEGORYNAME));
			jsonProdArray = new JSONArray();
			try {
				jsonDataObject = jsonArray.getJSONObject(cartCount)
						.getJSONObject(
								CommonConstants.SHOPPINGLIST_PRODUCTDETAIL);
				jsonProdArray.put(jsonDataObject);
			} catch (Exception e) {
				e.printStackTrace();
				jsonProdArray = jsonArray.getJSONObject(cartCount)
						.getJSONArray(
								CommonConstants.SHOPPINGLIST_PRODUCTDETAIL);
			}
			for (int prodCount = 0; prodCount < jsonProdArray.length(); prodCount++) {
				catData = new HashMap<>();
				catData.put("itemName", "productdetails");
				catData.put("selected", isAdded);
				catData.put("categoryID", jsonProdArray
						.getJSONObject(prodCount).getString("categoryID"));
				catData.put(
						"parentCategoryName",
						jsonProdArray.getJSONObject(prodCount).getString(
								"parentCategoryName"));
				catData.put(
						CommonConstants.SHOPPINGLIST_PRODUCTIMAGEPATH,
						jsonProdArray.getJSONObject(prodCount).getString(
								CommonConstants.SHOPPINGLIST_PRODUCTIMAGEPATH));
				catData.put(
						CommonConstants.SHOPPINGLIST_COUPONSTATUS,
						jsonProdArray.getJSONObject(prodCount).getString(
								CommonConstants.SHOPPINGLIST_COUPONSTATUS));
				catData.put(
						CommonConstants.SHOPPINGLIST_REBATESTATUS,
						jsonProdArray.getJSONObject(prodCount).getString(
								CommonConstants.SHOPPINGLIST_REBATESTATUS));
				catData.put(
						CommonConstants.SHOPPINGLIST_LOYALTYSTATUS,
						jsonProdArray.getJSONObject(prodCount).getString(
								CommonConstants.SHOPPINGLIST_LOYALTYSTATUS));
				catData.put(
						CommonConstants.SHOPPINGLIST_PRODUCTNAME,
						jsonProdArray.getJSONObject(prodCount).getString(
								CommonConstants.SHOPPINGLIST_PRODUCTNAME));
				catData.put(
						CommonConstants.SHOPPINGLIST_PRODUCTID,
						jsonProdArray.getJSONObject(prodCount).getString(
								CommonConstants.SHOPPINGLIST_PRODUCTID));
				catData.put(
						CommonConstants.SHOPPINGLIST_PRODUCTSHORTDESC,
						jsonProdArray.getJSONObject(prodCount).getString(
								CommonConstants.SHOPPINGLIST_PRODUCTSHORTDESC));
				catData.put(
						CommonConstants.SHOPPINGLIST_USERPRODUCTID,
						jsonProdArray.getJSONObject(prodCount).getString(
								CommonConstants.SHOPPINGLIST_USERPRODUCTID));
				allProds.add(catData);

			}
		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();

			if ("true".equals(result)) {

				shoppingListListAdapter = new ShoppingListListAdapter(
						ShoppingListActivity.this, shoppingList);
				shoppingListListView.setAdapter(shoppingListListAdapter);

			} else {
				Toast.makeText(getBaseContext(), "No Products Found .",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void saveHelper(HashMap<String, String> item) {
		if (shoppingList != null && !shoppingList.isEmpty()) {
			cartList = new ArrayList<>();
			basketList = new ArrayList<>();
			if ("true".equalsIgnoreCase(item.get("selected"))) {
				cartList.add(item);
			} else if ("false".equalsIgnoreCase(item.get("selected"))) {
				basketList.add(item);
			}
			new SaveCheckList().execute();
		}
	}

	private class SaveCheckList extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {

				if (jsonObject != null) {
					jsonObject = jsonObject.getJSONObject("response");
					result = jsonObject.getString("responseCode");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;

		}

		@Override
		protected void onPostExecute(String result) {
		}
	}

	public void deleteItem() {
		new DeleteItem().execute();
	}

	private class DeleteItem extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(ShoppingListActivity.this, "",
					"Performing Delete....", true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {
			shoppingList = new ArrayList<>();
			String result = "false";
			try {
				if (jsonObject != null) {
					jsonObject = jsonObject.getJSONObject("response");
					result = jsonObject.getString("responseCode");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;

		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();
			if (!"10000".equalsIgnoreCase(result)) {

				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						ShoppingListActivity.this);
				notificationAlert.setMessage("Delete Failed")
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				notificationAlert.create().show();
			}
			new GetShoppingList().execute();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.menu_nav_history:
			Toast.makeText(getApplicationContext(),
					"ShoppingListHistoryActivity.class", Toast.LENGTH_LONG)
					.show();
			break;
		case R.id.menu_nav_fav:
			Toast.makeText(getApplicationContext(), "FavoritesActivity.class",
					Toast.LENGTH_LONG).show();
			break;
		case R.id.menu_nav_mycoupons:
			Intent couponintent = new Intent(getApplicationContext(),
					CouponsActivty.class);
			startActivity(couponintent);
			break;
		case R.id.menu_nav_notes:
			Toast.makeText(getApplicationContext(),
					"ShoppingListNotesActivity.class", Toast.LENGTH_LONG)
					.show();
			break;
		case R.id.checkout_button:
			checkoutButton();
			break;

		default:
			break;
		}

	}

	private void checkoutButton() {
		AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
				ShoppingListActivity.this);
		notificationAlert
				.setMessage(getString(R.string.alert_checkout))
				.setPositiveButton(getString(R.string.specials_ok),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int id) {

								new CheckOut().execute();
							}
						})
				.setNegativeButton(getString(R.string.button_cancel),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		notificationAlert.create().show();
	}

}
