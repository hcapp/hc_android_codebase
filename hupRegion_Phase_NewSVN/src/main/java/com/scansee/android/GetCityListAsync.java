package com.scansee.android;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.hubcity.android.businessObjects.FilterOptionBO;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class GetCityListAsync extends AsyncTask<Void, Void, Void> {

    String mItemId, className, mBottomId, latitude, longitude, catName,
            srchKey, catIds, subCatId, mLinkId, typeId, deptId, level,
            citiExpID, fundId, retailerId, retailerLocationId, menuId,
            isRetailerEvents, radius;
    int groupPosition;
    Context mContext;
    boolean isDeals;
    ArrayList<FilterOptionBO> arrFilterOptionBOs;
    OnCreateFilterChildView filterChildView;

    private ProgressDialog mDialog;

    public GetCityListAsync(Context context, String mItemId, int groupPosition,
                            String catName, String srchKey, String cityExpId, String className,
                            String mBottomId, String latitude, String longitude, String catIds,
                            String subCatId, String mLinkId, String typeId, String deptId,
                            String level, String fundId, String retailerId,
                            String retailerLocationId, String isRetailerEvents,
                            boolean isDeals, OnCreateFilterChildView filterChildView,
                            String menuId, String radius) {
        this.mItemId = mItemId;
        this.groupPosition = groupPosition;
        this.mContext = context;
        this.catName = catName;
        this.srchKey = srchKey;
        this.citiExpID = cityExpId;
        this.className = className;
        this.mBottomId = mBottomId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.catIds = catIds;
        this.subCatId = subCatId;
        this.mLinkId = mLinkId;
        this.typeId = typeId;
        this.deptId = deptId;
        this.level = level;
        this.fundId = fundId;
        this.retailerId = retailerId;
        this.retailerLocationId = retailerLocationId;
        this.isRetailerEvents = isRetailerEvents;
        this.isDeals = isDeals;
        this.filterChildView = filterChildView;
        this.menuId = menuId;
        this.radius = radius;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mDialog = ProgressDialog.show(mContext, "", Constants.DIALOG_MESSAGE,
                true);
        mDialog.setCancelable(false);
    }

    @Override
    protected Void doInBackground(Void... params) {
        String urlParameters = "";

        if ("Find All".equals(className) || "Find Single".equals(className)) {
            urlParameters = new UrlRequestParams().createCityListParameterFind(
                    className, mItemId, mBottomId, latitude, longitude,
                    catName, srchKey, catIds, subCatId);
        } else if ("SubMenu".equals(className)) {
            urlParameters = new UrlRequestParams()
                    .createCityListParameterSubMenu(className, mLinkId, typeId,
                            deptId, level, menuId);
        } else {
            if (className.equals("Citi Exp"))
                className = "CitiEXP";
            if (isDeals) {
                urlParameters = new UrlRequestParams().createCityListParameter(
                        className, citiExpID, "", "", fundId, retailerId,
                        retailerLocationId, isRetailerEvents, radius, srchKey);
            } else {
                urlParameters = new UrlRequestParams().createCityListParameter(
                        className, citiExpID, mItemId, mBottomId, fundId,
                        retailerId, retailerLocationId, isRetailerEvents, radius,srchKey);
            }
        }

        String get_user_city_pref_url = Properties.url_local_server
                + Properties.hubciti_version + "firstuse/getusercitypref";

        JSONObject jsonResponse = new ServerConnections().getUrlPostResponse(
                get_user_city_pref_url, urlParameters, true);

        if (jsonResponse.has("City")) {
            try {
                if (jsonResponse.getJSONObject("City")
                        .getString("responseCode").equals("10000")) {

                    JSONArray jsonArray = null;
                    JSONObject jsonObject = null;
                    try {
                        jsonArray = jsonResponse.getJSONObject("City")
                                .getJSONObject("cityList").getJSONArray("City");
                    } catch (Exception e) {
                        jsonObject = jsonResponse.getJSONObject("City")
                                .getJSONObject("cityList")
                                .getJSONObject("City");
                    }
                    arrFilterOptionBOs = new ArrayList<>();
                    if (jsonArray != null) {
                        FilterOptionBO filterOption = new FilterOptionBO();
                        filterOption.setFilterGroupName("City");
                        filterOption.setFilterId("All");
                        filterOption.setChecked(true);
                        filterOption.setFilterName("All");
                        arrFilterOptionBOs.add(filterOption);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            FilterOptionBO filterOptionBO = new FilterOptionBO();
                            filterOptionBO.setFilterGroupName("City");
                            filterOptionBO.setFilterId(jsonArray.getJSONObject(
                                    i).getString("cityId"));
                            try {
                                if (jsonArray.getJSONObject(i)
                                        .getString("isCityChecked").equals("1"))
                                    filterOptionBO.setChecked(true);
                                else {
                                    filterOptionBO.setChecked(false);
                                    arrFilterOptionBOs.get(0).setChecked(false);
                                }
                            } catch (Exception e) {
                                arrFilterOptionBOs.get(0).setChecked(false);
                            }
                            filterOptionBO.setFilterName(jsonArray
                                    .getJSONObject(i).getString("cityName"));
                            arrFilterOptionBOs.add(filterOptionBO);
                        }
                    } else {
                        FilterOptionBO filterOptionBO = new FilterOptionBO();
                        filterOptionBO.setFilterGroupName("City");
                        filterOptionBO.setFilterId(jsonObject
                                .getString("cityId"));
                        try {
                            if (jsonObject.getString("isCityChecked").equals(
                                    "1"))
                                filterOptionBO.setChecked(true);
                            else
                                filterOptionBO.setChecked(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        filterOptionBO.setFilterName(jsonObject
                                .getString("cityName"));
                        arrFilterOptionBOs.add(filterOptionBO);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (arrFilterOptionBOs != null) {
            try {
                ((FilterAndSortScreen) mContext).setFilterOptionsForChildView(
                        arrFilterOptionBOs, groupPosition, "city");
            } catch (Exception e) {
                filterChildView.setFilterOptionsForChildView(
                        arrFilterOptionBOs, groupPosition, "city");
            }
        } else
            Toast.makeText(mContext, "No Records Found", Toast.LENGTH_SHORT)
                    .show();

        mDialog.dismiss();
    }

}
