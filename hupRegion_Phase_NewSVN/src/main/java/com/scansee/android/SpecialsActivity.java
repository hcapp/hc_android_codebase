package com.scansee.android;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.RetailersDetailsDescription;
import com.scansee.hubregion.R;

public class SpecialsActivity extends CustomTitleBar implements
		OnClickListener, OnItemClickListener {

	private ProgressDialog progDialog;
	protected ListView listview;

	ArrayList<Item> items = new ArrayList<>();
	View paginator;

	SpecialsListAdapter specialsListAdapter = null;
	ListDetails objListDetails;
	LocationManager locationManager;
	SpecialOfferDisplay specialsDisplay;
	String mItemId, mBottomId;

	// For Bottom Buttons
	LinearLayout linearLayout = null;
	BottomButtons bb = null;
	Activity activity;
	boolean hasBottomBtns = false;
	boolean enableBottomButton = true;

	String responseText;
	String returnValue = "sucessfull";
	String latitude = null;
	String longitude = null;

	int lastvisitProdId = 0, minProdId = 0;

	boolean isViewMoreClicked = false;
	boolean isFirst = false;
	boolean isfirstEntry = true;

	JSONObject jsonBB = null;
	private CustomNavigation customNaviagation;
	private Context mContext;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.special_offer_list);

		try {
			CommonConstants.hamburgerIsFirst = true;
			isFirst = true;

			title.setText("Specials");

			activity = SpecialsActivity.this;
			leftTitleImage.setVisibility(View.GONE);

			// Initiating Bottom button class
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "");

			linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
			linearLayout.setVisibility(View.INVISIBLE);

			try {
                if (getIntent().hasExtra("jsonBB")
                        && getIntent().getExtras().getString("jsonBB") != null) {
                    String jsonString = getIntent().getStringExtra("jsonBB");
                    jsonBB = new JSONObject(jsonString);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTask();
                    finish();
                }
            });

			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);

            }

			if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
                mBottomId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);

            }

			paginator = getLayoutInflater().inflate(
                    R.layout.listitem_get_retailers_viewmore, listview, true);
			listview = (ListView) findViewById(R.id.special_offerList);
			objListDetails = new ListDetails();
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

			boolean gpsEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
			gpsUpdate();
			if ((!gpsEnabled) && !(Constants.getZipCode().length() > 0)) {
                if (isfirstEntry) {
                    callSpecialsAsyncTask();
                }
            } else {
                callSpecialsAsyncTask();
            }


			//user for hamburger in modules
			drawerIcon.setVisibility(View.VISIBLE);
			drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void callSpecialsAsyncTask() {
		if (isFirst) {
			specialsDisplay = new SpecialOfferDisplay();
			specialsDisplay.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;

		}

		if (specialsDisplay != null) {
			specialsDisplay.cancel(true);
		}

		specialsDisplay = null;

		specialsDisplay = new SpecialOfferDisplay();
		specialsDisplay.execute();
	}

	private void cancelAsyncTask() {
		if (specialsDisplay != null && !specialsDisplay.isCancelled()) {
			specialsDisplay.cancel(true);
		}

		specialsDisplay = null;
	}

	@SuppressWarnings("deprecation")
	private void gpsUpdate() {
		latitude = null;
		longitude = null;
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		String provider = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

			if (provider.contains("gps")&& gpsEnabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (locNew != null) {

				latitude = String.valueOf(locNew.getLatitude());
				longitude = String.valueOf(locNew.getLongitude());

			} else {
				// N/W Tower Info Start
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					latitude = String.valueOf(locNew.getLatitude());
					longitude = String.valueOf(locNew.getLongitude());
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}

		try {
			objListDetails = new ListDetails();
			listview.setAdapter(null);
			lastvisitProdId = 0;
			gpsUpdate();
			if (!isfirstEntry) {
                callSpecialsAsyncTask();
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			cancelAsyncTask();
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	protected void viewMore() {
		isAlreadyLoading = true;
		isViewMoreClicked = true;
		callSpecialsAsyncTask();
	}

	boolean nextPage, isAlreadyLoading;

	// Calling Special offers to be displayed
	public class SpecialOfferDisplay extends AsyncTask<String, Void, String> {

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (isViewMoreClicked) {
				listview.removeFooterView(paginator);
			} else {
				progDialog = new ProgressDialog(SpecialsActivity.this);
				progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progDialog.setMessage(Constants.DIALOG_MESSAGE);
				progDialog.setCancelable(false);
				progDialog.show();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				isAlreadyLoading = false;
				if ("sucessfull".equals(returnValue)) {
                    Parcelable state = listview.onSaveInstanceState();
                    if (objListDetails != null && objListDetails.alertList != null) {

                        specialsListAdapter = new SpecialsListAdapter(
                                SpecialsActivity.this, objListDetails.alertList);
                    }
                    findViewById(R.id.no_coupons_found).setVisibility(
                            View.INVISIBLE);

                    if (nextPage) {

                        listview.removeFooterView(paginator);

                        LayoutInflater inflater = (LayoutInflater) SpecialsActivity.this
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        paginator = inflater.inflate(
                                R.layout.listitem_get_retailers_viewmore, null);
                        listview.addFooterView(paginator);
                        paginator.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // isViewMoreClicked = true;
                                // callSpecialsAsyncTask();

                            }
                        });

                    } else {
                        listview.removeFooterView(paginator);
                    }
                    if (specialsListAdapter != null) {
                        listview.setAdapter(specialsListAdapter);
                        listview.setOnItemClickListener(SpecialsActivity.this);
                        listview.onRestoreInstanceState(state);
                    } else {
                        findViewById(R.id.no_coupons_found).setVisibility(
                                View.VISIBLE);
                    }
                    isfirstEntry = false;
                } else {
                    listview.setAdapter(null);
                    findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
                    if (isfirstEntry) {
                        isfirstEntry = false;
                    }
                }

				if (hasBottomBtns && enableBottomButton) {

                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) listview
                            .getLayoutParams();
                    mlp.setMargins(0, 0, 0, 2);

                    bb.createbottomButtontTab(linearLayout, false);
                    enableBottomButton = false;
                }

				linearLayout.setVisibility(View.VISIBLE);

				if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				minProdId = lastvisitProdId;
				String url_getspecialofferretdisp = Properties.url_local_server
						+ Properties.hubciti_version
						+ "gallery/getspecialofferretdisp";
				String urlParameters = mUrlRequestParams.getSpecialOfferDisp(
						lastvisitProdId + "", latitude, longitude);
				JSONObject jsonRespone = mServerConnections.getUrlPostResponse(
						url_getspecialofferretdisp, urlParameters, true);
				if (jsonRespone.has("RetailersDetails")) {
					JSONObject jsonRetailersDetails = jsonRespone
							.getJSONObject("RetailersDetails");

					JSONObject jsonRetailers = jsonRetailersDetails
							.getJSONObject("retailers");
					if (jsonRetailersDetails.has("mainMenuId")) {
						Constants.saveMainMenuId(jsonRetailersDetails
								.getString("mainMenuId"));
					}
					try {
						try {
							JSONArray jsonRetailerArray = jsonRetailers
									.getJSONArray("Retailer");
							if (jsonRetailerArray != null) {

								try {

									nextPage = "1".equalsIgnoreCase(jsonRetailersDetails
											.getString("nextPage"));
								} catch (Exception e) {
									e.printStackTrace();
								}

								for (int i = 0; i < jsonRetailerArray.length(); i++) {
									JSONObject elem = jsonRetailerArray
											.getJSONObject(i);
									if (elem != null) {
										RetailerGroup objRetailerGroup = new RetailerGroup();
										objRetailerGroup.retailLocationId = elem
												.getString("retailLocationId");
										objRetailerGroup.distance = elem
												.getString("distance");
										objRetailerGroup.rowNumber = elem
												.getString("rowNumber");
										objRetailerGroup.retailerName = elem
												.getString("retailerName");
										objRetailerGroup.retailerId = elem
												.getString("retailerId");
										objRetailerGroup.retailAddress = elem
												.getString("retailAddress");
										objRetailerGroup.retailerImagePath = elem
												.getString("retailImgPath");
										objListDetails.alertList
												.add(objRetailerGroup);
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
							JSONObject elem = jsonRetailers
									.getJSONObject("Retailer");
							if (elem != null) {
								RetailerGroup objRetailerGroup = new RetailerGroup();
								objRetailerGroup.retailLocationId = elem
										.getString("retailLocationId");
								objRetailerGroup.distance = elem
										.getString("distance");
								objRetailerGroup.rowNumber = elem
										.getString("rowNumber");
								objRetailerGroup.retailerName = elem
										.getString("retailerName");
								objRetailerGroup.retailerId = elem
										.getString("retailerId");
								objRetailerGroup.retailAddress = elem
										.getString("retailAddress");
								objRetailerGroup.retailerImagePath = elem
										.getString("retailImgPath");
								objListDetails.alertList.add(objRetailerGroup);
							}
						}
						try {
							if (Integer.valueOf(jsonRetailersDetails
									.getString("maxRowNum")) > lastvisitProdId) {
								lastvisitProdId = Integer
										.valueOf(jsonRetailersDetails
												.getString("maxRowNum"));

							}

						} catch (Exception e) {
							e.printStackTrace();
						}

					} catch (Exception e) {
						e.printStackTrace();
						returnValue = "unsuccess";

					}

					// If there are bottom buttons
					if (jsonBB != null) {

						hasBottomBtns = true;

						try {
							ArrayList<BottomButtonBO> bottomButtonList = bb
									.parseForBottomButton(jsonBB);
							BottomButtonListSingleton
									.clearBottomButtonListSingleton();
							BottomButtonListSingleton
									.getListBottomButton(bottomButtonList);

						} catch (JSONException e1) {
							e1.printStackTrace();
						}

					} else {
						hasBottomBtns = false;
					}

				} else {
					JSONObject jsonRecordNotFound = jsonRespone
							.getJSONObject("response");
					responseText = jsonRecordNotFound.getString("responseText");

					returnValue = "unsuccess";
				}
			} catch (Exception e) {
				e.printStackTrace();
				returnValue = "unsuccess";

			}

			return returnValue;
		}
	}

	// List RetailerGroup
	public class ListDetails {
		ArrayList<RetailerGroup> alertList = new ArrayList<>();

	}

	// RetailerGroup Business logic
	class RetailerGroup {
		String retailLocationId;
		String distance;
		String retailerName;
		String retailerId;
		String retailAddress;
		String rowNumber;
		String retailerImagePath;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.hotdeals_prev_page:
			specialsPrevPage();
			break;
		case R.id.hotdeals_next_page:
			callSpecialsAsyncTask();
			break;
		default:
			break;
		}

	}

	private void specialsPrevPage() {
		lastvisitProdId = 0 > (minProdId - Constants.RANGE) ? 0 : minProdId
				- Constants.RANGE;
		callSpecialsAsyncTask();

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {

		try {
			retailersDetailsDescription(position);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void retailersDetailsDescription(int position) {
		RetailerGroup item = objListDetails.alertList.get(position);
		Intent alertDescripNav = new Intent(getApplicationContext(),
				RetailersDetailsDescription.class);
		alertDescripNav.putExtra("retailLocationId", item.retailLocationId);
		alertDescripNav.putExtra("retailerId", item.retailerId);
		alertDescripNav.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
		alertDescripNav.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
				mBottomId);
		startActivityForResult(alertDescripNav, 101);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cancelAsyncTask();
	}

}