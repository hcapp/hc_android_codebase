package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.screens.CurrentSpecialsListActivity;

public class CouponClaimRedeemExpiredListAdapter1 extends BaseAdapter implements
		OnClickListener {

	private CurrentSpecialsListActivity activity;
	private ArrayList<HashMap<String, String>> allcouponsList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	String itemName = null;
	HashMap<String, String> couponData = null;

	public CouponClaimRedeemExpiredListAdapter1(
			CurrentSpecialsListActivity currentSpecialsListActivity,
			ArrayList<HashMap<String, String>> allcouponsList) {
		this.activity = currentSpecialsListActivity;
		inflater = (LayoutInflater) currentSpecialsListActivity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.allcouponsList = allcouponsList;
		customImageLoader = new CustomImageLoader(currentSpecialsListActivity.getApplicationContext(), false);

	}

	@Override
	public int getCount() {
		return allcouponsList.size();
	}

	@Override
	public Object getItem(int id) {
		return allcouponsList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;

		couponData = allcouponsList.get(position);
		itemName = couponData.get("itemName");

		viewHolder = new ViewHolder();

		if ("category".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_hotdeals_categoryname,
					parent,false);
			viewHolder.categoryName = (TextView) view
					.findViewById(R.id.hotdeals_categoryname);
		} else if ("coupons".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_coupons, parent,false);
			viewHolder.couponimage = (ImageView) view
					.findViewById(R.id.coupon_image);
			viewHolder.couponname = (TextView) view
					.findViewById(R.id.coupon_product_name);
			viewHolder.couponimagePath = (ImageView) view
					.findViewById(R.id.coupon_imagepath);
			viewHolder.coupondiscountprice = (TextView) view
					.findViewById(R.id.coupon_product_couponDiscountAmount);
			viewHolder.couponexpiretext = (TextView) view
					.findViewById(R.id.coupon_product_expires);
			viewHolder.couponexpiredate = (TextView) view
					.findViewById(R.id.coupon_product_couponExpireDate);
			view.findViewById(R.id.ribbon_imagepath).setVisibility(View.GONE);
		}
		if ("location".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_hotdeals_categoryname,
					parent,false);
			viewHolder.locationName = (TextView) view
					.findViewById(R.id.hotdeals_categoryname);
		}
		if ("location".equalsIgnoreCase(itemName)) {
			viewHolder.locationName.setText(couponData
					.get(CommonConstants.ALLCOUPONSLOCATION));
		} else if ("category".equalsIgnoreCase(itemName)) {
			viewHolder.categoryName.setText(couponData
					.get(CommonConstants.ALLCOUPONSCOUPONCATNAME));
		} else if ("coupons".equalsIgnoreCase(itemName)) {
			viewHolder.couponname.setText(allcouponsList.get(position).get(
					CommonConstants.ALLCOUPONSCOUPONNAME));
			String usedFlag = couponData.get("couponUsed");
			viewHolder.couponimage.setTag(position);
			if ("0".equals(usedFlag)) {
				viewHolder.couponimage
						.setBackgroundResource(R.drawable.wl_add_cpn_live);
			} else {
				viewHolder.couponimage
						.setBackgroundResource(R.drawable.wl_add_cpn_off);
			}
			viewHolder.couponimage.setOnClickListener(this);
			viewHolder.couponimage.setVisibility(View.GONE);

			viewHolder.coupondiscountprice.setText(allcouponsList.get(position)
					.get(CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT));
			viewHolder.couponexpiretext.setText("off Expires");
			viewHolder.couponexpiredate.setText(allcouponsList.get(position)
					.get(CommonConstants.ALLCOUPONSEXPIRATIONDATE));
			viewHolder.couponimagePath.setTag(allcouponsList.get(position).get(
					CommonConstants.ALLCOUPONSIMAGEPATH));
			customImageLoader.displayImage(
					allcouponsList.get(position).get(
							CommonConstants.ALLCOUPONSIMAGEPATH), activity,
					viewHolder.couponimagePath);

		}

		else if ("viewMore".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_get_retailers_viewmore,
					parent,false);

		}
		return view;

	}

	public static class ViewHolder {
		protected TextView locationName;
		protected TextView categoryName;
		protected ImageView couponDollar;
		protected ImageView couponimage;
		protected TextView couponexpiredate;
		protected TextView couponexpiretext;
		protected TextView coupondiscountprice;
		protected ImageView couponimagePath;
		protected TextView couponname;

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.coupon_delete:

			break;

		case R.id.coupon_image:

			break;

		default:
			break;
		}

	}

}
