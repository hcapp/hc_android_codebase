package com.scansee.android;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.hubcity.android.businessObjects.FilterOptionBO;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class GetPriceListAsync extends AsyncTask<Void, Void, Void> {

	Context mContext;
	String mItemId, catId, fName, latitude, longitude, bottomBtnId, searchKey;
	ArrayList<FilterOptionBO> arrFilterOptionBOs;
	int groupPosition;
	OnCreateFilterChildView filterChildView;

	private ProgressDialog mDialog;

	public GetPriceListAsync(Context context, String mItemId,
			int groupPosition, String catId, String fName, String latitude,
			String longitude, OnCreateFilterChildView filterChildView,
			String bottomBtnId, String searchKey) {
		this.mContext = context;
		this.mItemId = mItemId;
		this.catId = catId;
		this.fName = fName;
		this.groupPosition = groupPosition;
		this.filterChildView = filterChildView;
		this.bottomBtnId = bottomBtnId;
		this.searchKey = searchKey;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		mDialog = ProgressDialog.show(mContext, "", Constants.DIALOG_MESSAGE,
				true);
		mDialog.setCancelable(false);
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {

			String get_filters_url = Properties.url_local_server
					+ Properties.hubciti_version + "find/getfilterlist";

			String urlParameters = new UrlRequestParams()
					.createPriceFilterParam(catId, "", "", latitude, longitude,
							mItemId, bottomBtnId, fName, searchKey);
			JSONObject jsonResponse = new ServerConnections()
					.getUrlPostResponse(get_filters_url, urlParameters, true);
			if (jsonResponse.getJSONObject("Filter").getString("responseCode")
					.equals("10000")) {

				JSONArray jsonArray = null;
				JSONObject jsonObject = null;
				try {
					jsonArray = jsonResponse.getJSONObject("Filter")
							.getJSONObject("filterList").getJSONArray("Filter");
				} catch (Exception e) {
					jsonObject = jsonResponse.getJSONObject("Filter")
							.getJSONObject("filterList")
							.getJSONObject("Filter");
				}
				arrFilterOptionBOs = new ArrayList<>();
				if (jsonArray != null) {
					FilterOptionBO filterOption = new FilterOptionBO();
					filterOption.setFilterGroupName(fName);
					filterOption.setFilterId("");
					filterOption.setFilterName("All");
					arrFilterOptionBOs.add(filterOption);
					for (int i = 0; i < jsonArray.length(); i++) {
						FilterOptionBO filterOptionBO = new FilterOptionBO();
						filterOptionBO.setFilterGroupName(fName);
						filterOptionBO.setFilterId(jsonArray.getJSONObject(i)
								.getString("fValueId"));
						filterOptionBO.setFilterName(jsonArray.getJSONObject(i)
								.getString("fValueName"));
						arrFilterOptionBOs.add(filterOptionBO);
					}
				} else {
					FilterOptionBO filterOptionBO = new FilterOptionBO();
					filterOptionBO.setFilterGroupName(fName);
					filterOptionBO
							.setFilterId(jsonObject.getString("fValueId"));
					filterOptionBO.setFilterName(jsonObject
							.getString("fValueName"));
					arrFilterOptionBOs.add(filterOptionBO);
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (arrFilterOptionBOs != null) {
			try {
				((FilterAndSortScreen) mContext).setFilterOptionsForChildView(
						arrFilterOptionBOs, groupPosition, "price");
			} catch (Exception e) {
				filterChildView.setFilterOptionsForChildView(
						arrFilterOptionBOs, groupPosition, "price");
			}
		} else
			Toast.makeText(mContext, "No Records Found", Toast.LENGTH_SHORT)
					.show();

		mDialog.dismiss();
	}

}
