package com.scansee.android;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.scansee.hubregion.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by supriya.m on 4/20/2016.
 */
public class BandsListAdapter extends BaseAdapter
{
	private ArrayList<BandListingSetGet> arrBands;
	private LayoutInflater inflater;
	private Context mContext;

	public BandsListAdapter(Context context, ArrayList<BandListingSetGet> arrBands)
	{
		this.arrBands = arrBands;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mContext = context;
	}

	@Override
	public int getCount()
	{
		return arrBands.size();
	}

	@Override
	public Object getItem(int position)
	{
		return null;
	}

	@Override
	public long getItemId(int position)
	{
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{

		ViewHolder viewHolder;
		/***
		 * If the list item reaches to its last position then it will be called
		 * for next pagination
		 ***/
		if (position == arrBands.size() - 1) {
			if (!((BandsLandingPageActivity) mContext).isAlreadyLoading) {
				if (((BandsLandingPageActivity) mContext).nextPage) {
					((BandsLandingPageActivity) mContext).loadMoreData();
				}
			}
		}

		if(arrBands.get(position).isGroup()){
			convertView = inflater.inflate(R.layout.events_list_item_section, null);
			TextView sectionView = (TextView) convertView
					.findViewById(R.id.events_list_item_section_text);
			sectionView.setBackgroundColor(mContext.getResources().getColor(R.color.light_grey));
			sectionView.setGravity(Gravity.LEFT);
			sectionView.setPadding(5,0,0,0);
			sectionView.setTextColor(mContext.getResources().getColor(R.color.black));
			sectionView.setText(arrBands.get(position).getGroupName());
		}else {

				convertView = inflater.inflate(R.layout.band_listing_item, null);
				viewHolder = new ViewHolder();
				viewHolder.bandName = (TextView) convertView.findViewById(R.id
						.band_listing_item_band_name);
				viewHolder.bandCategoryName = (TextView) convertView.findViewById(R.id
						.band_listing_item_band_category_name);
				viewHolder.bandImage = (ImageView) convertView.findViewById(R.id
						.band_listing_item_image);

			viewHolder.bandName.setText(arrBands.get(position).getBandName());
			viewHolder.bandCategoryName.setText(arrBands.get(position).getBandCategoryName());
			if (arrBands.get(position).getBandImageUrl() != null && !arrBands.get(position)
					.getBandImageUrl().equals("")) {
				Picasso.with(mContext).load(arrBands.get(position).getBandImageUrl().replace(" ",
						"%20")).error(R.drawable.ic_empty_icon)
						.placeholder(R.drawable.loading_animation)
						.into(viewHolder.bandImage);
			}
		}
		return convertView;
	}

	public class ViewHolder
	{
		TextView bandName, bandCategoryName;
		ImageView bandImage;
	}
}
