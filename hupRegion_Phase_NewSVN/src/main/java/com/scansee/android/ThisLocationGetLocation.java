package com.scansee.android;

import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.screens.CustomNavigation;
import com.scansee.hubregion.R;

public class ThisLocationGetLocation extends CustomTitleBar implements
		OnClickListener {
	SharedPreferences settings;
	TextView zipCode;
	String zipCodeText = null,  userId;

	TextView locateOnMap;
	String latitude;
	String longitude;

	LocationManager locationManager;
	EditText zipcodeEditTxt;
	String userCongfZiCode;
	private String mBkgrdColor;
	private String mBkgrdImage;
	private String mBtnColor;
	private String mBtnFontColor;
	Button okBtn;

	TextView tor;
	TextView tzipcode;
	LinearLayout layoutNearby;
	String mItemId, bottomBtnId;
	private CustomNavigation customNaviagation;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			CommonConstants.hamburgerIsFirst = true;
			title.setSingleLine(true);
			title.setText("Nearby");

			leftTitleImage.setVisibility(View.GONE);

			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			String provider = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			mItemId = getIntent().getExtras().getString("mItemId");
			bottomBtnId = getIntent().getExtras().getString("bottomBtnId");

			if (provider.contains("gps")) {

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                        CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                        new ScanSeeLocListener());
                Location locNew = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (locNew != null) {

                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());

                } else {
                    // N/W Tower Info Start
                    locNew = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (locNew != null) {
                        latitude = String.valueOf(locNew.getLatitude());
                        longitude = String.valueOf(locNew.getLongitude());
                    } else {
                        latitude = CommonConstants.LATITUDE;
                        longitude = CommonConstants.LONGITUDE;
                    }
                    // N/W Tower Info end

                }
                Intent getRetailer = new Intent(getApplicationContext(),
                        ThisLocationGetRetailers.class);

                getRetailer.putExtra("mapLat", latitude);
                getRetailer.putExtra("mItemId", mItemId);
                getRetailer.putExtra("bottomBtnId", bottomBtnId);
                getRetailer.putExtra("mapLng", longitude);
                getRetailer.putExtra("fromLocateMap", "fromLocateMap");
                finish();
                this.startActivityForResult(getRetailer, Constants.STARTVALUE);

            } else {

                setContentView(R.layout.thislocation_zipcode);

                mBkgrdColor = getIntent().getExtras().getString("mBkgrdColor");
                mBkgrdImage = getIntent().getExtras().getString("mBkgrdImage");
                mBtnColor = getIntent().getExtras().getString("mBtnColor");
                mBtnFontColor = getIntent().getExtras().getString("mBtnFontColor");
                okBtn = (Button) findViewById(R.id.btn_location_ok);
                tor = (TextView) findViewById(R.id.or_txtview);
                tzipcode = (TextView) findViewById(R.id.tzipcode);
                layoutNearby = (LinearLayout) findViewById(R.id.layout_nearby);

                setScreenUI();

                userCongfZiCode = Constants.getZipCode();
                settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);

                userId = settings.getString(CommonConstants.USER_ID, "0");
                settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);

                zipCode = (TextView) findViewById(R.id.txt_location_zipcode);
                zipcodeEditTxt = (EditText) findViewById(R.id.txt_location_zipcode);
                locateOnMap = (TextView) findViewById(R.id.btn_location_locate_on_map);

                if (userCongfZiCode != null && !"".equals(userCongfZiCode)) {

                    zipCode.setText(userCongfZiCode);
                    zipcodeEditTxt.setSelection(zipCode.getText().length());
                    locateOnMap
                            .setBackgroundResource(R.drawable.round_corner_change_color);

                    GradientDrawable locateOnMapShape = (GradientDrawable) locateOnMap
                            .getBackground();
                    if ((mBtnColor != null) && !("N/A".equalsIgnoreCase(mBtnColor))) {
                        locateOnMapShape.setColor(Color.parseColor(mBtnColor));
                    }
                    if ((mBtnFontColor != null)
                            && !("N/A".equalsIgnoreCase(mBtnFontColor))) {
                        locateOnMap.setTextColor(Color.parseColor(mBtnFontColor));
                    }

                    locateOnMap.setEnabled(true);

                }

                zipcodeEditTxt.addTextChangedListener(new TextWatcher() {
                    TextView zipcodeVal = (TextView) findViewById(R.id.txt_location_zipcode);

                    public void afterTextChanged(Editable s) {
                    }

                    public void beforeTextChanged(CharSequence s, int start,
                            int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start,
                            int before, int count) {
                        final GradientDrawable locateOnMapShape = (GradientDrawable) locateOnMap
                                .getBackground();
                        locateOnMapShape.mutate();

                        if (zipcodeVal.getText().toString().length() == 5) {

                            if ((mBtnColor != null)
                                    && !("N/A".equalsIgnoreCase(mBtnColor))) {
                                locateOnMapShape.setColor(Color
                                        .parseColor(mBtnColor));
                            }
                            if ((mBtnFontColor != null)
                                    && !("N/A".equalsIgnoreCase(mBtnFontColor))) {
                                locateOnMap.setTextColor(Color
                                        .parseColor(mBtnFontColor));
                            }
                            locateOnMap.setEnabled(true);
                        } else {
                            locateOnMapShape.setColor(Color.WHITE);
                            locateOnMap.setTextColor(Color.GRAY);
                            locateOnMap.setEnabled(false);
                        }
                    }
                });

                findViewById(R.id.btn_location_ok).setOnClickListener(this);
                findViewById(R.id.btn_location_locate_on_map).setOnClickListener(
                        this);

				//user for hamburger in modules
				drawerIcon.setVisibility(View.VISIBLE);
				drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
				customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
            }

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}
	}

	private void setScreenUI() {

		if ((mBkgrdColor != null) && !("N/A".equalsIgnoreCase(mBkgrdColor))) {
			layoutNearby.setBackgroundColor(Color.parseColor(mBkgrdColor));

		} else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
			new ImageLoaderAsync1(layoutNearby).execute(mBkgrdImage);
		}
		GradientDrawable okBtnShape = (GradientDrawable) okBtn.getBackground();
		if ((mBtnColor != null) && !("N/A".equalsIgnoreCase(mBtnColor))) {
			okBtnShape.setColor(Color.parseColor(mBtnColor));
		}

		if ((mBtnFontColor != null) && !("N/A".equalsIgnoreCase(mBtnFontColor))) {
			okBtn.setTextColor(Color.parseColor(mBtnFontColor));
			tzipcode.setTextColor(Color.parseColor(mBtnFontColor));
			tor.setTextColor(Color.parseColor(mBtnFontColor));
		}

	}

	@Override
	public void onClick(View view) {
		String enterzipCode = zipCode.getText().toString();
		switch (view.getId()) {
		case R.id.btn_location_ok:
			btnLocationOk(enterzipCode);
			break;
		case R.id.btn_location_locate_on_map:
			btnLocationLocateOnMap(enterzipCode);
			break;
		default:
			break;
		}

	}

	private void btnLocationLocateOnMap(String enterzipCode) {
		zipCodeText = zipCode.getText().toString();

		Intent showMapIntent = new Intent(ThisLocationGetLocation.this,
				LocateOnMapActivity.class);
		showMapIntent.putExtra(CommonConstants.ZIP_CODE, enterzipCode);
		startActivityForResult(showMapIntent, 10);

	}

	private void btnLocationOk(String enterzipCode) {
		if ("".equals(enterzipCode)) {

			Toast toast = Toast.makeText(getBaseContext(),
					"Please Enter Valid ZipCode", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();

		} else {
			if (enterzipCode.length() < 5) {
				Toast toast = Toast.makeText(getBaseContext(),
						"Please Enter Valid ZipCode", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();

			} else {

				Editor edit = settings.edit();
				edit.putString("ZipCode", zipCode.getText().toString());
				edit.apply();
				Intent getLocationIntent = new Intent(getApplicationContext(),
						ThisLocationGetRetailers.class);
				getLocationIntent.putExtra(CommonConstants.ZIP_CODE,
						enterzipCode);
				getLocationIntent.putExtra("mItemId", mItemId);
				getLocationIntent.putExtra("bottomBtnId", bottomBtnId);
				this.startActivityForResult(getLocationIntent,
						Constants.STARTVALUE);
			}

		}

	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}
		if (resultCode == 10) {
			zipCode.setText("");
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	class ImageLoaderAsync1 extends AsyncTask<String, Void, Bitmap> {
		View bmImage;

		public ImageLoaderAsync1(View bmImage) {
			this.bmImage = bmImage;
		}

		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {

			mDialog = ProgressDialog.show(ThisLocationGetLocation.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			urldisplay = urldisplay.replaceAll(" ", "%20");

			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return mIcon11;
		}

		@SuppressWarnings("deprecation")
		protected void onPostExecute(Bitmap bmImg) {

			try {
				BitmapDrawable background = new BitmapDrawable(bmImg);
				int sdk = android.os.Build.VERSION.SDK_INT;
				if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    bmImage.setBackgroundDrawable(background);
                } else {
                    bmImage.setBackgroundDrawable(background);
                }
				mDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}