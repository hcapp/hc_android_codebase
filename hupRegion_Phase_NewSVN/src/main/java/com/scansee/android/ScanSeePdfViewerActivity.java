package com.scansee.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.artifex.mupdflib.FilePicker;
import com.artifex.mupdflib.MuPDFCore;
import com.artifex.mupdflib.MuPDFPageAdapter;
import com.artifex.mupdflib.MuPDFReaderView;
import com.artifex.mupdflib.PDFPreviewGridActivityData;
import com.artifex.mupdflib.SearchTaskResult;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.ShareInformation;
import com.scansee.hubregion.R;

public class ScanSeePdfViewerActivity extends CustomTitleBar implements
        FilePicker.FilePickerSupport {

    String urL;
    private File myFilesDir;
    private MuPDFCore core;
    private MuPDFReaderView mDocView;
    ShareInformation shareInfo;
    boolean isShareTaskCalled;
    DownloadFile downloadFile;
    static private String TAG = "ScanSeePdfViewerActivity";
    private boolean isIoInterrupted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scansee_pdf_viewer);
        try {
            title.setText("Details");

            /*** getting the pdf url to be downloaded ***/
            urL = getIntent().getExtras().getString(
                    RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH);

            /*** Creating file to store pdf after download ***/
            myFilesDir = new File(
                    android.os.Environment.getExternalStorageDirectory(), "HubCiti");
            // context.getFilesDir()
            /*** Creating the proper url and executing the download process ***/
            if (urL != null) {
                urL = urL.replaceAll(" ", "%20");
                downloadFile = new DownloadFile();
                downloadFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            if (UrlRequestParams.getUid().equals("-1")) {
                leftTitleImage.setVisibility(View.GONE);
                rightImage.setVisibility(View.GONE);
            }
            backImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            /*** Call for Share Information ***/
            if (getIntent().hasExtra("isShare")
                    && getIntent().getExtras().getBoolean("isShare")) {
                shareInfo = new ShareInformation(ScanSeePdfViewerActivity.this,
                        getIntent().getExtras().getString("retailerId"), "",
                        getIntent().getExtras().getString("pageId"), getIntent()
                        .getExtras().getString("module"));
                isShareTaskCalled = false;


                leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
                leftTitleImage.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!Constants.GuestLoginId.equals(UrlRequestParams
                                .getUid().trim())) {
                            shareInfo.shareTask(isShareTaskCalled);
                            isShareTaskCalled = true;
                        } else {
                            Constants.SignUpAlert(ScanSeePdfViewerActivity.this);
                            isShareTaskCalled = false;
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (downloadFile != null && !downloadFile.isCancelled()) {
            downloadFile.cancel(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isIoInterrupted) {
            if (urL != null) {
                urL = urL.replaceAll(" ", "%20");
                isIoInterrupted = false;
                Log.v("isIoInterrupted","onResume");
                downloadFile = new DownloadFile();
                downloadFile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*** Releasing memory ***/
        urL = null;
        myFilesDir = null;
        core = null;
        mDocView = null;
        shareInfo = null;
    }

    /***
     * Downloading the Pdf, saving it to storage and initiating the pdf viewer
     ***/
    private class DownloadFile extends AsyncTask<Void, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(ScanSeePdfViewerActivity.this, "",
                    getResources().getString(R.string.progress_bar_text));
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(Void... params) {

            HttpURLConnection c;
            try {
                URL url = new URL(urL);
                c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
            } catch (IOException e1) {
                e1.printStackTrace();
                return e1.getMessage();
            }

            File file = new File(myFilesDir, "Read.pdf");

            if (file.exists()) {
                file.delete();
            } else {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (myFilesDir.mkdirs() || myFilesDir.isDirectory()) {
                try {
                    InputStream is = c.getInputStream();
                    FileOutputStream fos = new FileOutputStream(myFilesDir
                            + "/" + "Read.pdf");

                    byte[] buffer = new byte[1024];
                    int len1 = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);
                    }
                    fos.close();
                    is.close();

                } catch (Exception e) {
                    if (dialog != null && dialog.isShowing()) {
                        Log.v("isIoInterrupted","dialog dismiss");
                        dialog.dismiss();
                        isIoInterrupted = true;
                    }
                    e.printStackTrace();
                    return e.getMessage();
                }

            } else {
                return "Unable to create folder";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                dialog.dismiss();
                File file = new File(myFilesDir, "Read.pdf");
                initiatePdfViewer(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void initiatePdfViewer(File file) {
        if (core == null) {
            byte buffer[] = null;
            Uri uri = Uri.parse(file.getAbsolutePath());
            Log.d(TAG, "URI to open is: " + uri);
            if (uri.toString().startsWith("content://")) {

                String reason = null;
                try {
                    InputStream is = getContentResolver().openInputStream(uri);
                    int len = is.available();
                    buffer = new byte[len];
                    is.read(buffer, 0, len);
                    is.close();
                } catch (java.lang.OutOfMemoryError e) {
                    reason = e.toString();
                    Log.e(TAG, reason
                            + " : Out of memory during buffer reading");
                } catch (Exception e) {
                    Log.e(TAG, "Exception reading from stream: " + e);

                    // Handle view requests from the Transformer Prime's
                    // file manager
                    // Hopefully other file managers will use this same
                    // scheme, if not
                    // using explicit paths.
                    // I'm hoping that this case below is no longer
                    // needed...but it's
                    // hard to test as the file manager seems to have
                    // changed in 4.x.
                    try {
                        Cursor cursor = getContentResolver().query(uri,
                                new String[]{"_data"}, null, null, null);
                        if (cursor.moveToFirst()) {
                            String str = cursor.getString(0);
                            if (str == null) {
                                reason = "Couldn't parse data in intent";
                            } else {
                                uri = Uri.parse(str);
                            }

                        }
                        cursor.close();
                        // } else {
                        // uri = Uri.parse(str);
                    } catch (Exception e2) {
                        Log.e(TAG, "Exception in Transformer Prime file manager code: "
                                + e2);
                        reason = e2.toString();
                    }
                }

            }
            if (buffer != null) {
                // core = openBuffer(buffer);
                core = openBuffer(buffer, "");

            } else {
                // core = openFile(Uri.decode(uri.getEncodedPath()));
                String path = Uri.decode(uri.getEncodedPath());
                if (path == null) {
                    path = uri.toString();
                }
                core = openFile(path);
            }
            SearchTaskResult.set(null);

            try {
                if (core.countPages() == 0) {
                    core = null;
                }

                if (core != null && core.countPages() == 0) {
                    core = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (core != null) {
            core.setDisplayPages(1);
            createUI();
        }
    }

    private void createUI() {
        mDocView = new MuPDFReaderView(this) {
            @Override
            protected void onMoveToChild(int i) {
                super.onMoveToChild(i);

            }

            @Override
            protected void onTapMainDocArea() {

            }

            @Override
            protected void onDocMotion() {

            }

        };
        mDocView.setAdapter(new MuPDFPageAdapter(this, this, core));
        mDocView.setKeepScreenOn(false);
        mDocView.setLinksHighlighted(false);
        mDocView.setScrollingDirectionHorizontal(false);

        ((RelativeLayout) findViewById(R.id.rl_pdf_holder)).addView(mDocView);
    }

    private MuPDFCore openFile(String path) {
        // int lastSlashPos = path.lastIndexOf('/');
        // mFileName = new String(lastSlashPos == -1 ? path
        // : path.substring(lastSlashPos + 1));
        Log.d(TAG, "Trying to open " + path);
        try {
            core = new MuPDFCore(this, path);
            // New file: drop the old outline data
            // OutlineActivityData.set(null);
            PDFPreviewGridActivityData.set(null);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
        return core;
    }

    private MuPDFCore openBuffer(byte buffer[], String magic) {
        Log.d(TAG, "Trying to open byte buffer");
        try {
            // core = new MuPDFCore(this, buffer);
            core = new MuPDFCore(this, buffer, magic);

            // New file: drop the old outline data
            // OutlineActivityData.set(null);
            PDFPreviewGridActivityData.set(null);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
        return core;
    }

    @Override
    public void performPickFor(FilePicker picker) {
        // TODO Auto-generated method stub

    }
}
