package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.screens.QualifyingProductsActivity;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CouponsProductListAdapter extends BaseAdapter {
	private QualifyingProductsActivity activity;
	private ArrayList<HashMap<String, String>> couponsproductsList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;
	HashMap<String, String> couponsproductData = null;

	public CouponsProductListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> couponsproductsList) {
		this.activity = (QualifyingProductsActivity) activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.couponsproductsList = couponsproductsList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return couponsproductsList.size();
	}

	@Override
	public Object getItem(int id) {
		return couponsproductsList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder = null;
		if (convertView == null) {

			view = inflater.inflate(R.layout.listitem_product_coupons, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.couponimage = (ImageView) view
					.findViewById(R.id.coupon_image);
			viewHolder.couponname = (TextView) view
					.findViewById(R.id.coupon_product_name);
			viewHolder.couponDisamt = (TextView) view
					.findViewById(R.id.product_longdescription);
			viewHolder.couponimagePath = (ImageView) view
					.findViewById(R.id.product_imagepath);

		}
		if (viewHolder != null) {
			view.setTag(viewHolder);
			viewHolder.couponname.setSingleLine(false);
			viewHolder.couponname.setMaxLines(2);
			viewHolder.couponname.setText(couponsproductsList.get(position)
					.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
			viewHolder.couponDisamt.setText(couponsproductsList.get(position)
					.get(CommonConstants.COUPONPRODUCTDESCRIPTION));
			viewHolder.couponimagePath.setTag(couponsproductsList.get(position)
					.get(CommonConstants.COUPONPRODUCTIMAGEPATH));
			customImageLoader.displayImage(
					couponsproductsList.get(position).get(
							CommonConstants.COUPONPRODUCTIMAGEPATH), activity,
					viewHolder.couponimagePath);
		}
		return view;
	}

	public static class ViewHolder {
		protected ImageView couponimage;
		protected TextView couponexpirationdate;
		protected TextView couponexpiretext;
		protected TextView couponDisamt;
		protected TextView description;
		protected ImageView couponimagePath;
		protected TextView couponname;

	}

}
