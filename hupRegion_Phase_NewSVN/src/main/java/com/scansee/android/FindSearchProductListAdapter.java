package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.ImageLoaderAsync;

public class FindSearchProductListAdapter extends BaseAdapter {
	Activity activity;
	private ArrayList<HashMap<String, String>> findsearchList;
	private static LayoutInflater inflater = null;

	public FindSearchProductListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> findsearchList) {
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.findsearchList = findsearchList;
	}

	@Override
	public int getCount() {

		return findsearchList.size();
	}

	@Override
	public Object getItem(int id) {
		return findsearchList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		String clickedItem = findsearchList.get(position).get("itemName");

		if ("viewMore".equalsIgnoreCase(clickedItem)) {
			view = inflater.inflate(R.layout.listitem_get_retailers_viewmore,
					parent,false);

		} else if ("searchproduct".equalsIgnoreCase(clickedItem)
				|| "nameSearch".equalsIgnoreCase(clickedItem)) {
			viewHolder = new ViewHolder();
			view = inflater.inflate(R.layout.listitem_find_product, parent,false);

			viewHolder.productsearchImage = (ImageView) view
					.findViewById(R.id.findsearch_info_image);
			viewHolder.productsearchName = (TextView) view
					.findViewById(R.id.find_productsearch_name);
			viewHolder.productsearchDescription = (TextView) view
					.findViewById(R.id.find_productsearch_shortdesc);

			view.setTag(viewHolder);

			viewHolder.productsearchName.setText(findsearchList.get(position)
					.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));

			int count = 1;

			if (findsearchList.get(position).get(
					CommonConstants.FINDPRODUCTSEARCHPRODUCTCOUNT) != null) {
				count = Integer.valueOf(findsearchList.get(position).get(
						CommonConstants.FINDPRODUCTSEARCHPRODUCTCOUNT));
			}

			if (count > 1) {
				viewHolder.productsearchDescription.setText("("
						+ findsearchList.get(position).get(
								CommonConstants.FINDPRODUCTSEARCHPRODUCTCOUNT)
						+ "items" + ")");
			}

			else {
				viewHolder.productsearchDescription.setText(findsearchList.get(
						position).get(
						CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION));
			}

			if (!"N/A".equalsIgnoreCase(findsearchList.get(position).get(
					CommonConstants.FINDPRODUCTSEARCHIMAGEPATH))) {
				viewHolder.productsearchImage.setTag(findsearchList.get(
						position).get(
						CommonConstants.FINDPRODUCTSEARCHIMAGEPATH));
				new ImageLoaderAsync(viewHolder.productsearchImage)
						.execute(findsearchList.get(position).get(
								CommonConstants.FINDPRODUCTSEARCHIMAGEPATH));
			} else {
				viewHolder.productsearchImage.setImageBitmap(null);
			}
		}

		if (position == findsearchList.size() - 1) {

			try {
				if (!((FindSearchProductActivity) activity).isAlreadyLoading) {
					if (((FindSearchProductActivity) activity).nextPage) {

						((FindSearchProductActivity) activity).viewMore();
					}
				}
			} catch (Exception e) {
				if (!((ScanNowResultActivity) activity).isAlreadyLoading) {
					if (((ScanNowResultActivity) activity).nextPage) {

						((ScanNowResultActivity) activity).viewMore();
					}
				}
			}
		}
		return view;
	}

	public static class ViewHolder {
		protected TextView productsearchDescription;
		protected TextView productsearchName;
		protected ImageView productsearchImage;

	}
}
