package com.scansee.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.com.hubcity.model.CouponModel;
import com.com.hubcity.model.CouponObj;
import com.com.hubcity.model.RetailerDetails;
import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CommonMethods;

import java.util.ArrayList;
import java.util.LinkedList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by subramanya.v on 12/5/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class CouponsFragment extends Fragment implements View.OnClickListener {
    private Activity mActivity;
    private View view;
    public final LinkedList<ArrayList<RetailerDetails>> listItem = new LinkedList<>();
    private final LinkedList<CouponObj> listMainItem = new LinkedList<>();
    private int categoryHeight;
    private int screenHeight;
    private LinearLayout llParent;
    private int screenWidth;
    private RestClient mRestClient;
    private String sortColumn = "distance";
    private String isFeatOrNonFeat;

    private String searchKey;
    private String latitude;
    private String longitude;
    private String catIds;
    private String cityIds;
    private boolean isFirst = true;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (CouponsActivty) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            view = inflater.inflate(R.layout.coupons_activity, null);
            getValue();
            bindView();
            mRestClient = RestClient.getInstance();
            callService();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void callService() {
        ProgressDialog progDialog = null;
        if (isFirst) {
            progDialog = new ProgressDialog(mActivity);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(mActivity.getString(R.string.progress_message));
            progDialog.setCancelable(false);
            progDialog.show();
        }
        if (latitude != null && latitude.isEmpty()) {
            latitude = null;
        }
        if (longitude != null && longitude.isEmpty()) {
            longitude = null;
        }


        String lastVisitedNo = "0";
        String sortOrder = "asc";
        CouponModel request = UrlRequestParams.sendCoupon(sortColumn, isFeatOrNonFeat, lastVisitedNo, sortOrder, searchKey, latitude, longitude, catIds, cityIds);
        final ProgressDialog finalProgDialog = progDialog;
        mRestClient.getCoupon(request, new Callback<CouponObj>() {
            @Override
            public void success(final CouponObj couponObj, Response response) {
                try {
                    String responseText = couponObj.getResponseText();
                    if (!responseText.equalsIgnoreCase(mActivity.getString(R.string.success)) && responseText != null) {
                        CommonConstants.displayToast(mActivity, responseText);
                    }

                    listMainItem.add(couponObj);
                    if (couponObj.getFeaturedCouponsList() != null) {
                        listItem.add(couponObj.getFeaturedCouponsList());
                    }
                    if (couponObj.getNonFeaturedCouponsList() != null) {
                        listItem.add(couponObj.getNonFeaturedCouponsList());
                    }
                    ArrayList<BottomButtonBO> bottomButtonList = couponObj.getBottomBtnList();
                    if (bottomButtonList != null && bottomButtonList
                            .size() != 0) {
                        BottomButtonListSingleton
                                .clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                        ((CouponsActivty) mActivity).bb.createbottomButtontTab(((CouponsActivty) mActivity).bottomLayout, false);
                        ((CouponsActivty) mActivity).bottomLayout.setVisibility(View.VISIBLE);
                    }
                    if (((CouponsActivty) mActivity).bottomButtonHeight == 0 && bottomButtonList != null && bottomButtonList
                            .size() != 0) {
                        ViewTreeObserver observer = ((CouponsActivty) mActivity).bottomLayout.getViewTreeObserver();
                        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                ViewTreeObserver obs = ((CouponsActivty) mActivity).bottomLayout.getViewTreeObserver();
                                ((CouponsActivty) mActivity).bottomButtonHeight = ((CouponsActivty) mActivity).bottomLayout.getMeasuredHeight();

                                addViewPagerItem(couponObj.getLabel());
                                hideProgressBar(finalProgDialog);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                                {
                                    obs.removeOnGlobalLayoutListener(this);
                                } else
                                {
                                    //noinspection deprecation
                                    obs.removeGlobalOnLayoutListener(this);
                                }

                            }
                        });
                    } else {
                        addViewPagerItem(couponObj.getLabel());
                        hideProgressBar(finalProgDialog);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressBar(finalProgDialog);
                if (error.getResponse() != null) {
                    CommonConstants.displayToast(mActivity, error
                            .getResponse().getReason());
                }

            }
        });


    }

    private void addViewPagerItem(String label) {
        categoryHeight = 80;
        int parentHeight;
        int size = listItem.size() + 1;
        if (size > 3) {
            screenHeight = screenHeight - categoryHeight;
            parentHeight = (screenHeight - ((CouponsActivty) mActivity).bottomButtonHeight) / 2 - categoryHeight;
        } else {
            parentHeight = (screenHeight - ((CouponsActivty) mActivity).bottomButtonHeight) / 2 - categoryHeight;
        }

        LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, parentHeight);
        for (int id = 1; id < size; id++) {
            label = checkLabel(label);
            setCategoryView(id, label);
            final ViewPager viewPager = new ViewPager(mActivity);
            viewPager.setId(Integer.parseInt(String.valueOf(id)));
            viewPager.setLayoutParams(parentParam);
            viewPager.setTag(id);

            viewPager.setPadding(40, 0, 40, 0);
            viewPager.setClipToPadding(false);
            viewPager.setPageMargin(20);
            llParent.addView(viewPager);
            ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(), viewPager);
            viewPager.setAdapter(viewPagerAdapter);
        }
    }

    private void hideProgressBar(ProgressDialog progDialog) {
        if (progDialog != null && progDialog.isShowing()) {
            progDialog.dismiss();
        }
    }


    private void setCategoryView(int position, String label)
    {
        LinearLayout.LayoutParams categoryParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, categoryHeight);
        TextView category = new TextView(mActivity);
        category.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.light_gray));
        category.setGravity(Gravity.CENTER_VERTICAL);
        category.setPadding(20, 0, 10, 0);
        CouponObj listItemValue = listMainItem.get(0);
        if (listItem.size() < 2) {
            if (listItemValue.getNonFeaturedCouponsList() != null) {
                category.setText(label);
            }
            if (listItemValue.getFeaturedCouponsList() != null) {
                category.setText(mActivity.getString(R.string.featured));
            }
        } else {
            if (position > 1) {
                category.setText(label);
            } else {
                category.setText(mActivity.getString(R.string.featured));
            }
        }
        category.setTextColor(ContextCompat.getColor(mActivity, R.color.white));
        category.setTextSize((float) 20);
        category.setTag(position - 1);
        category.setOnClickListener(this);
        category.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow, 0);
        category.setLayoutParams(categoryParam);
        llParent.addView(category);

    }

    private void getValue()
    {
        Bundle bundle = getArguments();
        screenHeight = bundle.getInt("screenHeight");
        screenWidth = bundle.getInt("screenWidth");
        searchKey = bundle.getString("searchKey");
        latitude = bundle.getString("latitude");
        longitude = bundle.getString("longitude");

        //sort and filter param
        catIds = bundle.getString("catIds");
        sortColumn = bundle.getString("sortColumn");
        cityIds = bundle.getString("cityIds");
        isFirst = bundle.getBoolean("isFirst");

    }

    private void bindView() {
        llParent = (LinearLayout) view.findViewById(R.id.coupon_parent);
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        Intent listingScreen = new Intent(mActivity, CouponsListingActivity.class);
        CouponObj listItemValue = listMainItem.get(0);
        if (listItem.size() < 2) {
            if (listItemValue.getNonFeaturedCouponsList() != null) {
                String label = checkLabel(listMainItem.get(0).getLabel());
                listingScreen.putExtra("title", label);
                listingScreen.putExtra("isFeatOrNonFeat", "1");
            }
            if (listItemValue.getFeaturedCouponsList() != null) {
                listingScreen.putExtra("title", mActivity.getString(R.string.featured));
                listingScreen.putExtra("isFeatOrNonFeat", "0");
            }
        } else {
            if (position == 0) {
                listingScreen.putExtra("title", mActivity.getString(R.string.featured));
                listingScreen.putExtra("isFeatOrNonFeat", "0");

            } else {
                String label = checkLabel(listMainItem.get(0).getLabel());
                listingScreen.putExtra("title", label);
                listingScreen.putExtra("isFeatOrNonFeat", "1");
            }
        }
        putSortFilterValues(listingScreen);
        listingScreen.putExtra("searchKey", searchKey);
        ((CouponsActivty) mActivity).isClaimed = false;
        startActivity(listingScreen);
    }

    private void putSortFilterValues(Intent listingScreen) {
        listingScreen.putExtra("catIds", catIds);
        listingScreen.putExtra("sortColumn", sortColumn);
        listingScreen.putExtra("cityIds", cityIds);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final ViewPager viewPager;
        private SwipePager swipePager;
        private int id;
        private int listPosition;
        private int size;
        private String label;

        public ViewPagerAdapter(FragmentManager childFragmentManager, ViewPager viewPager) {
            super(childFragmentManager);
            this.viewPager = viewPager;
        }

        @Override
        public Fragment getItem(int position) {
            swipePager = new SwipePager();
            CouponObj listItemValue = listMainItem.get(0);
            if (listItem.size() < 2) {
                if (listItemValue.getNonFeaturedCouponsList() != null) {
                    isFeatOrNonFeat = "1";
                    label = checkLabel(listItemValue.getLabel());

                }
                if (listItemValue.getFeaturedCouponsList() != null) {
                    isFeatOrNonFeat = "0";
                    label = mActivity.getString(R.string.featured);
                }
            } else {
                if (listPosition == 0) {
                    isFeatOrNonFeat = "0";
                    label = mActivity.getString(R.string.featured);

                } else {
                    isFeatOrNonFeat = "1";
                    label = checkLabel(listItemValue.getLabel());
                }
            }
            if (size > 10) {
                if (position != size - 1) {
                    setValue(position, listItem.get(listPosition).get(position), label, isFeatOrNonFeat);
                } else {
                    setValue(position, null, label, isFeatOrNonFeat);
                }
            } else {
                setValue(position, listItem.get(listPosition).get(position), label, isFeatOrNonFeat);
            }

            return swipePager;
        }

        private void setValue(int position, RetailerDetails listItem, String label, String isFeatOrNonFeat) {
            Bundle bundle = new Bundle();
            if (listItem != null) {
                bundle.putInt("couponId", listItem.getCouponId());
                bundle.putString("couponName", listItem.getCouponName());
                bundle.putString("couponImagePath", listItem.getCouponImagePath());
                bundle.putString("bannerTitle", listItem.getBannerTitle());
                bundle.putInt("counts", listItem.getCounts());
                bundle.putInt("retId", listItem.getRetId());
                bundle.putString("retName", listItem.getRetName());
                bundle.putString("distance", listItem.getDistance());
                bundle.putInt("rowNum", listItem.getRowNum());
                bundle.putString("couponListId", listItem.getCouponListId());
                bundle.putInt("position", position);
                bundle.putInt("screenHeight", screenHeight);
                bundle.putInt("categoryHeight", categoryHeight);
                bundle.putInt("screenWidth", screenWidth);
                bundle.putString("title", label);
                bundle.putString("isFeatOrNonFeat", isFeatOrNonFeat);
                bundle.putString("searchKey", searchKey);

                bundle.putString("catIds", catIds);
                bundle.putString("cityIds", cityIds);
                bundle.putString("sortColumn", sortColumn);

            } else {
                bundle.putBoolean("isLastView", true);
                bundle.putString("title", label);
                bundle.putString("isFeatOrNonFeat", isFeatOrNonFeat);

            }
            swipePager.setArguments(bundle);
        }


        @Override
        public int getCount() {
            ViewPagerAdapter adapter = ((ViewPagerAdapter) viewPager.getAdapter());
            id = (int) adapter.viewPager.getTag();
            listPosition = id - 1;
            size = listItem.get(listPosition).size();
            if (size > 10) {
                size = size + 1;
            }
            return size;
        }
    }

    private String checkLabel(String label) {
        if (label != null) {
            if (label.equalsIgnoreCase("distance")) {
                label = mActivity.getString(R.string.distance);
            } else {
                label = mActivity.getString(R.string.atoz);

            }
        }
        return label;
    }

    public static class SwipePager extends Fragment implements View.OnClickListener {
        private CouponsActivty mActivity;
        private int position;
        private ImageView imCouponImage;
        private TextView tvCouponName;
        private ProgressBar progressBar;
        private View view;

        private boolean isLastView;
        private int couponId;
        private String couponName;
        private String couponImagePath;
        private String bannerTitle;
        private int counts;
        private String retName;
        private String distance;
        private String couponListId;
        private TextView tvDistance;
        private TextView tvCouponCount;
        private TextView tvPrice;
        private TextView tvRetailerName;
        private String title;
        private String isFeatOrNonFeat;
        private String searchKey;
        private String catIds;
        private String sortColumn;
        private String cityIds;

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            mActivity = (CouponsActivty) context;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            getBudleData();
            if (isLastView) {
                view = inflater.inflate(R.layout.see_more_item, container, false);
                view.setOnClickListener(this);
                view.setTag(position);
            } else {
                view = inflater.inflate(R.layout.coupon_item, container, false);
                bindViewPager();
                view.setOnClickListener(this);
                view.setTag(position);
                if (couponImagePath != null && !couponImagePath.isEmpty()) {
                    new CommonMethods().loadImage(mActivity, progressBar, couponImagePath, imCouponImage);
                }
                if (retName != null && !retName.isEmpty()) {
                    tvRetailerName.setText(retName);
                    tvRetailerName.setVisibility(View.VISIBLE);
                }

                if (couponName != null && !couponName.isEmpty()) {
                    tvCouponName.setText(couponName);
                    tvCouponName.setVisibility(View.VISIBLE);
                }
                if (distance != null && !distance.isEmpty()) {
                    if (counts > 1) {
                        tvDistance.setText(mActivity.getString(R.string.close_loc) + " " + distance);
                    } else {
                        tvDistance.setText(distance);
                    }
                    tvDistance.setVisibility(View.VISIBLE);
                }
                if (counts >= 1) {
                    StringBuilder Couponcount = new StringBuilder();
                    Couponcount.append(String.valueOf(counts));
                    Couponcount.append(" ");
                    Couponcount.append(mActivity.getString(R.string.part_loc));
                    tvCouponCount.setText(Couponcount.toString());
                    tvCouponCount.setVisibility(View.VISIBLE);
                } else {
                    tvCouponCount.setVisibility(View.INVISIBLE);
                }

                if (bannerTitle != null && !bannerTitle.isEmpty()) {
                    tvPrice.setText(bannerTitle);
                    tvPrice.setVisibility(View.VISIBLE);
                }

            }
            return view;
        }

        private void bindViewPager() {
            imCouponImage = (ImageView) view.findViewById(R.id.coupon_image);
            tvCouponName = (TextView) view.findViewById(R.id.coupon_name);
            tvRetailerName = (TextView) view.findViewById(R.id.retailer_name);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            tvDistance = (TextView) view.findViewById(R.id.distance);
            tvCouponCount = (TextView) view.findViewById(R.id.coupon_count);
            tvPrice = (TextView) view.findViewById(R.id.price);
        }

        private void getBudleData() {
            Bundle bundle = getArguments();
            position = bundle.getInt("position");
            isLastView = bundle.getBoolean("isLastView");

            couponId = bundle.getInt("couponId");
            couponName = bundle.getString("couponName");
            couponImagePath = bundle.getString("couponImagePath");
            bannerTitle = bundle.getString("bannerTitle");
            counts = bundle.getInt("counts");
            retName = bundle.getString("retName");
            distance = bundle.getString("distance");
            couponListId = bundle.getString("couponListId");
            title = bundle.getString("title");
            isFeatOrNonFeat = bundle.getString("isFeatOrNonFeat");
            searchKey = bundle.getString("searchKey");

            catIds = bundle.getString("catIds");
            sortColumn = bundle.getString("sortColumn");
            cityIds = bundle.getString("cityIds");


        }

        @Override
        public void onClick(View v) {
            Intent screen;
            if (isLastView) {
                screen = new Intent(mActivity, CouponsListingActivity.class);
                screen.putExtra("isFeatOrNonFeat", isFeatOrNonFeat);
                screen.putExtra("title", title);
                putSwipeSortFilterValues(screen);
                screen.putExtra("searchKey", searchKey);
                 mActivity.isClaimed = false;
            } else {
                screen = new Intent(mActivity, CouponsDetailActivity.class);
                screen.putExtra("couponId", String.valueOf(couponId));
                screen.putExtra("couponListId", couponListId);
                screen.putExtra("isCoupon", true);
                 mActivity.isClaimed = true;

            }
            startActivity(screen);

        }

        private void putSwipeSortFilterValues(Intent screen) {
            screen.putExtra("catIds", catIds);
            screen.putExtra("sortColumn", sortColumn);
            screen.putExtra("cityIds", cityIds);
        }
    }

}
