package com.scansee.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by supriya.m on 4/22/2016.
 */
public class GetBandListAsyncTask extends AsyncTask<Void, Void, Void> {


    private final String groupByView;
    private HashMap<String, String> resultSet;
    Context mContext;
    String radius, postalCode, mItemId, responseText, searchKey, lastVisitedNo, cityIds;
    ProgressDialog progressDialog;
    ArrayList<BandListingSetGet> arrBands;
    ArrayList<BandSearchListSetGet> arrSearch;
    BottomButtons bb;
    LinearLayout parentLayout;
    boolean hasBottomButtons, enableBottomButtons, showProgress;
    boolean nextPage;
    Double latitude;
    Double longitude;


    public GetBandListAsyncTask(Context context, String searchKey, String radius,
                                String postalCode, String mItemId, BottomButtons bb, LinearLayout layout,
                                String lastVisitedNo, boolean enableBottomButtons, boolean showProgress, Double
                                        latitude, Double longitude, HashMap<String, String> resultSet, String groupBy, String cityIds) {
        this.mContext = context;
        this.mItemId = mItemId;
        this.postalCode = postalCode;
        this.radius = radius;
        this.searchKey = searchKey;
        this.bb = bb;
        this.parentLayout = layout;
        this.lastVisitedNo = lastVisitedNo;
        this.enableBottomButtons = enableBottomButtons;
        this.showProgress = showProgress;
        this.latitude = latitude;
        this.longitude = longitude;
        this.resultSet = resultSet;
        this.groupByView = groupBy;
//        this.cityIds = cityIds;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgress) {
            progressDialog = new ProgressDialog(mContext, ProgressDialog.THEME_HOLO_DARK);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(Constants.DIALOG_MESSAGE);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        String url = Properties.url_local_server
                + Properties.hubciti_version + "band/getbandlist";

        String sortColumn;
        String sortOrder;
        String groupBy;
        sortColumn = "BandName";
        sortOrder = "ASC";
        groupBy = groupByView;
        String catIds = null;
        if (resultSet != null && !resultSet.isEmpty()) {
            if (resultSet.containsKey("savedSubCatIds")) {
                catIds = resultSet.get("savedSubCatIds");
            }
//            if (resultSet.containsKey("savedCityIds")) {
//                if (resultSet.get("savedCityIds") != null && !resultSet.get("savedCityIds").equals("")) {
//                    cityIds = resultSet.get("savedCityIds");
//                    cityIds = cityIds.replace("All,", "");
//                }
//            }
        }

        try {

            UrlRequestParams mUrlRequestParams = new UrlRequestParams();
            ServerConnections mServerConnections = new ServerConnections();
            JSONObject jsonUrlParameters = mUrlRequestParams
                    .getBandRequestParam(radius, postalCode, mItemId, sortColumn, sortOrder,
                            groupBy, searchKey,
                            lastVisitedNo, catIds, cityIds);
            JSONObject jsonObject = mServerConnections.getUrlJsonPostResponse(
                    url, jsonUrlParameters);
            if (jsonObject != null) {
                responseText = jsonObject.optString("responseText");
                if (jsonObject.has("catList")) {
                    arrBands = new ArrayList<>();
                    arrSearch = new ArrayList<>();
                    JSONArray jsonArray = jsonObject.optJSONArray("catList");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (searchKey.equals("")) {
                            BandListingSetGet bandListing = new BandListingSetGet();
                            bandListing.setIsGroup(true);
                            String groupContent = jsonArray.getJSONObject(i).optString
                                    ("groupContent");
                            bandListing.setGroupName(jsonArray.getJSONObject(i).optString
                                    ("groupContent"));
                            arrBands.add(bandListing);
                            if (jsonArray.getJSONObject(i).has("retDetList")) {
                                JSONArray jsonRetArray = jsonArray.getJSONObject(i).getJSONArray
                                        ("retDetList");
                                for (int j = 0; j < jsonRetArray.length(); j++) {
                                    BandListingSetGet bandListingSetGet = new BandListingSetGet();
                                    bandListingSetGet.setGroupName(groupContent);
                                    bandListingSetGet.setBandId(jsonRetArray.getJSONObject(j)
                                            .optString
                                                    ("bandID"));
                                    bandListingSetGet.setBandName(jsonRetArray.getJSONObject(j)
                                            .optString
                                                    ("bandName"));
                                    bandListingSetGet.setBandImageUrl(jsonRetArray.getJSONObject
                                            (j).optString
                                            ("bandImgPath"));
                                    bandListingSetGet.setBandCategoryName(jsonRetArray
                                            .getJSONObject(j)
                                            .optString("catName"));
                                    bandListingSetGet.setBandCatId(jsonRetArray.getJSONObject(j)
                                            .optString
                                                    ("catId"));
                                    arrBands.add(bandListingSetGet);
                                }
                            }
                        } else {
                            String groupContent = null;
                            groupContent = jsonArray.getJSONObject(i).optString
                                    ("groupContent");
                            if (((BandsLandingPageActivity) mContext).type.equalsIgnoreCase("band")) {
                                BandSearchListSetGet bandSearchList = new BandSearchListSetGet();
                                bandSearchList.setIsGroup(true);
                                bandSearchList.setGroupName(jsonArray.getJSONObject(i).optString
                                        ("groupContent"));
                                arrSearch.add(bandSearchList);
                            }
                            if (jsonArray.getJSONObject(i).has("retDetList")) {
                                JSONArray jsonRetArray = jsonArray.getJSONObject(i).getJSONArray
                                        ("retDetList");
                                for (int j = 0; j < jsonRetArray.length(); j++) {
                                    BandSearchListSetGet bandSearchListSetGet = new
                                            BandSearchListSetGet();
                                    bandSearchListSetGet.setGroupName(groupContent);
                                    bandSearchListSetGet.setSearchedItemId(jsonRetArray
                                            .getJSONObject(j)
                                            .optString("bandID"));
                                    bandSearchListSetGet.setSearchedItemName(jsonRetArray
                                            .getJSONObject(j)
                                            .optString("bandName"));
                                    bandSearchListSetGet.setSearchedCatName(jsonRetArray
                                            .getJSONObject(j)
                                            .optString("catName"));
                                    HubCityContext.searchedArrSubCatIds.add(jsonRetArray
                                            .getJSONObject(
                                                    j).optString("catId"));
                                    bandSearchListSetGet.setSearchedImgPath(jsonRetArray
                                            .getJSONObject(j)
                                            .optString
                                                    ("bandImgPath"));
                                    arrSearch.add(bandSearchListSetGet);
                                }
                            }
                        }
                    }
                }
                if (jsonObject.has("bottomBtnList")) {
                    try {
                        ArrayList<BottomButtonBO> bottomButtonList = bb
                                .parseForBottomButton(jsonObject);
                        BottomButtonListSingleton
                                .clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                        hasBottomButtons = true;
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }
                if (jsonObject.optString("nextPage").equals("1")) {
                    if (jsonObject.has("maxRowNum")) {
                        lastVisitedNo = jsonObject.getString("maxRowNum");
                    }
                    nextPage = true;
                } else {
                    nextPage = false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try {
            if (responseText != null && responseText.equals("Success")) {
                //first time
                if (searchKey.equals("")) {
                    ((BandsLandingPageActivity) mContext).loadBandList(arrBands, nextPage,
                            lastVisitedNo);
                } else {
                    ((BandsLandingPageActivity) mContext).loadSearchList(arrSearch, lastVisitedNo,
                            nextPage);
                }
            } else {
                if (searchKey.equals("")) {
                    arrBands = new ArrayList<>();
                    ((BandsLandingPageActivity) mContext).loadBandList(arrBands, false, lastVisitedNo);
                } else {
                    arrSearch = new ArrayList<>();
                    ((BandsLandingPageActivity) mContext).loadSearchList(arrSearch, lastVisitedNo,
                            nextPage);
                }
                if (responseText != null) {
                    Toast.makeText(mContext, responseText, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Technical problem.", Toast.LENGTH_SHORT).show();
                }
            }
            if (hasBottomButtons && enableBottomButtons) {
                bb.createbottomButtontTab(parentLayout, false);
                parentLayout.setVisibility(View.VISIBLE);
            }
            if (!hasBottomButtons) {
                parentLayout.setVisibility(View.GONE);
            }
            if (bb != null) {
                bb.setOptionsValues(getListValues(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();

        values.put("Class", "BandLanding");

        values.put("mItemId", mItemId);

        values.put("latitude", String.valueOf(latitude));

        values.put("longitude", String.valueOf(longitude));

        values.put("moduleName", "Band");

        values.put("postalCode", postalCode);

        values.put("srchKey", searchKey);

        return values;
    }
}
