package com.scansee.android;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.hubcity.android.CustomLayouts.ToggleButtonView;
import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.RetailerActivity;
import com.hubcity.android.screens.RetailerImpressionUpdateAsync;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.hubcity.android.screens.SubMenuDetails;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
public class FindLocationActivity extends CustomTitleBar implements
		OnClickListener, OnInfoWindowClickListener, ToggleButtonView.ToggleViewInterface
{
	private ArrayList<HashMap<String, String>> dupeList = null;
	ArrayList<HashMap<String, String>> findlocationList = new ArrayList<>();
	protected ListView findlocationListView;
	String accZipcode = null;

	Button mCancel;
	FindLocationGoogleListAdapter findlocationListAdapter;
	// Beena
	TextView moreInfo;
	View moreResultsView;
	String mLinkId;
	int previousPosition = 0;
	String userID, latitude = null, longitude = null, catName, catID = "";
	int lastVisitedNo = 0;
	ViewFlipper findLocationFlipper;
	LocationManager locationManager;
	MenuItem item;
	String mItemId = null, bottomBtnId = null;
	private GoogleMap mMap;
	LatLngBounds.Builder latLngBounds;
	private ArrayList<Marker> markers = new ArrayList<>();
	String searchkey;
	String parCatId;
	EditText retailerEditBox;
	String locationText;
	boolean isRetailerCatSearch = false;
	String catType;
	ArrayList<String> arrRetailerId;
	ArrayList<String> arrRetailerLocId;
	LinearLayout linearLayout = null;
	BottomButtons bb = null;
	Activity activity;
	boolean hasBottomBtns = false;
	boolean enableBottomButton = true;

	String subCatId = "";
	String typeName = "";
	String sortColumn = "Distance";
	boolean isViewMore;
	boolean isRefresh, isAlreadyLoading;

	// @Rekha
	String tempLastVisit = "0", lastVisited = "0";

	ArrayList<HashMap<String, String>> retailersList = new ArrayList<>();
	ArrayList<HashMap<String, String>> mFindList = new ArrayList<>();
	ArrayList<Item> items = new ArrayList<>();

	FindLocationCategory findLocAsyncTask;
	GetZipcode zipcodeAsyncTask;
	boolean isFirst, isItemClicked;
	String strMainMenuId = "";

	String locSpecials = "0", interests = "";

	HubCityContext mHubCiti;
	TextView mLabelDistance, mLabelName;
	private TextView sortTextView;
	private CustomNavigation customNaviagation;

	private int visibleItemCount = 0;
	@SuppressWarnings("deprecation")
	@SuppressLint("DefaultLocale")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.find_location_listview);
			CommonConstants.hamburgerIsFirst = true;

			isFirst = true;
			isViewMore = false;
			isRefresh = true;

			mHubCiti = (HubCityContext) getApplicationContext();
			mHubCiti.setCancelled(false);
			activity = FindLocationActivity.this;
			try {
				// Initiating Bottom button class
				bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
				// Add screen name when needed
				bb.setActivityInfo(activity, "");
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			HubCityContext.savedValues = new HashMap<>();
			Log.v("", "RETAILER LOCATION* ");
			linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
			linearLayout.setVisibility(View.INVISIBLE);

			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
				mItemId = getIntent().getExtras().getString(
						Constants.MENU_ITEM_ID_INTENT_EXTRA);
			}

			if (getIntent().hasExtra("catName")) {
				catName = getIntent().getExtras().getString("catName");
			}

			if (getIntent().hasExtra("selectedCat")) {
				catName = getIntent().getExtras().getString("selectedCat");
			}

			if (getIntent().hasExtra("subCatId")) {
				subCatId = getIntent().getExtras().getString("subCatId");
			}

			if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
				bottomBtnId = getIntent().getExtras().getString(
						Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
			}

			if (getIntent().hasExtra("typeName")) {
				typeName = getIntent().getExtras().getString("typeName");
			}

			if (getIntent().hasExtra("mLinkId")) {
				mLinkId = getIntent().getExtras().getString("mLinkId");

			} else if (getIntent().hasExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA)) {
				mLinkId = getIntent().getExtras().getString(
						Constants.BOTTOM_LINK_ID_INTENT_EXTRA);
			}

			String catId = "", filterId = "";
			if (getIntent().hasExtra("filterIds")) {
				filterId = getIntent().getExtras().getString("filterIds");
			}
			if (getIntent().hasExtra("selectedCat")) {
				catName = getIntent().getExtras().getString("selectedCat");
			}

			if (getIntent().hasExtra("selectedCatId")) {
				catId = getIntent().getExtras().getString("selectedCatId");
			}
			sortTextView = (TextView) findViewById(R.id.sort_text);
			Constants.FIND_CAT_LINK_ID = mLinkId;
			mLabelName = (TextView) this.findViewById(R.id.toggle_name);
			mLabelDistance = (TextView) this.findViewById(R.id.toggle_distance);
			title.setText(catName);
			leftTitleImage.setVisibility(View.INVISIBLE);
//			rightImage.setVisibility(View.GONE);
			divider.setVisibility(View.GONE);

			if (getIntent().hasExtra("selectedCat")) {

				parCatId = getIntent().getExtras().getString("selectedCat");

			}


			retailerEditBox = (EditText) findViewById(R.id.find_retailer_edit_text);
			mCancel = (Button) findViewById(R.id.find_retailer_cancel);

			mCancel.setOnClickListener(this);

			retailerEditBox.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
			retailerEditBox.setOnEditorActionListener(new OnEditorActionListener()
			{

				@Override
				public boolean onEditorAction(TextView textView, int actionId,
						KeyEvent event)
				{
					onClick(textView);
					hideKeyboardItem();
					return true;
				}
			});

			retailerEditBox.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void onTextChanged(CharSequence sequence, int start,
						int before, int count)
				{
					String value = sequence.toString();
					locationText = value.trim();
				}

				@Override
				public void afterTextChanged(Editable arg0)
				{
				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3)
				{

				}

			});

			backImage.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					hideKeyboardItem();
					finish();
				}
			});

			SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
			userID = settings.getString(CommonConstants.USER_ID, "0");

			if (getIntent().getExtras().getString("selectedCat") != null) {

				catName = getIntent().getExtras().getString("selectedCat").trim();
			}

			if (getIntent().getExtras().getString("selectedCatId") != null) {

				catID = getIntent().getExtras().getString("selectedCatId").trim();
			}
			findLocationFlipper = (ViewFlipper) findViewById(R.id.find_location_flipper);
			findLocationFlipper.setDisplayedChild(0);

			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			String provider = Settings.Secure.getString(getContentResolver(),
					Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

			findlocationListView = (ListView) findViewById(R.id.find_location_listview_list);

			moreResultsView = getLayoutInflater().inflate(
					R.layout.listitem_get_retailers_viewmore, findlocationListView,
					false);
			moreInfo = (TextView) moreResultsView.findViewById(R.id.viewMore);
			moreInfo.setOnClickListener(this);

			findlocationListView.setOnItemClickListener(new OnItemClickListener()
			{

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id)
				{
					try {
						isItemClicked = true;
						findNavigation(position);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});
			hideKeyBoard();

			if (provider.contains("gps")) {

					locationManager.requestLocationUpdates(
							LocationManager.GPS_PROVIDER,
							CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
							CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
							new ScanSeeLocListener());
					Location locNew = locationManager
							.getLastKnownLocation(LocationManager.GPS_PROVIDER);

					if (locNew != null) {
						latitude = String.valueOf(locNew.getLatitude());
						longitude = String.valueOf(locNew.getLongitude());

					} else {
						// N/W Tower Info Start
						locNew = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (locNew != null) {
							latitude = String.valueOf(locNew.getLatitude());
							longitude = String.valueOf(locNew.getLongitude());
						}

					}

					accZipcode = Constants.getZipCode();

				callFindLocAsyncTask();

			} else {
				accZipcode = Constants.getZipCode();
				callFindLocAsyncTask();
			}

			setUpMapIfNeeded();

			findlocationListView.removeFooterView(moreResultsView);

			//user for hamburger in modules
			drawerIcon.setVisibility(View.VISIBLE);
			drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);

		} catch (Exception e) {
			e.printStackTrace();
		}

		rightImage.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{

				if (GlobalConstants.isFromNewsTemplate) {
					Intent intent = null;
					switch (GlobalConstants.className) {
						case Constants.COMBINATION:
							intent = new Intent(FindLocationActivity.this, CombinationTemplate
									.class);
							break;
						case Constants.SCROLLING:
							intent = new Intent(FindLocationActivity.this, ScrollingPageActivity
									.class);
							break;
						case Constants.NEWS_TILE:
							intent = new Intent(FindLocationActivity.this,
									TwoTileNewsTemplateActivity.class);
							break;
					}
					if (intent != null) {
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
					}
				} else {
					if (SortDepttNTypeScreen.subMenuDetailsList != null) {
						SortDepttNTypeScreen.subMenuDetailsList.clear();
						SubMenuStack.clearSubMenuStack();
						SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<SubMenuDetails>();
					}
					LoginScreenViewAsyncTask.bIsLoginFlag = true;
					callingMainMenu();
				}
			}
		});

		//user for hamburger in modules
		drawerIcon.setVisibility(View.VISIBLE);
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
		findlocationListView.setOnScrollListener(new AbsListView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				FindLocationActivity.this.visibleItemCount = visibleItemCount;
				Log.d("visibleItemCount : ",""+visibleItemCount);
			}
		});
	}

	@Override
	public void onToggleClick(String toggleBtnName)
	{
		if (toggleBtnName.equalsIgnoreCase("Distance")) {
			sortTextView.setText(R.string.sort_by_distance);
			sortColumn = "distance";
			setToggleButtonColor();
			freshApiCall();
		} else if (toggleBtnName.equalsIgnoreCase("Name")) {
			sortTextView.setText(R.string.sort_by_name);
			sortColumn = "atoz";
			setToggleButtonColor();
			freshApiCall();
		} else {
			CommonConstants.startMapScreen(FindLocationActivity.this, getRetailerDetails(), false);
		}
	}
	private void setToggleButtonColor()
	{
		if (sortColumn.equalsIgnoreCase("Distance")) {
			sortTextView.setText(R.string.sort_by_distance);
			if (Build.VERSION.SDK_INT >= 16) {
				mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
				mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
			}else{
				mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
				mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
			}

			mLabelName.setTextColor(getResources().getColor(R.color.white));
			mLabelName.setTypeface(null, Typeface.NORMAL);
			mLabelDistance.setTextColor(getResources().getColor(R.color.black));
			mLabelDistance.setTypeface(null, Typeface.BOLD);
		}
		if (sortColumn.equalsIgnoreCase("Name") || sortColumn.equalsIgnoreCase("atoz")) {
			sortTextView.setText(R.string.sort_by_name);
			if (Build.VERSION.SDK_INT >= 16) {
				mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
				mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
			}else{
				mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
				mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
			}
			mLabelDistance.setTextColor(getResources().getColor(R.color.white));
			mLabelDistance.setTypeface(null, Typeface.NORMAL);
			mLabelName.setTextColor(getResources().getColor(R.color.black));
			mLabelName.setTypeface(null, Typeface.BOLD);
		}
	}

	private void getSortItemName(){

		HashMap<String, String> resultSet;
		resultSet = ((HubCityContext)getApplicationContext()).getFilterValues();
		if (resultSet != null && !resultSet.isEmpty()) {
			if (resultSet.containsKey("SortBy")) {
				sortColumn = resultSet.get("SortBy");
			}
		}
		setToggleButtonColor();
	}

	private void setUpMapIfNeeded()
	{
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.

			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				latLngBounds = LatLngBounds.builder();
				UiSettings mUiSettings = mMap.getUiSettings();
				mUiSettings.setAllGesturesEnabled(true);
				mMap.setOnInfoWindowClickListener(this);
			}
		}
	}

	private class GetZipcode extends AsyncTask<String, Void, String>
	{
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute()
		{
			mDialog = ProgressDialog.show(FindLocationActivity.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		// u try to do this 2screens
		@Override
		protected String doInBackground(String... params)
		{

			String result = "false";
			try {
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();
				String fetchuserlocationpoints = Properties.url_local_server
						+ Properties.hubciti_version
						+ "thislocation/fetchuserlocationpoints?userId=";
				String urlParameters = mUrlRequestParams.getUserZipcode();
				urlParameters = URLEncoder.encode(urlParameters, "utf-8");
				jsonObject = mServerConnections.getJSONFromUrl(false,
						fetchuserlocationpoints + urlParameters, null);
				if (jsonObject != null) {
					if (jsonObject.has("UserLocationPoints")) {
						jsonObject = jsonObject
								.getJSONObject("UserLocationPoints");
						if (jsonObject.getString("postalCode") != null) {
							accZipcode = jsonObject.getString("postalCode");
						} else {
							accZipcode = null;
						}
					}
				}
				result = "true";
			} catch (Exception e) {
				e.printStackTrace();
				result = "true";
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result)
		{
			try {
				mDialog.dismiss();
				callFindLocAsyncTask();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void findNavigation(int position)
	{

		Item i = items.get(position);

			FindRetailerSearchEntryItem ei = (FindRetailerSearchEntryItem) i;

			Intent findlocationInfo = new Intent(FindLocationActivity.this,
					RetailerActivity.class);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
					ei.retailerId);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAILE_LOCATIONID,
					ei.retailerLocId);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_LIST_ID,
					ei.retListId);
			Log.v("", "RETAILER LIST**** " + ei.retListId);
			findlocationInfo.putExtra(Constants.MAIN_MENU_ID, strMainMenuId);
			findlocationInfo
					.putExtra(CommonConstants.TAG_DISTANCE, ei.distance);
			findlocationInfo.putExtra(CommonConstants.TAG_FINDLOCATION_LAT,
					ei.latitude);
			findlocationInfo.putExtra(CommonConstants.TAG_FINDLOCATION_LON,
					ei.longitude);
			try {
				findlocationInfo.putExtra("zipCode", accZipcode);
				findlocationInfo.putExtra("fromsearch", true);
			} catch (Exception e) {
				e.printStackTrace();

			}
			findlocationInfo.putExtra(CommonConstants.TAG_RETAILER_NAME,
					ei.retailerName);
			findlocationInfo.putExtra(CommonConstants.TAG_BANNER_IMAGE_PATH,
					ei.bannerImagePath);

			findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
					ei.ribbonAdImagePath);
			findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_AD_URL,
					ei.ribbonAdUrl);
			findlocationInfo.putExtra("Find", true);
			findlocationInfo.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
					mItemId);
			findlocationInfo.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
					bottomBtnId);
			startActivityForResult(findlocationInfo, Constants.STARTVALUE);

			mHubCiti.setCancelled(false);



	}

	private void hideKeyBoard()
	{
		FindLocationActivity.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private void hideKeyboardItem()
	{
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(retailerEditBox.getWindowToken(), 0);
	}

	@Override
	public void onDestroy()
	{
		if (locationManager != null) {
			locationManager.removeUpdates(new ScanSeeLocListener());
		}
		HubCityContext mHubCity = (HubCityContext) getApplicationContext();
		mHubCity.clearArrayAndAllValues(false);
		HubCityContext.isDoneClicked = false;
		cancelAsyncTasks();

		super.onDestroy();
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();

		finishAction();
	}

	boolean nextPage = false;
	boolean isLoadMore;

	private class FindLocationCategory extends AsyncTask<String, Void, String>
	{
		JSONArray jsonArrayScanseeData;
		private ProgressDialog mDialog;
		String responseCode = "", responseText = "";

		@Override
		protected void onPreExecute()
		{
			if (!isLoadMore) {
				isLoadMore = true;
				mDialog = ProgressDialog.show(FindLocationActivity.this, "",
						Constants.DIALOG_MESSAGE, true);
				mDialog.setCancelable(false);
			}
		}

		@Override
		protected String doInBackground(String... params)
		{

			dupeList = new ArrayList<>();
			jsonArrayScanseeData = new JSONArray();
			String result = "false";
			try {

				lastVisited = lastVisitedNo + "";

				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();

				String radius = null, searchKey = null;
				String filterId = "";
				String filterValueId = "";
				String cityIds = "";
				String sorted_cities = "";
				HashMap<String, String> resultSet = new HashMap<>();
				resultSet = mHubCiti.getFilterValues();
				sorted_cities = Constants.getSortedCityIds(new CityDataHelper().getCitiesList
						(FindLocationActivity.this));

				if (resultSet != null && !resultSet.isEmpty()) {

					if (resultSet.containsKey("locSpecials")) {
						locSpecials = resultSet.get("locSpecials");
					}

					if (resultSet.containsKey("savedinterests")) {
						interests = resultSet.get("savedinterests");
					}

					if (resultSet.containsKey("savedOptionFilterIds")) {
						filterId = resultSet.get("savedOptionFilterIds");
					}

					if (resultSet.containsKey("savedFValueIds")) {
						filterValueId = resultSet.get("savedFValueIds");
					}

					if (resultSet.containsKey("savedCityIds")) {
						cityIds = resultSet.get("savedCityIds");
						cityIds = cityIds.replace("All,", "");
					}

					if (resultSet.containsKey("savedSubCatIds")) {
						if (resultSet.get("savedSubCatIds") != null && !resultSet.get
								("savedSubCatIds").equals("")) {
							subCatId = resultSet.get("savedSubCatIds");
						}
					}
				}

				if (cityIds != null && !cityIds.isEmpty()) {
					sorted_cities = Constants.getSortedCityIds(cityIds);
				}
				radius = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).getString
						("radious", "");
				String get_findcategory_sscatsearch = Properties.url_local_server
						+ Properties.hubciti_version + "find/sscatsearchjson";

				JSONObject urlParameters = mUrlRequestParams.getFindLocationInfo(
						mItemId, catName, "" + lastVisited, latitude,
						longitude, radius, searchKey, subCatId, bottomBtnId,
						sortColumn, filterId, filterValueId, locSpecials,
						interests, sorted_cities, accZipcode, CommonConstants
								.convertDeviceTimeToUTC());

//				String encodedurlParameters = URLEncoder.encode(urlParameters.toString(), "utf-8");
//				urlParameters = new JSONObject(encodedurlParameters);
				JSONObject jsonObject = mServerConnections.getUrlJsonPostResponse(
						get_findcategory_sscatsearch, urlParameters);

				if (jsonObject != null) {

					responseCode = jsonObject.getString("responseCode");
					responseText = jsonObject.getString("responseText");
//					if (!jsonObject
//							.has("retailerDetail")) {
//						return "false";
//					}
//
					// For BottomButton

					if (jsonObject.has("bottomBtnList")) {

						hasBottomBtns = true;

						try {
							ArrayList<BottomButtonBO> bottomButtonList = bb
									.parseForBottomButton(jsonObject);
							BottomButtonListSingleton
									.clearBottomButtonListSingleton();
							BottomButtonListSingleton
									.getListBottomButton(bottomButtonList);

						} catch (JSONException e1) {
							e1.printStackTrace();
						}

					} else {
						hasBottomBtns = false;
					}

					if (!"10000".equals(responseCode)) {

						try {
							findlocationListView
									.removeFooterView(moreResultsView);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						return "false";
					}

					try {

						jsonArrayScanseeData = new JSONArray();

						try {
							jsonArrayScanseeData
									.put(jsonObject.getJSONObject("retailerDetail"));
						} catch (Exception e) {
							jsonArrayScanseeData = jsonObject.getJSONArray("retailerDetail");
						}

						retailersList = new ArrayList<>();
						arrRetailerId = new ArrayList<>();
						arrRetailerLocId = new ArrayList<>();
						for (int arrayCount = 0; arrayCount < jsonArrayScanseeData
								.length(); arrayCount++) {
							HashMap<String, String> scanseeData = new HashMap<>();
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants
											.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERIBBONADURL));
							}


							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERETAILERID));
								arrRetailerId.add(jsonArrayScanseeData
										.getJSONObject(
												arrayCount)
										.getString(
												CommonConstants
														.TAG_FINDLOCATION_SCANSEERETAILERID));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants
											.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID));
								arrRetailerLocId.add(jsonArrayScanseeData
										.getJSONObject(
												arrayCount)
										.getString(
												CommonConstants
														.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLISTID)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEERETAILERLISTID,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERETAILERLISTID));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_RETAILERNAME)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_RETAILERNAME,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_RETAILERNAME));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEEADDRESS1,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEADDRESS1));
							}

							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS2)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEEADDRESS2,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEADDRESS2));
							}

							// Complete Address
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_RETAILER_OPEN_CLOSE)) {
								scanseeData
										.put(CommonConstants
														.TAG_RETAILER_OPEN_CLOSE,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_RETAILER_OPEN_CLOSE));
							}

							// Adding city
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEECITY)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEECITY,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEECITY));
							}

							// Adding state
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEESTATE)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEESTATE,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEESTATE));
							}

							// Adding postal code
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEEPOSTALCODE)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEEPOSTALCODE,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEPOSTALCODE));
							}

							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEDISTANCE));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEELAT)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_LAT,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEELAT));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEELON)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_LON,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEELON));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_RETAILERNAME)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_NAME,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_RETAILERNAME));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_ADDRESS,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS));
							}

							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEESALEFLAG));

							}

							scanseeData.put("itemName", "scansee");
							findlocationList.add(scanseeData);

							retailersList.add(scanseeData); // @Rekha
						}

						if (jsonObject.has("mainMenuId")) {
							strMainMenuId = jsonObject.getString("mainMenuId");
							Constants.saveMainMenuId(strMainMenuId);
							Log.v("", "MAIN MENU ID : " + strMainMenuId);
						}
						if (jsonObject.has(CommonConstants.FINDPRODUCTSEARCHPRODUCTNEXTPAGE)) {

							if ("1".equalsIgnoreCase(jsonObject.getString(
									CommonConstants.FINDPRODUCTSEARCHPRODUCTNEXTPAGE))) {

								if (jsonObject.has("maxRowNum")) {
									tempLastVisit = jsonObject.getString("maxRowNum");

									if (Integer.valueOf(tempLastVisit) > Integer
											.valueOf(lastVisitedNo)) {
										lastVisitedNo = Integer
												.valueOf(tempLastVisit);
									}

									nextPage = lastVisitedNo < Integer
											.valueOf(jsonObject.getString("maxCnt"));

								}

							} else {
								nextPage = false;
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						jsonArrayScanseeData = null;
						findlocationList.clear();
						dupeList.clear();
					}

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
				try {
					findlocationListView.removeFooterView(moreResultsView);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result)
		{
			try {
				markers = new ArrayList<>();
				isAlreadyLoading = false;
				if ("true".equals(result)) {
//				Calling AsyncTask to update retailer impression
					new RetailerImpressionUpdateAsync(catName, TextUtils.join
							(",", arrRetailerId), TextUtils.join(",", arrRetailerLocId)).execute();
					try {
						if (nextPage) {

							try {
								moreResultsView.setVisibility(View.VISIBLE);
								findlocationListView
										.removeFooterView(moreResultsView);
							} catch (Exception ex) {
								ex.printStackTrace();
							}

							// moreInfo.setVisibility(View.VISIBLE);

							findlocationListView.addFooterView(moreResultsView);

						} else {
							findlocationListView.removeFooterView(moreResultsView);
						}

					} catch (Exception ex) {
						ex.printStackTrace();
					}

					mFindList = new ArrayList<>();
					mFindList = retailersList;

					if (!isViewMore && !"".equalsIgnoreCase(sortColumn)) {

						String sortText = "";

						if ("distance".equalsIgnoreCase(sortColumn)) {
							sortText = "Sorted By Distance";
						} else if ("atoz".equalsIgnoreCase(sortColumn)) {
							sortText = "Sorted By Name";

						}
						sortTextView.setText(sortText);
					}

					for (int i = 0; i < mFindList.size(); i++) {
						items.add(entryItemOject(i));
					}

					findlocationListAdapter = new FindLocationGoogleListAdapter(
							FindLocationActivity.this, items, sortColumn);
					findlocationListView.setAdapter(findlocationListAdapter);

					for (int listCount = 0; listCount < dupeList.size(); listCount++) {
						String lat, lon;
						HashMap<String, String> item = dupeList.get(listCount);
						lat = item.get(CommonConstants.TAG_FINDLOCATION_LAT);
						lon = item.get(CommonConstants.TAG_FINDLOCATION_LON);
						if ((lat == null || "N/A".equalsIgnoreCase(lat))
								|| (lon == null || "N/A".equalsIgnoreCase(lon))) {

							dupeList.remove(listCount);
						}
					}

					if (null != lastVisited)
					{
						previousPosition = previousPosition-visibleItemCount + 1;
						findlocationListView.setSelection(Integer
								.valueOf(previousPosition));
					}

					if (bb != null) {
						bb.setOptionsValues(getListValues(), getRetailerDetails());
					}

				} else {

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							FindLocationActivity.this);
					findlocationListAdapter = new FindLocationGoogleListAdapter(
							FindLocationActivity.this, new ArrayList<Item>(),
							sortColumn);
					findlocationListView.setAdapter(findlocationListAdapter);
					alertDialogBuilder.setCancelable(false);
					if ("".equals(responseText)) {
						responseText = "Connection Timeout!!";
					}
					alertDialogBuilder.setMessage(Html.fromHtml(responseText
							.replace("\\n", "<br/>")));
					alertDialogBuilder.setPositiveButton("OK",
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{
									dialog.dismiss();
									// finishAction();
								}
							});
					alertDialogBuilder.show();
				}

				// Added to disable toggle click
				CommonConstants.disableClick = false;

				if (hasBottomBtns && enableBottomButton) {

					ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams)
							findlocationListView
									.getLayoutParams();
					mlp.setMargins(0, 0, 0, 2);

					bb.createbottomButtontTab(linearLayout, false);
				}

				linearLayout.setVisibility(View.VISIBLE);

				if (mDialog != null && mDialog.isShowing()) {
					mDialog.dismiss();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private FindRetailerSearchEntryItem entryItemOject(int i)
	{
		return (new FindRetailerSearchEntryItem(
				mFindList.get(i).get("itemName"),
				mFindList
						.get(i)
						.get(CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH),
				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH),
				mFindList
						.get(i)
						.get(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH),
				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID),

				mFindList
						.get(i)
						.get(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLISTID),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_RETAILERNAME),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1),
				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS2),
				mFindList.get(i).get(
						CommonConstants.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEECITY),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEESTATE),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEEPOSTALCODE),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEELAT),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEELON),

				mFindList.get(i).get(
						CommonConstants.TAG_FINDLOCATION_RETAILERNAME), mFindList.get(i).get(
				CommonConstants.TAG_RETAILER_OPEN_CLOSE)));

	}

	@Override
	public void onClick(View v)
	{

		switch (v.getId()) {

			case R.id.find_retailer_cancel:
				findRetailerCancel();
				break;

			case R.id.find_retailer_edit_text:
				findRetailerEditText();
				break;

			case R.id.viewMore:
				enableBottomButton = false;
				isViewMore = true;
				callFindLocAsyncTask();
				break;

			default:
				break;
		}
	}

	void loadMore()
	{
		// Added to disable toggle click
		CommonConstants.disableClick = true;
		isAlreadyLoading = true;
		enableBottomButton = false;
		isViewMore = true;
		callFindLocAsyncTask();
	}

	private HashMap<String, String> getListValues()
	{
		HashMap<String, String> values = new HashMap<>();

		String catId = "";

		if (getIntent().hasExtra("selectedCat")) {
			catName = getIntent().getExtras().getString("selectedCat");
		}

		if (getIntent().hasExtra("selectedCatId")) {
			catId = getIntent().getExtras().getString("selectedCatId");
		}

		values.put("Class", "Find");
		values.put("selectedCatId", catId);

		values.put("catName", catName);

		values.put("subCatId", subCatId);
		values.put("mLinkId", mLinkId);

		values.put("mItemId", mItemId);

		values.put("sortChoice", sortColumn);

		values.put("typeName", typeName);

		values.put("latitude", latitude);

		values.put("longitude", longitude);

		values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

		return values;

	}

	private ArrayList<RetailerBo> getRetailerDetails()
	{
		ArrayList<RetailerBo> mRetailerList = new ArrayList<>();

		for (int i = 0; i < findlocationList.size(); i++) {

			String retailerName = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_RETAILERNAME);
			String retailAddress = findlocationList.get(i).get(
					CommonConstants.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS);
			String latitude = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_LAT);
			String longitude = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_LON);
			String retailId = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID);
			String ribbonAdImagePath = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH);
			String ribbonAdURL = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL);
			String locationID = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID);
			String bannerAd = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH);
			RetailerBo retailerObject = new RetailerBo(null, retailId,
					retailerName, locationID, null, retailAddress, null, null,
					bannerAd, ribbonAdImagePath, ribbonAdURL, null, latitude,
					longitude, null);
			mRetailerList.add(retailerObject);

		}
		return mRetailerList;
	}

	private void findRetailerEditText()
	{
		hideKeyboardItem();
		isRetailerCatSearch = true;
		lastVisitedNo = 0;

		Intent locationIntent = new Intent(FindLocationActivity.this,
				FindRetailerSearchActivity.class);
		locationIntent.putExtra("retSearchText", retailerEditBox.getText()
				.toString().trim());
		locationIntent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
				bottomBtnId);
		locationIntent.putExtra("isRetailerCatSearch", true);
		locationIntent.putExtra("mItemId", mItemId);

		String catName = "", catId = "";

		if (getIntent().hasExtra("selectedCat")) {
			catName = getIntent().getExtras().getString("selectedCat");
		}

		if (getIntent().hasExtra("selectedCatId")) {
			catId = getIntent().getExtras().getString("selectedCatId");
		}

		locationIntent.putExtra("parCatId", parCatId);
		locationIntent.putExtra("subCatId", subCatId);
		locationIntent.putExtra("selectedCatId", catId);
		locationIntent.putExtra("catName", catName);

		finishAction();
		startActivityForResult(locationIntent, Constants.STARTVALUE);

	}

	private void findRetailerCancel()
	{
		if (retailerEditBox.getText().toString() != null) {
			retailerEditBox.setText("");
			hideKeyboardItem();
		}

	}

	@Override
	public void onInfoWindowClick(Marker marker)
	{
		if (!markers.isEmpty()) {

			Item i = items.get(markers.indexOf(marker));

			FindRetailerSearchEntryItem ei = (FindRetailerSearchEntryItem) i;

			Intent findlocationInfo = new Intent(FindLocationActivity.this,
					RetailerActivity.class);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
					ei.retailerId);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_LIST_ID,
					ei.retListId);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAILE_LOCATIONID,
					ei.retailerLocId);
			findlocationInfo.putExtra(Constants.MAIN_MENU_ID, strMainMenuId);
			findlocationInfo
					.putExtra(CommonConstants.TAG_DISTANCE, ei.distance);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAILER_NAME,
					ei.retailerName);
			findlocationInfo.putExtra(CommonConstants.TAG_BANNER_IMAGE_PATH,
					ei.bannerImagePath);
			findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
					ei.ribbonAdImagePath);
			findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_AD_URL,
					ei.ribbonAdUrl);
			findlocationInfo.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
					mItemId);
			findlocationInfo.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
					bottomBtnId);
			findlocationInfo.putExtra("Find", true);
			startActivity(findlocationInfo);

			mHubCiti.setCancelled(false);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finishAction();
		}

		if (resultCode == 30001 || resultCode == 2) {
			isRefresh = false;
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		isRefresh = true;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}

		// Add screen name when needed
		if (bb != null) {
			bb.setActivityInfo(activity, "");
			bb.setOptionsValues(getListValues(), getRetailerDetails());
		}

		if (mHubCiti.isCancelled()) {
			return;
		}

		if (!isRefresh) {
			if (!isItemClicked) {
				items.clear();

				moreResultsView.setVisibility(View.GONE);
				mFindList = new ArrayList<>();

				findlocationListAdapter = new FindLocationGoogleListAdapter(
						FindLocationActivity.this, items, sortColumn);
				findlocationListView.setAdapter(findlocationListAdapter);
				getSortItemName();
				freshApiCall();
			}
		}

		isRefresh = false;
		isItemClicked = false;

	}

	private void freshApiCall()
	{
		lastVisitedNo = 0;
		findlocationList.clear();
		isLoadMore = false;
		isViewMore = false;
		items.clear();
		enableBottomButton = false;
		callFindLocAsyncTask();
	}

	private void callFindLocAsyncTask()
	{
		previousPosition = items.size();
		if (isFirst) {
			findLocAsyncTask = new FindLocationCategory();
			findLocAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;
		}

		if (findLocAsyncTask != null) {
			if (!findLocAsyncTask.isCancelled()) {
				findLocAsyncTask.cancel(true);
			}

			findLocAsyncTask = null;
		}

		findLocAsyncTask = new FindLocationCategory();
		findLocAsyncTask.execute();
	}
	private void cancelAsyncTasks()
	{

		if (findLocAsyncTask != null && !findLocAsyncTask.isCancelled()) {
			findLocAsyncTask.cancel(true);
		}

		findLocAsyncTask = null;

		if (zipcodeAsyncTask != null && !zipcodeAsyncTask.isCancelled()) {
			zipcodeAsyncTask.cancel(true);
		}

		zipcodeAsyncTask = null;
	}

	private void finishAction()
	{
		cancelAsyncTasks();
		finish();
		mHubCiti.clearSavedValues();
	}

}
