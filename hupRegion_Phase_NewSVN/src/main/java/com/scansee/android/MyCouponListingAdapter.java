package com.scansee.android;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.com.hubcity.model.MyCouponObj;
import com.com.hubcity.model.RetailerDetails;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CommonMethods;

import java.util.LinkedList;

/**
 * Created by subramanya.v on 12/9/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class MyCouponListingAdapter extends BaseAdapter {
    private final Activity activity;
    private final int parentHeight;
    private LinkedList<RetailerDetails> listItem;
    private LinkedList<MyCouponObj> listMainItem;

    public MyCouponListingAdapter(Activity activity, int parentHeight, LinkedList<RetailerDetails> listItem, LinkedList<MyCouponObj> listMainItem) {
        this.activity = activity;
        this.parentHeight = parentHeight;
        this.listItem = listItem;
        this.listMainItem = listMainItem;
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        MyCouponObj mainItem = listMainItem.get(0);
        int maxRowNum = mainItem.getMaxRowNum();
        int maxCnt = mainItem.getMaxCnt();
        int pagPosition = position + 1;
        if (maxRowNum < maxCnt) {
            if (pagPosition == maxRowNum) {
                if (!((MyCouponsListingActivity) activity).isLoaded) {
                    ((MyCouponsListingActivity) activity).startPagination(maxRowNum);
                    Log.v("lastvisited", String.valueOf(maxRowNum));
                }
            }
        }

        LayoutInflater layoutInflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.coupon_item, parent, false);
            holder = new ViewHolder();
            holder.ivCouponImage = (ImageView) convertView.findViewById(R.id.coupon_image);
            holder.tvRetailerName = (TextView) convertView.findViewById(R.id.retailer_name);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.price);
            holder.tvCouponName = (TextView) convertView.findViewById(R.id.coupon_name);
            holder.tvDistance = (TextView) convertView.findViewById(R.id.distance);
            holder.tvCouponCount = (TextView) convertView.findViewById(R.id.coupon_count);
            holder.pbProgressBar = (ProgressBar) convertView.findViewById(R.id.progress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        RelativeLayout.LayoutParams itemParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, parentHeight);
        convertView.setLayoutParams(itemParam);

        RetailerDetails listData = listItem.get(position);
        String couponName = listData.getCouponName();
        String couponImagePath = listData.getCouponImagePath();
        String bannerTitle = listData.getBannerTitle();
        int counts = listData.getCounts();

        String retName = listData.getRetName();
        String distance = listData.getDistance();


        if (couponImagePath != null && !couponImagePath.isEmpty()) {
            new CommonMethods().loadImage(activity, holder.pbProgressBar, couponImagePath, holder.ivCouponImage);
        }
        if (retName != null && !retName.isEmpty()) {
            holder.tvRetailerName.setText(retName);
            holder.tvRetailerName.setVisibility(View.VISIBLE);
        }

        if (couponName != null && !couponName.isEmpty()) {
            holder.tvCouponName.setText(couponName);
            holder.tvCouponName.setVisibility(View.VISIBLE);
        }
        if (distance != null && !distance.isEmpty()) {
            if (counts > 1) {
                holder.tvDistance.setText(activity.getString(R.string.close_loc) + " " + distance);
            } else {
                holder.tvDistance.setText(distance);
            }
            holder.tvDistance.setVisibility(View.VISIBLE);
        }
        if (counts >= 1) {
            StringBuilder Couponcount = new StringBuilder();
            Couponcount.append(String.valueOf(counts));
            Couponcount.append(" ");
            Couponcount.append(activity.getString(R.string.part_loc));
            holder.tvCouponCount.setText(Couponcount.toString());
            holder.tvCouponCount.setVisibility(View.VISIBLE);
        } else {
            holder.tvCouponCount.setVisibility(View.INVISIBLE);
        }

        if (bannerTitle != null && !bannerTitle.isEmpty()) {
            holder.tvPrice.setText(bannerTitle);
            holder.tvPrice.setVisibility(View.VISIBLE);
        }


        return convertView;
    }

    public void updateList(LinkedList<RetailerDetails> listItem, LinkedList<MyCouponObj> listMainItem) {
        this.listItem = listItem;
        this.listMainItem = listMainItem;
    }

    private class ViewHolder {
        private ImageView ivCouponImage;
        private TextView tvCouponName;
        private TextView tvPrice;
        private TextView tvDistance;
        private TextView tvCouponCount;


        public TextView tvRetailerName;

        public ProgressBar pbProgressBar;
    }
}
