package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.screens.ProductCLRInfoActivity;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CouponsProductListAdapter2 extends BaseAdapter implements
		OnClickListener {
	private ProductCLRInfoActivity activity;
	private ArrayList<HashMap<String, String>> couponsproductsList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;
	HashMap<String, String> couponsproductData = null;

	public CouponsProductListAdapter2(Activity activity,
			ArrayList<HashMap<String, String>> couponsproductsList) {
		this.activity = (ProductCLRInfoActivity) activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.couponsproductsList = couponsproductsList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return couponsproductsList.size();
	}

	@Override
	public Object getItem(int id) {
		return couponsproductsList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		if (convertView == null) {

			view = inflater.inflate(R.layout.lisitem_couponwishlist, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.couponimage = (ImageView) view
					.findViewById(R.id.coupon_image);
			viewHolder.couponname = (TextView) view
					.findViewById(R.id.coupon_product_name);
			viewHolder.couponDisamt = (TextView) view
					.findViewById(R.id.coupon_product_couponDiscountAmount);
			viewHolder.couponexpiretext = (TextView) view
					.findViewById(R.id.coupon_product_expires);
			viewHolder.couponexpirationdate = (TextView) view
					.findViewById(R.id.coupon_product_couponExpireDate);
			viewHolder.couponimagePath = (ImageView) view
					.findViewById(R.id.coupon_imagepath);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		if (viewHolder != null) {
			if ("red".equalsIgnoreCase(couponsproductsList.get(position).get(
					CommonConstants.WISHLISTCOUPONUSAGE))) {
				viewHolder.couponimage.findViewById(R.id.coupon_image)
						.setBackgroundResource(R.drawable.wl_add_cpn_live);
			} else {
				viewHolder.couponimage.findViewById(R.id.coupon_image)
						.setBackgroundResource(R.drawable.coupons_dollar);
			}
			viewHolder.couponname.setText(couponsproductsList.get(position)
					.get(CommonConstants.WISHLISTCOUPONNAME));

			viewHolder.couponDisamt.setText(couponsproductsList.get(position)
					.get(CommonConstants.WISHLISTCOUPONDISCOUNTAMOUNT));
			viewHolder.couponexpirationdate.setText(couponsproductsList.get(
					position).get(CommonConstants.WISHLISTCOUPONEXPIREDATE));
			viewHolder.couponexpiretext.setText("Expires :");
			viewHolder.couponimagePath.setTag(couponsproductsList.get(position)
					.get(CommonConstants.WISHLISTCOUPONIMAGEPATH));
			customImageLoader.displayImage(
					couponsproductsList.get(position).get(
							CommonConstants.WISHLISTCOUPONIMAGEPATH), activity,
					viewHolder.couponimagePath);
		}
		view.findViewById(R.id.shoppinglist_delete).setOnClickListener(this);
		view.findViewById(R.id.shoppinglist_delete).setTag(position);

		return view;
	}

	public static class ViewHolder {
		protected ImageView couponimage;
		protected TextView couponexpirationdate;
		protected TextView couponexpiretext;
		protected TextView couponDisamt;
		protected TextView description;
		protected ImageView couponimagePath;
		protected TextView couponname;

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.shoppinglist_delete:
			shoppinglistDelete(view);
			break;

		default:
			break;
		}

	}

	private void shoppinglistDelete(View view) {
		int position = (Integer) view.getTag();
		activity.isDelete = false;
		activity.redeemCoupon(couponsproductsList.get(position).get(
				CommonConstants.WISHLISTCOUPONID));

	}

}
