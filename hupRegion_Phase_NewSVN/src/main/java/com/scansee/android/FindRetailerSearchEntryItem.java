package com.scansee.android;

import com.hubcity.android.businessObjects.Item;

public class FindRetailerSearchEntryItem implements Item
{

	protected final String bannerImagePath;
	protected final String logoImagePath;
	protected final String ribbonAdUrl;
	protected final String retailerId;
	protected final String retListId;

	protected final String retailerLocId;
	protected final String locationOpen;
	protected final String retailerName;
	protected final String retailerAddress1;
	protected final String retailerAddress2;
	protected final String city;
	protected final String state;
	protected final String postalCode;
	protected final String saleFlag;
	protected final String distance;
	protected final String latitude;
	protected final String longitude;

	protected final String ribbonAdImagePath;

	public FindRetailerSearchEntryItem(String itemName, String bannerImagePath,
			String logoImagePath, String ribbonAdImagePath, String ribbonAdUrl,
			String retailerId, String retailerLocId, String retListID,
			String retailerName, String retailerAddress1, String retailerAddress2,
			String completeAddress, String city, String state,
			String postalCode, String saleFlag, String distance,
			String latitude, String longitude, String name, String locationOpen)
	{

		this.bannerImagePath = bannerImagePath;
		this.logoImagePath = logoImagePath;
		this.ribbonAdImagePath = ribbonAdImagePath;
		this.ribbonAdUrl = ribbonAdUrl;
		this.retailerId = retailerId;
		this.retListId = retListID;
		this.retailerLocId = retailerLocId;
		this.retailerName = retailerName;
		this.retailerAddress1 = retailerAddress1;
		this.retailerAddress2 = retailerAddress2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.saleFlag = saleFlag;
		this.distance = distance;
		this.latitude = latitude;
		this.longitude = longitude;
		this.locationOpen = locationOpen;
	}

	@Override
	public boolean isSection()
	{
		return false;
	}

}
