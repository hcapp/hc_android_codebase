package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CustomImageLoader;

public class ScanNowHistoryListAdapter extends BaseAdapter {
	private Activity activity;
	private ArrayList<HashMap<String, String>> scannowhistoryList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;
	String itemName = null;
	HashMap<String, String> historyData = null;

	public ScanNowHistoryListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> scannowhistoryList) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.scannowhistoryList = scannowhistoryList;
		this.activity = activity;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return scannowhistoryList.size();
	}

	@Override
	public Object getItem(int id) {
		return scannowhistoryList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;

		historyData = scannowhistoryList.get(position);
		itemName = historyData.get("itemName");

		viewHolder = new ViewHolder();
		if ("dateScanned".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_scannow_date, parent,false);
			viewHolder.dateScanned = (TextView) view
					.findViewById(R.id.dummydata_scannow_date);
		} else if ("viewMore".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_get_retailers_viewmore,
					parent,false);

		} else {
			view = inflater.inflate(R.layout.listitem_scannow_products, parent,false);
			viewHolder.productName = (TextView) view
					.findViewById(R.id.scannow_products_text);
			viewHolder.productImagePath = (ImageView) view
					.findViewById(R.id.scannow_image);

		}
		view.setTag(viewHolder);

		if ("dateScanned".equalsIgnoreCase(itemName)) {
			viewHolder.dateScanned.setText(historyData.get("dateScanned"));
		} else if ("details".equalsIgnoreCase(itemName)) {
			viewHolder.productName.setText(historyData.get("productName"));
			viewHolder.productImagePath.setTag(historyData
					.get("productImagePath"));
			customImageLoader.displayImage(historyData.get("productImagePath"),
					activity, viewHolder.productImagePath);
		}

		if (position == scannowhistoryList.size() - 1) {
			if (!((ScanNowHistoryActivity) activity).isAlreadyLoading) {
				if (((ScanNowHistoryActivity) activity).nextPage) {
					((ScanNowHistoryActivity) activity).viewMore();
				}
			}
		}
		return view;
	}

	public static class ViewHolder {
		protected ImageView productImagePath;
		protected TextView productName;
		protected TextView dateScanned;

	}

}
