package com.scansee.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.ShareInformation;
import com.scansee.hubregion.R;

public class ScanseeFileActivity extends CustomTitleBar implements
		OnClickListener {
	WebView webView;
	String urL;
	ProgressBar webProgress;
	Button backBtn_web, fwdBtn, reloadBtn, stopBtn;

	private String filename;
	private File myFilesDir;

	ShareInformation shareInfo;
	boolean isShareTaskCalled;
	boolean isPDFView;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.scansee_browser);
			urL = getIntent().getExtras().getString(
                    RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH);

			title.setText("Details");

			webView = (WebView) findViewById(R.id.hubciti_webview);
			backBtn_web = (Button) findViewById(R.id.browser_btn_back);
			fwdBtn = (Button) findViewById(R.id.browser_btn_foward);
			stopBtn = (Button) findViewById(R.id.browser_btn_stop);
			reloadBtn = (Button) findViewById(R.id.browser_btn_reload);
			backBtn_web.setOnClickListener(this);
			fwdBtn.setOnClickListener(this);
			stopBtn.setOnClickListener(this);
			reloadBtn.setOnClickListener(this);

			reloadBtn.setEnabled(true);

			myFilesDir = new File(
                    android.os.Environment.getExternalStorageDirectory(), "HubCiti");

			if (urL != null) {

                urL = urL.replaceAll(" ", "%20");
                filename = urL;

                new DownloadFile().execute();

            }
			webProgress = (ProgressBar) findViewById(R.id.progressBar_browser);
			webView.getSettings().setJavaScriptEnabled(true);
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                webView.getSettings().setDisplayZoomControls(false);
            }
			webView.getSettings().setBuiltInZoomControls(true);
			webView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url.startsWith("http:") || url.startsWith("https:")) {
                        return false;
                    }

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                    finish();
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(webView, url);
                    webProgress.setVisibility(View.GONE);
                    backBtn_web.setEnabled(view.canGoBack());
                    fwdBtn.setEnabled(view.canGoForward());
                }
            });
			webView.setWebChromeClient(new WebChromeClient() {

                @Override
                public void onProgressChanged(WebView view, int newProgress) {
                    super.onProgressChanged(view, newProgress);
                    webProgress.setProgress(newProgress);
                    if (newProgress == 100) {
                        webProgress.setVisibility(View.GONE);
                        stopBtn.setEnabled(false);

                    } else {
                        webProgress.setVisibility(View.VISIBLE);
                        stopBtn.setEnabled(true);

                    }
                }

            });

			// Call for Share Information
			if (getIntent().hasExtra("isShare")
                    && getIntent().getExtras().getBoolean("isShare")) {
                shareInfo = new ShareInformation(ScanseeFileActivity.this,
                        getIntent().getExtras().getString("retailerId"), "",
                        getIntent().getExtras().getString("pageId"), getIntent()
                                .getExtras().getString("module"));
                isShareTaskCalled = false;

                leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
                leftTitleImage.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!Constants.GuestLoginId.equals(UrlRequestParams
                                .getUid().trim())) {
                            shareInfo.shareTask(isShareTaskCalled);
                            isShareTaskCalled = true;
                        } else {
                            Constants.SignUpAlert(ScanseeFileActivity.this);
                            isShareTaskCalled = false;
                        }
                    }
                });
            }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onDestroy() {
		webView.clearHistory();
		webView.clearView();
		webView.clearCache(true);
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.browser_btn_back:
			webView.goBack();
			break;
		case R.id.browser_btn_foward:
			webView.goForward();
			break;

		case R.id.browser_btn_stop:
			webView.stopLoading();
			stopBtn.setEnabled(false);
			break;

		case R.id.browser_btn_reload:
			webView.reload();
			break;

		default:
			break;
		}

	}

	private void backAction() {
		if (isPDFView) {
			if (getIntent().hasExtra("isShare")
					&& getIntent().getExtras().getBoolean("isShare")) {

				webView.loadUrl("http://docs.google.com/gview?embedded=true&url="
						+ urL);
			}
		}
	}

	private class DownloadFile extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {

			HttpURLConnection c;
			try {
				URL url = new URL(filename);
				c = (HttpURLConnection) url.openConnection();
				c.setRequestMethod("GET");
				c.setDoOutput(true);
				c.connect();
			} catch (IOException e1) {
				e1.printStackTrace();
				return e1.getMessage();
			}

			File file = new File(myFilesDir, "Read.pdf");

			if (file.exists()) {
				file.delete();
			} else {
				try {
					file.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			if (myFilesDir.mkdirs() || myFilesDir.isDirectory()) {
				try {
					InputStream is = c.getInputStream();
					FileOutputStream fos = new FileOutputStream(myFilesDir
							+ "/" + "Read.pdf");

					byte[] buffer = new byte[1024];
					int len1 = 0;
					while ((len1 = is.read(buffer)) != -1) {
						fos.write(buffer, 0, len1);
					}
					fos.close();
					is.close();

				} catch (Exception e) {
					e.printStackTrace();
					return e.getMessage();
				}

			} else {
				return "Unable to create folder";
			}
			return filename;
		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			if (filename.equalsIgnoreCase(result)) {
				isPDFView = true;

				File file = new File(myFilesDir, "Read.pdf");
				Intent target = new Intent(Intent.ACTION_VIEW);
				target.setPackage("com.adobe.reader");
				target.setDataAndType(Uri.fromFile(file), "application/pdf");
				target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

				try {
					startActivity(target);
					// finish();
				} catch (ActivityNotFoundException e) {
					e.printStackTrace();

					AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
							ScanseeFileActivity.this);
					notificationAlert
							.setMessage(
									"Adobe reader is required to view Interactive PDFs.Do you want to install it?")
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog, int id) {
											// GPS hack
											Intent playIntent = new Intent(
													Intent.ACTION_VIEW);
											playIntent.setData(Uri
													.parse("market://details?id=com.adobe.reader"));
											startActivity(playIntent);
											finish();
										}

									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog, int id) {
											// GPS hack
											if (filename != null) {
												isPDFView = false;
												webView.loadUrl("http://docs.google.com/gview?embedded=true&url="
														+ urL);
											}
										}

									});

					notificationAlert.create().show();
				}

			} else {
				if (filename != null) {
					isPDFView = false;
					webView.loadUrl("http://docs.google.com/gview?embedded=true&url="
							+ urL);
				}
			}

		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		backAction();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);

	}
}
