package com.scansee.android;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.hubcity.android.businessObjects.FilterOptionBO;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class GetCategoryListAsync extends AsyncTask<Void, Void, Void>
{

	private final String radius;
	private final String latitude;
	private final String longitude;
	String moduleName = "", cityExpId = "", filterId = "", catId = "",
			postalCode = "", mItemId = "", bottomBtnId = "", retialId = "",
			retailLocationId = "", fundId = "", searchKey = "", eventTypeId = "";
	Context mContext;
	ArrayList<FilterOptionBO> arrFilterOptionBOs;
	int groupPosition;
	OnCreateFilterChildView filterChildView;
	int bandId;

	private ProgressDialog mDialog;

	public GetCategoryListAsync(Context context, String moduleName,
			String cityExpId, String filterId, int groupPosition, String catId,
			String postalCode, String mItemId,
			OnCreateFilterChildView filterChildView, String bottomBtnId,
			String retialId, String retailLocationId, String fundId, String searchKey, String
			eventTypeId, int bandId, String radius, String latitude, String longitude)
	{
		this.mContext = context;
		this.cityExpId = cityExpId;
		this.filterId = filterId;
		this.moduleName = moduleName;
		this.groupPosition = groupPosition;
		this.catId = catId;
		this.postalCode = postalCode;
		this.mItemId = mItemId;
		this.filterChildView = filterChildView;
		this.bottomBtnId = bottomBtnId;
		this.retialId = retialId;
		this.retailLocationId = retailLocationId;
		this.fundId = fundId;
		this.searchKey = searchKey;
		this.eventTypeId = eventTypeId;
		this.bandId = bandId;
		this.radius = radius;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();

		mDialog = ProgressDialog.show(mContext, "", Constants.DIALOG_MESSAGE,
				true);
		mDialog.setCancelable(false);
	}

	@Override
	protected Void doInBackground(Void... params)
	{
		JSONObject jsonResponse;
		String urlParameters;
		if (moduleName.equals("citiEXP") || moduleName.equals("filter")
				|| moduleName.equals("Events") || moduleName.equals("Band") || moduleName.equals
				("BandEvents") || moduleName.equals
				("Coupons")|| moduleName.equals
				("myaccounts")) {
			if (moduleName.equals("Events")) {
				if (retialId != null && retialId.length() > 0) {
					urlParameters = new UrlRequestParams()
							.createCategoryListParameter(moduleName, cityExpId,
									filterId, mItemId, "", retialId,
									retailLocationId, "", "", eventTypeId, bandId, radius,
									latitude, longitude);
				} else if (fundId != null && fundId.length() > 0) {
					urlParameters = new UrlRequestParams()
							.createCategoryListParameter(moduleName, cityExpId,
									filterId, "", "", retialId,
									retailLocationId, fundId, "", eventTypeId, bandId, radius,
									latitude, longitude);
				} else {

					urlParameters = new UrlRequestParams()
							.createCategoryListParameter(moduleName, cityExpId,
									filterId, mItemId, bottomBtnId, retialId,
									retailLocationId, "", "", eventTypeId, bandId, radius,
									latitude, longitude);
				}
			} else {
				urlParameters = new UrlRequestParams()
						.createCategoryListParameter(moduleName, cityExpId,
								filterId, mItemId, bottomBtnId, "", "", "", searchKey,
								eventTypeId, bandId, radius, latitude, longitude);
			}

			String get_category_filter_list_url = Properties.url_local_server
					+ Properties.hubciti_version + "find/getcategorylist";

			jsonResponse = new ServerConnections().getUrlPostResponse(
					get_category_filter_list_url, urlParameters, true);
		} else {
			urlParameters = new UrlRequestParams().getFindSubCategoryParam(
					catId, mItemId, bottomBtnId, searchKey);

			String url = Properties.url_local_server
					+ Properties.hubciti_version + "find/getsubcategory";

			jsonResponse = new ServerConnections().getUrlPostResponse(url,
					urlParameters, true);
		}

		if (jsonResponse.has("Filter")) {
			try {
				if (jsonResponse.getJSONObject("Filter")
						.getString("responseCode").equals("10000")) {

					JSONArray jsonArray = null;
					JSONObject jsonObject = null;
					try {
						jsonArray = jsonResponse.getJSONObject("Filter")
								.getJSONObject("categoryList")
								.getJSONArray("CategoryInfo");
					} catch (Exception e) {
						jsonObject = jsonResponse.getJSONObject("Filter")
								.getJSONObject("categoryList")
								.getJSONObject("CategoryInfo");
					}
					arrFilterOptionBOs = new ArrayList<>();
					if (jsonArray != null) {
						FilterOptionBO filterOption = new FilterOptionBO();
						filterOption.setFilterGroupName("Category");
						filterOption.setFilterId("");
						filterOption.setFilterName("All");
						arrFilterOptionBOs.add(filterOption);
						for (int i = 0; i < jsonArray.length(); i++) {
							FilterOptionBO filterOptionBO = new FilterOptionBO();
							filterOptionBO.setFilterGroupName("Category");
							filterOptionBO.setFilterId(jsonArray.getJSONObject(
									i).getString("busCatId"));
							filterOptionBO.setFilterName(jsonArray
									.getJSONObject(i).getString("busCatName"));
							arrFilterOptionBOs.add(filterOptionBO);
						}
					} else {
						FilterOptionBO filterOptionBO = new FilterOptionBO();
						filterOptionBO.setFilterGroupName("Category");
						filterOptionBO.setFilterId(jsonObject
								.getString("busCatId"));
						filterOptionBO.setFilterName(jsonObject
								.getString("busCatName"));
						arrFilterOptionBOs.add(filterOptionBO);
					}

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else if (jsonResponse.has("CategoryDetails")) {

			try {
				if (jsonResponse.getJSONObject("CategoryDetails")
						.getString("responseCode").equals("10000")) {

					JSONArray jsonArray = null;
					JSONObject jsonObject = null;
					try {
						jsonArray = jsonResponse
								.getJSONObject("CategoryDetails")
								.getJSONObject("listCatDetails")
								.getJSONArray("CategoryDetails");
					} catch (Exception e) {
						jsonObject = jsonResponse
								.getJSONObject("CategoryDetails")
								.getJSONObject("listCatDetails")
								.getJSONObject("CategoryDetails");
					}
					arrFilterOptionBOs = new ArrayList<>();
					if (jsonArray != null) {
						FilterOptionBO filterOption = new FilterOptionBO();
						filterOption.setFilterGroupName("Category");
						filterOption.setFilterId("");
						filterOption.setFilterName("All");
						arrFilterOptionBOs.add(filterOption);
						for (int i = 0; i < jsonArray.length(); i++) {
							FilterOptionBO filterOptionBO = new FilterOptionBO();
							filterOptionBO.setFilterGroupName("Category");
							filterOptionBO.setFilterId(jsonArray.getJSONObject(
									i).getString("catId"));
							filterOptionBO.setFilterName(jsonArray
									.getJSONObject(i).getString("catName"));
							arrFilterOptionBOs.add(filterOptionBO);
						}
					} else {
						FilterOptionBO filterOptionBO = new FilterOptionBO();
						filterOptionBO.setFilterGroupName("Category");
						filterOptionBO.setFilterId(jsonObject
								.getString("catId"));
						filterOptionBO.setFilterName(jsonObject
								.getString("catName"));
						arrFilterOptionBOs.add(filterOptionBO);
					}

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		return null;
	}

	@Override
	protected void onPostExecute(Void result)
	{
		super.onPostExecute(result);
		if (arrFilterOptionBOs != null) {
			try {
				((FilterAndSortScreen) mContext).setFilterOptionsForChildView(
						arrFilterOptionBOs, groupPosition, "category");
			} catch (Exception e) {
				filterChildView.setFilterOptionsForChildView(
						arrFilterOptionBOs, groupPosition, "category");
			}
		} else {
			Toast.makeText(mContext, "No Records Found", Toast.LENGTH_SHORT)
					.show();
		}

		mDialog.dismiss();
	}

}
