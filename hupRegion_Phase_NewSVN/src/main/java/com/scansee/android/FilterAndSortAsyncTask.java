package com.scansee.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.hubcity.android.businessObjects.FilterArdSortScreenBO;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FilterAndSortAsyncTask extends AsyncTask<Void, Void, Void> {

    String urlParameters;
    Context mContext;
    ProgressDialog pgDialog;
    ArrayList<FilterArdSortScreenBO> arrSortAndFilter;
    OnFilterTaskComplete onFilterTaskComplete;
    boolean alreadyChecked;
    String templateName;

    public FilterAndSortAsyncTask(Context context, String urlParameters,
                                  ProgressDialog pg, OnFilterTaskComplete onFilterTaskComplete,
                                  String templateName) {
        this.mContext = context;
        this.urlParameters = urlParameters;
        this.pgDialog = pg;
        this.onFilterTaskComplete = onFilterTaskComplete;
        // templateName is used to check wheather the respective menu or
        // submenu's template name is rectangular grid template, So that we can
        // check and remove sort by colomn
        this.templateName = templateName;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        String get_sort_filter_list_url = Properties.url_local_server
                + Properties.hubciti_version + "find/getsortfilterlist";


                JSONObject jsonResponse = new ServerConnections().getUrlPostResponse(
                get_sort_filter_list_url, urlParameters, true);

        if (jsonResponse != null && jsonResponse.has("Filter")) {
            try {
                if (jsonResponse.getJSONObject("Filter")
                        .getString("responseCode").equals("10000")) {

                    JSONArray arrayFilter = null;
                    JSONObject jsonObject = null;
                    try {
                        arrayFilter = jsonResponse.getJSONObject("Filter")
                                .getJSONObject("filterList")
                                .getJSONArray("Filter");
                    } catch (Exception e) {
                        jsonObject = jsonResponse.getJSONObject("Filter")
                                .getJSONObject("filterList")
                                .getJSONObject("Filter");
                    }
                    arrSortAndFilter = new ArrayList<>();
                    if (arrayFilter != null) {
                        if (templateName != null
                                && (templateName
                                .equalsIgnoreCase("Rectangular Grid") || templateName
                                .equalsIgnoreCase("4X4 Grid") || templateName.equalsIgnoreCase("Two Image Template") || templateName.equalsIgnoreCase("Four Tile Template")
                                || templateName.equalsIgnoreCase("Two Tile Template") || templateName.equalsIgnoreCase("Square Image Template")
                                )) {
                            for (int i = 0; i < arrayFilter.length(); i++) {
                                String sortBy = arrayFilter.getJSONObject(i)
                                        .getString("fHeader");
                                if (!sortBy.equalsIgnoreCase("Sort Items by")) {
                                    FilterArdSortScreenBO filterArdSortScreenBO = new FilterArdSortScreenBO();
                                    filterArdSortScreenBO
                                            .setFilterName(arrayFilter
                                                    .getJSONObject(i)
                                                    .getString("fHeader"));
                                    filterArdSortScreenBO
                                            .setHeaderName(arrayFilter
                                                    .getJSONObject(i)
                                                    .getString("fHeader"));
                                    filterArdSortScreenBO.setHeader(true);
                                    arrSortAndFilter.add(filterArdSortScreenBO);
                                }
                                Object filterObject = arrayFilter
                                        .getJSONObject(i)
                                        .getJSONObject("filterList")
                                        .get("Filter");

                                if (filterObject instanceof JSONObject) {
                                    if (!sortBy
                                            .equalsIgnoreCase("Sort Items by")) {
                                        FilterArdSortScreenBO filterArdSort = new FilterArdSortScreenBO();
                                        filterArdSort.setHeaderName(arrayFilter
                                                .getJSONObject(i).getString(
                                                        "fHeader"));
                                        filterArdSort
                                                .setFilterName(((JSONObject) filterObject)
                                                        .getString("filterName"));
                                        if (((JSONObject) filterObject)
                                                .getString("filterName")
                                                .equalsIgnoreCase("Distance"))
                                            filterArdSort.setChecked(true);

                                        filterArdSort.setHeader(false);
                                        arrSortAndFilter.add(filterArdSort);
                                    }

                                } else if (filterObject instanceof JSONArray) {
                                    for (int j = 0; j < ((JSONArray) filterObject)
                                            .length(); j++) {
                                        FilterArdSortScreenBO filterArdSort = new FilterArdSortScreenBO();
                                        filterArdSort.setHeader(false);
                                        filterArdSort.setHeaderName(arrayFilter
                                                .getJSONObject(i).getString(
                                                        "fHeader"));
                                        if (((JSONArray) filterObject)
                                                .getJSONObject(j)
                                                .getString("filterName")
                                                .equalsIgnoreCase("Date") && !arrayFilter
                                                .getJSONObject(i).getString(
                                                        "fHeader").equalsIgnoreCase(
                                                        "Filter Items by")) {
                                            filterArdSort.setChecked(true);
                                            alreadyChecked = true;
                                        } else {
                                            if (!alreadyChecked) {
                                                if (((JSONArray) filterObject)
                                                        .getJSONObject(j)
                                                        .getString("filterName")
                                                        .equalsIgnoreCase(
                                                                "Distance") )
                                                    filterArdSort
                                                            .setChecked(true);
                                            }
                                        }
                                        filterArdSort
                                                .setFilterName(((JSONArray) filterObject)
                                                        .getJSONObject(j)
                                                        .getString("filterName"));

                                        arrSortAndFilter.add(filterArdSort);
                                    }
                                }
                            }
                        } else {
                            for (int i = 0; i < arrayFilter.length(); i++) {
                                FilterArdSortScreenBO filterArdSortScreenBO = new FilterArdSortScreenBO();
                                filterArdSortScreenBO.setFilterName(arrayFilter
                                        .getJSONObject(i).getString("fHeader"));
                                filterArdSortScreenBO.setHeaderName(arrayFilter
                                        .getJSONObject(i).getString("fHeader"));
                                filterArdSortScreenBO.setHeader(true);
                                arrSortAndFilter.add(filterArdSortScreenBO);

                                Object filterObject = arrayFilter
                                        .getJSONObject(i)
                                        .getJSONObject("filterList")
                                        .get("Filter");

                                if (filterObject instanceof JSONObject) {

                                    FilterArdSortScreenBO filterArdSort = new FilterArdSortScreenBO();
                                    filterArdSort.setHeaderName(arrayFilter
                                            .getJSONObject(i).getString(
                                                    "fHeader"));
                                    filterArdSort
                                            .setFilterName(((JSONObject) filterObject)
                                                    .getString("filterName"));
                                    if (((JSONObject) filterObject).getString(
                                            "filterName").equalsIgnoreCase(
                                            "Distance"))
                                        filterArdSort.setChecked(true);
                                    else if (((JSONObject) filterObject).getString(
                                            "filterName").equalsIgnoreCase(
                                            "Date"))
                                        filterArdSort.setChecked(true);
                                    filterArdSort.setHeader(false);
                                    arrSortAndFilter.add(filterArdSort);

                                } else if (filterObject instanceof JSONArray) {
                                    for (int j = 0; j < ((JSONArray) filterObject)
                                            .length(); j++) {
                                        FilterArdSortScreenBO filterArdSort = new FilterArdSortScreenBO();
                                        filterArdSort.setHeader(false);
                                        filterArdSort.setHeaderName(arrayFilter
                                                .getJSONObject(i).getString(
                                                        "fHeader"));
                                        if (((JSONArray) filterObject)
                                                .getJSONObject(j)
                                                .getString("filterName")
                                                .equalsIgnoreCase("Date") && !arrayFilter
                                                .getJSONObject(i).getString(
                                                        "fHeader").equalsIgnoreCase(
                                                        "Filter Items by")) {
                                            filterArdSort.setChecked(true);
                                            alreadyChecked = true;
                                        } else {
                                            if (!alreadyChecked) {
                                                if (((JSONArray) filterObject)
                                                        .getJSONObject(j)
                                                        .getString("filterName")
                                                        .equalsIgnoreCase(
                                                                "Distance")|| ((JSONArray) filterObject)
                                                        .getJSONObject(j)
                                                        .getString("filterName")
                                                        .equalsIgnoreCase(
                                                                "mileage"))
                                                    filterArdSort
                                                            .setChecked(true);
                                            }
                                        }
                                        filterArdSort
                                                .setFilterName(((JSONArray) filterObject)
                                                        .getJSONObject(j)
                                                        .getString("filterName"));

                                        arrSortAndFilter.add(filterArdSort);
                                    }
                                }
                            }

                        }
                    } else {

                        FilterArdSortScreenBO filterArdSortScreenBO = new FilterArdSortScreenBO();
                        filterArdSortScreenBO.setFilterName(jsonObject
                                .getString("fHeader"));
                        filterArdSortScreenBO.setHeaderName(jsonObject
                                .getString("fHeader"));
                        filterArdSortScreenBO.setHeader(true);
                        arrSortAndFilter.add(filterArdSortScreenBO);
                        Object filterObject = jsonObject.getJSONObject(
                                "filterList").get("Filter");

                        if (filterObject instanceof JSONObject) {
                            FilterArdSortScreenBO filterArdSort = new FilterArdSortScreenBO();
                            filterArdSort.setHeaderName(jsonObject
                                    .getString("fHeader"));
                            filterArdSort
                                    .setFilterName(((JSONObject) filterObject)
                                            .getString("filterName"));

                            if (((JSONObject) filterObject).getString(
                                    "filterName").equalsIgnoreCase("Date")) {
                                filterArdSort.setChecked(true);
                                alreadyChecked = true;
                            } else {
                                if (!alreadyChecked) {
                                    if (((JSONObject) filterObject).getString(
                                            "filterName").equalsIgnoreCase(
                                            "Distance"))
                                        filterArdSort.setChecked(true);
                                }
                            }

                            filterArdSort.setHeader(false);
                            arrSortAndFilter.add(filterArdSort);
                        } else if (filterObject instanceof JSONArray) {
                            for (int j = 0; j < ((JSONArray) filterObject)
                                    .length(); j++) {
                                FilterArdSortScreenBO filterArdSort = new FilterArdSortScreenBO();
                                filterArdSort.setHeader(false);
                                filterArdSort.setHeaderName(jsonObject
                                        .getString("fHeader"));
                                if (((JSONArray) filterObject).getJSONObject(j)
                                        .getString("filterName")
                                        .equalsIgnoreCase("Distance"))
                                    filterArdSort.setChecked(true);
                                filterArdSort
                                        .setFilterName(((JSONArray) filterObject)
                                                .getJSONObject(j).getString(
                                                        "filterName"));

                                arrSortAndFilter.add(filterArdSort);
                            }
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        try {
            if (pgDialog != null && pgDialog.isShowing())
                pgDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (arrSortAndFilter != null) {
            try {
                ((FilterAndSortScreen) mContext)
                        .setDynamicFilterAndSortView(arrSortAndFilter);
            } catch (Exception e) {
                onFilterTaskComplete.setDynamicFilterView(arrSortAndFilter);
            }
        }
    }

}
