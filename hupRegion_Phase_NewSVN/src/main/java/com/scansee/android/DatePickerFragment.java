package com.scansee.android;

import java.util.Calendar;
import java.util.Locale;

import com.hubcity.android.screens.EventsListDisplay;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar c = Calendar.getInstance();
		c.set(year, monthOfYear, dayOfMonth);
		String month_name = c.getDisplayName(Calendar.MONTH, Calendar.SHORT,
				Locale.US);
		String date = month_name + " " + String.valueOf(dayOfMonth) + ", "
				+ String.valueOf(year);
		try {
			((FilterAndSortScreen) getActivity()).eventDate = date;
		} catch (Exception e) {
		}
	}

}
