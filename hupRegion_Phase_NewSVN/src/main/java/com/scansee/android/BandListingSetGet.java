package com.scansee.android;

/**
 * Created by supriya.m on 4/19/2016.
 */
public class BandListingSetGet {

    public String getBandName() {
        return bandName;
    }

    public void setBandName(String bandName) {
        this.bandName = bandName;
    }

    public String getBandCategoryName() {
        return bandCategoryName;
    }

    public void setBandCategoryName(String bandCategoryName) {
        this.bandCategoryName = bandCategoryName;
    }

    public String getBandImageUrl() {
        return bandImageUrl;
    }

    public void setBandImageUrl(String bandImageUrl) {
        this.bandImageUrl = bandImageUrl;
    }

    private String bandName;
    private String bandCategoryName;
    private String bandImageUrl;

    public boolean isGroup() {
        return isGroup;
    }

    public void setIsGroup(boolean isGroup) {
        this.isGroup = isGroup;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    private String groupName;
    private boolean isGroup;

    public String getBandId() {
        return bandId;
    }

    public void setBandId(String bandId) {
        this.bandId = bandId;
    }

    public String getBandCatId() {
        return bandCatId;
    }

    public void setBandCatId(String bandCatId) {
        this.bandCatId = bandCatId;
    }

    private String bandId;
    private String bandCatId;
}
