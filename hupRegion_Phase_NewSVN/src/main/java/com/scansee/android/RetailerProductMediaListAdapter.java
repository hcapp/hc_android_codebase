package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class RetailerProductMediaListAdapter extends BaseAdapter {
	Activity activity;
	private ArrayList<HashMap<String, String>> productmediaList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	public RetailerProductMediaListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> productmediaList) {
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.productmediaList = productmediaList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return productmediaList.size();
	}

	@Override
	public Object getItem(int id) {
		return productmediaList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		if (convertView == null) {

			view = inflater.inflate(
					R.layout.listitem_retailer_productdetail_media, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) view
					.findViewById(R.id.retailer_productmedia_text);

			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();

		}
		viewHolder.name.setText(productmediaList.get(position).get("filename"));

		return view;
	}

	public static class ViewHolder {
		protected TextView name;

	}
}
