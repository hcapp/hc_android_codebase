package com.scansee.android;

import java.util.ArrayList;

import com.hubcity.android.businessObjects.FilterArdSortScreenBO;

public class SubMenuFilterAndSortObject {

    private String mItemId = "";
    private String dept = "";
    private String type = "";
    private String sortBy = "";
    private int level;
    private int sortSelectedPostion = -1;
    private String savedDepartmentId = "";
    private String savedTypeId = "";
    private ArrayList<String> arrCityIds = new ArrayList<>();

    private ArrayList<FilterArdSortScreenBO> arrSortAndFilter = new ArrayList<>();

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getmItemId() {
        return mItemId;
    }

    public void setmItemId(String mItemId) {
        this.mItemId = mItemId;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public ArrayList<FilterArdSortScreenBO> getArrSortAndFilter() {
        return arrSortAndFilter;
    }

    public void setArrSortAndFilter(
            ArrayList<FilterArdSortScreenBO> arrSortAndFilter) {
        this.arrSortAndFilter = arrSortAndFilter;
    }

    public int getSortSelectedPostion() {
        return sortSelectedPostion;
    }

    public void setSortSelectedPostion(int sortSelectedPostion) {
        this.sortSelectedPostion = sortSelectedPostion;
    }

    public String getSavedDepartmentId() {
        return savedDepartmentId;
    }

    public void setSavedDepartmentId(String savedDepartmentId) {
        this.savedDepartmentId = savedDepartmentId;
    }

    public String getSavedTypeId() {
        return savedTypeId;
    }

    public void setSavedTypeId(String savedTypeId) {
        this.savedTypeId = savedTypeId;
    }

    public ArrayList<String> getArrCityIds() {
        return arrCityIds;
    }

    public void setArrCityIds(ArrayList<String> arrCityIds) {
        this.arrCityIds = arrCityIds;
    }
}
