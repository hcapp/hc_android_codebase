package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;

public class RetailerProductMediaActivity extends Activity {
	private ArrayList<HashMap<String, String>> productmediaList = null;
	public static final String TAG_PRODUCTMEDIA_RESULT_SET = "ProductDetails";
	public static final String TAG_PRODUCTMEDIA_MEDIATYPE = "mediaType";
	public static final String TAG_PRODUCTMEDIA_MENU = "ProductDetail";
	public static final String TAG_PRODUCTMEDIA_PRODUCTMEDIA_ID = "productMediaID";
	public static final String TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH = "productMediaPath";
	public static final String TAG_PRODUCTMEDIA_PRODUCTMEDIANAME = "productMediaName";
	SharedPreferences settings = null;
	String userId, productId, mediaType;
	protected ListView productmediaListView;
	RetailerProductMediaListAdapter productmediaListAdapter;
	Activity activity;
	Context context = this;

	@Override
	protected void onResume() {
		super.onResume();
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		List<ApplicationInfo> packages;
		PackageManager pm;
		pm = getPackageManager();
		// get a list of installed apps.
		packages = pm.getInstalledApplications(0);

		ActivityManager mActivityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);

		for (ApplicationInfo packageInfo : packages) {
			if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1
					|| packageInfo.packageName.contains("com.adobe.reader")) {
				continue;
			}

			mActivityManager.killBackgroundProcesses(packageInfo.packageName);
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.retailer_productdetail_media_listview);
		activity = RetailerProductMediaActivity.this;
		SharedPreferences settings = getSharedPreferences(
				CommonConstants.PREFERANCE_FILE, 0);

		// get extras
		userId = settings.getString(CommonConstants.USER_ID, "0");

		productId = getIntent().getExtras().getString(
				CommonConstants.TAG_CURRENTSALES_PRODUCTID);
		mediaType = getIntent().getExtras().getString("mediaType");
		productmediaListView = (ListView) findViewById(R.id.retailer_productmedialist);

		productmediaListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (("audio".equalsIgnoreCase(mediaType))
						|| ("video".equalsIgnoreCase(mediaType))) {
					Intent navIntent = new Intent(
							RetailerProductMediaActivity.this,
							ScanSeeVideoActivity.class);
					navIntent.putExtra(
							TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
							productmediaList.get(position).get(
									TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH));
					startActivity(navIntent);
				} else if ("file".equalsIgnoreCase(mediaType)) {
//					Intent navIntent = new Intent(
//							RetailerProductMediaActivity.this,
//							ScanseeFileActivity.class);
					Intent navIntent = new Intent(
							RetailerProductMediaActivity.this,
							ScanSeePdfViewerActivity.class);
					navIntent.putExtra(
							TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
							productmediaList.get(position).get(
									TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH));
					navIntent.putExtra(
							TAG_PRODUCTMEDIA_PRODUCTMEDIANAME,
							productmediaList.get(position).get(
									TAG_PRODUCTMEDIA_PRODUCTMEDIANAME));
					startActivity(navIntent);
				}

			}
		});

		new GetMedia().execute();

	}

	@Override
	public void onDestroy() {
		if (productmediaListAdapter != null) {
			productmediaListAdapter.customImageLoader.stopThread();
			productmediaListView.setAdapter(null);
		}
		super.onDestroy();

	}

	private class GetMedia extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		JSONArray jsonArray = null;
		JSONObject responseMenuObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(RetailerProductMediaActivity.this,
					"", Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {

			productmediaList = new ArrayList<>();
			String result = "false";
			try {

				if (jsonObject != null) {
					jsonArray = new JSONArray();
					try {
						jsonObject = jsonObject.getJSONObject(
								TAG_PRODUCTMEDIA_RESULT_SET).getJSONObject(
								TAG_PRODUCTMEDIA_MENU);
						jsonArray.put(jsonObject);
					} catch (Exception e) {
						e.printStackTrace();
						jsonArray = jsonObject.getJSONObject(
								TAG_PRODUCTMEDIA_RESULT_SET).getJSONArray(
								TAG_PRODUCTMEDIA_MENU);
					}

					if (jsonArray != null) {

						for (int arrayCount = 0; arrayCount < jsonArray
								.length(); arrayCount++) {
							HashMap<String, String> productmediaData = new HashMap<>();
							responseMenuObject = jsonArray
									.getJSONObject(arrayCount);
							productmediaData
									.put(TAG_PRODUCTMEDIA_PRODUCTMEDIA_ID,
											responseMenuObject
													.getString(TAG_PRODUCTMEDIA_PRODUCTMEDIA_ID));

							productmediaData
									.put(TAG_PRODUCTMEDIA_PRODUCTMEDIANAME,
											responseMenuObject
													.getString(TAG_PRODUCTMEDIA_PRODUCTMEDIANAME));
							productmediaData
									.put(TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
											responseMenuObject
													.getString(TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH));
							if (responseMenuObject
									.getString(TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH) != null) {
								String[] filenames = responseMenuObject
										.getString(
												TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH)
										.split("/");
								String filename = filenames[filenames.length - 1];
								productmediaData.put("filename", filename);
							}

							productmediaList.add(productmediaData);
						}

						result = "true";
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();
			if ("true".equals(result)) {

				productmediaListAdapter = new RetailerProductMediaListAdapter(
						activity, productmediaList);
				productmediaListView.setAdapter(productmediaListAdapter);
			} else {
				Toast.makeText(getBaseContext(), "Error Calling WebService",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

}