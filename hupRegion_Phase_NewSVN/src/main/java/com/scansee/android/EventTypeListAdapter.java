package com.scansee.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.scansee.newsfirst.CommonMethods;

import java.util.ArrayList;

/**
 * Created by supriya.m on 4/20/2016.
 */
public class EventTypeListAdapter extends BaseAdapter {
    ArrayList<BandSearchListSetGet> arrEvents;
    LayoutInflater inflater;
    Context mContext;

    public EventTypeListAdapter(Context context, ArrayList<BandSearchListSetGet> arrEvents) {
        this.arrEvents = arrEvents;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return arrEvents.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (position == arrEvents.size() - 1) {
            if (!((BandsLandingPageActivity) mContext).isAlreadyLoading) {
                if (((BandsLandingPageActivity) mContext).nextPage) {
                    ((BandsLandingPageActivity) mContext).loadMoreData();
                }
            }
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.band_listing_item, null);
            viewHolder = new ViewHolder();
            viewHolder.bandName = (TextView) convertView.findViewById(R.id.band_listing_item_band_name);
            viewHolder.bandCategoryName = (TextView) convertView.findViewById(R.id.band_listing_item_band_category_name);
            viewHolder.bandImage = (ImageView) convertView.findViewById(R.id.band_listing_item_image);
            viewHolder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String bandName = arrEvents.get(position).getSearchedItemName();
        String catName = arrEvents.get(position).getSearchedCatName();
        String ImgPath = arrEvents.get(position).getSearchedImgPath();
        if (ImgPath != null && !ImgPath.equalsIgnoreCase("")) {
            ImgPath = ImgPath.replaceAll(" ", "%20");
            new CommonMethods().loadImage(mContext,viewHolder.progressBar, ImgPath, viewHolder.bandImage);
        }
        if (bandName != null && !bandName.equalsIgnoreCase("")) {
            viewHolder.bandName.setText(bandName);
        }
        if (catName != null && !catName.equalsIgnoreCase("")) {
            viewHolder.bandCategoryName.setText(catName);
        }
        return convertView;
    }

    public class ViewHolder {
        TextView bandName, bandCategoryName;
        ImageView bandImage;
        ProgressBar progressBar;
    }
}
