package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.UrlRequestParams;


import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;

public class AllCouponsListAdapter extends BaseAdapter implements
		OnClickListener {

	private boolean isLocation = false;
	private CouponsActivty activity;
	private ArrayList<HashMap<String, String>> allcouponsList;
	private static LayoutInflater inflater = null;
	private CustomImageLoader customImageLoader;
	private String itemName = null;
	private HashMap<String, String> couponData = null;

	public AllCouponsListAdapter(CouponsActivty activity,
			ArrayList<HashMap<String, String>> allcouponsList) {
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.allcouponsList = allcouponsList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(),
				"couponImages");

	}

	public AllCouponsListAdapter(CouponsActivty activity,
			ArrayList<HashMap<String, String>> allcouponsList,
			boolean isLocationBased) {
		isLocation = isLocationBased;
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.allcouponsList = allcouponsList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(),
				"couponImages");

	}

	@Override
	public int getCount() {
		return allcouponsList.size();
	}

	@Override
	public Object getItem(int id) {
		return allcouponsList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;

		try {
			couponData = allcouponsList.get(position);
			itemName = couponData.get("itemName");

			viewHolder = new ViewHolder();
			if (position == allcouponsList.size() - 1) {
				/*if (!((CouponsActivty) activity).isAlreadyLoading) {
					if (((CouponsActivty) activity).nextPage) {
						// ((EventsListDisplay) activity).enableBottomButton =
						// false;
						((CouponsActivty) activity).isAlreadyLoading = true;
						((CouponsActivty) activity).viewMore();
					}
				}*/
			}
			if ("category".equalsIgnoreCase(itemName)) {
				view = inflater.inflate(R.layout.listitem_hotdeals_categoryname,
                        parent,false);
				if (isLocation) {
					view.setBackgroundColor(Color.DKGRAY);
				} else {
					view.setBackgroundColor(Color.GRAY);
				}
				viewHolder.categoryName = (TextView) view
						.findViewById(R.id.hotdeals_categoryname);
			} else if ("coupons".equalsIgnoreCase(itemName)) {
				view = inflater.inflate(R.layout.listitem_coupons, parent,false);
				viewHolder.couponimage = (ImageView) view
						.findViewById(R.id.coupon_image);

				viewHolder.couponname = (TextView) view
						.findViewById(R.id.coupon_product_name);
				viewHolder.couponimagePath = (ImageView) view
						.findViewById(R.id.coupon_imagepath);
				viewHolder.coupondiscountprice = (TextView) view
						.findViewById(R.id.coupon_product_couponDiscountAmount);
				viewHolder.couponexpiretext = (TextView) view
						.findViewById(R.id.coupon_product_expires);
				viewHolder.couponexpiredate = (TextView) view
						.findViewById(R.id.coupon_product_couponExpireDate);
				viewHolder.ribbonimage = (ImageView) view
						.findViewById(R.id.ribbon_imagepath);

			}
			if ("location".equalsIgnoreCase(itemName)) {
				view = inflater.inflate(R.layout.listitem_hotdeals_categoryname,
                        parent,false);
				viewHolder.locationName = (TextView) view
						.findViewById(R.id.hotdeals_categoryname);
				view.setBackgroundColor(Color.GRAY);
			}
			if ("location".equalsIgnoreCase(itemName)) {
				viewHolder.locationName.setText(couponData
						.get(CommonConstants.ALLCOUPONSLOCATION));
			} else if ("category".equalsIgnoreCase(itemName)) {
				viewHolder.categoryName.setText(couponData
						.get(CommonConstants.ALLCOUPONSCOUPONCATNAME));
			} else if ("coupons".equalsIgnoreCase(itemName)) {
				viewHolder.couponname.setText(allcouponsList.get(position).get(
						CommonConstants.ALLCOUPONSCOUPONNAME));
				viewHolder.couponimage.setTag(position);

				String redeemFlag = couponData.get("redeemFlag");
				String claimFlag = couponData.get("claimFlag");

				if ("0".equals(redeemFlag) && "0".equals(claimFlag)) {
					viewHolder.couponimage
							.setBackgroundResource(R.drawable.wl_add_cpn_live);
					viewHolder.couponimage.setOnClickListener(this);
				} else {
					viewHolder.couponimage
							.setBackgroundResource(R.drawable.wl_add_cpn_off);
					viewHolder.couponimage.setOnClickListener(this);
				}

				if ("0".equals(allcouponsList.get(position).get("newFlag"))) {
					viewHolder.ribbonimage.setVisibility(View.INVISIBLE);
				}

				if (!("N/A".equalsIgnoreCase(allcouponsList.get(position).get(
						CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT)))) {
					viewHolder.coupondiscountprice.setText(allcouponsList.get(
							position).get(
							CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT));
					viewHolder.couponexpiretext.setText("off Expires");
					viewHolder.couponexpiredate.setText(allcouponsList
							.get(position).get(
									CommonConstants.ALLCOUPONSEXPIRATIONDATE));
				}
				viewHolder.couponimagePath.setTag(allcouponsList.get(position).get(
						CommonConstants.ALLCOUPONSIMAGEPATH));
				customImageLoader.displayImage(
						allcouponsList.get(position).get(
								CommonConstants.ALLCOUPONSIMAGEPATH), activity,
						viewHolder.couponimagePath);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// else if ("viewMore".equalsIgnoreCase(itemName)) {
		// view = inflater.inflate(R.layout.listitem_get_retailers_viewmore,
		// null);
		//
		// }

		return view;

	}

	public static class ViewHolder {
		TextView locationName;
		TextView categoryName;
		protected ImageView couponDollar;
		ImageView couponimage;
		ImageView ribbonimage;
		TextView couponexpiredate;
		TextView couponexpiretext;
		TextView coupondiscountprice;
		ImageView couponimagePath;
		TextView couponname;

	}

	@Override
	public void onClick(View view) {

		switch (view.getId()) {
		case R.id.coupon_delete:
			couponDelete(view);
			break;

		case R.id.coupon_image:
			couponImage(view);
			break;

		default:
			break;
		}

	}

	private void couponImage(View view) {
		/*if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
			activity.isCliped = false;

			int currPst = -1;
			if (view != null) {
				currPst = (Integer) view.getTag();
			}
			if (currPst != -1 && activity.allcouponsList != null) {
				HashMap<String, String> couponInfo;
				couponInfo = activity.allcouponsList.get(currPst);
				String redeemFlag = couponInfo.get("redeemFlag");
				String claimFlag = couponInfo.get("claimFlag");
				if ("0".equals(redeemFlag) && "0".equals(claimFlag)) {
					activity.addCoupon(
							activity.allcouponsList.get(currPst).get(
									CommonConstants.ALLCOUPONSCOUPONID), view);
				} else if ("0".equals(redeemFlag) && "1".equals(claimFlag)) {
					activity.removeCoupon(
							activity.allcouponsList.get(currPst).get(
									CommonConstants.ALLCOUPONSCOUPONID), view);
				}
			}
		}else {
			Constants.SignUpAlert(activity);
		}*/

	}

	private void couponDelete(View view) {
		/*view.setVisibility(View.GONE);
		int currPst;
		currPst = (Integer) view.getTag();
		if (currPst != -1 && activity.allcouponsList != null) {
			activity.deleteItem(activity.allcouponsList.get(currPst).get(
					CommonConstants.ALLCOUPONSCOUPONID));
		}*/

	}

}
