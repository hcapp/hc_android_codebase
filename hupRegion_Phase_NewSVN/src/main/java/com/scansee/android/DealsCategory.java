package com.scansee.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.FundraiserActivity;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This Screen contains Deals UI changes
 *
 * @author rekha_p
 */
public class DealsCategory extends CustomTitleBar {

    // List Holding deals data
    ArrayList<HashMap<String, String>> dealsDetailsList = new ArrayList<>();
    ArrayList<HashMap<String, String>> dealsStateDetailsList = new ArrayList<>();

    JSONObject jsonBBObject = null;

    boolean isFirst;
    boolean isSubMenu;

    DealsCategoryAsyncTask dealsAsyncTask;
    ProgressDialog progDialog;
    LocationManager locationManager;

    private static String returnValue = "sucessfull";

    // For Bottom Buttons
    LinearLayout linearLayout = null;
    BottomButtons bb = null;
    Activity activity;
    boolean hasBottomBtns = false;
    boolean enableBottomButton = true;

    boolean isFirstLoad = true;

    // Colors for the List
    String mColor = "";
    String mFontColor = "";
    String sectionColor = "";
    String mBackGrdColor = "";

    String mItemId = "";
    String mBottomId = "";
    String gpsLatitude = null, gpsLongitude = null, zipcode = null;

    // Lists holding deals data
    ListView dealsList;
    DealsCategoryAdapter dealsCatAdapter;
    private CustomNavigation customNaviagation;
    private int itemCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deals_cat);

        try {
            CommonConstants.hamburgerIsFirst = true;

            isFirstLoad = true;

            isFirst = true;

            activity = DealsCategory.this;
            try {
                // Initiating Bottom button class
                bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
                // Add screen name when needed
                bb.setActivityInfo(activity, "");
            } catch (Exception e) {
                e.printStackTrace();
            }

            linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
            linearLayout.setVisibility(View.INVISIBLE);

            title.setText("Deals");

            if (getIntent().hasExtra("isSubMenu")) {
                isSubMenu = getIntent().getExtras().getBoolean("isSubMenu");
            }

            if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }

            if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
                mBottomId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }

            dealsList = (ListView) findViewById(R.id.deals_menu);
            dealsList.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    if (dealsDetailsList != null && !dealsDetailsList.isEmpty()) {
                        selectDealsOption(position);
                    }
                }

            });

            // Back button click action
            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTask();
                    finish();
                }
            });

            loadData();

            //user for hamburger in modules
            drawerIcon.setVisibility(View.VISIBLE);
            leftTitleImage.setVisibility(View.GONE);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * On item click action
     */
    private void selectDealsOption(int position)
    {
        String selectedItem = dealsDetailsList.get(position).get("Name");
        String isEnabled = dealsDetailsList.get(position).get("flag");

        Intent intent = new Intent();
        if ("Local Specials".equalsIgnoreCase(selectedItem)) {
            intent.setClass(this, SpecialsActivity.class);

            if (jsonBBObject != null) {
                intent.putExtra("jsonBB", jsonBBObject.toString());
            }

        } else if ("Deals".equalsIgnoreCase(selectedItem)) {
            intent.setClass(this, HotDealsActivity.class);

        } else if ("Coupons".equalsIgnoreCase(selectedItem)) {
            intent.setClass(this, CouponsActivty.class);

        } else if ("Fundraisers".equalsIgnoreCase(selectedItem)) {
            intent.setClass(this, FundraiserActivity.class);
            intent.putExtra("isDeals", true);

        } else if ("Claimed".equalsIgnoreCase(selectedItem)
                || "Expired".equalsIgnoreCase(selectedItem)
                || "Used".equalsIgnoreCase(selectedItem)) {

            if ("false".equalsIgnoreCase(isEnabled)) {
                return;
            }

            intent.setClass(this, DealsGallery.class);
            intent.putExtra("type", selectedItem);
            intent.putExtra("isSubMenu", isSubMenu);

            if (mItemId != null && !"".equals(mItemId)) {
                intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            } else if (mBottomId != null && !"".equals(mBottomId)) {
                intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        mBottomId);
            }

        } else {
            return;
        }

        if (mItemId != null && !"".equals(mItemId)) {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);

        } else if (mBottomId != null && !"".equals(mBottomId)) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mBottomId);

        }

        startActivity(intent);

    }

    /*
     * Call to AsyncTask
     */
    private void callDealsCategory() {
        if (isFirst) {
            dealsAsyncTask = new DealsCategoryAsyncTask();
            dealsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            isFirst = false;
            return;
        }

        if (dealsAsyncTask != null) {
            if (!dealsAsyncTask.isCancelled()) {
                dealsAsyncTask.cancel(true);

            }

            dealsAsyncTask = null;

            dealsAsyncTask = new DealsCategoryAsyncTask();
            dealsAsyncTask.execute();
        }
    }

    /*
     * Cancel AsyncTask on navigating to different screen
     */
    private void cancelAsyncTask() {
        if (dealsAsyncTask != null && !dealsAsyncTask.isCancelled()) {
            dealsAsyncTask.cancel(true);
        }

        dealsAsyncTask = null;
    }

    /**
     * Background process to get the Deals categories
     *
     * @author rekha_p
     */
    public class DealsCategoryAsyncTask extends AsyncTask<String, Void, String> {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        String responseText = "";
        String responseCode = "";

        @Override
        protected void onPreExecute() {
            progDialog = new ProgressDialog(DealsCategory.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();

        }

        @Override
        protected String doInBackground(String... arg0) {

            try {
                String deals_list = Properties.url_local_server
                        + Properties.hubciti_version + "hotdeals/dealdisplay";

                String urlParameters = mUrlRequestParams.createDealsParameter(
                        isSubMenu, gpsLatitude, gpsLongitude, zipcode);
                JSONObject jsonResponse = mServerConnections
                        .getUrlPostResponse(deals_list,
                                urlParameters, true);

                // If response is Success
                if (jsonResponse != null) {

                    JSONObject responseJson;

                    // If the response has Deals data
                    if (jsonResponse.has("Data")) {

                        responseJson = jsonResponse.getJSONObject("Data");

                        if (responseJson.has("responseCode")) {
                            responseCode = responseJson
                                    .getString("responseCode");
                        }

                        if (responseJson.has("responseText")) {
                            responseText = responseJson
                                    .getString("responseText");
                        }

                        if (responseJson.has("mColor")) {
                            mColor = responseJson.getString("mColor");
                        }

                        if (responseJson.has("mFontColor")) {
                            mFontColor = responseJson.getString("mFontColor");
                        }

                        if (responseJson.has("sectionColor")) {
                            sectionColor = responseJson
                                    .getString("sectionColor");
                        }

                        if (responseJson.has("mBackGrdColor")) {
                            mBackGrdColor = responseJson
                                    .getString("mBackGrdColor");
                        }

                        // If there are bottom buttons
                        if (responseJson.has("bottomBtn")
                                && "1".equals(responseJson
                                .getString("bottomBtn"))) {

                            hasBottomBtns = true;
                            jsonBBObject = null;

                            jsonBBObject = responseJson
                                    .getJSONObject("arBottomBtnList");

                            if (jsonBBObject.has("BottomButton")) {

                                try {
                                    ArrayList<BottomButtonBO> bottomButtonList = bb
                                            .parseForBottomButton(jsonBBObject);
                                    BottomButtonListSingleton
                                            .clearBottomButtonListSingleton();
                                    BottomButtonListSingleton
                                            .getListBottomButton(bottomButtonList);

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                            }

                        } else {
                            hasBottomBtns = false;
                        }

                        // If there are deals list
                        if (responseJson.has("dealList")) {
                            JSONObject dealsJsonObject = responseJson
                                    .getJSONObject("dealList");
                            JSONArray dealsListJson = new JSONArray();

                            try {
                                dealsListJson.put(dealsJsonObject
                                        .getJSONObject("Deal"));
                            } catch (Exception e) {
                                e.printStackTrace();
                                dealsListJson = dealsJsonObject
                                        .getJSONArray("Deal");
                            }
                            dealsDetailsList = new ArrayList<>();
                            dealsStateDetailsList = new ArrayList<>();

                            // If there are deals add section for Deals header
                            if (dealsListJson.length() > 0) {
                                HashMap<String, String> section = new HashMap<>();
                                section.put("isSection", "Deals");
                                dealsDetailsList.add(section);
                            }
                            itemCount = dealsListJson
                                    .length();
                            for (int arrayCount = 0; arrayCount < dealsListJson
                                    .length(); arrayCount++) {
                                HashMap<String, String> dealsData = new HashMap<>();

                                if (dealsListJson.getJSONObject(arrayCount)
                                        .has("Name")) {
                                    dealsData.put("Name", dealsListJson
                                            .getJSONObject(arrayCount)
                                            .getString("Name"));
                                }

                                if (dealsListJson.getJSONObject(arrayCount)
                                        .has("imagePath")) {
                                    dealsData.put("imagePath", dealsListJson
                                            .getJSONObject(arrayCount)
                                            .getString("imagePath"));
                                }

                                if (dealsListJson.getJSONObject(arrayCount)
                                        .has("flag")) {
                                    dealsData.put("flag", dealsListJson
                                            .getJSONObject(arrayCount)
                                            .getString("flag"));
                                }

                                dealsDetailsList.add(dealsData);
                            }
                        }


                        // If there are deals State List
                        if (responseJson.has("dealStateList")) {
                            JSONObject dealsStateJsonObject = responseJson
                                    .getJSONObject("dealStateList");
                            JSONArray dealsStateListJson = new JSONArray();

                            try {
                                dealsStateListJson.put(dealsStateJsonObject
                                        .getJSONObject("Deal"));
                            } catch (Exception e) {
                                e.printStackTrace();
                                dealsStateListJson = dealsStateJsonObject
                                        .getJSONArray("Deal");
                            }

                            // If there are at least one deals state item then a
                            // section should be added

                            if (!dealsDetailsList.isEmpty()
                                    && dealsStateListJson.length() > 0) {
                                HashMap<String, String> section = new HashMap<>();
                                section.put("isSection", "1");
                                dealsDetailsList.add(section);
                            }

                            for (int arrayCount = 0; arrayCount < dealsStateListJson
                                    .length(); arrayCount++) {
                                HashMap<String, String> dealsStateData = new HashMap<>();

                                if (dealsStateListJson
                                        .getJSONObject(arrayCount).has("Name")) {
                                    dealsStateData.put(
                                            "Name",
                                            dealsStateListJson.getJSONObject(
                                                    arrayCount).getString(
                                                    "Name"));
                                }

                                if (dealsStateListJson
                                        .getJSONObject(arrayCount).has(
                                                "imagePath")) {
                                    dealsStateData.put(
                                            "imagePath",
                                            dealsStateListJson.getJSONObject(
                                                    arrayCount).getString(
                                                    "imagePath"));
                                }

                                if (dealsStateListJson
                                        .getJSONObject(arrayCount).has("flag")) {
                                    dealsStateData.put(
                                            "flag",
                                            dealsStateListJson.getJSONObject(
                                                    arrayCount).getString(
                                                    "flag"));
                                }
                                dealsDetailsList.add(dealsStateData);
                            }

                        }

                        returnValue = "sucessfull";

                    } else {
                        // If the response failed
                        if (jsonResponse.has("response")) {
                            responseJson = jsonResponse
                                    .getJSONObject("response");
                            if (responseJson.has("responseCode")) {
                                responseCode = responseJson
                                        .getString("responseCode");
                            }

                            if (responseJson.has("responseText")) {
                                responseText = responseJson
                                        .getString("responseText");
                            }
                        }
                    }
                } else {
                    returnValue = "UNSUCCESS";
                    responseText = "Error";
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                returnValue = "UNSUCCESS";
                return returnValue;
            }
            return returnValue;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if ("sucessfull".equals(returnValue)) {

                    isFirstLoad = false;

                    if (!dealsDetailsList.isEmpty()) {

                        if (dealsDetailsList.size() > 1) {
                            if (itemCount == 1) {
                                String selectedItem = dealsDetailsList.get(1).get("Name");
                                Intent intent = new Intent();
                                if ("Local Specials".equalsIgnoreCase(selectedItem)) {
                                    intent.setClass(DealsCategory.this, SpecialsActivity.class);
                                    if (jsonBBObject != null) {
                                        intent.putExtra("jsonBB", jsonBBObject.toString());
                                    }
                                } else if ("Coupons".equalsIgnoreCase(selectedItem)) {
                                    intent.setClass(DealsCategory.this, CouponsActivty.class);
                                }
                                if (mItemId != null && !"".equals(mItemId)) {
                                    intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);

                                } else if (mBottomId != null && !"".equals(mBottomId)) {
                                    intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mBottomId);
                                }
                                startActivity(intent);
                                finish();
                            } else {
                                dealsCatAdapter = new DealsCategoryAdapter(
                                        DealsCategory.this, dealsDetailsList,
                                        sectionColor);
                                dealsList.setAdapter(dealsCatAdapter);
                            }
                        } else {
                            selectDealsOption(0);
                            finish();
                        }
                    }

                } else {
                    // Display Alert if there are no deals data
                    progDialog.dismiss();

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            DealsCategory.this);

                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setMessage(responseText);
                    alertDialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {

                                    dialog.dismiss();
                                    cancelAsyncTask();
                                    finish();
                                }
                            });
                    alertDialogBuilder.show();

                }

                if (hasBottomBtns && enableBottomButton) {

                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) dealsList
                            .getLayoutParams();
                    mlp.setMargins(0, 0, 0, 2);

                    bb.createbottomButtontTab(linearLayout, false);

                    enableBottomButton = false;
                }

                if (mBackGrdColor != null && !"".equals(mBackGrdColor)
                        && !"N/A".equalsIgnoreCase(mBackGrdColor)) {
                    linearLayout
                            .setBackgroundColor(Color.parseColor(mBackGrdColor));
                } else {
                    linearLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }

                linearLayout.setVisibility(View.VISIBLE);

                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Adapter to display deals list
    class DealsCategoryAdapter extends BaseAdapter {
        Activity mContext;
        ArrayList<HashMap<String, String>> mlist;
        CustomImageLoader customImageLoader;
        String sColor = "#FFFFFF";

        DealsCategoryAdapter(Activity context,
                             ArrayList<HashMap<String, String>> list, String sectionColor) {
            mContext = context;
            mlist = list;
            customImageLoader = new CustomImageLoader(context, false);
            sColor = sectionColor;
        }

        class ViewHolder {
            ImageView icon;
            TextView text;
            LinearLayout menuListTemplateParent;
            protected LinearLayout textViewLayout;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder mViewHolder;

            String imagePath = mlist.get(position).get("imagePath");
            String listText = mlist.get(position).get("Name");
            String isSection = mlist.get(position).get("isSection");

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.template_list_menu_row,
                    parent, false);

            mViewHolder = new ViewHolder();

            // If the list item is a section
            if ("1".equals(isSection) || "Deals".equalsIgnoreCase(isSection)) {
                convertView = inflater
                        .inflate(R.layout.list_item_section, parent, false);

                convertView.setOnClickListener(null);
                convertView.setOnLongClickListener(null);
                convertView.setLongClickable(false);

                final TextView sectionView = (TextView) convertView
                        .findViewById(R.id.list_item_section_text);

                if (sColor != null && !"".equals(sColor)) {
                    sectionView.setBackgroundColor(Color.parseColor(sColor));

                } else {
                    sectionView.setBackgroundColor(Color.parseColor("#000000"));
                }

                if ("1".equals(isSection)) {

                    sectionView.setText("");
                } else if ("Deals".equalsIgnoreCase(isSection)) {
                    sectionView.setText("Deals");
                    sectionView.setPadding(10, 10, 10, 10);
                    sectionView.setTextColor(Color.parseColor(mFontColor));
                }

            } else if (listText != null && !"".equals(listText)) {

                mViewHolder.textViewLayout = (LinearLayout) convertView
                        .findViewById(R.id.textView_layout);
                mViewHolder.textViewLayout
                        .setLayoutParams(new LinearLayout.LayoutParams(
                                LayoutParams.MATCH_PARENT,
                                LayoutParams.WRAP_CONTENT));

                mViewHolder.icon = (ImageView) convertView
                        .findViewById(R.id.imageView_row_icon);
                mViewHolder.icon.setVisibility(View.VISIBLE);

                mViewHolder.text = (TextView) convertView
                        .findViewById(R.id.tv_row_header);

                mViewHolder.menuListTemplateParent = (LinearLayout) convertView
                        .findViewById(R.id.menu_list_template_parent);

                convertView.setTag(mViewHolder);

                mViewHolder.text.setText(listText);

                if (mFontColor != null && !"".equals(mFontColor)
                        && !"N/A".equalsIgnoreCase(mFontColor)) {
                    mViewHolder.text.setTextColor(Color.parseColor(mFontColor));
                } else {
                    mViewHolder.text.setTextColor(Color.WHITE);
                }

                if (mColor != null && !"".equals(mColor)
                        && !"N/A".equalsIgnoreCase(mColor)) {
                    mViewHolder.menuListTemplateParent.setBackgroundColor(Color
                            .parseColor(mColor));

                } else {
                    mViewHolder.menuListTemplateParent
                            .setBackgroundColor(Color.BLACK);

                }

                if (imagePath != null && !"".equals(imagePath)) {

                    customImageLoader.displayImage(imagePath, mContext,
                            mViewHolder.icon);

                } else {
                    mViewHolder.icon.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return mlist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancelAsyncTask();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }

        try {
            if (isFirstLoad) {
                return;
            }

            if (bb != null) {
                // Add screen name when needed
                bb.setActivityInfo(activity, "Deals");
            }
            dealsDetailsList = new ArrayList<>();
            dealsStateDetailsList = new ArrayList<>();

            dealsCatAdapter = null;
            dealsList.setAdapter(null);

            callDealsCategory();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("", "OnPause");

        try {

            if (progDialog != null && progDialog.isShowing()) {
                progDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private void loadData() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                gpsLatitude = String.valueOf(locNew.getLatitude());
                gpsLongitude = String.valueOf(locNew.getLongitude());

            } else {
                // N/W Tower Info Start
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    gpsLatitude = String.valueOf(locNew.getLatitude());
                    gpsLongitude = String.valueOf(locNew.getLongitude());
                } else {
                    gpsLatitude = CommonConstants.LATITUDE;
                    gpsLongitude = CommonConstants.LONGITUDE;
                }

            }

        } else {
            zipcode = Constants.getZipCode();
        }

        callDealsCategory();

    }

}
