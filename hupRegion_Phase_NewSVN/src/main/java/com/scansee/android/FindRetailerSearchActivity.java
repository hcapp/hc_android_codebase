package com.scansee.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.hubcity.android.CustomLayouts.ToggleButtonView;
import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.OptionSortByList;
import com.hubcity.android.screens.RetailerActivity;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.hubcity.android.screens.SubMenuDetails;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FindRetailerSearchActivity extends CustomTitleBar implements
		OnClickListener, OnInfoWindowClickListener, ToggleButtonView.ToggleViewInterface
{

	private ArrayList<HashMap<String, String>> findlocationList = new ArrayList<>();
	ArrayList<HashMap<String, String>> dupeList = null;
	protected ListView findretsearchListView;

	String accZipcode, selectedCatId;
	FindRetailerSearchAdapter findlocationListAdapter;
	String latitude = null;
	String longitude = null;
	boolean allow = false;
	ViewFlipper findLocationFlipper;
	String userID, retSearchText = null, bottomBtnId = null;
	LocationManager locationManager;
	private GoogleMap mMap;
	LatLngBounds.Builder latLngBounds;
	private ArrayList<Marker> markers = new ArrayList<>();
	String mItemId;
	String radius = "20";
	int lastVisitedNo = 0;
	boolean isViewMore;
	int previousPosition = 0;

	String parCatId;
	boolean isRetailerCatSearch = false;
	EditText edittextRetailer;

	LinearLayout linearLayout = null;
	BottomButtons bb = null;
	Activity activity;
	boolean hasBottomBtns = false;
	boolean enableBottomButton = true;
	boolean gpsEnabled = false;
	boolean isRefresh, isAlreadyLoading, isItemClicked;
	String sortColumn = "Distance";

	// @Rekha START
	boolean isSearchresult = false;
	String strMainMenuId = "";
	PrepareFindList mPrepareFindList = null;
	ListDetails mListDetails = new ListDetails();

	ArrayList<PrepareFindList> mFindList;
	ArrayList<Item> items = new ArrayList<>();

	TextView moreInfo;
	View moreResultsView;
	String tempLastVisit = "0";
	String lastVisited = "0";
	String mLinkId;

	FindLocationCategory findLocAsyncTask;
	HubCityContext mHubCiti;

	String catName = "";
	String srchKey = "";

	String locSpecials = "0", interests = "";

	// @Rekha END

	TextView mLabelDistance, mLabelName;
	private TextView sortTextView;
	private CustomNavigation customNaviagation;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		try {

			setContentView(R.layout.find_location_listview);
			CommonConstants.hamburgerIsFirst = true;
			OptionSortByList.sortChoice = "Distance";
			OptionSortByList.groupChoice = "";
			OptionSortByList.isSortByType = false;
			HubCityContext.savedValues = new HashMap<>();
			isRefresh = true;
			mHubCiti = (HubCityContext) getApplicationContext();
			mHubCiti.setCancelled(false);
			activity = FindRetailerSearchActivity.this;

			try {
				// Initiating Bottom button class
				bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
				// Add screen name when needed
				bb.setActivityInfo(activity, "");
			} catch (Exception e) {
				e.printStackTrace();
			}

			linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
			linearLayout.setVisibility(View.INVISIBLE);

			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
				mItemId = getIntent().getExtras().getString(
						Constants.MENU_ITEM_ID_INTENT_EXTRA);
			}

			if (getIntent().hasExtra("srchKey")
					&& getIntent().getExtras().getString("srchKey") != null) {
				srchKey = getIntent().getExtras().getString("srchKey");
			}

			catName = getIntent().getExtras().getString("catName");
			mLabelName = (TextView) this.findViewById(R.id.toggle_name);
			mLabelDistance = (TextView) this.findViewById(R.id.toggle_distance);
			title.setText("Find");
			leftTitleImage.setVisibility(View.INVISIBLE);
//			rightImage.setVisibility(View.GONE);
			divider.setVisibility(View.GONE);

			backImage.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					hideKeyboardItem();
					finishAction();
				}
			});
			Log.v("", "FIND SEARCH ");
			findViewById(R.id.find_retailer_cancel).setOnClickListener(this);

			edittextRetailer = (EditText) findViewById(R.id.find_retailer_edit_text);
			if (getIntent().getExtras().getString("retSearchText") != null) {
				retSearchText = getIntent().getExtras().getString("retSearchText")
						.trim();
				srchKey = retSearchText;
			}
			if (getIntent().hasExtra("parCatId")) {
				parCatId = getIntent().getExtras().getString("parCatId");
			}

			if (getIntent().hasExtra("isRetailerCatSearch")) {
				isRetailerCatSearch = getIntent().getExtras().getBoolean(
						"isRetailerCatSearch", false);
				edittextRetailer.setText(retSearchText);
			}

			if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
				bottomBtnId = getIntent().getExtras().getString(
						Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
			}

			if (getIntent().hasExtra("mLinkId")) {
				mLinkId = getIntent().getExtras().getString("mLinkId");

			}

			selectedCatId = "";
			if (getIntent().hasExtra("selectedCatId")) {
				selectedCatId = getIntent().getExtras().getString("selectedCatId");
			}

			findLocationFlipper = (ViewFlipper) findViewById(R.id.find_location_flipper);
			findLocationFlipper.setDisplayedChild(0);
			SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
			userID = settings.getString(CommonConstants.USER_ID, "0");

			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			gpsEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
			sortTextView = (TextView) findViewById(R.id.sort_text);
			findretsearchListView = (ListView) findViewById(R.id.find_location_listview_list);

			moreResultsView = getLayoutInflater().inflate(
					R.layout.listitem_get_retailers_viewmore,
					findretsearchListView, false);
			moreInfo = (TextView) moreResultsView.findViewById(R.id.viewMore);
			moreInfo.setOnClickListener(this);

			findretsearchListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id)
				{
					try {
						isItemClicked = true;
						findNavigation(position);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			});
			if (getIntent().hasExtra("allow")) {
				allow = getIntent().getExtras().getBoolean("allow");
			}

			refresh();

			edittextRetailer.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
			edittextRetailer
					.setOnEditorActionListener(new OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView textView,
													  int actionId, KeyEvent event)
						{
							if (actionId == EditorInfo.IME_ACTION_SEARCH) {

								onClick(textView);
								retSearchText = edittextRetailer.getText()
										.toString().trim();

							}
							hideKeyboardItem();
							return true;
						}
					});
			edittextRetailer.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence sequence, int start,
										  int before, int count)
				{
					String value = sequence.toString();

					retSearchText = value.trim();

				}

				@Override
				public void beforeTextChanged(CharSequence sequence, int start,

											  int count, int after)
				{
				}

				@Override
				public void afterTextChanged(Editable editable) {
				}
			});

			findretsearchListView.removeFooterView(moreResultsView);

			//user for hamburger in modules
			drawerIcon.setVisibility(View.VISIBLE);
			drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);

		} catch (Exception e) {
			e.printStackTrace();
		}

		rightImage.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{

				if (GlobalConstants.isFromNewsTemplate) {
					Intent intent = null;
					switch (GlobalConstants.className) {
						case Constants.COMBINATION:
							intent = new Intent(FindRetailerSearchActivity.this,
									CombinationTemplate.class);
							break;
						case Constants.SCROLLING:
							intent = new Intent(FindRetailerSearchActivity.this,
									ScrollingPageActivity.class);
							break;
						case Constants.NEWS_TILE:
							intent = new Intent(FindRetailerSearchActivity.this,
									TwoTileNewsTemplateActivity.class);
							break;
					}
					if (intent != null) {
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
					}
				} else {
					if (SortDepttNTypeScreen.subMenuDetailsList != null) {
						SortDepttNTypeScreen.subMenuDetailsList.clear();
						SubMenuStack.clearSubMenuStack();
						SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<SubMenuDetails>();
					}
					LoginScreenViewAsyncTask.bIsLoginFlag = true;
					callingMainMenu();
				}
			}
		});

	}

	@Override
	public void onToggleClick(String toggleBtnName)
	{
		if (toggleBtnName.equalsIgnoreCase("Distance")) {
			sortTextView.setText(R.string.sort_by_distance);
			sortColumn = "distance";
			setToggleButtonColor();
			freshApiCall();
		} else if (toggleBtnName.equalsIgnoreCase("Name")) {
			sortTextView.setText(R.string.sort_by_name);
			sortColumn = "atoz";
			setToggleButtonColor();
			freshApiCall();
		} else {
			CommonConstants.startMapScreen(FindRetailerSearchActivity.this, getRetailerDetails(), false);
		}
	}
	private void setToggleButtonColor()
	{
		if (sortColumn.equalsIgnoreCase("Distance")) {
			sortTextView.setText(R.string.sort_by_distance);
			if (Build.VERSION.SDK_INT >= 16) {
				mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
				mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
			}else{
				mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
				mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
			}

			mLabelName.setTextColor(getResources().getColor(R.color.white));
			mLabelName.setTypeface(null, Typeface.NORMAL);
			mLabelDistance.setTextColor(getResources().getColor(R.color.black));
			mLabelDistance.setTypeface(null, Typeface.BOLD);
		}
		if (sortColumn.equalsIgnoreCase("Name") || sortColumn.equalsIgnoreCase("atoz")) {
			sortTextView.setText(R.string.sort_by_name);
			if (Build.VERSION.SDK_INT >= 16) {
				mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
				mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
			}else{
				mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
				mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
			}
			mLabelDistance.setTextColor(getResources().getColor(R.color.white));
			mLabelDistance.setTypeface(null, Typeface.NORMAL);
			mLabelName.setTextColor(getResources().getColor(R.color.black));
			mLabelName.setTypeface(null, Typeface.BOLD);
		}
	}
	private void getSortItemName(){

		HashMap<String, String> resultSet;
		resultSet = ((HubCityContext)getApplicationContext()).getFilterValues();
		if (resultSet != null && !resultSet.isEmpty()) {
			if (resultSet.containsKey("SortBy")) {
				sortColumn = resultSet.get("SortBy");
			}
		}
		setToggleButtonColor();
	}

	private void freshApiCall()
	{
		lastVisitedNo = 0;
		findlocationList.clear();
		items.clear();
		mListDetails = new ListDetails();
		isLoadMore = false;
		isViewMore = false;
		enableBottomButton = false;
		refresh();
	}

	private void hideKeyboardItem()
	{
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edittextRetailer.getWindowToken(), 0);
	}

	private void setUpMapIfNeeded()
	{
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				latLngBounds = LatLngBounds.builder();
				UiSettings mUiSettings = mMap.getUiSettings();
				mUiSettings.setAllGesturesEnabled(true);
				mMap.setOnInfoWindowClickListener(this);
			}
		}
	}

	private void refresh()
	{

		if (gpsEnabled) {

			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (locNew != null) {

				latitude = String.valueOf(locNew.getLatitude());
				longitude = String.valueOf(locNew.getLongitude());

			} else {
				// N/W Tower Info Start
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					latitude = String.valueOf(locNew.getLatitude());
					longitude = String.valueOf(locNew.getLongitude());
				} else {
					latitude = CommonConstants.LATITUDE;
					longitude = CommonConstants.LONGITUDE;
				}
				// N/W Tower Info End
			}

			}
			accZipcode = Constants.getZipCode();



		callFindAsyncTask();
		setUpMapIfNeeded();
	}

	private void findNavigation(int position)
	{

		Item i = items.get(position);


		FindRetailerSearchEntryItem ei = (FindRetailerSearchEntryItem) i;
		Log.v("", "USER TRACKING LIST :" + ei.retListId);
		Intent findlocationInfo = new Intent(
				FindRetailerSearchActivity.this, RetailerActivity.class);
		findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
				ei.retailerId);
		findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_LIST_ID,
				ei.retListId);
		findlocationInfo.putExtra(CommonConstants.TAG_RETAILE_LOCATIONID,
				ei.retailerLocId);
		findlocationInfo.putExtra(Constants.MAIN_MENU_ID, strMainMenuId);
		findlocationInfo.putExtra(CommonConstants.TAG_RETAILER_NAME,
				ei.retailerName);
		findlocationInfo.putExtra(CommonConstants.TAG_BANNER_IMAGE_PATH,
				ei.bannerImagePath);
		findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
				ei.ribbonAdImagePath);
		findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_AD_URL,
				ei.ribbonAdUrl);
		findlocationInfo.putExtra(CommonConstants.LINK_ID, mLinkId);
		findlocationInfo.putExtra("ZipCode", accZipcode);
		findlocationInfo.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
				mItemId);
		findlocationInfo.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
				bottomBtnId);
		findlocationInfo.putExtra("fromsearch", true);
		startActivityForResult(findlocationInfo, Constants.STARTVALUE);

		mHubCiti.setCancelled(false);


	}

	private void callFindAsyncTask()
	{
		previousPosition = items.size();
		if (findLocAsyncTask != null) {
			if (!findLocAsyncTask.isCancelled()) {
				findLocAsyncTask.cancel(true);
			}

			findLocAsyncTask = null;
		}

		findLocAsyncTask = new FindLocationCategory();
		findLocAsyncTask.execute();
	}

	private void cancelFindAsyncTask()
	{
		if (findLocAsyncTask != null && !findLocAsyncTask.isCancelled()) {
			findLocAsyncTask.cancel(true);
		}

		findLocAsyncTask = null;
	}

	@Override
	public void onDestroy()
	{
		if (locationManager != null) {
			locationManager.removeUpdates(new ScanSeeLocListener());
		}
		HubCityContext mHubCity = (HubCityContext) getApplicationContext();
		mHubCity.clearArrayAndAllValues(false);
		HubCityContext.isDoneClicked = false;
		cancelFindAsyncTask();

		super.onDestroy();
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		finishAction();
	}

	boolean nextPage = false;
	boolean isLoadMore;

	private class FindLocationCategory extends AsyncTask<String, Void, String>
	{
		JSONObject jsonObject = null;
		JSONArray jsonArrayScanseeData;
		private ProgressDialog mDialog;
		String responseCode = "";

		@Override
		protected void onPreExecute()
		{
			if (!isLoadMore) {
				isLoadMore = true;
				mDialog = ProgressDialog.show(FindRetailerSearchActivity.this,
						"", Constants.DIALOG_MESSAGE, true);
				mDialog.setCancelable(false);
			}
		}

		@Override
		protected String doInBackground(String... params)
		{

			dupeList = new ArrayList<>();
			ArrayList<HashMap<String, String>> scanseeList = new ArrayList<>();
			jsonArrayScanseeData = new JSONArray();
			String result = "false";

			if (isSearchresult || !isViewMore) {
				mListDetails.finalRetailersList.clear();
				findlocationList.clear();

				items.clear();
				isSearchresult = false;
			}

			try {

				String filterId = "";
				String filterValueId = "";
				String cityIds = "";
				String subCatIds = "";
				String sorted_cities = "";
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();
				sorted_cities = Constants.getSortedCityIds(new CityDataHelper().getCitiesList
						(FindRetailerSearchActivity.this));
				HashMap<String, String> resultSet = new HashMap<>();
				resultSet = mHubCiti.getFilterValues();

				if (resultSet != null && !resultSet.isEmpty()) {

					if (resultSet.containsKey("locSpecials")) {
						locSpecials = resultSet.get("locSpecials");
					}

					if (resultSet.containsKey("savedinterests")) {
						interests = resultSet.get("savedinterests");
					}

					if (resultSet.containsKey("savedOptionFilterIds")) {
						filterId = resultSet.get("savedOptionFilterIds");
					}

					if (resultSet.containsKey("savedFValueIds")) {
						filterValueId = resultSet.get("savedFValueIds");
					}

					if (resultSet.containsKey("savedCityIds")) {
						cityIds = resultSet.get("savedCityIds");
						cityIds = cityIds.replace("All,", "");
					}

					if (resultSet.containsKey("savedSubCatIds")) {
						subCatIds = resultSet.get("savedSubCatIds");
					}
				}

				if (cityIds != null && !cityIds.isEmpty()) {
					sorted_cities = Constants.getSortedCityIds(cityIds);
				}
				radius = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).getString
						("radious", "");

				lastVisited = lastVisitedNo + "";
				String get_findcategory_ssretsearch;
				if (isRetailerCatSearch) {
					get_findcategory_ssretsearch = Properties.url_local_server
							+ Properties.hubciti_version + "find/sscatsearchjson";
				} else {
					get_findcategory_ssretsearch = Properties.url_local_server
							+ Properties.hubciti_version + "find/ssretsearchjson";
				}

				JSONObject urlParameters = mUrlRequestParams.getRetailerSearch(
						mItemId, lastVisited, latitude, longitude, radius,
						srchKey, parCatId, accZipcode, bottomBtnId, sortColumn,
						subCatIds, filterId, filterValueId, locSpecials,
						interests, sorted_cities, CommonConstants.convertDeviceTimeToUTC());
				jsonObject = mServerConnections.getUrlJsonPostResponse(
						get_findcategory_ssretsearch, urlParameters);

				HashMap<String, String> scanseeData = null;

				if (jsonObject != null) {

					if (jsonObject.has(
							"responseCode")) {
						responseCode = jsonObject.getString("responseCode");
					}

					// For BottomButton

					if (jsonObject.has(
							"bottomBtnList")) {
						hasBottomBtns = true;

						try {
							ArrayList<BottomButtonBO> bottomButtonList = bb
									.parseForBottomButton(jsonObject);
							BottomButtonListSingleton
									.clearBottomButtonListSingleton();
							BottomButtonListSingleton
									.getListBottomButton(bottomButtonList);

						} catch (JSONException e1) {
							e1.printStackTrace();
						}

					} else {
						hasBottomBtns = false;
					}


					if (!"10000".equals(responseCode)) {
						isSearchresult = false;
						return "false";
					}

					try {

						jsonArrayScanseeData = new JSONArray();

						try {
							jsonArrayScanseeData
									.put(jsonObject.getJSONObject("retailerDetail"));
						} catch (Exception e) {
							jsonArrayScanseeData = jsonObject.getJSONArray("retailerDetail");
						}

						mListDetails = new ListDetails();
						mPrepareFindList = new PrepareFindList();

						for (int arrayCount = 0; arrayCount < jsonArrayScanseeData
								.length(); arrayCount++) {
							scanseeData = new HashMap<>();
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants
											.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH));
							}

							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants
											.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH));
							}

							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERIBBONADURL));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERETAILERID));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants
											.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLISTID)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEERETAILERLISTID,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEERETAILERLISTID));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_RETAILERNAME)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_RETAILERNAME,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_RETAILERNAME));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEADDRESS1));
							}

							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS2)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDLOCATION_SCANSEEADDRESS2,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEADDRESS2));
							}

							// Complete Address
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS)) {
								scanseeData
										.put(CommonConstants
														.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS));
							}

							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_RETAILER_OPEN_CLOSE)) {
								scanseeData
										.put(CommonConstants
														.TAG_RETAILER_OPEN_CLOSE,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_RETAILER_OPEN_CLOSE));
							}

							// City
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEECITY)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEECITY,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEECITY));
							}

							// State
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEESTATE)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEESTATE,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEESTATE));
							}
							// Postal Code
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEEPOSTALCODE)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEEPOSTALCODE,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEPOSTALCODE));
							}

							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEESALEFLAG));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEEDISTANCE));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEELAT)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_LAT,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEELAT));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_SCANSEELON)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_LON,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_SCANSEELON));
							}
							if (jsonArrayScanseeData
									.getJSONObject(arrayCount)
									.has(CommonConstants.TAG_FINDLOCATION_RETAILERNAME)) {
								scanseeData
										.put(CommonConstants.TAG_FINDLOCATION_NAME,
												jsonArrayScanseeData
														.getJSONObject(
																arrayCount)
														.getString(
																CommonConstants
																		.TAG_FINDLOCATION_RETAILERNAME));
							}

							if (scanseeData.isEmpty()) {
								result = "false";
							}

							scanseeData.put("itemName", "scansee");

							// @Rekha START
							findlocationList.add(scanseeData);
							mPrepareFindList.retailersList.add(scanseeData);
							// @Rekha END

						}

						mListDetails.finalRetailersList.add(mPrepareFindList);

						if (jsonObject.has("mainMenuId")) {
							strMainMenuId = jsonObject.getString("mainMenuId");
							Constants.saveMainMenuId(strMainMenuId);
							Log.v("", "MAIN MENU ID : " + strMainMenuId);
						}
						if (jsonObject.has(CommonConstants.FINDPRODUCTSEARCHPRODUCTNEXTPAGE)) {

							if ("1".equalsIgnoreCase(jsonObject.getString(
									CommonConstants.FINDPRODUCTSEARCHPRODUCTNEXTPAGE))) {

								if (jsonObject.has("maxRowNum")) {
									tempLastVisit = jsonObject.getString("maxRowNum");

									if (Integer.valueOf(tempLastVisit) > Integer
											.valueOf(lastVisitedNo)) {
										lastVisitedNo = Integer
												.valueOf(tempLastVisit);
									}

									nextPage = lastVisitedNo < Integer
											.valueOf(jsonObject.getString("maxCnt"));

								}

							} else {
								nextPage = false;
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						scanseeList.clear();
						dupeList.clear();
					}

					result = "true";

				}
			} catch (Exception e) {
				e.printStackTrace();

			}

			// if (isViewMore) {
			// isViewMore = false;
			// }

			return result;
		}

		@Override
		protected void onPostExecute(String result)
		{
			isAlreadyLoading = false;
			if ("true".equals(result)) {

				try {
					if (nextPage) {

						try {
							moreResultsView.setVisibility(View.VISIBLE);
							findretsearchListView
									.removeFooterView(moreResultsView);
						} catch (Exception ex) {
							ex.printStackTrace();
						}

						// moreInfo.setVisibility(View.VISIBLE);

						findretsearchListView.addFooterView(moreResultsView);

					} else {
						findretsearchListView.removeFooterView(moreResultsView);
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}

				mFindList = new ArrayList<>();
				mFindList = mListDetails.finalRetailersList;

				// if (items != null) {
				// items = null;
				// }
				//
				// items = new ArrayList<Item>();

				if (!isViewMore && !"".equalsIgnoreCase(sortColumn)) {
					String sortText = "";

					if ("distance".equalsIgnoreCase(sortColumn)) {
						sortText = "Sorted By Distance";
					} else if ("atoz".equalsIgnoreCase(sortColumn)) {
						sortText = "Sorted By Name";

					}
					sortTextView.setText(sortText);
				}

				for (int i = 0; i < mFindList.size(); i++) {
					for (int j = 0; j < mFindList.get(i).retailersList.size(); j++) {
						items.add(entryItemOject(i, j));
					}
				}

				findlocationListAdapter = new FindRetailerSearchAdapter(
						FindRetailerSearchActivity.this, items, sortColumn,
						findlocationList);
				findretsearchListView.setAdapter(findlocationListAdapter);

				if (null != lastVisited) {
					findretsearchListView.setSelection(Integer
							.valueOf(previousPosition));
				}
				edittextRetailer.setText(retSearchText);
				// edittextRetailer.setSelection(edittextRetailer.getText()
				// .length());

				if (bb != null) {
					bb.setOptionsValues(getListValues(), getRetailerDetails());
				}

			} else {
				try {
					findretsearchListView.removeFooterView(moreResultsView);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				ArrayList<HashMap<String, String>> noItems = new ArrayList<>();
				ArrayList<Item> noItem = new ArrayList<>();

				findlocationListAdapter = new FindRetailerSearchAdapter(
						FindRetailerSearchActivity.this, noItem, "", noItems);
				findretsearchListView.setAdapter(findlocationListAdapter);

				mDialog.dismiss();

				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindRetailerSearchActivity.this);
				notificationAlert.setMessage(getString(R.string.norecord))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(DialogInterface dialog,
														int id)
									{
										dialog.dismiss();
										// finishAction();

									}
								});
				notificationAlert.create().show();

			}

			// Added to disable toggle click
			CommonConstants.disableClick = false;

			if (hasBottomBtns && enableBottomButton) {

				bb.createbottomButtontTab(linearLayout, false);
			}

			FindRetailerSearchActivity.this.getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			linearLayout.setVisibility(View.VISIBLE);

			try {
				if (mDialog != null && mDialog.isShowing()) {
					mDialog.dismiss();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private FindRetailerSearchEntryItem entryItemOject(int i, int j)
	{
		return (new FindRetailerSearchEntryItem(
				mFindList.get(i).retailersList.get(j).get("itemName"),
				mFindList.get(i).retailersList
						.get(j)
						.get(CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH),
				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH),
				mFindList.get(i).retailersList
						.get(j)
						.get(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH),
				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID),

				mFindList.get(i).retailersList
						.get(j)
						.get(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLISTID),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_RETAILERNAME),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1),
				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS2),
				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEECITY),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEESTATE),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEEPOSTALCODE),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEELAT),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_SCANSEELON),

				mFindList.get(i).retailersList.get(j).get(
						CommonConstants.TAG_FINDLOCATION_RETAILERNAME), mFindList.get(i)
				.retailersList.get(j).get(
						CommonConstants.TAG_RETAILER_OPEN_CLOSE)));

	}

	public void performSearch()
	{
		Intent intent = new Intent(FindRetailerSearchActivity.this,
				ScanseeBrowserActivity.class);
		startActivity(intent);
		mHubCiti.setCancelled(false);
	}

	@Override
	public void onClick(View v)
	{

		switch (v.getId()) {

			case R.id.find_retailer_edit_text:
				searchAction();
				break;
			case R.id.find_retailer_cancel:
				cancel();
				break;
			case R.id.viewMore:
				enableBottomButton = false;
				isViewMore = true;
				callFindAsyncTask();
				break;
			default:
				break;
		}
	}

	void LoadMore()
	{
		isAlreadyLoading = true;
		enableBottomButton = false;
		isViewMore = true;
		// Added to disable toggle click
		CommonConstants.disableClick = true;
		callFindAsyncTask();
	}

	private void searchAction()
	{

		lastVisitedNo = 0;
		findlocationList.clear();
		items.clear();
		mListDetails = new ListDetails();
		isLoadMore = false;
		enableBottomButton = false;

		retSearchText = edittextRetailer.getText().toString();
		srchKey = retSearchText;
		isSearchresult = true;
		callFindAsyncTask();
	}

	private void cancel()
	{
		if (edittextRetailer != null) {
			edittextRetailer.setText("");
			hideKeyboardItem();
		}
	}

	private ArrayList<RetailerBo> getRetailerDetails()
	{
		ArrayList<RetailerBo> mRetailerList = new ArrayList<>();
		for (int i = 0; i < findlocationList.size(); i++) {

			String retailerName = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_RETAILERNAME);
			String retailAddress = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1);
			String latitude = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_LAT);
			String longitude = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_LON);
			String retailId = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID);
			String ribbonAdImagePath = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH);
			String ribbonAdURL = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL);
			String locationID = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID);
			String bannerAd = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH);
			RetailerBo retailerObject = new RetailerBo(null, retailId,
					retailerName, locationID, null, retailAddress, null, null,
					bannerAd, ribbonAdImagePath, ribbonAdURL, null, latitude,
					longitude, null);
			mRetailerList.add(retailerObject);

		}

		return mRetailerList;

	}

	private HashMap<String, String> getListValues()
	{
		HashMap<String, String> values = new HashMap<>();

		values.put("Class", "Find All");

		if (getIntent().hasExtra("catName")) {

			values.put("catName", getIntent().getExtras().getString("catName"));
		}

		if (getIntent().hasExtra("selectedCatId")) {

			values.put("selectedCatId",
					getIntent().getExtras().getString("selectedCatId"));
		}

		values.put("srchKey", srchKey);

		values.put("mItemId", mItemId);

		values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

		values.put("latitude", latitude);

		values.put("longitude", longitude);
		values.put("mLinkId", mLinkId);

		values.put("subCatId", getIntent().getExtras().getString("subCatId"));

		return values;
	}

	// @Rekha START
	class ListDetails
	{
		ArrayList<PrepareFindList> finalRetailersList = new ArrayList<>();
	}

	class PrepareFindList
	{
		ArrayList<HashMap<String, String>> retailersList = new ArrayList<>();
	}

	// @Rekha END

	@Override
	public void onInfoWindowClick(Marker marker)
	{
		if (!markers.isEmpty()) {

			Item i = items.get(markers.indexOf(marker));

			FindRetailerSearchEntryItem ei = (FindRetailerSearchEntryItem) i;

			Intent findlocationInfo = new Intent(
					FindRetailerSearchActivity.this, RetailerActivity.class);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
					ei.retailerId);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_LIST_ID,
					ei.retListId);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAILE_LOCATIONID,
					ei.retailerLocId);
			findlocationInfo.putExtra(Constants.MAIN_MENU_ID, strMainMenuId);
			findlocationInfo.putExtra(CommonConstants.TAG_RETAILER_NAME,
					ei.retailerName);
			findlocationInfo.putExtra(CommonConstants.TAG_BANNER_IMAGE_PATH,
					ei.bannerImagePath);
			findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
					ei.ribbonAdImagePath);
			findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_AD_URL,
					ei.ribbonAdUrl);
			findlocationInfo.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
					mItemId);
			findlocationInfo.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
					bottomBtnId);
			startActivityForResult(findlocationInfo, Constants.STARTVALUE);
			mHubCiti.setCancelled(false);
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}
		// Add screen name when needed
		if (bb != null) {
			bb.setActivityInfo(activity, "");
			bb.setOptionsValues(getListValues(), getRetailerDetails());
		}

		if (mHubCiti.isCancelled()) {
			return;
		}

		if (!isRefresh) {
			if (!isItemClicked) {
				ArrayList<HashMap<String, String>> noItems = new ArrayList<>();
				ArrayList<Item> noItem = new ArrayList<>();

				findlocationListAdapter = new FindRetailerSearchAdapter(
						FindRetailerSearchActivity.this, noItem, "", noItems);
				findretsearchListView.setAdapter(findlocationListAdapter);

				lastVisitedNo = 0;
				findlocationList.clear();
				items.clear();
				mListDetails = new ListDetails();
				isLoadMore = false;
				isViewMore = false;
				enableBottomButton = false;
				getSortItemName();
				refresh();
			}

		}
		isItemClicked = false;
		isRefresh = false;
	}

	private void finishAction()
	{

		cancelFindAsyncTask();
		finish();
		mHubCiti.clearSavedValues();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{

		if (resultCode == 30001 || resultCode == 2) {
			isRefresh = false;
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		isRefresh = true;
	}

}
