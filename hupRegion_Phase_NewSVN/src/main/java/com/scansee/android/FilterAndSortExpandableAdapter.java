package com.scansee.android;

import java.util.ArrayList;

import com.hubcity.android.businessObjects.FilterArdSortScreenBO;
import com.scansee.hubregion.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class FilterAndSortExpandableAdapter extends BaseExpandableListAdapter
{

	Context mContext;
	ArrayList<FilterArdSortScreenBO> arrSortAndFilter;
	boolean notChecked;

	public FilterAndSortExpandableAdapter(Context context,
			ArrayList<FilterArdSortScreenBO> arrSortAndFilter)
	{
		this.mContext = context;
		this.arrSortAndFilter = arrSortAndFilter;
	}

	@Override
	public int getGroupCount()
	{
		return arrSortAndFilter.size();
	}

	@Override
	public int getChildrenCount(int groupPosition)
	{
		return arrSortAndFilter.get(groupPosition).getArrFilterOptionBOs()
				.size();
	}

	@Override
	public Object getGroup(int groupPosition)
	{
		return arrSortAndFilter.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition)
	{
		return arrSortAndFilter.get(groupPosition).getArrFilterOptionBOs()
				.get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition)
	{
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition)
	{
		return 0;
	}

	@Override
	public boolean hasStableIds()
	{
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent)
	{
		LayoutInflater infalInflater = (LayoutInflater) this.mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TextView filter_name;

		if (arrSortAndFilter.get(groupPosition).isHeader()) {
			convertView = infalInflater.inflate(
					R.layout.filter_sort_header_layout, parent, false);
			filter_name = (TextView) convertView
					.findViewById(R.id.filter_sort_header_text);
		} else {
			convertView = infalInflater.inflate(
					R.layout.filter_sort_filters_layout, parent, false);
			filter_name = (TextView) convertView
					.findViewById(R.id.filter_sort_filters_text);
		}

		filter_name
				.setText(arrSortAndFilter.get(groupPosition).getFilterName());

		try {
			if (arrSortAndFilter.get(groupPosition).getHeaderName().equalsIgnoreCase("Sort Items" +
					" " +
					"by")) {
				if (!arrSortAndFilter.get(groupPosition).isHeader()) {
					if (arrSortAndFilter.get(groupPosition).isChecked()) {
						convertView
								.findViewById(R.id.filter_sort_tick_image)
								.setVisibility(View.VISIBLE);
					} else {
						convertView
								.findViewById(R.id.filter_sort_tick_image)
								.setVisibility(View.GONE);
					}
				}
			} else {
				if (arrSortAndFilter.get(groupPosition).getFilterName()
						.contains("Local Specials") || arrSortAndFilter.get(groupPosition)
						.getFilterName()
						.contains("Date")
						) {
					if (!arrSortAndFilter.get(groupPosition).isHeader()) {
						if (arrSortAndFilter.get(groupPosition).isChecked()) {
							convertView
									.findViewById(R.id.filter_sort_tick_image)
									.setVisibility(View.VISIBLE);
						} else {
							convertView
									.findViewById(R.id.filter_sort_tick_image)
									.setVisibility(View.GONE);
						}
					}
				} else {
					if (!arrSortAndFilter.get(groupPosition).isHeader()) {
						convertView
								.findViewById(R.id.filter_sort_down_arrow_image)
								.setVisibility(View.VISIBLE);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent)
	{
		LayoutInflater infalInflater = (LayoutInflater) this.mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = infalInflater
				.inflate(R.layout.filter_option_layout, parent, false);
		((TextView) convertView.findViewById(R.id.filter_option_text))
				.setText(arrSortAndFilter.get(groupPosition)
						.getArrFilterOptionBOs().get(childPosition)
						.getFilterName());
		if (arrSortAndFilter.get(groupPosition).getArrFilterOptionBOs()
				.get(childPosition).isChecked()) {
			((CheckBox) convertView.findViewById(R.id.filter_option_checkbox))
					.setChecked(true);
		} else {
			((CheckBox) convertView.findViewById(R.id.filter_option_checkbox))
					.setChecked(false);
			if (!arrSortAndFilter.get(groupPosition)
					.getArrFilterOptionBOs().get(childPosition)
					.getFilterGroupName().equals("Department")
					&& !arrSortAndFilter.get(groupPosition)
					.getArrFilterOptionBOs()
					.get(childPosition)
					.getFilterGroupName().equals("Type")) {
				arrSortAndFilter.get(groupPosition).getArrFilterOptionBOs().get(0)
						.setChecked(false);
			}
			notifyDataSetChanged();
		}

		((CheckBox) convertView.findViewById(R.id.filter_option_checkbox))
				.setOnCheckedChangeListener(new OnCheckedChangeListener()
				{

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked)
					{
						if (arrSortAndFilter.get(groupPosition)
								.getArrFilterOptionBOs().get(childPosition)
								.getFilterName().equals("All"))
						{
							if (!isChecked) {
								for (int i = 0; i < arrSortAndFilter
										.get(groupPosition)
										.getArrFilterOptionBOs().size(); i++) {
									arrSortAndFilter.get(groupPosition)
											.getArrFilterOptionBOs().get(i)
											.setChecked(false);
								}
							} else {
								for (int i = 0; i < arrSortAndFilter
										.get(groupPosition)
										.getArrFilterOptionBOs().size(); i++) {
									arrSortAndFilter.get(groupPosition)
											.getArrFilterOptionBOs().get(i)
											.setChecked(true);
								}
							}
							notifyDataSetChanged();
						}
						if (arrSortAndFilter.get(groupPosition)
								.getArrFilterOptionBOs().get(childPosition)
								.getFilterGroupName().equals("Department")
								|| arrSortAndFilter.get(groupPosition)
								.getArrFilterOptionBOs()
								.get(childPosition)
								.getFilterGroupName().equals("Type")) {
							if (!isChecked) {
								arrSortAndFilter.get(groupPosition)
										.getArrFilterOptionBOs()
										.get(childPosition).setChecked(false);
							} else {
								for (int i = 0; i < arrSortAndFilter.size(); i++) {
									for (int j = 0; j < arrSortAndFilter
											.get(i)
											.getArrFilterOptionBOs().size(); j++) {
										if (arrSortAndFilter.get(i)
												.getArrFilterOptionBOs().get(j)
												.getFilterGroupName().equals("Department")
												|| arrSortAndFilter.get(i)
												.getArrFilterOptionBOs()
												.get(j)
												.getFilterGroupName().equals("Type")) {
											arrSortAndFilter.get(i)
													.getArrFilterOptionBOs().get(j)
													.setChecked(false);
										}
									}
								}

								arrSortAndFilter.get(groupPosition)
										.getArrFilterOptionBOs()
										.get(childPosition).setChecked(true);
							}
							notifyDataSetChanged();
						} else {
							if (!isChecked) {
								arrSortAndFilter.get(groupPosition)
										.getArrFilterOptionBOs()
										.get(childPosition).setChecked(false);
								arrSortAndFilter.get(groupPosition)
										.getArrFilterOptionBOs().get(0)
										.setChecked(false);
								notifyDataSetChanged();
							} else {
								arrSortAndFilter.get(groupPosition)
										.getArrFilterOptionBOs()
										.get(childPosition).setChecked(true);
								for (int i = 0; i < arrSortAndFilter
										.get(groupPosition)
										.getArrFilterOptionBOs().size(); i++) {
									if (!arrSortAndFilter.get(groupPosition)
											.getArrFilterOptionBOs().get(i)
											.getFilterName().equals("All")) {
										if (!arrSortAndFilter
												.get(groupPosition)
												.getArrFilterOptionBOs().get(i)
												.isChecked()) {
											notChecked = true;
											return;
										} else {
											notChecked = false;
										}
									}
								}

								if (!notChecked) {
									arrSortAndFilter.get(groupPosition)
											.getArrFilterOptionBOs().get(0)
											.setChecked(true);
									notifyDataSetChanged();
								}
							}
						}
					}
				});
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition)
	{
		return false;
	}

	public void updateArray(ArrayList<FilterArdSortScreenBO> arrSortAndFilter)
	{
		this.arrSortAndFilter = arrSortAndFilter;
	}

}
