package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import com.scansee.hubregion.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RetailerOnlinestoresListAdapter extends BaseAdapter {

	private ArrayList<HashMap<String, String>> onlinestoresList;
	private static LayoutInflater inflater = null;
	HashMap<String, String> onlinestoresData = null;
	String itemName = null;

	public RetailerOnlinestoresListAdapter(
			ArrayList<HashMap<String, String>> onlinestoresList,
			Activity activity) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.onlinestoresList = onlinestoresList;
	}

	@Override
	public int getCount() {
		return onlinestoresList.size();
	}

	@Override
	public Object getItem(int id) {
		return onlinestoresList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		onlinestoresData = onlinestoresList.get(position);
		itemName = onlinestoresData.get("itemName");
		viewHolder = new ViewHolder();
		if ("poweredBy".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_retailer_poweredby, parent,false);
			viewHolder.poweredBy = (TextView) view
					.findViewById(R.id.retailer_online_poweredby);
		}

		else {
			view = inflater.inflate(R.layout.listitem_retaileronlinestores,
					parent,false);
			viewHolder.merchantName = (TextView) view
					.findViewById(R.id.retailer_onlinestores_merchantname);
			viewHolder.originalPrice = (TextView) view
					.findViewById(R.id.retailer_onlinestores_actualprice);
			viewHolder.shippingAmount = (TextView) view
					.findViewById(R.id.retailer_onlinestores_shippingamount);

		}
		view.setTag(viewHolder);

		if ("poweredBy".equalsIgnoreCase(itemName)) {
			viewHolder.poweredBy.setText(onlinestoresData.get("poweredBy"));
		} else {
			viewHolder.merchantName.setText(onlinestoresData
					.get("merchantName"));

			viewHolder.originalPrice.setText(onlinestoresData
					.get("originalPrice"));
			if (onlinestoresData.get("shipAmount").equalsIgnoreCase(
					String.valueOf("$0.00"))
					|| onlinestoresData.get("shipAmount").equalsIgnoreCase(
							String.valueOf("0"))
					|| "N/A".equalsIgnoreCase(onlinestoresData
							.get("shipAmount"))) {
				viewHolder.shippingAmount.setText("Free shipping");
			} else {
				viewHolder.shippingAmount.setText(" + "
						+ onlinestoresData.get("shipAmount") + " shipping");
			}

		}
		return view;
	}

	public static class ViewHolder {
		protected TextView shippingAmount;
		protected TextView originalPrice;
		protected TextView merchantName;
		protected TextView poweredBy;
		protected TextView name;
		protected TextView distance;
		protected TextView price;

	}

}
