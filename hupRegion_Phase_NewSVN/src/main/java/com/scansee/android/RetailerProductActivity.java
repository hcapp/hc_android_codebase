package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.ImageLoaderAsync;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.ShareInformation;
import com.scansee.hubregion.R;

public class RetailerProductActivity extends CustomTitleBar implements
		OnClickListener {

	private HashMap<String, String> productdetailData = new HashMap<>();
	ArrayList<HashMap<String, String>> productList = null;
	public static final String TAG_PRODUCT_RESULT_SET = "ProductDetail";
	public static final String TAG_PRODUCT_VIDEOFLAG = "videoFlag";
	public static final String TAG_PRODUCT_AUDIOFLAG = "audioFlag";
	public static final String TAG_PRODUCT_FILEFLAG = "fileFlag";
	public static final String TAG_PRODUCT_MODEL_NUMBER = "modelNumber";
	public static final String TAG_PRODUCT_WARRANTY = "warrantyServiceInfo";
	public static final String TAG_PRODUCT_PRODUCTATTRIBUTELIST = "productAttributeslst";
	public static final String TAG_PRODUCT_PRODUCTATTRIBUTE = "ProdcutAttributes";
	public static final String TAG_PRODUCT_NUTRITION_TEXT = "attributeName";
	public static final String TAG_PRODUCT_NUTRITION_INFO = "displayValue";

	String productId, userId;
	TextView productname, regularprice, saleprice, productdescription,
			productlongdescription, modelNumber, modelText, warrantyText,
			warranty, nutrition, nutritionInfo;
	ImageView productimage, largeImage;
	CustomImageLoader customImageLoader;
	ImageButton videoBtn, audioBtn, otherinfoBtn;
	PopupWindow pwindo;

	ShareInformation shareInfo;
	boolean isShareTaskCalled;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.retailer_currentsales_product_detail);

		try {
			title.setSingleLine(false);
			title.setMaxLines(1);
			title.setText("Product");


			productdetailData = (HashMap<String, String>) getIntent()
                    .getSerializableExtra("prodDetails");

			productname = (TextView) findViewById(R.id.retailer_currentsales_productname);
			regularprice = (TextView) findViewById(R.id.retailer_currentsales_regularprice);
			saleprice = (TextView) findViewById(R.id.retailer_currentsales_saleprice);
			productdescription = (TextView) findViewById(R.id.retailer_currentsales_productdescription);
			productimage = (ImageView) findViewById(R.id.retailer_currentsales_productimage);
			productlongdescription = (TextView) findViewById(R.id.retailer_product_longdesc);
			modelNumber = (TextView) findViewById(R.id.model_text);
			warranty = (TextView) findViewById(R.id.warranty_text);
			warrantyText = (TextView) findViewById(R.id.warranty);
			modelText = (TextView) findViewById(R.id.model);
			nutrition = (TextView) findViewById(R.id.nutritional);
			nutritionInfo = (TextView) findViewById(R.id.nutritional_text);
			videoBtn = (ImageButton) findViewById(R.id.video_product);
			audioBtn = (ImageButton) findViewById(R.id.audio_product);
			otherinfoBtn = (ImageButton) findViewById(R.id.otherinfo_product);

//			videoBtn.setVisibility(3);
//			audioBtn.setVisibility(3);
//			otherinfoBtn.setVisibility(3);

			if (productdetailData != null) {

                productId = productdetailData
                        .get(CommonConstants.TAG_CURRENTSALES_PRODUCTID);

                // Call for Share Information
                shareInfo = new ShareInformation(RetailerProductActivity.this,
                        productId, "Product Details");
                isShareTaskCalled = false;

                leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
                leftTitleImage.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!Constants.GuestLoginId.equals(UrlRequestParams
                                .getUid().trim())) {
                            // CityPreferencesScreen.bIsSendingTimeStamp =
                            // false;
                            shareInfo.shareTask(isShareTaskCalled);
                            isShareTaskCalled = true;
                        } else {
                            Constants.SignUpAlert(RetailerProductActivity.this);
                            isShareTaskCalled = false;
                        }
                    }
                });

                productname.setText(productdetailData
                        .get(CommonConstants.TAG_PRODUCTNAME));
                if (!"N/A".equalsIgnoreCase(productdetailData
                        .get(CommonConstants.TAG_CURRENTSALES_REGULARPRICE))) {
                    regularprice
                            .setText(getString(R.string.hotdeal_regular_price_text)
                                    + productdetailData
                                            .get(CommonConstants.TAG_CURRENTSALES_REGULARPRICE));
                } else {
                    regularprice.setVisibility(View.GONE);
                }
                if (productdetailData
                        .get(CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER) != null) {
                    if (!"N/A"
                            .equalsIgnoreCase(productdetailData
                                    .get(CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER))) {
                        modelNumber
                                .setText(getString(R.string.scannow_model_number)
                                        + productdetailData
                                                .get(CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER));
                    }
                } else {
                    modelNumber.setVisibility(View.GONE);
                }

                if (!"N/A".equalsIgnoreCase(productdetailData
                        .get(CommonConstants.TAG_CURRENTSALES_SALEPRICE))) {
                    saleprice
                            .setText(getString(R.string.hotdeal_saleprice)
                                    + productdetailData
                                            .get(CommonConstants.TAG_CURRENTSALES_SALEPRICE));
                } else {
                    saleprice.setVisibility(View.GONE);
                }
                if (!"N/A".equalsIgnoreCase(productdetailData
                        .get(CommonConstants.TAG_PRODUCTSHORTDESCRIPTION))) {
                    productdescription.setText(productdetailData
                            .get(CommonConstants.TAG_PRODUCTSHORTDESCRIPTION));
                } else {
                    productdescription.setVisibility(View.GONE);
                }
                if (!"N/A".equalsIgnoreCase(productdetailData
                        .get(CommonConstants.TAG_PRODUCTLONGDESCRIPTION))) {
                    productlongdescription.setText(productdetailData
                            .get(CommonConstants.TAG_PRODUCTLONGDESCRIPTION));
                } else {
                    productlongdescription.setVisibility(View.GONE);
                }

                productimage.setTag(productdetailData
                        .get(CommonConstants.TAG_IMAGEPATH));

                new ImageLoaderAsync(productimage).execute(productdetailData
                        .get(CommonConstants.TAG_IMAGEPATH));
                productimage.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        LayoutInflater inflater = (LayoutInflater) RetailerProductActivity.this
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.screen_popup_image,
                                (ViewGroup) RetailerProductActivity.this
                                        .findViewById(R.id.popup_element));

                        largeImage = (ImageView) layout
                                .findViewById(R.id.enlargeImage);

                        DisplayMetrics metrics = new DisplayMetrics();
                        RetailerProductActivity.this.getWindowManager()
                                .getDefaultDisplay().getMetrics(metrics);
                        int screenWidth = metrics.widthPixels;
                        int screenHeight = metrics.heightPixels;
                        pwindo = new PopupWindow(layout, screenWidth,
                                screenHeight / 2, true);
                        pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
                        new ImageLoaderAsync(largeImage).execute(productdetailData
                                .get(CommonConstants.TAG_IMAGEPATH));
                        btnClosePopup = (Button) layout
                                .findViewById(R.id.btn_close_popup);
                        btnClosePopup.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                pwindo.dismiss();
                            }
                        });
                    }
                });

                title.setSingleLine(false);
                title.setMaxLines(2);
                title.setText(productdetailData
                        .get(CommonConstants.TAG_PRODUCTNAME));

            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Button btnClosePopup;

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.audio_product:
			audioProduct();
			break;

		case R.id.video_product:
			videoProduct();
			break;

		case R.id.otherinfo_product:
			otherinfoProduct();
			break;

		default:
			break;

		}
	}

	private void otherinfoProduct() {
		Intent navIntent = null;
		navIntent = new Intent(RetailerProductActivity.this,
				RetailerProductMediaActivity.class);

		navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_PRODUCTID,
				productId);
		navIntent.putExtra("mediaType", "file");
		startActivity(navIntent);

	}

	private void videoProduct() {
		Intent navIntent = null;
		navIntent = new Intent(RetailerProductActivity.this,
				RetailerProductMediaActivity.class);

		navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_PRODUCTID,
				productId);
		navIntent.putExtra("mediaType", "video");
		startActivity(navIntent);
	}

	private void audioProduct() {
		Intent navIntent = null;
		navIntent = new Intent(RetailerProductActivity.this,
				RetailerProductMediaActivity.class);

		navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_PRODUCTID,
				productId);
		navIntent.putExtra("mediaType", "audio");
		startActivity(navIntent);

	}
}
