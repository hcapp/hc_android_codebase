package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.helper.StringUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.hubcity.android.businessObjects.FilterArdSortScreenBO;
import com.hubcity.android.businessObjects.FilterOptionBO;
import com.hubcity.android.businessObjects.SavedFilterData;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

public class FilterAndSortScreen extends CustomTitleBar {

    private ExpandableListView filter_sort_expandable_list;
    private FilterAndSortExpandableAdapter filterAndSortExpandableAdapter;
    private ArrayList<FilterArdSortScreenBO> arrSortAndFilter;
    private String mItemId = "";
    private String menuId = "";
    private String mBottomId = "";
    private String latitude = "";
    private String longitude = "";
    private String catName = "";
    private String srchKey = "";
    private String eventTypeId = "";
    private int bandId;
    private String filterId = "";
    private String postalCode = "";
    private String className = "";
    private String cityExpId = "";
    private String retailerId = "";
    private String retailLocationId = "";
    private String isRetailerEvents = "";
    private String catId = "";
    private String businessId = "";
    private String fundId = "";
    private String mUniqueId;

    private ArrayList<String> arrSubCatIds = new ArrayList<>();
    private ArrayList<String> arrInterests = new ArrayList<>();
    private ArrayList<String> arrCityIds = new ArrayList<>();
    private ArrayList<String> arrOptionFilterIds = new ArrayList<>();
    private ArrayList<String> arrFValueIds = new ArrayList<>();

    private HashMap<String, String> moduleValues = new HashMap<>();

    private String savedSubCatIds;
    private String savedinterests;
    public String savedCityIds;
    public String savedOptionFilterIds;
    public String savedFValueIds;
    public String sortBy;
    public String eventDate;
    public String savedDepartmentId;
    public String savedTypeId;
    private boolean isDeals;
    int locSpecials;
    OnFilterTaskComplete taskComplete;
    private OnCreateFilterChildView childView;
    private Bundle savedData;
    private HubCityContext hubCiti;
    boolean isSaveSubMenuCityIds;
    String templateName;
    private String radius;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_sort_layout);

        hubCiti = (HubCityContext) getApplicationContext();

        Intent intent = getIntent();
        try {
            savedData = intent.getExtras();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (getIntent().hasExtra("rectangularTemp")) {
            templateName = getIntent().getExtras().getString("rectangularTemp");
        }

        if (intent.hasExtra("values")) {
            moduleValues = (HashMap<String, String>) intent
                    .getSerializableExtra("values");
        }

        if (intent.hasExtra("title")) {
            if ("SortFilter".equalsIgnoreCase(getIntent().getExtras().getString("title"))) {
                this.title.setText("Filter & Sort");
            } else {
                this.title.setText("Filter");
            }
        } else {
            this.title.setText("Filter & Sort");
        }

        this.backImage.setVisibility(View.GONE);
        hubCiti = (HubCityContext) getApplicationContext();
        setViewAndListener();
        getFilterAndSortList();
    }

    private void setViewAndListener() {
        this.leftTitleImage.setBackgroundResource(R.drawable.ic_action_cancel);
        this.leftTitleImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                hubCiti.setCancelled(true);
                HubCityContext.isRefreshSubMenu = false;
                finish();
            }
        });
        this.rightImage.setImageDrawable(null);
        this.rightImage.setBackgroundResource(R.drawable.ic_action_accept);
        this.rightImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                saveChanges();
                if (!className.equalsIgnoreCase("SubMenu")) {
                    SavedFilterData filterData = new SavedFilterData();
                    filterData.setArrSavedData(arrSortAndFilter);
                    savedData.putSerializable("SavedData", filterData);
                    setResult(2, new Intent().putExtras(savedData));
                } else {
                    setResult(2, null);
                }
                Constants.IS_SUB_MENU_REFRESHED = true;
                finish();

            }
        });
        filter_sort_expandable_list = (ExpandableListView) findViewById(R.id
                .filter_sort_expandable_list);
        filter_sort_expandable_list
                .setOnGroupClickListener(new OnGroupClickListener() {

                    @Override
                    public boolean onGroupClick(ExpandableListView parent,
                                                View v, int groupPosition, long id) {

                        if (arrSortAndFilter.get(groupPosition).getFilterName()
                                .equalsIgnoreCase("Category") || arrSortAndFilter.get
                                (groupPosition).getFilterName()
                                .equalsIgnoreCase("Genre"))
                        {

                            if (arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().size() == 0)
                            {
                                //subramanya added as common function
                                getCategoryList(groupPosition);
                            }
                        } else if (arrSortAndFilter.get(groupPosition)
                                .getFilterName().equalsIgnoreCase("Options"))
                        {
                            if (arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().size() == 0)
                            {
                                new GetOptionsAsync(FilterAndSortScreen.this,
                                        mItemId, groupPosition, childView,
                                        businessId, mBottomId, srchKey, latitude, longitude)
                                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        } else if (arrSortAndFilter.get(groupPosition)
                                .getFilterName().equalsIgnoreCase("Interests"))
                        {

                            if (arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().size() == 0)
                            {
                                switch (className) {
                                    case "Find Single":

                                        new GetInterestsAsync(
                                                FilterAndSortScreen.this, mItemId,
                                                groupPosition, catName,
                                                "Find Single", "", childView,
                                                srchKey, mBottomId, latitude, longitude)
                                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        break;
                                    case "Find All":
                                        new GetInterestsAsync(
                                                FilterAndSortScreen.this, mItemId,
                                                groupPosition, catName, "find All",
                                                "", childView, srchKey, mBottomId, latitude, longitude)
                                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        break;
                                    case "Citi Exp":
                                        new GetInterestsAsync(
                                                FilterAndSortScreen.this, "",
                                                groupPosition, "", "CitiExp",
                                                cityExpId, childView, srchKey,
                                                mBottomId, latitude, longitude)
                                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        break;
                                }
                            }
                        } else if (arrSortAndFilter.get(groupPosition)
                                .getFilterName().equalsIgnoreCase("City")) {
                            if (arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().size() == 0) {
                                new GetCityListAsync(FilterAndSortScreen.this,
                                        mItemId, groupPosition, catName,
                                        srchKey, cityExpId, className,
                                        mBottomId, latitude, longitude, catId,
                                        "", "", "", "", "", "", retailerId,
                                        retailLocationId, isRetailerEvents,
                                        isDeals, childView, menuId,radius)
                                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        } else if (arrSortAndFilter.get(groupPosition)
                                .getHeaderName()
                                .contains("Sort") || arrSortAndFilter.get(groupPosition)
                                .getFilterName().contains("Local Specials") /*|| arrSortAndFilter
                                .get(groupPosition)
                                .getFilterName().contains("Date")*/) {
                            if (!arrSortAndFilter.get(groupPosition).isHeader()) {
                                if (!arrSortAndFilter.get(groupPosition)
                                        .isChecked()) {
                                    for (int i = 0; i < arrSortAndFilter.size(); i++) {
                                        if (arrSortAndFilter.get(i)
                                                .getHeaderName()
                                                .contains("Sort")) {
                                            if (arrSortAndFilter
                                                    .get(groupPosition)
                                                    .getHeaderName()
                                                    .equals(arrSortAndFilter
                                                            .get(i)
                                                            .getHeaderName())) {
                                                arrSortAndFilter.get(i)
                                                        .setChecked(false);
                                            }
                                        }
                                    }
                                    arrSortAndFilter.get(groupPosition)
                                            .setChecked(true);
                                    if (arrSortAndFilter.get(groupPosition).getFilterName
                                            ().contains("Local Specials")) {
                                        HubCityContext.savedValues.put("locSpecials", String
                                                .valueOf("1"));
                                    } else {
                                        if (!arrSortAndFilter.get(groupPosition).getFilterName
                                                ().contains("Date")) {
//                                            HubCityContext.sortSelectedPostion = groupPosition;
                                        }
                                    }
                                } else {
                                    if (className.equalsIgnoreCase("SubMenu")) {
                                        arrSortAndFilter.get(groupPosition)
                                                .setChecked(false);
                                    } else {
                                        if (!arrSortAndFilter
                                                .get(groupPosition)
                                                .getHeaderName()
                                                .contains("Sort")) {
                                            arrSortAndFilter.get(groupPosition)
                                                    .setChecked(false);
                                            if (arrSortAndFilter.get(groupPosition).getFilterName
                                                    ().contains("Local Specials")) {
                                                HubCityContext.savedValues.put("locSpecials",
                                                        String.valueOf
                                                                ("0"));
                                            }
                                        }
                                    }
                                }

                                filterAndSortExpandableAdapter
                                        .updateArray(arrSortAndFilter);
                                filterAndSortExpandableAdapter
                                        .notifyDataSetChanged();

                            }
                        } else if (arrSortAndFilter.get(groupPosition)
                                .getFilterName().equalsIgnoreCase("Department")) {
                            if (arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().size() == 0) {
                                new GetDepartmentAndTypeAsync(
                                        FilterAndSortScreen.this, "dept",
                                        groupPosition)
                                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        } else if (arrSortAndFilter.get(groupPosition)
                                .getFilterName().equalsIgnoreCase("Type")) {
                            if (arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().size() == 0) {
                                new GetDepartmentAndTypeAsync(
                                        FilterAndSortScreen.this, "type",
                                        groupPosition)
                                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        } else if ((arrSortAndFilter.get(groupPosition)
                                .getFilterName().equalsIgnoreCase("Event Date") ||
                                arrSortAndFilter.get(groupPosition)
                                        .getFilterName().equalsIgnoreCase("Date")) &&
                                arrSortAndFilter.get
                                        (groupPosition)
                                        .getHeaderName().equalsIgnoreCase("Filter Items by")) {

                            if (!arrSortAndFilter.get(groupPosition)
                                    .isChecked()) {
                                DialogFragment newFragment = new DatePickerFragment();
                                newFragment.show(getSupportFragmentManager(),
                                        "datePicker");
                                arrSortAndFilter.get(groupPosition).setChecked(
                                        true);
                            } else {
                                arrSortAndFilter.get(groupPosition).setChecked(
                                        false);
                                eventDate = "";
                            }
                            filterAndSortExpandableAdapter
                                    .updateArray(arrSortAndFilter);
                            filterAndSortExpandableAdapter
                                    .notifyDataSetChanged();


                        } else {
                            if (arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().size() == 0) {
                                new GetPriceListAsync(FilterAndSortScreen.this,
                                        mItemId, groupPosition, catId, arrSortAndFilter.get
                                        (groupPosition)
                                        .getFilterName(),
                                        latitude, longitude, childView,
                                        mBottomId, srchKey)
                                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        }

                        return false;
                    }
                });
        filter_sort_expandable_list
                .setOnChildClickListener(new OnChildClickListener() {

                    @Override
                    public boolean onChildClick(ExpandableListView parent,
                                                View v, int groupPosition, int childPosition,
                                                long id) {
                        if (!arrSortAndFilter.get(groupPosition)
                                .getArrFilterOptionBOs().get(childPosition)
                                .isChecked()) {
                            arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().get(childPosition)
                                    .setChecked(true);
                        } else {
                            arrSortAndFilter.get(groupPosition)
                                    .getArrFilterOptionBOs().get(childPosition)
                                    .setChecked(false);
                        }

                        filterAndSortExpandableAdapter
                                .updateArray(arrSortAndFilter);
                        filterAndSortExpandableAdapter.notifyDataSetChanged();
                        return false;
                    }
                });
    }

    private void getCategoryList(int groupPosition) {
        switch (className) {
            case "Citi Exp":
                new GetCategoryListAsync(
                        FilterAndSortScreen.this,
                        "citiEXP", cityExpId, "",
                        groupPosition, "", "", "",
                        childView, mBottomId, retailerId,
                        retailLocationId, fundId, "", eventTypeId, bandId, radius, latitude,
                        longitude)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case "filter":
                new GetCategoryListAsync(
                        FilterAndSortScreen.this,
                        className, "", filterId,
                        groupPosition, "", "", "",
                        childView, mBottomId, retailerId,
                        retailLocationId, fundId, "", eventTypeId, bandId, radius, latitude,
                        longitude)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            default:
                new GetCategoryListAsync(
                        FilterAndSortScreen.this,
                        className, "", filterId,
                        groupPosition, catId, postalCode,
                        mItemId, childView, mBottomId,
                        retailerId, retailLocationId,
                        fundId, srchKey, eventTypeId, bandId, radius, latitude, longitude)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
        }
    }

    private void getFilterAndSortList() {
        ProgressDialog pgDialog = ProgressDialog.show(this, "", getResources()
                .getString(R.string.progress_bar_text), false, false);

        if (moduleValues != null && !moduleValues.isEmpty()) {
            className = moduleValues.get("Class");

            if (moduleValues.containsKey("mLinkId")) {
                menuId = moduleValues.get("mLinkId");
            }
            if (moduleValues.containsKey("radius")) {
                radius = moduleValues.get("radius");
            }

            if (moduleValues.containsKey("mItemId")) {
                mItemId = moduleValues.get("mItemId");
            }

        } else {
            if (getIntent().hasExtra("Class")) {
                className = getIntent().getExtras().getString("Class");
            }
            if (getIntent().hasExtra("mLinkId")) {
                menuId = getIntent().getExtras().getString("mLinkId");
            }

            if (getIntent().hasExtra("mItemId")) {
                mItemId = getIntent().getExtras().getString("mItemId");
            }
        }

        String urlParameters;
        if (moduleValues != null && !moduleValues.isEmpty()) {
            if ("Citi Exp".equalsIgnoreCase(className)) {
                latitude = moduleValues.get("latitude");

                longitude = moduleValues.get("longitude");

                srchKey = moduleValues.get("srchKey");

                cityExpId = moduleValues.get(Constants.CITI_EXPID_INTENT_EXTRA);

            } else if ("FindSingleCat".equalsIgnoreCase(className)
                    || "Find".equalsIgnoreCase(className)
                    || "Find All".equalsIgnoreCase(className)) {

                latitude = moduleValues.get("latitude");

                longitude = moduleValues.get("longitude");

                mItemId = moduleValues.get("mItemId");

                srchKey = moduleValues.get("srchKey");

                mBottomId = moduleValues
                        .get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);

                catName = moduleValues.get("catName");

                catId = moduleValues.get("catId");
                if (catId == null) {
                    catId = moduleValues.get("selectedCatId");
                }

                businessId = catId;

            } else if ("filter".equalsIgnoreCase(className)) {
                latitude = moduleValues.get("latitude");

                longitude = moduleValues.get("longitude");

                filterId = moduleValues.get(CommonConstants.TAG_FILTERID);

                if (moduleValues.containsKey("srchKey")) {
                    srchKey = moduleValues.get("srchKey");
                }

            } else {
                mItemId = moduleValues.get(Constants.MENU_ITEM_ID_INTENT_EXTRA);
                mBottomId = moduleValues
                        .get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);

                catName = moduleValues.get("catName");

                catId = moduleValues.get("catId");

            }
            fundId = moduleValues.get("fundId");
            retailerId = moduleValues.get("retailerId");
            retailLocationId = moduleValues.get("retailLocationId");
            isRetailerEvents = moduleValues.get("retailerEvents");
        }

        if (mItemId != null && !mItemId.equals("")) {
            mUniqueId = mItemId;
        } else if (mBottomId != null && !mBottomId.equals("")) {
            mUniqueId = mBottomId;
        }

        if (className.equals("Citi Exp")) {
            urlParameters = new UrlRequestParams()
                    .createSortFilterListParameter("CitiExp", latitude,
                            longitude, cityExpId, srchKey, "");
        } else {
            if (className.equals("FindSingleCat")) {
                className = "Find Single";
            }
            else if (className.equalsIgnoreCase("Coupons") || className.equalsIgnoreCase("myaccounts")) {
                latitude = moduleValues.get("latitude");
                longitude = moduleValues.get("longitude");
                srchKey = moduleValues.get("srchKey");
                radius = moduleValues.get("radius");
            }
            else if (className.equals("BandLanding")) {
                className = "Band";
                latitude = moduleValues.get("latitude");
                srchKey = moduleValues.get("srchKey");
                longitude = moduleValues.get("longitude");
            } else if (className.equals("BandEvents")) {
                className = "BandEvents";
                latitude = moduleValues.get("latitude");

                longitude = moduleValues.get("longitude");
                eventTypeId = moduleValues.get("BandEventTypeID");
                bandId = Integer.parseInt(moduleValues.get("BandID"));

            } else if (className.equals("Find")) {
                className = "Find All";
            }
            urlParameters = new UrlRequestParams()
                    .createSortFilterListParameterAll(className, latitude,
                            longitude, mItemId, srchKey, mBottomId, catName,
                            postalCode, filterId, menuId, eventTypeId, bandId);
        }

        if (className.equalsIgnoreCase("SubMenu")) {
            if (HubCityContext.arrSubMenuDetails.size() > 0) {
                boolean isItemIdExists = false;
                for (int i = 0; i < HubCityContext.arrSubMenuDetails.size(); i++) {
                    if (HubCityContext.arrSubMenuDetails.get(i).getmItemId().equals(mUniqueId)) {
                        arrSortAndFilter = HubCityContext.arrSubMenuDetails.get(i)
                                .getArrSortAndFilter();
                        arrSortAndFilter = HubCityContext
                                .getFilterSavedValuesForSubMenu(
                                        arrSortAndFilter,
                                        HubCityContext.arrSubMenuDetails
                                                .get(i));
                        isItemIdExists = true;
                    }
                }
                if (isItemIdExists) {

                    HubCityContext.arrSortAndFilter = arrSortAndFilter;

                    filterAndSortExpandableAdapter = new FilterAndSortExpandableAdapter(
                            FilterAndSortScreen.this, arrSortAndFilter);
                    filter_sort_expandable_list
                            .setAdapter(filterAndSortExpandableAdapter);
                    pgDialog.dismiss();
                    findViewById(R.id.initial_loader_holder)
                            .setVisibility(View.GONE);
                } else {

                    new FilterAndSortAsyncTask(this, urlParameters, pgDialog,
                            taskComplete, templateName)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            } else {

                new FilterAndSortAsyncTask(this, urlParameters, pgDialog,
                        taskComplete, templateName)
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            new FilterAndSortAsyncTask(this, urlParameters, pgDialog,
                    taskComplete, templateName)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    protected void setDynamicFilterAndSortView(
            ArrayList<FilterArdSortScreenBO> arrSortAndFilter) {
        findViewById(R.id.initial_loader_holder)
                .setVisibility(View.GONE);
        try {
            if (className.equalsIgnoreCase("SubMenu")) {
                try {
                    arrSortAndFilter.get(
                            HubCityContext.savedSubMenuSortByPosition
                                    .get(mUniqueId)).setChecked(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.arrSortAndFilter = arrSortAndFilter;
            } else {
                if (className.equals("Band") && srchKey.equals("")) {
                    this.arrSortAndFilter = HubCityContext
                            .getFilterSavedValues(arrSortAndFilter, true);
                } else {
                    this.arrSortAndFilter = HubCityContext
                            .getFilterSavedValues(arrSortAndFilter, false);
                }
                try {
                    eventDate = HubCityContext.savedValues.get("EventDate");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (className.equals("Band")) {
            arrSortAndFilter.get(1).setChecked(true);
        }

        HubCityContext.arrSortAndFilter = this.arrSortAndFilter;
        isSaveSubMenuCityIds = true;
//		saveChanges();
        HubCityContext.isDoneClicked = false;
        if (filterAndSortExpandableAdapter == null) {
            filterAndSortExpandableAdapter = new FilterAndSortExpandableAdapter(
                    FilterAndSortScreen.this, arrSortAndFilter);
            filter_sort_expandable_list
                    .setAdapter(filterAndSortExpandableAdapter);
        }
        //subramanya added to expand the list in shows and tab screen of austin before click on
        // expand list
        int groupPosition = 0;
        groupPosition = getGroupPosition(groupPosition);

        if (arrSortAndFilter.get(groupPosition).getFilterName()
                .equalsIgnoreCase("Genre")) {
            getCategoryList(groupPosition);
            filter_sort_expandable_list.expandGroup(groupPosition);
        }
    }

    private int getGroupPosition(int groupPosition) {
        for (int i = 0; i < arrSortAndFilter.size(); i++) {
            if (arrSortAndFilter.get(i).getFilterName()
                    .equalsIgnoreCase("Genre")) {
                groupPosition = i;
            }
        }
        return groupPosition;
    }

    protected void setFilterOptionsForChildView(
            ArrayList<FilterOptionBO> arrFilterOptionBOs, int groupPosition,
            String whichFilter) {

        arrSortAndFilter.get(groupPosition).setArrFilterOptionBOs(
                arrFilterOptionBOs);
        HubCityContext.arrSortAndFilter.get(groupPosition)
                .setArrFilterOptionBOs(arrFilterOptionBOs);
//		saveChanges();
        if (className.equalsIgnoreCase("SubMenu")) {
            arrSortAndFilter = HubCityContext
                    .getFilterSavedValuesforCitySubMenu(arrSortAndFilter,
                            groupPosition, mUniqueId);
        } else {
            if (className.equals("Band") && srchKey.equals("")) {
                arrSortAndFilter = HubCityContext
                        .getFilterSavedValues(arrSortAndFilter, true);
            } else if (className.equals("Band") && !srchKey.equals("")) {
                arrSortAndFilter = HubCityContext
                        .getSearchedFilterSavedValues(arrSortAndFilter);
            } else {
                arrSortAndFilter = HubCityContext
                        .getFilterSavedValues(arrSortAndFilter, false);
            }
        }
        HubCityContext.isDoneClicked = false;

        filterAndSortExpandableAdapter.updateArray(arrSortAndFilter);
        filterAndSortExpandableAdapter.notifyDataSetChanged();
    }

    private void saveChanges() {
        // Added this boolean to check whether the values are changed in sort and filter class
        // and if it needs to be modified
        boolean changeCategoryValue = false, changeOptionValue = false, changeInterestValue =
                false, changeCityValue = false;

        arrSubCatIds = new ArrayList<>();
        arrInterests = new ArrayList<>();
        arrCityIds = new ArrayList<>();
        arrOptionFilterIds = new ArrayList<>();
        arrFValueIds = new ArrayList<>();
        HubCityContext.bandArrSubCatIds = new ArrayList<>();
        boolean isSortChecked = false;
        SubMenuFilterAndSortObject subMenuFilterAndSortObject = new SubMenuFilterAndSortObject();
        for (int i = 0; i < arrSortAndFilter.size(); i++)
        {
            if (arrSortAndFilter.get(i).getHeaderName().contains("Sort"))
            {
                if (arrSortAndFilter.get(i).isChecked()) {
                    HubCityContext.sortSelectedPostion = i;
                    sortBy = arrSortAndFilter.get(i).getFilterName();
                    HubCityContext.savedSubMenuSortByPosition.put(mUniqueId, i);
                    isSortChecked = true;
                }
            } else {
                if (arrSortAndFilter.get(i).getFilterName()
                        .contains("Specials")) {
                    if (arrSortAndFilter.get(i).isChecked()) {
                        locSpecials = 1;
                    } else {
                        locSpecials = 0;
                    }
                }
            }
            int arrSize = arrSortAndFilter.get(i).getArrFilterOptionBOs()
                    .size();
            for (int j = 0; j < arrSize; j++)
            {
                if (arrSortAndFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Category"))
                {
                    changeCategoryValue = true;
                    if (arrSortAndFilter.get(i).getArrFilterOptionBOs().get(j)
                            .isChecked() /*&& !arrSortAndFilter.get(i).getArrFilterOptionBOs().get
                            (j).getFilterName().equalsIgnoreCase("All")*/) {
                        if (className.equals("Band") && srchKey.equals("")) {
                            HubCityContext.bandArrSubCatIds.add(arrSortAndFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterId());
                        }
                        arrSubCatIds.add(arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId());
                    }
                } else if (arrSortAndFilter.get(i).getArrFilterOptionBOs()
                        .get(j).getFilterGroupName()
                        .equalsIgnoreCase("Options")) {
                    changeOptionValue = true;
                    if (arrSortAndFilter.get(i).getArrFilterOptionBOs().get(j)
                            .isChecked() /*&& !arrSortAndFilter.get(i).getArrFilterOptionBOs().get
                            (j).getFilterName().equalsIgnoreCase("All")*/) {
                        arrOptionFilterIds.add(arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId());
                    }
                } else if (arrSortAndFilter.get(i).getArrFilterOptionBOs()
                        .get(j).getFilterGroupName()
                        .equalsIgnoreCase("Interests")) {
                    changeInterestValue = true;
                    if (arrSortAndFilter.get(i).getArrFilterOptionBOs().get(j)
                            .isChecked() /*&& !arrSortAndFilter.get(i).getArrFilterOptionBOs().get
                            (j).getFilterName().equalsIgnoreCase("All")*/) {
                        arrInterests.add(arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId());
                    }
                } else if (arrSortAndFilter.get(i).getArrFilterOptionBOs()
                        .get(j).getFilterGroupName().equalsIgnoreCase("City")) {
                    changeCityValue = true;
                    if (arrSortAndFilter.get(i).getArrFilterOptionBOs().get(j)
                            .isChecked() /*&& !arrSortAndFilter.get(i).getArrFilterOptionBOs().get
                            (j).getFilterName().equalsIgnoreCase("All")*/) {
                        isSaveSubMenuCityIds = true;
                        subMenuFilterAndSortObject.getArrCityIds().add(
                                arrSortAndFilter.get(i).getArrFilterOptionBOs()
                                        .get(j).getFilterId());
                        arrCityIds.add(arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId());
                    }
                } else if (arrSortAndFilter.get(i).getArrFilterOptionBOs()
                        .get(j).getFilterGroupName()
                        .equalsIgnoreCase("Department")) {
                    if (arrSortAndFilter.get(i).getArrFilterOptionBOs().get(j)
                            .isChecked() /*&& !arrSortAndFilter.get(i).getArrFilterOptionBOs().get
							(j).getFilterName().equalsIgnoreCase("All")*/) {
                        savedDepartmentId = arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId();
                    }
                } else if (arrSortAndFilter.get(i).getArrFilterOptionBOs()
                        .get(j).getFilterGroupName().equalsIgnoreCase("Type")) {
                    if (arrSortAndFilter.get(i).getArrFilterOptionBOs().get(j)
                            .isChecked() /*&& !arrSortAndFilter.get(i).getArrFilterOptionBOs().get
							(j).getFilterName().equalsIgnoreCase("All")*/) {
                        savedTypeId = arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId();
                    }
                } else {
                    if (arrSortAndFilter.get(i).getArrFilterOptionBOs().get(j)
                            .isChecked() /*&& !arrSortAndFilter.get(i).getArrFilterOptionBOs().get
							(j).getFilterName().equalsIgnoreCase("All")*/) {
                        if (!arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId().equals("")) {
                            arrFValueIds.add(arrSortAndFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterId());
                            HubCityContext.arrFValueIds.remove(arrSortAndFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterId());
                            HubCityContext.arrFValueIds.add(arrSortAndFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterId());
                        } else {
                            HubCityContext.arrFValueIds.remove(arrSortAndFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterGroupName());
                            HubCityContext.arrFValueIds.add(arrSortAndFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterGroupName());
                        }
                    } else {
                        HubCityContext.arrFValueIds.remove(arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterGroupName());
                        HubCityContext.arrFValueIds.remove(arrSortAndFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId());
                    }
                }
            }

        }

        if (changeCategoryValue) {
            HubCityContext.arrSubCatIds = arrSubCatIds;
            savedSubCatIds = StringUtil.join(arrSubCatIds, ",");
            HubCityContext.savedValues.put("savedSubCatIds", savedSubCatIds);
        }
        if (changeOptionValue) {
            HubCityContext.arrOptionFilterIds = arrOptionFilterIds;
            savedOptionFilterIds = StringUtil.join(arrOptionFilterIds, ",");
            HubCityContext.savedValues.put("savedOptionFilterIds", savedOptionFilterIds);
        }
        if (changeInterestValue) {
            HubCityContext.arrInterests = arrInterests;
            savedinterests = StringUtil.join(arrInterests, ",");
            HubCityContext.savedValues.put("savedinterests", savedinterests);
        }
//		if (arrFValueIds.size() > 0) {
//			HubCityContext.arrFValueIds = arrFValueIds;
        savedFValueIds = StringUtil.join(arrFValueIds, ",");
        HubCityContext.savedValues.put("savedFValueIds", savedFValueIds);
//		}
        if (changeCityValue) {
            HubCityContext.arrCityIds = arrCityIds;
            savedCityIds = StringUtil.join(arrCityIds, ",");
            HubCityContext.savedValues.put("savedCityIds", savedCityIds);
        }

        if (!isSortChecked) {
            sortBy = null;
        }
        if (sortBy != null && sortBy.equals("Alphabetically")) {
            sortBy = "atoz";
        }
        if (sortBy == null) {
            HubCityContext.sortSelectedPostion = -1;
            HubCityContext.savedSubMenuSortByPosition.put(mUniqueId, -1);
        }

        HubCityContext.savedValues.put("locSpecials", String.valueOf(locSpecials));
        HubCityContext.savedValues.put("savedDepartmentId", savedDepartmentId);
        HubCityContext.savedValues.put("savedTypeId", savedTypeId);
        HubCityContext.savedValues.put("SortBy", sortBy);
        HubCityContext.savedValues.put("EventDate", eventDate);
        hubCiti.setCancelled(false);

        if (className.equalsIgnoreCase("SubMenu")) {
            if (arrCityIds != null && arrCityIds.size() > 0) {
                HubCityContext.savedSubMenuCityIds.put(mUniqueId, savedCityIds);
            }
            subMenuFilterAndSortObject.setDept("0");
            subMenuFilterAndSortObject
                    .setLevel(HubCityContext.arrSubMenuDetails.size());
            subMenuFilterAndSortObject.setType("0");
            subMenuFilterAndSortObject.setSortBy(sortBy);
            subMenuFilterAndSortObject
                    .setSortSelectedPostion(HubCityContext.sortSelectedPostion);
            subMenuFilterAndSortObject
                    .setSavedDepartmentId(savedDepartmentId);
            subMenuFilterAndSortObject.setSavedTypeId(savedTypeId);
            subMenuFilterAndSortObject.setLevel(HubCityContext.level);
            subMenuFilterAndSortObject.setmItemId(mUniqueId);
            subMenuFilterAndSortObject
                    .setArrSortAndFilter(arrSortAndFilter);
            boolean isItemIdPresent = false;
            if (HubCityContext.arrSubMenuDetails.size() > 0) {
                for (int i = 0; i < HubCityContext.arrSubMenuDetails.size(); i++) {
                    if (HubCityContext.arrSubMenuDetails.get(i).getmItemId().equals(mUniqueId)) {
                        HubCityContext.arrSubMenuDetails.remove(i);
                        HubCityContext.arrSubMenuDetails.add(subMenuFilterAndSortObject);
                        isItemIdPresent = true;
                    }
                }
                if (!isItemIdPresent) {
                    HubCityContext.arrSubMenuDetails.add(subMenuFilterAndSortObject);
                }

            } else {
                HubCityContext.arrSubMenuDetails
                        .add(subMenuFilterAndSortObject);
            }
        }
        HubCityContext.isDoneClicked = true;
        HubCityContext.isRefreshSubMenu = true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hubCiti.setCancelled(true);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        filter_sort_expandable_list = null;
        filterAndSortExpandableAdapter = null;
        arrSortAndFilter = null;

        arrCityIds = null;
        arrFValueIds = null;
        arrInterests = null;
        arrOptionFilterIds = null;
        arrSubCatIds = null;
    }
}
