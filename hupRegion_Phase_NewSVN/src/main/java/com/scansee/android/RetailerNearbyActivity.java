package com.scansee.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.screens.NearByRetailerSummery;
import com.hubcity.android.screens.RetailerActivity;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.HashMap;

public class RetailerNearbyActivity extends CustomTitleBar implements
		OnInfoWindowClickListener {
	RadioGroup nearbyTab;
	String userId, postalcode, productId, ribbonImage, ribbonUrl, productPrice;
	RadioButton radioList, radioMap;
	ListView listView;
	ViewFlipper flipper;
	String retailLocationID = null, retailID, retListId;
	SharedPreferences settings;
	Double mapLng, mapLat;
	HashMap<String, String> nearbyData = null;
	ArrayList<HashMap<String, String>> nearbyList = null;
	HashMap<String, String> loginData = null;
	Intent navIntent;
	HashMap<String, String> reviewsData = null;
	private GoogleMap mMap;
	UiSettings mUiSettings;
	String latitude;
	String longitude;
	LocationManager locationManager;
	com.google.android.gms.maps.model.LatLngBounds.Builder latLngBounds;
	private ArrayList<RetailerBo> mRetailerList;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.retailer_nearby);

		try {
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText("Local Stores");

			leftTitleImage.setVisibility(View.GONE);
			settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
			userId = settings.getString(CommonConstants.USER_ID, "0");

			retailID = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAIL_ID);
			productId = getIntent().getExtras().getString(
                    CommonConstants.TAG_CURRENTSALES_PRODUCTID);
			productPrice = getIntent().getExtras().getString(
                    CommonConstants.TAG_FINDNEARBYDETAILSPRODUCTPRICE);
			retailLocationID = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAILE_LOCATIONID);
			retListId = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAIL_LIST_ID);
			postalcode = getIntent().getExtras().getString("postalcode");
			nearbyTab = (RadioGroup) findViewById(R.id.nearby_radio);
			radioList = (RadioButton) findViewById(R.id.nearby_radio_list);
			radioMap = (RadioButton) findViewById(R.id.nearby_radio_map);
			listView = (ListView) findViewById(R.id.nearby_listview);
			flipper = (ViewFlipper) findViewById(R.id.nearby_flipper);
			nearbyTab.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup arg0, int id) {
                    if (id == radioList.getId()) {
                        flipper.setDisplayedChild(0);
                    } else if (id == radioMap.getId()) {
                        flipper.setDisplayedChild(1);
                    }
                }
            });

			listView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {

                    if (mRetailerList.get(position).getRetailLocationId() != null) {
                        navIntent = new Intent(RetailerNearbyActivity.this,
                                RetailerActivity.class);

                        navIntent.putExtra(CommonConstants.TAG_BANNER_IMAGE_PATH,
                                mRetailerList.get(position).getBannerAdImagePath());
                        navIntent.putExtra(CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
                                mRetailerList.get(position).getRibbonAdImagePath());
                        navIntent.putExtra(CommonConstants.TAG_RIBBON_AD_URL,
                                mRetailerList.get(position).getRibbonAdURL());

                        navIntent.putExtra(CommonConstants.TAG_RETAIL_ID,
                                mRetailerList.get(position).getRetailerId());
                        navIntent.putExtra(CommonConstants.TAG_RETAILE_LOCATIONID,
                                mRetailerList.get(position).getRetailLocationId());
                        navIntent.putExtra(CommonConstants.TAG_RETAILER_NAME,
                                mRetailerList.get(position).getRetailerName());
                        startActivityForResult(navIntent, 116);

                    } else {
                        navIntent = new Intent(RetailerNearbyActivity.this,
                                NearByRetailerSummery.class);
                        navIntent.putExtra("retName", mRetailerList.get(position)
                                .getRetailerName());
                        navIntent.putExtra("address", mRetailerList.get(position)
                                .getRetailAddress());
                        navIntent.putExtra("phone", mRetailerList.get(position)
                                .getPhone());
                        navIntent.putExtra("retailUrl", mRetailerList.get(position)
                                .getRetailerUrl());
                        navIntent.putExtra("latitude", mRetailerList.get(position)
                                .getLatitude());
                        navIntent.putExtra("longitude", mRetailerList.get(position)
                                .getLongitude());
                        startActivity(navIntent);
                    }

                }

            });
			mRetailerList = (getIntent().getExtras()
                    .getParcelableArrayList("mRetailerList"));
			listView.setAdapter(new RetailerNearbyListAdapter(mRetailerList,
                    RetailerNearbyActivity.this));
			if (null != mRetailerList && !mRetailerList.isEmpty()) {

                if (mMap == null) {
                    mMap = ((MapFragment) getFragmentManager().findFragmentById(
                            R.id.map)).getMap();
                    mMap.setOnInfoWindowClickListener(this);

                    for (RetailerBo retailerBo : mRetailerList) {
                        mapLat = Double.valueOf(retailerBo.getLatitude());
                        mapLng = Double.valueOf(retailerBo.getLongitude());

                        // create marker
                        MarkerOptions marker = new MarkerOptions().position(
                                new LatLng(
                                        Double.valueOf(retailerBo.getLatitude()),
                                        Double.valueOf(retailerBo.getLongitude())))
                                .title(retailerBo.getRetailerName() + ","
                                        + retailerBo.getRetailAddress());

                        retailerBo.setMapMarkerId(mMap.addMarker(marker).getId());

                    }
                    if (mRetailerList.size() == 1) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(mapLat, mapLng), 14));
                    } else {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(mapLat, mapLng), 5));
                    }

                    // check if map is created successfully or not
                    if (mMap == null) {
                        Toast.makeText(getApplicationContext(),
                                "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		for (int i = 0; i < mRetailerList.size(); i++) {

			if (mRetailerList.get(i) != null
					&& marker.getId().equals(
							mRetailerList.get(i).getMapMarkerId())) {
				Intent findlocationInfo = new Intent(
						RetailerNearbyActivity.this, RetailerActivity.class);
				findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
						mRetailerList.get(i).getRetailerId());
				findlocationInfo.putExtra(
						CommonConstants.TAG_RETAILE_LOCATIONID, mRetailerList
								.get(i).getRetailLocationId());
				findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_LIST_ID,
						mRetailerList.get(i).getRetListId());
				findlocationInfo.putExtra(CommonConstants.TAG_RETAILER_NAME,
						mRetailerList.get(i).getRetailerName());
				findlocationInfo.putExtra(
						CommonConstants.TAG_BANNER_IMAGE_PATH, mRetailerList
								.get(i).getBannerAdImagePath());
				findlocationInfo.putExtra(
						CommonConstants.TAG_RIBBON_ADIMAGE_PATH, mRetailerList
								.get(i).getRibbonAdImagePath());
				findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_AD_URL,
						mRetailerList.get(i).getRibbonAdURL());
				findlocationInfo.putExtra(CommonConstants.TAG_DISTANCE,
						mRetailerList.get(i).getDistance());
				startActivityForResult(findlocationInfo, Constants.STARTVALUE);
			}

		}
	}

}