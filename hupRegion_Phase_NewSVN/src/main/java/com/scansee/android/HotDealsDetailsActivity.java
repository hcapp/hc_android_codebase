package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.ImageLoaderAsync;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.DealsMoreInformation;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.RetailerActivity;
import com.hubcity.android.screens.ShareInformation;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class HotDealsDetailsActivity extends CustomTitleBar {

    private HashMap<String, String> hotdealsdetails = null;

    String hotDealId, hotDealRetId, hotDealRetLocId, userId, hotDealListId;
    TextView dealName, dealWhen, retailerName, dealsDetails;
    String strTitle;
    ImageView dealImage;
    ImageButton locBotton, infoBotton;
    FrameLayout locLayout, moreInfoLayout;
    Button dealButton, rightButton;
    boolean isMainmenuBtnClicked = false;
    StringBuffer stringBuffer = new StringBuffer();
    private PopupWindow pwindo;
    LinearLayout cityLayout, parentLayout;

    ShareInformation shareInfo;
    boolean isShareTaskCalled;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hotdeals_detail);
        try {
            strTitle = "Deals";

            title.setSingleLine(false);
            title.setMaxLines(2);
            title.setText(strTitle);

            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (GlobalConstants.isFromNewsTemplate){
                        Intent intent = null;
                        switch (GlobalConstants.className) {
                            case Constants.COMBINATION:
                                intent = new Intent(HotDealsDetailsActivity.this, CombinationTemplate.class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(HotDealsDetailsActivity.this, ScrollingPageActivity.class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(HotDealsDetailsActivity.this, TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }else {
                        if ("pushnotify".equals(getIntent().getStringExtra("push"))) {
                            if (SortDepttNTypeScreen.subMenuDetailsList != null) {
                                SortDepttNTypeScreen.subMenuDetailsList.clear();
                                SubMenuStack.clearSubMenuStack();
                                SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
                            }
                            LoginScreenViewAsyncTask.bIsLoginFlag = true;
                            callingMainMenu();
                        } else {
                            setResult(Constants.FINISHVALUE);
                            isMainmenuBtnClicked = true;
                            LoginScreenViewAsyncTask.bIsLoginFlag = true;
                            callingMainMenu();
                        }
                    }
                }
            });
            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (SubMenuStack.getSubMenuStack() != null
                            && SubMenuStack.getSubMenuStack().size() > 0) {
                        finish();
                    } else {
                        callingMainMenu();
                    }
                }
            });
            SharedPreferences settings = getSharedPreferences(
                    CommonConstants.PREFERANCE_FILE, 0);

            userId = settings.getString(CommonConstants.USER_ID, "0");
            hotDealId = getIntent().getExtras().getString("hotDealId");

            hotDealRetId = getIntent().getExtras().getString("hotDealRetId");
            hotDealRetLocId = getIntent().getExtras().getString("hotDealRetLocId");

            parentLayout = (LinearLayout) findViewById(R.id.parent_layout);
            parentLayout.setVisibility(View.INVISIBLE);

            dealName = (TextView) findViewById(R.id.hotdeals_info_name);
            dealWhen = (TextView) findViewById(R.id.hotdeals_info_when);
            dealsDetails = (TextView) findViewById(R.id.short_desc);

            dealImage = (ImageView) findViewById(R.id.hotdeals_info_image);
            dealButton = (Button) findViewById(R.id.getdeal_button);

            locLayout = (FrameLayout) findViewById(R.id.loc_info_layout);
            moreInfoLayout = (FrameLayout) findViewById(R.id.more_info_layout);
            moreInfoLayout.setVisibility(View.GONE);

            locBotton = (ImageButton) findViewById(R.id.loc_info_Button);
            infoBotton = (ImageButton) findViewById(R.id.more_info_Button);

            locBotton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    initiateLocationWindow();

                }
            });

            infoBotton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent openbrowser = new Intent(HotDealsDetailsActivity.this,
                            DealsMoreInformation.class);
                    openbrowser.putExtra("dealName",
                            hotdealsdetails.get("hotDealName"));
                    openbrowser.putExtra("long_desc",
                            hotdealsdetails.get("hDLognDescription"));
                    startActivity(openbrowser);
                }
            });

            hotDealListId = getIntent().getExtras().getString("hotDealListId");
            dealButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if ("Get Deal".equals(dealButton.getText())) {
                        if (!Constants.GuestLoginId.equals(UrlRequestParams
                                .getUid().trim())) {
                            Intent openbrowser = new Intent(
                                    HotDealsDetailsActivity.this,
                                    ScanseeBrowserActivity.class);

                            openbrowser.putExtra(CommonConstants.URL,
                                    hotdealsdetails.get("hdURL"));
                            startActivity(openbrowser);
                        } else {
                            Constants.SignUpAlert(HotDealsDetailsActivity.this);

                        }
                    } else if ("Redeem".equals(dealButton.getText().toString())) {
                        if (!Constants.GuestLoginId.equals(UrlRequestParams
                                .getUid().trim())) {
                            initiatePopupWindow();
                        } else {
                            Constants.SignUpAlert(HotDealsDetailsActivity.this);

                        }
                    } else if ("Claim".equals(dealButton.getText().toString())) {
                        if (!Constants.GuestLoginId.equals(UrlRequestParams
                                .getUid().trim())) {
                            new ClaimHotDeal().execute();
                        } else {
                            Constants.SignUpAlert(HotDealsDetailsActivity.this);
                        }
                    }
                }
            });

            new GetHotDealsDetails().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    Button btnClosePopup, btnRedeem;

    private void initiatePopupWindow() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) HotDealsDetailsActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_new_popup,
                    (ViewGroup) findViewById(R.id.popup_element));

            layout.setBackgroundColor(Color.WHITE);

            DisplayMetrics metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            int screenHeight = metrics.heightPixels;
            pwindo = new PopupWindow(layout, screenWidth - 20,
                    3 * (screenHeight / 4), true);
            pwindo.showAtLocation(layout, Gravity.LEFT | Gravity.BOTTOM, 15, 25);

            btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);
            btnRedeem = (Button) layout.findViewById(R.id.btn_redeem_popup);

            TextView txtEndDatelbl = (TextView) layout
                    .findViewById(R.id.txtEnddate);
            txtEndDatelbl.setVisibility(View.GONE);

            TextView txtExpirationlbl = (TextView) layout
                    .findViewById(R.id.txtExpitation);
            txtExpirationlbl.setVisibility(View.GONE);

            TextView txtCodelbl = (TextView) layout.findViewById(R.id.txtCode);
            txtCodelbl.setVisibility(View.GONE);

            TextView txtView = (TextView) layout.findViewById(R.id.txtView);
            TextView txtStartdateValue = (TextView) layout
                    .findViewById(R.id.txtStartdateValue);

            TextView txtEnddateValue = (TextView) layout
                    .findViewById(R.id.txtEnddateValue);
            txtEnddateValue.setVisibility(View.GONE);

            TextView txtExpitationValue = (TextView) layout
                    .findViewById(R.id.txtExpitationValue);
            txtExpitationValue.setVisibility(View.GONE);

            TextView txtCodeValue = (TextView) layout
                    .findViewById(R.id.txtCodeValue);
            txtCodeValue.setVisibility(View.GONE);

            TextView txtDescriptionValue = (TextView) layout
                    .findViewById(R.id.txtDescriptionValue);
            TextView txtUsernameValue = (TextView) layout
                    .findViewById(R.id.txtUsernameValue);

            txtView.setTextColor(Color.BLACK);
            txtStartdateValue.setTextColor(Color.BLACK);
            txtEnddateValue.setTextColor(Color.BLACK);
            txtExpitationValue.setTextColor(Color.BLACK);
            txtCodeValue.setTextColor(Color.BLACK);
            txtDescriptionValue.setTextColor(Color.BLACK);
            txtUsernameValue.setTextColor(Color.BLACK);

            txtDescriptionValue
                    .setMovementMethod(new ScrollingMovementMethod());
            txtView.setText(hotdealsdetails.get("hotDealName"));
            txtStartdateValue.setText(hotdealsdetails.get("hDStartDate"));

            if (hotdealsdetails.get("hDEndDate") != null
                    && !"".equals(hotdealsdetails.get("hDEndDate"))
                    && !"N/A".equals(hotdealsdetails.get("hDEndDate"))) {
                txtEnddateValue.setVisibility(View.VISIBLE);
                txtEndDatelbl.setVisibility(View.VISIBLE);
                txtEnddateValue.setText(hotdealsdetails.get("hDEndDate"));
            }

            if (hotdealsdetails.get("hDExpDate") != null
                    && !"".equals(hotdealsdetails.get("hDExpDate"))
                    && !"N/A".equals(hotdealsdetails.get("hDExpDate"))) {
                txtExpitationValue.setVisibility(View.VISIBLE);
                txtExpirationlbl.setVisibility(View.VISIBLE);
                txtExpitationValue.setText(hotdealsdetails.get("hDExpDate"));
            }

            if (hotdealsdetails.get("coupCode") != null
                    && !"".equals(hotdealsdetails.get("coupCode"))
                    && !"N/A".equals(hotdealsdetails.get("coupCode"))) {
                txtCodeValue.setVisibility(View.VISIBLE);
                txtCodelbl.setVisibility(View.VISIBLE);
                txtCodeValue.setText(hotdealsdetails.get("coupCode"));
            }

            txtDescriptionValue.setText(hotdealsdetails
                    .get("hDLognDescription"));
            txtUsernameValue.setText(hotdealsdetails.get("userName"));
            btnClosePopup.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();

                }
            });

            btnRedeem.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
                            HotDealsDetailsActivity.this);
                    notificationAlert.setTitle(R.string.confirm);
                    notificationAlert
                            .setMessage(getString(R.string.redeem_text))
                            .setPositiveButton(getString(R.string.specials_ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            new RedeemHotDeal().execute();
                                        }
                                    })
                            .setCancelable(true)
                            .setNegativeButton(
                                    getString(R.string.button_cancel),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();

                                        }

                                    });
                    notificationAlert.create().show();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    Button btnCloseLocPopup;

    private void initiateLocationWindow() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) HotDealsDetailsActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.retailer_screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));

            DisplayMetrics metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            int screenHeight = metrics.heightPixels;
            layout.setBackgroundColor(Color.WHITE);

            TextView txtRetailer = (TextView) layout
                    .findViewById(R.id.txtRetailer);
            TextView txtRetailerName = (TextView) layout
                    .findViewById(R.id.txtRetailerName);
            TextView txtLocations = (TextView) layout
                    .findViewById(R.id.txtLocations);
            TextView txtLocationValue = (TextView) layout
                    .findViewById(R.id.txtLocationsValue);
            TextView txtHotdeal = (TextView) layout
                    .findViewById(R.id.txtHotdeal);

            ImageView locIcon = (ImageView) layout.findViewById(R.id.loc_icon);

            if (hotdealsdetails.get("retLocImgLogo") != null
                    && !"N/A".equalsIgnoreCase(hotdealsdetails
                    .get("retLocImgLogo"))) {
                new CustomImageLoader(getApplicationContext(), false).displayImage(
                        hotdealsdetails.get("retLocImgLogo"),
                        HotDealsDetailsActivity.this, locIcon);
                locIcon.setVisibility(View.VISIBLE);

            } else {
                locIcon.setVisibility(View.GONE);
            }

            txtRetailer.setTextColor(Color.BLACK);
            txtRetailerName.setTextColor(Color.BLACK);
            txtLocations.setTextColor(Color.BLACK);
            txtLocationValue.setTextColor(Color.BLACK);
            txtHotdeal.setTextColor(Color.BLACK);

            if (hotdealsdetails != null) {
                txtRetailerName.setText(hotdealsdetails.get("retName"));
                txtLocationValue.setText(hotdealsdetails.get("retLocs"));
                txtLocationValue.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                HotDealsDetailsActivity.this,
                                RetailerActivity.class);

                        intent.putExtra(CommonConstants.TAG_RETAILER_NAME,
                                hotdealsdetails.get("retName"));

                        if (null != hotdealsdetails
                                .get(CommonConstants.TAG_RETAIL_ID)) {
                            intent.putExtra(CommonConstants.TAG_RETAIL_ID,
                                    hotdealsdetails
                                            .get(CommonConstants.TAG_RETAIL_ID));
                        }

                        if (null != hotdealsdetails.get("retailLocationId")) {
                            intent.putExtra(Constants.TAG_RETAILE_LOCATIONID,
                                    hotdealsdetails.get("retailLocationId"));
                        }

                        if (null != hotdealsdetails
                                .get(CommonConstants.TAG_RETAIL_ID)
                                && null != hotdealsdetails
                                .get("retailLocationId")) {
                            startActivity(intent);
                        }
                        pwindo.dismiss();

                    }
                });
                txtHotdeal.setText(hotdealsdetails.get("hotDealName"));
            }

            pwindo = new PopupWindow(layout, screenWidth - 70,
                    3 * (screenHeight / 4), true);
            pwindo.showAtLocation(layout, Gravity.CENTER | Gravity.BOTTOM, 15,
                    25);
            btnCloseLocPopup = (Button) layout
                    .findViewById(R.id.btn_location_close_popup);
            btnCloseLocPopup.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class GetHotDealsDetails extends AsyncTask<Void, Void, Boolean> {

        JSONObject jsonObject = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPostExecute(Boolean result) {
            try {
                mDialog.dismiss();
                parentLayout.setVisibility(View.VISIBLE);

                if (result) {

                    if (null != hotdealsdetails.get("hDLognDescription")
                            && !"".equals(hotdealsdetails
                            .get("hDLognDescription"))
                            && !"N/A".equals(hotdealsdetails
                            .get("hDLognDescription"))) {

                        moreInfoLayout.setVisibility(View.VISIBLE);

                    } else {

                        moreInfoLayout.setVisibility(View.GONE);
                    }

                    dealName.setText(hotdealsdetails.get("hotDealName"));
                    dealsDetails.setText(hotdealsdetails.get("hDDesc"));

                    if (hotdealsdetails.get("hDEndDate") != null
                            && !"N/A".equalsIgnoreCase(hotdealsdetails
                            .get("hDEndDate"))) {
                        dealWhen.setText(hotdealsdetails.get("hDStartDate")
                                + " - " + hotdealsdetails.get("hDEndDate"));
                    } else {
                        dealWhen.setText(hotdealsdetails.get("hDStartDate"));
                    }

                    dealImage.setTag(hotdealsdetails.get("hotDealImagePath"));

                    new ImageLoaderAsync(dealImage).execute(hotdealsdetails
                            .get("hotDealImagePath"));

                    if (hotdealsdetails.get("extFlag") != null) {
                        if ("0".equalsIgnoreCase(hotdealsdetails.get("extFlag"))) {

                            if (hotdealsdetails.containsKey("retLocs")) {
                                locLayout.setVisibility(View.VISIBLE);
                            } else {
                                locLayout.setVisibility(View.GONE);
                            }

                            if (hotdealsdetails.get("claimFlag") != null) {
                                if ("0".equalsIgnoreCase(hotdealsdetails
                                        .get("claimFlag"))) {
                                    dealButton.setText("Claim");
                                    dealButton.setVisibility(0);
                                } else {
                                    if (hotdealsdetails.get("redeemFlag") != null) {
                                        if ("0".equalsIgnoreCase(hotdealsdetails
                                                .get("redeemFlag"))) {
                                            dealButton.setText("Redeem");
                                            dealButton.setVisibility(0);
                                        } else {
                                            dealButton.setText("Redeem");
                                            dealButton.setVisibility(1);
                                            dealButton.setClickable(false);
                                        }
                                    }
                                }
                            } else if (hotdealsdetails.get("redeemFlag") != null) {
                                if ("0".equalsIgnoreCase(hotdealsdetails
                                        .get("redeemFlag"))) {
                                    dealButton.setText("Redeem");
                                    dealButton.setVisibility(0);
                                } else {
                                    dealButton.setText("Redeem");
                                    dealButton.setEnabled(false);
                                }
                            }
                        } else if ("1".equalsIgnoreCase(hotdealsdetails
                                .get("extFlag"))) {
                            locLayout.setVisibility(View.GONE);
                            dealButton.setText("Get Deal");
                            dealButton.setVisibility(View.VISIBLE);
                        }
                    }

                    // Call for Share Information
                    shareInfo = new ShareInformation(
                            HotDealsDetailsActivity.this, hotDealId, "HotDeals");
                    isShareTaskCalled = false;

                    leftTitleImage
                            .setBackgroundResource(R.drawable.share_btn_down);
                    leftTitleImage
                            .setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    if (!Constants.GuestLoginId
                                            .equals(UrlRequestParams.getUid()
                                                    .trim())) {
                                        shareInfo.shareTask(isShareTaskCalled);
                                        isShareTaskCalled = true;
                                    } else {
                                        Constants
                                                .SignUpAlert(HotDealsDetailsActivity.this);
                                        isShareTaskCalled = false;
                                    }
                                }
                            });

                } else {
                    Toast.makeText(getBaseContext(),
                            "Error Calling WebService", Toast.LENGTH_SHORT)
                            .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(HotDealsDetailsActivity.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean result = false;
            try {
                hotdealsdetails = new HashMap<>();

                UrlRequestParams mUrlRequestParams = new UrlRequestParams();
                ServerConnections mServerConnections = new ServerConnections();

                String get_hotdeals_details = Properties.url_local_server
                        + Properties.hubciti_version + "hotdeals/hddetail";

                String urlParameters = mUrlRequestParams.getGetDeal(hotDealId,
                        hotDealListId);
                JSONObject jsonDataObject = mServerConnections
                        .getUrlPostResponse(get_hotdeals_details,
                                urlParameters, true);

                JSONArray jsonHotDealsArray;
                if (jsonDataObject != null) {
                    JSONObject jsonHotdealsObject = jsonDataObject
                            .getJSONObject("HotDealsListResultSet")
                            .getJSONObject("hotDealsDetailsArrayLst");
                    jsonHotDealsArray = new JSONArray();
                    try {
                        jsonHotDealsArray.put(jsonHotdealsObject
                                .getJSONObject("HotDealsDetails"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonHotDealsArray = jsonHotdealsObject
                                .getJSONArray("HotDealsDetails");
                    }
                    for (int index = 0; index < 1; index++) {

                        jsonObject = jsonHotDealsArray.getJSONObject(index);
                        hotdealsdetails.put("productHotDealID",
                                jsonObject.getString("productHotDealId"));

                        try {
                            hotdealsdetails.put("city",
                                    jsonObject.getString("city"));
                        } catch (Exception e) {
                            e.printStackTrace();
                            hotdealsdetails.put("city", null);
                        }
                        if (jsonObject.has("apiPartnerImagePath")) {
                            hotdealsdetails
                                    .put("apiPartnerImagePath", jsonObject
                                            .getString("apiPartnerImagePath"));
                        }
                        if (jsonObject.has("apiPartnerName")) {
                            hotdealsdetails.put("apiPartnerName",
                                    jsonObject.getString("apiPartnerName"));
                        }
                        if (jsonObject.has("hotDealName")) {
                            hotdealsdetails.put("hotDealName",
                                    jsonObject.getString("hotDealName"));
                        }

                        if (jsonObject.has("hDDesc")) {
                            hotdealsdetails.put("hDDesc",
                                    jsonObject.getString("hDDesc"));
                        }

                        if (jsonObject.has("hotDealImagePath")) {
                            hotdealsdetails.put("hotDealImagePath",
                                    jsonObject.getString("hotDealImagePath"));
                        }
                        if (jsonObject.has("hDLognDescription")) {
                            hotdealsdetails.put("hDLognDescription",
                                    jsonObject.getString("hDLognDescription"));
                        }
                        if (jsonObject.has("retName")) {
                            hotdealsdetails.put("retName",
                                    jsonObject.getString("retName"));
                        }
                        if (jsonObject.has("retLocs")) {
                            hotdealsdetails.put("retLocs",
                                    jsonObject.getString("retLocs"));
                        }
                        if (jsonObject.has("hDPrice")) {
                            hotdealsdetails.put("hDPrice",
                                    jsonObject.getString("hDPrice"));
                        }
                        if (jsonObject.has("hDExpDate")) {
                            hotdealsdetails.put("hDExpDate",
                                    jsonObject.getString("hDExpDate"));
                        }
                        if (jsonObject.has("coupCode")) {
                            hotdealsdetails.put("coupCode",
                                    jsonObject.getString("coupCode"));
                        }
                        if (jsonObject.has("hDSalePrice")) {
                            hotdealsdetails.put("hDSalePrice",
                                    jsonObject.getString("hDSalePrice"));
                        }
                        if (jsonObject.has("hDTermsConditions")) {
                            hotdealsdetails.put("hDTermsConditions",
                                    jsonObject.getString("hDTermsConditions"));
                        }
                        if (jsonObject.has("hdURL")) {
                            hotdealsdetails.put("hdURL",
                                    jsonObject.getString("hdURL"));
                        }
                        if (jsonObject.has("hDStartDate")) {
                            hotdealsdetails.put("hDStartDate",
                                    jsonObject.getString("hDStartDate"));
                        }
                        if (jsonObject.has("hDEndDate")) {
                            hotdealsdetails.put("hDEndDate",
                                    jsonObject.getString("hDEndDate"));
                        }
                        if (jsonObject.has("hDDiscountType")) {
                            hotdealsdetails.put("hDDiscountType",
                                    jsonObject.getString("hDDiscountType"));
                        }
                        if (jsonObject.has("hDDiscountAmount")) {
                            hotdealsdetails.put("hDDiscountAmount",
                                    jsonObject.getString("hDDiscountAmount"));
                        }
                        if (jsonObject.has("hDDiscountPct")) {
                            hotdealsdetails.put("hDDiscountPct",
                                    jsonObject.getString("hDDiscountPct"));
                        }
                        if (jsonObject.has("userName")) {
                            hotdealsdetails.put("userName",
                                    jsonObject.getString("userName"));
                        }
                        if (jsonObject.has("redeemFlag")) {
                            hotdealsdetails.put("redeemFlag",
                                    jsonObject.getString("redeemFlag"));
                        }
                        if (jsonObject.has("claimFlag")) {
                            hotdealsdetails.put("claimFlag",
                                    jsonObject.getString("claimFlag"));
                        }
                        if (jsonObject.has("extFlag")) {
                            hotdealsdetails.put("extFlag",
                                    jsonObject.getString("extFlag"));
                        }
                        // For Appsite
                        if (jsonObject.has("retailId")) {
                            hotdealsdetails.put(CommonConstants.TAG_RETAIL_ID,
                                    jsonObject.getString("retailId"));
                        }
                        if (jsonObject.has("retLocIds")) {
                            hotdealsdetails.put("retailLocationId",
                                    jsonObject.getString("retLocIds"));
                        }
                        if (jsonObject.has("retLocImgLogo")) {
                            hotdealsdetails.put("retLocImgLogo",
                                    jsonObject.getString("retLocImgLogo"));
                        }

                        result = true;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                result = false;
            }
            return result;
        }

    }

    private class RedeemHotDeal extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(HotDealsDetailsActivity.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mDialog.dismiss();
            stringBuffer = new StringBuffer();
            if (result) {
                pwindo.dismiss();
                dealButton.setClickable(false);
            }

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean result = false;
            try {
                String url_get_hotdealredeem = Properties.url_local_server
                        + Properties.hubciti_version + "hotdeals/hotdealredeem";

                UrlRequestParams mUrlRequestParams = new UrlRequestParams();
                ServerConnections mServerConnections = new ServerConnections();
                String urlParameters = mUrlRequestParams
                        .hotDealClaimRedeem(hotDealId);
                JSONObject jsonObject = mServerConnections.getUrlPostResponse(
                        url_get_hotdealredeem, urlParameters, true);

                if (jsonObject != null) {
                    jsonObject = jsonObject.getJSONObject("response");
                    String val = jsonObject.getString("responseText");
                    if ("Success".equalsIgnoreCase(val)) {
                        result = true;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }
    }

    private class ClaimHotDeal extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(HotDealsDetailsActivity.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mDialog.dismiss();
            stringBuffer = new StringBuffer();
            if (result) {
                dealButton.setText("Redeem");
            }

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean result = false;
            try {
                String url_get_hotdealclaim = Properties.url_local_server
                        + Properties.hubciti_version + "hotdeals/hotdealclaim";
                UrlRequestParams mUrlRequestParams = new UrlRequestParams();
                ServerConnections mServerConnections = new ServerConnections();
                String urlParameters = mUrlRequestParams
                        .hotDealClaimRedeem(hotDealId);
                JSONObject jsonObject = mServerConnections.getUrlPostResponse(
                        url_get_hotdealclaim, urlParameters, true);
                if (jsonObject != null) {
                    jsonObject = jsonObject.getJSONObject("response");
                    String val = jsonObject.getString("responseText");
                    if ("Success".equalsIgnoreCase(val)) {
                        result = true;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }
    }

}
