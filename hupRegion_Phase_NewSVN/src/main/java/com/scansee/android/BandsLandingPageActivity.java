package com.scansee.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.screens.BandDetailScreen;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.EventMapScreen;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by supriya.m on 4/19/2016.
 */
public class BandsLandingPageActivity extends CustomTitleBar implements View.OnClickListener {


    private ListView listView;
    private ListView listViewSearch;
    String type;
    private ArrayList<BandSearchListSetGet> arrEvents;
    ArrayList<BandListingSetGet> arrBands;
    private ArrayList<BandSearchListSetGet> arrSearch = new ArrayList<>();
    Double latitude, longitude;
    private String zipcode;
    String radious;
    String mItemId;
    String searchKey;
    String lastVisitedNo;
    String searchLastVisitedNo;
    String mItemName;
    private BottomButtons bb;
    LinearLayout bottomLayout;
    View moreResultsView;
    BandsListAdapter adapter;
    private BandSearchListAdapter bandSearchListAdapter;
    boolean isAlreadyLoading, nextPage, searchNextPage, isFirstLoadDoneForBandSearch;

    private GetBandListAsyncTask bandListAsyncTask;
    private HubCityContext mHubCiti;
    private HashMap<String, String> resultSet;
    private boolean isRefresh;
    private Context mContext;
    private EventTypeListAdapter eventListAdapter;
    private String groupBy, sorted_cities;
    private boolean isShowsScreen;
    private CustomNavigation customNaviagation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_page_screen_layout);
        try {
            mContext = BandsLandingPageActivity.this;
            CommonConstants.hamburgerIsFirst = true;
            leftTitleImage.setVisibility(View.GONE);

            //fetching item id from intent
            if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }
            if (getIntent().hasExtra(Constants.MENU_ITEM_NAME_INTENT_EXTRA)) {
                mItemName = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_NAME_INTENT_EXTRA);
            }
            isRefresh = true;
            mHubCiti = (HubCityContext) getApplicationContext();
            mHubCiti.setCancelled(false);
            // Initiating Bottom button class
            try {
                bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
                bb.setActivityInfo(this, "Name");
            } catch (Exception e) {
                e.printStackTrace();
            }

            title.setText(mItemName);
//		title.setText("Band");

//      setting click listener for all required views
            setListener();
//      creating view for pagination which will be added as listview footer
            moreResultsView = getLayoutInflater().inflate(
                    R.layout.listitem_get_retailers_viewmore, listView,
                    false);
//      hiding device keyboard for first launch
            hideKeyBoard();
//      getting latitude, longitude if not zipcode and fetching radious
            getLocationAndRadious();
//        creating comma separated string for user preferred cities
            sorted_cities = CommonConstants.getSortedCityIds(new CityDataHelper().getCitiesList(this));
//      Loading Names data as default
            setActionForEvents();

            //user for hamburger in modules
            drawerIcon.setVisibility(View.VISIBLE);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
        }

        if (resultCode == 30001 || resultCode == 2) {
            isRefresh = false;

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRefresh = true;
    }

    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();

        values.put("Class", "BandLanding");

        values.put("mItemId", mItemId);

        values.put("latitude", String.valueOf(latitude));

        values.put("longitude", String.valueOf(longitude));

        values.put("moduleName", "Band");

        values.put("postalCode", zipcode);

        values.put("srchKey", searchKey);

        return values;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }

        if (null != bb) {
            if (type.equals("name")) {
                bb.setActivityInfo(this, "Name");
            } else {
                bb.setActivityInfo(this, "Band");
            }
        }
        if (bb != null) {
            bb.setOptionsValues(getListValues(), null);
        }
        if (mHubCiti.isCancelled()) {
            return;
        }
        resultSet = mHubCiti.getFilterValues();
        if (!isRefresh) {
            if (searchKey.equals("")) {
                if (!type.equals("name")) {
                    lastVisitedNo = "0";
                    arrBands = new ArrayList<>();
                    adapter = null;
                    new GetBandListAsyncTask(BandsLandingPageActivity.this, searchKey, radious,
                            zipcode,
                            mItemId, bb, bottomLayout, lastVisitedNo, true, true, latitude, longitude,
                            resultSet, groupBy, sorted_cities)
                            .execute();
                } else {
                    lastVisitedNo = "0";
                    arrEvents = new ArrayList<>();
                    eventListAdapter = null;
                    new GetEventTypeAsyncTask(this, String.valueOf(latitude), String.valueOf(longitude),
                            radious, zipcode, mItemId, bb, bottomLayout, true, lastVisitedNo, resultSet, true, searchKey, sorted_cities).execute();
                }
            } else {

                searchLastVisitedNo = "0";
                bandSearchListAdapter = null;
                arrSearch = new ArrayList<>();
                new GetBandListAsyncTask(BandsLandingPageActivity.this, searchKey, radious,
                        zipcode,
                        mItemId, bb, bottomLayout, searchLastVisitedNo, true, true, latitude,
                        longitude, resultSet, groupBy, sorted_cities)
                        .execute();
            }


        }
        //if category selected in eventmap screen(sortFilter) then selected category reflecting when click sort filter from bandLanding screen.
        if (isShowsScreen) {
            resultSet = null;
            lastVisitedNo = "0";
            searchKey = "";
            ((EditText) findViewById(R.id.landing_page_search_box)).setText("");
            if (type.equals("name")) {
                eventListAdapter = null;
                arrEvents = new ArrayList<>();
                new GetEventTypeAsyncTask(this, String.valueOf(latitude), String.valueOf(longitude),
                        radious, zipcode, mItemId, bb, bottomLayout, true, lastVisitedNo, resultSet, true, searchKey, sorted_cities).execute();
            } else {
                adapter = null;
                arrBands = new ArrayList<>();
                new GetBandListAsyncTask(BandsLandingPageActivity.this, searchKey, radious,
                        zipcode,
                        mItemId, bb, bottomLayout, searchLastVisitedNo, true, true, latitude,
                        longitude, resultSet, groupBy, sorted_cities)
                        .execute();
            }
            isShowsScreen = false;
        }

        isRefresh = false;

    }


    private void getLocationAndRadious() {
        LocationManager locationManager = (LocationManager) getSystemService(Context
                .LOCATION_SERVICE);
        boolean gpsEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (gpsEnabled) {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        } else {
            zipcode = Constants.getZipCode();
        }
        radious = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).getString
                ("radious", "");
    }


    private void setListener() {
        bottomLayout = (LinearLayout) findViewById(R.id.bottom_bar);
        bottomLayout.setVisibility(View.INVISIBLE);
        findViewById(R.id.landing_page_events_button_Layout).setOnClickListener(this);
        findViewById(R.id.landing_page_bands_button_Layout).setOnClickListener(this);
        findViewById(R.id.shows_page_events_button_Layout).setOnClickListener(this);
        listView = (ListView) findViewById(R.id.landing_page_list);
        listViewSearch = (ListView) findViewById(R.id.landing_page_search_list);
        listViewSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!arrSearch.get(position).isGroup()) {
                    Intent intent = new Intent(BandsLandingPageActivity.this, BandDetailScreen
                            .class);
                    intent.putExtra("EventBandID", arrSearch.get(position).getSearchedItemId());
                    startActivity(intent);
                }

            }
        });
        ((ImageView) findViewById(R.id.landing_page_search_icon)).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                manualSearch(((EditText) findViewById(R.id.landing_page_search_box)).getText()
                        .toString());
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(BandsLandingPageActivity.this, BandDetailScreen
                        .class);
                if (type.equals("band")) {
                    if (!arrBands.get(position).isGroup()) {
                        intent.putExtra("EventBandID", arrBands.get(position).getBandId());
                        startActivity(intent);
                    }
                } else {
                    intent.putExtra("EventBandID", arrEvents.get(position).getSearchedItemId());
                    startActivity(intent);
                }
            }
        });

        ((EditText) findViewById(R.id.landing_page_search_box)).setOnEditorActionListener(new
                                                                                                  TextView.OnEditorActionListener() {
                                                                                                      @Override
                                                                                                      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                                                                                          if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                                                                                              manualSearch(v.getText().toString());

                                                                                                          }
                                                                                                          return false;

                                                                                                      }

                                                                                                  });


        ((EditText) findViewById(R.id.landing_page_search_box)).addTextChangedListener(new
                                                                                               TextWatcher() {
                                                                                                   @Override
                                                                                                   public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                                                                   }

                                                                                                   @Override
                                                                                                   public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                                                                       if (s.toString().length() >= 3) {
                                                                                                           arrSearch = new ArrayList<>();
                                                                                                           bandSearchListAdapter = null;
                                                                                                           searchKey = s.toString();
                                                                                                           callBandSearch(false);


                                                                                                       }

                                                                                                   }

                                                                                                   @Override
                                                                                                   public void afterTextChanged(Editable s) {
                                                                                                       try {
                                                                                                           if (s.toString().equals("")) {
                                                                                                               cancelBandSearchAsync();

                                                                                                               HubCityContext.arrSortAndFilter = new ArrayList<>();
                                                                                                               searchKey = "";
                                                                                                               if (bb != null) {
                                                                                                                   bb.setOptionsValues(getListValues(), null);

                                                                                                               }
                                                                                                               listViewSearch.setVisibility(View.GONE);
                                                                                                               listView.setVisibility(View.VISIBLE);

                                                                                                           }

                                                                                                       } catch (Exception e) {
                                                                                                           e.printStackTrace();
                                                                                                       }
                                                                                                   }
                                                                                               });
    }

    private void hideKeyBoard() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context
                    .INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.landing_page_events_button_Layout:
                if (!type.equals("name")) {
                    eventListAdapter = null;
                    lastVisitedNo = "0";
                    setActionForEvents();
                }
                break;
            case R.id.landing_page_bands_button_Layout:
                if (!type.equals("band")) {
                    adapter = null;
                    lastVisitedNo = "0";
                    setActionForBands();
                }

                break;
            case R.id.shows_page_events_button_Layout:
                callEventMapScreen();
                break;
            default:
                break;
        }
    }

    private void callEventMapScreen() {
        isShowsScreen = true;
        Intent eventMapScreen = new Intent(mContext, EventMapScreen.class);
        startActivity(eventMapScreen);
    }

    private void manualSearch(String searchKey) {
        this.searchKey = searchKey;
        if (!searchKey.equals("")) {
            this.arrSearch = new ArrayList<>();
            bandSearchListAdapter = null;
            callBandSearch(true);

            hideKeyBoard();
        }
    }

    /**
     * This method will do all the action for events and load the event type listing
     */
    private void setActionForEvents() {
        // View changes for events button
        RelativeLayout events_button = (RelativeLayout) findViewById(R.id
                .landing_page_events_button_Layout);
        events_button.setBackground(getResources().getDrawable(R.drawable.radio_selected));
        ((TextView) findViewById(R.id.landing_page_events_button_text)).setTypeface(null, Typeface
                .BOLD);
        ((TextView) findViewById(R.id.landing_page_events_button_text)).setTextColor(getResources
                ().getColor(R.color.black));

        // View changes for bands button
        RelativeLayout bands_button = (RelativeLayout) findViewById(R.id
                .landing_page_bands_button_Layout);
        bands_button.setBackground(getResources().getDrawable(R.drawable.radio_unselected));
        ((TextView) findViewById(R.id.landing_page_bands_button_text)).setTypeface(null, Typeface
                .NORMAL);
        ((TextView) findViewById(R.id.landing_page_bands_button_text)).setTextColor(getResources
                ().getColor(R.color.white));


        ((EditText) findViewById(R.id.landing_page_search_box)).setText("");
        ((EditText) findViewById(R.id.landing_page_search_box)).setHint("Search by Band " +
                "name");
        type = "name";
        groupBy = "name";
        searchKey = "";
        arrEvents = new ArrayList<>();
        this.arrSearch = new ArrayList<>();
        bandSearchListAdapter = null;
        try {
            listView.removeFooterView(moreResultsView);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //Clearing filter cached values and sending resultset as null to send fresh request every
        // time
        HubCityContext mHubCity = (HubCityContext) getApplicationContext();
        mHubCity.clearArrayAndAllValues(false);
        HubCityContext.isDoneClicked = false;
        if (bb != null) {
            bb.setActivityInfo(this, "Name");
        }
        new GetEventTypeAsyncTask(this, String.valueOf(latitude), String.valueOf(longitude),
                radious, zipcode, mItemId, bb, bottomLayout, true, lastVisitedNo, resultSet, true, searchKey, sorted_cities).execute();
    }

    /**
     * This method will do all the action for bands and load the band listing
     */
    private void setActionForBands() {
        // View changes for bands button
        RelativeLayout bands_button = (RelativeLayout) findViewById(R.id
                .landing_page_bands_button_Layout);
        bands_button.setBackground(getResources().getDrawable(R.drawable.radio_selected));
        ((TextView) findViewById(R.id.landing_page_bands_button_text)).setTypeface(null, Typeface
                .BOLD);
        ((TextView) findViewById(R.id.landing_page_bands_button_text)).setTextColor(getResources
                ().getColor(R.color.black));


        // View changes for events button
        RelativeLayout events_button = (RelativeLayout) findViewById(R.id
                .landing_page_events_button_Layout);
        events_button.setBackground(getResources().getDrawable(R.drawable.radio_unselected));
        ((TextView) findViewById(R.id.landing_page_events_button_text)).setTypeface(null, Typeface
                .NORMAL);
        ((TextView) findViewById(R.id.landing_page_events_button_text)).setTextColor(getResources
                ().getColor(R.color.white));


        ((EditText) findViewById(R.id.landing_page_search_box)).setText("");
        ((EditText) findViewById(R.id.landing_page_search_box)).setHint("Search by Band " +
                "name/Category");
        type = "band";
        groupBy = "type";

        arrBands = new ArrayList<>();
        this.arrSearch = new ArrayList<>();
        try {
            listViewSearch.removeFooterView(moreResultsView);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        bandSearchListAdapter = null;
        searchKey = "";
        lastVisitedNo = "0";
        //Clearing filter cached values and sending resultset as null to send fresh request every
        // time
        HubCityContext mHubCity = (HubCityContext) getApplicationContext();
        mHubCity.clearArrayAndAllValues(false);
        HubCityContext.isDoneClicked = false;
        if (bb != null) {
            bb.setActivityInfo(this, "Band");
        }
        new GetBandListAsyncTask(BandsLandingPageActivity.this, searchKey, radious, zipcode,
                mItemId, bb, bottomLayout, lastVisitedNo, true, true, latitude, longitude,
                null, groupBy, sorted_cities).execute();
    }

    private void callBandSearch(boolean showProgress) {
        HubCityContext.arrSortAndFilter = new ArrayList<>();
        if (!isFirstLoadDoneForBandSearch) {
            bandListAsyncTask = new GetBandListAsyncTask(BandsLandingPageActivity.this, searchKey,
                    radious,
                    zipcode,
                    mItemId, bb, bottomLayout, "0", false, showProgress, latitude, longitude,
                    resultSet, groupBy, sorted_cities);
            bandListAsyncTask.execute();
            isFirstLoadDoneForBandSearch = true;
            return;
        }

        if (bandListAsyncTask != null) {
            if (!bandListAsyncTask.isCancelled()) {
                bandListAsyncTask.cancel(true);
            }

            bandListAsyncTask = null;
        }

        bandListAsyncTask = new GetBandListAsyncTask(BandsLandingPageActivity.this, searchKey,
                radious,
                zipcode,
                mItemId, bb, bottomLayout, "0", false, showProgress, latitude, longitude,
                resultSet, groupBy, sorted_cities);
        bandListAsyncTask.execute();
    }


    private void cancelBandSearchAsync() {
        if (bandListAsyncTask != null) {
            if (!bandListAsyncTask.isCancelled()) {
                bandListAsyncTask.cancel(true);
            }

            bandListAsyncTask = null;
        }
    }


    public void loadEventList(ArrayList<BandSearchListSetGet> arrEvents, boolean nextPage, String lastVisitedNo) {
        try {
            if (this.arrEvents.size() > 0) {
                this.arrEvents.addAll(arrEvents);
            } else {
                this.arrEvents = arrEvents;
            }

            listViewSearch.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);

            this.lastVisitedNo = lastVisitedNo;
            this.nextPage = nextPage;
            if (nextPage) {
                try {
                    listView.removeFooterView(moreResultsView);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                listView.addFooterView(moreResultsView);
            } else {
                listView.removeFooterView(moreResultsView);
            }
            if (eventListAdapter == null)

            {
                eventListAdapter = new EventTypeListAdapter(BandsLandingPageActivity.this, arrEvents);
                listView.setAdapter(eventListAdapter);
            } else

            {
                eventListAdapter.notifyDataSetChanged();
            }
            isAlreadyLoading = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadSearchList(ArrayList<BandSearchListSetGet> arrSearch, String
            searchLastVisitedNo, boolean searchNextPage) {
        try {
            if (this.arrSearch.size() > 0) {
                int i = 0;
                int mRetailerSize = this.arrSearch.size();
                while (arrSearch.size() != 0) {
                    for (int mEvent = 0; mEvent < this.arrSearch.size(); mEvent++) {
                        String sGroupContent = arrSearch.get(0).getGroupName();
                        boolean isHeader = arrSearch.get(0).isGroup();
                        String mGroupContent = this.arrSearch.get(mEvent).getGroupName();
                        if (sGroupContent.equalsIgnoreCase(mGroupContent)) {
                            if (!isHeader) {
                                int lastCount = 0;
                                for (int count = 0; count < this.arrSearch.size(); count++) {
                                    String mGroupHeader = this.arrSearch.get(count).getGroupName();
                                    if (sGroupContent.equalsIgnoreCase(mGroupHeader)) {
                                        lastCount = count;
                                    }
                                }
                                int insertPos = lastCount + 1;
                                this.arrSearch.add(insertPos, arrSearch.get(0));
                                arrSearch.remove(0);
                                mRetailerSize++;
                                break;
                            } else {
                                arrSearch.remove(0);
                                break;
                            }
                        }

                    }
                    i++;
                    if (i == mRetailerSize) {
                        if (mRetailerSize != 0) {
                            this.arrSearch.addAll(arrSearch);
                            //noinspection CollectionAddedToSelf
                            arrSearch.removeAll(arrSearch);
                        }
                    }
                }
            } else {
                this.arrSearch.addAll(arrSearch);
            }

    /*        int matchedGroupNmbr = 0;
            int addToPosition = 0;
            boolean isAdd = false;
            if (this.arrSearch.size() > 0) {
                for (int i = 0; i < arrSearch.size() - 1; i++) {
                    if (arrSearch.get(i).isGroup()) {
                        for (int j = 0; j < this.arrSearch.size(); j++) {
                            if (this.arrSearch.get(j).isGroup()) {
                                if (this.arrSearch.get(j).getGroupName().equals(arrSearch.get(i)
                                        .getGroupName())) {
                                    for (int k = j + 1; k < this.arrSearch.size(); k++) {
                                        if (this.arrSearch.get(k).isGroup()) {
                                            addToPosition = k;
                                            break;
                                        } else {
                                            if (k == this.arrSearch.size() - 1) {
                                                addToPosition = -1;
                                            }
                                        }
                                    }
                                    matchedGroupNmbr = i;
                                    isAdd = true;
                                    break;
                                }
                            }
                        }
                        if (isAdd) {
                            ArrayList<BandSearchListSetGet> arrayList = new ArrayList<>();
                            for (int k = matchedGroupNmbr + 1; k < arrSearch.size(); k++) {
                                if (!arrSearch.get(k).isGroup()) {
                                    arrayList.add(arrSearch.get(k));
                                } else {
                                    break;
                                }
                            }
                            for (int ind = arrayList.size() - 1; ind >= 0; ind--) {
                                if (addToPosition == -1) {
                                    this.arrSearch.add(arrayList.get(ind));
                                } else {
                                    this.arrSearch.add(addToPosition, arrayList.get(ind));
                                }
                            }
                        }
                    }
                }
            } else {
                this.arrSearch.addAll(arrSearch);
            }*/

            this.searchLastVisitedNo = searchLastVisitedNo;
            this.searchNextPage = searchNextPage;
            listView.setVisibility(View.GONE);
            listViewSearch.setVisibility(View.VISIBLE);
            if (searchNextPage) {

                try {
                    listViewSearch.removeFooterView(moreResultsView);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                listViewSearch.addFooterView(moreResultsView);

            } else {
                listViewSearch.removeFooterView(moreResultsView);
            }
            if (bandSearchListAdapter == null) {
                if (type.equals("band")) {
                    bandSearchListAdapter = new BandSearchListAdapter(BandsLandingPageActivity.this,
                            this.arrSearch, true);
                } else {
                    bandSearchListAdapter = new BandSearchListAdapter(BandsLandingPageActivity.this,
                            this.arrSearch, false);
                }
                listViewSearch.setAdapter(bandSearchListAdapter);
            } else {
                bandSearchListAdapter.notifyDataSetChanged();
            }
            isAlreadyLoading = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadBandList(ArrayList<BandListingSetGet> arrBands, boolean nextPage, String
            lastVisitedNo) {
        try {
          /*  int matchedGroupNmbr = 0;
            int addToPosition = 0;
            boolean isAdd = false;
            if (this.arrBands.size() > 0) {
                for (int i = 0; i < arrBands.size() - 1; i++) {
                    if (arrBands.get(i).isGroup()) {
                        for (int j = 0; j < this.arrBands.size(); j++) {
                            if (this.arrBands.get(j).isGroup()) {
                                if (this.arrBands.get(j).getGroupName().equals(arrBands.get(i)
                                        .getGroupName())) {
                                    for (int k = j + 1; k < this.arrBands.size(); k++) {
                                        if (this.arrBands.get(k).isGroup()) {
                                            addToPosition = k;
                                            break;
                                        } else {
                                            if (k == this.arrBands.size() - 1) {
                                                addToPosition = -1;
                                            }
                                        }
                                    }
                                    matchedGroupNmbr = i;
                                    isAdd = true;
                                    break;
                                }
                            }
                        }
                        if (isAdd) {
                            ArrayList<BandListingSetGet> listingSetGets = new ArrayList<>();
                            for (int k = matchedGroupNmbr + 1; k < arrBands.size(); k++) {
                                if (!arrBands.get(k).isGroup()) {
                                    listingSetGets.add(arrBands.get(k));
                                } else {
                                    break;
                                }
                            }
                            for (int index = listingSetGets.size() - 1; index >= 0; index--) {
                                if (addToPosition == -1) {
                                    this.arrBands.add(listingSetGets.get(index));
                                } else {
                                    this.arrBands.add(addToPosition, listingSetGets.get(index));
                                }
                            }
                        }
                    }
                }
            } else {
                this.arrBands.addAll(arrBands);
            }*/
            if (this.arrBands.size() > 0) {
                int i = 0;
                int mRetailerSize = this.arrBands.size();
                while (arrBands.size() != 0) {
                    for (int mEvent = 0; mEvent < this.arrBands.size(); mEvent++) {
                        String sGroupContent = arrBands.get(0).getGroupName();
                        boolean isHeader = arrBands.get(0).isGroup();
                        String mGroupContent = this.arrBands.get(mEvent).getGroupName();
                        if (sGroupContent.equalsIgnoreCase(mGroupContent)) {
                            if (!isHeader) {
                                int lastCount = 0;
                                for (int count = 0; count < this.arrBands.size(); count++) {
                                    String mGroupHeader = this.arrBands.get(count).getGroupName();
                                    if (sGroupContent.equalsIgnoreCase(mGroupHeader)) {
                                        lastCount = count;
                                    }
                                }
                                int insertPos = lastCount + 1;
                                this.arrBands.add(insertPos, arrBands.get(0));
                                arrBands.remove(0);
                                mRetailerSize++;
                                break;
                            } else {
                                arrBands.remove(0);
                                break;
                            }
                        }

                    }
                    i++;
                    if (i == mRetailerSize) {
                        if (mRetailerSize != 0) {
                            this.arrBands.addAll(arrBands);
                            //noinspection CollectionAddedToSelf
                            arrBands.removeAll(arrBands);
                        }
                    }
                }
            } else {
                this.arrBands.addAll(arrBands);
            }

            this.lastVisitedNo = lastVisitedNo;
            this.nextPage = nextPage;
            if (nextPage) {
                try {
                    listView.removeFooterView(moreResultsView);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                listView.addFooterView(moreResultsView);
            } else {
                listView.removeFooterView(moreResultsView);
            }

            listViewSearch.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            if (adapter == null)

            {
                adapter = new BandsListAdapter(BandsLandingPageActivity.this, this.arrBands);
                listView.setAdapter(adapter);
            } else

            {
                adapter.notifyDataSetChanged();
            }

            isAlreadyLoading = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is written for pagination
     */
    public void loadMoreDataForSearch() {
        isAlreadyLoading = true;
        searchKey = ((EditText) findViewById(R.id.landing_page_search_box)).getText().toString();
        new GetBandListAsyncTask(BandsLandingPageActivity.this, searchKey,
                radious, zipcode, mItemId, bb, bottomLayout, searchLastVisitedNo, false, false,
                latitude, longitude, resultSet, groupBy, sorted_cities)
                .execute();
    }

    /**
     * This method is written for pagination
     */
    public void loadMoreData() {
        isAlreadyLoading = true;
        searchKey = "";
        if (type.equals("band")) {
            new GetBandListAsyncTask(BandsLandingPageActivity.this, searchKey,
                    radious, zipcode, mItemId, bb, bottomLayout, lastVisitedNo, false, false,
                    latitude, longitude, resultSet, groupBy, sorted_cities)
                    .execute();
        } else {
            new GetEventTypeAsyncTask(this, String.valueOf(latitude), String.valueOf(longitude),
                    radious, zipcode, mItemId, bb, bottomLayout, false, lastVisitedNo, resultSet, false, searchKey, sorted_cities).execute();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//       clearing memories by making all the global variables to null
        listView = null;
        listViewSearch = null;
        type = null;
        arrEvents = null;
        arrBands = null;
        arrSearch = null;
        latitude = null;
        longitude = null;
        searchKey = null;
        lastVisitedNo = null;
        zipcode = null;
        radious = null;
        mItemId = null;
        bb = null;
        adapter = null;
        eventListAdapter = null;
        bottomLayout = null;

        HubCityContext mHubCity = (HubCityContext) getApplicationContext();
        mHubCity.clearArrayAndAllValues(false);
        HubCityContext.isDoneClicked = false;
    }
}
