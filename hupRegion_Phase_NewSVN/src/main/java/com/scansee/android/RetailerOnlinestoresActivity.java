package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class RetailerOnlinestoresActivity extends CustomTitleBar {
	private ArrayList<HashMap<String, String>> onlinestoresList = null;
	protected ListView onlinestoresListView;
	RetailerOnlinestoresListAdapter onlinestoresListAdapter;
	String url = null;
	String userId, retailID, postalcode, productId, retailLocationID,
			productListId;
	Intent navIntent;
	SharedPreferences settings = null;
	String latitude;
	String longitude;
	LocationManager locationManager;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.retailer_currentsales_onlinestores_listview);

		try {
			title.setSingleLine(false);
			title.setMaxLines(1);
			title.setText("Online Stores");

			leftTitleImage.setVisibility(View.GONE);

			productId = getIntent().getExtras().getString(
                    CommonConstants.TAG_CURRENTSALES_PRODUCTID);
			productListId = getIntent().getExtras().getString(
                    CommonConstants.TAG_CURRENTSALES_RODUCTLISTID);
			postalcode = Constants.getZipCode();
			retailID = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAILE_ID);
			retailLocationID = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAILE_LOCATIONID);
			onlinestoresListView = (ListView) findViewById(R.id.retailer_onlinestoreslist);
			onlinestoresListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {
                    if ("items".equalsIgnoreCase(onlinestoresList.get(position)
                            .get("itemName"))) {
                        navIntent = new Intent(RetailerOnlinestoresActivity.this,
                                ScanseeBrowserActivity.class);
                        navIntent.putExtra(CommonConstants.URL, onlinestoresList
                                .get(position).get("url"));
                        startActivityForResult(navIntent, Constants.STARTVALUE);
                    }
                }

            });
			new GetOnlineStores().execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onResume() {
		super.onResume();
		try {
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			String provider = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			if (provider.contains("gps")) {

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                        CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                        new ScanSeeLocListener());
                Location locNew = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (locNew != null) {

                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());

                } else {
                    // N/W Tower Info Start
                    locNew = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (locNew != null) {
                        latitude = String.valueOf(locNew.getLatitude());
                        longitude = String.valueOf(locNew.getLongitude());
                    } else {
                        latitude = CommonConstants.LATITUDE;
                        longitude = CommonConstants.LONGITUDE;
                    }
                }
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	UrlRequestParams objUrlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	private class GetOnlineStores extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null, jsonPoweredbyObject, jsonMerchantObject;
		JSONArray jsonPoweredbyArray, jsonMerchantArray;
		HashMap<String, String> onlinestoresData = null;
		private ProgressDialog mDialog;
		Set<String> retailNames = null;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(RetailerOnlinestoresActivity.this,
					"", Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {
			onlinestoresList = new ArrayList<>();
			String result = "false";
			try {
				String get_product_summary = Properties.url_local_server
						+ Properties.hubciti_version + "find/getproductsummary";

				if ((latitude != null) && (longitude != null)) {
					postalcode = null;

					String urlParameters = objUrlRequestParams
							.getRetailersSummary(productId, productListId, "0",
									postalcode, latitude, longitude, retailID,
									retailLocationID);

					jsonObject = mServerConnections.getUrlPostResponse(
							get_product_summary, urlParameters, true);
				} else {
					String urlParameters = objUrlRequestParams
							.getRetailersSummary(productId, productListId, "0",
									postalcode, latitude, longitude, retailID,
									retailLocationID);
					jsonObject = mServerConnections.getUrlPostResponse(
							get_product_summary, urlParameters, true);

				}
				if (jsonObject != null) {
					onlinestoresList = new ArrayList<>();
					jsonPoweredbyArray = new JSONArray();
					jsonMerchantArray = new JSONArray();

					try {
						jsonMerchantObject = jsonObject
								.getJSONObject("ProductSummary")
								.getJSONObject("OnlineStores")
								.getJSONObject("CommissionJunctionData")
								.getJSONObject("RetailerDetail");
						jsonMerchantArray.put(jsonMerchantObject);
					} catch (Exception e) {
						e.printStackTrace();
						try {
							jsonMerchantArray = jsonObject
									.getJSONObject("ProductSummary")
									.getJSONObject("OnlineStores")
									.getJSONObject("CommissionJunctionData")
									.getJSONArray("RetailerDetail");
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					try {
						jsonPoweredbyObject = jsonObject
								.getJSONObject("ProductSummary")
								.getJSONObject("OnlineStores")
								.getJSONObject("offers").getJSONObject("Offer");
						jsonPoweredbyArray.put(jsonPoweredbyObject);
					} catch (Exception e) {
						e.printStackTrace();
						try {
							jsonPoweredbyArray = jsonObject
									.getJSONObject("ProductSummary")
									.getJSONObject("OnlineStores")
									.getJSONObject("offers")
									.getJSONArray("Offer");
						} catch (Exception ex) {
							ex.printStackTrace();
						}

					}
					retailNames = new TreeSet<>();
					for (int catCount = 0; catCount < jsonPoweredbyArray
							.length(); catCount++) {
						retailNames.add(jsonPoweredbyArray.getJSONObject(
								catCount).getString("poweredBy"));
					}
					retailNames = new TreeSet<>(retailNames);

					onlinestoresList = new ArrayList<>();
					for (int catCount = 0; catCount < jsonMerchantArray
							.length(); catCount++) {
						onlinestoresData = new HashMap<>();
						onlinestoresData.put("itemName", "items");
						onlinestoresData.put("merchantName",
								jsonMerchantArray.getJSONObject(catCount)
										.getString("retailerName"));
						onlinestoresData
								.put("originalPrice",
										jsonMerchantArray.getJSONObject(
												catCount)
												.getString("salePrice"));
						onlinestoresData.put("shipAmount",
								jsonMerchantArray.getJSONObject(catCount)
										.getString("shipmentCost"));
						onlinestoresData.put("url", jsonMerchantArray
								.getJSONObject(catCount).getString("buyURL"));
						onlinestoresList.add(onlinestoresData);
					}
					for (String name : retailNames) {
						onlinestoresData = new HashMap<>();
						onlinestoresData.put("itemName", "poweredBy");
						onlinestoresData.put("poweredBy", name);
						onlinestoresList.add(onlinestoresData);
						for (int catCount = 0; catCount < jsonPoweredbyArray
								.length()
								&& name.equalsIgnoreCase(jsonPoweredbyArray
										.getJSONObject(catCount).getString(
												"poweredBy")); catCount++) {
							onlinestoresData = new HashMap<>();
							onlinestoresData.put("itemName", "items");

							onlinestoresData.put("merchantName",
									jsonPoweredbyArray.getJSONObject(catCount)
											.getString("merchantName"));
							onlinestoresData.put("originalPrice",
									jsonPoweredbyArray.getJSONObject(catCount)
											.getString("salePrice"));

							onlinestoresData.put("shipAmount",
									jsonPoweredbyArray.getJSONObject(catCount)
											.getString("shipAmount"));
							onlinestoresData.put("url", jsonPoweredbyArray
									.getJSONObject(catCount).getString("url"));

							onlinestoresList.add(onlinestoresData);
						}
					}
					result = "true";

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				mDialog.dismiss();
				if ("true".equals(result)) {

                    onlinestoresListAdapter = new RetailerOnlinestoresListAdapter(
                            onlinestoresList, RetailerOnlinestoresActivity.this);
                    onlinestoresListView.setAdapter(onlinestoresListAdapter);
                } else {
                    Toast.makeText(getBaseContext(), "Error Calling WebService",
                            Toast.LENGTH_SHORT).show();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
