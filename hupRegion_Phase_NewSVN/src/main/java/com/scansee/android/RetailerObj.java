package com.scansee.android;

/**
 * Created by subramanya.v on 8/4/2016.
 */
public class RetailerObj {
    private boolean isHeader;
    private String rowNumber;
    private String retailerId;
    private String retailLocationId;
    private String retailerName;
    private String distance;
    private String logoImagePath;
    private String bannerAdImagePath;
    private String ribbonAdImagePath;
    private String ribbonAdURL;
    private String saleFlg;
    private String retailAddress1;
    private String retailAddress2;
    private String city;
    private String state;
    private String postalCode;
    private String sortItemBy;
    private String completeAddress;
    private String locationOpen;

    public String getLocationOpen() {
        return locationOpen;
    }

    public void setLocationOpen(String locationOpen) {
        this.locationOpen = locationOpen;
    }

    public String getCompleteAddress() {
        return completeAddress;
    }

    public void setCompleteAddress(String completeAddress) {
        this.completeAddress = completeAddress;
    }



    public String getSortItemBy() {
        return sortItemBy;
    }

    public void setSortItemBy(String sortItemBy) {
        this.sortItemBy = sortItemBy;
    }




    public String getRetailAddress2() {
        return retailAddress2;
    }

    public void setRetailAddress2(String retailAddress2) {
        this.retailAddress2 = retailAddress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRetailAddress1() {
        return retailAddress1;
    }

    public void setRetailAddress1(String retailAddress1) {
        this.retailAddress1 = retailAddress1;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBannerAdImagePath() {
        return bannerAdImagePath;
    }

    public void setBannerAdImagePath(String bannerAdImagePath) {
        this.bannerAdImagePath = bannerAdImagePath;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setIsHeader(boolean isHeader) {
        this.isHeader = isHeader;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLogoImagePath() {
        return logoImagePath;
    }

    public void setLogoImagePath(String logoImagePath) {
        this.logoImagePath = logoImagePath;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }

    public String getRetailLocationId() {
        return retailLocationId;
    }

    public void setRetailLocationId(String retailLocationId) {
        this.retailLocationId = retailLocationId;
    }

    public String getRetListId() {
        return retListId;
    }

    public void setRetListId(String retListId) {
        this.retListId = retListId;
    }

    public String getRibbonAdImagePath() {
        return ribbonAdImagePath;
    }

    public void setRibbonAdImagePath(String ribbonAdImagePath) {
        this.ribbonAdImagePath = ribbonAdImagePath;
    }

    public String getRibbonAdURL() {
        return ribbonAdURL;
    }

    public void setRibbonAdURL(String ribbonAdURL) {
        this.ribbonAdURL = ribbonAdURL;
    }

    public String getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(String rowNumber) {
        this.rowNumber = rowNumber;
    }

    public String getSaleFlg() {
        return saleFlg;
    }

    public void setSaleFlg(String saleFlg) {
        this.saleFlg = saleFlg;
    }

    private String latitude;
    private String longitude;
    private String retListId;
}

