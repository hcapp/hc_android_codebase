package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.ImageLoaderAsync;

public class HotDealClaimRedeemExpiredListAdapter extends BaseAdapter {
	Activity activity;
	private ArrayList<HashMap<String, String>> hotdealsList;
	private static LayoutInflater inflater = null;
	String itemName = null;
	HashMap<String, String> hotData = null;

	public HotDealClaimRedeemExpiredListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> hotdealsList) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.hotdealsList = hotdealsList;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		return hotdealsList.size();
	}

	@Override
	public Object getItem(int id) {
		return hotdealsList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;

		hotData = hotdealsList.get(position);
		itemName = hotData.get("itemName");
		/***
		 * If the list item reaches to its last position then it will be called
		 * for next pagination
		 ***/
		if (position == hotdealsList.size() - 1) {
			if (!((HotDealClaimRedeemExpiredActivty) activity).isAlreadyLoading) {
				if (((HotDealClaimRedeemExpiredActivty) activity).nextPage) {
					((HotDealClaimRedeemExpiredActivty) activity)
							.viewMoreAction();
				}
			}
		}
		viewHolder = new ViewHolder();
		if ("categoryName".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_hotdeals_categoryname,
					parent,false);

			viewHolder.categoryName = (TextView) view
					.findViewById(R.id.hotdeals_categoryname);
		} else if ("apiPartnerName".equalsIgnoreCase(itemName)) {
			view = inflater.inflate(R.layout.listitem_hotdeals_apipartnername,
					parent,false);
			viewHolder.apiPartnerName = (TextView) view
					.findViewById(R.id.hotdeals_apipartenername);
		} else {
			view = inflater.inflate(R.layout.listitem_hotdeals_details, parent,false);
			viewHolder.hotDealName = (TextView) view
					.findViewById(R.id.hotdeals_details_name);
			viewHolder.hDPrice = (TextView) view
					.findViewById(R.id.hotdeals_details_price);
			viewHolder.hDSalePrice = (TextView) view
					.findViewById(R.id.hotdeals_details_sales_price);
			viewHolder.hotDealImagePath = (ImageView) view
					.findViewById(R.id.hotdeals_details_image);
			view.findViewById(R.id.hotdeal_ribbon_imagepath).setVisibility(
					View.GONE);

		}
		view.setTag(viewHolder);
		if ("categoryName".equalsIgnoreCase(itemName)) {
			if (hotData.containsKey("retName")) {
				viewHolder.categoryName.setText(hotData.get("retName") + "-"
						+ hotData.get("categoryName"));
			} else {
				viewHolder.categoryName.setText(hotData.get("categoryName"));
			}
		} else if ("apiPartnerName".equalsIgnoreCase(itemName)) {
			viewHolder.apiPartnerName.setText(hotData.get("apiPartnerName"));
		}

		else {
			viewHolder.hotDealName.setText(hotData.get("hotDealName"));
			if (!"N/A".equalsIgnoreCase(hotData.get("hDPrice"))) {
				view.findViewById(R.id.hotdeals_details_price).setVisibility(
						View.VISIBLE);
				if (hotData.get("hDExpDate") != null
						&& !"".equals(hotData.get("hDExpDate"))
						&& !"N/A".equalsIgnoreCase(hotData.get("hDExpDate"))) {

					viewHolder.hDPrice.setText(hotData.get("hDDiscountAmount")
							+ " off Expires " + hotData.get("hDExpDate"));
				} else {
					viewHolder.hDPrice.setText(hotData.get("hDDiscountAmount"));
				}
			}
			viewHolder.hDSalePrice.setVisibility(View.GONE);
			viewHolder.hotDealImagePath.setTag(hotData.get("hotDealImagePath"));

			if (viewHolder.hotDealImagePath != null) {

				new ImageLoaderAsync(viewHolder.hotDealImagePath)
						.execute(hotData.get("hotDealImagePath"));
			}

		}
		return view;
	}

	public static class ViewHolder {

		protected TextView city;
		protected TextView apiPartnerName;
		protected TextView categoryName;
		protected TextView hotDealName;
		protected ImageView hotDealImagePath;
		protected TextView hDPrice;
		protected TextView hDSalePrice;

	}

}
