package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityLogger;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class FindGetProductListActivity extends CustomTitleBar {

	private ArrayList<HashMap<String, String>> findsearchlist = null;
	protected ListView findprosearchListView;

	FindSearchProductListListAdapter findproductsearchListAdapter;
	String latitude = null, longitude = null;
	String lastVistedProductNo = "0";
	String searchkey, productId, addTo;
	String parCatId;
	protected HashMap<String, String> productData;
	boolean fromfavorites = false;
	boolean fromlist = false;

	LocationManager locationManager;

	FindLocationCategory findLocAsyncTask;
	AddTo addToAsyncTask;
	AddToFavorites addToFavAsyncTask;

	LinearLayout mainLayout;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			cancelAsyncTasks();
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_product_listview);

		try {
			mainLayout = (LinearLayout) findViewById(R.id.main_layout);
			mainLayout.setVisibility(View.INVISIBLE);

			HubCityLogger.i("Called class is", this.getClass().getSimpleName());
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText(R.string.title_searchresult);

			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			findsearchlist = new ArrayList<>();
			if (getIntent().hasExtra(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME)) {

                searchkey = getIntent().getExtras().getString(
                        CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME);

            }
			if (getIntent().hasExtra("selectedCat")) {
                parCatId = getIntent().getExtras().getString("selectedCat");

            } else {
                parCatId = "0";
            }
			lastVistedProductNo = "0";

			fromfavorites = getIntent().getExtras().getBoolean("Favorites");
			fromlist = getIntent().getExtras().getBoolean("List");

			if (getIntent().hasExtra("findTitleText")) {

                title.setText(R.string.title_searchresult);
            }

			leftTitleImage.setVisibility(View.GONE);

			backImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTasks();
                    finish();

                }
            });

			findprosearchListView = (ListView) findViewById(R.id.find_product_listview_list);
			findprosearchListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {

                    HashMap<String, String> selectedItem = findsearchlist
                            .get(position);
                    String itemName = selectedItem.get("itemName");

                    if ("viewMore".equalsIgnoreCase(itemName)) {
                        findsearchlist.remove(position);
                        lastVistedProductNo = selectedItem
                                .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER);
                        callFindAsyncTask();
                    } else {
                        if (fromfavorites) {
                            productId = selectedItem
                                    .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID);
                            callAddToFavAsyncTask();
                        } else if (fromlist) {
                            productId = selectedItem
                                    .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID);
                            addTo = "List";
                            callAddToAsyncTask();
                        } else {
                            Intent navIntent = new Intent(
                                    FindGetProductListActivity.this,
                                    RetailerCurrentsalesActivity.class);

                            navIntent
                                    .putExtra(
											CommonConstants.TAG_CURRENTSALES_PRODUCTID,
											selectedItem
													.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
                            navIntent
                                    .putExtra(
											CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
											selectedItem
													.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID));
                            navIntent
                                    .putExtra(
											CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
											selectedItem
													.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));

                            navIntent.putExtra("Find", true);
                            startActivity(navIntent);
                        }

                    }
                }
            });
			hideKeyBoard();
			callFindAsyncTask();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void callFindAsyncTask() {
		if (findLocAsyncTask != null) {
			if (!findLocAsyncTask.isCancelled()) {
				findLocAsyncTask.cancel(true);
			}

			findLocAsyncTask = null;
		}

		findLocAsyncTask = new FindLocationCategory();
		findLocAsyncTask.execute();

	}

	private void callAddToAsyncTask() {
		if (addToAsyncTask != null) {
			if (!addToAsyncTask.isCancelled()) {
				addToAsyncTask.cancel(true);
			}

			addToAsyncTask = null;
		}

		addToAsyncTask = new AddTo();
		addToAsyncTask.execute();
	}

	private void callAddToFavAsyncTask() {
		if (addToFavAsyncTask != null) {
			if (!addToFavAsyncTask.isCancelled()) {
				addToFavAsyncTask.cancel(true);
			}

			addToFavAsyncTask = null;
		}

		addToFavAsyncTask = new AddToFavorites();
		addToFavAsyncTask.execute();
	}

	private void cancelAsyncTasks() {

		if (findLocAsyncTask != null && !findLocAsyncTask.isCancelled()) {
			findLocAsyncTask.cancel(true);
		}

		findLocAsyncTask = null;

		if (addToAsyncTask != null && !addToAsyncTask.isCancelled()) {
			addToAsyncTask.cancel(true);
		}

		addToAsyncTask = null;

		if (addToFavAsyncTask != null && !addToFavAsyncTask.isCancelled()) {
			addToFavAsyncTask.cancel(true);
		}

		addToFavAsyncTask = null;
	}

	@Override
	public void onDestroy() {

		if (locationManager != null) {
			locationManager.removeUpdates(new ScanSeeLocListener());
		}

		cancelAsyncTasks();

		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		cancelAsyncTasks();
		finish();
	}

	private void hideKeyBoard() {
		FindGetProductListActivity.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	UrlRequestParams objUrlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	private class FindLocationCategory extends AsyncTask<String, Void, String> {

		JSONObject jsonObject = null;
		JSONObject responseMenuObject = null;
		JSONArray jsonArrayProductDetail = null;
		HashMap<String, String> productData = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			HubCityLogger.d("progress-loadMenu", " pre execute async");
			mDialog = ProgressDialog.show(FindGetProductListActivity.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			String tempLastVisit = "0";
			try {
				String urlParameters = objUrlRequestParams
						.getFindProductSearchList(searchkey, parCatId,
								lastVistedProductNo, Constants.getMainMenuId());
				String get_product_items_list = Properties.url_local_server
						+ Properties.hubciti_version
						+ "scannow/getsmartsearchcount";
				jsonObject = mServerConnections.getUrlPostResponse(
						get_product_items_list, urlParameters, true);

				if (jsonObject != null) {

					try {
						JSONObject productDetail = jsonObject.getJSONObject(
								CommonConstants.FINDPRODUCTSEARCHRESULTSET)
								.getJSONObject(
										CommonConstants.FINDPRODUCTSEARCHMENU);

						jsonArrayProductDetail = new JSONArray();
						jsonArrayProductDetail.put(productDetail);
					} catch (Exception e) {
						e.printStackTrace();
						jsonArrayProductDetail = jsonObject.getJSONObject(
								CommonConstants.FINDPRODUCTSEARCHRESULTSET)
								.getJSONArray(
										CommonConstants.FINDPRODUCTSEARCHMENU);
					}

					for (int arrayCount = 0; arrayCount < jsonArrayProductDetail
							.length(); arrayCount++) {
						productData = new HashMap<>();
						responseMenuObject = jsonArrayProductDetail
								.getJSONObject(arrayCount);

						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTID,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHIMAGEPATH,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHIMAGEPATH));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER));
						tempLastVisit = productData
								.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER);

						findsearchlist.add(productData);
					}
					if ("1".equalsIgnoreCase(jsonObject
							.getJSONObject(
									CommonConstants.FINDPRODUCTSEARCHRESULTSET)
							.getString(
									CommonConstants.FINDPRODUCTSEARCHPRODUCTNEXTPAGE))
							&& productData != null) {
						productData = new HashMap<>();
						productData.put("itemName", "viewMore");
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
										tempLastVisit);
						findsearchlist.add(productData);
					}
					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				HubCityLogger.d("progress-load Menu", " post execute async");

				if ("true".equals(result)) {

                    findproductsearchListAdapter = new FindSearchProductListListAdapter(
                            FindGetProductListActivity.this, findsearchlist);
                    findprosearchListView.setAdapter(findproductsearchListAdapter);
                    Parcelable state = findprosearchListView.onSaveInstanceState();
                    findprosearchListView.setAdapter(findproductsearchListAdapter);
                    findprosearchListView.onRestoreInstanceState(state);
                    findprosearchListView.setSelection(Integer
                            .valueOf(lastVistedProductNo));

                } else {

                    mDialog.dismiss();
                    AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
                            FindGetProductListActivity.this);
                    notificationAlert.setMessage(getString(R.string.norecord))
                            .setPositiveButton(getString(R.string.specials_ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                int id) {
                                            dialog.cancel();
                                        }
                                    });

                    notificationAlert.create().show();

                }

				mainLayout.setVisibility(View.VISIBLE);

				if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

	}

	private class AddTo extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			try {

				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.ADDTOSHOPPINGLISTRESULTSET);

					productData = new HashMap<>();
					productData
							.put(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE,
									String.valueOf(jsonObject
											.getInt(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE)));

					productData
							.put(CommonConstants.ADDTOSHOPPINGLIST,
									jsonObject
											.getString(CommonConstants.ADDTOSHOPPINGLIST));
					productData
							.put(CommonConstants.ADDTOSHOPPINGLISTUSERPRODUCTIDS,
									jsonObject
											.getString(CommonConstants.ADDTOSHOPPINGLISTUSERPRODUCTIDS));

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if ("10000".equalsIgnoreCase(productData
					.get(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE))) {

			} else if ("10010".equalsIgnoreCase(productData
					.get(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE))) {

				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindGetProductListActivity.this);
				notificationAlert.setMessage(
						productData.get(CommonConstants.ADDTOSHOPPINGLIST))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				notificationAlert.create().show();

			} else {
				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindGetProductListActivity.this);
				notificationAlert.setMessage(
						getString(R.string.network_failure)).setPositiveButton(
						getString(R.string.specials_ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

				notificationAlert.create().show();

			}

		}
	}

	private class AddToFavorites extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			try {

				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.ADDTOSLRESULTSET);

					productData = new HashMap<>();
					productData
							.put(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE,
									String.valueOf(jsonObject
											.getInt(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE)));
					productData.put(CommonConstants.ADDTOSL,
							jsonObject.getString(CommonConstants.ADDTOSL));

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if ("10000".equalsIgnoreCase(productData
					.get(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE))) {

			} else if ("10010".equalsIgnoreCase(productData
					.get(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE))) {
				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindGetProductListActivity.this);
				notificationAlert.setMessage(
						productData.get(CommonConstants.ADDTOSHOPPINGLIST))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				notificationAlert.create().show();
			} else {
				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindGetProductListActivity.this);
				notificationAlert.setMessage(
						getString(R.string.network_failure)).setPositiveButton(
						getString(R.string.specials_ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
				notificationAlert.create().show();
			}
		}
	}

}
