package com.scansee.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.ProductCLRInfoActivity;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RetailerCurrentsalesActivity extends CustomTitleBar implements
		OnClickListener {
	String userId, retailID, retListId, postalcode, retailerName,
			retailerSaleprice, retailerRegularPrice, retailerAd, retailerAdURL,
			productId, productListID, modelNumber, productNametext, pname,
			userProductId, addTo;
	HashMap<String, String> productDetailData = null, onlineStoresData = null,
			nearByData = null, reviewsData = null, loginData = null,
			discountData = null;
	HashMap<String, String> historyData = null;
	TextView retailerNameTextView, retailerSalePriceTextView;
	ImageView retailerNameImageView;
	protected int position;
	String latitude;
	String longitude;
	LocationManager locationManager;
	ProgressBar locationProgress;
	Context context = this;
	String retailLocationID = null;
	AlertDialog.Builder specialsAlert;
	protected CustomImageLoader customImageLoader;
	LinearLayout productDetailLayout, onlineStoresLayout, nearbyLayout,
			reviewsLayout, discountLayout;
	TextView productName, productDesc, nearbyCount, nearbyPrice,
			onlineStoreCount, onlineStorePrice, reviewcount, ratingCount,
			discountCount;
	RatingBar reviewRating;
	ImageView productImage, nearbyImage, onlineStoreImage, reviewImage,
			discountImage, discountcouponImage, discountloyaltyImage,
			discountrebateImage;
	Activity activity;
	Intent navIntent = null;
	boolean fromFind = false;
	boolean fromCoupons = false;
	boolean fromScanNow = false;
	boolean fromShoppingList = false;
	boolean fromWishList = false;
	boolean fromFavorites = false;
	ArrayList<RetailerBo> mRetailerList = new ArrayList<>();

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.retailer_currentsales_listview);
			activity = this;
			// Comment
			SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);

			try {
                postalcode = settings.getString("ZipCode", Constants.getZipCode());
                if (!(postalcode.length() > 0)) {
                    postalcode = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

			productNametext = getIntent().getExtras().getString(
                    CommonConstants.TAG_CURRENTSALES_PRODUCTNAME);
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText(productNametext);

			leftTitleImage.setVisibility(View.GONE);

			retailerAd = getIntent().getExtras().getString(
                    CommonConstants.TAG_RIBBON_ADIMAGE_PATH);
			retailerAdURL = getIntent().getExtras().getString(
                    CommonConstants.TAG_RIBBON_AD_URL);
			retailID = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAIL_ID);
			retailLocationID = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAILE_LOCATIONID);
			retListId = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAIL_LIST_ID);
			productId = getIntent().getExtras().getString(
                    CommonConstants.TAG_CURRENTSALES_PRODUCTID);
			productListID = getIntent().getExtras().getString(
                    CommonConstants.TAG_CURRENTSALES_RODUCTLISTID);
			retailerName = getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAILER_NAME);
			retailerSaleprice = getIntent().getExtras().getString(
                    CommonConstants.TAG_CURRENTSALES_SALEPRICE);
			retailerRegularPrice = getIntent().getExtras().getString(
                    CommonConstants.TAG_CURRENTSALES_REGULARPRICE);

			if (getIntent().getExtras().containsKey(
                    CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER)) {
                modelNumber = getIntent().getExtras().getString(
                        CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER);
            }

			fromFind = getIntent().getExtras().getBoolean("Find");
			fromCoupons = getIntent().getExtras().getBoolean("Coupons");
			fromScanNow = getIntent().getExtras().getBoolean("ScanNow");
			fromShoppingList = getIntent().getExtras().getBoolean("ShoppingList");
			fromWishList = getIntent().getExtras().getBoolean("Wishlist");
			fromFavorites = getIntent().getExtras().getBoolean("Favorites");

			if (fromFind) {
                findViewById(R.id.retailer_currentsales_layout).setVisibility(
                        View.GONE);
                findViewById(R.id.retailer_currentsales_saleprice_layout)
                        .setVisibility(View.GONE);
            }
			if (fromCoupons) {
                findViewById(R.id.retailer_currentsales_layout).setVisibility(
                        View.GONE);
                findViewById(R.id.retailer_currentsales_saleprice_layout)
                        .setVisibility(View.GONE);
            }

			if (fromScanNow) {
                findViewById(R.id.retailer_currentsales_layout).setVisibility(
                        View.GONE);
                findViewById(R.id.retailer_currentsales_saleprice_layout)
                        .setVisibility(View.GONE);
            }

			if (fromWishList) {
                findViewById(R.id.retailer_currentsales_layout).setVisibility(
                        View.GONE);
                findViewById(R.id.retailer_currentsales_saleprice_layout)
                        .setVisibility(View.GONE);
                postalcode = Constants.getZipCode();

            }

			if (null == retailerAd) {
                retailerAd = "N/A";
            }
			if (null == retailerAdURL) {
                retailerAdURL = "N/A";
            }
			if (null == retailerName) {
                retailerName = "N/A";
            }
			if (null == retailerSaleprice) {
                retailerSaleprice = "N/A";
            }
			if (null == retailerRegularPrice) {
                retailerRegularPrice = "N/A";
            }

			customImageLoader = new CustomImageLoader(getApplicationContext(), false);
			retailerNameTextView = (TextView) findViewById(R.id.retailer_currentsales_name);
			retailerSalePriceTextView = (TextView) findViewById(R.id.retailer_currentsales_saleprice);
			retailerNameImageView = (ImageView) findViewById(R.id.retailer_currentsales_image);

			retailerNameTextView.setText(retailerName);
			retailerNameImageView.setTag(retailerAd);
			retailerNameImageView.setOnClickListener(this);
			retailerSalePriceTextView.setText(retailerSaleprice);
			customImageLoader.displayImage(retailerAd, this, retailerNameImageView);

			if (retailerAd != null && "N/A".equalsIgnoreCase(retailerAd)) {
                retailerNameImageView.setVisibility(View.GONE);
                retailerNameTextView.setVisibility(View.VISIBLE);
            } else {
                retailerNameImageView.setVisibility(View.VISIBLE);
                retailerNameTextView.setVisibility(View.GONE);
            }
			// productdetails ids
			productName = (TextView) findViewById(R.id.retailer_currentsales_list_name);
			productDesc = (TextView) findViewById(R.id.retailer_currentsales_list_weight);
			productImage = (ImageView) findViewById(R.id.retailer_currentsales_list_image);
			// nearby ids
			nearbyCount = (TextView) findViewById(R.id.retailer_currentsales_list_nearby_stores);
			nearbyPrice = (TextView) findViewById(R.id.retailer_currentsales_list_nearby_price);
			nearbyImage = (ImageView) findViewById(R.id.retailer_currentsales_list_nearby_image);
			// discount ids

			discountCount = (TextView) findViewById(R.id.retailer_currentsales_discount_count);
			discountImage = (ImageView) findViewById(R.id.retailer_currentsales_discount_image);
			discountcouponImage = (ImageView) findViewById(R.id.c_image);
			discountloyaltyImage = (ImageView) findViewById(R.id.l_image);
			discountrebateImage = (ImageView) findViewById(R.id.r_image);

			// onlinestores ids
			onlineStoreCount = (TextView) findViewById(R.id.retailer_currentsales_onlinestores_count);
			onlineStorePrice = (TextView) findViewById(R.id.retailer_currentsales_onlinestores_price);
			onlineStoreImage = (ImageView) findViewById(R.id.retailer_currentsales_onlinestores_image);
			// review ids
			reviewcount = (TextView) findViewById(R.id.retailer_currentsales_review_count);
			ratingCount = (TextView) findViewById(R.id.retailer_currentsales_rating_count);
			reviewImage = (ImageView) findViewById(R.id.retailer_currentsales_review_image);
			// layout ids
			productDetailLayout = (LinearLayout) findViewById(R.id.retailer_item_productdetail);
			onlineStoresLayout = (LinearLayout) findViewById(R.id.retailer_item_onlinestores);
			discountLayout = (LinearLayout) findViewById(R.id.retailer_item_discount);
			nearbyLayout = (LinearLayout) findViewById(R.id.retailer_item_nearby);
			reviewRating = (RatingBar) findViewById(R.id.retailer_currentsales_rating);
			reviewsLayout = (LinearLayout) findViewById(R.id.retailer_item_review);
			// set onclick listener
			productDetailLayout.setOnClickListener(this);
			onlineStoresLayout.setOnClickListener(this);
			nearbyLayout.setOnClickListener(this);
			reviewsLayout.setOnClickListener(this);
			discountLayout.setOnClickListener(this);
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			String provider = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			if (provider.contains("gps")) {

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                        CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                        new ScanSeeLocListener());
                Location locNew = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (locNew != null) {

                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());

                } else {
                    // N/W Tower Info Start
                    locNew = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (locNew != null) {
                        latitude = String.valueOf(locNew.getLatitude());
                        longitude = String.valueOf(locNew.getLongitude());
                    } else {
                        latitude = CommonConstants.LATITUDE;
                        longitude = CommonConstants.LONGITUDE;
                    }
                }

                // When Lat and long is sent we will get the zipcode so iam
                // commenting it because we will send only lat and long in this

                new GetCurrentsalesInfo().execute();

            } else {
                new GetCurrentsalesInfo().execute();

            }

			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText(productNametext);

			leftTitleImage.setVisibility(View.GONE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void finish() {
		super.finish();
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.retailer_item_productdetail:
			retailerItemProductDetail();
			break;

		case R.id.retailer_item_nearby:
			retailerItemNearby();
			break;

		case R.id.retailer_currentsales_image:
			retailerCurrentSalesImage();
			break;

		case R.id.retailer_item_onlinestores:
			retailerItemOnlineStores();
			break;

		case R.id.retailer_item_discount:
			retailerItemDiscount();
			break;

		case R.id.retailer_item_review:
			break;

		default:
			break;
		}

	}

	private void retailerItemDiscount() {
		navIntent = new Intent(RetailerCurrentsalesActivity.this,
				ProductCLRInfoActivity.class);
		navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_PRODUCTID,
				productId);
		navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
				productListID);
		navIntent.putExtra(CommonConstants.TAG_RETAILER_NAME, productNametext);
		startActivityForResult(navIntent, Constants.STARTVALUE);
	}

	private void retailerItemOnlineStores() {
		navIntent = new Intent(RetailerCurrentsalesActivity.this,
				RetailerOnlinestoresActivity.class);
		navIntent.putExtra(CommonConstants.TAG_RETAILE_ID, retailID);
		navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_PRODUCTID,
				productId);
		navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
				productListID);
		startActivityForResult(navIntent, Constants.STARTVALUE);
	}

	private void retailerCurrentSalesImage() {
		if (retailerNameImageView != null && retailerAdURL != null) {
			Intent retailerLink = new Intent(RetailerCurrentsalesActivity.this,
					ScanseeBrowserActivity.class);
			retailerLink.putExtra(CommonConstants.URL, retailerAdURL);
			startActivityForResult(retailerLink, Constants.STARTVALUE);
		}
	}

	private void retailerItemNearby() {
		navIntent = new Intent(RetailerCurrentsalesActivity.this,
				RetailerNearbyActivity.class);

		navIntent.putParcelableArrayListExtra("mRetailerList", mRetailerList);
		navIntent.putExtra(CommonConstants.TAG_RETAIL_LIST_ID, retListId);
		startActivityForResult(navIntent, Constants.STARTVALUE);
	}

	private void retailerItemProductDetail() {
		navIntent = new Intent(RetailerCurrentsalesActivity.this,
				RetailerProductActivity.class);
		productDetailData.put(CommonConstants.TAG_CURRENTSALES_REGULARPRICE,
				retailerRegularPrice);
		productDetailData.put(CommonConstants.TAG_CURRENTSALES_SALEPRICE,
				retailerSaleprice);
		productDetailData.put(CommonConstants.TAG_CURRENTSALES_PRODUCTID,
				productId);
		productDetailData.put(CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
				productListID);
		productDetailData.put(
				CommonConstants.TAG_CURRENTSALES_PRODUCTMODELNUMBER,
				modelNumber);
		productDetailData.put(CommonConstants.TAG_CURRENTSALES_PRODUCTID,
				productId);
		navIntent.putExtra("prodDetails", productDetailData);
		startActivityForResult(navIntent, Constants.STARTVALUE);
	}

	UrlRequestParams mUrlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	UrlRequestParams objUrlRequestParams = new UrlRequestParams();

	private class GetCurrentsalesInfo extends AsyncTask<String, Void, String> {
		JSONObject jsonObject, productDetail = null, nearbyDetail = null,
				onlinestoresDetail = null, reviewDetail = null,
				discountDetail = null;
		private ProgressDialog mDialog;
		JSONArray jsonArray = null;

		@Override
		protected void onPreExecute() {

			mDialog = ProgressDialog.show(RetailerCurrentsalesActivity.this,
					"", Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {
				String get_product_summary = Properties.url_local_server
						+ Properties.hubciti_version
						+ "find/getproductsummary";
				
				if ((latitude != null) && (longitude != null)) {
					postalcode = null;
					String urlParameters = objUrlRequestParams
							.getRetailersSummary(productId, productListID, "0",
									postalcode, latitude, longitude, retailID,
									retailLocationID);
					jsonObject = mServerConnections.getUrlPostResponse(
							get_product_summary, urlParameters, true);
				} else {
					String urlParameters = objUrlRequestParams
							.getRetailersSummary(productId, productListID, "0",
									postalcode, latitude, longitude, retailID,
									retailLocationID);
					jsonObject = mServerConnections.getUrlPostResponse(
							get_product_summary, urlParameters, true);
				}
				if (jsonObject != null) {
					try {
						productDetail = jsonObject.getJSONObject(
								"ProductSummary")
								.getJSONObject("ProductDetail");
						productDetailData = new HashMap<>();
						productDetailData
								.put(CommonConstants.TAG_IMAGEPATH,
										productDetail
												.getString(CommonConstants.TAG_IMAGEPATH));
						productDetailData
								.put(CommonConstants.TAG_PRODUCTNAME,
										productDetail
												.getString(CommonConstants.TAG_PRODUCTNAME));
						productDetailData
								.put(CommonConstants.TAG_PRODUCTSHORTDESCRIPTION,
										productDetail
												.getString(CommonConstants.TAG_PRODUCTSHORTDESCRIPTION));
						productDetailData
								.put(CommonConstants.TAG_PRODUCTLONGDESCRIPTION,
										productDetail
												.getString(CommonConstants.TAG_PRODUCTLONGDESCRIPTION));

					} catch (Exception e) {
						e.printStackTrace();
						productDetailData = null;
					}

					try {
						nearbyDetail = jsonObject.getJSONObject(
								"ProductSummary").getJSONObject(
								"FindNearByDetailsResultSet");
						nearByData = new HashMap<>();
						nearByData.put("stockImagePath",
								nearbyDetail.getString("stockImagePath"));
						nearByData.put("totalLocations",
								nearbyDetail
										.getJSONObject("findNearByMetaData")
										.getString("totalLocations"));
						nearByData.put("lowestPrice",
								nearbyDetail
										.getJSONObject("findNearByMetaData")
										.getString("lowestPrice"));
						if (null != nearbyDetail) {
							jsonArray = nearbyDetail
									.getJSONArray("FindNearByDetails");

							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject jsonObject = jsonArray
										.getJSONObject(i);
								String ribbonAdImagePath = null;
								String ribbonAdURL = null;
								String bannerAd = null;
								String locationID = null;
								String poweredby = null;
								String retailerName = jsonObject
										.getString(CommonConstants.TAG_FINDLOCATION_RETAILERNAME);
								String retailAddress = jsonObject
										.getString("address");
								String latitude = jsonObject
										.getString("latitude");
								String longitude = jsonObject
										.getString("longitude");
								String retailId = jsonObject
										.getString("retailerId");
								if (jsonObject
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH)) {
									ribbonAdImagePath = jsonObject
											.getString(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH);
								}
								if (jsonObject
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL)) {
									ribbonAdURL = jsonObject
											.getString(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL);
								}

								if (jsonObject
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH)) {
									bannerAd = jsonObject
											.getString(CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH);
								}
								if (jsonObject.has("retLocId")) {
									locationID = jsonObject
											.getString("retLocId");
								}
								if (jsonObject.has("poweredby")) {
									poweredby = jsonObject
											.getString("poweredby");
								}

								String retailerUrl = jsonObject
										.getString("retailerUrl");
								String productPrice = jsonObject
										.getString("productPrice");
								String phone = jsonObject.getString("phone");
								String distance = jsonObject
										.getString("distance");

								RetailerBo retailerObject = new RetailerBo(
										null, retailId, retailerName,
										locationID, distance, retailAddress,
										null, bannerAd, ribbonAdImagePath,
										ribbonAdURL, null, latitude, longitude,
										null, retailerUrl, productPrice, phone,
										poweredby);
								mRetailerList.add(retailerObject);

							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						try {
							if (null != nearbyDetail) {
								JSONObject jsonObject = nearbyDetail
										.getJSONObject("FindNearByDetails");
								String ribbonAdImagePath = null;
								String ribbonAdURL = null;
								String bannerAd = null;
								String locationID = null;
								String retailerName = jsonObject
										.getString(CommonConstants.TAG_FINDLOCATION_RETAILERNAME);
								String retailAddress = jsonObject
										.getString("address");
								String latitude = jsonObject
										.getString("latitude");
								String longitude = jsonObject
										.getString("longitude");
								String retailId = jsonObject
										.getString("retailerId");
								if (jsonObject
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH)) {
									ribbonAdImagePath = jsonObject
											.getString(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH);
								}
								if (jsonObject
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL)) {
									ribbonAdURL = jsonObject
											.getString(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL);
								}

								if (jsonObject
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH)) {
									bannerAd = jsonObject
											.getString(CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH);
								}
								if (jsonObject.has("retLocId")) {
									locationID = jsonObject
											.getString("retLocId");
								}

								String retailerUrl = "", productPrice = "", phone = "", distance = "", poweredby = "";

								if (jsonObject.has("retailerUrl")) {
									retailerUrl = jsonObject
											.getString("retailerUrl");
								}

								if (jsonObject.has("productPrice")) {
									productPrice = jsonObject
											.getString("productPrice");
								}

								if (jsonObject.has("phone")) {
									phone = jsonObject.getString("phone");
								}

								if (jsonObject.has("distance")) {
									distance = jsonObject.getString("distance");
								}

								if (jsonObject.has("poweredby")) {
									poweredby = jsonObject
											.getString("poweredby");
								}

								RetailerBo retailerObject = new RetailerBo(
										null, retailId, retailerName,
										locationID, distance, retailAddress,
										null, bannerAd, ribbonAdImagePath,
										ribbonAdURL, null, latitude, longitude,
										null, retailerUrl, productPrice, phone,
										poweredby);
								mRetailerList.add(retailerObject);
							}
						} catch (Exception exception) {
							exception.printStackTrace();
							nearbyDetail = null;
						}

					}

					try {
						discountDetail = jsonObject.getJSONObject(
								"ProductSummary").getJSONObject("CLRInfo");
						discountData = new HashMap<>();
						discountData.put("stockImagePath",
								discountDetail.getString("stockImagePath"));
						discountData.put("stockHeader",
								discountDetail.getString("stockHeader"));
						discountData.put("couponFlag",
								discountDetail.getString("couponFlag"));
						discountData.put("rebateFlag",
								discountDetail.getString("rebateFlag"));
						discountData.put("loyaltyFlag",
								discountDetail.getString("loyaltyFlag"));

					} catch (Exception e) {
						e.printStackTrace();
						discountData = null;
					}

					try {
						onlinestoresDetail = jsonObject.getJSONObject(
								"ProductSummary").getJSONObject("OnlineStores");
						onlineStoresData = new HashMap<>();
						onlineStoresData.put("stockImagePath",
								onlinestoresDetail.getString("stockImagePath"));
						onlineStoresData.put("minPrice", onlinestoresDetail
								.getJSONObject("onlineStoresMetaData")
								.getString("minPrice"));
						onlineStoresData.put("totalStores", onlinestoresDetail
								.getJSONObject("onlineStoresMetaData")
								.getString("totalStores")
								+ " Stores");

					} catch (Exception e) {
						e.printStackTrace();
						onlineStoresData = null;
					}

					try {
						reviewDetail = jsonObject.getJSONObject(
								"ProductSummary").getJSONObject(
								"ProductRatingReview");
						reviewsData = new HashMap<>();
						reviewsData.put("stockImagePath",
								reviewDetail.getString("stockImagePath"));
						reviewsData.put("totalReviews",
								reviewDetail.getString("totalReviews"));
						reviewsData.put("avgRating",
								reviewDetail.getJSONObject("userRatingInfo")
										.getString("avgRating"));
						reviewsData.put("currentRating",
								reviewDetail.getJSONObject("userRatingInfo")
										.getString("currentRating"));
						reviewsData.put("productId",
								reviewDetail.getJSONObject("userRatingInfo")
										.getString("productId"));
						reviewsData.put("noOfUsersRated",
								reviewDetail.getJSONObject("userRatingInfo")
										.getString("noOfUsersRated")
										+ " Ratings");

					} catch (Exception e) {
						e.printStackTrace();
						reviewsData = null;
					}

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				mDialog.dismiss();
				if ("true".equals(result)) {
                    if (productDetailData != null) {
                        productName.setText(productDetailData
                                .get(CommonConstants.TAG_PRODUCTNAME));
                        productDesc.setText(productDetailData
                                .get(CommonConstants.TAG_PRODUCTSHORTDESCRIPTION));
                        productImage.setTag(productDetailData
                                .get(CommonConstants.TAG_IMAGEPATH));
                        customImageLoader.displayImage(productDetailData
                                .get(CommonConstants.TAG_IMAGEPATH), activity,
                                productImage);
                        productDetailLayout.setVisibility(View.VISIBLE);
                    }
                    if (nearByData != null) {

                        nearbyCount.setText(nearByData.get("totalLocations"));
                        if (nearByData.get("lowestPrice") != null
                                && !"N/A".equalsIgnoreCase(nearByData
                                        .get("lowestPrice"))) {
                            nearbyPrice.setText(nearByData.get("lowestPrice"));
                        } else {
                            nearbyPrice.setText("");
                        }
                        nearbyImage.setTag(nearByData.get("stockImagePath"));
                        customImageLoader.displayImage(nearByData.get("stockImagePath"),
                                activity, nearbyImage);

                        nearbyLayout.setVisibility(View.VISIBLE);
                    }

                    if (discountData != null) {

                        discountCount.setText(discountData.get("stockHeader"));
                        discountImage.setTag(discountData.get("stockImagePath"));
                        customImageLoader.displayImage(
                                discountData.get("stockImagePath"), activity,
                                discountImage);
                        if ("Green"
                                .equalsIgnoreCase(discountData.get("couponFlag"))) {
                            discountcouponImage
                                    .setImageResource(R.drawable.c_green);
                            discountData.put("type", "coupon");
                        } else if ("Red".equalsIgnoreCase(discountData
                                .get("couponFlag"))) {
                            discountcouponImage.setImageResource(R.drawable.c_red);
                            discountData.put("type", "coupon");
                        } else if ("Grey".equalsIgnoreCase(discountData
                                .get("couponFlag"))) {
                            discountcouponImage.setImageResource(R.drawable.c_grey);
                        }

                        if ("Green"
                                .equalsIgnoreCase(discountData.get("rebateFlag"))) {
                            discountrebateImage
                                    .setImageResource(R.drawable.r_green);
                            discountData.put("type", "rebate");
                        } else if ("Red".equalsIgnoreCase(discountData
                                .get("rebateFlag"))) {
                            discountrebateImage.setImageResource(R.drawable.r_red);
                            discountData.put("type", "rebate");
                        } else if ("Grey".equalsIgnoreCase(discountData
                                .get("rebateFlag"))) {
                            discountrebateImage.setImageResource(R.drawable.r_grey);
                        }

                        if ("Green".equalsIgnoreCase(discountData
                                .get("loyaltyFlag"))) {
                            discountloyaltyImage
                                    .setImageResource(R.drawable.l_green);
                            discountData.put("type", "loyalty");
                        } else if ("Red".equalsIgnoreCase(discountData
                                .get("loyaltyFlag"))) {
                            discountloyaltyImage.setImageResource(R.drawable.l_red);
                            discountData.put("type", "loyalty");
                        } else if ("Grey".equalsIgnoreCase(discountData
                                .get("loyaltyFlag"))) {
                            discountloyaltyImage
                                    .setImageResource(R.drawable.l_grey);
                        }
                        discountLayout.setVisibility(View.VISIBLE);
                    }

                    if (onlineStoresData != null) {
                        onlineStoreCount.setText(onlineStoresData
                                .get("totalStores"));
                        onlineStorePrice.setText(onlineStoresData.get("minPrice"));
                        onlineStoreImage.setTag(onlineStoresData
                                .get("stockImagePath"));
                        customImageLoader.displayImage(
                                onlineStoresData.get("stockImagePath"), activity,
                                onlineStoreImage);
                        onlineStoresLayout.setVisibility(View.VISIBLE);
                    }
                    if (reviewsData != null) {
                        reviewcount.setText(reviewsData.get("totalReviews"));
                        ratingCount.setText(reviewsData.get("noOfUsersRated"));
                        reviewRating.setRating(Float.valueOf(reviewsData
                                .get("avgRating")));
                        reviewImage.setTag(reviewsData.get("stockImagePath"));
                        customImageLoader.displayImage(reviewsData.get("stockImagePath"),
                                activity, reviewImage);
                        reviewsLayout.setVisibility(View.VISIBLE);
                    }

                } else {
                    Toast.makeText(getBaseContext(), "Error Calling WebService",
                            Toast.LENGTH_SHORT).show();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
