package com.scansee.android;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.hubcity.android.commonUtil.CommonConstants;

public class ScanSeeLocListener implements LocationListener {
	double latitude, longitude;

	@Override
	public void onLocationChanged(Location location) {
		latitude = location.getLatitude();
		longitude = location.getLongitude();

		CommonConstants.LATITUDE = location.getLatitude() + "";
		CommonConstants.LONGITUDE = location.getLongitude() + "";

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
