package com.scansee.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;

import com.com.hubcity.model.MyCouponModel;
import com.com.hubcity.model.MyCouponObj;
import com.com.hubcity.model.RetailerDetails;
import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by subramanya.v on 12/9/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class MyCouponsListingActivity extends CustomTitleBar {
    private ListView couponListView;
    private Activity mActivity;
    private int screenHeight;
    private String listTitle;
    private SearchView svSearch;
    private RestClient mRestClient;
    private String sortColumn = "distance";
    private String lastVisitedNo = "0";
    private int bottomButtonHeight;
    private BottomButtons bb;
    private LinearLayout bottomLayout;
    private LinkedList<RetailerDetails> listItem = new LinkedList<>();
    private LinkedList<MyCouponObj> listMainItem;
    private boolean isFirst = true;
    private boolean hasBottomButtons;
    private MyCouponListingAdapter couponAdapter;
    private boolean isPaginationEnd = true;
    private View moreResultsView;
    public boolean isLoaded = false;
    private String latitude;
    private String longitude;
    private String searchKey;
    private String catIds;
    private String cityIds;
    private HubCityContext mHubCiti;
    private boolean isRefresh;
    private String radius;
    private CustomNavigation customNaviagation;
    private boolean isClaimed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupon_listing);
        try {
            CommonConstants.hamburgerIsFirst = true;
            mActivity = MyCouponsListingActivity.this;
            getIntentValue();
            bindView();
            getValue();
            title.setText(listTitle);
            CommonConstants.traverseSearchView(svSearch, mActivity);
            initializeBottomButton();



            HashMap<String, String> gpsValue = CommonConstants.getGpsValue(mActivity);
            latitude = gpsValue.get("latitude");
            longitude = gpsValue.get("longitude");

            setClickListener();

            isRefresh = true;
            mHubCiti = (HubCityContext) getApplicationContext();
            mHubCiti.setCancelled(false);

            mRestClient = RestClient.getInstance();
            ViewTreeObserver observer = couponListView.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    ViewTreeObserver obs = couponListView.getViewTreeObserver();
                    screenHeight = couponListView.getMeasuredHeight();
                    callService();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)

                    {
                        obs.removeOnGlobalLayoutListener(this);
                    } else

                    {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void hideProgressBar(ProgressDialog progDialog) {
        if (progDialog != null && progDialog.isShowing()) {
            progDialog.dismiss();
        }
    }

    private void callService() {
        ProgressDialog progDialog = null;
        if (isFirst) {
            listItem = new LinkedList<>();
            progDialog = new ProgressDialog(mActivity);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(mActivity.getString(R.string.progress_message));
            progDialog.setCancelable(false);
            progDialog.show();
        }
        if (latitude != null && latitude.isEmpty()) {
            latitude = null;
        }
        if (longitude != null && longitude.isEmpty()) {
            longitude = null;
        }
        String sortOrder = "asc";
        MyCouponModel request = UrlRequestParams.sendMyCoupon(sortColumn, listTitle, lastVisitedNo, sortOrder, searchKey, latitude, longitude, catIds, cityIds);
        final ProgressDialog finalProgDialog = progDialog;
        mRestClient.getMyCoupon(request, new Callback<MyCouponObj>() {


            @Override
            public void success(final MyCouponObj myCouponObj, Response response) {
                String responseText = myCouponObj.getResponseText();
                if (!responseText.equalsIgnoreCase(mActivity.getString(R.string.success)) && responseText != null) {
                    CommonConstants.displayToast(mActivity, responseText);
                }

                if (myCouponObj != null  ) {
                    listMainItem = new LinkedList<>();
                    listMainItem.add(myCouponObj);
                    if(myCouponObj.getCouponList() != null) {
                        listItem.addAll(myCouponObj.getCouponList().get(0).getList());
                    }

                    if (myCouponObj.getNextPage() == 0) {
                        loadListView();

                        //removing footer view
                        couponListView.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);

                    } else {
                        loadListView();

                        //adding footer view
                        couponListView.removeFooterView(moreResultsView);
                        couponListView.addFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.VISIBLE);
                    }


                }
                hideProgressBar(finalProgDialog);
                isFirst = false;
                isLoaded = false;

            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressBar(finalProgDialog);
                if (error.getResponse() != null) {
                    CommonConstants.displayToast(mActivity, error
                            .getResponse().getReason());
                }
            }
        });


    }

    private void loadListView() {
        MyCouponObj listMainItemData = listMainItem.get(0);
        ArrayList<BottomButtonBO> bottomButtonList = listMainItemData.getBottomBtnList();
        if (bottomButtonList != null && bottomButtonList
                .size() != 0) {
            BottomButtonListSingleton
                    .clearBottomButtonListSingleton();
            BottomButtonListSingleton
                    .getListBottomButton(bottomButtonList);
            hasBottomButtons = true;
        }
        if (hasBottomButtons && isFirst) {
            bb.createbottomButtontTab(bottomLayout, false);
            bottomLayout.setVisibility(View.VISIBLE);
        }
        if (bottomButtonHeight == 0 && bottomButtonList != null && bottomButtonList
                .size() != 0) {
            ViewTreeObserver observer1 = bottomLayout.getViewTreeObserver();
            observer1.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    ViewTreeObserver obs = bottomLayout.getViewTreeObserver();
                    bottomButtonHeight = bottomLayout.getMeasuredHeight();
                    LoadListView();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)

                    {
                        obs.removeOnGlobalLayoutListener(this);
                    } else

                    {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }

                }
            });

        } else {
            LoadListView();
        }


    }

    private void LoadListView() {
        if (isPaginationEnd) {
            couponAdapter = new MyCouponListingAdapter(mActivity, (screenHeight - bottomButtonHeight) / 2, listItem, listMainItem);
            couponListView.setAdapter(couponAdapter);
            isPaginationEnd = false;
        } else {
            couponAdapter.updateList(listItem, listMainItem);
            couponAdapter.notifyDataSetChanged();
        }
    }

    private void initializeBottomButton() {
        try {
            //noinspection ConstantConditions
            bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
            bb.setActivityInfo(this, "CouponsListingActivity");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickListener() {
        couponListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent screen = new Intent(mActivity, CouponsDetailActivity.class);
                screen.putExtra("couponId", String.valueOf(listItem.get(position).getCouponId()));
                screen.putExtra("couponListId", listItem.get(position).getCouponListId());
                screen.putExtra("isCoupon", true);
                isClaimed = true;
                startActivity(screen);
            }
        });
        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override

            public boolean onQueryTextSubmit(String query) {
                searchKey = query;
                if (bb != null) {
                    bb.setOptionsValues(getListValues(), null);
                }
                isFirst = true;
                isPaginationEnd = true;
                lastVisitedNo = "0";
                callService();
                svSearch.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0 || newText.length() >= 3) {
                    isFirst = false;
                    searchKey = null;
                    isPaginationEnd = true;
                    lastVisitedNo = "0";
                    listItem = new LinkedList<>();

                    if(newText.length() >= 3)
                    {
                        searchKey = newText;
                    }
                    if (bb != null) {
                        bb.setOptionsValues(getListValues(), null);
                    }
                    callService();
                }

                return true;
            }
        });
    }

    private void getValue() {
        radius = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).getString
                ("radious", "");


    }

    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();

        if (radius != null && !radius.equals(""))
            values.put("radius", radius);
        else
            values.put("radius", "50");
        values.put("latitude", String.valueOf(latitude));

        values.put("longitude", String.valueOf(longitude));
        String className = "myaccounts";
        values.put("Class", className);
        values.put("srchKey", searchKey);
        return values;
    }

    private void getIntentValue() {
        Intent intent = getIntent();
        if (intent.hasExtra("title")) {
            listTitle = intent.getExtras().getString("title");
        }
        if(intent.hasExtra("searchKey"))
        {
            searchKey = intent.getExtras().getString("searchKey");
        }

        if(intent.hasExtra("catIds"))
        {
            catIds = intent.getExtras().getString("catIds");
        }
        if(intent.hasExtra("sortColumn"))
        {
            sortColumn = intent.getExtras().getString("sortColumn");
        }
        if(intent.hasExtra("cityIds"))
        {
            cityIds = intent.getExtras().getString("cityIds");
        }

    }

    private void bindView() {
        moreResultsView = getLayoutInflater().inflate(
                R.layout.pagination, couponListView, true);
        couponListView = (ListView) findViewById(R.id.coupon_listview);
        svSearch = (SearchView) findViewById(R.id.search);
        bottomLayout = (LinearLayout) findViewById(R.id.bottom_bar);
        bottomLayout.setVisibility(View.INVISIBLE);
        //user for hamburger in modules
        drawerIcon.setVisibility(View.VISIBLE);
        leftTitleImage.setVisibility(View.GONE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
        svSearch.setQuery(searchKey,true);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);

        }

        if (resultCode == 30001 || resultCode == 2) {
            isRefresh = false;

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRefresh = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isClaimed)
        {
            isFirst = true;
            isPaginationEnd = true;
            lastVisitedNo = "0";
            callService();
        }
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }
        if (null != bb) {
            bb.setActivityInfo(this, "CouponsListingActivity");
        }
        if (bb != null) {
            bb.setOptionsValues(getListValues(), null);
        }
        if (mHubCiti.isCancelled()) {
            return;
        }
        if (!isRefresh) {
            HashMap<String, String> resultSet = mHubCiti.getFilterValues();
            if (resultSet != null && !resultSet.isEmpty()) {
                if (resultSet.containsKey("savedSubCatIds")) {
                    catIds = resultSet.get("savedSubCatIds");
                }
                if (resultSet.containsKey("SortBy")) {
                    sortColumn = resultSet.get("SortBy");
                }
                if (resultSet.containsKey("savedCityIds")) {
                    if (resultSet.get("savedCityIds") != null) {
                        cityIds = resultSet.get("savedCityIds");
                        cityIds = cityIds.replace("All,", "");
                    }
                }
            }
            isFirst = true;
            isPaginationEnd = true;
            lastVisitedNo = "0";
            if (catIds != null && catIds.isEmpty()) {
                catIds = null;
            }
            if (cityIds != null && cityIds.isEmpty()) {
                cityIds = null;
            }
            callService();
        }

        isRefresh = false;

    }

    public void startPagination(int maxRowNum) {
        isLoaded = true;
        lastVisitedNo = String.valueOf(maxRowNum);
        callService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HubCityContext mHubCity = (HubCityContext) getApplicationContext();
        mHubCity.clearArrayAndAllValues(false);
        HubCityContext.isDoneClicked = false;
    }
}
