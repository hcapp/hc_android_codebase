package com.scansee.android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.CustomNavigation;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ScanNowHistoryActivity extends CustomTitleBar {
	private ArrayList<HashMap<String, String>> scannowhistoryList = new ArrayList<>();
	protected ListView scannowhistoryListView;

	ScanNowHistoryListAdapter scannowhistoryListAdapter;
	private ProgressDialog mDialog;

	String lastVisitedProductNo = "0";
	private CustomNavigation customNaviagation;


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scannow_history_listview);
		try {
			CommonConstants.hamburgerIsFirst = true;

			scannowhistoryListView = (ListView) findViewById(R.id.menu_list_scannow);

			scannowhistoryListView
                    .setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                int position, long id) {
                            try {
                                HashMap<String, String> selectedData = scannowhistoryList
                                        .get(position);
                                if ("details".equalsIgnoreCase(selectedData
                                        .get("itemName"))) {
                                    Intent dealDetail = new Intent(
                                            ScanNowHistoryActivity.this,
                                            RetailerCurrentsalesActivity.class);
                                    dealDetail
                                            .putExtra(
                                                    CommonConstants.TAG_CURRENTSALES_PRODUCTID,
                                                    selectedData.get("productId"));
                                    dealDetail
                                            .putExtra(
                                                    CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
                                                    selectedData.get("prodListId"));
                                    dealDetail
                                            .putExtra(
                                                    CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
                                                    selectedData.get("productName"));
                                    dealDetail.putExtra("ScanNow", true);
                                    startActivityForResult(dealDetail,
                                            Constants.STARTVALUE);
                                } else if ("viewMore".equalsIgnoreCase(selectedData
                                        .get("itemName"))) {
                                    // scannowhistoryList.remove(position);
                                    // new ScanNowHistory().execute();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

			mDialog = ProgressDialog.show(ScanNowHistoryActivity.this, "",
                    Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
			new ScanNowHistory().execute();

			//user for hamburger in modules
			drawerIcon.setVisibility(View.VISIBLE);
			drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	boolean isAlreadyLoading;

	protected void viewMore() {
		isAlreadyLoading = true;
		scannowhistoryList.remove(scannowhistoryList.size() - 1);
		new ScanNowHistory().execute();
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}

		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		title.setSingleLine(false);
		title.setMaxLines(2);
		title.setText(R.string.history_title);

		leftTitleImage.setVisibility(View.GONE);
	}

	UrlRequestParams mUrlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();
	boolean nextPage;

	private class ScanNowHistory extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null, jsonDateObject, jsonDetailsObject;
		JSONArray jsonDateArray, jsonDetailsArray;
		HashMap<String, String> historyData = null;

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			try {

				String urlParameters = mUrlRequestParams.getScanNowHistory("0");

				jsonObject = mServerConnections.getJSONFromUrl(false,
						urlParameters, null);

				if (jsonObject != null) {
					jsonDateArray = new JSONArray();
					try {
						jsonDateObject = jsonObject
								.getJSONObject("ScanHistory")
								.getJSONObject("productHistoryInfo")
								.getJSONObject("ScanHistoryInfo");
						jsonDateArray.put(jsonDateObject);
					} catch (Exception e) {
						e.printStackTrace();
						jsonDateArray = jsonObject.getJSONObject("ScanHistory")
								.getJSONObject("productHistoryInfo")
								.getJSONArray("ScanHistoryInfo");
					}
					for (int catCount = 0; catCount < jsonDateArray.length(); catCount++) {
						historyData = new HashMap<>();
						historyData.put("itemName", "dateScanned");
						historyData.put("dateScanned",
								jsonDateArray.getJSONObject(catCount)
										.getString("dateScanned"));
						scannowhistoryList.add(historyData);

						jsonDetailsArray = new JSONArray();
						try {
							jsonDetailsObject = jsonDateArray
									.getJSONObject(catCount)
									.getJSONObject("ProductDetails")
									.getJSONObject("ProductDetail");
							jsonDetailsArray.put(jsonDetailsObject);
						} catch (Exception e) {
							e.printStackTrace();
							jsonDetailsArray = jsonDateArray
									.getJSONObject(catCount)
									.getJSONObject("ProductDetails")
									.getJSONArray("ProductDetail");
						}
						for (int detCount = 0; detCount < jsonDetailsArray
								.length(); detCount++) {
							historyData = new HashMap<>();
							historyData.put("itemName", "details");
							historyData.put("productImagePath",
									jsonDetailsArray.getJSONObject(detCount)
											.getString("productImagePath"));
							historyData.put("productId",
									jsonDetailsArray.getJSONObject(detCount)
											.getString("productId"));
							historyData.put("prodListId",
									jsonDetailsArray.getJSONObject(detCount)
											.getString("prodListId"));
							historyData.put("productName",
									jsonDetailsArray.getJSONObject(detCount)
											.getString("productName"));
							historyData.put("rowNumber",
									jsonDetailsArray.getJSONObject(detCount)
											.getString("rowNumber"));
							lastVisitedProductNo = jsonDetailsArray
									.getJSONObject(detCount).getString(
											"rowNumber");
							scannowhistoryList.add(historyData);
						}
					}
					if ("1".equalsIgnoreCase(jsonObject.getJSONObject(
							"ScanHistory").getString("nextPage"))) {
						nextPage = true;
						historyData = new HashMap<>();
						historyData.put("itemName", "viewMore");
						historyData.put(CommonConstants.TAG_ROW_NUMBER,
								lastVisitedProductNo);
						scannowhistoryList.add(historyData);
					} else {
						nextPage = false;
					}
				}
				result = "true";

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			isAlreadyLoading = false;
			mDialog.dismiss();
			if ("true".equals(result)) {
				Parcelable state = scannowhistoryListView.onSaveInstanceState();
				scannowhistoryListAdapter = new ScanNowHistoryListAdapter(
						ScanNowHistoryActivity.this, scannowhistoryList);
				scannowhistoryListView.setAdapter(scannowhistoryListAdapter);
				scannowhistoryListView.onRestoreInstanceState(state);
			} else {
				Toast.makeText(getBaseContext(), "No Products Found.",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

}
