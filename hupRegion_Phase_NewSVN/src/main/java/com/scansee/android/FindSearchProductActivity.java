package com.scansee.android;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class FindSearchProductActivity extends CustomTitleBar {

	private HashMap<String, String> productData = new HashMap<>();
	private ArrayList<HashMap<String, String>> findsearchlist = null;
	public ListView findprosearchListView;

	FindSearchProductListAdapter findproductsearchListAdapter;
	String userID, latitude = null, longitude = null;
	String lastVistedProductNo = "0";
	String searchkey;
	String parCatId, productId, addTo;
	boolean fromfavorites = false;
	boolean fromlist, isAlreadyLoading;
	LocationManager locationManager;

	FindLocationCategory findLocAsyncTask;
	AddTo addToAsyncTask;
	AddToFavorites addToFavAsyncTask;
	LinearLayout mainLayout;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_product_listview);

		try {
			mainLayout = (LinearLayout) findViewById(R.id.main_layout);
			mainLayout.setVisibility(View.INVISIBLE);

			SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
			userID = settings.getString(CommonConstants.USER_ID, "0");
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			String provider = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			findsearchlist = new ArrayList<>();
			if (getIntent().getExtras().getString("smartsearchKey") != null) {
                searchkey = getIntent().getExtras().getString("smartsearchKey")
                        .trim();
            }
			if (getIntent().getExtras().getString("selectedCat") != null) {
                parCatId = getIntent().getExtras().getString("selectedCat");

            }

			lastVistedProductNo = "0";
			if (provider.contains("gps")) {

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                        CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                        new ScanSeeLocListener());
                Location locNew = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (locNew != null) {

                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());

                } else {
                    // N/W Tower Info Start
                    locNew = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (locNew != null) {
                        latitude = String.valueOf(locNew.getLatitude());
                        longitude = String.valueOf(locNew.getLongitude());
                    } else {
                        latitude = CommonConstants.LATITUDE;
                        longitude = CommonConstants.LONGITUDE;
                    }
                    // N/W Tower Info end
                }

            }
			fromfavorites = getIntent().getExtras().getBoolean("Favorites");
			fromlist = getIntent().getExtras().getBoolean("List");

			findprosearchListView = (ListView) findViewById(R.id.find_product_listview_list);
			findprosearchListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {

                    try {
                        HashMap<String, String> selectedItem = findsearchlist
                                .get(position);
                        String itemName = selectedItem.get("itemName");

                        if ("searchproduct".equalsIgnoreCase(itemName)) {

                            int count = Integer.valueOf(selectedItem
                                    .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTPRODUCTCOUNT));

                            if (count > 1) {
                                Intent intent = new Intent(
                                        FindSearchProductActivity.this,
                                        FindGetProductListActivity.class);

                                intent.putExtra(
                                        CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
                                        selectedItem
                                                .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
                                intent.putExtra("List", fromlist);
                                intent.putExtra("Favorites", fromfavorites);

                                if (getIntent().getExtras()
                                        .getString("selectedCat") != null) {
                                    intent.putExtra("selectedCat", parCatId);
                                }

                                startActivity(intent);
                            }

                            else if (fromfavorites) {
                                productId = selectedItem
                                        .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID);
                                callAddToFavAsyncTask();
                            } else if (fromlist) {
                                productId = selectedItem
                                        .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID);
                                addTo = "List";
                                callAddToAsyncTask();
                            } else {
                                Intent navIntent = new Intent(
                                        FindSearchProductActivity.this,
                                        RetailerCurrentsalesActivity.class);
                                navIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_PRODUCTID,
                                                selectedItem
                                                        .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
                                navIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_RODUCTLISTID,
                                                selectedItem
                                                        .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID));
                                navIntent
                                        .putExtra(
                                                CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
                                                selectedItem
                                                        .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));

                                navIntent.putExtra("Find", true);
                                startActivity(navIntent);

                            }

                        } else if ("viewMore".equalsIgnoreCase(itemName)) {
                            // findsearchlist.remove(position);
                            // lastVistedProductNo = selectedItem
                            // .get(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER);
                            // callFindLocAsyncTask();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
			hideKeyBoard();
			callFindLocAsyncTask();
			title.setText("Find");
			leftTitleImage.setVisibility(View.INVISIBLE);

			backImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTasks();
                    finish();
                }
            });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void viewMore() {
		isAlreadyLoading = true;
		findsearchlist.remove(findsearchlist.size() - 1);
		lastVistedProductNo = findsearchlist.get(findsearchlist.size() - 1)
				.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER);
		callFindLocAsyncTask();
	}

	private void callFindLocAsyncTask() {
		if (findLocAsyncTask != null) {
			if (!findLocAsyncTask.isCancelled()) {
				findLocAsyncTask.cancel(true);
			}

			findLocAsyncTask = null;
		}

		findLocAsyncTask = new FindLocationCategory();
		findLocAsyncTask.execute();
	}

	private void callAddToAsyncTask() {
		if (addToAsyncTask != null) {
			if (!addToAsyncTask.isCancelled()) {
				addToAsyncTask.cancel(true);
			}

			addToAsyncTask = null;
		}

		addToAsyncTask = new AddTo();
		addToAsyncTask.execute();
	}

	private void callAddToFavAsyncTask() {
		if (addToFavAsyncTask != null) {
			if (!addToFavAsyncTask.isCancelled()) {
				addToFavAsyncTask.cancel(true);
			}

			addToFavAsyncTask = null;
		}

		addToFavAsyncTask = new AddToFavorites();
		addToFavAsyncTask.execute();
	}

	private void cancelAsyncTasks() {
		if (findLocAsyncTask != null && !findLocAsyncTask.isCancelled()) {
			findLocAsyncTask.cancel(true);
		}

		findLocAsyncTask = null;

		if (addToAsyncTask != null && !addToAsyncTask.isCancelled()) {
			addToAsyncTask.cancel(true);
		}

		addToAsyncTask = null;

		if (addToFavAsyncTask != null && !addToFavAsyncTask.isCancelled()) {
			addToFavAsyncTask.cancel(true);
		}

		addToFavAsyncTask = null;
	}

	@Override
	public void onDestroy() {
		if (locationManager != null) {
			locationManager.removeUpdates(new ScanSeeLocListener());
		}

		cancelAsyncTasks();

		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		cancelAsyncTasks();
		finish();
	}

	private void hideKeyBoard() {
		FindSearchProductActivity.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	boolean nextPage;
	boolean isShowdialog;

	private class FindLocationCategory extends AsyncTask<String, Void, String> {

		JSONObject jsonObject = null;
		JSONObject responseMenuObject = null;
		JSONArray jsonArrayProductDetail = null;
		HashMap<String, String> productData = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			if (!isShowdialog) {
				isShowdialog = true;
				mDialog = ProgressDialog.show(FindSearchProductActivity.this,
						"", Constants.DIALOG_MESSAGE, true);
				mDialog.setCancelable(false);
			}
		}

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			String tempLastVisit = "0";

			try {

				UrlRequestParams objUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();
				String urlParameters = objUrlRequestParams
						.getFindProductSearchList(searchkey, parCatId,
								lastVistedProductNo, Constants.getMainMenuId());
				String get_product_items_list = Properties.url_local_server
						+ Properties.hubciti_version
						+ "scannow/getsmartsearchcount";
				jsonObject = mServerConnections.getUrlPostResponse(
						get_product_items_list, urlParameters, true);
				if (jsonObject != null) {

					try {
						JSONObject productDetail = jsonObject.getJSONObject(
								CommonConstants.FINDPRODUCTSEARCHRESULTSET)
								.getJSONObject(
										CommonConstants.FINDPRODUCTSEARCHMENU);

						jsonArrayProductDetail = new JSONArray();
						jsonArrayProductDetail.put(productDetail);
					} catch (Exception e) {
						e.printStackTrace();
						jsonArrayProductDetail = jsonObject.getJSONObject(
								CommonConstants.FINDPRODUCTSEARCHRESULTSET)
								.getJSONArray(
										CommonConstants.FINDPRODUCTSEARCHMENU);
					}

					for (int arrayCount = 0; arrayCount < jsonArrayProductDetail
							.length(); arrayCount++) {
						productData = new HashMap<>();

						responseMenuObject = jsonArrayProductDetail
								.getJSONObject(arrayCount);
						productData.put("itemName", "searchproduct");

						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTPRODUCTCOUNT,
										jsonObject
												.getJSONObject(
														CommonConstants.FINDPRODUCTSEARCHRESULTSET)
												.getString(
														CommonConstants.FINDPRODUCTSEARCHPRODUCTPRODUCTCOUNT));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTID,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTLISTID));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTDESCRIPTION));
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHIMAGEPATH,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHIMAGEPATH));

						tempLastVisit = productData
								.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER);
						findsearchlist.add(productData);
					}
					if ("1".equalsIgnoreCase(jsonObject
							.getJSONObject(
									CommonConstants.FINDPRODUCTSEARCHRESULTSET)
							.getString(
									CommonConstants.FINDPRODUCTSEARCHPRODUCTNEXTPAGE))
							&& productData != null) {
						nextPage = true;
						productData = new HashMap<>();
						productData.put("itemName", "viewMore");
						productData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
										tempLastVisit);

						findsearchlist.add(productData);
					} else {
						nextPage = false;
					}

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			isAlreadyLoading = false;
			try {
				if ("true".equals(result)) {
                    Parcelable state = findprosearchListView.onSaveInstanceState();
                    findproductsearchListAdapter = new FindSearchProductListAdapter(
                            FindSearchProductActivity.this, findsearchlist);
                    findprosearchListView.setAdapter(findproductsearchListAdapter);
                    findprosearchListView.onRestoreInstanceState(state);
                } else {
                    mDialog.dismiss();
                    Toast.makeText(getBaseContext(), "No Products Found",
                            Toast.LENGTH_SHORT).show();
                }

				mainLayout.setVisibility(View.VISIBLE);

				if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private class AddTo extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(FindSearchProductActivity.this,
					Constants.DIALOG_MESSAGE, "");
			mDialog.setCancelable(false);

		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			try {
				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.ADDTOSHOPPINGLISTRESULTSET);

					productData = new HashMap<>();
					productData
							.put(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE,
									String.valueOf(jsonObject
											.getInt(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE)));

					productData
							.put(CommonConstants.ADDTOSHOPPINGLIST,
									jsonObject
											.getString(CommonConstants.ADDTOSHOPPINGLIST));
					productData
							.put(CommonConstants.ADDTOSHOPPINGLISTUSERPRODUCTIDS,
									jsonObject
											.getString(CommonConstants.ADDTOSHOPPINGLISTUSERPRODUCTIDS));

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {

			if ("10000".equalsIgnoreCase(productData
					.get(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE))) {

				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindSearchProductActivity.this);
				notificationAlert.setMessage(
						productData.get(CommonConstants.ADDTOSHOPPINGLIST))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				notificationAlert.create().show();

			} else if ("10010".equalsIgnoreCase(productData
					.get(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE))) {

				mDialog.dismiss();

				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindSearchProductActivity.this);
				notificationAlert.setMessage(
						productData.get(CommonConstants.ADDTOSHOPPINGLIST))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				notificationAlert.create().show();

			}

			else {
				mDialog.dismiss();

				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindSearchProductActivity.this);
				notificationAlert.setMessage(
						getString(R.string.network_failure)).setPositiveButton(
						getString(R.string.specials_ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

				notificationAlert.create().show();

			}

			if (mDialog.isShowing()) {
				mDialog.dismiss();
			}

		}
	}

	private class AddToFavorites extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(FindSearchProductActivity.this,
					Constants.DIALOG_MESSAGE, "");
			mDialog.setCancelable(false);

		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			try {

				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.ADDTOSLRESULTSET);

					productData = new HashMap<>();
					productData
							.put(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE,
									String.valueOf(jsonObject
											.getInt(CommonConstants.ADDTOSHOPPINGLISTRESPONSECODE)));
					productData.put(CommonConstants.ADDTOSL,
							jsonObject.getString(CommonConstants.ADDTOSL));

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {

			if ("true".equalsIgnoreCase(result)) {

				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindSearchProductActivity.this);
				notificationAlert.setMessage(
						productData.get(CommonConstants.ADDTOSHOPPINGLIST))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				notificationAlert.create().show();
			} else {
				mDialog.dismiss();

				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						FindSearchProductActivity.this);
				notificationAlert.setMessage(
						getString(R.string.network_failure)).setPositiveButton(
						getString(R.string.specials_ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
				notificationAlert.create().show();
			}

			if (mDialog.isShowing()) {
				mDialog.dismiss();
			}
		}
	}

}
