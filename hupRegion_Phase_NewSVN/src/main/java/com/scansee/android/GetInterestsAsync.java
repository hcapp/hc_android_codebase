package com.scansee.android;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.hubcity.android.businessObjects.FilterOptionBO;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class GetInterestsAsync extends AsyncTask<Void, Void, Void> {

    Context mContext;
    String mItemId, catName, spType, cityExpId, searchKey, bottomBtnId, latitude, longitude;
    ArrayList<FilterOptionBO> arrFilterOptionBOs;
    int groupPosition;
    OnCreateFilterChildView filterChildView;

    private ProgressDialog mDialog;

    public GetInterestsAsync(Context context, String mItemId,
                             int groupPosition, String catName, String spType, String cityExpId,
                             OnCreateFilterChildView filterChildView, String searchKey,
                             String bottomBtnId, String latitude, String longitude) {
        this.mContext = context;
        this.mItemId = mItemId;
        this.groupPosition = groupPosition;
        this.catName = catName;
        this.spType = spType;
        this.bottomBtnId = bottomBtnId;
        this.cityExpId = cityExpId;
        this.filterChildView = filterChildView;
        this.searchKey = searchKey;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mDialog = ProgressDialog.show(mContext, "", Constants.DIALOG_MESSAGE,
                true);
        mDialog.setCancelable(false);
    }

    @Override
    protected Void doInBackground(Void... params) {
        String urlParameters = new UrlRequestParams()
                .createInterestsListParameter(mItemId, catName, spType,
                        cityExpId, searchKey, bottomBtnId, latitude, longitude);
        String get_interest_filter_list_url = Properties.url_local_server
                + Properties.hubciti_version + "find/getinterestlist";
        JSONObject jsonResponse = new ServerConnections().getUrlPostResponse(
                get_interest_filter_list_url, urlParameters, true);
        if (jsonResponse.has("Filter")) {
            try {
                if (jsonResponse.getJSONObject("Filter")
                        .getString("responseCode").equals("10000")) {

                    JSONArray jsonArray = null;
                    JSONObject jsonObject = null;
                    try {
                        jsonArray = jsonResponse.getJSONObject("Filter")
                                .getJSONObject("filterList")
                                .getJSONArray("Filter");
                    } catch (Exception e) {
                        jsonObject = jsonResponse.getJSONObject("Filter")
                                .getJSONObject("filterList")
                                .getJSONObject("Filter");
                    }
                    arrFilterOptionBOs = new ArrayList<>();
                    if (jsonArray != null) {
                        FilterOptionBO filterOption = new FilterOptionBO();
                        filterOption.setFilterGroupName("Interests");
                        filterOption.setFilterId("");
                        filterOption.setFilterName("All");
                        arrFilterOptionBOs.add(filterOption);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            FilterOptionBO filterOptionBO = new FilterOptionBO();
                            filterOptionBO.setFilterGroupName("Interests");
                            filterOptionBO.setFilterId(jsonArray.getJSONObject(
                                    i).getString("filterId"));
                            filterOptionBO.setFilterName(jsonArray
                                    .getJSONObject(i).getString("filterName"));
                            arrFilterOptionBOs.add(filterOptionBO);
                        }
                    } else {
                        FilterOptionBO filterOptionBO = new FilterOptionBO();
                        filterOptionBO.setFilterGroupName("Interests");
                        filterOptionBO.setFilterId(jsonObject
                                .getString("filterId"));
                        filterOptionBO.setFilterName(jsonObject
                                .getString("filterName"));
                        arrFilterOptionBOs.add(filterOptionBO);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (arrFilterOptionBOs != null) {
            try {
                ((FilterAndSortScreen) mContext).setFilterOptionsForChildView(
                        arrFilterOptionBOs, groupPosition, "interest");
            } catch (Exception e) {
                filterChildView.setFilterOptionsForChildView(
                        arrFilterOptionBOs, groupPosition, "interest");
            }
        } else
            Toast.makeText(mContext, "No Records Found", Toast.LENGTH_SHORT)
                    .show();

        mDialog.dismiss();
    }

}
