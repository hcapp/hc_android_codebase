package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by supriya.m on 6/3/2016.
 */
public class MenuList
{
	public String getAndroidLink()
	{
		return androidLink;
	}

	public void setAndroidLink(String androidLink)
	{
		this.androidLink = androidLink;
	}

	public String getAppIconImg()
	{
		return appIconImg;
	}

	public void setAppIconImg(String appIconImg)
	{
		this.appIconImg = appIconImg;
	}

	public String getDownLoadLink()
	{
		return downLoadLink;
	}

	public void setDownLoadLink(String downLoadLink)
	{
		this.downLoadLink = downLoadLink;
	}

	public String getBkImgPath()
	{
		return bkImgPath;
	}

	public void setBkImgPath(String bkImgPath)
	{
		this.bkImgPath = bkImgPath;
	}

	public String getDateModified()
	{
		return dateModified;
	}

	public void setDateModified(String dateModified)
	{
		this.dateModified = dateModified;
	}

	public String getDepartFlag()
	{
		return departFlag;
	}

	public void setDepartFlag(String departFlag)
	{
		this.departFlag = departFlag;
	}

	public String getHomeImgPath()
	{
		return homeImgPath;
	}

	public void setHomeImgPath(String homeImgPath)
	{
		this.homeImgPath = homeImgPath;
	}

	public String getHubCitiId()
	{
		return hubCitiId;
	}

	public void setHubCitiId(String hubCitiId)
	{
		this.hubCitiId = hubCitiId;
	}

	public String getIsTempChanged()
	{
		return isTempChanged;
	}

	public void setIsTempChanged(String isTempChanged)
	{
		this.isTempChanged = isTempChanged;
	}

	public String getLevel()
	{
		return level;
	}

	public void setLevel(String level)
	{
		this.level = level;
	}

	public String getmBannerImg()
	{
		return mBannerImg;
	}

	public void setmBannerImg(String mBannerImg)
	{
		this.mBannerImg = mBannerImg;
	}

	public String getmBkgrdColor()
	{
		return mBkgrdColor;
	}

	public void setmBkgrdColor(String mBkgrdColor)
	{
		this.mBkgrdColor = mBkgrdColor;
	}

	public String getmBkgrdImage()
	{
		return mBkgrdImage;
	}

	public void setmBkgrdImage(String mBkgrdImage)
	{
		this.mBkgrdImage = mBkgrdImage;
	}

	public String getMenuId()
	{
		return menuId;
	}

	public void setMenuId(String menuId)
	{
		this.menuId = menuId;
	}

	public String getMenuName()
	{
		return menuName;
	}

	public void setMenuName(String menuName)
	{
		this.menuName = menuName;
	}

	public String getmFontColor()
	{
		return mFontColor;
	}

	public void setmFontColor(String mFontColor)
	{
		this.mFontColor = mFontColor;
	}

	public String getRetAffCount()
	{
		return retAffCount;
	}

	public void setRetAffCount(String retAffCount)
	{
		this.retAffCount = retAffCount;
	}

	public String getRetAffId()
	{
		return retAffId;
	}

	public void setRetAffId(String retAffId)
	{
		this.retAffId = retAffId;
	}

	public String getRetAffName()
	{
		return retAffName;
	}

	public void setRetAffName(String retAffName)
	{
		this.retAffName = retAffName;
	}

	public String getRetGroupImg()
	{
		return retGroupImg;
	}

	public void setRetGroupImg(String retGroupImg)
	{
		this.retGroupImg = retGroupImg;
	}

	public String getSmBkgrdColor()
	{
		return smBkgrdColor;
	}

	public void setSmBkgrdColor(String smBkgrdColor)
	{
		this.smBkgrdColor = smBkgrdColor;
	}

	public String getSmBkgrdImage()
	{
		return smBkgrdImage;
	}

	public void setSmBkgrdImage(String smBkgrdImage)
	{
		this.smBkgrdImage = smBkgrdImage;
	}

	public String getSmFontColor()
	{
		return smFontColor;
	}

	public void setSmFontColor(String smFontColor)
	{
		this.smFontColor = smFontColor;
	}

	public String getTemplateName()
	{
		return templateName;
	}

	public void setTemplateName(String templateName)
	{
		this.templateName = templateName;
	}

	public String getTitleBkGrdColor()
	{
		return titleBkGrdColor;
	}

	public void setTitleBkGrdColor(String titleBkGrdColor)
	{
		this.titleBkGrdColor = titleBkGrdColor;
	}

	public String getTitleTxtColor()
	{
		return titleTxtColor;
	}

	public void setTitleTxtColor(String titleTxtColor)
	{
		this.titleTxtColor = titleTxtColor;
	}

	public String getTypeFlag()
	{
		return typeFlag;
	}

	public void setTypeFlag(String typeFlag)
	{
		this.typeFlag = typeFlag;
	}

	public ArrayList<ArMItemList> getArMItemList()
	{
		return arMItemList;
	}

	public void setArMItemList(ArrayList<ArMItemList> arMItemList)
	{
		this.arMItemList = arMItemList;
	}

	private String androidLink;
	private String appIconImg;
	private String downLoadLink;
	private String bkImgPath;
	private String dateModified;
	private String departFlag;
	private String homeImgPath;
	private String hubCitiId;
	private String isTempChanged;
	private String level;
	private String mBannerImg;
	private String mBkgrdColor;
	private String mBkgrdImage;
	private String menuId;
	private String menuName;
	private String mFontColor;
	private String retAffCount;
	private String retAffId;
	private String retAffName;
	private String retGroupImg;
	private String smBkgrdColor;
	private String smBkgrdImage;
	private String smFontColor;
	private String templateName;
	private String titleBkGrdColor;
	private String titleTxtColor;
	private String typeFlag;
	private ArrayList<ArMItemList> arMItemList;



}
