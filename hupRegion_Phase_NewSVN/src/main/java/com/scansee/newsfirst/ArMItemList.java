package com.scansee.newsfirst;

/**
 * Created by supriya.m on 6/3/2016.
 */
public class ArMItemList
{
	public int getFlag()
	{
		return flag;
	}

	public void setFlag(int flag)
	{
		this.flag = flag;
	}

	public String getLinkId()
	{
		return linkId;
	}

	public void setLinkId(String linkId)
	{
		this.linkId = linkId;
	}

	public String getLinkTypeId()
	{
		return linkTypeId;
	}

	public void setLinkTypeId(String linkTypeId)
	{
		this.linkTypeId = linkTypeId;
	}

	public String getLinkTypeName()
	{
		return linkTypeName;
	}

	public void setLinkTypeName(String linkTypeName)
	{
		this.linkTypeName = linkTypeName;
	}

	public String getmBtnColor()
	{
		return mBtnColor;
	}

	public void setmBtnColor(String mBtnColor)
	{
		this.mBtnColor = mBtnColor;
	}

	public String getmBtnFontColor()
	{
		return mBtnFontColor;
	}

	public void setmBtnFontColor(String mBtnFontColor)
	{
		this.mBtnFontColor = mBtnFontColor;
	}

	public String getmGrpBkgrdColor()
	{
		return mGrpBkgrdColor;
	}

	public void setmGrpBkgrdColor(String mGrpBkgrdColor)
	{
		this.mGrpBkgrdColor = mGrpBkgrdColor;
	}

	public String getmGrpFntColor()
	{
		return mGrpFntColor;
	}

	public void setmGrpFntColor(String mGrpFntColor)
	{
		this.mGrpFntColor = mGrpFntColor;
	}

	public String getmItemId()
	{
		return mItemId;
	}

	public void setmItemId(String mItemId)
	{
		this.mItemId = mItemId;
	}

	public String getmItemImg()
	{
		return mItemImg;
	}

	public void setmItemImg(String mItemImg)
	{
		this.mItemImg = mItemImg;
	}

	public String getmItemName()
	{
		return mItemName;
	}

	public void setmItemName(String mItemName)
	{
		this.mItemName = mItemName;
	}

	public String getPosition()
	{
		return position;
	}

	public void setPosition(String position)
	{
		this.position = position;
	}

	public String getSmBtnColor()
	{
		return smBtnColor;
	}

	public void setSmBtnColor(String smBtnColor)
	{
		this.smBtnColor = smBtnColor;
	}

	public String getSmBtnFontColor()
	{
		return smBtnFontColor;
	}

	public void setSmBtnFontColor(String smBtnFontColor)
	{
		this.smBtnFontColor = smBtnFontColor;
	}

	public String getSortOrder()
	{
		return sortOrder;
	}

	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	private int flag;
	private String sortOrder;
	private String linkId;
	private String linkTypeId;
	private String linkTypeName;
	private String mBtnColor;
	private String mBtnFontColor;
	private String mGrpBkgrdColor;
	private String mGrpFntColor;
	private String mItemId;
	private String mItemImg;
	private String mItemName;
	private String position;
	private String smBtnColor;
	private String smBtnFontColor;

	public String getSmGrpBkgrdColor() {
		return smGrpBkgrdColor;
	}

	public void setSmGrpBkgrdColor(String smGrpBkgrdColor) {
		this.smGrpBkgrdColor = smGrpBkgrdColor;
	}

	public String getSmGrpFntColor() {
		return smGrpFntColor;
	}

	public void setSmGrpFntColor(String smGrpFntColor) {
		this.smGrpFntColor = smGrpFntColor;
	}

	private String smGrpBkgrdColor;
	private String smGrpFntColor;

}
