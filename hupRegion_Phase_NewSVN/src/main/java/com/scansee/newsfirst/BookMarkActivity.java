package com.scansee.newsfirst;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.commonUtil.BookMarkModel;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.LinkedList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by subramanya.v on 5/18/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class BookMarkActivity extends CustomTitleBar implements OnStartDragListener {
    public RecyclerView bookMark;
    private final LinkedList<BookMarkObj> bookmarkListData = new LinkedList<>();
    private ItemTouchHelper mItemTouchHelper;
    private Button bookMkButton;
    private RestClient mRestClient;
    private String bkMarkOrder, navigOrder;
    private String mClassName, mLinkID, mLevel;
    private Activity mActivity;
    private RecyclerView sideMenu;
    private Button sideMnButtton;
    private final LinkedList<BookMarkObj> sectionsListData = new LinkedList<>();
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_mark_activity);
        mActivity = BookMarkActivity.this;
        mContext = BookMarkActivity.this;
        // Title of the screen
        title.setText("Settings");
        ((TextView) findViewById(R.id.right_text)).setText("Done");
        ((TextView) findViewById(R.id.right_text)).setTextColor(Color.parseColor(Constants
                .getNavigationBarValues("titleTxtColor")));
        rightImage.setVisibility(View.GONE);
        mRestClient = RestClient.getInstance();
        getIntentData();
        bindView();
        setListener();
        setRecyclerFields();

    }


    private void setRecyclerFields() {
        bookMark.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        bookMark.setLayoutManager(mLayoutManager);

        sideMenu.setHasFixedSize(true);
        RecyclerView.LayoutManager sideMenuManager = new LinearLayoutManager(this);
        sideMenu.setLayoutManager(sideMenuManager);

    }


    private void getIntentData() {
        Bundle intent = getIntent().getExtras();
        if (getIntent().hasExtra("ClassName")) {
            mClassName = intent.getString("ClassName");
        }
        if (getIntent().hasExtra("LinkId")) {
            mLinkID = intent.getString("LinkId");
        }
        if (getIntent().hasExtra("Level")) {
            mLevel = intent.getString("Level");
        }
    }

    private void setListener() {
        bookMkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bookmarkListData.size() == 0) {
                    getBookMarkFields();
                }
                changeViewVisibility(sideMenu, bookMark, sideMnButtton, bookMkButton);
            }
        });
        sideMnButtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sectionsListData.size() == 0) {
                    getSideMenuFields();
                }
                if (mClassName != null) {
                    changeViewVisibility(bookMark, sideMenu, bookMkButton, sideMnButtton);
                } else {
                    if(sideMenu.getVisibility() == View.GONE)
                    {
                        sideMenu.setVisibility(View.VISIBLE);
                        sideMnButtton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
                    }
                    else
                    {
                        sideMenu.setVisibility(View.GONE);
                        sideMnButtton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
                    }

                }

            }
        });


        findViewById(R.id.right_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSortOrder();
                setUpdateFields();

            }
        });

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void changeViewVisibility(RecyclerView removingView, RecyclerView stateChangeView, Button downButtton, Button upButton) {
        removingView.setVisibility(View.GONE);
        downButtton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        if (View.VISIBLE == stateChangeView.getVisibility()) {
            stateChangeView.setVisibility(View.GONE);
            upButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        } else {
            stateChangeView.setVisibility(View.VISIBLE);
            upButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
        }
    }

    private void getSideMenuFields() {
        final ProgressDialog progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage(mActivity.getString(R.string.progress_message));
        progDialog.setCancelable(false);
        progDialog.show();
        SideMenuModel sideMenuRequest = UrlRequestParams.getSideMenuDetails(mLinkID, mLevel, "true");
        mRestClient.getSideMenuDetails(sideMenuRequest, new Callback<BookMarkObj>() {

                    @Override
                    public void success(BookMarkObj bookMarkObj, Response response) {
                        try {
                            //Rearraging values according to the view
                            if (bookMarkObj.getResponseText().equalsIgnoreCase(mActivity.getString(R.string.success))) {
                                int i = 0;
                                for (BookMarkList bookMarkListObj : bookMarkObj.getBookMarkList
                                        ()) {

                                    BookMarkObj bookMarkHeader = new BookMarkObj();
                                    if (i != 0) {
                                        bookMarkHeader.setCategory(bookMarkListObj.getHeader());
                                    } else {
                                        bookMarkHeader.setCategory(mActivity.getString(R.string.side_menu));
                                    }
                                    bookMarkHeader.setInBothSections(false);
                                    bookMarkHeader.setDragCategory(bookMarkListObj.getHeader());
                                    bookMarkHeader.setIsHeader(1);
                                    sectionsListData.add(bookMarkHeader);


                                    for (ListCatDetails listDetails : bookMarkListObj
                                            .getListCatDetails()) {
                                        if (!(bookMarkListObj.getHeader().equalsIgnoreCase
                                                (mActivity.getString(R.string.other_section))
                                                && listDetails.getIsBkMark() == 1)) {
                                            BookMarkObj bookMarkData = new BookMarkObj();
                                            bookMarkData.setCategory(listDetails.getParCatName());
                                            if (listDetails.getIsBkMark() == 1) {
                                                bookMarkData.setInBothSections(true);
                                            } else {
                                                bookMarkData.setInBothSections(false);
                                            }
                                            bookMarkData.setDragCategory(bookMarkListObj.getHeader());
                                            bookMarkData.setIsHeader(0);
                                            bookMarkData.setParCatId(listDetails.getParCatId());
                                            bookMarkData.setSortOrder(listDetails.getSortOrder());
                                            bookMarkData.setIsHubFunctn(listDetails.getIsHubFunctn());
                                            sectionsListData.add(bookMarkData);
                                        }
                                    }
                                    i++;
                                }
                                BookMarkAdapter sideMenuAdater = new BookMarkAdapter
                                        (sectionsListData,
                                                BookMarkActivity.this, mContext);
                                sideMenu.setAdapter(sideMenuAdater);

                                ItemTouchHelper.Callback bookmarkCallback = new
                                        SimpleItemTouchHelperCallback(sideMenuAdater,
                                        sectionsListData);
                                mItemTouchHelper = new ItemTouchHelper(bookmarkCallback);
                                mItemTouchHelper.attachToRecyclerView(sideMenu);

                                sideMenu.addItemDecoration(
                                        new HorizontalDividerItemDecoration.Builder
                                                (BookMarkActivity
                                                        .this)
                                                .color(Integer.parseInt(String.valueOf(R.color
                                                        .devider_colour)))
                                                .build());

                            } else {
                                Toast.makeText(BookMarkActivity.this, bookMarkObj
                                                .getResponseText(),
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception ignored) {

                        }
                        hideProgressBar(progDialog);
                    }


                    @Override
                    public void failure(RetrofitError retrofitError) {
                        hideProgressBar(progDialog);

                        if (retrofitError.getResponse() != null) {
                            CommonConstants.displayToast(mActivity, retrofitError
                                    .getResponse().getReason());
                        }
                    }
                }

        );

    }

    private void setSortOrder() {
        StringBuilder bkMarkBuffer = new StringBuilder();
        StringBuilder sideMenuBuffer = new StringBuilder();
        if (bookmarkListData.size() != 0) {
            for (int i = 0; i < bookmarkListData.size(); i++) {
                if ((bookmarkListData.get(i).getDragCategory().equalsIgnoreCase(mActivity.getString(R
                        .string
                        .book_mark))) &&
                        (bookmarkListData.get(i).isHeader() == 0)) {
                    bkMarkBuffer.append(bookmarkListData.get(i).getCategory()).append("!~~!");
                }
            }
            bkMarkOrder = bkMarkBuffer.toString();
            if (bkMarkOrder.endsWith("!~~!")) {
                bkMarkOrder = bkMarkOrder.substring(0, bkMarkOrder.length() - 4);
            }
            Log.d("selected categories :", bkMarkOrder);
        }
        if (sectionsListData.size() != 0) {
            for (int i = 0; i < sectionsListData.size(); i++) {
                if ((sectionsListData.get(i).getDragCategory().equalsIgnoreCase(mActivity.getString(R
                        .string
                        .book_mark))) &&
                        (sectionsListData.get(i).isHeader() == 0)) {
                    if (sectionsListData.get(i).getIsHubFunctn() == 1) {
                        sideMenuBuffer.append(sectionsListData.get(i).getParCatId()).append("-F,");
                    } else {
                        sideMenuBuffer.append(sectionsListData.get(i).getParCatId()).append("-C,");
                    }
                }
            }
            navigOrder = sideMenuBuffer.toString();
            if (navigOrder.endsWith(",")) {
                navigOrder = navigOrder.substring(0, navigOrder.length() - 1);
            }
        }

    }

    private void setUpdateFields() {
        final ProgressDialog progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();

        if ((bkMarkOrder != null) && (bkMarkOrder.length() == 0)) {
            bkMarkOrder = "0";
        }
        if ((navigOrder != null) && (navigOrder.length() == 0)) {
            navigOrder = "0";
        }
        UpdateModel updateRequest = UrlRequestParams.getUpdateDetails(bkMarkOrder, navigOrder);
        mRestClient.getUpdateDetails(updateRequest, new Callback<BookMarkObj>() {

            @Override
            public void success(BookMarkObj bookMarkObj, Response response) {
                Intent intent = null;
                if (bookMarkObj.getResponseText().equalsIgnoreCase("Success")) {
                    if (mClassName != null) {
                        //to refresh main page screen again so acivuty for result not used
                        switch (mClassName) {
                            case Constants.COMBINATION:
                                intent = new Intent(BookMarkActivity.this, CombinationTemplate.class);
                                startIntent(intent);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(BookMarkActivity.this, ScrollingPageActivity
                                        .class);
                                startIntent(intent);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(BookMarkActivity.this, TwoTileNewsTemplateActivity
                                        .class);
                                startIntent(intent);
                                break;
                        }
                    } else {
                        finish();
                    }
                    CommonConstants.hamburgerIsFirst = true;

                } else {
                    CommonConstants.displayToast(BookMarkActivity.this, bookMarkObj.getResponseText());
                }
                hideProgressBar(progDialog);

            }


            @Override
            public void failure(RetrofitError retrofitError) {
                hideProgressBar(progDialog);

                if (retrofitError.getResponse() != null) {
                    CommonConstants.displayToast(BookMarkActivity.this, retrofitError
                            .getResponse().getReason());
                }
            }

        });
    }

    private void startIntent(Intent intent) {
        if (intent != null) {
            intent.putExtra(Constants.LEVEL, mLevel);
            intent.putExtra(Constants.LINKID, mLinkID);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    private void hideProgressBar(ProgressDialog progDialog) {
        if (progDialog != null && progDialog.isShowing()) {
            progDialog.dismiss();
        }
    }

    private void bindView() {
        bookMark = (RecyclerView) findViewById(R.id.bookmark);
        sideMenu = (RecyclerView) findViewById(R.id.side_menu);
        bookMkButton = (Button) findViewById(R.id.bookmark_button);
        sideMnButtton = (Button) findViewById(R.id.sidemenu_button);
        if (mClassName == null) {
            bookMark.setVisibility(View.GONE);
            bookMkButton.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    RecyclerView.LayoutParams.MATCH_PARENT, 0);
            params.weight = 9.0f;
            sideMenu.setLayoutParams(params);
            sideMenu.setVisibility(View.VISIBLE);
            sideMnButtton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
            getSideMenuFields();

        }
    }

    private void getBookMarkFields() {
        final ProgressDialog progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();
        BookMarkModel bookMarkRequest = UrlRequestParams.getBookMarkDetails("0", mLinkID, mLevel);
        mRestClient.getBookMarkDetails(bookMarkRequest, new Callback<BookMarkObj>() {

                    @Override
                    public void success(BookMarkObj bookMarkObj, Response response) {
                        try {
                            //Rearraging values according to the view
                            if (bookMarkObj.getResponseText().equalsIgnoreCase("Success")) {
                                for (BookMarkList bookMarkListObj : bookMarkObj.getBookMarkList
                                        ()) {


                                    BookMarkObj bookMarkHeader = new BookMarkObj();
                                    bookMarkHeader.setCategory(bookMarkListObj.getHeader());
                                    bookMarkHeader.setInBothSections(false);
                                    bookMarkHeader.setDragCategory(bookMarkListObj.getHeader());
                                    bookMarkHeader.setIsHeader(1);
                                    bookmarkListData.add(bookMarkHeader);

                                    for (ListCatDetails listDetails : bookMarkListObj
                                            .getListCatDetails()) {
                                        if (!(bookMarkListObj.getHeader().equalsIgnoreCase
                                                (mActivity.getString(R.string.other_section))
                                                && listDetails.getIsBkMark() == 1)) {
                                            BookMarkObj bookMarkData = new BookMarkObj();
                                            bookMarkData.setCategory(listDetails.getParCatName());
                                            if (listDetails.getIsBkMark() == 1) {
                                                bookMarkData.setInBothSections(true);
                                            } else {
                                                bookMarkData.setInBothSections(false);
                                            }
                                            bookMarkData.setDragCategory(bookMarkListObj.getHeader());
                                            bookMarkData.setIsHeader(0);
                                            bookMarkData.setParCatId(listDetails.getParCatId());
                                            bookMarkData.setSortOrder(listDetails.getSortOrder());
                                            bookmarkListData.add(bookMarkData);
                                        }
                                    }
                                }
                                BookMarkAdapter bookMarkAdptr = new BookMarkAdapter
                                        (bookmarkListData,
                                                BookMarkActivity.this, mContext);
                                bookMark.setAdapter(bookMarkAdptr);

                                ItemTouchHelper.Callback bookmarkCallback = new
                                        SimpleItemTouchHelperCallback(bookMarkAdptr,
                                        bookmarkListData);
                                mItemTouchHelper = new ItemTouchHelper(bookmarkCallback);
                                mItemTouchHelper.attachToRecyclerView(bookMark);

                                bookMark.addItemDecoration(
                                        new HorizontalDividerItemDecoration.Builder
                                                (BookMarkActivity
                                                        .this)
                                                .color(Integer.parseInt(String.valueOf(R.color
                                                        .devider_colour)))
                                                .build());

                            } else {
                                CommonConstants.displayToast(BookMarkActivity.this, bookMarkObj
                                        .getResponseText());
                            }
                        } catch (Exception ignored) {

                        }
                        hideProgressBar(progDialog);
                    }


                    @Override
                    public void failure(RetrofitError retrofitError) {
                        hideProgressBar(progDialog);
                        if (retrofitError.getResponse() != null) {
                            CommonConstants.displayToast(BookMarkActivity.this, retrofitError
                                    .getResponse().getReason());
                        }
                    }
                }

        );

    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
}
