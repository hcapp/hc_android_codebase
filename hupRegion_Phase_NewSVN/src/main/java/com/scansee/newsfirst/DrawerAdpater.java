package com.scansee.newsfirst;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subramanya.v on 5/21/2016.
 */
public class DrawerAdpater extends BaseExpandableListAdapter {
    private final Context mContext;
    private final List<NavigationItem> newsList;

    public DrawerAdpater(Context scrollingPageActivity, List<NavigationItem> newsList) {
        this.mContext = scrollingPageActivity;
        this.newsList = newsList;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return newsList.get(groupPosition).getMainCatList().size();
        } catch (Exception e) {
            return 0;
        }
    }


    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return newsList.get(groupPosition).getMainCatList().get(childPosititon);
    }


    @Override
    public long getChildId(int groupPosition, int childPosititon) {
        return childPosititon;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View convertView,
                             ViewGroup parent) {
        TextView category;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(
                R.layout.sub_cat_adapter, parent, false);

        category = (TextView) convertView.findViewById(R.id.item_name);
        String subCat = newsList.get(groupPosition).getMainCatList().get(childPosition)
                .getSubCatName();
        if (subCat != null && !subCat.equalsIgnoreCase("")) {
            category.setText(subCat);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getGroup(int i) {
        return newsList.get(i);
    }

    @Override
    public int getGroupCount() {
        return newsList.size();
    }

    @Override
    public View getGroupView(final int position, final boolean b, View convertView, final
    ViewGroup parent) {
        TextView category;
        ImageView next;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(
                R.layout.drawer_row, parent, false);

        category = (TextView) convertView.findViewById(R.id.item_name);
        next = (ImageView) convertView.findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (b) {
                    ((ExpandableListView) parent).collapseGroup(position);
                } else {
                    ((ExpandableListView) parent).expandGroup(position, true);
                }
            }
        });

        ArrayList<MainCatList> SubCategory = newsList.get(position).getMainCatList();
        if (SubCategory != null) {
            next.setVisibility(View.VISIBLE);
        } else {
            next.setVisibility(View.GONE);
        }
        NavigationItem rowItem = newsList.get(position);


        if (rowItem.getmItemName() != null && !rowItem.getmItemName().equals("")) {
            category.setText(rowItem.getmItemName());
        }
        if (rowItem.getCatName() != null && !rowItem.getCatName
                ().equals("")) {
            category.setText(rowItem.getCatName());
        }


        return convertView;
    }

}

