package com.scansee.newsfirst;

import java.io.Serializable;

/**
 * Created by subramanya.v on 6/1/2016.
 */
public class Items implements Serializable{
    private int itemID;
    private int rowNum;
    private String title;
    private String image;
    private String link;
    private String catName;
    private String date;
    private String sDesc;
    private String lDesc;
    private String time;
    private String imgPosition;
    private String author;
    private String catColor;
    private String videoLink;
    private String catTxtColor;

    public String getBackButtonColor() {
        return backButtonColor;
    }

    public void setBackButtonColor(String backButtonColor) {
        this.backButtonColor = backButtonColor;
    }

    private String backButtonColor;


    public String getCatTxtColor() {
        return catTxtColor;
    }

    public void setCatTxtColor(String catTxtColor) {
        this.catTxtColor = catTxtColor;
    }



    public String getlDesc() {
        return lDesc;
    }

    public void setlDesc(String lDesc) {
        this.lDesc = lDesc;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }



    public String getCatColor() {
        return catColor;
    }

    public void setCatColor(String catColor) {
        this.catColor = catColor;
    }




    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImgPosition() {
        return imgPosition;
    }

    public void setImgPosition(String imgPosition) {
        this.imgPosition = imgPosition;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getsDesc() {
        return sDesc;
    }

    public void setsDesc(String sDesc) {
        this.sDesc = sDesc;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
