package com.scansee.newsfirst;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.scansee.newsfirst.BackgroundTask.ThumbNailAsyTask;
import com.scansee.newsfirst.model.CategoryItemList;

import java.util.ArrayList;


public class ListSummaryStoryAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private Context mContext;
    private ArrayList<CategoryItemList> mCategoryItemList = new ArrayList<>();

    public ListSummaryStoryAdapter(Context context, ArrayList<CategoryItemList> listCategoryItemList) {
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.mCategoryItemList.addAll(listCategoryItemList);
        mCategoryItemList.remove(0);
    }

    @Override
    public int getCount() {
        return mCategoryItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        View view = convertView;
        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.list_summary_sub_view, null);
            viewHolder = new ViewHolder();
            viewHolder.story_image = (ImageView) view.findViewById(R.id
                    .list_summary_sub_image);

            viewHolder.playImageView = (ImageView) view.findViewById(R.id.play_button);
            viewHolder.title = (TextView) view.findViewById(R.id.list_summary_sub_title);
            viewHolder.author = (TextView) view.findViewById(R.id.list_summary_author);
            viewHolder.shortDescription = (TextView) view.findViewById(R.id.list_summary_short_description);
            viewHolder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
            viewHolder.photosCountTextView = (TextView) view.findViewById(R.id.count);
            viewHolder.countViewLayout = (RelativeLayout) view.findViewById(R.id.count_parent);
            viewHolder.alphaLayout = (FrameLayout) view.findViewById(R.id.alpha_view);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.title.setText(mCategoryItemList.get(position).getTitle());
        if (mCategoryItemList.get(position).getAuthor() != null) {
            viewHolder.author.setVisibility(View.VISIBLE);
            viewHolder.author.setText(mCategoryItemList.get(position).getAuthor());
        } else {
            viewHolder.author.setVisibility(View.GONE);
            viewHolder.shortDescription.setText(mCategoryItemList.get(position).getsDesc());
        }
        String category = mCategoryItemList.get(position).getCatName();
        String link;
        if (category.equalsIgnoreCase(GlobalConstants.PHOTOS)) {
            link = mCategoryItemList.get(position).getImage();

            if (link != null) {
                String[] images = link.split(",");
                if (images.length > 1) {
                    setVisibilityIfPhotosVdeos(viewHolder);
                    viewHolder.photosCountTextView.setText(String.valueOf("" + images.length));
                } else {
                    setVisibility(viewHolder);
                }
                new CommonMethods().loadImage(mContext, viewHolder.progressBar, images[0], viewHolder.story_image);
            }

        } else if (category.equalsIgnoreCase(GlobalConstants.VIDEOS)) {
            link = mCategoryItemList.get(position).getVideoLink();

            if (link != null) {
                String[] images = link.split(",");
                if (images.length > 1) {
                    setVisibilityIfPhotosVdeos(viewHolder);
                    viewHolder.photosCountTextView.setText(String.valueOf("" + images.length));
                } else {
                    setVisibility(viewHolder);
                }
                viewHolder.playImageView.setVisibility(View.VISIBLE);
                try {
                    ThumbNailAsyTask thumNail = new ThumbNailAsyTask(images[0], viewHolder.story_image);
                    thumNail.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }

        } else {
            setVisibility(viewHolder);
            if (mCategoryItemList.get(position).getImage() != null) {
                new CommonMethods().loadImage(mContext
                        , viewHolder.progressBar, mCategoryItemList.get(position).getImage(), viewHolder.story_image);
            }
        }
        return view;
    }

    private class ViewHolder {
        ImageView story_image,playImageView;
        TextView author, title,photosCountTextView,shortDescription;
        ProgressBar progressBar;
        private RelativeLayout countViewLayout;
        private FrameLayout alphaLayout;
    }
    private void setVisibility(ViewHolder holder) {
        holder.countViewLayout.setVisibility(View.GONE);
        holder.alphaLayout.setVisibility(View.GONE);
        holder.playImageView.setVisibility(View.GONE);
    }

    private void setVisibilityIfPhotosVdeos(ViewHolder holder) {
        holder.countViewLayout.setVisibility(View.VISIBLE);
        holder.alphaLayout.setVisibility(View.VISIBLE);
        holder.playImageView.setVisibility(View.GONE);
    }

}
