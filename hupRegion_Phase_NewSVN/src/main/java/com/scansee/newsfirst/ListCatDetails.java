package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by supriya.m on 6/3/2016.
 */
public class ListCatDetails {
    //for Hamburger side menu
    private ArrayList<ListMainCat> listMainCat;
    private ArrayList<MenuList> menuList;
    private ArrayList<MainCatList> mainCatList;
    private int parCatId;
    private String parCatName;
    private int isBkMark;
    private String sortOrder;
    private int isHubFunctn;

    public String getNonfeedlink() {
        return nonfeedlink;
    }

    public void setNonfeedlink(String nonfeedlink) {
        this.nonfeedlink = nonfeedlink;
    }

    private String nonfeedlink;

    public ArrayList<MainCatList> getMainCatList() {
        return mainCatList;
    }

    public void setMainCatList(ArrayList<MainCatList> mainCatList) {
        this.mainCatList = mainCatList;
    }


    public int getIsBkMark() {
        return isBkMark;
    }

    public void setIsBkMark(int isBkMark) {
        this.isBkMark = isBkMark;
    }

    public ArrayList<com.scansee.newsfirst.MenuList> getMenuList() {
        return menuList;
    }

    public void setMenuList(ArrayList<com.scansee.newsfirst.MenuList> menuList) {
        menuList = menuList;
    }

    public ArrayList<ListMainCat> getListMainCat() {
        return listMainCat;
    }

    public void setListMainCat(ArrayList<ListMainCat> listMainCat) {
        this.listMainCat = listMainCat;
    }


    //for Sidemenu in BookMark settings
    public int getIsHubFunctn() {
        return isHubFunctn;
    }

    public void setIsHubFunctn(int isHubFunctn) {
        this.isHubFunctn = isHubFunctn;
    }

    public int getParCatId() {
        return parCatId;
    }

    public void setParCatId(int parCatId) {
        this.parCatId = parCatId;
    }


    public String getParCatName() {
        return parCatName;
    }

    public void setParCatName(String parCatName) {
        this.parCatName = parCatName;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }


}
