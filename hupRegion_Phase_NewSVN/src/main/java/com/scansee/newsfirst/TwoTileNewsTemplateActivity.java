
package com.scansee.newsfirst;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.MenuPropertiesActivity;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.model.TileTemplateCategoryModel;
import com.scansee.newsfirst.model.TileTemplateModel;
import com.scansee.newsfirst.model.TileTemplateRequestModel;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sharanamma on 23-06-2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class TwoTileNewsTemplateActivity extends MenuPropertiesActivity {
    private GridView mGridView;
    private static final String TAG = TwoTileNewsTemplateActivity.class.getSimpleName();
    private Activity activity;
    private RelativeLayout customParent;
    private ConnectionUtils objConnectionUtils;
    private ArrayList<TileTemplateCategoryModel> tileTempCategoryList = new ArrayList<>();
    private ProgressDialog progDialog;
    private int mScreenHeight;
    private int mScreenWidth;
    private ImageView bannerImage, drawerIcon;
    private ProgressBar progressBar;
    private LinearLayout bookMarkList;
    private String mDateCreated = null, mLinkID = "0", mLevel = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_tile__news_template);
        getIntentData();
        // Added by Supriya
        // Setting level to in parent activity to navigate and call api for proper level(Which is
        // required to differenciate
        // between MainMenu and SubMenu)
        previousMenuLevel = mLevel;
        mLinkId = mLinkID;
        bindView();
        listItemClick();
        setUpDrawer();
        objConnectionUtils = new ConnectionUtils(this);
        getDeviceDimensions();

    }

    private void setUpDrawer() {

        if (mLevel.equals("1"))
        {
            GlobalConstants.isFromNewsTemplate = true;
            GlobalConstants.className = Constants.NEWS_TILE;
        }
        CommonConstants.previousMenuLevel = mLevel;
        NewsTemplateDataHolder.className = Constants.NEWS_TILE;
        CommonConstants.commonLinkId = mLinkID;

        callSideMenuApi((CustomNavigation) findViewById(R.id.custom_navigation), true);
    }

    private void bindView() {
        drawerIcon = (ImageView) findViewById(R.id.drawer_icon);
        bookMarkList = (LinearLayout) findViewById(R.id.book_mark);
        mGridView = (GridView) findViewById(R.id.grid_view);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        bannerImage = (ImageView) findViewById(R.id.banner_image);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        activity = TwoTileNewsTemplateActivity.this;
        customParent = (RelativeLayout) findViewById(R.id.custom_parent);
        ImageView customMoreView = (ImageView) findViewById(R.id.more_view);
        customMoreView.setVisibility(View.GONE);
        TextView CustomMore = (TextView) findViewById(R.id.more);
        CustomMore.setVisibility(View.GONE);
        if (!mLevel.equals("1")) {
            ImageView backImage = ((ImageView) findViewById(R.id.back_btn));
            backImage.setVisibility(View.VISIBLE);
            //Loading back image
            if (Constants.getNavigationBarValues("bkImgPath") != null) {
                Picasso.with(this).load(Constants.getNavigationBarValues("bkImgPath").replace(" ",
                        "%20")).placeholder(R.drawable.loading_button).into
                        (backImage);
            } else {
                backImage.setBackgroundResource(R.drawable.ic_action_back);
            }
            backImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackClick();
                }
            });
        }
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Constants.LINKID)) {
            mLinkID = intent.getStringExtra(Constants.LINKID);
        }
        if (intent != null && intent.hasExtra(Constants.ITEMID)) {
            mItemId = intent.getStringExtra(Constants.ITEMID);
        }
        if (intent != null && intent.hasExtra(Constants.LEVEL)) {
            mLevel = intent.getStringExtra(Constants.LEVEL);
        }
    }

    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.drawer_icon:
                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.searchView:
                Intent intent = new Intent(activity, NewsSearchActivity.class);
                startActivity(intent);
                break;
            case R.id.HomeView:
                if (SortDepttNTypeScreen.subMenuDetailsList != null) {
                    SortDepttNTypeScreen.subMenuDetailsList.clear();
                    SubMenuStack.clearSubMenuStack();
                    SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
                }
                startMenuActivity();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {

        NewsTemplateDataHolder.className = Constants.NEWS_TILE;
        CommonConstants.commonLinkId = mLinkID;
        CommonConstants.previousMenuLevel = mLevel;

        isRefresh = false;
        super.onResume();
        getCategories();

    }

    private void gotoSubPage(String catType, String catTxtColor) {
        Intent intent = new Intent(this, SubpageActivty.class);
        intent.putExtra("templateType", "subpage");
        intent.putExtra("sideMenuCat", catType);
        intent.putExtra("catTxtColor", catTxtColor);
        intent.putExtra("isSideBar", "1");
        intent.putExtra(Constants.LEVEL, mLevel);
        intent.putExtra(Constants.LINKID, mLinkID);
        startActivity(intent);
    }

    private void listItemClick() {
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (tileTempCategoryList.get(position).getNonfeedlink() != null)
                {
                    CustomTitleBar.isNonFeed = true;

                    Intent intent = new Intent(activity,
                            ScanseeBrowserActivity.class);
                    intent.putExtra("module","news");
                    intent.putExtra("title",tileTempCategoryList.get(position).getCatName());
                    intent.putExtra("textColor",tileTempCategoryList.get(position).getCatTxtColor());
                    intent.putExtra("textBgColor",tileTempCategoryList.get(position).getCatColor());
                    intent.putExtra("backButtonColor",tileTempCategoryList.get(position).getBackButtonColor());
                    intent.putExtra(CommonConstants.URL, tileTempCategoryList.get(position).getNonfeedlink());
                    activity.startActivity(intent);
                }else{
                    gotoSubPage(tileTempCategoryList.get(position).getCatName(), tileTempCategoryList
                            .get(position).getCatTxtColor());
                }
            }
        });
    }

    private void getCategoryList(TileTemplateRequestModel requestObj) {
        RestClient.getInstance().getTileTemplateCategories(requestObj, new
                Callback<TileTemplateModel>() {
                    @Override
                    public void success(TileTemplateModel tileTemplateModel, Response response) {
                        Log.d(TAG, "Success");
                        try {
                            if (progDialog != null && progDialog.isShowing()) {
                                progDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            //Check for template change
                            if (tileTemplateModel.getResponseCode().equals("10000")) {
                                if (tileTemplateModel.getTemplateChanged() == 1) {
                                    if (tileTemplateModel.getNewtempName().equalsIgnoreCase
                                            (Constants
                                                    .COMBINATION)) {
                                        Intent intent = new Intent(activity,
                                                CombinationTemplate.class);
                                        intent.putExtra(Constants.LEVEL, mLevel);
                                        intent.putExtra(Constants.LINKID, mLinkID);
                                        startActivity(intent);
                                        finish();
                                    } else if (tileTemplateModel.getNewtempName().equalsIgnoreCase
                                            (Constants
                                                    .SCROLLING)) {
                                        Intent intent = new Intent(activity,
                                                ScrollingPageActivity.class);
                                        intent.putExtra(Constants.LEVEL, mLevel);
                                        intent.putExtra(Constants.LINKID, mLinkID);
                                        startActivity(intent);
                                        finish();
                                    } else if (tileTemplateModel.getNewtempName().equalsIgnoreCase
                                            (Constants
                                                    .NEWS_TILE)) {
                                        setUpDrawer();
                                        loadValues(tileTemplateModel);
                                    } else {
                                        GlobalConstants.isFromNewsTemplate = false;
                                        SubMenuStack.clearSubMenuStack();
                                        MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null,
                                                null, 0,
                                                null, mLinkId, null, null, null, null, "1", null,
                                                null, null,
                                                null, null, null, null);
                                        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
                                        MenuAsyncTask menu = new MenuAsyncTask(activity);
                                        menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "1",
                                                "0",
                                                "0", "None", "0", "0");
                                    }
                                } else {
                                    loadValues(tileTemplateModel);
                                }
                                // Creates bottom button singleton
                                BottomButtons bottomButtons = new BottomButtons(CommonConstants.smNewsBtnFontColor,
                                        CommonConstants.smNewsBtnColor, CommonConstants.mBtnNewsFontColor,
                                        CommonConstants.mBtnNewsColor, mLinkID, mItemId,
                                        CommonConstants.mNewsBkgrdColor, CommonConstants.mNewsBkgrdImage, CommonConstants.smNewsBkgrdColor,
                                        CommonConstants.smNewsBkgrdImage,
                                        CommonConstants.newsCityExpImgUlr, null, null,
                                        null, null, null, null, CommonConstants.newsCityExpId,
                                        mLevel, menuName,CommonConstants.mNewsFontColor ,CommonConstants.smNewsFontColor );

                                BottomButtonInfoSingleton.clearBottomButtonInfoSingleton();
                                BottomButtonInfoSingleton
                                        .createBottomButtonsSingleton(bottomButtons);
                            } else if (tileTemplateModel.getResponseCode().equals("10005")) {
                                if (tileTemplateModel.getHumbergurImgPath() != null) {
                                    Constants.setNavigationBarValues("hamburgerImg", tileTemplateModel.getHumbergurImgPath());
                                }
                                if (tileTemplateModel.getHumbergurImgPath() != null) {
                                    Constants.setNavigationBarValues("hamburgerImg", tileTemplateModel.getHumbergurImgPath());
                                }
                                loadHambergerMenu();
                                Toast.makeText(activity, tileTemplateModel
                                        .getResponseText(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ignored) {
                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.d(TAG, "failure");
                        if (progDialog != null && progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                        if (retrofitError.getResponse() != null) {
                            Log.d(TAG, "failure : " + retrofitError
                                    .getResponse().getReason());
                        }
                    }
                });
    }

    private void getCategories() {
        if (objConnectionUtils.getConnectivityStatus() == Constants
                .TYPE_NOT_CONNECTED) {
            Toast.makeText(this, getResources().getString(R.string.enable_internet), Toast
                    .LENGTH_SHORT).show();
        } else {
            showLoading();
            getCategoryList(new UrlRequestParams().getTileTempRequestObject(mDateCreated, mLinkID,
                    mLevel));
        }
    }


    private void loadValues(TileTemplateModel tileTemplateModel) {

        //Getting Navigation bar values and storing it to shared preference to access
        // from all the screens
        if (tileTemplateModel.getBkImgPath() != null) {
            Constants.setNavigationBarValues("bkImgPath", tileTemplateModel.getBkImgPath());
        }
        if (tileTemplateModel.getHomeImgPath() != null) {
            Constants.setNavigationBarValues("homeImgPath", tileTemplateModel.getHomeImgPath());
        }
        if (tileTemplateModel.getHumbergurImgPath() != null) {
            Constants.setNavigationBarValues("hamburgerImg", tileTemplateModel.getHumbergurImgPath());
        }
        if (tileTemplateModel.getTitleTxtColor() != null) {
            Constants.setNavigationBarValues("titleTxtColor", tileTemplateModel.getTitleTxtColor
                    ());
        }
        if (tileTemplateModel.getTitleBkGrdColor() != null) {
            Constants.setNavigationBarValues("titleBkGrdColor", tileTemplateModel
                    .getTitleBkGrdColor());
        }

        tileTempCategoryList = tileTemplateModel.getItems();
        mDateCreated = tileTemplateModel.getModifiedDate();
        TwoTileNewsAdapter towTileAdapter = new TwoTileNewsAdapter(TwoTileNewsTemplateActivity
                .this,
                tileTempCategoryList
                , mScreenWidth, mScreenHeight, mGridView.getPaddingBottom());
        mGridView.setAdapter(towTileAdapter);
        if (bookMarkList.getChildCount() != 0) {
            bookMarkList.removeAllViews();
        }
        addBookMarkView(tileTempCategoryList);

        if (tileTemplateModel.getBannerImg() != null) {
            progressBar.setVisibility(View.VISIBLE);
            Picasso.with(activity).load(tileTemplateModel.getBannerImg().replaceAll(" ", "%20")
            ).into(bannerImage, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
        loadHambergerMenu();
        if (Constants.getNavigationBarValues("titleBkGrdColor") != null) {
            customParent.setBackgroundColor(Color.parseColor(Constants.getNavigationBarValues
                    ("titleBkGrdColor")));
        }

    }

    private void loadHambergerMenu() {
        if (Constants.getNavigationBarValues("hamburgerImg") != null) {
            Picasso.with(this).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (drawerIcon);
        }
    }

    private void addBookMarkView(final ArrayList<TileTemplateCategoryModel> tileTempCategoryList) {
        try {
            int size = tileTempCategoryList.size();
            int count = 0;
            while (count != size) {
                LayoutInflater inflater =
                        (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View bookMarkView = inflater.inflate(R.layout.book_mark_view, bookMarkList, false);
                final LinearLayout catView = (LinearLayout) bookMarkView.findViewById(R.id
                        .ctegory_view);
                TextView bookMark = (TextView) bookMarkView.findViewById(R.id.book_mark);
                String category = tileTempCategoryList.get(count).getCatName();
                if (category != null && !category.equalsIgnoreCase("")) {
                    bookMark.setText(category);
                }
                final int finalCount = count;
                catView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        if (tileTempCategoryList.get(finalCount).getNonfeedlink() != null)
                        {
                            CustomTitleBar.isNonFeed = true;
                            Intent intent = new Intent(activity,
                                    ScanseeBrowserActivity.class);
                            intent.putExtra("module","news");
                            intent.putExtra("title",tileTempCategoryList.get(finalCount).getCatName());
                            intent.putExtra("textColor",tileTempCategoryList.get(finalCount).getCatTxtColor());
                            intent.putExtra("textBgColor",tileTempCategoryList.get(finalCount).getCatColor());
                            intent.putExtra("backButtonColor",tileTempCategoryList.get(finalCount).getBackButtonColor());
                            intent.putExtra(CommonConstants.URL, tileTempCategoryList.get(finalCount).getNonfeedlink());
                            activity.startActivity(intent);
                        } else
                            gotoSubPage(tileTempCategoryList.get(finalCount).getCatName(),
                                    tileTempCategoryList
                                            .get(finalCount).getCatTxtColor());
                    }
                });
                bookMarkList.addView(bookMarkView);
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getDeviceDimensions() {
        final int[] bookMarkHeight = new int[1];
        ViewTreeObserver bookMarkObserver = bookMarkList.getViewTreeObserver();
        bookMarkObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                bookMarkHeight[0] = bookMarkList.getMeasuredHeight();
                ViewTreeObserver obs = bookMarkList.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    obs.removeGlobalOnLayoutListener(this);
                }
            }
        });

        ViewTreeObserver observer = mGridView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                mScreenHeight = mScreenHeight - bookMarkHeight[0];
                mScreenHeight = mGridView.getMeasuredHeight();
                mScreenWidth = mGridView.getMeasuredWidth();
                Log.d(TAG, " Device width " + mScreenWidth);
                Log.d(TAG, " Device height " + mScreenHeight);
                ViewTreeObserver obs = mGridView.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    obs.removeGlobalOnLayoutListener(this);
                }
            }

        });
    }

    private void showLoading() {
        try {
            progDialog = new ProgressDialog(this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage("Please Wait..");
            progDialog.setCancelable(false);
            progDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        onBackClick();

    }

    private void onBackClick() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
        }
        super.onBackPressed();

    }
}
