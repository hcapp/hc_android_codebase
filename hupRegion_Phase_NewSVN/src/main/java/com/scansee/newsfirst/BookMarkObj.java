package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 5/18/2016.
 */
public class BookMarkObj {
    public String getBkImgPath() {
        return bkImgPath;
    }

    public void setBkImgPath(String bkImgPath) {
        this.bkImgPath = bkImgPath;
    }

    public String getHomeImgPath() {
        return homeImgPath;
    }

    public void setHomeImgPath(String homeImgPath) {
        this.homeImgPath = homeImgPath;
    }

    public String getWeatherURL() {
        return weatherURL;
    }

    public void setWeatherURL(String weatherURL) {
        this.weatherURL = weatherURL;
    }

    public String getBannerImg() {
        return bannerImg;
    }

    public void setBannerImg(String bannerImg) {
        this.bannerImg = bannerImg;
    }

    public String getTitleBkGrdColor() {
        return titleBkGrdColor;
    }

    public void setTitleBkGrdColor(String titleBkGrdColor) {
        this.titleBkGrdColor = titleBkGrdColor;
    }

    public int getIsHeader() {
        return isHeader;
    }

    private String bkImgPath;
    private String homeImgPath;
    private String weatherURL;
    private String bannerImg;

    public String getHamburgerImg() {
        return hamburgerImg;
    }

    public void setHamburgerImg(String hamburgerImg) {
        this.hamburgerImg = hamburgerImg;
    }

    private String hamburgerImg;
    private String titleBkGrdColor;
    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    private String responseText;

    public ArrayList<BookMarkList> getBookMarkList() {
        return bookMarkList;
    }

    public void setBookMarkList(ArrayList<BookMarkList> bookMarkList) {
        this.bookMarkList = bookMarkList;
    }

    private ArrayList<BookMarkList> bookMarkList ;































    private String category;
    private int isHeader;
    private String dragCategory;
    private boolean inBothSections;
    private int parCatId;
    private String sortOrder;
    private int isHubFunctn;

    public int getIsHubFunctn()
    {
        return isHubFunctn;
    }

    public void setIsHubFunctn(int isHubFunctn)
    {
        this.isHubFunctn = isHubFunctn;
    }

    public int getParCatId() {
        return parCatId;
    }

    public void setParCatId(int parCatId) {
        this.parCatId = parCatId;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }


    public boolean isInBothSections() {
        return inBothSections;
    }

    public void setInBothSections(boolean inBothSections) {
        this.inBothSections = inBothSections;
    }



    public String getDragCategory() {
        return dragCategory;
    }

    public void setDragCategory(String dragCategory) {
        this.dragCategory = dragCategory;
    }



    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int isHeader() {
        return isHeader;
    }

    public void setIsHeader(int isHeader) {
        this.isHeader = isHeader;
    }



}
