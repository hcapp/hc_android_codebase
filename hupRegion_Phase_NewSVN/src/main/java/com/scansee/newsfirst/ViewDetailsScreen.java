package com.scansee.newsfirst;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.ShareInformation;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by subramanya.v on 5/25/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class ViewDetailsScreen extends CustomTitleBar {
    private static ArrayList<String> linkList = new ArrayList<>();
    private  String linkDesc;
    private String category;
    private ImageFragmentPagerAdapter imageFragmentPagerAdapter;
    private ViewPager viewPager;
    private String catColor, catTxtColor;
    private int viewPosition;
    private LinearLayout parentView;
    private int mScreenHeight;
    private boolean isScrolling;
    private int itemId;
    private boolean isShareTaskCalled;
    private ShareInformation shareInfo;
    private ViewDetailsScreen mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe_fragment);
        mActivity = ViewDetailsScreen.this;
        bindView();
        getIntentData();
        shareInfo = new ShareInformation(
                mActivity, "", "",
                String.valueOf(itemId), "NewsDetails");
        setClickListener();
        // Title of the screen
        title.setText(R.string.details);
//        rightImage.setVisibility(View.GONE);
        //setting navigation bar background color
        if (catColor != null) {
            findViewById(R.id.title_layout).setBackgroundColor(Color.parseColor(catColor));
        }
        if (catTxtColor != null) {
            title.setTextColor(Color.parseColor(catTxtColor));
        }


        ViewTreeObserver observer = parentView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                mScreenHeight = parentView.getMeasuredHeight();
                ViewTreeObserver obs = parentView.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    obs.removeGlobalOnLayoutListener(this);
                }
                imageFragmentPagerAdapter = new ImageFragmentPagerAdapter(getSupportFragmentManager(), mScreenHeight, category,isScrolling,linkDesc);
                viewPager.setAdapter(imageFragmentPagerAdapter);
                viewPager.setCurrentItem(viewPosition);

            }

        });

    }
    private void setClickListener() {
        leftTitleImage
                .setBackgroundResource(R.drawable.share_btn_down);
        leftTitleImage
                .setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!Constants.GuestLoginId
                                .equals(UrlRequestParams.getUid()
                                        .trim())) {
                            shareInfo.shareTask(isShareTaskCalled);
                            isShareTaskCalled = true;
                        } else {
                            Constants
                                    .SignUpAlert(mActivity);
                            isShareTaskCalled = false;
                        }
                    }
                });

    }


    public static class ImageFragmentPagerAdapter extends FragmentPagerAdapter {

        private final int mScreenHeight;
        private final String category;
        private final boolean isScrolling;
        private final String linkDesc;

        public ImageFragmentPagerAdapter(FragmentManager fm, int mScreenHeight, String category, boolean isScrolling, String linkDesc) {
            super(fm);
            this.mScreenHeight = mScreenHeight;
            this.category = category;
            this.isScrolling = isScrolling;
            this.linkDesc = linkDesc;
        }

        @Override
        public int getCount() {
            return linkList.size();
        }

        @Override
        public Fragment getItem(int position) {
            return SwipeFragment.newInstance(position, mScreenHeight, category,isScrolling,linkDesc);
        }
    }


    public static class SwipeFragment extends Fragment {
        private MediaController mediaController;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View swipeView = inflater.inflate(R.layout.view_details_screen, container, false);
            final Context context = getActivity();
            Bundle bundle = getArguments();
            int position = bundle.getInt("position");
            int mScreenHeight = bundle.getInt("mScreenHeight");
            boolean isScrolling = bundle.getBoolean("isScrolling");
            final String category = bundle.getString("category");
            String linkDesc = bundle.getString("linkDesc");
            String url = linkList.get(position);
            ScrollView scrollPaent = (ScrollView) swipeView.findViewById(R.id.scroll_view);
            TextView detailsView = (TextView) swipeView.findViewById(R.id.details);
            if(isScrolling)
            {
                scrollPaent.setBackgroundColor(Color.WHITE);
                detailsView.setTextColor(Color.BLACK);
            }

            ImageView imageView = (ImageView) swipeView.findViewById(R.id.image_view);
            ImageView playButton = (ImageView) swipeView.findViewById(R.id.play_button);
            final ProgressBar progress = (ProgressBar)swipeView.findViewById(R.id.progress);
            final ProgressBar imageProgress = (ProgressBar)swipeView.findViewById(R.id.image_progress);
            final VideoView[] videoView = {(VideoView) swipeView.findViewById(R.id.video_view)};

            videoView[0].setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {

                    mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                        @Override
                        public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i1) {
                            progress.setVisibility(View.GONE);

                        }
                    });
                }
            });
            final FrameLayout frameLayout = (FrameLayout) swipeView.findViewById(R.id.frame_parent);

            if (!(category.equalsIgnoreCase("photos"))) {
                if (url != null) {
                    try {
                        ThumbNailDetailsAsyTask thumNail = new ThumbNailDetailsAsyTask(url, imageView,context);
                        thumNail.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                    playButton.setVisibility(View.VISIBLE);
                    videoView[0].setVisibility(View.GONE);
                }
                final String finalUrl = url;
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!(category.equalsIgnoreCase("photos"))) {
                            frameLayout.setVisibility(View.GONE);
                            progress.setVisibility(View.VISIBLE);
                            videoView[0].setVisibility(View.VISIBLE);
                            mediaController = new MediaController(context);
                            mediaController.setAnchorView(videoView[0]);
                            videoView[0].setMediaController(mediaController);
                            videoView[0].setVideoURI((Uri.parse(finalUrl)));
                            videoView[0].requestFocus();
                            videoView[0].start();

                        }
                    }
                });

            } else {
                videoView[0].setVisibility(View.GONE);
                url = url.replaceAll(" ", "%20");
                imageProgress.setVisibility(View.VISIBLE);
                Picasso.with(context).load(url).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        imageProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        imageProgress.setVisibility(View.GONE);
                    }
                });
            }


            int viewHeight = (mScreenHeight * 50) / 100;
            FrameLayout.LayoutParams imageParams = (FrameLayout.LayoutParams) imageView.getLayoutParams();
            imageParams.height = viewHeight;
            imageView.setLayoutParams(imageParams);

            RelativeLayout.LayoutParams videoParams = (RelativeLayout.LayoutParams) videoView[0].getLayoutParams();
            videoParams.height = viewHeight;
            videoView[0].setLayoutParams(videoParams);

            if (linkDesc != null) {
                detailsView.setText(Html.fromHtml(linkDesc));
            }
            return swipeView;
        }

        static SwipeFragment newInstance(int position, int mScreenHeight, String category, boolean isScrolling,String linkDesc) {
            SwipeFragment swipeFragment = new SwipeFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            bundle.putInt("mScreenHeight", mScreenHeight);
            bundle.putString("category", category);
            bundle.putString("linkDesc", linkDesc);
            bundle.putBoolean("isScrolling", isScrolling);
            swipeFragment.setArguments(bundle);
            return swipeFragment;
        }
    }

    private static class ThumbNailDetailsAsyTask extends AsyncTask<String, Void, Bitmap> {
        private final String uri;
        private final ImageView gridImage;
        private final Context context;
        private ProgressDialog mDialog;

        public ThumbNailDetailsAsyTask(String uri, ImageView gridImage, Context context) {
            this.gridImage = gridImage;
            this.uri = uri;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(context, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            gridImage.setImageBitmap(bitmap);
            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
            }
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap thumb = null;
            try {
                thumb = retriveVideoFrameFromVideo(uri);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            return thumb;
        }
    }

    private static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    private void getIntentData() {
        if (getIntent().hasExtra("link")) {
            linkList = getIntent().getStringArrayListExtra("link");
        }
        if (getIntent().hasExtra("desc")) {
            linkDesc = getIntent().getExtras().getString("desc");
        }
        if (getIntent().hasExtra("itemId")) {
            itemId =  getIntent().getExtras().getInt("itemId");
        }
        if (getIntent().hasExtra("category")) {
            category = getIntent().getExtras().getString("category");
        }
        if (getIntent().hasExtra("catColor")) {
            catColor = getIntent().getExtras().getString("catColor");
        }
        if (getIntent().hasExtra("viewPosition")) {
            viewPosition = getIntent().getExtras().getInt("viewPosition");
        }
        if (getIntent().hasExtra("catTxtColor")) {
            catTxtColor = getIntent().getExtras().getString("catTxtColor");
        }
        if (getIntent().hasExtra("scrollingScreen")) {
            isScrolling =  getIntent().getExtras().getBoolean("scrollingScreen");
        }
    }

    private void bindView() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        parentView = (LinearLayout) findViewById(R.id.parent_view);
    }

}


