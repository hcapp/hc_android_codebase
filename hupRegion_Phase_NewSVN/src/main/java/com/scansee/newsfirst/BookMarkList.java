package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 5/31/2016.
 */
public class BookMarkList {


    private String header;
    private ArrayList<ListCatDetails> listCatDetails;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }


    public ArrayList<ListCatDetails> getListCatDetails() {
        return listCatDetails;
    }

    public void setListCatDetails(ArrayList<ListCatDetails> listCatDetails) {
        this.listCatDetails = listCatDetails;
    }




}
