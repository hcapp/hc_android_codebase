package com.scansee.newsfirst;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by subramanya.v on 7/6/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class GridFragment extends Fragment {
    private int mScreenHeight;
    private int mScreenWidth;
    private String category;
    private int tabPosition;
    private FragmentActivity mContext;
    public SwipeRefreshLayout gridRefresh;
    private GridView grid;
    private subPageViewAdapter subPagetAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_fragment, container, false);
        gridRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.grid_refresh);
        mContext = getActivity();
        getIntentValues();
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setVisibility(View.GONE);
        gridRefresh.setVisibility(View.VISIBLE);
        displayVideoView(rootView);
        return rootView;
    }

    private void getIntentValues() {
        Bundle bundle = getArguments();
        mScreenHeight = bundle.getInt("screenHeight");
        mScreenWidth = bundle.getInt("screenWidth");
        tabPosition = bundle.getInt("position");
        category = bundle.getString("category");
    }

    private void displayVideoView(View rootView) {
        grid = (GridView) rootView.findViewById(R.id.grid_view);
        setGridClickListener();
        subPagetAdapter = new subPageViewAdapter
                (GridFragment.this.getActivity(), mScreenHeight,
                        mScreenWidth, category, grid, false, false, tabPosition);
        grid.setAdapter(subPagetAdapter);
        if (((ScrollingPageActivity) mContext).gridProgDialog != null && ((ScrollingPageActivity) mContext).gridProgDialog.isShowing()) {
            ((ScrollingPageActivity) mContext).gridProgDialog.dismiss();
            ((ScrollingPageActivity) mContext).gridProgDialog = null;
        }

    }

    private void setGridClickListener() {
        gridRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ((ScrollingPageActivity) mContext).startPullToRequest(
                        tabPosition, subPagetAdapter);
            }
        });

        grid.setOnScrollListener(new GridView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if ((firstVisibleItem + visibleItemCount) == totalItemCount) {

                    try {
                        if (((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getNextPage() == 1) {
                            if (((ScrollingPageActivity) mContext).gridProgDialog == null) {
                                ((ScrollingPageActivity) mContext).gridProgDialog = new ProgressDialog(mContext);
                                ((ScrollingPageActivity) mContext).gridProgDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                ((ScrollingPageActivity) mContext).gridProgDialog.setMessage("Please Wait..");
                                ((ScrollingPageActivity) mContext).gridProgDialog.setCancelable(false);
                                ((ScrollingPageActivity) mContext).gridProgDialog.show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String link;
                if (category.equalsIgnoreCase("photos")) {
                    link = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().get(position).getImage();
                } else {
                    link = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().get(position).getVideoLink();
                }
                ArrayList<String> links = null;
                if (link != null) {
                    String[] words = link.split(",");
                    links = new ArrayList<>();
                    Collections.addAll(links, words);
                }
                String lDesc = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().get(position).getlDesc();
                String catColor = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().get(position).getCatColor();
                String catTxtColor = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().get(position).getCatTxtColor();
                int itemId = ((ScrollingPageActivity) mContext).scrollingData.get(tabPosition)
                        .getItems().get(position).getItemID();

                Intent intent;

                if (links != null) {
                    if (links.size() > 1) {
                        intent = new Intent(getActivity(), GridChildView.class);
                        intent.putStringArrayListExtra("link", links);
                        intent.putExtra("category", category);
                        intent.putExtra("itemId", itemId);
                        intent.putExtra("catColor", catColor);
                        intent.putExtra("catTxtColor", catTxtColor);
                        intent.putExtra("desc", lDesc);
                        intent.putExtra("scrollingScreen", true);
                    } else {
                        intent = new Intent(getActivity(), ViewDetailsScreen.class);
                        intent.putStringArrayListExtra("link", links);
                        intent.putExtra("itemId", itemId);
                        intent.putExtra("category", category);
                        intent.putExtra("itemId", itemId);
                        intent.putExtra("catColor", catColor);
                        intent.putExtra("catTxtColor", catTxtColor);
                        intent.putExtra("desc", lDesc);
                        intent.putExtra("scrollingScreen", true);
                    }
                    startActivity(intent);
                }
                else
                {
                    CommonConstants.displayToast(mContext, mContext.getString(R.string
                            .video_alert));
                }
            }
        });

    }

}
