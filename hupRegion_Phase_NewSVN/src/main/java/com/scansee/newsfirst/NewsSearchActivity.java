package com.scansee.newsfirst;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.hubregion.R;

import java.util.ArrayList;


public class NewsSearchActivity extends CustomTitleBar {

    private int lastPosition = 0;

    public boolean isAlreadyLoading = false;
    public boolean nextPage;
    private ArrayList<Items> newsSearchList = new ArrayList<>();
    private String searchkey = "";

    private ListView newsSearchListView;
    private View moreResultsView;
    private String lowerLimit = "0";


    private final String isSideBar = "2";

    private GetNewsSearchAsynTask searchNewsAsynTask;

    NewsSearchListAdapter newsSearchAdapter;
    EditText newsSearchEdittext;
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (newsSearchAdapter != null) {
            newsSearchListView.setAdapter(null);
        }
        cancelAysnTask();
        searchkey = null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_search_list);


        newsSearchEdittext = (EditText) findViewById(R.id.news_search_edit_text);

        newsSearchListView = (ListView) findViewById(R.id.news_search_listview);


        findViewById(R.id.news_search_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsSearchEdittext.setText("");
                hideKeyboardItem();
            }
        });

        title.setText(R.string.search_text);
        leftTitleImage.setVisibility(View.GONE);
        rightImage.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboardItem();
                cancelAysnTask();
                finish();
            }
        });

        hideKeyboardItem();
        moreResultsView = getLayoutInflater().inflate(R.layout
                .listitem_get_retailers_viewmore, newsSearchListView, false);
        newsSearchAdapter = null;
        try {
            newsSearchListView.removeFooterView(moreResultsView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        newsSearchEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() >= 3) {
                    newsSearchList = new ArrayList<>();
                    newsSearchAdapter = null;
                    lowerLimit = "0";
                    searchkey = s.toString().trim();
                    callNewsSearch();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    cancelAysnTask();
                    searchkey = "";
                    lowerLimit = "0";
                    newsSearchList = new ArrayList<>();
                    newsSearchAdapter = null;
                    newsSearchListView.setVisibility(View.GONE);


                }
            }
        });
        setItemClickListener();
    }

    private void setItemClickListener() {
        newsSearchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(NewsSearchActivity.this, NewsDetailsActivity.class);
                intent.putExtra("itemId", newsSearchList.get(position).getItemID());
                startActivity(intent);
            }
        });
    }

    private void hideKeyboardItem() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context
                    .INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void cancelAysnTask() {

        if (searchNewsAsynTask != null && !searchNewsAsynTask.isCancelled()) {
            searchNewsAsynTask.cancel(true);
        }

        searchNewsAsynTask = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelAysnTask();
    }

    private void callNewsSearch() {

        lastPosition = newsSearchList.size();


        if (searchNewsAsynTask != null && !searchNewsAsynTask.isCancelled()) {
            searchNewsAsynTask.cancel(true);
        }
        searchNewsAsynTask = null;

        searchNewsAsynTask = new GetNewsSearchAsynTask(NewsSearchActivity.this, searchkey,
                isSideBar, lowerLimit, newsSearchList);
        searchNewsAsynTask.execute();
    }


    public void loadSearchList(ArrayList<Items> searchResult, boolean nextpage, String
            lowerLimit, String responseText) {

        isAlreadyLoading = false;
        newsSearchList = new ArrayList<>();
        this.lowerLimit = lowerLimit;
        this.nextPage = nextpage;
        this.newsSearchList = searchResult;

        newsSearchListView.setVisibility(View.VISIBLE);

        if (nextpage) {
            try {
                newsSearchListView.removeFooterView(moreResultsView);
            } catch (Exception e) {
                e.printStackTrace();
            }
            newsSearchListView.addFooterView(moreResultsView);

        } else {
            newsSearchListView.removeFooterView(moreResultsView);
        }

        if (newsSearchAdapter == null) {
            hideKeyboardItem();

            if (this.newsSearchList.size() > 0) {
                newsSearchAdapter = new NewsSearchListAdapter(NewsSearchActivity.this, this
                        .newsSearchList);
                newsSearchListView.setAdapter(newsSearchAdapter);
                newsSearchListView.setSelection(Integer.valueOf(lastPosition));


            } else {
                newsSearchAdapter = new NewsSearchListAdapter(NewsSearchActivity.this, this
                        .newsSearchList);
                newsSearchListView.setAdapter(newsSearchAdapter);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        NewsSearchActivity.this);

                alertDialogBuilder.setMessage(responseText);
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialogBuilder.show();
            }
        } else {
            newsSearchAdapter.notifyDataSetChanged();
        }


    }


    public void loadMoreDataForSearch() {

        isAlreadyLoading = true;

        searchkey = ((EditText) findViewById(R.id.news_search_edit_text)).getText().toString();

        new GetNewsSearchAsynTask(NewsSearchActivity.this, searchkey,
                isSideBar, lowerLimit, newsSearchList).execute();
    }


}


