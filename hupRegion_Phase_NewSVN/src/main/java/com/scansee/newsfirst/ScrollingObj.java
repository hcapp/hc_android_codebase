package com.scansee.newsfirst;

import java.util.LinkedList;

/**
 * Created by subramanya.v on 5/18/2016.
 */
public class ScrollingObj  {

    private String responseText;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    private String responseCode;
    private String bannerImg;
    private int maxCnt;
    private int nextPage;
    private int lowerLimitFlag;

    private int templateChanged;
    private String modifiedDate;
    private String newtempName;
    LinkedList<Items> items = new LinkedList<>();
    private boolean isLoaded;
    int count;
    String viewSummary;

    String content;
    String description;


    public String getBackButtonColor() {
        return backButtonColor;
    }

    public void setBackButtonColor(String backButtonColor) {
        this.backButtonColor = backButtonColor;
    }

    String backButtonColor;
    String subPagesummary;
    private String subPageName;

    public String getBkImgPath()
    {
        return bkImgPath;
    }

    public void setBkImgPath(String bkImgPath)
    {
        this.bkImgPath = bkImgPath;
    }

    public String getHomeImgPath()
    {
        return homeImgPath;
    }

    public void setHomeImgPath(String homeImgPath)
    {
        this.homeImgPath = homeImgPath;
    }

    public String getTitleTxtColor()
    {
        return titleTxtColor;
    }

    public void setTitleTxtColor(String titleTxtColor)
    {
        this.titleTxtColor = titleTxtColor;
    }

    public String getTitleBkGrdColor()
    {
        return titleBkGrdColor;
    }

    public void setTitleBkGrdColor(String titleBkGrdColor)
    {
        this.titleBkGrdColor = titleBkGrdColor;
    }

    public String getWeatherURL()
    {
        return weatherURL;
    }

    public void setWeatherURL(String weatherURL)
    {
        this.weatherURL = weatherURL;
    }

    private String bkImgPath;
    private String homeImgPath;

    public String getHumbergurImgPath() {
        return humbergurImgPath;
    }

    public void setHumbergurImgPath(String humbergurImgPath) {
        this.humbergurImgPath = humbergurImgPath;
    }

    private String humbergurImgPath;
    private String titleTxtColor;
    private String titleBkGrdColor;
    private String weatherURL;


    public String getSubPageName() {
        return subPageName;
    }

    public void setSubPageName(String subPageName) {
        this.subPageName = subPageName;
    }


    public int getTemplateChanged() {
        return templateChanged;
    }

    public void setTemplateChanged(int templateChanged) {
        this.templateChanged = templateChanged;
    }

    public String getNewtempName() {
        return newtempName;
    }

    public void setNewtempName(String newtempName) {
        this.newtempName = newtempName;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public boolean isLoaded()
    {
        return false;
    }

    public void setIsLoaded(boolean isLoaded)
    {
        this.isLoaded = isLoaded;
    }


    public boolean isFirst() {
        return isFirst;
    }

    public void setIsFirst(boolean isFirst) {
        this.isFirst = isFirst;
    }

    private boolean isFirst;


    public String getBannerImg() {
        return bannerImg;
    }

    public void setBannerImg(String bannerImg) {
        this.bannerImg = bannerImg;
    }

    public LinkedList<Items> getItems() {
        return items;
    }

    public void setItems(LinkedList<Items> items) {
        this.items.addAll(items);
    }

    public int getLowerLimitFlag() {
        return lowerLimitFlag;
    }

    public void setLowerLimitFlag(int lowerLimitFlag) {
        this.lowerLimitFlag = lowerLimitFlag;
    }

    public int getMaxCnt() {
        return maxCnt;
    }

    public void setMaxCnt(int maxCnt) {
        this.maxCnt = maxCnt;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubPagesummary() {
        return subPagesummary;
    }

    public void setSubPagesummary(String subPagesummary) {
        this.subPagesummary = subPagesummary;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getViewSummary() {
        return viewSummary;
    }

    public void setViewSummary(String viewSummary) {
        this.viewSummary = viewSummary;
    }


}
