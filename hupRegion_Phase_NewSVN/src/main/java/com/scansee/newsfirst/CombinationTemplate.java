package com.scansee.newsfirst;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.com.hubcity.model.CombinationalTempRequest;
import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.MenuPropertiesActivity;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.model.CategoryListModel;
import com.scansee.newsfirst.model.GetCombinationTempModel;
import com.scansee.newsfirst.model.TileTemplateCategoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by supriya.m on 5/18/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class CombinationTemplate extends MenuPropertiesActivity {
    private static final String TAG = CombinationTemplate.class.getSimpleName();
    private ListView mListView;
    private View moreFooterView;
    private int preLast = 0;
    private int mLowerLimit = 0;
    private int mNextPage = 0;
    private final ArrayList<CategoryListModel> mCategoryList = new ArrayList<>();
    private CombinationListAdapter mComboTempAdapter;
    private boolean isDataLoaded = false;

    private ProgressDialog progDialog;

    private FragmentActivity mContext;

    private String mDateCreated = null, mLinkID = "0", mLevel = "1", mItemId = "0";
    private ImageView bannerImage,drawerIcon;;
    private RelativeLayout customParent;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isRefreshNeeded = false;
    private LinearLayout bookMarkList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.combination_template);
        mContext = CombinationTemplate.this;
        getIntentData();
        // Added by Supriya
        // Setting level to in parent activity to navigate and call api for proper level(Which is
        // required to differenciate
        // between MainMenu and SubMenu)
        previousMenuLevel = mLevel;
        mLinkId = mLinkID;
        bindViews();
        setScrollListener();
        setUpDrawer();
        setSwipeRefreshListener();

    }

    //Added by Sharanamma
    private void setUpDrawer() {

        if (mLevel.equals("1")) {
            GlobalConstants.isFromNewsTemplate = true;
            GlobalConstants.className = Constants.COMBINATION;
        }
        CommonConstants.previousMenuLevel = mLevel;
        CommonConstants.commonLinkId = mLinkID;
        NewsTemplateDataHolder.className = Constants.COMBINATION;


        callSideMenuApi((CustomNavigation) findViewById(R.id.custom_navigation), true);
    }

    //Code added by Supriya
    @Override
    protected void onResume() {
        CommonConstants.previousMenuLevel = mLevel;
        NewsTemplateDataHolder.className = Constants.COMBINATION;
        CommonConstants.commonLinkId = mLinkID;

        Log.d(TAG, "Inside onResume");
        isRefresh = false;
        super.onResume();
        mLowerLimit = 0;
        mCategoryList.clear();
        preLast = 0;
        showLoading();
        isRefreshNeeded = true;
        getTemplateData(UrlRequestParams.getComboTemplate(mLowerLimit, mDateCreated, mLinkID,
                mLevel));

    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Constants.LINKID)) {
            mLinkID = intent.getStringExtra(Constants.LINKID);
        }
        if (intent != null && intent.hasExtra(Constants.ITEMID)) {
            mItemId = intent.getStringExtra(Constants.ITEMID);
        }
        if (intent != null && intent.hasExtra(Constants.LEVEL)) {
            mLevel = intent.getStringExtra(Constants.LEVEL);
        }
    }


    private void showLoading() {
        progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();
    }

    private void bindViews() {
        drawerIcon = (ImageView)findViewById(R.id.drawer_icon);
        bookMarkList = (LinearLayout) findViewById(R.id.book_mark);
        mListView = (ListView) findViewById(R.id.combination_template_list);
        moreFooterView = getLayoutInflater().inflate(
                R.layout.pagination, null);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        bannerImage = (ImageView) findViewById(R.id.banner_image);
        customParent = (RelativeLayout) findViewById(R.id.custom_parent);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        ImageView customMoreView = (ImageView) findViewById(R.id.more_view);
        customMoreView.setVisibility(View.GONE);
        TextView CustomMore = (TextView) findViewById(R.id.more);
        CustomMore.setVisibility(View.GONE);
        if (!mLevel.equals("1")) {
            ImageView backImage = ((ImageView) findViewById(R.id.back_btn));
            backImage.setVisibility(View.VISIBLE);
            //Loading back image
            if (Constants.getNavigationBarValues("bkImgPath") != null) {
                Picasso.with(this).load(Constants.getNavigationBarValues("bkImgPath").replace(" ",
                        "%20")).placeholder(R.drawable.loading_button).into
                        (backImage);
            } else {
                backImage.setBackgroundResource(R.drawable.ic_action_back);
            }
            backImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  onBackClick();
                }
            });
        }
        // Code added by Supriya

//		if (!mLevel.equals("1")) {
//			//required in submenu
//			HomeView.setVisibility(View.VISIBLE);
//			//loading home image url to imageview
//			if (Constants.getNavigationBarValues("homeImgPath") != null) {
//				Picasso.with(this).load(Constants.getNavigationBarValues("homeImgPath").replace
//						(" ",
//								"%20")).placeholder(R.drawable.loading_button).into
//						(HomeView);
//			} else {
//				HomeView.setBackgroundResource(R.drawable.mainmenu_white);
//			}
//
//			(findViewById(R.id.more_view)).setVisibility(View.GONE);
//			(findViewById(R.id.more)).setVisibility(View.GONE);
//			(findViewById(R.id.searchView)).setVisibility(View.GONE);
//		}
    }

    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.drawer_icon:
                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.searchView:
                Intent intent = new Intent(CombinationTemplate.this, NewsSearchActivity.class);
                startActivity(intent);

                break;
            case R.id.HomeView:
                if (SortDepttNTypeScreen.subMenuDetailsList != null) {
                    SortDepttNTypeScreen.subMenuDetailsList.clear();
                    SubMenuStack.clearSubMenuStack();
                    SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
                }
                startMenuActivity();
                break;
            default:
                break;
        }
    }

    private void getTemplateData(CombinationalTempRequest requestInfo) {
        RestClient.getInstance().getCombinationTemplate(requestInfo, new
                Callback<GetCombinationTempModel>() {
                    @Override
                    public void success(GetCombinationTempModel getCombinationTempModel, Response
                            response) {
                        Log.d(TAG, "combination template success : ");
                        try {
                            if (progDialog != null && progDialog.isShowing()) {
                                progDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        removeMoreView();
                        if (getCombinationTempModel.getResponseCode().equals("10000")) {
                            //Check for template change
                            if (getCombinationTempModel.getTemplateChanged() == 1) {
                                onTemplateChange(getCombinationTempModel);
                            } else {
                                loadValues(getCombinationTempModel);
                                loadRefreshData(getCombinationTempModel);
                                if (Constants.getNavigationBarValues("hamburgerImg") != null) {
                                    Picasso.with(mContext).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                                            "%20")).placeholder(R.drawable.loading_button).into
                                            (drawerIcon);
                                }
                            }
                            // Creates bottom button singleton
                            BottomButtons bottomButtons = new BottomButtons(CommonConstants.smNewsBtnFontColor,
                                    CommonConstants.smNewsBtnColor, CommonConstants.mBtnNewsFontColor,
                                    CommonConstants.mBtnNewsColor, mLinkID, mItemId,
                                    CommonConstants.mNewsBkgrdColor, CommonConstants.mNewsBkgrdImage, CommonConstants.smNewsBkgrdColor,
                                    CommonConstants.smNewsBkgrdImage,
                                    CommonConstants.newsCityExpImgUlr, null, null,
                                    null, null, null, null, CommonConstants.newsCityExpId,
                                    mLevel, menuName,CommonConstants.mNewsFontColor ,CommonConstants.smNewsFontColor );

                            BottomButtonInfoSingleton.clearBottomButtonInfoSingleton();
                            BottomButtonInfoSingleton
                                    .createBottomButtonsSingleton(bottomButtons);
                        } else if (getCombinationTempModel.getResponseCode().equals("10005")) {
                            if (getCombinationTempModel.getHumbergurImgPath() != null) {
                                Constants.setNavigationBarValues("hamburgerImg", getCombinationTempModel.getHumbergurImgPath());
                            }
                            if (Constants.getNavigationBarValues("hamburgerImg") != null) {
                                Picasso.with(mContext).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                                        "%20")).placeholder(R.drawable.loading_button).into
                                        (drawerIcon);
                            }
                            Toast.makeText(CombinationTemplate.this, getCombinationTempModel
                                    .getResponseText(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        onFailure(retrofitError);
                    }
                });
    }

    private void onFailure(RetrofitError retrofitError) {
        if (progDialog != null && progDialog.isShowing()) {
            progDialog.dismiss();
        }
        removeMoreView();
        if (retrofitError.getResponse() != null) {
            Log.d(TAG, "failure : " + retrofitError
                    .getResponse().getReason());
        }
    }

    private void getTemplatePaginationValues(CombinationalTempRequest requestInfo) {

        RestClient.getInstance().getCombinationTemplate(requestInfo, new
                Callback<GetCombinationTempModel>() {
                    @Override
                    public void success(GetCombinationTempModel getCombinationTempModel, Response
                            response) {
                        Log.d(TAG, "combination template pagination success : ");
                        try {
                            if (progDialog != null && progDialog.isShowing()) {
                                progDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        removeMoreView();
                        if (getCombinationTempModel.getResponseCode().equals("10000")) {
                            //Check for template change
                            if (getCombinationTempModel.getTemplateChanged() == 1) {
                                onTemplateChange(getCombinationTempModel);
                            } else {
                                loadValues(getCombinationTempModel);
                                loadPaginationData(getCombinationTempModel);
                            }

                        } else {
                            Toast.makeText(CombinationTemplate.this, getCombinationTempModel
                                    .getResponseText(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        onFailure(retrofitError);
                    }
                });
    }

    private void removeMoreView() {
        mListView.removeFooterView(moreFooterView);
        moreFooterView.setVisibility(View.GONE);
    }

    //Code added by Supriya
    private void loadValues(GetCombinationTempModel getCombinationTempModel) {
        //Getting Navigation bar values and storing it to shared preference to access
        // from all the screens
        if (getCombinationTempModel.getBkImgPath() != null) {
            Constants.setNavigationBarValues("bkImgPath", getCombinationTempModel.getBkImgPath());
        }
        if (getCombinationTempModel.getHomeImgPath() != null) {
            Constants.setNavigationBarValues("homeImgPath", getCombinationTempModel.getHomeImgPath
                    ());
        }
        if (getCombinationTempModel.getHumbergurImgPath() != null) {
            Constants.setNavigationBarValues("hamburgerImg", getCombinationTempModel.getHumbergurImgPath());
        }
        if (getCombinationTempModel.getTitleTxtColor() != null) {
            Constants.setNavigationBarValues("titleTxtColor", getCombinationTempModel
                    .getTitleTxtColor());
        }
        if (getCombinationTempModel.getTitleBkGrdColor() != null) {
            Constants.setNavigationBarValues("titleBkGrdColor", getCombinationTempModel
                    .getTitleBkGrdColor());
        }

        //Code added by Supriya
        mLowerLimit = getCombinationTempModel.getLowerLimitFlag();
        mNextPage = getCombinationTempModel.getNextPage();
        mDateCreated = getCombinationTempModel.getModifiedDate();
        isDataLoaded = true;

    }

    private void loadPaginationData(GetCombinationTempModel getCombinationTempModel) {
        mCategoryList.addAll(getCombinationTempModel.getCategoryList());
        addBookMarkFields();
        getCombinationTempModel.setCategoryList(mCategoryList);
        mComboTempAdapter.notifyDataSetChanged();
    }

    private void loadRefreshData(GetCombinationTempModel getCombinationTempModel) {

        if (isRefreshNeeded) {
            Log.d(TAG, "refresh success");
            isRefreshNeeded = false;
        }
        mCategoryList.clear();
        mCategoryList.addAll(getCombinationTempModel.getCategoryList());
        mComboTempAdapter = new CombinationListAdapter(CombinationTemplate
                .this, mListView,
                mCategoryList, swipeRefreshLayout);
        mListView.setAdapter(mComboTempAdapter);

        addBookMarkFields();

        if (getCombinationTempModel.getBannerImg() != null) {
            new CommonMethods().loadImage(mContext, progressBar, getCombinationTempModel
                    .getBannerImg(), bannerImage);
        }
        if (Constants.getNavigationBarValues("titleBkGrdColor") != null) {
            customParent.setBackgroundColor(Color.parseColor(Constants
                    .getNavigationBarValues("titleBkGrdColor")));
        }
    }

    private void addBookMarkFields() {
        if (bookMarkList.getChildCount() != 0) {
            bookMarkList.removeAllViews();
        }
        addBookMarkView(mCategoryList);
    }

    private void addBookMarkView(final ArrayList<CategoryListModel> CategoryList) {
        try {
            int size = CategoryList.size();
            int count = 0;
            while (count != size) {
                LayoutInflater inflater =
                        (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View bookMarkView = inflater.inflate(R.layout.book_mark_view, bookMarkList, false);
                final LinearLayout catView = (LinearLayout) bookMarkView.findViewById(R.id
                        .ctegory_view);
                TextView bookMark = (TextView) bookMarkView.findViewById(R.id.book_mark);
                String category = CategoryList.get(count).getCatName();
                if (category != null && !category.equalsIgnoreCase("")) {
                    bookMark.setText(category);
                }
                final int finalCount = count;
                catView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(CategoryList.get(finalCount).getNonfeedlink() != null)
                        {
                            CustomTitleBar.isNonFeed = true;
                            Intent intent = new Intent(mContext,
                                    ScanseeBrowserActivity.class);
                            intent.putExtra("module","news");
                            intent.putExtra("title",mCategoryList.get(finalCount).getCatName());
                            intent.putExtra("textColor",mCategoryList.get(finalCount).getCatTxtColor());
                            intent.putExtra("textBgColor",mCategoryList.get(finalCount).getCatColor());
                            intent.putExtra("backButtonColor",mCategoryList.get(finalCount).getBackButtonColor());
                            intent.putExtra(CommonConstants.URL, mCategoryList.get(finalCount).getNonfeedlink());
                            mContext.startActivity(intent);
                        }else
                        {
                            gotoSubPage(CategoryList.get(finalCount).getCatName(),
                                    CategoryList
                                            .get(finalCount).getCatTxtColor());
                        }
                    }
                });
                bookMarkList.addView(bookMarkView);
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void gotoSubPage(String catType, String catTxtColor) {
        Intent intent = new Intent(mContext, SubpageActivty.class);
        intent.putExtra("templateType", "subpage");
        intent.putExtra("sideMenuCat", catType);
        intent.putExtra("isSideBar", "1");
        if (catTxtColor != null) {
            intent.putExtra("catTxtColor", catTxtColor);
        }
        intent.putExtra(Constants.LEVEL, mLevel);
        intent.putExtra(Constants.LINKID, mLinkID);
        mContext.startActivity(intent);
    }

    private void onTemplateChange(GetCombinationTempModel getCombinationTempModel) {

        if (getCombinationTempModel.getNewtempName().equalsIgnoreCase
                (Constants
                        .SCROLLING)) {
            Intent intent = new Intent(CombinationTemplate.this,
                    ScrollingPageActivity.class);
            intent.putExtra(Constants.LEVEL, mLevel);
            intent.putExtra(Constants.LINKID, mLinkID);
            startActivity(intent);
            finish();
        } else if (getCombinationTempModel.getNewtempName()
                .equalsIgnoreCase(Constants
                        .NEWS_TILE)) {
            Intent intent = new Intent(CombinationTemplate.this,
                    TwoTileNewsTemplateActivity.class);
            intent.putExtra(Constants.LEVEL, mLevel);
            intent.putExtra(Constants.LINKID, mLinkID);
            startActivity(intent);
            finish();
        } else if (getCombinationTempModel.getNewtempName()
                .equalsIgnoreCase(Constants
                        .COMBINATION)) {
            setUpDrawer();
            loadValues(getCombinationTempModel);
        } else {
            GlobalConstants.isFromNewsTemplate = false;
            SubMenuStack.clearSubMenuStack();
            MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null,
                    null, 0,
                    null, mLinkId, null, null, null, null, "1", null,
                    null, null,
                    null, null, null, null);
            SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
            MenuAsyncTask menu = new MenuAsyncTask(CombinationTemplate
                    .this);
            menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "1",
                    "0",
                    "0", "None", "0", "0", "NewsTemplate");
        }
    }


    private void setScrollListener() {
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int
                    totalItemCount) {
                final int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    // you have reached end of list, load more data
                    if (preLast != lastItem) { //to avoid multiple calls for last item
                        if (mNextPage == 1 && isDataLoaded) {
                            if (!isRefreshNeeded) {
                                Log.d(TAG, "Inside scroll");
                                mListView.removeFooterView(moreFooterView);
                                mListView.addFooterView(moreFooterView);
                                moreFooterView.setVisibility(View.VISIBLE);
                                preLast = lastItem;
                                isDataLoaded = false;
                                getTemplatePaginationValues(UrlRequestParams.getComboTemplate
                                        (mLowerLimit,
                                                mDateCreated, mLinkID, mLevel));
                            }
                        }
                    }
                }
            }

        });
    }

    private void setSwipeRefreshListener() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isRefreshNeeded) {
                    Log.d(TAG, "inside refresh");
                    getTemplateData(UrlRequestParams.getComboTemplate(0, mDateCreated, mLinkID,
                            mLevel));
                    preLast = 0;
                }
                isRefreshNeeded = true;
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        onBackClick();
    }

    private void onBackClick() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
        }
        super.onBackPressed();

    }
}
