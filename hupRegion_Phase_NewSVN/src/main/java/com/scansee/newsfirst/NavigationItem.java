package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 5/17/2016.
 */
public class NavigationItem {
    //to rearrange sortOrder
    private int flag;
    private String sortOrder;
    private int index;

    public int getIsSubCategory() {
        return isSubCategory;
    }

    public void setIsSubCategory(int isSubCategory) {
        this.isSubCategory = isSubCategory;
    }

    private int isSubCategory;

    private String catId;
    private String catName;

    private String linkId;
    private String linkTypeId;
    private String linkTypeName;
    private String mBtnColor;
    private String mBtnFontColor;
    private String mGrpBkgrdColor;
    private String mGrpFntColor;
    private String mItemId;
    private String mItemImg;
    private String mItemName;
    private String position;
    private String smBtnColor;
    private String smBtnFontColor;
    private ArrayList<MainCatList> mainCatList;

    public String getCatTxtColor() {
        return catTxtColor;
    }

    public void setCatTxtColor(String catTxtColor) {
        this.catTxtColor = catTxtColor;
    }

    private String catTxtColor;

    public String getSmGrpBkgrdColor() {
        return smGrpBkgrdColor;
    }

    public void setSmGrpBkgrdColor(String smGrpBkgrdColor) {
        this.smGrpBkgrdColor = smGrpBkgrdColor;
    }

    public String getSmGrpFntColor() {
        return smGrpFntColor;
    }

    public void setSmGrpFntColor(String smGrpFntColor) {
        this.smGrpFntColor = smGrpFntColor;
    }

    private String smGrpBkgrdColor;
    private String smGrpFntColor;

    public ArrayList<MainCatList> getMainCatList() {
        return mainCatList;
    }

    public void setMainCatList(ArrayList<MainCatList> mainCatList) {
        this.mainCatList = mainCatList;
    }


    public String getLinkTypeId() {
        return linkTypeId;
    }

    public void setLinkTypeId(String linkTypeId) {
        this.linkTypeId = linkTypeId;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public String getLinkTypeName() {
        return linkTypeName;
    }

    public void setLinkTypeName(String linkTypeName) {
        this.linkTypeName = linkTypeName;
    }

    public String getmBtnColor() {
        return mBtnColor;
    }

    public void setmBtnColor(String mBtnColor) {
        this.mBtnColor = mBtnColor;
    }

    public String getmBtnFontColor() {
        return mBtnFontColor;
    }

    public void setmBtnFontColor(String mBtnFontColor) {
        this.mBtnFontColor = mBtnFontColor;
    }

    public String getmGrpBkgrdColor() {
        return mGrpBkgrdColor;
    }

    public void setmGrpBkgrdColor(String mGrpBkgrdColor) {
        this.mGrpBkgrdColor = mGrpBkgrdColor;
    }

    public String getmGrpFntColor() {
        return mGrpFntColor;
    }

    public void setmGrpFntColor(String mGrpFntColor) {
        this.mGrpFntColor = mGrpFntColor;
    }

    public String getmItemName() {
        return mItemName;
    }

    public void setmItemName(String mItemName) {
        this.mItemName = mItemName;
    }

    public String getmItemImg() {
        return mItemImg;
    }

    public void setmItemImg(String mItemImg) {
        this.mItemImg = mItemImg;
    }

    public String getmItemId() {
        return mItemId;
    }

    public void setmItemId(String mItemId) {
        this.mItemId = mItemId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSmBtnColor() {
        return smBtnColor;
    }

    public void setSmBtnColor(String smBtnColor) {
        this.smBtnColor = smBtnColor;
    }

    public String getSmBtnFontColor() {
        return smBtnFontColor;
    }

    public void setSmBtnFontColor(String smBtnFontColor) {
        this.smBtnFontColor = smBtnFontColor;
    }


    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }


    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

}
