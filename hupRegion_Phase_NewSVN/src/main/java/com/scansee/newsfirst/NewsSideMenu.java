package com.scansee.newsfirst;

/**
 * Created by subramanya.v on 6/7/2016.
 */
public class NewsSideMenu {
    private final String hubCitiId;
    private final String userId;
    private final String linkId;
    private final String level;
    private final String sideNaviPersonalizatn;

    public NewsSideMenu(String hubCitiId, String userId, String linkID, String level,String sideNaviPersonalizatn) {
        this.hubCitiId = hubCitiId;
        this.userId = userId;
        this.linkId = linkID;
        this.level = level;
        this.sideNaviPersonalizatn = sideNaviPersonalizatn;
    }
}
