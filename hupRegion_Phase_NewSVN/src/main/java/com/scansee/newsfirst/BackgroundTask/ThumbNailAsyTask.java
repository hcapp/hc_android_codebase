package com.scansee.newsfirst.BackgroundTask;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.scansee.newsfirst.CommonMethods;

/**
 * Created by sharanamma on 13-07-2016.
 */
public class ThumbNailAsyTask extends AsyncTask<String, Void, Bitmap> {
    private final String uri;
    private final ImageView gridImage;

    public ThumbNailAsyTask(String uri, ImageView gridImage) {
        this.gridImage = gridImage;
        this.uri = uri;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        gridImage.setImageBitmap(bitmap);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        Bitmap thumb = null;
        try {
            thumb = new CommonMethods().retriveVideoFrameFromVideo(uri);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return thumb;
    }
}

