package com.scansee.newsfirst.BackgroundTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.WindowManager;

import com.artifex.mupdflib.AsyncTask;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by sharanamma on 10-06-2016.
 */
public class GuestUserLoginAsyncTask extends AsyncTask<String, String, String> {
    UrlRequestParams mUrlRequestParams = new UrlRequestParams();
    ServerConnections mServerConnections = new ServerConnections();

    private Hashtable<String, String> logInResponseData;
    private Activity activity;
    private String className;

    public GuestUserLoginAsyncTask(Activity activity, String className) {
        this.activity = activity;
        this.className = className;
    }

    @Override
    protected String doInBackground(String... params) {
        try {

            String url_log_in = Properties.url_local_server
                    + Properties.hubciti_version + "firstuse/v2/getuserlogin";
            String urlParameters = mUrlRequestParams
                    .createLogInUrlParameter(Constants.USERNAME, Constants.PASSWORD);

            JSONObject xmlResponde = mServerConnections.getUrlPostResponse(
                    url_log_in, urlParameters, true);
            return jsonParseLoginResponse(xmlResponde);

        } catch (Exception e) {
            e.printStackTrace();
            return UN_SUCCESSFULL;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equalsIgnoreCase(SUCCESSFULL)) {
            Constants.bISLoginStatus = true;
            if (logInResponseData != null) {
                hideKeyboard();
            }
        } else {
            Constants.bISLoginStatus = false;
            if (logInResponseData != null) {
                logInResponseData.get("responseCode");
            }
        }

            // call news template activity
            SubMenuStack.clearSubMenuStack();
            MainMenuBO mainMenuBO = new MainMenuBO(null, null, null, null, 0,
                    null, null, null, null, null, null, "1", null,
                    null, null,
                    null, null, null, null);
            SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
            if (className.equalsIgnoreCase(Constants.COMBINATION)) {
                activity.startActivity(new Intent(activity, CombinationTemplate.class));
            } else if (className.equalsIgnoreCase(Constants.SCROLLING)) {
                activity.startActivity(new Intent(activity, ScrollingPageActivity.class));
            } else {
                activity.startActivity(new Intent(activity, TwoTileNewsTemplateActivity.class));
            }
            activity.finish();
        }

    private static String SUCCESSFULL = "sucessfull";
    private static String UN_SUCCESSFULL = "un_sucessfull";

    public String jsonParseLoginResponse(JSONObject xmlResponde) {

        JSONObject localJSONObject;
        logInResponseData = new Hashtable<>();

        if (xmlResponde == null) {
            return UN_SUCCESSFULL;
        }
        try {

            if (xmlResponde.has(Constants.SUCCESSFULL_REGISTRATION_RESPONSE)) {

                localJSONObject = xmlResponde
                        .getJSONObject(Constants.SUCCESSFULL_REGISTRATION_RESPONSE);

                String userId = localJSONObject.getString("userId");
                logInResponseData.put("userId", userId);
                logInResponseData.put("isHCActive", localJSONObject.getString("isHCActive"));
                logInResponseData.put("isPrefSet",  localJSONObject.getString("isPrefSet"));

                logInResponseData.put("addDevToUser", localJSONObject.getString("addDevToUser"));

                SharedPreferences preferences = activity.getSharedPreferences(
                        Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
                SharedPreferences.Editor e = preferences.edit().putString(
                        Constants.PREFERENCE_KEY_U_ID, userId);
                e.apply();

                Constants.GuestLoginId = Constants.GuestLoginIdforSigup = userId;

                //Added by sharanamma
                if (localJSONObject.has("cityList")) {
                    JSONArray cityArray = new JSONArray();
                    JSONObject cityObject = localJSONObject.optJSONObject("cityList");
                    if (cityObject != null && cityObject.has("City")) {
                        JSONObject isJSONObject = cityObject.optJSONObject("City");
                        if (isJSONObject == null) {
                            cityArray = cityObject.optJSONArray("City");
                            if (cityArray != null) {
                                new CityDataHelper().storePreferedCities(activity, cityArray);
                            }
                        } else if (cityObject.optJSONObject("City") != null) {
                            cityArray.put(cityObject.optJSONObject("City"));
                            new CityDataHelper().storePreferedCities(activity, cityArray);
                        }
                    }
                }

                return SUCCESSFULL;

            } else if (xmlResponde.has(Constants.ERROR_REGISTRATION_RESPONSE)) {

                localJSONObject = xmlResponde
                        .getJSONObject(Constants.ERROR_REGISTRATION_RESPONSE);

                String responseCode = localJSONObject.getString("responseCode");
                String responseText = localJSONObject.getString("responseText");

                logInResponseData.put("responseCode", responseCode);
                logInResponseData.put("responseText", responseText);

                return UN_SUCCESSFULL;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return UN_SUCCESSFULL;
        }
        return UN_SUCCESSFULL;
    }

    public void hideKeyboard() {
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

}
