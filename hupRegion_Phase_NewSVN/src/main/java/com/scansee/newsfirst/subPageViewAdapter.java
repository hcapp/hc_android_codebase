package com.scansee.newsfirst;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.scansee.newsfirst.BackgroundTask.ThumbNailAsyTask;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 5/19/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class subPageViewAdapter extends BaseAdapter {
    private int tabPosition;
    private boolean isSubpage;
    private final GridView grid;
    private final boolean isItemClick;
    private ArrayList<String> gridChildData;
    private final int mScreenHeight;
    private final int mScreenWidth;
    private final String category;
    private final Context mContext;


    public subPageViewAdapter(Context gridFragment, int mScreenHeight, int mScreenWidth, String category, GridView grid, boolean isItemClick, boolean isSubpage, int tabPosition) {
        this.mScreenHeight = mScreenHeight;
        this.mScreenWidth = mScreenWidth;
        this.category = category;
        this.mContext = gridFragment;
        this.grid = grid;
        this.isItemClick = isItemClick;
        this.isSubpage = isSubpage;
        this.tabPosition = tabPosition;

    }

    public subPageViewAdapter(Context gridFragment, ArrayList<String> link, int mScreenHeight, int mScreenWidth, String category, GridView grid, boolean isItemClick) {
        this.gridChildData = link;
        this.mScreenHeight = mScreenHeight;
        this.mScreenWidth = mScreenWidth;
        this.category = category;
        this.mContext = gridFragment;
        this.grid = grid;
        this.isItemClick = isItemClick;

    }


    @Override
    public int getCount() {
        int size;
        try {
            if (isItemClick) {
                size = gridChildData.size();
            } else {
                if (isSubpage) {
                    size = ((SubpageActivty)mContext).subPageData.get(0).getItems().size();
                } else {
                    size = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().size();
                }
            }

            return size;

        } catch (Exception e) {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return ((ScrollingPageActivity)mContext).scrollingData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi;

        int lowerLimit = 0;
        int maxCount = 0;
        if (!isItemClick) {
            if (isSubpage) {
                lowerLimit =((SubpageActivty)mContext).subPageData.get(0).getLowerLimitFlag();
                maxCount = ((SubpageActivty)mContext).subPageData.get(0).getMaxCnt();
                if (lowerLimit < maxCount) {
                    if (position + 2 == lowerLimit) {
                        if (!((SubpageActivty) mContext).isGridLoaded) {
                            if (isSubpage) {
                                ((SubpageActivty) mContext).getGridPagination(lowerLimit, category, this);
                            } else {
                                ((SubpageActivty) mContext).getPagination(lowerLimit, tabPosition, this,category);
                            }
                        }
                    }
                }
            } else {
                lowerLimit = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getLowerLimitFlag();
                maxCount = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getMaxCnt();
                if (lowerLimit < maxCount) {
                    if (position + 2 == lowerLimit) {
                        if (!((ScrollingPageActivity) mContext).isGridLoaded) {
                                ((ScrollingPageActivity) mContext).getPagination(lowerLimit, tabPosition, this,category);
                        }
                    }
                }
            }
        }


        LayoutInflater inflaInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        vi = inflaInflater.inflate(
                R.layout.grid_view, parent, false);
        RelativeLayout gridParent = (RelativeLayout) vi.findViewById(R.id.grid_adapter_parent);
        TextView countView = (TextView) vi.findViewById(R.id.count);
        TextView viewSummary = (TextView) vi.findViewById(R.id.view_summary);
        FrameLayout alphaView = (FrameLayout) vi.findViewById(R.id.alpha_view);
        RelativeLayout countParent = (RelativeLayout) vi.findViewById(R.id.count_parent);
        ImageView playButton = (ImageView) vi.findViewById(R.id.play_button);
        ImageView gridImage = (ImageView) vi.findViewById(R.id.grid_image);
        ProgressBar progressBar = (ProgressBar) vi.findViewById(R.id.progress);
        if (isItemClick) {
            alphaView.setVisibility(View.GONE);
            countParent.setVisibility(View.GONE);
            viewSummary.setVisibility(View.GONE);
        }

        int width = mScreenWidth - grid.getPaddingLeft() - grid.getPaddingRight() - grid.getHorizontalSpacing();

        int height = mScreenHeight - grid.getPaddingBottom() - grid.getPaddingTop() - (grid.getVerticalSpacing() * 3);
        int viewHeight = height / 3;

        LinearLayout.LayoutParams rowView = new LinearLayout.LayoutParams((width / 2), viewHeight);
        gridParent.setLayoutParams(rowView);

        String title = null, image = null;
        int count = 0;

        if (isItemClick) {
            image = gridChildData.get(position);
            if (category.equalsIgnoreCase("videos")) {
                playButton.setVisibility(View.VISIBLE);
                try {
                    image = image.replaceAll(" ", "%20");
                    ThumbNailAsyTask thumNail = new ThumbNailAsyTask(image, gridImage);
                    thumNail.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        } else {
            if (isSubpage) {
                title = ((SubpageActivty)mContext).subPageData.get(0).getItems().get(position).getTitle();
            } else {
                title = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().get(position).getTitle();
            }
            String link;
            if (category.equalsIgnoreCase("photos")) {
                if (isSubpage) {
                    link = ((SubpageActivty)mContext).subPageData.get(0).getItems().get(position).getImage();
                } else {
                    link = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().get(position).getImage();
                }
            } else {
                playButton.setVisibility(View.VISIBLE);
                //creating thumbnail image
                if (isSubpage) {
                    link = ((SubpageActivty)mContext).subPageData.get(0).getItems().get(position).getVideoLink();
                } else {
                    link = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition).getItems().get(position).getVideoLink();
                }

                if (link != null) {
                    try {
                        String[] videoLink = link.split(",");
                        String videoUrl = videoLink[0].replaceAll(" ", "%20");
                        ThumbNailAsyTask thumNail = new ThumbNailAsyTask(videoUrl, gridImage);
                        thumNail.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            }
            if (link != null) {
                String[] images = link.split(",");
                image = images[0];
                for (String imageLink : images) {
                    count++;
                }
            }
        }
        countView.setText(String.valueOf(count));
        if (title != null && !title.equalsIgnoreCase("")) {
            viewSummary.setText(title);
        }
        if (category.equalsIgnoreCase("photos")) {
            if (image != null && !image.equalsIgnoreCase("")) {
                image = image.replaceAll(" ", "%20");
                new CommonMethods().loadImage(mContext, progressBar, image, gridImage);
            }
        }

        return vi;
    }
}
