package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by supriya.m on 6/3/2016.
 */
public class SideMenuObject
{
	public String getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(String responseCode)
	{
		this.responseCode = responseCode;
	}

	public String getResponseText()
	{
		return responseText;
	}

	public void setResponseText(String responseText)
	{
		this.responseText = responseText;
	}

	public ArrayList<ListCatDetails> getListCatDetails()
	{
		return listCatDetails;
	}

	public void setListCatDetails(ArrayList<ListCatDetails> listCatDetails)
	{
		this.listCatDetails = listCatDetails;
	}

	private String responseCode;
	private String responseText;
	private ArrayList<ListCatDetails> listCatDetails;
}
