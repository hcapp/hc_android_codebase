package com.scansee.newsfirst;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.ScrollingPageModel;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.MenuPropertiesActivity;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.model.SubPageModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.TreeMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SubpageActivty extends MenuPropertiesActivity {
    private int lowerLimitFlag = 0, isSubCategory;
    private String mLinkID;
    private String mLevel;
    private String template;
    private String category;
    private String catTxtColor;
    private String isSideBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SwipeRefreshLayout gridRefresh;
    private View moreResultsView;
    private ListView sideMenuList;
    private ViewPager subViewPager;
    private RelativeLayout tabParent;
    private GridView grid;
    private LinearLayout screenSummary;
    public ImageView bannerImage, drawerIcon;
    public ImageView backImage;
    public RelativeLayout customParent;
    public TextView newsTitle;
    private ImageView weatherIcon;
    private TabLayout subPageTabLayout;
    public TreeMap<Integer, ScrollingObj> subPageData = new TreeMap<>();
    private ImageView HomeView;
    private Activity mContext;
    private RestClient mRestClient;
    private LinkedList<SubCatObj> subCat = new LinkedList<>();
    private int tabPosition;
    private subPageViewAdapter subPageAdapter;
    private int mScreenHeight;
    private int mScreenWidth;
    private String mDateCreated;
    public boolean isLoaded = false;
    public ProgressDialog gridProgDialog = null;
    public boolean isGridLoaded = false;
    View subPageHeader;
    private final boolean isItemClick = false;
    private subPageViewAdapter subPagetAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subpage_activity);
        try {
            mContext = SubpageActivty.this;
            CommonConstants.hamburgerIsFirst = true;
            subPageData = new TreeMap<>();
            getIntentData();
            // Added by Supriya
            // Setting level to in parent activity to navigate and call api for proper level(Which is
            // required to differenciate
            // between MainMenu and SubMenu)
            previousMenuLevel = mLevel;
            mLinkId = mLinkID;
            bindView();
            setClickListener();
            setVisibility();
            removeView();
            attachTab();
            ViewTreeObserver observer = screenSummary.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    mScreenHeight = screenSummary.getMeasuredHeight();
                    mScreenWidth = screenSummary.getMeasuredWidth();
                    addView();

                    ViewTreeObserver obs = screenSummary.getViewTreeObserver();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setUpDrawer() {

        CommonConstants.previousMenuLevel = mLevel;
        CommonConstants.commonLinkId = mLinkID;

        callSideMenuApi((CustomNavigation) findViewById(R.id.custom_navigation), false);
    }

    private void addView() {
        if (isSideBar.equalsIgnoreCase("1") && isSubCategory == 1) {
            getSubCategogories();
        } else {
            if ((category.equalsIgnoreCase("photos") || category.equalsIgnoreCase("Videos")
            )) {
                setPhotoViewVisibility();
                swipeRefreshLayout.setVisibility(View.GONE);
                gridRefresh.setVisibility(View.VISIBLE);
                getGridDetails(category);
            } else {
                setPhotoViewVisibility();
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                gridRefresh.setVisibility(View.GONE);
                SubPage sideMenuObj = new SubPage();
                sideMenuObj.sideMenu(mContext
                        , category, sideMenuList,
                        swipeRefreshLayout, moreResultsView,
                        isSideBar, template, mLinkID, mLevel);
            }
        }
    }

    private void setPhotoViewVisibility() {
        tabParent.setVisibility(View.GONE);
        subViewPager.setVisibility(View.GONE);
    }


    private void getSubCategogories() {
        final ProgressDialog progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();
        SubPageModel subCatRequest = UrlRequestParams.getnewsubcategories(category);
        mRestClient.getnewsubcategories(subCatRequest, new Callback<SubCatObj>() {

                    @Override
                    public void success(SubCatObj subCatObj, Response response) {
                        try {
                            String responseText = subCatObj.getResponseText();
                            subCat = new LinkedList<>();

                            subCat.add(subCatObj);
                            if (subCatObj
                                    .getListCatDetails() != null) {

                                for (ListSubCatSetails subCatList : subCatObj
                                        .getListCatDetails()) {

                                    if (subCatList.getListCatDetails() != null) {
                                        tabParent.setVisibility(View.VISIBLE);
                                        gridRefresh.setVisibility(View.GONE);
                                        subViewPager.setVisibility(View.VISIBLE);
                                        swipeRefreshLayout.setVisibility(View.GONE);
                                        int i = 0;
                                        for (SubCat listDetails : subCatList
                                                .getListCatDetails
                                                        ()) {
                                            View textView = LayoutInflater.from(mContext)
                                                    .inflate(R.layout.custom_tab, null);
                                            TextView tabText = (TextView) textView
                                                    .findViewById(R.id.custom_text);
                                            String subCatName = listDetails
                                                    .getSubCatName();
                                            tabText.setText(subCatName);
                                            if (catTxtColor != null && !catTxtColor.isEmpty()) {
                                                tabText.setTextColor(Color.parseColor(catTxtColor));
                                            }
                                            subPageTabLayout.addTab(subPageTabLayout.newTab());
                                            TabLayout.Tab tab = subPageTabLayout.getTabAt(i);
                                            assert tab != null;
                                            tab.setCustomView(textView);
                                            i++;
                                        }
                                    } else {
                                        displaySubpage();
                                    }
                                }
                            } else {
                                //if no record found
                                displaySubpage();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dismissProgressBar(progDialog);
                    }


                    @Override
                    public void failure(RetrofitError retrofitError) {
                        if (retrofitError.getResponse() != null) {
                            CommonConstants.displayToast(mContext, retrofitError
                                    .getResponse()
                                    .getReason());
                        }
                        dismissProgressBar(progDialog);
                    }


                }

        );
    }

    private void displaySubpage() {
        if ((category.equalsIgnoreCase("photos") ||
                category.equalsIgnoreCase("Videos"))) {
            setPhotoViewVisibility();
            swipeRefreshLayout.setVisibility(View.GONE);
            gridRefresh.setVisibility(View.VISIBLE);
            grid.setBackgroundColor(Color.BLACK);
            getGridDetails(category);
        } else {
            setPhotoViewVisibility();
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            gridRefresh.setVisibility(View.GONE);
            SubPage sideMenuObj = new SubPage();
            sideMenuObj.sideMenu(mContext
                    , category, sideMenuList,
                    swipeRefreshLayout, moreResultsView,
                    isSideBar, template, mLinkID, mLevel);
        }
    }

    private void attachTab() {
        subViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener
                (subPageTabLayout));
        subPageTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                if (subPageData.size() != 0) {
                    try {
                        if (subPageData.get(tabPosition) != null) {

                            TabFragmentView frag = (TabFragmentView) subViewPager
                                    .getAdapter()
                                    .instantiateItem(subViewPager, tabPosition);

                            View moreResultsView = frag.moreResultsView;
                            if (moreResultsView != null) {
                                if (subPageData.get(tabPosition)
                                        .getNextPage() == 1) {
                                    frag.viewPagerFragment.removeFooterView(moreResultsView);
                                    frag.viewPagerFragment.addFooterView(moreResultsView);
                                    moreResultsView.setVisibility(View.VISIBLE);
                                } else {
                                    frag.viewPagerFragment.removeFooterView(moreResultsView);
                                    moreResultsView.setVisibility(View.GONE);
                                }
                            }
                            subViewPager.setCurrentItem(tab.getPosition());

                        } else {
                            lowerLimitFlag = 0;
                            getscroltemplate(tabPosition, "3");
                        }


                    } catch (IndexOutOfBoundsException e) {
                        getscroltemplate(tabPosition, "3");
                    }
                } else {
                    getscroltemplate(tabPosition, "3");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getscroltemplate(final int selectedTab, String isSideBar) {
        final ProgressDialog progressBar = new ProgressDialog(this);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setMessage("Please Wait..");
        progressBar.setCancelable(false);
        progressBar.show();
        String catName = null;
        try {
            catName = subCat.get(0).getListCatDetails().get(0).getListCatDetails().get
                    (selectedTab).getSubCatName();
            isSideBar = "3";
        } catch (Exception e) {
            dismissProgressBar(progressBar);
        }

        ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(catName,
                lowerLimitFlag, isSideBar, mDateCreated, mLevel, mLinkID);

        final String finalCatName = catName;
        mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {
            @Override
            public void success(ScrollingObj scrollingObj, Response response) {
                try {
                    loadValues(scrollingObj, selectedTab, finalCatName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    dismissProgressBar(progressBar);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    CommonConstants.displayToast(mContext, error.getResponse().getReason());
                }
                dismissProgressBar(progressBar);
            }
        });

    }

    private void loadValues(ScrollingObj scrollingObj, final int selectedTab, String catName) {
        //Getting Navigation bar values and storing it to shared preference to access
        // from all the screens
        if (scrollingObj.getBkImgPath() != null) {
            Constants.setNavigationBarValues("bkImgPath", scrollingObj.getBkImgPath());
        }
        if (scrollingObj.getHomeImgPath() != null) {
            Constants.setNavigationBarValues("homeImgPath", scrollingObj.getHomeImgPath());
        }
        if (scrollingObj.getBackButtonColor() != null) {
            String color = scrollingObj.getBackButtonColor();
           // Constants.setNavigationBarValues("backButtonColor", scrollingObj.getBackButtonColor());
            //The color u SRC_ATOP
            backImage.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
          //  backImage.setBackgroundResource(R.drawable.ic_action_back);
        }
        if (scrollingObj.getHumbergurImgPath() != null) {
            Constants.setNavigationBarValues("hamburgerImg", scrollingObj.getHumbergurImgPath());
        }
        if (scrollingObj.getTitleTxtColor() != null) {
            Constants.setNavigationBarValues("titleTxtColor", scrollingObj.getTitleTxtColor());
        }
        if (scrollingObj.getTitleBkGrdColor() != null) {
            Constants.setNavigationBarValues("titleBkGrdColor", scrollingObj.getTitleBkGrdColor());
        }

        String responseText = scrollingObj.getResponseText();
        mDateCreated = scrollingObj.getModifiedDate();
        if (!responseText.equalsIgnoreCase("Success")) {
            CommonConstants.displayToast(mContext, responseText);
        }
        TabAdapter tabViewadapter;
        try {
            ArrayList<ListCatDetails> bookMarkList = null;
            ArrayList<SubCat> subCatList;

            subCatList = subCat.get(0).getListCatDetails().get(0).getListCatDetails();
            subPageData.put(selectedTab, scrollingObj);
            tabViewadapter = new TabAdapter
                    (getSupportFragmentManager(), subPageTabLayout.getTabCount(),
                            mScreenHeight, mScreenWidth, bookMarkList, subCatList, template);
            subViewPager.setAdapter(tabViewadapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        subViewPager.setCurrentItem(selectedTab);
        TabFragmentView frag;

        frag = (TabFragmentView) subViewPager.getAdapter()
                .instantiateItem(subViewPager, selectedTab);

        try {
            assert frag != null;
            if (frag.viewPagerFragment != null) {
                View moreResultsView = frag.moreResultsView;
                int nextPage;

                nextPage = subPageData.get(selectedTab).getNextPage();

                if (moreResultsView != null) {
                    if (nextPage ==
                            1) {
                        if (frag.viewPagerFragment != null) {
                            frag.viewPagerFragment.addFooterView(moreResultsView);
                            moreResultsView.setVisibility(View.VISIBLE);
                        }
                    } else {
                        frag.viewPagerFragment.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);
                    }
                }
            } else {
                if (!(catName.equalsIgnoreCase("photos") || catName.equalsIgnoreCase("Videos"))) {
                    final TabFragmentView finalFrag = frag;
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            int nextPage;

                            subViewPager.setCurrentItem(selectedTab);
                            nextPage = subPageData.get(selectedTab)
                                    .getNextPage();

                            View moreResultsView = finalFrag.moreResultsView;
                            if (moreResultsView != null) {
                                if (nextPage
                                        == 1) {
                                    finalFrag.viewPagerFragment.addFooterView(moreResultsView);
                                    moreResultsView.setVisibility(View.VISIBLE);
                                } else {
                                    finalFrag.viewPagerFragment.removeFooterView(moreResultsView);
                                    moreResultsView.setVisibility(View.GONE);
                                }
                            }
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (subPageData.get(selectedTab).getItems() != null) {
            if (subPageData.get(selectedTab).getItems().size() != 0) {
                String catTxtColor = subPageData.get(selectedTab)
                        .getItems().get(0).getCatTxtColor();
                String catColour = subPageData.get(selectedTab).getItems
                        ().get(0).getCatColor();
                if (catTxtColor != null) {
                    newsTitle.setTextColor(Color.parseColor(catTxtColor));

                }
                if (catColour != null) {
                    customParent.setBackgroundColor(Color.parseColor(catColour));
                    changeTabColour(catColour);
                }

            }
        }
        displayHamburger(scrollingObj);

    }

    private void displayHamburger(ScrollingObj scrollingObj) {
        if (scrollingObj.getResponseCode().equals("10000")) {
            //Check for template change


            if (Constants.getNavigationBarValues("hamburgerImg") != null) {
                Picasso.with(mContext).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                        "%20")).placeholder(R.drawable.loading_button).into
                        (drawerIcon);
            }
            if (scrollingObj.getBackButtonColor() != null) {
                String color = scrollingObj.getBackButtonColor();//The color u want
                backImage.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
               // backImage.setBackgroundResource(R.drawable.ic_action_back);
            }

        } else if (scrollingObj.getResponseCode().equals("10005")) {
            if (scrollingObj.getHumbergurImgPath() != null) {
                Constants.setNavigationBarValues("hamburgerImg", scrollingObj.getHumbergurImgPath());
            }
            if (Constants.getNavigationBarValues("hamburgerImg") != null) {
                Picasso.with(mContext).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                        "%20")).placeholder(R.drawable.loading_button).into
                        (drawerIcon);
            }
            if (scrollingObj.getBackButtonColor() != null) {
                String color = scrollingObj.getBackButtonColor();//The color u want
                backImage.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
               // backImage.setBackgroundResource(R.drawable.ic_action_back);
            }

        }
    }

    private void changeTabColour(String catColour) {
        try {
            subPageTabLayout.setBackgroundColor(Color.parseColor(catColour));
            if (catTxtColor != null && !catTxtColor.isEmpty()) {
                subPageTabLayout.setSelectedTabIndicatorColor(Color.parseColor(catTxtColor));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeView() {
        subViewPager.removeAllViews();
        if (subPageTabLayout.getTabCount() != 0) {
            subPageTabLayout.removeAllTabs();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRefresh = false;
        if (CommonConstants.hamburgerIsFirst) {
            setUpDrawer();
            CommonConstants.hamburgerIsFirst = false;
        }
    }

    private void setVisibility() {
        bannerImage.setVisibility(View.GONE);
        newsTitle.setVisibility(View.VISIBLE);
        newsTitle.setText(category);
        if (catTxtColor != null && !catTxtColor.isEmpty()) {
            newsTitle.setTextColor(Color.parseColor(catTxtColor));
        }
        HomeView.setVisibility(View.VISIBLE);
        if (Constants.getNavigationBarValues("homeImgPath") != null) {
            Picasso.with(this).load(Constants.getNavigationBarValues("homeImgPath").replace
                    (" ",

                            "%20")).placeholder(R.drawable.loading_button).into
                    (HomeView);
        } else {
            HomeView.setBackgroundResource(R.drawable.mainmenu_white);
        }

        subPageTabLayout.setVisibility(View.VISIBLE);
        subPageTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        weatherIcon.setVisibility(View.GONE);
        backImage.setVisibility(View.VISIBLE);

    }

    private void bindView() {
        mRestClient = RestClient.getInstance();
        drawerIcon = (ImageView) findViewById(R.id.drawer_icon);
        backImage = (ImageView) findViewById(R.id.back_btn);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        gridRefresh = (SwipeRefreshLayout) findViewById(R.id.grid_refresh);
        moreResultsView = getLayoutInflater().inflate(
                R.layout.pagination, sideMenuList, true);
        subPageHeader = getLayoutInflater().inflate(
                R.layout.tab_subpage, sideMenuList, true);
        sideMenuList = (ListView) findViewById(R.id.side_menu_list);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        subViewPager = (ViewPager) findViewById(R.id.sub_pager);
        subPageTabLayout = (TabLayout) findViewById(R.id.subpage_tab_layout);
        tabParent = (RelativeLayout) findViewById(R.id.tab_parent);
        grid = (GridView) findViewById(R.id.grid_view);
        screenSummary = (LinearLayout) findViewById(R.id.screen_summary);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        bannerImage = (ImageView) findViewById(R.id.banner_image);
        customParent = (RelativeLayout) findViewById(R.id.custom_parent);
        HomeView = (ImageView) findViewById(R.id.HomeView);

        weatherIcon = (ImageView) findViewById(R.id.weather_icon);

        /*if (Constants.getNavigationBarValues("bkImgPath") != null) {
            Picasso.with(this).load(Constants.getNavigationBarValues("bkImgPath").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (backImage);
        } else {*/
            //backImage.setBackgroundResource(R.drawable.ic_action_back);
       // }
        backImage.setVisibility(View.VISIBLE);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });
        newsTitle = (TextView) findViewById(R.id.titleTextView);
        ImageView customMoreView = (ImageView) findViewById(R.id.more_view);
        customMoreView.setVisibility(View.GONE);
        TextView CustomMore = (TextView) findViewById(R.id.more);
        CustomMore.setVisibility(View.GONE);
        ImageView customSearch = (ImageView) findViewById(R.id.searchView);
        customSearch.setVisibility(View.GONE);
        weatherIcon.setVisibility(View.GONE);
    }

    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.drawer_icon:
                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.HomeView:
                onHomeButtonClick();
                break;
        }
    }

    private void onHomeButtonClick() {
        Intent intent = null;
        if (GlobalConstants.isFromNewsTemplate) {

            switch (GlobalConstants.className) {
                case Constants.COMBINATION:
                    intent = new Intent(mContext, CombinationTemplate.class);
                    break;
                case Constants.SCROLLING:
                    intent = new Intent(mContext, ScrollingPageActivity.class);
                    break;
                case Constants.NEWS_TILE:
                    intent = new Intent(mContext, TwoTileNewsTemplateActivity.class);
                    break;
            }
            if (intent != null) {
                intent.putExtra(Constants.LEVEL, mLevel);
                intent.putExtra(Constants.LINKID, mLinkID);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        } else {
            if (SortDepttNTypeScreen.subMenuDetailsList != null) {
                SortDepttNTypeScreen.subMenuDetailsList.clear();
                SubMenuStack.clearSubMenuStack();
                SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
            }
            startMenuActivity();
        }

    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Constants.LINKID)) {
            mLinkID = intent.getStringExtra(Constants.LINKID);
        }
        if (intent != null && intent.hasExtra(Constants.ITEMID)) {
            mItemId = intent.getStringExtra(Constants.ITEMID);
        }
        if (intent != null && intent.hasExtra(Constants.LEVEL)) {
            mLevel = intent.getStringExtra(Constants.LEVEL);
        }
        if (intent != null && intent.hasExtra("templateType")) {
            template = intent.getExtras().getString("templateType");
        }
        if (intent != null && intent.hasExtra("sideMenuCat")) {
            category = intent.getExtras().getString("sideMenuCat");
        }
        if (intent != null && intent.hasExtra("catTxtColor")) {
            catTxtColor = intent.getExtras().getString("catTxtColor");
        }
        if (intent != null && intent.hasExtra("isSideBar")) {
            isSideBar = intent.getExtras().getString("isSideBar");
        }
        if (intent != null && intent.hasExtra("isSubCategory")) {
            isSubCategory = intent.getExtras().getInt("isSubCategory");
        }
    }

    private void getGridDetails(final String sideMenuCat) {
        final ProgressDialog progresDialog = new ProgressDialog(this);
        progresDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progresDialog.setMessage("Please Wait..");
        progresDialog.setCancelable(false);
        progresDialog.show();


        ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(sideMenuCat,
                lowerLimitFlag, "1", mDateCreated, mLevel, mLinkID);
        mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {

            @Override
            public void success(ScrollingObj scrollingObj, Response response) {
                try {
                    String responseText = scrollingObj.getResponseText();
                    if (!responseText.equalsIgnoreCase("Success")) {
                        CommonConstants.displayToast(mContext, responseText);
                    }
                    subPageData.put(0, scrollingObj);


                    int tabPosition = 0;
                    subPageAdapter = new subPageViewAdapter
                            (mContext, mScreenHeight,
                                    mScreenWidth, sideMenuCat, grid, false, true,
                                    tabPosition);
                    grid.setAdapter(subPageAdapter);
                    subPageAdapter.notifyDataSetChanged();

                    if (subPageData.get(0).getItems() != null) {
                        if (subPageData.get(0).getItems().size() != 0) {
                            String catTxtColor = subPageData.get(0).getItems().get(0)
                                    .getCatTxtColor();
                            String catColour = subPageData.get(0).getItems().get(0).getCatColor();
                            if (catTxtColor != null) {
                                newsTitle.setTextColor(Color.parseColor(catTxtColor));

                            }
                            if (catColour != null) {
                                customParent.setBackgroundColor(Color.parseColor(catColour));

                            }
                        }
                    }
                    if (scrollingObj.getHumbergurImgPath() != null) {
                        Constants.setNavigationBarValues("hamburgerImg", scrollingObj.getHumbergurImgPath());
                    }
                    if (scrollingObj.getBackButtonColor() != null) {

                        String color = scrollingObj.getBackButtonColor(); //The color u want
                        backImage.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
                       // backImage.setBackgroundResource(R.drawable.ic_action_back);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                displayHamburger(scrollingObj);

                dismissProgressBar(progresDialog);

            }


            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    CommonConstants.displayToast(mContext, error.getResponse().getReason());
                }
                dismissProgressBar(progresDialog);

            }
        });

    }

    private void dismissProgressBar(ProgressDialog progresDialog) {
        if (progresDialog != null && progresDialog.isShowing()) {
            progresDialog.dismiss();
        }
    }

    public void getPagination(int lowerLimit, int selectedTab, BaseAdapter adapter, String
            category) {
        if (category != null) {
            isGridLoaded = true;
        } else {
            isLoaded = true;
        }
        this.lowerLimitFlag = lowerLimit;
        startPagination(selectedTab, adapter);
    }

    private void startPagination(final int selectedTab, final BaseAdapter adapter) {

        try {
            final String catName;
            catName = subCat.get(0).getListCatDetails().get(0).getListCatDetails().get
                    (selectedTab).getSubCatName();
            isSideBar = "3";

            ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(catName,
                    lowerLimitFlag, isSideBar, mDateCreated, mLevel, mLinkID);
            mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {

                @Override
                public void success(ScrollingObj scrollingObj, Response response) {
                    String responseText = scrollingObj.getResponseText();
                    if (!responseText.equalsIgnoreCase("Success")) {
                        CommonConstants.displayToast(mContext, responseText);
                    }
                    try {
                        loadMoreData(selectedTab, catName, scrollingObj, adapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getResponse() != null) {
                        CommonConstants.displayToast(mContext, error.getResponse().getReason());
                    }
                }
            });
        } catch (Exception e) {
            isLoaded = false;
            e.printStackTrace();
        }

    }

    private void loadMoreData(int selectedTab, String catName, ScrollingObj
            scrollingObj, BaseAdapter adapter) {
        TabFragmentView frag;
        View moreResultsView;
        subPageData.get(selectedTab).setItems(scrollingObj.getItems());
        subPageData.get(selectedTab).setMaxCnt(scrollingObj.getMaxCnt());
        subPageData.get(selectedTab).setLowerLimitFlag(scrollingObj
                .getLowerLimitFlag());
        subPageData.get(selectedTab).setNextPage(scrollingObj.getNextPage());
        frag = (TabFragmentView) subViewPager.getAdapter()
                .instantiateItem(subViewPager, selectedTab);
        moreResultsView = frag.moreResultsView;
        adapter.notifyDataSetChanged();

        if (moreResultsView != null) {
            int nextPage;

            nextPage = subPageData.get(selectedTab)
                    .getNextPage();

            if (nextPage ==
                    1) {
                frag.viewPagerFragment.removeFooterView(moreResultsView);
                frag.viewPagerFragment.addFooterView(moreResultsView);
                moreResultsView.setVisibility(View.VISIBLE);
            } else {
                frag.viewPagerFragment.removeFooterView(moreResultsView);
                moreResultsView.setVisibility(View.GONE);
            }
        }
        if ((catName.equalsIgnoreCase("photos") || catName.equalsIgnoreCase("Videos"))) {
            if (gridProgDialog != null && gridProgDialog.isShowing()) {
                gridProgDialog.dismiss();
                gridProgDialog = null;
            }
            isGridLoaded = false;
        }
        isLoaded = false;
    }

    public void startPullToRequest(final int selectedTab, final BaseAdapter adapter) {
        try {
            final String catName;

            catName = subCat.get(0).getListCatDetails().get(0).getListCatDetails().get
                    (selectedTab).getSubCatName();
            isSideBar = "3";


            ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(catName, 0,
                    isSideBar, mDateCreated, mLevel, mLinkID);
            mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {


                @Override
                public void success(ScrollingObj scrollingObj, Response response) {
                    try {
                        if (scrollingObj.getResponseCode().equals("10000")) {
                            try {
                                loadPullToRequestData(selectedTab, scrollingObj,
                                        adapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            CommonConstants.displayToast(mContext, scrollingObj
                                    .getResponseText());
                            TabFragmentView frag;
                            frag = (TabFragmentView) subViewPager.getAdapter()
                                    .instantiateItem(subViewPager, selectedTab);

                            assert frag != null;
                            if (frag.swipeRefreshLayout != null) {
                                frag.swipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getResponse() != null) {
                        CommonConstants.displayToast(mContext, error.getResponse().getReason());
                    }
                    try {
                        TabFragmentView frag;
                        frag = (TabFragmentView) subViewPager.getAdapter()
                                .instantiateItem(subViewPager, selectedTab);

                        if (frag.swipeRefreshLayout != null) {
                            frag.swipeRefreshLayout.setRefreshing(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadPullToRequestData(int selectedTab, ScrollingObj
            scrollingObj, BaseAdapter adapter) {

        subPageData.put(selectedTab, scrollingObj);

        TabFragmentView frag;
        View moreResultsView;

        frag = (TabFragmentView) subViewPager.getAdapter()
                .instantiateItem(subViewPager, selectedTab);
        moreResultsView = frag.moreResultsView;

        if (frag != null) {
            if (frag.swipeRefreshLayout != null) {
                frag.swipeRefreshLayout.setRefreshing(false);
            }
        }
        if (moreResultsView != null) {
            int nextPage;

            nextPage = subPageData.get(selectedTab)
                    .getNextPage();
            if (nextPage ==
                    1) {
                frag.viewPagerFragment.removeFooterView(moreResultsView);
                frag.viewPagerFragment.addFooterView(moreResultsView);
                moreResultsView.setVisibility(View.VISIBLE);
            } else {
                frag.viewPagerFragment.removeFooterView(moreResultsView);
                moreResultsView.setVisibility(View.GONE);
            }

        }

        if (subPageData.get(selectedTab).getItems() != null) {
            if (subPageData.get(selectedTab).getItems().size() != 0) {
                String catTxtColor = subPageData.get(selectedTab)
                        .getItems().get(0).getCatTxtColor();
                String catColour = subPageData.get(selectedTab).getItems
                        ().get(0).getCatColor();
                if (catTxtColor != null) {
                    newsTitle.setTextColor(Color.parseColor(catTxtColor));

                }
                if (catColour != null) {
                    customParent.setBackgroundColor(Color.parseColor(catColour));
                    changeTabColour(catColour);
                }
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getGridPagination(int lowerLimit, String category, subPageViewAdapter
            subPageViewAdapter) {
        startGridPagination(lowerLimit, category, subPageViewAdapter);
    }

    private void startGridPagination(int lowerLimit, String category, final subPageViewAdapter
            subPageViewAdapter) {
        ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(category,
                lowerLimit, "1", null, mLevel, mLinkID);
        mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {

            @Override
            public void success(ScrollingObj scrollingObj, Response response) {
                String responseText = scrollingObj.getResponseText();
                if (!responseText.equalsIgnoreCase("Success")) {
                    CommonConstants.displayToast(mContext, responseText);
                }
                try {
                    ScrollingObj item = subPageData.get(0);
                    item.setItems(scrollingObj.getItems());
                    item.setMaxCnt(scrollingObj.getMaxCnt());
                    item.setLowerLimitFlag(scrollingObj
                            .getLowerLimitFlag());
                    item.setNextPage(scrollingObj.getNextPage());
                    item.setSubPageName(scrollingObj.getSubPageName());
                    subPageViewAdapter.notifyDataSetChanged();
                    if (gridProgDialog != null && gridProgDialog.isShowing()) {
                        gridProgDialog.dismiss();
                        gridProgDialog = null;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    CommonConstants.displayToast(mContext, error.getResponse().getReason());
                }
            }
        });
    }


    private void setClickListener() {

        gridRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startGridPullToRequest();
            }
        });

        grid.setOnScrollListener(new GridView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int
                    visibleItemCount, int totalItemCount) {
                if ((firstVisibleItem + visibleItemCount) == totalItemCount) {

                    try {
                        if (subPageData.get(0).getNextPage() == 1) {
                            if (totalItemCount != 0) {
                                if (gridProgDialog == null) {
                                    gridProgDialog = new ProgressDialog(mContext);
                                    gridProgDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                    gridProgDialog.setMessage("Please Wait..");
                                    gridProgDialog.setCancelable(false);
                                    gridProgDialog.show();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String link;
                if (category.equalsIgnoreCase("photos")) {
                    link = subPageData.get(0).getItems().get(position).getImage();
                } else {
                    link = subPageData.get(0).getItems().get(position).getVideoLink();
                }
                ArrayList<String> links = null;
                if (link != null) {
                    String[] words = link.split(",");
                    links = new ArrayList<>();
                    Collections.addAll(links, words);
                }
                String lDesc = subPageData.get(0).getItems().get(position).getlDesc();
                String catColor = subPageData.get(0).getItems().get(position).getCatColor();
                String catTxtColor = subPageData.get(0).getItems().get(position).getCatTxtColor();
                int itemId = subPageData.get(0).getItems().get(position).getItemID();


                Intent intent;

                if (links != null) {
                    if (links.size() > 1) {
                        intent = new Intent(SubpageActivty.this, GridChildView.class);
                        intent.putStringArrayListExtra("link", links);
                        intent.putExtra("category", category);
                        intent.putExtra("itemId", itemId);
                        intent.putExtra("catColor", catColor);
                        intent.putExtra("catTxtColor", catTxtColor);
                        intent.putExtra("desc", lDesc);
                    } else {
                        intent = new Intent(SubpageActivty.this, ViewDetailsScreen.class);
                        intent.putStringArrayListExtra("link", links);
                        intent.putExtra("category", category);
                        intent.putExtra("itemId", itemId);
                        intent.putExtra("catColor", catColor);
                        intent.putExtra("catTxtColor", catTxtColor);
                        intent.putExtra("desc", lDesc);
                    }
                    startActivity(intent);
                } else {
                    CommonConstants.displayToast(mContext, mContext.getString(R.string
                            .video_alert));
                }
            }
        });


    }

    private void startGridPullToRequest() {
        ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(category,
                0, "1", mDateCreated, mLevel, mLinkID);
        mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {

            @Override
            public void success(ScrollingObj scrollingObj, Response response) {
                try {
                    String responseText = scrollingObj.getResponseText();
                    if (!responseText.equalsIgnoreCase("Success")) {
                        CommonConstants.displayToast(mContext, responseText);
                    }
                    subPageData.put(0, scrollingObj);
                    gridRefresh.setRefreshing(false);

                    int tabPosition = 0;
                    subPagetAdapter = new subPageViewAdapter
                            (mContext, mScreenHeight,
                                    mScreenWidth, category, grid, isItemClick, true, tabPosition);
                    grid.setAdapter(subPagetAdapter);
                    subPagetAdapter.notifyDataSetChanged();

                    if (subPageData.get(0).getItems() != null) {
                        if (subPageData.get(0).getItems().size() != 0) {
                            String catTxtColor = subPageData.get(0).getItems().get(0)
                                    .getCatTxtColor();
                            String catColour = subPageData.get(0).getItems().get(0).getCatColor();
                            if (catTxtColor != null) {
                                newsTitle.setTextColor(Color.parseColor(catTxtColor));

                            }
                            if (catColour != null) {
                                customParent.setBackgroundColor(Color.parseColor(catColour));

                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    CommonConstants.displayToast(mContext, error.getResponse().getReason());
                }
            }
        });
    }


}
