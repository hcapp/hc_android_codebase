package com.scansee.newsfirst;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scansee.hubregion.R;

import java.util.TreeMap;

/**
 * Created by subramanya.v on 5/19/2016.
 */
public class subPageView extends BaseAdapter{
    private final TreeMap<Integer, ScrollingObj> scrollingData;
    private final GridView grid;
    private final boolean isItemClick;
    private int mScreenHeight;
    private int mScreenWidth;
    private final String category;
    private final Context mContext;
    int height;
    int width;

    public subPageView(Context scrollingPageActivity, TreeMap<Integer, ScrollingObj> scrollingData, int mScreenHeight, int mScreenWidth, String category, GridView grid, boolean isItemClick) {
        this.scrollingData = scrollingData;
        this.mScreenHeight = mScreenHeight;
        this.mScreenWidth = mScreenWidth;
        this.category = category;
        this.mContext = scrollingPageActivity;
        this.grid = grid;
        this.isItemClick = isItemClick;

    }

    @Override
    public int getCount() {

        return scrollingData.get(0).getItems().size();

    }

    @Override
    public Object getItem(int position) {
        return scrollingData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflaInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflaInflater.inflate(
                    R.layout.grid_view, parent, false);
            RelativeLayout gridParent = (RelativeLayout)vi.findViewById(R.id.grid_adapter_parent);
            ImageView gridImage = (ImageView)vi.findViewById(R.id.grid_image);
            holder = new ViewHolder();
            holder.count = (TextView) vi.findViewById(R.id.count);
            holder.viewSummary = (TextView) vi.findViewById(R.id.view_summary);
            holder.alphaView = (FrameLayout)vi.findViewById(R.id.alpha_view);
            holder.countParent = (RelativeLayout)vi.findViewById(R.id.count_parent);
            if(isItemClick)
            {
                holder.alphaView.setVisibility(View.GONE);
                holder.countParent.setVisibility(View.GONE);
                holder.viewSummary.setVisibility(View.GONE);
            }

            width = mScreenWidth - grid.getPaddingLeft()-grid.getPaddingRight() - grid.getHorizontalSpacing();
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) grid.getLayoutParams();
            height = mScreenHeight - grid.getPaddingBottom()-grid.getPaddingTop() - (grid.getVerticalSpacing()*3) - lp.topMargin;
            int viewHeight = height/3;

            LinearLayout.LayoutParams rowView = new LinearLayout.LayoutParams((width/2),viewHeight);
            gridParent.setLayoutParams(rowView);

            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }

            holder.count.setText(String.valueOf(scrollingData.get(0).getItems().get(position).getAuthor()));
            holder.viewSummary.setText(String.valueOf(scrollingData.get(0).getItems().get(position).getAuthor()));


        return vi;
    }
    private class ViewHolder
    {
        TextView count;
        TextView viewSummary;
        ImageView gridImage;
        FrameLayout alphaView;
        RelativeLayout countParent;

    }
}
