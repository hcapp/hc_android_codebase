package com.scansee.newsfirst;

/**
 * Created by subramanya.v on 6/6/2016.
 */
public class SideMenuModel {
    private final String hubCitiId;
    private final String userId;
    private final String sideNaviPersonalizatn;

    public SideMenuModel(String hubCitiId, String userId, String sideNaviPersonalizatn) {
        this.hubCitiId = hubCitiId;
        this.userId = userId;
        this.sideNaviPersonalizatn = sideNaviPersonalizatn;
    }
}
