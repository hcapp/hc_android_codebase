package com.scansee.newsfirst;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.scansee.newsfirst.BackgroundTask.ThumbNailAsyTask;
import com.scansee.newsfirst.model.CategoryItemList;

import java.util.ArrayList;

/**
 * Created by supriya.m on 5/18/2016.
 */
public class ThreeAcrossAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private ArrayList<CategoryItemList> mCategoryItemList;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private final String PHOTOS = "PHOTOS";
    private final String VIDEOS = "videos";

    public ThreeAcrossAdapter(Context context, ArrayList<CategoryItemList> categoryItemList) {
        this.mContext = context;
        this.mCategoryItemList = categoryItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.three_across_sub_view, parent, false);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            itemView.setLayoutParams(new RelativeLayout.LayoutParams(width / 3, ViewGroup.LayoutParams
                    .MATCH_PARENT));

            return new MyViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_more, parent, false);
            return new FooterViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            final MyViewHolder myViewHolder = (MyViewHolder) holder;
            CategoryItemList item = mCategoryItemList.get(position);
            myViewHolder.subTitleTextView.setText(item.getTitle());
            if (item.getAuthor() != null) {
                myViewHolder.subAuthorTextView.setVisibility(View.VISIBLE);
                myViewHolder.subAuthorTextView.setText(item.getAuthor());
                myViewHolder.subTitleTextView.setMaxLines(2);
            } else {
                myViewHolder.subAuthorTextView.setVisibility(View.GONE);
                myViewHolder.shortDescrition.setText(item.getsDesc());
                myViewHolder.subTitleTextView.setMaxLines(2);
            }

            String category = mCategoryItemList.get(position).getCatName();
            String link;
            if (category.equalsIgnoreCase(PHOTOS)) {
                link = mCategoryItemList.get(position).getImage();

                if (link != null) {
                    String[] images = link.split(",");
                    if (images.length > 1) {
                        setVisibilityIfPhotosVdeos(myViewHolder);
                        myViewHolder.photosCountTextView.setText(String.valueOf("" + images.length));
                    } else {
                        setVisibility(myViewHolder);
                    }
                    new CommonMethods().loadImage(mContext, myViewHolder.progressBar, images[0], myViewHolder.subImageView);
                }

            } else if (category.equalsIgnoreCase(VIDEOS)) {
                link = mCategoryItemList.get(position).getVideoLink();

                if (link != null) {
                    String[] images = link.split(",");
                    if (images.length > 1) {
                        setVisibilityIfPhotosVdeos(myViewHolder);
                        myViewHolder.photosCountTextView.setText(String.valueOf("" + images.length));
                    } else {
                        setVisibility(myViewHolder);
                    }
                    myViewHolder.playImageView.setVisibility(View.VISIBLE);
                    try {
                        ThumbNailAsyTask thumNail = new ThumbNailAsyTask(images[0], myViewHolder.subImageView);
                        thumNail.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }

            } else {
                setVisibility(myViewHolder);
                if (mCategoryItemList.get(position).getImage() != null) {
                    new CommonMethods().loadImage(mContext
                            , myViewHolder.progressBar, mCategoryItemList.get(position).getImage(), myViewHolder.subImageView);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mCategoryItemList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionFooter(int position) {
        return position == mCategoryItemList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView subTitleTextView, subAuthorTextView, photosCountTextView,shortDescrition;
        private ImageView subImageView, playImageView;
        private ProgressBar progressBar;
        private RelativeLayout countViewLayout;
        private FrameLayout alphaLayout;

        public MyViewHolder(View view) {
            super(view);
            subImageView = (ImageView) view.findViewById(R.id.three_across_sub_image);
            playImageView = (ImageView) view.findViewById(R.id.play_button);
            subTitleTextView = (TextView) view.findViewById(R.id.three_across_sub_title);
            shortDescrition = (TextView) view.findViewById(R.id.three_across_short_description);
            subAuthorTextView = (TextView) view.findViewById(R.id.three_across_author);
            photosCountTextView = (TextView) view.findViewById(R.id.count);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            countViewLayout = (RelativeLayout) view.findViewById(R.id.count_parent);
            alphaLayout = (FrameLayout) view.findViewById(R.id.alpha_view);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        ImageView viewMoreImageView;

        public FooterViewHolder(View itemView) {
            super(itemView);
            //viewMoreImageView = (ImageView) itemView.findViewById(R.id.viewMoreImageView);
        }
    }

    private void setVisibility(MyViewHolder holder) {
        holder.countViewLayout.setVisibility(View.GONE);
        holder.alphaLayout.setVisibility(View.GONE);
        holder.playImageView.setVisibility(View.GONE);
    }

    private void setVisibilityIfPhotosVdeos(MyViewHolder holder) {
        holder.countViewLayout.setVisibility(View.VISIBLE);
        holder.alphaLayout.setVisibility(View.VISIBLE);
        holder.playImageView.setVisibility(View.GONE);
    }

}
