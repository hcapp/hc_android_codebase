package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 6/22/2016.
 */
public class ListSubCatSetails {
    private int parCatId;
    private String parCatName;
    private String catColor;
    private int isSubCategory;
    private ArrayList<SubCat> listCatDetails;

    public String getParCatName() {
        return parCatName;
    }

    public void setParCatName(String parCatName) {
        this.parCatName = parCatName;
    }

    public String getCatColor() {
        return catColor;
    }

    public void setCatColor(String catColor) {
        this.catColor = catColor;
    }

    public int getIsSubCategory() {
        return isSubCategory;
    }

    public void setIsSubCategory(int isSubCategory) {
        this.isSubCategory = isSubCategory;
    }

    public ArrayList<SubCat> getListCatDetails() {
        return listCatDetails;
    }

    public void setListCatDetails(ArrayList<SubCat> listCatDetails) {
        this.listCatDetails = listCatDetails;
    }

    public int getParCatId() {
        return parCatId;
    }

    public void setParCatId(int parCatId) {
        this.parCatId = parCatId;
    }


}
