package com.scansee.newsfirst;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scansee.hubregion.R;

/**
 * Created by subramanya.v on 5/18/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class listViewAdapter extends BaseAdapter {
    private final TabFragmentView tabFragmentView;
    private final String template;
    private final int tabPosition;
    private final Context mcontext;

    public listViewAdapter(TabFragmentView tabFragmentView, String template, int tabPosition) {
        this.tabFragmentView = tabFragmentView;
        this.mcontext = tabFragmentView.getActivity();
        this.template = template;
        this.tabPosition = tabPosition;
    }

    @Override
    public int getCount() {
        int size = 0;
        try {
            if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {
                size = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition).getItems().size() - 1;
            } else {
                String subPageName = ((SubpageActivty)mcontext).subPageData.get(
                        tabPosition).getSubPageName();
                if (subPageName != null) {
                    if (subPageName.equalsIgnoreCase("Big Banner")) {
                        size = ((SubpageActivty)mcontext).subPageData.get(tabPosition).getItems().size() - 1;
                    } else {
                        size = ((SubpageActivty)mcontext).subPageData.get(tabPosition).getItems().size();
                    }
                }
            }
            return size;

        } catch (Exception e) {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        ScrollingObj item;
        if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {
            item = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition);
        } else {
            item = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition);
        }
        return item;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder = null;
        SubPageHolder subPage = null;
        int lowerLimit;
        int maxCount;
        int count = 0;
        if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {

            lowerLimit = ((ScrollingPageActivity)mcontext).scrollingData.get(
                    tabPosition).getLowerLimitFlag();
            maxCount = ((ScrollingPageActivity)mcontext).scrollingData.get(
                    tabPosition).getMaxCnt();
            count = position + 2;
            if (lowerLimit < maxCount) {
                if (count == lowerLimit) {
                    if (!((ScrollingPageActivity) mcontext).isLoaded) {
                        ((ScrollingPageActivity) mcontext).getPagination(lowerLimit, tabPosition, this, null);

                    }
                }
            }
        } else {
            String subPageName = ((SubpageActivty)mcontext).subPageData.get(
                    tabPosition).getSubPageName();
            if (subPageName != null) {
                if (subPageName.equalsIgnoreCase("Big Banner")) {
                    count = position + 2;
                } else {
                    count = position + 1;
                }
            }
            lowerLimit = ((SubpageActivty)mcontext).subPageData.get(
                    tabPosition).getLowerLimitFlag();
            maxCount = ((SubpageActivty)mcontext).subPageData.get(
                    tabPosition).getMaxCnt();

            if (lowerLimit < maxCount) {
                if (count == lowerLimit) {
                    if (!((SubpageActivty) mcontext).isLoaded) {
                        ((SubpageActivty) mcontext).getPagination(lowerLimit, tabPosition, this, null);

                    }
                }
            }
        }



        if (convertView == null) {
            LayoutInflater inflaInflater = (LayoutInflater) this.tabFragmentView.getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {

                if (((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition).getItems
                        ().size() != 0) {
                    String imgPosition = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition).getItems
                            ().get(position).getImgPosition();
                    if (imgPosition != null) {
                        if (imgPosition.equalsIgnoreCase("Left")) {
                            vi = inflaInflater.inflate(
                                    R.layout.scrolling_left_adapter, parent, false);
                        } else {
                            vi = inflaInflater.inflate(
                                    R.layout.scrolling_adapter, parent, false);
                        }
                    } else {
                        vi = inflaInflater.inflate(
                                R.layout.scrolling_left_adapter, parent, false);
                    }
                }
                holder = new ViewHolder();
                holder.subStoryTitle = (TextView) vi.findViewById(R.id.sub_story_title);
                holder.subStoryAuthor = (TextView) vi.findViewById(R.id.sub_story_author);
                holder.subStoryChild=(LinearLayout) vi.findViewById(R.id.sub_story_child);
                holder.subStorySummary = (TextView) vi.findViewById(R.id.sub_story_summary);
                holder.logo = (ImageView) vi.findViewById(R.id.sub_story_logo);
                holder.subStoryTime=(TextView) vi.findViewById(R.id.sub_story_time);
                holder.progressBar = (ProgressBar) vi.findViewById(R.id.progress);
                vi.setTag(holder);
            } else {
                vi = inflaInflater.inflate(
                        R.layout.tab_subpage_adapter, parent, false);

                subPage = new SubPageHolder();
                subPage.author = (TextView) vi.findViewById(R.id.sub_story_author);
                // changes to include date in subpages HC-569 Sprint 3 task.
                subPage.subStoryTime=(TextView) vi.findViewById(R.id.sub_story_time);
                subPage.subStoryChild=(LinearLayout) vi.findViewById(R.id.sub_story_child);
                subPage.shortDescription = (TextView) vi.findViewById(R.id.short_description);

                subPage.title = (TextView) vi.findViewById(R.id.sub_story_title);
                subPage.bigBanner = (ImageView) vi.findViewById(R.id.subpage_image);
                subPage.progressBar = (ProgressBar) vi.findViewById(R.id.progress);
                vi.setTag(subPage);
            }
        } else {
            if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {
                holder = (ViewHolder) vi.getTag();
            } else {
                subPage = (SubPageHolder) vi.getTag();
            }
        }
        if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {

            String title = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition)
                    .getItems().get(position + 1).getTitle();
            if (title != null && !title.equalsIgnoreCase("")) {
                assert holder != null;
                holder.subStoryTitle.setText(title);
            }
            String author = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition)
                    .getItems().get(position + 1).getAuthor();
            if (author != null) {
                if (author.length() > 15) {
                    author = author.substring(0, 15).concat("...");
                }
            }
            if (author != null && !author.equalsIgnoreCase("")) {
                assert holder != null;
                holder.subStoryAuthor.setVisibility(View.VISIBLE);
                holder.subStoryChild.setVisibility(View.VISIBLE);
                holder.subStoryAuthor.setText(author.concat("  "));
            } else {
                holder.subStoryAuthor.setVisibility(View.GONE);
            }


            String time = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition)
                    .getItems().get(position + 1).getTime();
            String date = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition)
                    .getItems().get(position + 1).getDate();
            if ((author == null) && (time == null) && (date == null)) {
                holder.subStoryChild.setVisibility(View.GONE);
            }
            if(date != null) {
                holder.subStoryChild.setVisibility(View.VISIBLE);
                if(time != null) {
                    time = date.concat(" " + time);
                }
                else
                {
                    time = date;
                }
            }
            if (time != null && !time.equalsIgnoreCase("")) {
                assert holder != null;
                holder.subStoryTime.setText(time);
            }

            String des = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition)
                    .getItems().get(position + 1).getsDesc();
            if (des != null && !des.equalsIgnoreCase("")) {
                assert holder != null;
                holder.subStorySummary.setText(des);
            }
            String image = ((ScrollingPageActivity)mcontext).scrollingData.get(tabPosition)
                    .getItems().get(position + 1).getImage();
            if (image != null && !image.equalsIgnoreCase("")) {
                image = image.replaceAll(" ", "%20");

                new CommonMethods().loadImage(mcontext, holder.progressBar, image, holder.logo);
                assert holder != null;
            }

        } else {
            String subPageName = ((SubpageActivty)mcontext).subPageData.get(
                    tabPosition).getSubPageName();
            int itemPosition;
            if (subPageName.equalsIgnoreCase("Big Banner")) {
                itemPosition = position + 1;
            } else {
                itemPosition = position;
            }


            String author = ((SubpageActivty)mcontext).subPageData.get
                    (tabPosition).getItems().get(itemPosition).getAuthor();
            //String author = null;
            if (author != null) {
                if (author.length() > 15) {
                    author = author.substring(0, 15).concat("...");
                }
            }

            if (author != null && !author.equalsIgnoreCase("")) {
                assert subPage != null;



                subPage.author.setVisibility(View.VISIBLE);
                subPage.subStoryChild.setVisibility(View.VISIBLE);
                subPage.author.setText(author.concat("  "));
            } else {
                subPage.author.setVisibility(View.GONE);
            }

            // changes to include date in subpages HC-569 Sprint 3 task.
            String time = ((SubpageActivty)mcontext).subPageData.get
                    (tabPosition).getItems().get(itemPosition).getTime();
            String date = ((SubpageActivty)mcontext).subPageData.get
                    (tabPosition).getItems().get(itemPosition).getDate();
            if(date != null) {
                subPage.subStoryChild.setVisibility(View.VISIBLE);
                if(time != null) {
                    time = date.concat(" " + time);
                }
                else
                {
                    time = date;
                }
            }
            if (time != null && !time.equalsIgnoreCase("")) {
                assert holder != null;
                subPage.subStoryTime.setText(time);
            }

            if((author == null) && (time == null) && (date == null))
            {
                subPage.subStoryChild.setVisibility(View.GONE);
            }
            String title = ((SubpageActivty)mcontext).subPageData.get(
                    tabPosition).getItems().get(itemPosition).getTitle();
            if (title != null && !title.equalsIgnoreCase("")) {
                assert subPage != null;
                subPage.title.setText(title);
            }

            String desc = ((SubpageActivty)mcontext).subPageData.get(
                    tabPosition).getItems().get(itemPosition).getsDesc();
            if (desc != null && !desc.equalsIgnoreCase("")) {
                assert holder != null;
                subPage.shortDescription.setText(desc);
            }
            String image = ((SubpageActivty)mcontext).subPageData.get(tabPosition)
                    .getItems().get(itemPosition).getImage();
            if (image != null && !image.equalsIgnoreCase("")) {
                image = image.replaceAll(" ", "%20");
                assert subPage != null;
                new CommonMethods().loadImage(mcontext, subPage.progressBar, image, subPage.bigBanner);

            }

        }


        return vi;
    }


    private class ViewHolder {
        TextView subStoryTitle;
        TextView subStoryAuthor;
        TextView subStoryTime;
        TextView subStorySummary;
        ImageView logo;
        ProgressBar progressBar;
        LinearLayout subStoryChild;
    }

    private class SubPageHolder {
        TextView author;
        TextView shortDescription;
        TextView title;
        LinearLayout subStoryChild;
        TextView subStoryTime;
        ImageView bigBanner;
        ProgressBar progressBar;

    }

}

