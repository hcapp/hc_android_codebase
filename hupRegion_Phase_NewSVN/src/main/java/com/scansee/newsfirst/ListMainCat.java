package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by supriya.m on 6/3/2016.
 */
public class ListMainCat
{
	public String getCatId()
	{
		return catId;
	}

	public void setCatId(String catId)
	{
		this.catId = catId;
	}

	public String getCatName()
	{
		return catName;
	}

	public void setCatName(String catName)
	{
		this.catName = catName;
	}

	public int getFlag()
	{
		return flag;
	}

	public void setFlag(int flag)
	{
		this.flag = flag;
	}

	public String getSortOrder()
	{
		return sortOrder;
	}

	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	public ArrayList<MainCatList> getMainCatList()
	{
		return mainCatList;
	}

	public void setMainCatList(ArrayList<MainCatList> mainCatList)
	{
		this.mainCatList = mainCatList;
	}

	private String catId;
	private String catName;
	private int flag;

	public int getIsSubCategory() {
		return isSubCategory;
	}

	public void setIsSubCategory(int isSubCategory) {
		this.isSubCategory = isSubCategory;
	}

	private int isSubCategory;
	private String sortOrder;
	private String catTxtColor;
	private ArrayList<MainCatList> mainCatList;


	public String getCatTxtColor() {
		return catTxtColor;
	}

	public void setCatTxtColor(String catTxtColor) {
		this.catTxtColor = catTxtColor;
	}


}
