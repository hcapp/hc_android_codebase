package com.scansee.newsfirst.model;

/**
 * Created by subramanya.v on 6/22/2016.
 */
public class SubPageModel {
    private final String userId;
    private final String catName;

    public SubPageModel(String userId, String catName) {
        this.userId = userId;
        this.catName = catName;
    }
}
