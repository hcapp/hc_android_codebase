package com.scansee.newsfirst.model;

/**
 * Created by sharanamma on 01-06-2016.
 */
public class CombinationTempRequest {
    private final String hubCitiID;
    private final String TemplateType;
    public CombinationTempRequest(String hubCitiID, String TemplateType) {
        this.hubCitiID = hubCitiID;
        this.TemplateType = TemplateType;
    }
}
