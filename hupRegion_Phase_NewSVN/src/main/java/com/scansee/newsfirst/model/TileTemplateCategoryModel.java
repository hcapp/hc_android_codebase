package com.scansee.newsfirst.model;

/**
 * Created by sharanamma on 23-06-2016.
 */
public class TileTemplateCategoryModel {
    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public String getCatColor() {
        return catColor;
    }

    public void setCatColor(String catColor) {
        this.catColor = catColor;
    }

    public String getCatImgPath() {
        return catImgPath;
    }

    public void setCatImgPath(String catImgPath) {
        this.catImgPath = catImgPath;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getIsSubcategory() {
        return isSubcategory;
    }

    public void setIsSubcategory(int isSubcategory) {
        this.isSubcategory = isSubcategory;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    private int rowNum;
    private int catId;
    private int isSubcategory;
    private String catName;
    private String catImgPath;
    private String catColor;

    public String getNonfeedlink() {
        return nonfeedlink;
    }

    public void setNonfeedlink(String nonfeedlink) {
        this.nonfeedlink = nonfeedlink;
    }

    private String nonfeedlink;

    public String getCatTxtColor() {
        return catTxtColor;
    }

    public void setCatTxtColor(String catTxtColor) {
        this.catTxtColor = catTxtColor;
    }

    private String catTxtColor;

    public String getBackButtonColor() {
        return backButtonColor;
    }

    public void setBackButtonColor(String backButtonColor) {
        this.backButtonColor = backButtonColor;
    }

    private String backButtonColor;
}
