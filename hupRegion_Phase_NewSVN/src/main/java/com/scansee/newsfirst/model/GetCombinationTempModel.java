package com.scansee.newsfirst.model;

import java.util.ArrayList;

/**
 * Created by sharanamma on 31-05-2016.
 */
public class GetCombinationTempModel {
    private String responseCode;
    private String responseText;
    private String bannerImg;
    private int maxCnt;
    private int nextPage;
    private int lowerLimitFlag;
    private int templateChanged;
    private String modifiedDate;
    private String newtempName;

    public String getBkImgPath()
    {
        return bkImgPath;
    }

    public void setBkImgPath(String bkImgPath)
    {
        this.bkImgPath = bkImgPath;
    }

    public String getHomeImgPath()
    {
        return homeImgPath;
    }

    public void setHomeImgPath(String homeImgPath)
    {
        this.homeImgPath = homeImgPath;
    }

    public String getTitleTxtColor()
    {
        return titleTxtColor;
    }

    public void setTitleTxtColor(String titleTxtColor)
    {
        this.titleTxtColor = titleTxtColor;
    }

    public String getTitleBkGrdColor()
    {
        return titleBkGrdColor;
    }

    public void setTitleBkGrdColor(String titleBkGrdColor)
    {
        this.titleBkGrdColor = titleBkGrdColor;
    }

    public String getWeatherURL()
    {
        return weatherURL;
    }

    public void setWeatherURL(String weatherURL)
    {
        this.weatherURL = weatherURL;
    }

    private String bkImgPath;
    private String homeImgPath;

    public String getHumbergurImgPath() {
        return humbergurImgPath;
    }

    public void setHumbergurImgPath(String humbergurImgPath) {
        this.humbergurImgPath = humbergurImgPath;
    }

    private String humbergurImgPath;
    private String titleTxtColor;
    private String titleBkGrdColor;
    private String weatherURL;

    public int getTemplateChanged() {
        return templateChanged;
    }

    public void setTemplateChanged(int templateChanged) {
        this.templateChanged = templateChanged;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getNewtempName() {
        return newtempName;
    }

    public void setNewtempName(String newtempName) {
        this.newtempName = newtempName;
    }



    public ArrayList<CategoryListModel> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<CategoryListModel> categoryList) {
        this.categoryList = categoryList;
    }

    public int getLowerLimitFlag() {
        return lowerLimitFlag;
    }

    public void setLowerLimitFlag(int lowerLimitFlag) {
        this.lowerLimitFlag = lowerLimitFlag;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public int getMaxCnt() {
        return maxCnt;
    }

    public void setMaxCnt(int maxCnt) {
        this.maxCnt = maxCnt;
    }

    public String getBannerImg() {
        return bannerImg;
    }

    public void setBannerImg(String bannerImg) {
        this.bannerImg = bannerImg;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    private ArrayList<CategoryListModel> categoryList;
}
