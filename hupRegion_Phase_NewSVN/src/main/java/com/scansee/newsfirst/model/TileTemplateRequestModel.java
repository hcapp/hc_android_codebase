package com.scansee.newsfirst.model;

/**
 * Created by sharanamma on 23-06-2016.
 */
public class TileTemplateRequestModel {

    private final String userId;
    private final String hubCitiId;
    private final String dateCreated;
    private final String linkId;
    private final String level;

    public TileTemplateRequestModel(String userId, String hubCitiId,String dateCreated,String linkID,String level) {
        this.userId = userId;
        this.hubCitiId = hubCitiId;
        this.dateCreated = dateCreated;
        this.linkId = linkID;
        this.level = level;
    }

}
