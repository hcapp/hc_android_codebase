package com.scansee.newsfirst.model;

import java.util.ArrayList;

/**
 * Created by sharanamma on 01-06-2016.
 */
public class CategoryListModel {
    private String catName;
    private String groupContent;
    private String catColor;
    private String nonfeedlink;

    public String getBackButtonColor() {
        return backButtonColor;
    }

    public void setBackButtonColor(String backButtonColor) {
        this.backButtonColor = backButtonColor;
    }

    private String backButtonColor;

    public String getNonfeedlink() {
        return nonfeedlink;
    }

    public void setNonfeedlink(String nonfeedlink) {
        this.nonfeedlink = nonfeedlink;
    }
    public String getCatTxtColor() {
        return catTxtColor;
    }

    public void setCatTxtColor(String catTxtColor) {
        this.catTxtColor = catTxtColor;
    }

    private String catTxtColor;
    private ArrayList<CategoryItemList> itemList;

    public ArrayList<CategoryItemList> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<CategoryItemList> itemList) {
        this.itemList = itemList;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getGroupContent() {
        return groupContent;
    }

    public void setGroupContent(String groupContent) {
        this.groupContent = groupContent;
    }
    public String getCatColor() {
        return catColor;
    }

    public void setCatColor(String catColor) {
        this.catColor = catColor;
    }


}
