package com.scansee.newsfirst.model;

import java.util.ArrayList;

/**
 * Created by sharanamma on 24-06-2016.
 */
public class TileTemplateModel {
    private String responseCode;
    private String responseText;
    private int templateChanged;
    private String newtempName;
    private String bannerImg;

    public String getBkImgPath()
    {
        return bkImgPath;
    }

    public void setBkImgPath(String bkImgPath)
    {
        this.bkImgPath = bkImgPath;
    }

    public String getHomeImgPath()
    {
        return homeImgPath;
    }

    public void setHomeImgPath(String homeImgPath)
    {
        this.homeImgPath = homeImgPath;
    }

    public String getTitleTxtColor()
    {
        return titleTxtColor;
    }

    public void setTitleTxtColor(String titleTxtColor)
    {
        this.titleTxtColor = titleTxtColor;
    }

    public String getTitleBkGrdColor()
    {
        return titleBkGrdColor;
    }

    public void setTitleBkGrdColor(String titleBkGrdColor)
    {
        this.titleBkGrdColor = titleBkGrdColor;
    }

    public String getWeatherURL()
    {
        return weatherURL;
    }

    public void setWeatherURL(String weatherURL)
    {
        this.weatherURL = weatherURL;
    }

    private String bkImgPath;
    private String homeImgPath;

    public String getHumbergurImgPath() {
        return humbergurImgPath;
    }

    public void setHumbergurImgPath(String humbergurImgPath) {
        this.humbergurImgPath = humbergurImgPath;
    }

    private String humbergurImgPath;
    private String titleTxtColor;
    private String titleBkGrdColor;
    private String weatherURL;

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    private String modifiedDate;

    public String getBannerImg() {
        return bannerImg;
    }

    public void setBannerImg(String bannerImg) {
        this.bannerImg = bannerImg;
    }


    public String getNewtempName() {
        return newtempName;
    }

    public void setNewtempName(String newtempName) {
        this.newtempName = newtempName;
    }

    public int getTemplateChanged() {
        return templateChanged;
    }

    public void setTemplateChanged(int templateChanged) {
        this.templateChanged = templateChanged;
    }

    public ArrayList<TileTemplateCategoryModel> getItems() {
        return items;
    }

    public void setItems(ArrayList<TileTemplateCategoryModel> items) {
        this.items = items;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    private ArrayList<TileTemplateCategoryModel> items;


}
