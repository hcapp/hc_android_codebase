package com.scansee.newsfirst.model;

import java.util.ArrayList;

/**
 * Created by sharanamma on 08-06-2016.
 */
public class NewsDetailsModel {
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    private String responseCode;
    private String responseText;

    public ArrayList<NewsDetailsItemList> getItemList() {
        return items;
    }

    public void setItemList(ArrayList<NewsDetailsItemList> itemList) {
        this.items = itemList;
    }

    private ArrayList<NewsDetailsItemList> items;


}
