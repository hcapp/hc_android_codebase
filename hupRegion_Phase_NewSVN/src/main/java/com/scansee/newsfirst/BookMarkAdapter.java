package com.scansee.newsfirst;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.scansee.hubregion.R;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import static android.R.attr.id;

/**
 * Created by subramanya.v on 5/18/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class BookMarkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements ItemTouchHelperAdapter {

    private final LinkedList<BookMarkObj> bookMarkList;

    private final BookMarkActivity bookMarkActivity;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private final Context mContext;


    public BookMarkAdapter(LinkedList<BookMarkObj> bookMarkList, BookMarkActivity bookMarkActivity, Context mContext) {
        this.bookMarkList = bookMarkList;
        this.bookMarkActivity = bookMarkActivity;
        this.mContext = mContext;
        // uncomment if you want to open only one row at a time
        binderHelper.setOpenOnlyOne(true);
    }

    @Override
    public int getItemViewType(int position) {
        return bookMarkList.get(position).isHeader();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemViewHolder itemViewHolder;
        HeaderViewHolder headerViewHolder;
        View view;

        if (viewType == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_row, parent, false);

            headerViewHolder = new HeaderViewHolder(view);
            return headerViewHolder;
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmark_row, parent, false);
            itemViewHolder = new ItemViewHolder(view);
            return itemViewHolder;
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        String category = bookMarkList.get(position).getCategory();
        final String dragCategory = bookMarkList.get(position).getDragCategory();
        if (bookMarkList.get(position).isHeader() == 1) {
            HeaderViewHolder itemView = (HeaderViewHolder) holder;
            itemView.category.setText(category);
        } else {
            final ItemViewHolder headerViewHolder = (ItemViewHolder) holder;

            if (!bookMarkList.get(position).isInBothSections()) {
                headerViewHolder.addRow.setImageResource(R.drawable.plus);
            } else {
                headerViewHolder.addRow.setImageResource(R.drawable.minus);
            }
            if(category != null && !category.isEmpty()) {
                binderHelper.bind(headerViewHolder.swipeLayout, category);
            }
            headerViewHolder.category.setText(category);

            if (!dragCategory.equalsIgnoreCase(bookMarkActivity.getString(R
                    .string
                    .book_mark))) {
                headerViewHolder.handleView.setVisibility(View.GONE);
                headerViewHolder.swipeLayout.setLockDrag(true);
            } else {
                headerViewHolder.handleView.setVisibility(View.VISIBLE);
                headerViewHolder.swipeLayout.setLockDrag(false);

            }
            headerViewHolder.deleteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if(((BookMarkActivity)mContext).bookMark.getVisibility() == View.VISIBLE)
                        {
                            addRemoveBookMarkList(position);
                        }
                        else {
                            addRemoveSideMenuList(position);
                        }
                        headerViewHolder.swipeLayout.close(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            headerViewHolder.addRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (!dragCategory.equalsIgnoreCase(bookMarkActivity.getString(R
                                .string
                                .book_mark))) {
                            if(((BookMarkActivity)mContext).bookMark.getVisibility() == View.VISIBLE) {
                                addRemoveBookMarkList(position);
                            }
                            else {
                                addRemoveSideMenuList(position);
                            }
                        } else {
                            headerViewHolder.swipeLayout.open(true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            // Start a drag whenever the handle view it touched
            headerViewHolder.handleView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                        bookMarkActivity.onStartDrag(headerViewHolder);
                    }
                    return false;
                }
            });
        }
    }

    private void addRemoveSideMenuList(int position) {
        String category = bookMarkList.get(position).getCategory();
        int isHubFunctn = bookMarkList.get(position).getIsHubFunctn();
        int parCatId = bookMarkList.get(position).getParCatId();
        if(bookMarkList.get(position).getDragCategory().equalsIgnoreCase(bookMarkActivity.getString(R
                .string
                .book_mark))) {
            bookMarkList.remove(position);
            sideMenuAdd(category, bookMarkActivity.getString(R.string.other_section), false, parCatId,
                    bookMarkList.size(), isHubFunctn);
        }
        else
        {
            bookMarkList.remove(position);
            int dragPos = 0;
            for (BookMarkObj dragCategory : bookMarkList) {
                if (dragCategory.getDragCategory().equalsIgnoreCase(bookMarkActivity.getString(R
                        .string
                        .book_mark))) {
                    dragPos++;
                }
            }
            sideMenuAdd(category, bookMarkActivity.getString(R.string.book_mark), true, parCatId, dragPos, isHubFunctn);

        }
        notifyDataSetChanged();
    }

    private void addRemoveBookMarkList(int position) {
        String category = bookMarkList.get(position).getCategory();
        int parCatId = bookMarkList.get(position).getParCatId();
        if (bookMarkList.get(position).getDragCategory().equalsIgnoreCase(bookMarkActivity.getString(R
                .string
                .book_mark))) {
            bookMarkList.remove(position);
            bookMarkAdd(category, bookMarkActivity.getString(R.string.other_section), false, parCatId, bookMarkList.size());

        } else {
            bookMarkList.remove(position);
            int dragPos = 0;
            for (BookMarkObj dragCategory : bookMarkList) {
                if (dragCategory.getDragCategory().equalsIgnoreCase(bookMarkActivity.getString(R
                        .string
                        .book_mark))) {
                    dragPos++;
                }
            }
            bookMarkAdd(category, bookMarkActivity.getString(R.string.book_mark), true, parCatId, dragPos);
        }
        notifyDataSetChanged();

    }

    private void bookMarkAdd(String category, String dragCat, boolean isBothSection, int parCatId, int position) {
        BookMarkObj bookMarkData = new BookMarkObj();
        bookMarkData.setCategory(category);
        bookMarkData.setIsHeader(0);
        bookMarkData.setDragCategory(dragCat);
        bookMarkData.setInBothSections(isBothSection);
        bookMarkData.setParCatId(parCatId);
        bookMarkList.add(position, bookMarkData);
    }
    private void sideMenuAdd(String category, String dragCat, boolean isBothSection, int parCatId, int position, int isHubFunctn) {
        BookMarkObj bookMarkData = new BookMarkObj();
        bookMarkData.setCategory(category);
        bookMarkData.setIsHeader(0);
        bookMarkData.setDragCategory(dragCat);
        bookMarkData.setInBothSections(isBothSection);
        bookMarkData.setIsHubFunctn(isHubFunctn);
        bookMarkData.setParCatId(parCatId);
        bookMarkList.add(position, bookMarkData);
    }

    @Override
    public int getItemCount() {
        return bookMarkList.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (bookMarkList.get(fromPosition).getDragCategory().equalsIgnoreCase(bookMarkList.get(toPosition).getDragCategory())) {
            Collections.swap(bookMarkList, fromPosition, toPosition);
            notifyItemMoved(fromPosition, toPosition);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onItemDismiss(int position) {
        try {
//            addRemoveList(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        public final TextView category;
        public final ImageView handleView;
        private final ImageView addRow;
        private final SwipeRevealLayout swipeLayout;
        private final View deleteLayout;


        public ItemViewHolder(View itemView) {
            super(itemView);
            category = (TextView) itemView.findViewById(R.id.category);
            handleView = (ImageView) itemView.findViewById(R.id.handle);
            addRow = (ImageView) itemView.findViewById(R.id.add_row);
            swipeLayout = (SwipeRevealLayout) itemView.findViewById(R.id.swipe_layout);
            deleteLayout = itemView.findViewById(R.id.delete_layout);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        public final TextView category;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            category = (TextView) itemView.findViewById(R.id.category);
        }

    }


}
