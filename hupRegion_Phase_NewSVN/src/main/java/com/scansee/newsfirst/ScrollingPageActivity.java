package com.scansee.newsfirst;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.BookMarkModel;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.ScrollingPageModel;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.MenuPropertiesActivity;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.TreeMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ScrollingPageActivity extends MenuPropertiesActivity {


    private ViewPager viewPager;
    public TabLayout tabLayout, subPageTabLayout;
    private int mScreenHeight;
    private int mScreenWidth;
    private static int tabPosition;

    public String template = "Scrolling News Template";
    public ImageView  contentSearch;
    public TextView newsTitle;
    private RelativeLayout tabParent;
    private LinearLayout screenSummary;
    public TreeMap<Integer, ScrollingObj> scrollingData = new TreeMap<>();
    public final LinkedList<BookMarkObj> bookMarkData = new LinkedList<>();
    private ProgressDialog progDialog;
    private ProgressDialog scrollprogDialog;
    private RestClient mRestClient;
    private int lowerLimitFlag = 0;
    public boolean isLoaded = false;
    public boolean isGridLoaded = false;
    private FragmentActivity mContext;

    private ListView sideMenuList;
    private String mDateCreated = null, mLinkID = "0", mLevel = "1", mItemId = "0";
    public ImageView bannerImage, drawerIcon;
    public RelativeLayout customParent;
    View subPageHeader;
    private String weatherUrl;
    private String isSideBar = "0";

    private ProgressBar progressBar;
    private boolean isRefreshNeeded;
    public ImageView HomeView, weatherIcon, backImage;
    public ProgressDialog gridProgDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrolling_page);
        mContext = ScrollingPageActivity.this;
        scrollingData = new TreeMap<>();
        getIntentData();

        // Added by Supriya
        // Setting level to in parent activity to navigate and call api for proper level(Which is
        // required to differenciate
        // between MainMenu and SubMenu)
        previousMenuLevel = mLevel;
        mLinkId = mLinkID;
        bindView();

        mRestClient = RestClient.getInstance();


        if (mLevel.equals("1")) {
            GlobalConstants.isFromNewsTemplate = true;
            GlobalConstants.className = Constants.SCROLLING;
        }
        ViewTreeObserver observer = screenSummary.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                mScreenHeight = screenSummary.getMeasuredHeight();
                mScreenWidth = screenSummary.getMeasuredWidth();
                addTabViews();

                ViewTreeObserver obs = screenSummary.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    obs.removeGlobalOnLayoutListener(this);
                }
            }

        });
    }

    //Code added by Supriya
    @Override
    protected void onResume() {
        //Side menu set up
        setUpDrawer();

        CommonConstants.previousMenuLevel = mLevel;
        CommonConstants.commonLinkId = mLinkID;

        if (isRefreshNeeded) {
            lowerLimitFlag = 0;
            getscroltemplate(tabPosition, "0");
        }
        String nonFeedLink = null;
        if (bookMarkData.size() > 0) {
            nonFeedLink = bookMarkData.get(0).getBookMarkList().get(0)
                    .getListCatDetails().get
                            (tabPosition).getNonfeedlink();
        }

        if (nonFeedLink != null) {
            isRefreshNeeded = false;
        } else {
            isRefreshNeeded = true;
        }

        isRefresh = false;
        NewsTemplateDataHolder.className = Constants.SCROLLING;
        super.onResume();
    }

    //Added by Sharanamma
    private void setUpDrawer() {

        CommonConstants.previousMenuLevel = mLevel;
        NewsTemplateDataHolder.className = Constants.SCROLLING;
        CommonConstants.commonLinkId = mLinkID;

        callSideMenuApi((CustomNavigation) findViewById(R.id.custom_navigation), true);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Constants.LINKID)) {
            mLinkID = intent.getStringExtra(Constants.LINKID);
        }
        if (intent != null && intent.hasExtra(Constants.ITEMID)) {
            mItemId = intent.getStringExtra(Constants.ITEMID);
        }
        if (intent != null && intent.hasExtra(Constants.LEVEL)) {
            mLevel = intent.getStringExtra(Constants.LEVEL);
        }
        if (intent != null && intent.hasExtra("templateType")) {
            template = intent.getExtras().getString("templateType");
        }
        if (intent != null && intent.hasExtra("isSideBar")) {
            isSideBar = intent.getExtras().getString("isSideBar");
        }
    }


    public void startPullToRequest(final int selectedTab, final BaseAdapter adapter) {
        try {
            final String catName, nonFeedUrl;

            catName = bookMarkData.get(0).getBookMarkList().get(0).getListCatDetails().get
                    (selectedTab).getParCatName();
            nonFeedUrl = bookMarkData.get(0).getBookMarkList().get(0).getListCatDetails().get
                    (selectedTab).getNonfeedlink();
            isSideBar = "0";

            ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(catName, 0,
                    isSideBar, mDateCreated, mLevel, mLinkID);
            mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {


                @Override
                public void success(ScrollingObj scrollingObj, Response response) {
                    try {
                        if (scrollingObj.getResponseCode().equals("10000")) {
                            //Check for template change
                            if (scrollingObj.getTemplateChanged() == 1) {

                                if (scrollingObj.getNewtempName().equalsIgnoreCase(Constants
                                        .COMBINATION)) {
                                    Intent intent = new Intent(ScrollingPageActivity.this,
                                            CombinationTemplate.class);
                                    intent.putExtra(Constants.LEVEL, mLevel);
                                    intent.putExtra(Constants.LINKID, mLinkID);
                                    startActivity(intent);
                                    finish();
                                } else if (scrollingObj.getNewtempName().equalsIgnoreCase(Constants
                                        .NEWS_TILE)) {
                                    Intent intent = new Intent(ScrollingPageActivity.this,
                                            TwoTileNewsTemplateActivity.class);
                                    intent.putExtra(Constants.LEVEL, mLevel);
                                    intent.putExtra(Constants.LINKID, mLinkID);
                                    startActivity(intent);
                                    finish();
                                } else if (scrollingObj.getNewtempName().equalsIgnoreCase(Constants
                                        .SCROLLING)) {

                                    setUpDrawer();
                                    loadValues(scrollingObj, selectedTab, catName, nonFeedUrl);
                                } else {
                                    GlobalConstants.isFromNewsTemplate = false;
                                    SubMenuStack.clearSubMenuStack();
                                    MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null,
                                            null, 0,
                                            null, mLinkId, null, null, null, null, "1", null,
                                            null, null,
                                            null, null, null, null);
                                    SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
                                    MenuAsyncTask menu = new MenuAsyncTask(ScrollingPageActivity
                                            .this);
                                    menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mLevel,
                                            mItemId,
                                            mLinkID, "None", "0", "0", "NewsTemplate");
                                }
                            } else {
                                try {
                                    loadPullToRequestData(selectedTab, scrollingObj,
                                            catName, adapter);
                                } catch (Exception e) {
                                    //if user moves to subpage and coming back to scrolling page
                                    loadPullToRequestData(selectedTab,
                                            scrollingObj, catName, adapter);
                                    e.printStackTrace();
                                }
                            }
                        } else if (scrollingObj.getResponseCode().equals("10005")) {
                            setDrawerIcon(scrollingObj.getHumbergurImgPath());

                            Toast.makeText(ScrollingPageActivity.this, scrollingObj
                                            .getResponseText(),
                                    Toast.LENGTH_LONG)
                                    .show();
                            TabFragmentView frag = null;

                            if (!(catName.equalsIgnoreCase("photos") || catName
                                    .equalsIgnoreCase("Videos"))) {
                                frag = (TabFragmentView) viewPager.getAdapter()
                                        .instantiateItem(viewPager, selectedTab);
                            } else {
                                GridFragment gridFrag = (GridFragment) viewPager.getAdapter()
                                        .instantiateItem(viewPager, selectedTab);
                                if (gridFrag.gridRefresh != null) {
                                    gridFrag.gridRefresh.setRefreshing(false);
                                }
                            }

                            assert frag != null;
                            if (frag.swipeRefreshLayout != null) {
                                frag.swipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getResponse() != null) {
                        Toast.makeText(ScrollingPageActivity.this, error.getResponse().getReason()
                                , Toast
                                        .LENGTH_LONG).show();
                    }
                    try {
                        TabFragmentView frag;

                        frag = (TabFragmentView) viewPager.getAdapter()
                                .instantiateItem(viewPager, selectedTab);

                        if (frag.swipeRefreshLayout != null) {
                            frag.swipeRefreshLayout.setRefreshing(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadPullToRequestData(int selectedTab, ScrollingObj
            scrollingObj, String catName, BaseAdapter adapter) {


        scrollingData.put(selectedTab, scrollingObj);

        TabFragmentView frag = null;
        View moreResultsView = null;

        if (!(catName.equalsIgnoreCase("photos") || catName.equalsIgnoreCase("Videos"))) {
            frag = (TabFragmentView) viewPager.getAdapter()
                    .instantiateItem(viewPager, selectedTab);
            moreResultsView = frag.moreResultsView;
        } else {
            GridFragment gridFrag = (GridFragment) viewPager.getAdapter()
                    .instantiateItem(viewPager, selectedTab);
            if (gridFrag.gridRefresh != null) {
                gridFrag.gridRefresh.setRefreshing(false);
            }
        }

        if (frag != null) {
            if (frag.swipeRefreshLayout != null) {
                frag.swipeRefreshLayout.setRefreshing(false);
            }
        }
        if (moreResultsView != null) {
            int nextPage = 0;

            nextPage = scrollingData.get
                    (selectedTab).getNextPage();


            if (nextPage ==
                    1) {
                frag.viewPagerFragment.removeFooterView(moreResultsView);
                frag.viewPagerFragment.addFooterView(moreResultsView);
                moreResultsView.setVisibility(View.VISIBLE);
            } else {
                frag.viewPagerFragment.removeFooterView(moreResultsView);
                moreResultsView.setVisibility(View.GONE);
            }

        }

        if (Constants.getNavigationBarValues("titleBkGrdColor") != null) {
            customParent.setBackgroundColor(Color.parseColor(Constants.getNavigationBarValues
                    ("titleBkGrdColor")));
        }

        adapter.notifyDataSetChanged();
    }


    private void startPagination(final int selectedTab, final BaseAdapter adapter) {

        try {
            final String catName;

            catName = bookMarkData.get(0).getBookMarkList().get(0).getListCatDetails().get
                    (selectedTab).getParCatName();
            isSideBar = "0";

            ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(catName,
                    lowerLimitFlag, isSideBar, mDateCreated, mLevel, mLinkID);
            mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {

                @Override
                public void success(ScrollingObj scrollingObj, Response response) {
                    String responseText = scrollingObj.getResponseText();
                    if (!responseText.equalsIgnoreCase("Success")) {
                        Toast.makeText(ScrollingPageActivity.this, responseText, Toast.LENGTH_LONG)
                                .show();
                    }
                    try {
                        loadMoreData(template, selectedTab, catName, scrollingObj, adapter);
                    } catch (Exception e) {
                        //if user moves to subpage and coming back to scrolling page
                        loadMoreData("Scrolling News Template", selectedTab, catName,
                                scrollingObj, adapter);
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getResponse() != null) {
                        Toast.makeText(ScrollingPageActivity.this, error.getResponse().getReason()
                                , Toast
                                        .LENGTH_LONG).show();
                    }
                }
            });
        } catch (Exception e) {
            isLoaded = false;
            e.printStackTrace();
        }

    }

    private void loadMoreData(String template, int selectedTab, String catName, ScrollingObj
            scrollingObj, BaseAdapter adapter) {
        TabFragmentView frag = null;
        View moreResultsView = null;

        scrollingData.get(selectedTab).setItems(scrollingObj.getItems());
        scrollingData.get(selectedTab).setMaxCnt(scrollingObj.getMaxCnt());
        scrollingData.get(selectedTab).setLowerLimitFlag(scrollingObj
                .getLowerLimitFlag());
        scrollingData.get(selectedTab).setNextPage(scrollingObj.getNextPage());

        if (!(catName.equalsIgnoreCase("photos") || catName.equalsIgnoreCase("Videos"))) {
            frag = (TabFragmentView) viewPager.getAdapter()
                    .instantiateItem(viewPager, selectedTab);
            moreResultsView = frag.moreResultsView;
        }

        adapter.notifyDataSetChanged();

        if (moreResultsView != null) {
            int nextPage = 0;

            nextPage = scrollingData.get(selectedTab)
                    .getNextPage();

            if (nextPage ==
                    1) {
                frag.viewPagerFragment.removeFooterView(moreResultsView);
                frag.viewPagerFragment.addFooterView(moreResultsView);
                moreResultsView.setVisibility(View.VISIBLE);
            } else {
                frag.viewPagerFragment.removeFooterView(moreResultsView);
                moreResultsView.setVisibility(View.GONE);
            }
        }
        if ((catName.equalsIgnoreCase("photos") || catName.equalsIgnoreCase("Videos"))) {
            if (gridProgDialog != null && gridProgDialog.isShowing()) {
                gridProgDialog.dismiss();
                gridProgDialog = null;
            }
            isGridLoaded = false;
        }
        isLoaded = false;
    }

    private void getscroltemplate(final int selectedTab, String isSideBar) {
        scrollprogDialog = new ProgressDialog(this);
        scrollprogDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        scrollprogDialog.setMessage("Please Wait..");
        scrollprogDialog.setCancelable(false);
        scrollprogDialog.show();
        String catName = null;
        final String nonFeedUrl = bookMarkData.get(0).getBookMarkList().get(0).getListCatDetails().get
                (selectedTab).getNonfeedlink();
        try {

            catName = bookMarkData.get(0).getBookMarkList().get(0).getListCatDetails().get
                    (selectedTab).getParCatName();
            isSideBar = "0";

        } catch (Exception e) {
            if (scrollprogDialog != null && scrollprogDialog.isShowing()) {
                scrollprogDialog.dismiss();
            }
        }

        ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(catName,
                lowerLimitFlag, isSideBar, mDateCreated, mLevel, mLinkID);

        final String finalCatName = catName;
        mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {
            @Override
            public void success(ScrollingObj scrollingObj, Response response) {
                try {
                    //Code added by Supriya
                    //Check for template change
                    if (scrollingObj.getTemplateChanged() == 1) {
                        if (scrollingObj.getNewtempName().equalsIgnoreCase(Constants
                                .COMBINATION)) {
                            Intent intent = new Intent(ScrollingPageActivity.this,
                                    CombinationTemplate.class);
                            intent.putExtra(Constants.LEVEL, mLevel);
                            intent.putExtra(Constants.LINKID, mLinkID);
                            startActivity(intent);
                            finish();
                        } else if (scrollingObj.getNewtempName().equalsIgnoreCase(Constants
                                .NEWS_TILE)) {
                            Intent intent = new Intent(ScrollingPageActivity.this,
                                    TwoTileNewsTemplateActivity.class);
                            intent.putExtra(Constants.LEVEL, mLevel);
                            intent.putExtra(Constants.LINKID, mLinkID);
                            startActivity(intent);
                            finish();
                        } else if (scrollingObj.getNewtempName().equalsIgnoreCase(Constants
                                .SCROLLING)) {

                            setUpDrawer();
                            scrollingData = new TreeMap<>();
                            addTabViews();
                        } else {
                            GlobalConstants.isFromNewsTemplate = false;
                            SubMenuStack.clearSubMenuStack();
                            MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null,
                                    null, 0,
                                    null, mLinkId, null, null, null, null, "1", null,
                                    null, null,
                                    null, null, null, null);
                            SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
                            MenuAsyncTask menu = new MenuAsyncTask(ScrollingPageActivity.this);
                            menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "1",
                                    "0",
                                    "0", "None", "0", "0", "NewsTemplate");
                        }


                    } else {

                        if (scrollingData.size() == 0) {
                            loadValues(scrollingObj, selectedTab, finalCatName, nonFeedUrl);
                        } else {
                            if (scrollingData.get(selectedTab) == null) {
                                loadValues(scrollingObj, selectedTab, finalCatName, nonFeedUrl);
                            }
                        }
                    }
                    // Creates bottom button singleton
                    BottomButtons bottomButtons = new BottomButtons(CommonConstants.smNewsBtnFontColor,
                            CommonConstants.smNewsBtnColor, CommonConstants.mBtnNewsFontColor,
                            CommonConstants.mBtnNewsColor, mLinkID, mItemId,
                            CommonConstants.mNewsBkgrdColor, CommonConstants.mNewsBkgrdImage, CommonConstants.smNewsBkgrdColor,
                            CommonConstants.smNewsBkgrdImage,
                            CommonConstants.newsCityExpImgUlr, null, null,
                            null, null, null, null, CommonConstants.newsCityExpId,
                            mLevel, menuName,CommonConstants.mNewsFontColor ,CommonConstants.smNewsFontColor );

                    BottomButtonInfoSingleton.clearBottomButtonInfoSingleton();
                    BottomButtonInfoSingleton
                            .createBottomButtonsSingleton(bottomButtons);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (scrollprogDialog != null && scrollprogDialog.isShowing()) {
                        scrollprogDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    Toast.makeText(ScrollingPageActivity.this, error.getResponse().getReason(),
                            Toast
                                    .LENGTH_LONG).show();
                }
                if (scrollprogDialog != null && scrollprogDialog.isShowing()) {
                    scrollprogDialog.dismiss();
                }
            }
        });

    }

    //Code added by Supriya
    private void loadValues(ScrollingObj scrollingObj, final int selectedTab, String catName, String nonFeedUrl) {
        //Getting Navigation bar values and storing it to shared preference to access
        // from all the screens
        if (scrollingObj.getBkImgPath() != null) {
            Constants.setNavigationBarValues("bkImgPath", scrollingObj.getBkImgPath());
        }
        if (scrollingObj.getHomeImgPath() != null) {
            Constants.setNavigationBarValues("homeImgPath", scrollingObj.getHomeImgPath());
        }

        if (scrollingObj.getTitleTxtColor() != null) {
            Constants.setNavigationBarValues("titleTxtColor", scrollingObj.getTitleTxtColor());
        }

        setDrawerIcon(scrollingObj.getHumbergurImgPath());
        setTitleBackGrdColor(scrollingObj.getTitleBkGrdColor());

        if (scrollingObj.getWeatherURL() != null) {
            weatherUrl = scrollingObj.getWeatherURL();
        }

        String responseText = scrollingObj.getResponseText();
        mDateCreated = scrollingObj.getModifiedDate();
        if (!responseText.equalsIgnoreCase("Success")) {
            Toast.makeText(ScrollingPageActivity.this, responseText, Toast.LENGTH_LONG)
                    .show();
        }
        TabAdapter tabViewadapter;
        try {
            ArrayList<ListCatDetails> bookMarkList = null;
            ArrayList<SubCat> subCatList = null;

            bookMarkList = bookMarkData.get(0).getBookMarkList().get(0).getListCatDetails();

            scrollingData.put(selectedTab, scrollingObj);
            tabViewadapter = new TabAdapter
                    (getSupportFragmentManager(), tabLayout.getTabCount(),
                            mScreenHeight, mScreenWidth, bookMarkList, subCatList, template);
            viewPager.setAdapter(tabViewadapter);

        } catch (Exception e) {
            e.printStackTrace();
        }


        viewPager.setCurrentItem(selectedTab);

        TabFragmentView frag = null;

        if (!(catName.equalsIgnoreCase("photos") || catName.equalsIgnoreCase("Videos")) || nonFeedUrl == null) {
            frag = (TabFragmentView) viewPager.getAdapter()
                    .instantiateItem(viewPager, selectedTab);
        }

        try {
            assert frag != null;
            if (frag.viewPagerFragment != null) {
                View moreResultsView = frag.moreResultsView;
                int nextPage = 0;

                nextPage = scrollingData.get(selectedTab).getNextPage();

                if (moreResultsView != null) {
                    if (nextPage ==
                            1) {
                        if (frag.viewPagerFragment != null) {
                            frag.viewPagerFragment.addFooterView(moreResultsView);
                            moreResultsView.setVisibility(View.VISIBLE);
                        }
                    } else {
                        frag.viewPagerFragment.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);
                    }
                }
            } else {
                if (!(catName.equalsIgnoreCase("photos") || catName.equalsIgnoreCase("Videos"))) {
                    final TabFragmentView finalFrag = frag;
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            int nextPage = 0;

                            viewPager.setCurrentItem(selectedTab);
                            nextPage = scrollingData.get(selectedTab)
                                    .getNextPage();

                            View moreResultsView = finalFrag.moreResultsView;
                            if (moreResultsView != null) {
                                if (nextPage
                                        == 1) {
                                    finalFrag.viewPagerFragment.addFooterView(moreResultsView);
                                    moreResultsView.setVisibility(View.VISIBLE);
                                } else {
                                    finalFrag.viewPagerFragment.removeFooterView(moreResultsView);
                                    moreResultsView.setVisibility(View.GONE);
                                }
                            }
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String banner = scrollingData.get(selectedTab)
                .getBannerImg();
        loadBannerImage(banner);

    }

    private void loadBannerImage(String bannerImagePath) {
        if (bannerImagePath != null) {
            progressBar.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(bannerImagePath.replaceAll(" ", "%20")).into(bannerImage, new com.squareup.picasso
                    .Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    private void setTitleBackGrdColor(String titleBgColr) {
        if (titleBgColr != null) {
            Constants.setNavigationBarValues("titleBkGrdColor", titleBgColr);
            customParent.setBackgroundColor(Color.parseColor(titleBgColr));
        }
    }

    private void setWeatherUrl(String weatherUrlPath) {
        if(weatherUrlPath != null){
            weatherIcon.setVisibility(View.VISIBLE);
            weatherUrl = weatherUrlPath;
        }

    }


    private void setAdapter(TabLayout.Tab tab) {
        TabAdapter tabViewadapter;

        ArrayList<ListCatDetails> bookMarkList;
        ArrayList<SubCat> subCatList = null;

        bookMarkList = bookMarkData.get(0).getBookMarkList().get(0).getListCatDetails();

        tabViewadapter = new TabAdapter
                (getSupportFragmentManager(), bookMarkList.size(),
                        mScreenHeight, mScreenWidth, bookMarkList, subCatList, template);
        viewPager.setAdapter(tabViewadapter);
        viewPager.setCurrentItem(tab.getPosition());
    }


    private void addTabViews() {

        viewPager.removeAllViews();

        if (tabLayout.getTabCount() != 0) {
            tabLayout.removeAllTabs();
        }
        checkTemplateView();
        getBookMarkFields();


    }


    public void getPagination(int lowerLimit, int selectedTab, BaseAdapter adapter, String
            category) {
        if (category != null) {
            isGridLoaded = true;
        } else {
            isLoaded = true;
        }
        this.lowerLimitFlag = lowerLimit;
        startPagination(selectedTab, adapter);
    }

    private void checkTemplateView() {
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener
                (tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                String catName = bookMarkData.get(0).getBookMarkList().get(0)
                        .getListCatDetails().get
                                (tabPosition).getParCatName();
                String nonFeedLink = bookMarkData.get(0).getBookMarkList().get(0)
                        .getListCatDetails().get
                                (tabPosition).getNonfeedlink();
                if (nonFeedLink != null) {
                    isRefreshNeeded = false;
                } else {
                    isRefreshNeeded = true;
                }
                if (scrollingData.size() != 0) {
                    try {
                        if (scrollingData.get(tabPosition) != null) {
                            if (!(catName.equalsIgnoreCase("photos") || catName
                                    .equalsIgnoreCase("Videos")) && nonFeedLink == null) {
                                TabFragmentView frag = (TabFragmentView) viewPager.getAdapter()
                                        .instantiateItem(viewPager, tabPosition);
                                View moreResultsView = frag.moreResultsView;
                                if (moreResultsView != null) {
                                    if (scrollingData.get(tabPosition)
                                            .getNextPage() == 1) {
                                        frag.viewPagerFragment.removeFooterView
                                                (moreResultsView);
                                        frag.viewPagerFragment.addFooterView(moreResultsView);
                                        moreResultsView.setVisibility(View.VISIBLE);
                                    } else {
                                        frag.viewPagerFragment.removeFooterView
                                                (moreResultsView);
                                        moreResultsView.setVisibility(View.GONE);
                                    }
                                }
                            }

                            viewPager.setCurrentItem(tab.getPosition());

                        } else {
                            if (nonFeedLink == null) {
                                lowerLimitFlag = 0;
                                getscroltemplate(tabPosition, "0");
                            } else if (nonFeedLink != null) {
                                viewPager.setCurrentItem(tab.getPosition());
                            }
                        }


                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                        if (nonFeedLink == null) {
                            getscroltemplate(tabPosition, "0");
                        }
                    }
                } else if (nonFeedLink == null) {
                    getscroltemplate(tabPosition, "0");
                }
                if (scrollingData.size() == 0 && nonFeedLink != null) {
                    setAdapter(tab);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getBookMarkFields() {

        progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();
        BookMarkModel bookMarkRequest = UrlRequestParams.getBookMarkDetails("1", mLinkID, mLevel);
        mRestClient.getBookMarkDetails(bookMarkRequest, new Callback<BookMarkObj>() {
                    @Override
                    public void success(BookMarkObj bookMarkObj, Response response) {
                        try {
                            String responseText = bookMarkObj.getResponseText();
                            //load title bar
                            setDrawerIcon(bookMarkObj.getHamburgerImg());
                            loadBannerImage(bookMarkObj.getBannerImg());
                            setTitleBackGrdColor(bookMarkObj.getTitleBkGrdColor());
                            setWeatherUrl(bookMarkObj.getWeatherURL());
                            if (responseText.equalsIgnoreCase("Success"))
                            {
                                bookMarkData.add(bookMarkObj);

                                for (BookMarkList bookMarkList : bookMarkObj.getBookMarkList()) {
                                    int i = 0;
                                    for (ListCatDetails listDetails : bookMarkList
                                            .getListCatDetails
                                                    ()) {
                                        View textView = LayoutInflater.from(mContext).inflate(R
                                                .layout.custom_tab, null);
                                        TextView tabText = (TextView) textView.findViewById(R.id
                                                .custom_text);
                                        String catName = listDetails
                                                .getParCatName();
                                        tabText.setText(catName);
                                        tabLayout.addTab(tabLayout.newTab());
                                        TabLayout.Tab tab = tabLayout.getTabAt(i);
                                        assert tab != null;
                                        tab.setCustomView(textView);
                                        i++;
                                    }
                                }
                            } else {
                                Toast.makeText(ScrollingPageActivity.this, responseText, Toast
                                        .LENGTH_LONG).show();
                            }

                        } catch (Exception ignored) {

                        }
                        if (progDialog != null && progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        if (retrofitError.getResponse() != null) {
                            Toast.makeText(ScrollingPageActivity.this, retrofitError.getResponse()
                                    .getReason(), Toast
                                    .LENGTH_LONG).show();
                        }
                        if (progDialog != null && progDialog.isShowing()) {
                            progDialog.dismiss();
                        }
                    }


                }
        );
    }


    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.drawer_icon:
                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.searchView:
                startActivity(new Intent(ScrollingPageActivity.this, ScrollingPageActivity.class));
                break;
            case R.id.weather_icon:
                openWeatherUrl();
                break;
        }
    }

    private void openWeatherUrl() {
        if (weatherUrl != null) {
            Intent browseIntent = new Intent(ScrollingPageActivity.this,
                    ScanseeBrowserActivity.class);
            browseIntent.putExtra(CommonConstants.URL,
                    weatherUrl);
            startActivity(browseIntent);
        }
    }


    private void bindView() {

        subPageHeader = getLayoutInflater().inflate(
                R.layout.tab_subpage, sideMenuList, true);
        sideMenuList = (ListView) findViewById(R.id.side_menu_list);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerIcon = (ImageView) findViewById(R.id.drawer_icon);
        viewPager = (ViewPager) findViewById(R.id.pager);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        subPageTabLayout = (TabLayout) findViewById(R.id.subpage_tab_layout);
        tabParent = (RelativeLayout) findViewById(R.id.tab_parent);
        screenSummary = (LinearLayout) findViewById(R.id.screen_summary);
        bannerImage = (ImageView) findViewById(R.id.banner_image);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        customParent = (RelativeLayout) findViewById(R.id.custom_parent);
        HomeView = (ImageView) findViewById(R.id.HomeView);
        weatherIcon = (ImageView) findViewById(R.id.weather_icon);
        backImage = ((ImageView) findViewById(R.id.back_btn));
        //Loading back image
        if (Constants.getNavigationBarValues("bkImgPath") != null) {
            Picasso.with(this).load(Constants.getNavigationBarValues("bkImgPath").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (backImage);
        } else {
            backImage.setBackgroundResource(R.drawable.ic_action_back);
        }

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackClick();
            }
        });
        if (!mLevel.equals("1")) {
            backImage.setVisibility(View.VISIBLE);
        }
        newsTitle = (TextView) findViewById(R.id.titleTextView);
        ImageView customMoreView = (ImageView) findViewById(R.id.more_view);
        customMoreView.setVisibility(View.GONE);
        TextView CustomMore = (TextView) findViewById(R.id.more);
        CustomMore.setVisibility(View.GONE);
        ImageView customSearch = (ImageView) findViewById(R.id.searchView);
        customSearch.setVisibility(View.GONE);

        weatherIcon.setVisibility(View.VISIBLE);

        contentSearch = (ImageView) findViewById(R.id.content_search);
        contentSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScrollingPageActivity.this, NewsSearchActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        onBackClick();
    }

    private void onBackClick() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
        }
        super.onBackPressed();

    }


    public ViewPager getViewPager() {
        return viewPager;
    }

    public RelativeLayout getTabView() {
        return tabParent;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    private void setDrawerIcon(String imagePath) {
        if (imagePath != null) {
            Constants.setNavigationBarValues("hamburgerImg", imagePath);
        }
        if (Constants.getNavigationBarValues("hamburgerImg") != null) {
            Picasso.with(activity).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (drawerIcon);
        }
    }
}

