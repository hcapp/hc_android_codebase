package com.scansee.newsfirst;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.hubregion.R;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 5/25/2016.
 */
public class GridChildView extends CustomTitleBar {
    GridView grid;
    String category;
    private int mScreenHeight;
    private int mScreenWidth;
    boolean isItemClick = true;
    private String desc;
    private String catColor,catTxtColor;
    private boolean isScrolling;

    ArrayList<String> link = new ArrayList<>();
    ImageView backBtn;
    private int itemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_child_view);
        getIntentValue();
        bindView();
        setClickListener();
        // Title of the screen
        title.setText(category);
//        rightImage.setVisibility(View.GONE);
        //setting navigation bar background color
        if(catColor != null) {
            ((RelativeLayout) findViewById(R.id.title_layout)).setBackgroundColor(Color.parseColor(catColor));
        }
        if(catTxtColor != null) {
            title.setTextColor(Color.parseColor(catTxtColor));
        }
        if(isScrolling)
        {
            grid.setBackgroundColor(Color.WHITE);
        }


        ViewTreeObserver observer = grid.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                mScreenHeight = grid.getMeasuredHeight();
                mScreenWidth = grid.getMeasuredWidth();
                ViewTreeObserver obs = grid.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    obs.removeGlobalOnLayoutListener(this);
                }
                subPageViewAdapter subPagetAdapter = new subPageViewAdapter
                        (GridChildView.this, link, mScreenHeight, mScreenWidth, category, grid, isItemClick);
                grid.setAdapter(subPagetAdapter);
            }

        });

    }

    private void bindView() {
        grid = (GridView) findViewById(R.id.grid_view);
    }

    private void setClickListener() {

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent gridChildView = new Intent(GridChildView.this, ViewDetailsScreen.class);
                gridChildView.putStringArrayListExtra("link", link);
                gridChildView.putExtra("desc", desc);
                gridChildView.putExtra("itemId", itemId);
                gridChildView.putExtra("category", category);
                gridChildView.putExtra("catColor", catColor);
                gridChildView.putExtra("catTxtColor", catTxtColor);
                gridChildView.putExtra("viewPosition", position);
                gridChildView.putExtra("scrollingScreen", isScrolling);
                startActivity(gridChildView);

            }
        });
    }

    private void getIntentValue() {
        if (getIntent().hasExtra("link")) {
            link =  getIntent().getStringArrayListExtra("link");
        }
        if (getIntent().hasExtra("itemId")) {
            itemId =  getIntent().getExtras().getInt("itemId");
        }
        if (getIntent().hasExtra("desc")) {
            desc =  getIntent().getExtras().getString("desc");
        }
        if (getIntent().hasExtra("category")) {
            category =  getIntent().getExtras().getString("category");
        }
        if (getIntent().hasExtra("catColor")) {
            catColor =  getIntent().getExtras().getString("catColor");
        }
        if (getIntent().hasExtra("catTxtColor")) {
            catTxtColor =  getIntent().getExtras().getString("catTxtColor");
        }
        if (getIntent().hasExtra("scrollingScreen")) {
            isScrolling =  getIntent().getExtras().getBoolean("scrollingScreen");
        }
    }


}
