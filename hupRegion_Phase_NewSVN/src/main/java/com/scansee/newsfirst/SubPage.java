package com.scansee.newsfirst;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.ScrollingPageModel;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


@SuppressWarnings("DefaultFileTemplate")
public class SubPage extends Activity {
    private Context mContext;
    private LinkedList<ScrollingObj> sideMenuItem;
    private int lowerLimit = 0;
    private RestClient mRestClient;
    private ListView listView;
    private boolean isLoaded = false;
    private String sideMenuCat;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View moreResultsView;
    private String isSidebar;
    private String templateType;
    private NewsAdapter newsAdapter;
    private String linkId, level;

    public void sideMenu(Context mContext, String sideMenuCat, ListView sideMenuList,
                         SwipeRefreshLayout swipeRefreshLayout, View moreResultsView, String
                                 isSidebar, String templateType, String linkId, String level) {

        this.mContext = mContext;
        this.sideMenuCat = sideMenuCat;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.listView = sideMenuList;
        this.moreResultsView = moreResultsView;
        this.templateType = templateType;
        this.isSidebar = isSidebar;
        setClickListener();
        mRestClient = RestClient.getInstance();
        listView.setAdapter(null);
        sideMenuItem = new LinkedList<>();
        this.level = level;
        this.linkId = linkId;
        getscroltemplate();

    }

    private void setClickListener() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startPullToRequest();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent detailsPage = new Intent(mContext, NewsDetailsActivity.class);
                detailsPage.putExtra("itemId", sideMenuItem.get(0).getItems().get(i).getItemID());
                mContext.startActivity(detailsPage);
            }
        });
    }

    private void startPullToRequest() {
        ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(sideMenuCat, 0,
                isSidebar, null, level, linkId);
        mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {
            @Override
            public void success(ScrollingObj scrollingObj, Response response) {
                try {
                    String responseText = scrollingObj.getResponseText();
                    if (!responseText.equalsIgnoreCase("Success")) {
                        Toast.makeText(mContext, responseText, Toast.LENGTH_LONG)
                                .show();
                    }
                    sideMenuItem = new LinkedList<>();
                    sideMenuItem.add(scrollingObj);
                    swipeRefreshLayout.setRefreshing(false);

                    if (sideMenuItem.get(0).getNextPage() == 1) {
                        listView.removeFooterView(moreResultsView);
                        listView.addFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.VISIBLE);
                    } else {
                        listView.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);
                    }
                    if (sideMenuItem.get(0).getItems() != null) {
                        if (sideMenuItem.get(0).getItems().size() != 0) {
                            String catTxtColor = sideMenuItem.get(0).getItems().get(0)
                                    .getCatTxtColor();
                            String catColour = sideMenuItem.get(0).getItems().get(0).getCatColor();
                            if (catTxtColor != null) {
                                ((SubpageActivty) mContext).newsTitle.setTextColor(Color
                                        .parseColor(catTxtColor));

                            }
                            if (catColour != null) {
                                ((SubpageActivty) mContext).customParent.setBackgroundColor
                                        (Color.parseColor(catColour));

                            }
                        }
                    }
                    isLoaded = false;
                } catch (Exception ignored) {

                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    Toast.makeText(mContext, error.getResponse().getReason(), Toast
                            .LENGTH_LONG).show();
                }
            }
        });
    }


    private class NewsAdapter extends BaseAdapter {
        private final Context mContext;
        private final String subpageName;


        public NewsAdapter(Context mContext, String subpageName) {
            this.mContext = mContext;
            this.subpageName = subpageName;
        }

        @Override
        public int getCount() {
            try {
                if (subpageName != null && subpageName.equalsIgnoreCase("Big Banner")) {
                    return sideMenuItem.get(0).getItems().size() - 1;
                } else {
                    return sideMenuItem.get(0).getItems().size();
                }
            } catch (Exception e) {
                return 0;
            }
        }

        @Override
        public Object getItem(int i) {
            return sideMenuItem.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;
            SubPageHolder subPage = null;

            int lowerLimit = sideMenuItem.get(0).getLowerLimitFlag();
            int maxCount = sideMenuItem.get(0).getMaxCnt();
            int count;
            if (subpageName != null && subpageName.equalsIgnoreCase("Big Banner")) {
                count = position + 2;
            } else {
                count = position + 1;
            }
            if (lowerLimit < maxCount) {
                if ((count) == lowerLimit) {
                    if (!isLoaded) {
                        getPagination(lowerLimit);
                    }
                }
            }
            if (convertView == null) {
                LayoutInflater inflaInflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


                if (templateType != null && templateType.equalsIgnoreCase("Scrolling News Template")) {
                    String imgPosition = sideMenuItem.get(0).getItems
                            ().get(position).getImgPosition();
                    if (imgPosition != null) {
                        if (imgPosition.equalsIgnoreCase("Left")) {
                            vi = inflaInflater.inflate(
                                    R.layout.scrolling_left_adapter, parent, false);
                        } else {
                            vi = inflaInflater.inflate(
                                    R.layout.scrolling_adapter, parent, false);
                        }
                    } else {
                        vi = inflaInflater.inflate(
                                R.layout.scrolling_left_adapter, parent, false);
                    }
                    holder = new ViewHolder();
                    holder.subStoryTitle = (TextView) vi.findViewById(R.id.sub_story_title);
                    holder.subStoryAuthor = (TextView) vi.findViewById(R.id.sub_story_author);
                    holder.subStoryChild=(LinearLayout) vi.findViewById(R.id.sub_story_child);
                    holder.subStorySummary = (TextView) vi.findViewById(R.id.sub_story_summary);
                    holder.logo = (ImageView) vi.findViewById(R.id.sub_story_logo);
                    holder.subStoryTime=(TextView) vi.findViewById(R.id.sub_story_time);
                    holder.progressBar = (ProgressBar) vi.findViewById(R.id.progress);
                    vi.setTag(holder);
                } else {
                    vi = inflaInflater.inflate(
                            R.layout.tab_subpage_adapter, parent, false);
                    subPage = new SubPageHolder();
                    subPage.author = (TextView) vi.findViewById(R.id.sub_story_author);
                    // changes to include date in subpages HC-569 Sprint 3 task.
                    subPage.subStoryTime=(TextView) vi.findViewById(R.id.sub_story_time);
                    subPage.subStoryChild=(LinearLayout) vi.findViewById(R.id.sub_story_child);
                    subPage.shortDescription = (TextView) vi.findViewById(R.id.short_description);

                    subPage.title = (TextView) vi.findViewById(R.id.sub_story_title);
                    subPage.bigBanner = (ImageView) vi.findViewById(R.id.subpage_image);
                    subPage.progressBar = (ProgressBar) vi.findViewById(R.id.progress);
                    vi.setTag(subPage);
                }
            } else {
                if (templateType != null && templateType.equalsIgnoreCase("Scrolling News Template")) {
                    holder = (ViewHolder) vi.getTag();
                } else {
                    subPage = (SubPageHolder) vi.getTag();
                }
            }

            if (subpageName != null && subpageName.equalsIgnoreCase("Big Banner")) {
                position = position + 1;
            }

            if (templateType != null && templateType.equalsIgnoreCase("Scrolling News Template")) {
                String title = sideMenuItem.get(0).getItems
                        ().get(position).getTitle();
                if (title != null && !title.equalsIgnoreCase("")) {
                    assert holder != null;
                    holder.subStoryTitle.setText(title);
                }



                String author = sideMenuItem.get(0).getItems
                        ().get(position).getAuthor();
                if (author != null) {
                    if (author.length() > 15) {
                        author = author.substring(0, 15).concat("...");
                    }
                }
                if (author != null && !author.equalsIgnoreCase("")) {
                    assert holder != null;
                    holder.subStoryAuthor.setVisibility(View.VISIBLE);
                    holder.subStoryChild.setVisibility(View.VISIBLE);
                    holder.subStoryAuthor.setText(author.concat("  "));
                } else {
                    holder.subStoryAuthor.setVisibility(View.GONE);
                }

                String date = sideMenuItem.get(0).getItems
                        ().get(position).getDate();
                String time = sideMenuItem.get(0).getItems
                        ().get(position).getTime();
                if(date != null) {
                    holder.subStoryChild.setVisibility(View.VISIBLE);
                    if(time != null) {
                        time = date.concat(" " + time);
                    }
                    else
                    {
                        time = date;
                    }
                }
                if (time != null && !time.equalsIgnoreCase("")) {
                    assert holder != null;
                    holder.subStoryTime.setText(time);
                }

                if((author == null) && (time == null) && (date == null))
                {
                    holder.subStoryChild.setVisibility(View.GONE);
                }


                String desc = sideMenuItem.get(0).getItems
                        ().get(position).getsDesc();
                if (desc != null && !desc.equalsIgnoreCase("")) {
                    assert holder != null;
                    holder.subStorySummary.setText(desc);
                }
                String image = sideMenuItem.get(0)
                        .getItems
                                ().get(position).getImage();
                if (image != null && !image.equalsIgnoreCase("")) {
                    image = image.replaceAll(" ", "%20");
                    assert holder != null;
                    new CommonMethods().loadImage(mContext, holder.progressBar, image, holder
                            .logo);
                }
            } else {
                String author = sideMenuItem.get
                        (0).getItems().get(position).getAuthor();

                if (author != null) {
                    if (author.length() > 15) {
                        author = author.substring(0, 15).concat("...");
                    }
                }
                if (author != null && !author.equalsIgnoreCase("")) {
                    assert subPage != null;



                    subPage.author.setVisibility(View.VISIBLE);
                    subPage.subStoryChild.setVisibility(View.VISIBLE);
                    subPage.author.setText(author.concat("  "));
                } else {
                    subPage.author.setVisibility(View.GONE);
                }







                // changes to include date in subpages HC-569 Sprint 3 task.


                String time = sideMenuItem.get(0).getItems
                        ().get(position).getTime();
                String date = sideMenuItem.get(0).getItems
                        ().get(position).getDate();
                if(date != null) {
                    subPage.subStoryChild.setVisibility(View.VISIBLE);
                    if(time != null) {
                        time = date.concat(" " + time);
                    }
                    else
                    {
                        time = date;
                    }
                }
                if (time != null && !time.equalsIgnoreCase("")) {
                    assert holder != null;
                    subPage.subStoryTime.setText(time);
                }

                if((author == null) && (time == null) && (date == null))
                {
                    subPage.subStoryChild.setVisibility(View.GONE);
                }


                String desc = sideMenuItem.get(0).getItems
                        ().get(position).getsDesc();
                if (desc != null && !desc.equalsIgnoreCase("")) {
                    assert holder != null;
                    subPage.shortDescription.setText(desc);
                }
                String title = sideMenuItem.get(
                        0).getItems().get(position).getTitle();
                if (title != null && !title.equalsIgnoreCase("")) {
                    assert subPage != null;
                    subPage.title.setText(title);
                }
                String image = sideMenuItem.get(0)
                        .getItems().get(position).getImage();
                if (image != null && !image.equalsIgnoreCase("")) {
                    image = image.replaceAll(" ", "%20");
                    assert subPage != null;
                    new CommonMethods().loadImage(mContext, subPage.progressBar, image, subPage
                            .bigBanner);
                }
            }
            return vi;
        }

        private class ViewHolder {
            TextView subStoryTitle;
            TextView subStoryAuthor;
            TextView subStorySummary;
            ImageView logo;
            TextView subStoryTime;
            LinearLayout subStoryChild;
            ProgressBar progressBar;
        }

        private class SubPageHolder {
            TextView author;
            LinearLayout subStoryChild;
            TextView shortDescription;
            TextView subStoryTime;
            TextView title;
            ImageView bigBanner;
            ProgressBar progressBar;

        }
    }

    private void getPagination(int lowerLimit) {
        isLoaded = true;
        this.lowerLimit = lowerLimit;
        startPagination();
    }


    private void startPagination() {

        ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(sideMenuCat,
                lowerLimit, isSidebar, null, level, linkId);
        mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {

            @Override
            public void success(ScrollingObj scrollingObj, Response response) {
                String responseText = scrollingObj.getResponseText();
                if (!responseText.equalsIgnoreCase("Success")) {
                    Toast.makeText(mContext, responseText, Toast.LENGTH_LONG)
                            .show();
                }
                try {
                    sideMenuItem.get(0).setItems(scrollingObj.getItems());
                    sideMenuItem.get(0).setMaxCnt(scrollingObj.getMaxCnt());
                    sideMenuItem.get(0).setLowerLimitFlag(scrollingObj
                            .getLowerLimitFlag());
                    sideMenuItem.get(0).setNextPage(scrollingObj.getNextPage());
                    sideMenuItem.get(0).setSubPageName(scrollingObj.getSubPageName());
                    if (sideMenuItem.get(0).getNextPage() == 1) {
                        listView.removeFooterView(moreResultsView);
                        listView.addFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.VISIBLE);
                    } else {
                        listView.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);
                    }
                    isLoaded = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    Toast.makeText(mContext, error.getResponse().getReason(), Toast
                            .LENGTH_LONG).show();
                }
            }
        });

    }


    private void getscroltemplate() {
        final ProgressDialog progDialog = new ProgressDialog(mContext);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();


        ScrollingPageModel scrollingModelObj = UrlRequestParams.getscroltemplate(sideMenuCat,
                lowerLimit, isSidebar, null, level, linkId);
        mRestClient.getscroltemplate(scrollingModelObj, new Callback<ScrollingObj>() {

            @Override
            public void success(ScrollingObj scrollingObj, Response response) {
                try {
                    String responseText = scrollingObj.getResponseText();
                    if (!responseText.equalsIgnoreCase("Success")) {
                        Toast.makeText(mContext, responseText, Toast.LENGTH_LONG)
                                .show();
                    }
                    sideMenuItem.add(scrollingObj);


                    String subpageName = sideMenuItem.get(0).getSubPageName();
                    //check with big banner or list
                    if (subpageName != null && subpageName.equalsIgnoreCase("Big Banner")) {
                        addHeader();
                    } else {
                        if (listView.getHeaderViewsCount() != 0) {
                            listView.removeHeaderView(((SubpageActivty) mContext)
                                    .subPageHeader);
                        }
                    }
                    newsAdapter = new NewsAdapter(mContext, subpageName);
                    listView.setAdapter(newsAdapter);
                    newsAdapter.notifyDataSetChanged();
                    if (sideMenuItem.get(0).getNextPage() == 1) {
                        listView.removeFooterView(moreResultsView);
                        listView.addFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.VISIBLE);
                    } else {
                        listView.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);
                    }
                    if (sideMenuItem.get(0).getItems() != null) {
                        if (sideMenuItem.get(0).getItems().size() != 0) {
                            String catTxtColor = sideMenuItem.get(0).getItems().get(0)
                                    .getCatTxtColor();
                            String catColour = sideMenuItem.get(0).getItems().get(0).getCatColor();
                            if (catTxtColor != null) {
                                ((SubpageActivty) mContext).newsTitle.setTextColor(Color
                                        .parseColor(catTxtColor));

                            }
                            if (catColour != null) {
                                ((SubpageActivty) mContext).customParent.setBackgroundColor
                                        (Color.parseColor(catColour));

                            }
                        }
                    }
                    if (scrollingObj.getHumbergurImgPath() != null) {
                        Constants.setNavigationBarValues("hamburgerImg", scrollingObj.getHumbergurImgPath());
                    }
                    if (scrollingObj.getBackButtonColor() != null) {

                        String color = scrollingObj.getBackButtonColor(); //The color u want
                        ((SubpageActivty)mContext).backImage.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
                    }
                    displayHamburger(scrollingObj);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (progDialog != null && progDialog.isShowing()) {
                    progDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progDialog != null && progDialog.isShowing()) {
                    progDialog.dismiss();
                }
                if (error.getResponse() != null) {
                    Toast.makeText(mContext, error.getResponse().getReason(), Toast
                            .LENGTH_LONG).show();
                }
            }
        });

    }

    private void displayHamburger(ScrollingObj scrollingObj) {
        if (scrollingObj.getResponseCode().equals("10000")) {
            //Check for template change


            if (Constants.getNavigationBarValues("hamburgerImg") != null) {
                Picasso.with(mContext).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                        "%20")).placeholder(R.drawable.loading_button).into
                        (((SubpageActivty)mContext).drawerIcon);
            }
            if (scrollingObj.getBackButtonColor() != null) {

                String color = scrollingObj.getBackButtonColor(); //The color u want
                ((SubpageActivty)mContext).backImage.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
            }


        } else if (scrollingObj.getResponseCode().equals("10005")) {
            if (scrollingObj.getHumbergurImgPath() != null) {
                Constants.setNavigationBarValues("hamburgerImg", scrollingObj.getHumbergurImgPath());
            }
            if (Constants.getNavigationBarValues("hamburgerImg") != null) {
                Picasso.with(mContext).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                        "%20")).placeholder(R.drawable.loading_button).into
                        (((SubpageActivty)mContext).drawerIcon);
            }
            if (scrollingObj.getBackButtonColor() != null) {

                String color = scrollingObj.getBackButtonColor(); //The color u want
                ((SubpageActivty)mContext).backImage.setColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY);
            }

        }
    }

    private void addHeader() {
        if (listView.getHeaderViewsCount() != 0) {
            listView.removeHeaderView(((SubpageActivty) mContext).subPageHeader);
        }
        ImageView bigBanner = (ImageView) ((SubpageActivty) mContext).subPageHeader
                .findViewById(R.id.big_banner);
        TextView author = (TextView) ((SubpageActivty) mContext).subPageHeader.findViewById
                (R.id.author);
        RelativeLayout detailsParent = (RelativeLayout) ((SubpageActivty) mContext).subPageHeader
                .findViewById(R.id.descriptionLayout);
        TextView title = (TextView) ((SubpageActivty) mContext).subPageHeader.findViewById
                (R.id.title);
        RelativeLayout bannerHolder = (RelativeLayout) ((SubpageActivty) mContext)
                .subPageHeader.findViewById(R.id.banner_holder);
        final ProgressBar progressBar = (ProgressBar) ((SubpageActivty) mContext)
                .subPageHeader.findViewById(R.id.progress);
        if (sideMenuItem.get(0) != null) {
            if (sideMenuItem.get(0)
                    .getItems().size() != 0) {
                detailsParent.setVisibility(View.VISIBLE);
                String subAuthor = sideMenuItem.get
                        (0).getItems().get(0).getAuthor();
                if (subAuthor != null && !subAuthor.equalsIgnoreCase("")) {
                    author.setVisibility(View.VISIBLE);
                    author.setText(subAuthor);
                } else {
                    author.setVisibility(View.GONE);
                }
                String subTitle = sideMenuItem.get(
                        0).getItems().get(0).getTitle();
                if (subTitle != null && !subTitle.equalsIgnoreCase("")) {
                    title.setText(subTitle);
                }
                String image = sideMenuItem.get(0)
                        .getItems().get(0).getImage();
                if (image != null && !image.equalsIgnoreCase("")) {
                    bigBanner.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    image = image.replaceAll(" ", "%20");
                    Picasso.with(mContext).load(image).into(bigBanner, new com.squareup.picasso
                            .Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    bannerHolder.setVisibility(View.GONE);
                }
                listView.addHeaderView(((SubpageActivty) mContext).subPageHeader);
            }
        }

    }

}