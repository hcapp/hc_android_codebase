package com.scansee.newsfirst;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by subramanya.v on 5/25/2016.
 */
public class ScrollingMainObj implements Serializable{
    LinkedList<ScrollingObj> scrollingData;
    public LinkedList<ScrollingObj> getScrollingData() {
        return scrollingData;
    }

    public void setScrollingData(LinkedList<ScrollingObj> scrollingData) {
        this.scrollingData = scrollingData;
    }


}
