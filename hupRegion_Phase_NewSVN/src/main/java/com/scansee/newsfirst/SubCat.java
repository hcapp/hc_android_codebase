package com.scansee.newsfirst;

/**
 * Created by subramanya.v on 6/22/2016.
 */
public class SubCat {
    private int subCatId;
    private String subCatName;
    private int isSubCategory;

    public int getIsSubCategory() {
        return isSubCategory;
    }

    public void setIsSubCategory(int isSubCategory) {
        this.isSubCategory = isSubCategory;
    }

    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(int subCatId) {
        this.subCatId = subCatId;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }


}
