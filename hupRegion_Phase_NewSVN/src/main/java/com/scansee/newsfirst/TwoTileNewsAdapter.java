package com.scansee.newsfirst;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.scansee.newsfirst.model.TileTemplateCategoryModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sharanamma on 23-06-2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class TwoTileNewsAdapter extends BaseAdapter {
    private final int paddingBottom;
    private final Activity activity;
    private final int dpHeight;
    private final int dpWidth;
    private ArrayList<TileTemplateCategoryModel> tileTempCategoryList = new ArrayList<>();

    public TwoTileNewsAdapter(Activity activity, ArrayList<TileTemplateCategoryModel> tileTempCategoryList, int width, int height, int paddingBottom) {
        this.activity = activity;
        this.tileTempCategoryList = tileTempCategoryList;
        this.dpWidth = width;
        this.dpHeight = height;
        this.paddingBottom = paddingBottom;
    }

    @Override
    public int getCount() {
        return tileTempCategoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return tileTempCategoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(
                    R.layout.tile_template_gridview_item, parent, false);


            holder = new ViewHolder();
            holder.catNameTextView = (TextView) view.findViewById(R.id.catNameTextView);
            holder.categoryImageView = (ImageView) view.findViewById(R.id.categoryImageView);
            holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);

            holder.imageHolder = (RelativeLayout) view.findViewById(R.id.image_holder);
            int parentHeight = (dpHeight - paddingBottom) / 3;
            int imageHeight = (parentHeight * 80)/100;
            LinearLayout.LayoutParams rowView = new LinearLayout.LayoutParams((dpWidth / 2),imageHeight );
            holder.imageHolder .setLayoutParams(rowView);

            int textHeight = parentHeight - imageHeight;
            LinearLayout.LayoutParams txtParam = new LinearLayout.LayoutParams((dpWidth / 2), textHeight);
            holder.catNameTextView .setLayoutParams(txtParam);
            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }


        holder.catNameTextView.setText(tileTempCategoryList.get(position).getCatName());
        if (tileTempCategoryList.get(position).getCatImgPath() != null) {
            holder.progressBar.setVisibility(View.VISIBLE);
            Picasso.with(activity).load(tileTempCategoryList.get(position).getCatImgPath()).into(holder.categoryImageView, new Callback() {
                @Override
                public void onSuccess() {
                    holder.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    holder.progressBar.setVisibility(View.GONE);
                }
            });
        }
        return view;
    }

    private class ViewHolder {
        TextView catNameTextView;
        ImageView categoryImageView;
        ProgressBar progressBar;
        RelativeLayout imageHolder;
    }

}
