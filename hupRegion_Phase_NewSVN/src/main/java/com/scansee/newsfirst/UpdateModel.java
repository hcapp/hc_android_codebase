package com.scansee.newsfirst;

/**
 * Created by subramanya.v on 6/6/2016.
 */
public class UpdateModel
{
	private final String hubCitiId;
	private final String userId;
	private final String bkMarkOrder;
	private final String navigOrder;


	public UpdateModel(String hubCitiId, String userId, String bkMarkOrder, String navigOrder)
	{
		this.hubCitiId = hubCitiId;
		this.userId = userId;
		this.bkMarkOrder = bkMarkOrder;
		this.navigOrder = navigOrder;
	}

}
