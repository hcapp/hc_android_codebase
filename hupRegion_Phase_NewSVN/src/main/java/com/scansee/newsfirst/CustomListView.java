package com.scansee.newsfirst;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by supriya.m on 7/1/2016.
 */
public class CustomListView extends ListView
{

	public CustomListView(Context context)
	{
		super(context);
	}

	public CustomListView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public CustomListView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}


	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		try {
			super.dispatchDraw(canvas);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
