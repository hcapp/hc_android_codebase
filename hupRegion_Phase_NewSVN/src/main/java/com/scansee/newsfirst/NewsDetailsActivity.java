package com.scansee.newsfirst;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.ShareInformation;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.model.NewsDetailsItemList;
import com.scansee.newsfirst.model.NewsDetailsModel;
import com.squareup.picasso.Picasso;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by supriya.m on 5/24/2016.
 */
public class NewsDetailsActivity extends CustomTitleBar {

    private static final String TAG = NewsDetailsActivity.class.getSimpleName();
    private TextView titleTextView, dateTextView,authorTextView;
    private ImageView catImageView;
    private WebView webview;
    private VideoView video_player_view;
    private ProgressDialog progDialog;
    private int mItemId;
    private ShareInformation shareInfo;
    private Activity activity;
    private boolean isShareTaskCalled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = NewsDetailsActivity.this;
        setContentView(R.layout.news_detail_screen);
        title.setText(R.string.details);
        divider.setVisibility(View.GONE);


        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mItemId = getIntent().getIntExtra("itemId", 0);
        shareInfo = new ShareInformation(
                activity, "", "",
                String.valueOf(mItemId), "NewsDetails");
        bindViews();
        getNewsDetails();
        setClickListener();
    }

    private void setClickListener() {
        leftTitleImage
                .setBackgroundResource(R.drawable.share_btn_down);
        leftTitleImage
                .setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!Constants.GuestLoginId
                                .equals(UrlRequestParams.getUid()
                                        .trim())) {
                            shareInfo.shareTask(isShareTaskCalled);
                            isShareTaskCalled = true;
                        } else {
                            Constants
                                    .SignUpAlert(activity);
                            isShareTaskCalled = false;
                        }
                    }
                });

    }

    private void bindViews() {
        titleTextView = (TextView) findViewById(R.id.news_detail_title);
        dateTextView = (TextView) findViewById(R.id.news_detail_date);
        authorTextView = (TextView)findViewById(R.id.news_detail_author);
        catImageView = (ImageView) findViewById(R.id.news_detail_image);
        webview = (WebView) findViewById(R.id.webviiew);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setVerticalScrollBarEnabled(false);
        webview.setHorizontalScrollBarEnabled(false);
        video_player_view = (VideoView) findViewById(R.id.video_player_view);

    }


    private void populateData(NewsDetailsItemList newsItemList) {
        titleTextView.setText(newsItemList.getTitle());
        if (newsItemList.getDate() != null && !newsItemList.getDate().isEmpty())
        {
            dateTextView.setText("Published Date : "+newsItemList.getDate() +" ");
        }
        if (newsItemList.getTime() != null && !newsItemList.getTime().isEmpty())
        {
            dateTextView.setText(dateTextView.getText()+" "+newsItemList.getTime());
        }
        if (newsItemList.getAuthor() != null && !newsItemList.getAuthor().isEmpty())
        {
            authorTextView.setText("Author : "+newsItemList.getAuthor());
        }
        if (newsItemList.getlDesc() != null && !newsItemList.getlDesc().isEmpty()) {
            webview.setVisibility(View.VISIBLE);
            webview.loadDataWithBaseURL("", newsItemList.getlDesc(), "text/html", "UTF-8", "");
        }

        if (newsItemList.getImage() != null && !newsItemList.getImage().isEmpty()) {
            catImageView.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(newsItemList.getImage())
                    .into(catImageView);
        }
        if (newsItemList.getVideoLink() != null && !newsItemList.getVideoLink().isEmpty()) {
            (findViewById(R.id.video_frame)).setVisibility(View.VISIBLE);
            MediaController media_Controller = new MediaController(this);
            media_Controller.setAnchorView(video_player_view);
            video_player_view.setMediaController(media_Controller);
            video_player_view.requestFocus();
           //video_player_view.setVideoPath("http://brightcove.vo.llnwd.net/v1/uds/pd/2322069287001/201606/3717/2322069287001_4930826523001_4930813160001.mp4");
            video_player_view.setVideoPath(newsItemList.getVideoLink());
            video_player_view.start();
        }

    }

    private void getNewsDetails() {
        showLoading();
        RestClient.getInstance().getNewsDetail(mItemId, new Callback<NewsDetailsModel>() {
            @Override
            public void success(NewsDetailsModel newsItemList, Response response) {
                Log.d(TAG, " NewsDetails : success");
                hideLoading();
                if (newsItemList.getItemList() != null && newsItemList.getItemList().size() > 0)
                    populateData(newsItemList.getItemList().get(0));

            }

            @Override
            public void failure(RetrofitError retrofitError) {
                if (retrofitError.getResponse() != null) {
                    Log.d(TAG, "failure : " + retrofitError
                            .getResponse().getReason());
                    hideLoading();
                }
            }
        });
    }
    private void showLoading(){
        progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();
    }
    private void hideLoading(){
        if (progDialog != null && progDialog.isShowing()){
            progDialog.dismiss();
        }
    }

}
