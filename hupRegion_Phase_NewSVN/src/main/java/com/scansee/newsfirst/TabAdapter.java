package com.scansee.newsfirst;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.scansee.newsfirst.Fragment.NonFeebWebViewFragment;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 5/17/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class TabAdapter extends FragmentStatePagerAdapter {

    private final int tabCount;
    private final int mScreenHeight;
    private final int mScreenWidth;
    private final String template;
    private final ArrayList<ListCatDetails> bookMarkList;
    private final ArrayList<SubCat> subCatList;

    private TabFragmentView tabView = null;
    private GridFragment videoView = null;


    public TabAdapter(FragmentManager supportFragmentManager, int tabCount, int mScreenHeight, int mScreenWidth, ArrayList<ListCatDetails> bookMarkList, ArrayList<SubCat> subCatList, String template) {
        super(supportFragmentManager);
        this.tabCount = tabCount;
        this.mScreenHeight = mScreenHeight;
        this.mScreenWidth = mScreenWidth;
        this.bookMarkList = bookMarkList;
        this.subCatList = subCatList;
        this.template = template;
    }


    @Override
    public Fragment getItem(int position) {
        String catName, nonFeedUrl = null;
        if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {
            catName = bookMarkList.get(position).getParCatName();
            nonFeedUrl = bookMarkList.get(position).getNonfeedlink();

        } else {
            catName = subCatList.get(position).getSubCatName();
        }
        if (nonFeedUrl == null)
        {
            if (catName.equalsIgnoreCase("videos") || catName.equalsIgnoreCase("photos")) {
                videoView = setGridView(position);
                return videoView;
            } else {
                tabView = setListView(position);
                return tabView;
            }
        } else {
            return getWebViewFragment(nonFeedUrl);
        }

    }

    private NonFeebWebViewFragment getWebViewFragment(String url) {
        NonFeebWebViewFragment webViewFragment = NonFeebWebViewFragment.newInstance(url);
        return webViewFragment;
    }


    private GridFragment setGridView(int position) {

        Bundle bundle = new Bundle();
        bundle.putInt("screenHeight", mScreenHeight);
        bundle.putInt("screenWidth", mScreenWidth);
        bundle.putString("category", bookMarkList.get(position).getParCatName());
        bundle.putInt("position", position);

        videoView = new GridFragment();
        videoView.setArguments(bundle);

        return videoView;
    }


    private TabFragmentView setListView(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putString("template", template);
        tabView = new TabFragmentView();
        tabView.setArguments(bundle);
        return tabView;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

}

