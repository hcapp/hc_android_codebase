package com.scansee.newsfirst;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 6/22/2016.
 */
public class SubCatObj {
    private String responseCode;
    private String responseText;
    private ArrayList<ListSubCatSetails> listCatDetails;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public ArrayList<ListSubCatSetails> getListCatDetails() {
        return listCatDetails;
    }

    public void setListCatDetails(ArrayList<ListSubCatSetails> listCatDetails) {
        this.listCatDetails = listCatDetails;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }


}
