package com.scansee.newsfirst;

/**
 * Created by supriya.m on 6/3/2016.
 */
public class MainCatList {
    private String subCatId;
    private String subCatName;
    private int isSubCategory;
    private String sortOrder;

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }


    public int getIsSubCategory() {
        return isSubCategory;
    }

    public void setIsSubCategory(int isSubCategory) {
        this.isSubCategory = isSubCategory;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }


}
