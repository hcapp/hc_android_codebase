package com.scansee.newsfirst;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.hubcity.android.commonUtil.Constants;

/**
 * Created by sharanamma on 24-06-2016.
 */
public class ConnectionUtils {
    private final Context mContext;

    public ConnectionUtils(Context context) {
        this.mContext = context;
    }

    public int getConnectivityStatus() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context
                .CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return Constants.TYPE_WIFI;
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return Constants.TYPE_MOBILE;
            }
        }
        return Constants.TYPE_NOT_CONNECTED;
    }
    public static ProgressDialog showLoading(Activity activity) {
        ProgressDialog  progDialog = new ProgressDialog(activity);
        try {
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage("Please Wait..");
            progDialog.setCancelable(false);
            progDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return progDialog;
    }
}
