package com.scansee.newsfirst;

import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by subramanya.v on 6/6/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class SideMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
		implements ItemTouchHelperAdapter
{
	private final LinkedList<BookMarkObj> sectionsList;
	private final BookMarkActivity bookMarkActivity;

	public SideMenuAdapter(LinkedList<BookMarkObj> sectionsListData, BookMarkActivity
			bookMarkActivity)
	{
		this.sectionsList = sectionsListData;
		this.bookMarkActivity = bookMarkActivity;

	}

	@Override
	public int getItemViewType(int position) {
		return sectionsList.get(position).isHeader();
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		ItemViewHolder itemViewHolder;
		HeaderViewHolder headerViewHolder;
		View view;
		if (viewType == 1) {
			view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_row, parent, false);

			headerViewHolder = new HeaderViewHolder(view);
			return headerViewHolder;
		} else {
			view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmark_row, parent, false);
			itemViewHolder = new ItemViewHolder(view);
			return itemViewHolder;
		}

	}

	@Override
	public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
		if (sectionsList.get(position).isHeader() == 1) {
			HeaderViewHolder itemView = (HeaderViewHolder) holder;
			itemView.category.setText(sectionsList.get(position).getCategory());
		} else {
			final ItemViewHolder headerViewHolder = (ItemViewHolder) holder;
			headerViewHolder.category.setText(sectionsList.get(position).getCategory());

			if(!sectionsList.get(position).isInBothSections())
			{
				headerViewHolder.addRow.setImageResource(R.drawable.plus);
			}
			else
			{
				headerViewHolder.addRow.setImageResource(R.drawable.minus);
			}
			if(!sectionsList.get(position).getDragCategory().equalsIgnoreCase(bookMarkActivity.getString(R
					.string
					.book_mark)))
			{
				headerViewHolder.handleView.setVisibility(View.GONE);
			}
			else
			{
				headerViewHolder.handleView.setVisibility(View.VISIBLE);
			}

			headerViewHolder.addRow.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						addRemoveList(position);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			// Start a drag whenever the handle view it touched
			headerViewHolder.handleView.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
						bookMarkActivity.onStartDrag(headerViewHolder);
					}
					return false;
				}
			});
		}
	}

	private void addRemoveList(int position) {
		String category = sectionsList.get(position).getCategory();
		int isHubFunctn = sectionsList.get(position).getIsHubFunctn();
		int parCatId = sectionsList.get(position).getParCatId();
		if(sectionsList.get(position).getDragCategory().equalsIgnoreCase(bookMarkActivity.getString(R
				.string
				.book_mark))) {
			sectionsList.remove(position);
			bookMarkAdd(category, bookMarkActivity.getString(R.string.other_section), false, parCatId,
					sectionsList.size(), isHubFunctn);
		}
		else
		{
			sectionsList.remove(position);
			int dragPos = 0;
			for (BookMarkObj dragCategory : sectionsList) {
				if (dragCategory.getDragCategory().equalsIgnoreCase(bookMarkActivity.getString(R
						.string
						.book_mark))) {
					dragPos++;
				}
			}
			bookMarkAdd(category, bookMarkActivity.getString(R.string.book_mark), true, parCatId, dragPos, isHubFunctn);

		}
		notifyDataSetChanged();
	}
	private void bookMarkAdd(String category, String dragCat, boolean isBothSection, int parCatId, int position, int isHubFunctn) {
		BookMarkObj bookMarkData = new BookMarkObj();
		bookMarkData.setCategory(category);
		bookMarkData.setIsHeader(0);
		bookMarkData.setDragCategory(dragCat);
		bookMarkData.setInBothSections(isBothSection);
		bookMarkData.setIsHubFunctn(isHubFunctn);
		bookMarkData.setParCatId(parCatId);
		sectionsList.add(position, bookMarkData);
	}

	@Override
	public int getItemCount() {
		return sectionsList.size();
	}

	@Override
	public boolean onItemMove(int fromPosition, int toPosition) {
		if (sectionsList.get(fromPosition).getDragCategory().equalsIgnoreCase(sectionsList.get(toPosition).getDragCategory())) {
			Collections.swap(sectionsList, fromPosition, toPosition);
			notifyItemMoved(fromPosition, toPosition);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onItemDismiss(int position) {
		try {
			addRemoveList(position);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static class ItemViewHolder extends RecyclerView.ViewHolder implements
			ItemTouchHelperViewHolder {

		public final TextView category;
		public final ImageView handleView;
		private final ImageView addRow;

		public ItemViewHolder(View itemView) {
			super(itemView);
			category = (TextView) itemView.findViewById(R.id.category);
			handleView = (ImageView) itemView.findViewById(R.id.handle);
			addRow = (ImageView) itemView.findViewById(R.id.add_row);
		}

		@Override
		public void onItemSelected() {
			itemView.setBackgroundColor(Color.LTGRAY);
		}

		@Override
		public void onItemClear() {
			itemView.setBackgroundColor(0);
		}
	}

	public static class HeaderViewHolder extends RecyclerView.ViewHolder {
		public final TextView category;

		public HeaderViewHolder(View itemView) {
			super(itemView);
			category = (TextView) itemView.findViewById(R.id.category);
		}

	}

}
