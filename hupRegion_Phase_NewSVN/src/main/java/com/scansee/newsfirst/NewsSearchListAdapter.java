package com.scansee.newsfirst;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scansee.hubregion.R;

import java.util.ArrayList;


public class NewsSearchListAdapter extends BaseAdapter {
    private final Context mContext;
    private final ArrayList<Items> searchList;
    private final LayoutInflater inflater;


    public NewsSearchListAdapter(Context context, ArrayList<Items> searchList) {
        this.mContext = context;
        this.searchList = searchList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return searchList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view = convertView;
        ViewHolder viewHolder = new ViewHolder();


        if (position == searchList.size() - 1) {
            if (!((NewsSearchActivity) mContext).isAlreadyLoading) {

                if (((NewsSearchActivity) mContext).nextPage) {

                    ((NewsSearchActivity) mContext).loadMoreDataForSearch();
                }
            }
        }

        Items item = searchList.get(position);
        if (convertView == null) {
            view = inflater.inflate(R.layout.scrolling_left_adapter, parent, false);
            viewHolder.newsTitle = (TextView) view.findViewById(R.id.sub_story_title);
            viewHolder.newsThumbnailImage = (ImageView) view.findViewById(R.id.sub_story_logo);
            viewHolder.newsAuthor = (TextView) view.findViewById(R.id.sub_story_author);
            viewHolder.newsTime = (TextView) view.findViewById(R.id.sub_story_time);
            viewHolder.newsSDesc = (TextView) view.findViewById(R.id.sub_story_summary);
            viewHolder.subStoryChild = (LinearLayout) view.findViewById(R.id.sub_story_child);
            viewHolder.progressBar = (ProgressBar)view.findViewById(R.id.progress);
            view.setTag(viewHolder);

        } else

        {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.newsTitle.setText(item.getTitle());
        if (item.getsDesc() != null && !item.getsDesc().

                equals("")

                )

        {
            viewHolder.newsSDesc.setText(item.getsDesc());
        }

        if (item.getAuthor() != null && !item.getAuthor().equals(""))

        {
            viewHolder.newsAuthor.setVisibility(View.VISIBLE);
            viewHolder.subStoryChild.setVisibility(View.VISIBLE);
            viewHolder.newsAuthor.setText(item.getAuthor().concat("  "));
        } else

        {
            view.findViewById(R.id.sub_story_author).setVisibility(View.GONE);
        }
        String date = item.getDate();
        String time = item.getTime();
        if(date != null) {
            viewHolder.subStoryChild.setVisibility(View.VISIBLE);
            if(time != null) {
                time = date.concat(" " + time);
            }
            else
            {
                time = date;
            }
        }
        if (time != null && !time.equalsIgnoreCase("")) {
            assert viewHolder != null;
            viewHolder.newsTime.setText(time);
        }

        if((item.getAuthor() == null) && (time == null) && (date == null))
        {
           viewHolder.subStoryChild.setVisibility(View.GONE);
        }

        if (item.getImage() != null && !item.getImage().equals("")) {
            new CommonMethods().loadImage(mContext, viewHolder.progressBar, item.getImage(), viewHolder.newsThumbnailImage);
        }

        return view;
    }

    @Override
    public int getCount() {
        return searchList.size();
    }

    public static class ViewHolder {
        TextView newsTitle;
        ImageView newsThumbnailImage;
        TextView newsAuthor;
        TextView newsSDesc;
        LinearLayout subStoryChild;
        TextView newsTime;
        ProgressBar progressBar;

    }
}
