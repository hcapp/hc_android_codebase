package com.scansee.newsfirst;


import android.content.Context;

import com.artifex.mupdflib.AsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetNewsSearchAsynTask extends AsyncTask<Void, Void, Void> {
    private final Context mContext;
    private final String searchkey;
    private final String isSideBar;
    private String lowerLimit = "";
    private String responseCode;
    private String responseText;
    private boolean nextPage;

    private ArrayList<Items> finalNewsSearchList;


    private static final String NEWS_MAX_COUNT = "maxCnt";
    private static final String NEWS_ITEM_ID = "itemID";
    private static final String NEWS_ROW_NUMBER = "rowNum";
    private static final String NEWS_TITLE = "title";
    private static final String NEWS_IMAGE = "image";
    private static final String NEWS_LINK = "link";
    private static final String NEWS_CAT_NAME = "catName";
    private static final String NEWS_SHORT_DESCRIPTION = "sDesc";
    private static final String NEWS_IMG_POSITION = "imgPosition";


    public GetNewsSearchAsynTask(Context context, String searchkey, String
            isSideBar, String lowerLimit, ArrayList<Items> newsList) {
        this.mContext = context;
        this.searchkey = searchkey;
        this.isSideBar = isSideBar;
        this.lowerLimit = lowerLimit;

        this.finalNewsSearchList = newsList;

    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (responseText != null && responseText.equals("Success")) {
            ((NewsSearchActivity) mContext).loadSearchList(finalNewsSearchList, nextPage,
                    lowerLimit, responseText);
        } else {

            finalNewsSearchList = new ArrayList<>();
            ((NewsSearchActivity) mContext).loadSearchList(finalNewsSearchList, nextPage,
                    lowerLimit,
                    responseText);

        }

    }

    @Override
    protected Void doInBackground(Void... params) {

        String url = Properties.getBaseUrl() + "band/getnewsscroltemplate";
        try {
            UrlRequestParams mUrlrequestParams = new UrlRequestParams();
            ServerConnections mServerConnections = new ServerConnections();



            JSONObject jsonUrlParams = mUrlrequestParams.getNewsSearchParam( searchkey,
                    isSideBar,
                    lowerLimit);
            JSONObject jsonObject = mServerConnections.getUrlJsonPostResponse(url,
                    jsonUrlParams);
            if (jsonObject != null) {

                responseCode = jsonObject.getString("responseCode");
                responseText = jsonObject.getString("responseText");




                JSONArray jsonArray = new JSONArray();
                if (jsonObject.has("items")) {

                    try {
                        JSONObject json1 = jsonObject.getJSONObject("items");
                        jsonArray.put(json1);

                    } catch (Exception e) {
                        jsonArray = jsonObject.getJSONArray("items");

                    }


                    for (int count = 0; count < jsonArray.length(); count++) {
                        JSONObject jsson = jsonArray.getJSONObject(count);

                        Items searchItem = new Items();
                        searchItem.setItemID(Integer.parseInt(jsson.getString(NEWS_ITEM_ID)));
                        searchItem.setRowNum(Integer.parseInt(jsson.getString
                                (NEWS_ROW_NUMBER)));
                        if (jsson.has(NEWS_TITLE)){
                            searchItem.setTitle(jsson.getString(NEWS_TITLE));
                        }
                        if (jsson.has(NEWS_IMAGE)){
                            searchItem.setImage(jsson.getString(NEWS_IMAGE));
                        }
                        if (jsson.has(NEWS_LINK)){
                            searchItem.setLink(jsson.getString(NEWS_LINK));
                        }
                        if (jsson.has(NEWS_CAT_NAME)){
                            searchItem.setCatName(jsson.getString(NEWS_CAT_NAME));
                        }
                        if (jsson.has(NEWS_SHORT_DESCRIPTION)){
                            searchItem.setsDesc(jsson.getString(NEWS_SHORT_DESCRIPTION));
                        }

                        if (jsson.has("date")){
                            searchItem.setDate(jsson.getString("date"));
                        }
                        if (jsson.has("time")){
                            searchItem.setTime(jsson.getString("time"));
                        }
                        if (jsson.has("author")){
                            searchItem.setAuthor(jsson.getString("author"));
                        }
                        finalNewsSearchList.add(searchItem);
                    }
                }

                if (jsonObject.optString("nextPage").equals("1")) {
                    if (jsonObject.has("lowerLimitFlag")) {
                        lowerLimit = jsonObject.getString("lowerLimitFlag");
                    }
                    nextPage = true;
                } else {
                    nextPage = false;
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
