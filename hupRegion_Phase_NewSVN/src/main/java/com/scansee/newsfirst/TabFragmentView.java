package com.scansee.newsfirst;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by subramanya.v on 5/17/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
@SuppressLint("ValidFragment")
public class TabFragmentView extends Fragment {

    private  String template;
    private  int tabPosition;
    ListView viewPagerFragment;
    private View headerView;
    public SwipeRefreshLayout swipeRefreshLayout;
    View moreResultsView;
    private String subPageName;
    private SwipeRefreshLayout gridRefresh;
    private listViewAdapter listAdapter;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_fragment, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        gridRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.grid_refresh);
        getIntentValues();
        displayListView(rootView, inflater);
        return rootView;
    }
    private void getIntentValues() {
        Bundle bundle = getArguments();
        tabPosition = bundle.getInt("position");
        template = bundle.getString("template");
    }
    private void displayListView(View rootView, LayoutInflater inflater) {
        gridRefresh.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);

        if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {
            displayScrollView(rootView, inflater);
        } else {
            displaySubPage(rootView, inflater);
        }

        setListClickListener();

        if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {
            viewPagerFragment.addHeaderView(headerView);
        } else {
            if (subPageName != null) {
                if (subPageName.equalsIgnoreCase("Big Banner")) {
                    viewPagerFragment.addHeaderView(headerView);
                }
            }
        }
        listAdapter = new listViewAdapter(TabFragmentView.this, template, tabPosition);
        viewPagerFragment.setAdapter(listAdapter);
    }

    private void displaySubPage(View rootView, LayoutInflater inflater) {
        if (((SubpageActivty)mContext).subPageData.get(
                tabPosition) != null) {
            subPageName = ((SubpageActivty)mContext).subPageData.get(
                    tabPosition).getSubPageName();
            moreResultsView = inflater.inflate(
                    R.layout.pagination, viewPagerFragment, true);
            if (subPageName != null) {
                if (subPageName.equalsIgnoreCase("Big Banner")) {
                    headerView = inflater.inflate(
                            R.layout.tab_subpage, viewPagerFragment, true);
                    ImageView bigBanner = (ImageView) headerView.findViewById(R.id.big_banner);
                    TextView author = (TextView) headerView.findViewById(R.id.author);
                    RelativeLayout detailsParent = (RelativeLayout) headerView.findViewById(R.id.descriptionLayout);
                    TextView title = (TextView) headerView.findViewById(R.id.title);
                    RelativeLayout bannerHolder = (RelativeLayout) headerView.findViewById(R.id.banner_holder);
                    final ProgressBar progressBar = (ProgressBar) headerView.findViewById(R.id.progress);
                    if (((SubpageActivty)mContext).subPageData.get(tabPosition) != null) {
                        if (((SubpageActivty)mContext).subPageData.get(tabPosition)
                                .getItems().size() != 0) {
                            detailsParent.setVisibility(View.VISIBLE);
                            String subAuthor = ((SubpageActivty)mContext).subPageData.get
                                    (tabPosition).getItems().get(0).getAuthor();
                            if (subAuthor != null && !subAuthor.equalsIgnoreCase("")) {
                                author.setVisibility(View.VISIBLE);
                                author.setText(subAuthor);
                            }
                            else
                            {
                                author.setVisibility(View.GONE);
                            }
                            String subTitle = ((SubpageActivty)mContext).subPageData.get(
                                    tabPosition).getItems().get(0).getTitle();
                            if (subTitle != null && !subTitle.equalsIgnoreCase("")) {
                                title.setText(subTitle);
                            }
                            String image = ((SubpageActivty)mContext).subPageData.get(tabPosition)
                                    .getItems().get(0).getImage();
                            if (image != null && !image.equalsIgnoreCase("")) {
                                bigBanner.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.VISIBLE);
                                image = image.replaceAll(" ", "%20");
                                Picasso.with(getActivity()).load(image).into(bigBanner, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        progressBar.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
                            } else {
                                bannerHolder.setVisibility(View.GONE);
                            }

                        }
                    }
                }
            }
        }
        viewPagerFragment = (ListView) rootView.findViewById(R.id.view_pager);
    }

    private void displayScrollView(View rootView, LayoutInflater inflater) {
        headerView = inflater.inflate(
                R.layout.scrollview_header, viewPagerFragment, true);
        moreResultsView = inflater.inflate(
                R.layout.pagination, viewPagerFragment, true);
        ImageView largeLogo = (ImageView) headerView.findViewById(R.id.large_logo);
        LinearLayout descSummary = (LinearLayout)headerView.findViewById(R.id.desc_summary);
        TextView titleView = (TextView) headerView.findViewById(R.id.title);
        TextView authorView = (TextView) headerView.findViewById(R.id.author);
        TextView timeView = (TextView) headerView.findViewById(R.id.time);
        TextView desc = (TextView) headerView.findViewById(R.id.summary);
        RelativeLayout bannerHolder = (RelativeLayout) headerView.findViewById(R.id.banner_holder);
        final ProgressBar progressBar = (ProgressBar) headerView.findViewById(R.id.progress);
        if (((ScrollingPageActivity)mContext).scrollingData.get(tabPosition) != null) {
            if (((ScrollingPageActivity)mContext).scrollingData.get(tabPosition)
                    .getItems().size() != 0) {
                String title = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition)
                        .getItems().get(0).getTitle();
                if (title != null && !title.equalsIgnoreCase("")) {
                    titleView.setText(title);
                }
                String author = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition)
                        .getItems().get(0).getAuthor();
                if (author != null) {
                    if (author.length() > 15) {
                        author = author.substring(0, 15).concat("...");
                    }
                }
                if (author != null && !author.equalsIgnoreCase("")) {
                    descSummary.setVisibility(View.VISIBLE);
                    authorView.setVisibility(View.VISIBLE);
                    authorView.setText(author.concat("  "));
                }
                String time = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition)
                        .getItems().get(0).getTime();
                String date = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition)
                        .getItems().get(0).getDate();
                if((author == null) && (time == null) && (date == null))
                {
                    descSummary.setVisibility(View.GONE);
                }
                if(date != null) {
                    descSummary.setVisibility(View.VISIBLE);
                    if(time != null) {
                        time = date.concat(" " + time);
                    }
                    else
                    {
                        time = date;
                    }
                }
                if (time != null && !time.equalsIgnoreCase("")) {
                    timeView.setText(time);
                }
                String summary = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition)
                        .getItems().get(0).getsDesc();
                if (summary != null && !summary.equalsIgnoreCase("")) {
                    desc.setText(summary);
                }
                String image = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition)
                        .getItems().get(0).getImage();

                if (image != null && !image.equalsIgnoreCase("")) {
                    largeLogo.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    image = image.replaceAll(" ", "%20");
                    Picasso.with(getActivity()).load(image).into(largeLogo, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    bannerHolder.setVisibility(View.GONE);
                }
            }
        }
        viewPagerFragment = (ListView) rootView.findViewById(R.id.view_pager);
    }

    private void setListClickListener() {
        viewPagerFragment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent detailScreen = new Intent(getActivity(), NewsDetailsActivity.class);
                int itemId;
                if (template != null && template.equalsIgnoreCase("Scrolling News Template")) {
                    itemId = ((ScrollingPageActivity)mContext).scrollingData.get(tabPosition)
                            .getItems().get(i).getItemID();
                } else {
                    itemId = ((SubpageActivty)mContext).subPageData.get(tabPosition)
                            .getItems().get(i).getItemID();
                }

                detailScreen.putExtra("itemId", itemId);
                startActivity(detailScreen);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (template.equalsIgnoreCase("Scrolling News Template")) {
                    ((ScrollingPageActivity) getActivity()).startPullToRequest(
                            tabPosition, listAdapter);
                }
                else
                {
                    ((SubpageActivty) getActivity()).startPullToRequest(
                            tabPosition, listAdapter);
                }
            }
        });
    }
}
