package com.scansee.newsfirst;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.BackgroundTask.ThumbNailAsyTask;
import com.scansee.newsfirst.model.CategoryItemList;
import com.scansee.newsfirst.model.CategoryListModel;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by supriya.m on 5/18/2016.
 */
public class CombinationListAdapter extends BaseAdapter
{
	private LayoutInflater mLayoutInflater;
	private Context mContext;
	private ListView mParentView;
	private ArrayList<CategoryListModel> mCategoryList = new ArrayList<>();
	private final String LIST = "List";
	private final String TWO_ACROSS = "Two Across";
	private final String THREE_ACROSS = "Three Across";

	private static SwipeRefreshLayout swipeRefreshLayout;

	public CombinationListAdapter(Context context, ListView listView,
			ArrayList<CategoryListModel> categoryList, SwipeRefreshLayout swipeRefreshLay)
	{
		mLayoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mContext = context;
		this.mParentView = listView;
		this.mCategoryList = categoryList;
		swipeRefreshLayout = swipeRefreshLay;

	}

	@Override
	public int getCount()
	{
		return mCategoryList.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mCategoryList.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{

		try {
			if(mCategoryList.get(position).getGroupContent() == null)
			{
				convertView = mLayoutInflater.inflate(R.layout.combination_story_header, null);
				TextView categoryTextView = (TextView) convertView.findViewById(R.id
						.story_header_text);
				if (mCategoryList.get(position).getCatTxtColor() != null)
				{
					categoryTextView.setTextColor(Color.parseColor(mCategoryList.get(position)
							.getCatTxtColor()));
				}
				categoryTextView.setText(mCategoryList.get(position).getCatName());
				setHeaderBackground((RelativeLayout) convertView, mCategoryList.get(position).getCatColor());
				convertView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						CustomTitleBar.isNonFeed = true;
						Intent intent = new Intent(mContext,
								ScanseeBrowserActivity.class);
						intent.putExtra("module","news");
						intent.putExtra("title",mCategoryList.get(position).getCatName());
						intent.putExtra("textColor",mCategoryList.get(position).getCatTxtColor());
						intent.putExtra("textBgColor",mCategoryList.get(position).getCatColor());
						intent.putExtra("backButtonColor",mCategoryList.get(position).getBackButtonColor());
						intent.putExtra(CommonConstants.URL, mCategoryList.get(position).getNonfeedlink());
						mContext.startActivity(intent);
					}
				});

			} else	if (mCategoryList.get(position).getGroupContent().equalsIgnoreCase(LIST)) {
				final ArrayList<CategoryItemList> itemList = mCategoryList.get(position)
						.getItemList();
				final CategoryItemList itemObject = itemList.get(0);

				convertView = mLayoutInflater.inflate(R.layout.large_image_view, null);

				ImageView imageView = (ImageView) convertView.findViewById(R.id.large_image);
				ImageView playImageView = (ImageView) convertView.findViewById(R.id.play_button);
				TextView titleTextView = (TextView) convertView.findViewById(R.id.news_title_text);
				TextView author = (TextView) convertView.findViewById(R.id.news_author);
				TextView photosCountTextView = (TextView) convertView.findViewById(R.id.count);
				RelativeLayout countViewLayout = (RelativeLayout) convertView.findViewById(R.id
						.count_parent);

				final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id
						.progress);

				titleTextView.setText(itemObject.getTitle());
				TextView categoryTextView = (TextView) convertView.findViewById(R.id
						.story_header_text);
				categoryTextView.setText(mCategoryList.get(position).getCatName());

				if (mCategoryList.get(position).getCatTxtColor() != null) {
					categoryTextView.setTextColor(Color.parseColor(mCategoryList.get(position)
							.getCatTxtColor()));
				}

				if (itemObject.getAuthor() != null) {
					author.setVisibility(View.VISIBLE);
					author.setText(itemObject.getAuthor());
				}else{
					author.setVisibility(View.GONE);
				}
				String category = itemObject.getCatName();
				String link;
				if (category.equalsIgnoreCase(GlobalConstants.PHOTOS)) {
					link = itemObject.getImage();

					if (link != null) {
						String[] images = link.split(",");
						if (images.length > 1) {
							countViewLayout.setVisibility(View.VISIBLE);
							playImageView.setVisibility(View.GONE);
							photosCountTextView.setText(String.valueOf("" + images.length));
							if (itemObject.getAuthor() == null) {
								author.setVisibility(View.INVISIBLE);
							}
						} else {
							countViewLayout.setVisibility(View.GONE);
							playImageView.setVisibility(View.GONE);
							if (itemObject.getAuthor() == null) {
								author.setVisibility(View.GONE);
							}
						}
						new CommonMethods().loadImage(mContext, progressBar, images[0], imageView);
					}

				} else if (category.equalsIgnoreCase(GlobalConstants.VIDEOS)) {
					link = itemObject.getVideoLink();

					if (link != null) {
						String[] images = link.split(",");
						if (images.length > 1) {
							countViewLayout.setVisibility(View.VISIBLE);
							photosCountTextView.setText(String.valueOf("" + images.length));
							if (itemObject.getAuthor() == null) {
								author.setVisibility(View.INVISIBLE);
							}
						} else {
							countViewLayout.setVisibility(View.GONE);
							if (itemObject.getAuthor() == null) {
								author.setVisibility(View.GONE);
							}
						}
						playImageView.setVisibility(View.VISIBLE);
						try {
							ThumbNailAsyTask thumNail = new ThumbNailAsyTask(images[0], imageView);
							thumNail.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
						} catch (Throwable throwable) {
							throwable.printStackTrace();
						}
					}
				} else {
					if (itemObject.getAuthor() == null) {
						author.setVisibility(View.GONE);
					}
					if (itemObject.getImage() != null) {
						convertView.findViewById(R.id.frame_parent).setVisibility(View.VISIBLE);
						countViewLayout.setVisibility(View.GONE);
						new CommonMethods().loadImage(mContext, progressBar, itemObject.getImage()
								, imageView);
					} else {
						convertView.findViewById(R.id.frame_parent).setVisibility(View.GONE);
					}
				}

				final ListView listView = (ListView) convertView.findViewById(R.id.story_list);

				if (itemList.size() > 1) {
					listView.setVisibility(View.VISIBLE);
					listView.setAdapter(new ListSummaryStoryAdapter(mContext, itemList));
				} else {
					listView.setVisibility(View.GONE);
				}

				//Scroll listener to add view more button at the end
				final View viewMoreLayout = mLayoutInflater.inflate(
						R.layout.view_more, null);
				if (itemList.size() > 1) {
					listView.removeFooterView(viewMoreLayout);
					listView.addFooterView(viewMoreLayout);
				}

				mParentView.setOnTouchListener(new View.OnTouchListener()
				{

					public boolean onTouch(View v, MotionEvent event)
					{
						Log.v("PARENT", "PARENT TOUCH");
						listView.getParent()
								.requestDisallowInterceptTouchEvent(false);
						swipeRefreshLayout.setEnabled(true);
						return false;
					}
				});

				listView.setOnTouchListener(new View.OnTouchListener()
				{

					public boolean onTouch(View v, MotionEvent event)
					{
						Log.v("CHILD", "CHILD TOUCH");
						swipeRefreshLayout.setEnabled(false);
						swipeRefreshLayout.setRefreshing(false);
						v.getParent().requestDisallowInterceptTouchEvent(true);
						return false;
					}
				});
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int pos, long
							id)
					{
	                   /* gotoDetailsScreen(itemList.get(position + 1).getItemID());*/
						handleViewItemClick(position, pos + 1);

					}
				});
				RelativeLayout headerLayout = (RelativeLayout) convertView.findViewById(R.id
						.headerLayout);

				if (mCategoryList.get(position).getCatColor() != null) {
					setHeaderBackground(headerLayout, mCategoryList.get(position).getCatColor());
				}
				headerLayout.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (mCategoryList != null && mCategoryList.size() > 0) {
							((CombinationTemplate)mContext).gotoSubPage(mCategoryList.get(position).getCatName(), mCategoryList.get
									(position).getCatTxtColor());
						}
					}
				});
				imageView.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						String imagePath;
						ArrayList<String> linksList = new ArrayList<>();
						if (itemObject.getCatName().equalsIgnoreCase(GlobalConstants.PHOTOS)) {
							imagePath = itemObject.getImage();
							if (imagePath != null) {
								String[] links = imagePath.split(",");
								Collections.addAll(linksList, links);
								gotoPhotosVideosDetailsPage(linksList, mCategoryList.get(0),
										itemObject.getlDesc(),itemObject.getItemID());
							}
						} else if (itemObject.getCatName().equalsIgnoreCase(GlobalConstants
								.VIDEOS)) {
							imagePath = itemObject.getVideoLink();
							if (imagePath != null) {
								String[] links = imagePath.split(",");
								Collections.addAll(linksList, links);
								gotoPhotosVideosDetailsPage(linksList, mCategoryList.get(0),
										itemObject.getlDesc(), itemObject.getItemID());
							}
							else {
								CommonConstants.displayToast(mContext, mContext.getString(R.string
										.video_alert));
							}
						} else {
							gotoDetailsScreen(itemObject.getItemID());
						}
					}
				});
				viewMoreLayout.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (mCategoryList != null && mCategoryList.size() > 0) {
							((CombinationTemplate)mContext).gotoSubPage(mCategoryList.get(position).getCatName(),
									mCategoryList.get(position).getCatTxtColor());
						}
					}
				});
			}

			if (mCategoryList.get(position).getGroupContent().equals(THREE_ACROSS)) {
				convertView = mLayoutInflater.inflate(R.layout.three_across_story, null);
				TextView categoryTextView = (TextView) convertView.findViewById(R.id
						.story_header_text);
				categoryTextView.setText(mCategoryList.get(position).getCatName());
				if (mCategoryList.get(position).getCatTxtColor() != null) {
					categoryTextView.setTextColor(Color.parseColor(mCategoryList.get(position)
							.getCatTxtColor()));
				}
				RecyclerView recyclerView = (RecyclerView) convertView.findViewById(R.id
						.three_across_recyclerView);
				recyclerView.setHasFixedSize(true);
				LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,
						LinearLayoutManager.HORIZONTAL, false);
				recyclerView.setLayoutManager(layoutManager);
				recyclerView.setItemAnimator(new DefaultItemAnimator());
				recyclerView.addItemDecoration(new DividerItemDecoration(mContext,
						LinearLayoutManager
								.HORIZONTAL));
				recyclerView.setAdapter(new ThreeAcrossAdapter(mContext, mCategoryList.get
						(position)
						.getItemList()));
				RelativeLayout headerLayout = (RelativeLayout) convertView.findViewById(R.id
						.headerLayout);
				if (mCategoryList.get(position).getCatColor() != null) {
					setHeaderBackground(headerLayout, mCategoryList.get(position).getCatColor());
				}
				headerLayout.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (mCategoryList != null && mCategoryList.size() > 0) {
							((CombinationTemplate)mContext).gotoSubPage(mCategoryList.get(position).getCatName(), mCategoryList.get
									(position).getCatTxtColor());
						}
					}
				});
				recyclerView.addOnItemTouchListener(new RecyclerTouchListener(mContext,
						recyclerView,
						new ClickListener()
						{
							@Override
							public void onClick(View view, int pos)
							{
								handleViewItemClick(position, pos);
							}

						}));
			}
			if (mCategoryList.get(position).getGroupContent().equals(TWO_ACROSS)) {

				convertView = mLayoutInflater.inflate(R.layout.three_across_story, null);
				TextView categoryTextView = (TextView) convertView.findViewById(R.id
						.story_header_text);
				categoryTextView.setText(mCategoryList.get(position).getCatName());
				if (mCategoryList.get(position).getCatTxtColor() != null) {
					categoryTextView.setTextColor(Color.parseColor(mCategoryList.get(position)
							.getCatTxtColor()));
				}
				RecyclerView recyclerView = (RecyclerView) convertView.findViewById(R.id
						.three_across_recyclerView);
				recyclerView.setHasFixedSize(true);
				LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,
						LinearLayoutManager.HORIZONTAL, false);
				recyclerView.setLayoutManager(layoutManager);
				recyclerView.setItemAnimator(new DefaultItemAnimator());
				recyclerView.addItemDecoration(new DividerItemDecoration(mContext,
						LinearLayoutManager
								.HORIZONTAL));
				recyclerView.setAdapter(new TwoAcrossAdapter(mContext, mCategoryList.get(position)
						.getItemList()));
				RelativeLayout headerLayout = (RelativeLayout) convertView.findViewById(R.id
						.headerLayout);
				if (mCategoryList.get(position).getCatColor() != null) {
					setHeaderBackground(headerLayout, mCategoryList.get(position).getCatColor());
				}
				headerLayout.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (mCategoryList != null && mCategoryList.size() > 0) {
							((CombinationTemplate)mContext).gotoSubPage(mCategoryList.get(position).getCatName(), mCategoryList.get
									(position).getCatTxtColor());
						}
					}
				});
				recyclerView.addOnItemTouchListener(new RecyclerTouchListener(mContext,
						recyclerView,
						new ClickListener()
						{
							@Override
							public void onClick(View view, int pos)
							{
								handleViewItemClick(position, pos);
							}

						}));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	private void gotoPhotosVideosDetailsPage(ArrayList links, CategoryListModel item,
											 String lDesc, int itemID)
	{
		Intent intent;

		if (links != null) {
			if (links.size() > 1) {
				intent = new Intent(mContext, GridChildView.class);
			} else {
				intent = new Intent(mContext, ViewDetailsScreen.class);
			}
			intent.putExtra("itemId", itemID);
			intent.putStringArrayListExtra("link", links);
			intent.putExtra("category", item.getCatName());
			intent.putExtra("catColor", item.getCatColor());
			intent.putExtra("catTxtColor", item.getCatTxtColor());
			intent.putExtra("desc", lDesc);
			//intent.putExtra("scrollingScreen", true);
			mContext.startActivity(intent);
		}
	}

	private void setHeaderBackground(RelativeLayout layout, String color)
	{
		try {
			layout.setBackgroundColor(Color.parseColor(color));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	private void gotoDetailsScreen(int itemId)
	{
		Intent intent = new Intent(mContext, NewsDetailsActivity.class);
		intent.putExtra("itemId", itemId);
		mContext.startActivity(intent);
	}

	private void handleViewItemClick(int parentPos, int childPos)
	{
		CategoryListModel item = mCategoryList.get(parentPos);
		ArrayList<String> linksList = new ArrayList<>();
		String imagePath;
		if (childPos < mCategoryList.get(parentPos).getItemList().size()) {
			if (item.getCatName().equalsIgnoreCase(GlobalConstants.PHOTOS)) {
				imagePath = item.getItemList().get(childPos).getImage();
				if (imagePath != null) {
					String[] links = imagePath.split(",");
					Collections.addAll(linksList, links);
					gotoPhotosVideosDetailsPage(linksList, item, item.getItemList().get(childPos)
							.getlDesc(), item.getItemList().get(childPos)
							.getItemID());
				}
			} else if (item.getCatName().equalsIgnoreCase(GlobalConstants.VIDEOS)) {
				imagePath = item.getItemList().get(childPos).getVideoLink();
				if (imagePath != null) {
					String[] links = imagePath.split(",");
					Collections.addAll(linksList, links);
					gotoPhotosVideosDetailsPage(linksList, item, item.getItemList().get(childPos)
							.getlDesc(), item.getItemList().get(childPos).getItemID());
				}
				else
				{
					CommonConstants.displayToast(mContext,mContext.getString(R.string
							.video_alert));
				}
			} else {
				gotoDetailsScreen(mCategoryList.get(parentPos).getItemList().get
						(childPos)
						.getItemID());
			}
		} else {
			if (mCategoryList != null && mCategoryList.size() > 0) {
				((CombinationTemplate)mContext).gotoSubPage(mCategoryList.get(parentPos).getCatName(),
						mCategoryList.get
								(parentPos).getCatTxtColor());
			}
		}

	}

	public interface ClickListener
	{
		void onClick(View view, int position);
	}

	public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener
	{

		private GestureDetector gestureDetector;
		private ClickListener clickListener;
		private static final int SWIPE_THRESHOLD = 100;
		private static final int SWIPE_VELOCITY_THRESHOLD = 100;

		public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final
		ClickListener clickListener)
		{
			this.clickListener = clickListener;
			gestureDetector = new GestureDetector(context, new GestureDetector
					.SimpleOnGestureListener()
			{
				@Override
				public boolean onSingleTapUp(MotionEvent e)
				{
					return true;
				}

				@Override
				public void onLongPress(MotionEvent e)
				{
				}

				@Override
				public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float
						velocityY)
				{
					boolean result = false;
					try {
						float diffY = e2.getY() - e1.getY();
						float diffX = e2.getX() - e1.getX();
						// right and left swipe
						if (Math.abs(diffX) > Math.abs(diffY)) {
							swipeRefreshLayout.setEnabled(false);
							swipeRefreshLayout.setRefreshing(false);
						}
						// top and bottom swipe
						else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) >
								SWIPE_VELOCITY_THRESHOLD) {
							if (diffY > 0) {
								swipeRefreshLayout.setEnabled(true);
								swipeRefreshLayout.setRefreshing(true);
							}
						}

					} catch (Exception exception) {
						exception.printStackTrace();
					}
					return result;
				}
			});
		}

		@Override
		public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
		{

			View child = rv.findChildViewUnder(e.getX(), e.getY());
			if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
				clickListener.onClick(child, rv.getChildPosition(child));
			}
			//swipeRefreshLayout.setEnabled(false);
			//swipeRefreshLayout.setRefreshing(false);

			return false;
		}

		@Override
		public void onTouchEvent(RecyclerView rv, MotionEvent e)
		{

		}

		@Override
		public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
		{

		}
	}



}
