package com.scansee.newsfirst;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sharanamma on 13-07-2016.
 */
public class CommonMethods {
    public Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public void loadImage(Context context, final ProgressBar progressBar, String imageLink, ImageView imageView) {
        progressBar.setVisibility(View.VISIBLE);
        imageLink = imageLink.replaceAll(" ", "%20");
        Picasso.with(context).load(imageLink).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public static String getCurrentDate() {

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd , yyyy");
        String currentDate = sdf.format(new Date());
        Log.d("Date string : ", currentDate);
        return currentDate;
    }

    public static List<NavigationItem> arrangeInSortedOrder(SideMenuObject
                                                                    sideMenuObject, int position) {
        List<NavigationItem> newsList = new LinkedList<>();

        try {

            ArrayList<ListMainCat> newsCatList = sideMenuObject.getListCatDetails().get(0).getListMainCat();
            if (newsCatList != null) {
                for (int i = 0; i < newsCatList.size(); i++) {
                    ListMainCat newsObj = newsCatList.get(i);
                    NavigationItem item = new NavigationItem();
                    item.setFlag(newsObj.getFlag());
                    item.setSortOrder(newsObj.getSortOrder());
                    item.setCatName(newsObj.getCatName());
                    item.setCatId(newsObj.getCatId());
                    item.setCatTxtColor(newsObj.getCatTxtColor());
                    item.setMainCatList(newsObj.getMainCatList());
                    item.setIsSubCategory(newsObj.getIsSubCategory());
                    newsList.add(item);
                }
            }

            ArrayList<ArMItemList> hubCityCatList = new ArrayList<>();
            if (sideMenuObject.getListCatDetails().size() > 1 || position == 0) {
                hubCityCatList = sideMenuObject.getListCatDetails().get(position).getMenuList
                        ().get(0).getArMItemList();
            }
            for (int i = 0; i < hubCityCatList.size(); i++) {
                ArMItemList hubCityObj = hubCityCatList.get(i);
//                if (hubCityObj.getLinkTypeName().equalsIgnoreCase("News")) {
//                  continue;
//                }
                NavigationItem item = new NavigationItem();
                if (hubCityObj.getLinkTypeName().equalsIgnoreCase("Login/Logout")
                        || hubCityObj.getLinkTypeName().equalsIgnoreCase("News Settings")) {
                    String userStatusText = null;
                    if(hubCityObj.getLinkTypeName().equalsIgnoreCase("Login/Logout")) {
                        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                            userStatusText = "Logout";
                        } else {
                            userStatusText = "Login/SignUp";
                        }
                        item.setmItemName(userStatusText);
                        item.setLinkTypeName(userStatusText);
                    }
                    else
                    {
                        item.setCatName(hubCityObj.getmItemName());
                        item.setLinkTypeName(hubCityObj.getLinkTypeName());
                    }

                } else {
                    item.setLinkTypeName(hubCityObj.getLinkTypeName());
                    item.setmItemName(hubCityObj.getmItemName());
                }


                item.setFlag(hubCityObj.getFlag());
                item.setSortOrder(hubCityObj.getSortOrder());
                item.setLinkId(hubCityObj.getLinkId());
                item.setLinkTypeId(hubCityObj.getLinkTypeId());
                item.setmBtnColor(hubCityObj.getmBtnColor());
                item.setmBtnFontColor(hubCityObj.getmBtnFontColor());
                item.setmGrpBkgrdColor(hubCityObj.getmGrpBkgrdColor());
                item.setmGrpFntColor(hubCityObj.getmGrpFntColor());
                item.setmItemId(hubCityObj.getmItemId());
                item.setmItemImg(hubCityObj.getmItemImg());
                item.setPosition(hubCityObj.getPosition());
                item.setSmBtnColor(hubCityObj.getSmBtnColor());
                item.setSmBtnFontColor(hubCityObj.getSmBtnFontColor());
                item.setSmGrpBkgrdColor(hubCityObj.getSmGrpBkgrdColor());
                item.setSmGrpFntColor(hubCityObj.getSmGrpFntColor());
                if (!item.getLinkTypeName().equalsIgnoreCase("News")) {
                    newsList.add(item);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(newsList, new Comparator<NavigationItem>() {
            public int compare(NavigationItem obj1, NavigationItem obj2) {
                // ## Ascending order
                return Integer.valueOf(obj1.getSortOrder()).compareTo(Integer.valueOf(obj2.getSortOrder()));
            }
        });
        return newsList;
    }

    public static boolean emailValidation(String emailid) {
        Pattern emailPattern = Pattern.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
        Matcher emailMatcher = emailPattern.matcher(emailid);
        return emailMatcher.matches();
    }
}
