package com.scansee.hubregion;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

/**
 * Created by supriya.m on 10/4/2016.
 */
public class ScanSeeFireBaseInstanceIDListenerService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        //Uploading token to server to save it to database
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        String urlPush = mUrlRequestParams.setregIdInServer(token);
        String url_push_msg_alert = Properties.url_local_server
                + Properties.hubciti_version
                + "firstuse/registerpushnotify";
        mServerConnections.getUrlPostResponse(
                url_push_msg_alert, urlPush, true);
    }
}
