package com.scansee.hubregion;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.AlertDialogActivity;
import com.hubcity.android.screens.PushNotificationListScreen;
import com.hubcity.android.screens.SplashActivity;
import com.scansee.android.ScanseeBrowserActivity;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by supriya.m on 10/4/2016.
 */
public class ScanSeeFireBaseMessagingService extends FirebaseMessagingService {

    ArrayList<String> arrayList = new ArrayList<>();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            String message = null;
            message = remoteMessage.getData().get("body");
            arrayList = Constants.saveNotificationMessage(message);
            //Calling method to generate notification
            sendNotification(message);
        }
    }


    /**
     * Creating a notification as per the received GCM message.
     * On click of notification user will be redirected to different classes as per some
     * validations.
     * If the the user logged in to the app or not.
     * If logged in then is it guest login or normal login.
     * If properly logged in then as per notification message it will be redirected.
     * If both News and Deals are there in the message then it will be redirected to
     * PushNotification list screen
     * If only News is there then it will be redirected to the app browser with the news link.
     * If only Deal is there then it will be redirected to the respective deal screen as per the
     * deal type.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        Intent intent = null;
        JSONObject jsonObject;
        String messageToNotify;
        try {
            jsonObject = new JSONObject(message);
            messageToNotify = jsonObject.getString("notiMgs");

            boolean loginScreenStatus = Constants.bISLoginStatus;
            if (!UrlRequestParams.getUid().equals("-1")
                    && (loginScreenStatus || Constants.isCredentialsSaved(this))) {
                if (Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                    //This is to check whether the user is in Login screen or not, so that we can
                    // show proper alert according to that
                    ActivityManager am = (ActivityManager)
                            getSystemService(Activity.ACTIVITY_SERVICE);
                    String className = am.getRunningTasks(1).get(0).topActivity.getClassName();
                    Constants.showLoginMsg = className.contains("LoginScreen");
                    intent = new Intent(this, AlertDialogActivity.class);
                } else {
                    if (Properties.HUBCITI_KEY.equals("Tyler19")||Properties.HUBCITI_KEY.equals("Tyler Test75")||Properties.HUBCITI_KEY.equals("shrini2113") || Properties.HUBCITI_KEY.equals("HEB Test2156") || Properties.HUBCITI_KEY.equals("HEB Test54")) {
                        if (arrayList.size() > 1) {
                            intent = new Intent(this, PushNotificationListScreen.class);
                        } else {
                            if (jsonObject.has("rssFeedList") && jsonObject.has("dealList")) {
                                intent = new Intent(this, PushNotificationListScreen.class);
                            } else if (jsonObject.has("rssFeedList")) {
                                String newsLink = jsonObject.getJSONArray("rssFeedList")
                                        .getJSONObject(0)

                                        .optString("link");
                                intent = new Intent(
                                        this,
                                        ScanseeBrowserActivity.class);
                                intent.putExtra(CommonConstants.URL, newsLink);
                            } else {
                                intent = Constants.getNotificationDealIntent(jsonObject, this);
                            }
                        }
                    } else {
//						If array size more than 1 it will navigate to list screen else it will
//						navigate to respective details screen
//						If the size is 0 it will go to list screen and show a
//                      pop up to user that deal has expired
                        if (arrayList.size() > 1) {
                            intent = new Intent(this, PushNotificationListScreen.class);
                        } else if (arrayList.size() > 0) {
                            intent = Constants.getNotificationDealIntent(jsonObject, this);
                        } else {
                            intent = new Intent(this, PushNotificationListScreen.class);
                        }
                    }
                }
            } else {
                intent = new Intent(this, SplashActivity.class);
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(Constants.PUSH_NOTIFY_MSG, message);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
                    intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(Properties.push_icon)
                    .setContentTitle(this.getString(R.string.app_name))
                    .setContentText(messageToNotify)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
