/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scansee.hubregion;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.google.android.gms.gcm.GcmListenerService;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.AlertDialogActivity;
import com.hubcity.android.screens.PushNotificationListScreen;
import com.hubcity.android.screens.SplashActivity;
import com.scansee.android.ScanseeBrowserActivity;

import org.json.JSONObject;

import java.util.ArrayList;


public class ScanSeeGcmListenerService extends GcmListenerService
{

	private static final String TAG = "ScanSeeGcmListenerService";
	ArrayList<String> arrayList = new ArrayList<>();


	/**
	 * Called when message is received.
	 *
	 * @param from SenderID of the sender.
	 * @param data Data bundle containing message data as key/value pairs.
	 *             For Set of keys use data.keySet().
	 */
	// [START receive_message]
	@Override
	public void onMessageReceived(String from, Bundle data)
	{
		if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
			String message = data.getString("data");

			try {
				JSONObject jsonObject = new JSONObject(message);
//			cheching if tyler instance if not then retaining all deals that has not expired else
//			retaining push if it is not daily deal
				if (Properties.HUBCITI_KEY.equals("Tyler19")||Properties.HUBCITI_KEY.equals("Tyler Test75")||Properties.HUBCITI_KEY.equals("shrini2113")) {

//            Checking if daily deal then clearing all the push
//            else retaining the deal
					if (jsonObject.getString("notiMgs").contains(".") || jsonObject.getString
							("notiMgs")

							.toLowerCase().contains("top")) {
						Constants.removePushArray("gcm_msg");
						arrayList = new ArrayList<>();
						arrayList.add(message);
						Constants.savePushArray(arrayList, "gcm_msg");
					} else {
						arrayList = Constants.getPushArray("gcm_msg");
						for (int i = 0; i < arrayList.size(); i++) {
							try {
								String dealId = new JSONObject(arrayList.get(i)).getJSONArray
										("dealList")
										.getJSONObject(0).optString("dealId");
								if (dealId.equals(jsonObject.getJSONArray
										("dealList")
										.getJSONObject(0).optString("dealId"))) {
									arrayList.remove(i);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						for (int i = 0; i < arrayList.size(); i++) {
							try {
								String title = new JSONObject(arrayList.get(i)).getJSONArray
										("rssFeedList")
										.getJSONObject(0).optString("title");
								if (title.equals(jsonObject.getJSONArray
										("rssFeedList")
										.getJSONObject(0).optString("title"))) {
									arrayList.remove(i);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						arrayList.add(message);
						Constants.savePushArray(arrayList, "gcm_msg");
					}
				} else {
//				getting the previous pushed deals if any
					arrayList = Constants.getPushArray("gcm_msg");
//				Checking if the deal has expired or not
					if (!Constants.getDealExpirationState(jsonObject.getJSONArray
							("dealList")
							.getJSONObject(0).optString("endDate") + " " + jsonObject
							.getJSONArray("dealList")
							.getJSONObject(0).optString("endTime"))) {
//					if same deal has came then clearing the previously stored one and retaining
// new one
						for (int i = 0; i < arrayList.size(); i++) {
							String dealId = new JSONObject(arrayList.get(i)).getJSONArray
									("dealList")
									.getJSONObject(0).optString("dealId");
							if (dealId.equals(jsonObject.getJSONArray
									("dealList")
									.getJSONObject(0).optString("dealId"))) {
								arrayList.remove(i);
							}
						}
						arrayList.add(message);
						Constants.savePushArray(arrayList, "gcm_msg");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


//	    //Saving push message value to preference for use inside application from Settings
//        SharedPreferences sharedPreferences = this.getSharedPreferences(
//                Constants.PREFERENCE_HUB_CITY, 0);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("gcm_msg", message);
//        editor.commit();

			sendNotification(message);
		}
	}
	// [END receive_message]

	/**
	 * Creating a notification as per the received GCM message.
	 * On click of notification user will be redirected to different classes as per some
	 * validations.
	 * If the the user logged in to the app or not.
	 * If logged in then is it guest login or normal login.
	 * If properly logged in then as per notification message it will be redirected.
	 * If both News and Deals are there in the message then it will be redirected to
	 * PushNotification list screen
	 * If only News is there then it will be redirected to the app browser with the news link.
	 * If only Deal is there then it will be redirected to the respective deal screen as per the
	 * deal type.
	 *
	 * @param message GCM message received.
	 */
	private void sendNotification(String message)
	{
		Intent intent = null;
		JSONObject jsonObject;
		String messageToNotify;
		try {
			jsonObject = new JSONObject(message);
			messageToNotify = jsonObject.getString("notiMgs");

			boolean loginScreenStatus = Constants.bISLoginStatus;
			if (!UrlRequestParams.getUid().equals("-1")
					&& (loginScreenStatus || isCredentialsSaved(this))) {
				if (Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
					//This is to check whether the user is in Login screen or not, so that we can
					// show proper alert according to that
					ActivityManager am = (ActivityManager)
							getSystemService(Activity.ACTIVITY_SERVICE);
					String className = am.getRunningTasks(1).get(0).topActivity.getClassName();
					Constants.showLoginMsg = className.contains("LoginScreen");
					intent = new Intent(this, AlertDialogActivity.class);
				} else {
					if (Properties.HUBCITI_KEY.equals("Tyler19")||Properties.HUBCITI_KEY.equals("Tyler Test75")||Properties.HUBCITI_KEY.equals("shrini2113")) {
						if (arrayList.size() > 1) {
							intent = new Intent(this, PushNotificationListScreen.class);
						} else {
							if (jsonObject.has("rssFeedList") && jsonObject.has("dealList")) {
								intent = new Intent(this, PushNotificationListScreen.class);
							} else if (jsonObject.has("rssFeedList")) {
								String newsLink = jsonObject.getJSONArray("rssFeedList")
										.getJSONObject(0)

										.optString("link");
								intent = new Intent(
										this,
										ScanseeBrowserActivity.class);
								intent.putExtra(CommonConstants.URL, newsLink);
							} else {
								intent = Constants.getNotificationDealIntent(jsonObject, this);
							}
						}
					} else {
//						If array size more than 1 it will navigate to list screen else it will
//						navigate to respective details screen
//						If the size is 0 it will go to list screen and show a
//                      pop up to user that deal has expired
						if (arrayList.size() > 1) {
							intent = new Intent(this, PushNotificationListScreen.class);
						} else if (arrayList.size() > 0) {
							intent = Constants.getNotificationDealIntent(jsonObject, this);
						} else {
							intent = new Intent(this, PushNotificationListScreen.class);
						}
					}
				}
			} else {
				intent = new Intent(this, SplashActivity.class);
			}

			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			intent.putExtra(Constants.PUSH_NOTIFY_MSG, message);
			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
					intent,
					PendingIntent.FLAG_ONE_SHOT);

			Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
					.setSmallIcon(Properties.push_icon)
					.setContentTitle(this.getString(R.string.app_name))
					.setContentText(messageToNotify)
					.setAutoCancel(true)
					.setSound(defaultSoundUri)
					.setContentIntent(pendingIntent);

			NotificationManager notificationManager =
					(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

			notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Checking the user credential is saved in preference or not
	 * to get the confirmation of user's login status
	 *
	 * @param context
	 * @return true if the credential is saved else false
	 */
	private boolean isCredentialsSaved(Context context)
	{
		boolean isCredentailSaved = false;
		SharedPreferences settings = context.getSharedPreferences(Constants.PREFERENCE_HUB_CITY,
				0);
		boolean mStateChecked = settings.getBoolean(Constants.REMEMBER, false);
		String usrName = settings.getString(Constants.REMEMBER_NAME, null);
		String passwd = settings.getString(Constants.REMEMBER_PASSWORD, null);

		if (mStateChecked && usrName != null && passwd != null) {
			isCredentailSaved = true;
		}

		return isCredentailSaved;

	}
}