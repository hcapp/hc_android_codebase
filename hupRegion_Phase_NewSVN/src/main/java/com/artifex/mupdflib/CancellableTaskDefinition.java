package com.artifex.mupdflib;

public interface CancellableTaskDefinition <Params, Result>
{
	Result doInBackground(Params... params);
	void doCancel();
	void doCleanup();
}
