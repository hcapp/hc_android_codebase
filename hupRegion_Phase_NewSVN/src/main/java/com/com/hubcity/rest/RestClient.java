package com.com.hubcity.rest;

import com.com.hubcity.model.CombinationalTempRequest;
import com.com.hubcity.model.CouponModel;
import com.com.hubcity.model.CouponObj;
import com.com.hubcity.model.MapModel;
import com.com.hubcity.model.MapObj;
import com.com.hubcity.model.MyCouponModel;
import com.com.hubcity.model.MyCouponObj;
import com.com.hubcity.model.UserInfoModel;
import com.com.hubcity.model.UserInfoRequestModel;
import com.com.hubcity.model.UserInfoUpdateModel;
import com.hubcity.android.commonUtil.AddCouponDetails;
import com.hubcity.android.commonUtil.BookMarkModel;
import com.hubcity.android.commonUtil.ClaimGetDetails;
import com.hubcity.android.commonUtil.ClaimModel;
import com.hubcity.android.commonUtil.CouponDetails;
import com.hubcity.android.commonUtil.GetAddCouponObject;
import com.hubcity.android.commonUtil.GetClaimObject;
import com.hubcity.android.commonUtil.GetCouponDetailsObject;
import com.hubcity.android.commonUtil.GetRedeemObject;
import com.hubcity.android.commonUtil.MultipleBandModel;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.RedeemGetDetails;
import com.hubcity.android.commonUtil.ScrollingPageModel;

import com.hubcity.android.model.HubCitiSlideModel;
import com.hubcity.android.model.UpdateUserResponseModel;
import com.hubcity.android.screens.ClaimRequestObj;
import com.hubcity.android.screens.MultipleBandObj;
import com.scansee.newsfirst.BookMarkObj;
import com.scansee.newsfirst.NewsSideMenu;
import com.scansee.newsfirst.ScrollingObj;
import com.scansee.newsfirst.SideMenuModel;
import com.scansee.newsfirst.SideMenuObject;
import com.scansee.newsfirst.SubCatObj;
import com.scansee.newsfirst.UpdateModel;
import com.scansee.newsfirst.model.GetCombinationTempModel;
import com.scansee.newsfirst.model.NewsDetailsModel;
import com.scansee.newsfirst.model.SubPageModel;
import com.scansee.newsfirst.model.TileTemplateModel;
import com.scansee.newsfirst.model.TileTemplateRequestModel;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by sharanamma on 1/8/2016.
 */
public class RestClient {

    private static final RestClient objAPIClient = new RestClient();
    private RestInterface sApiInterface;
    private Void specialOffer;
    private boolean hardcodedApi;

    private RestClient() {

    }

    public static RestClient getInstance() {
        return objAPIClient;
    }

    private RestInterface getAPIClient() {
        if (sApiInterface == null) {

            final OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
            okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
            String endPoint = Properties.getBaseUrl();




            RestAdapter.Builder restAdapterBuilder = new RestAdapter.Builder()
                    .setEndpoint(endPoint).setClient(new OkClient(okHttpClient));

            sApiInterface = restAdapterBuilder.build().create(RestInterface.class);
        }

        return sApiInterface;
    }

    public void getUserDetails(UserInfoRequestModel userInfoRequest, final
    Callback<UserInfoModel> callback) {
        getAPIClient().getUserInfo(userInfoRequest, callback);

    }

    public void updateUserDetails(UserInfoUpdateModel userInfo, final
    Callback<UpdateUserResponseModel> response) {
        getAPIClient().updateUserInfo(userInfo, response);

    }

    public void getCombinationTemplate(CombinationalTempRequest cTemplateInfo, final
    Callback<GetCombinationTempModel> callback) {
        getAPIClient().getCombinationTemplate(cTemplateInfo, callback);
    }

    public void getBookMarkDetails(BookMarkModel bookMarkRequest, Callback<BookMarkObj> callback) {
        getAPIClient().getBookMarkInfo(bookMarkRequest, callback);
    }

    public void getNewsDetail(int newsId, final
    Callback<NewsDetailsModel> callback) {
        getAPIClient().getNewsDetail(newsId, callback);
    }

    public void getscroltemplate(ScrollingPageModel scrollingObj, Callback<ScrollingObj> callback) {
        getAPIClient().getScrollingInfo(scrollingObj, callback);
    }
    public void getSideMenuDetails(SideMenuModel sideMenuRequest, Callback<BookMarkObj> callback) {
        getAPIClient().getSideMenuInfo(sideMenuRequest, callback);
    }

    public void getUpdateDetails(UpdateModel updateRequest, Callback<BookMarkObj> callback) {
        getAPIClient().getUpdateInfo(updateRequest, callback);
    }

    public void getSideNavMenu(NewsSideMenu sideMenuRequest, Callback<SideMenuObject> callback) {
        getAPIClient().getsideMenuInfo(sideMenuRequest, callback);
    }

    public void getnewsubcategories(SubPageModel subCatRequest, Callback<SubCatObj> callback) {
        getAPIClient().getnewsubcategories(subCatRequest, callback);
    }

    public void getTileTemplateCategories(TileTemplateRequestModel tileTemplateCatRequest,
                                          Callback<TileTemplateModel> callback) {
        getAPIClient().getTileTemplateCategories(tileTemplateCatRequest, callback);
    }

    public void getSlides(String hubCityKey, Callback<HubCitiSlideModel> callback) {
        getAPIClient().getHubCitiSlides(hubCityKey, callback);
    }

    public void setClaimRequest(ClaimModel detailsRequest, Callback<ClaimRequestObj> callback) {
        getAPIClient().setClaimRequest(detailsRequest, callback);
    }

    public void sendMulBandReq(MultipleBandModel multipleReq, Callback<MultipleBandObj> callback) {
        getAPIClient().setMulBand(multipleReq, callback);
    }

    public void getClaimDetails(ClaimGetDetails request, Callback<GetClaimObject> callback) {
        getAPIClient().getCliam(request, callback);
    }

    public void getCoupon(CouponModel request, Callback<CouponObj> callback) {
        getAPIClient().getCoupon(request,callback);
    }

    public void getMyCoupon(MyCouponModel request, Callback<MyCouponObj> callback) {
        getAPIClient().getMyCoupon(request,callback);
    }

    public void getMap(MapModel request, Callback<MapObj> callback) {
        getAPIClient().getMap(request,callback);
    }
    public void getRedeemDetails(RedeemGetDetails redeemrequest, Callback<GetRedeemObject> callback) {
        getAPIClient().getRedeemCliam(redeemrequest, callback);
    }

    public void getAddCouponDetails(AddCouponDetails addCouponDetails, Callback<GetAddCouponObject> callback) {
        getAPIClient().getAddCouponClaim(addCouponDetails, callback);
    }

    public void getCouponDetails(CouponDetails CouponDetails, Callback<GetCouponDetailsObject> callback) {
        sApiInterface = null;
        hardcodedApi = true;
        getAPIClient().getCouponClaim(CouponDetails, callback);
    }
}
