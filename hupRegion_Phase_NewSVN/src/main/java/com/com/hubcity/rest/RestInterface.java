package com.com.hubcity.rest;

import com.com.hubcity.model.CombinationalTempRequest;
import com.com.hubcity.model.CouponModel;
import com.com.hubcity.model.CouponObj;
import com.com.hubcity.model.MapModel;
import com.com.hubcity.model.MapObj;
import com.com.hubcity.model.MyCouponModel;
import com.com.hubcity.model.MyCouponObj;
import com.com.hubcity.model.UserInfoModel;
import com.com.hubcity.model.UserInfoRequestModel;
import com.com.hubcity.model.UserInfoUpdateModel;
import com.hubcity.android.commonUtil.AddCouponDetails;
import com.hubcity.android.commonUtil.BookMarkModel;
import com.hubcity.android.commonUtil.ClaimGetDetails;
import com.hubcity.android.commonUtil.ClaimModel;
import com.hubcity.android.commonUtil.CouponDetails;
import com.hubcity.android.commonUtil.GetAddCouponObject;
import com.hubcity.android.commonUtil.GetClaimObject;
import com.hubcity.android.commonUtil.GetCouponDetailsObject;
import com.hubcity.android.commonUtil.GetRedeemObject;
import com.hubcity.android.commonUtil.MultipleBandModel;
import com.hubcity.android.commonUtil.RedeemGetDetails;
import com.hubcity.android.commonUtil.ScrollingPageModel;
import com.hubcity.android.model.HubCitiSlideModel;
import com.hubcity.android.model.UpdateUserResponseModel;
import com.hubcity.android.screens.ClaimRequestObj;
import com.hubcity.android.screens.MultipleBandObj;
import com.scansee.newsfirst.BookMarkObj;
import com.scansee.newsfirst.NewsSideMenu;
import com.scansee.newsfirst.ScrollingObj;
import com.scansee.newsfirst.SideMenuModel;
import com.scansee.newsfirst.SideMenuObject;
import com.scansee.newsfirst.SubCatObj;
import com.scansee.newsfirst.UpdateModel;
import com.scansee.newsfirst.model.GetCombinationTempModel;
import com.scansee.newsfirst.model.NewsDetailsModel;
import com.scansee.newsfirst.model.SubPageModel;
import com.scansee.newsfirst.model.TileTemplateModel;
import com.scansee.newsfirst.model.TileTemplateRequestModel;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by sharanamma on 1/8/2016.
 */
public interface RestInterface {
    @Headers({
            "Content-Type: application/json",
            "Accept-Language: en-US,en;",
            "Accept-Charset: ISO-8859-1,utf-8;",
            "Accept: application/json;",
    })
    @POST("/firstuse/getuserinfo_json")
    void getUserInfo(@Body UserInfoRequestModel userInfoRequest, Callback<UserInfoModel> userInfo);

    @POST("/firstuse/v2/updateuserinfo_json")
    void updateUserInfo(@Body UserInfoUpdateModel userInfo, Callback<UpdateUserResponseModel> reponse);

    @POST("/band/getnewscombtemplate")
    void getCombinationTemplate(@Body CombinationalTempRequest templateInfo, Callback<GetCombinationTempModel> combinationTemplateInfo);

    @GET("/band/getnewsdetail")
    void getNewsDetail(@Query("newsID") int newsId, Callback<NewsDetailsModel> callback);

    @POST("/band/getnewstopnavigationmenu")
    void getBookMarkInfo(@Body BookMarkModel bookMarkRequest, Callback<BookMarkObj> callback);

    @POST("/band/getnewsscroltemplate")
    void getScrollingInfo(@Body ScrollingPageModel scrollingObj, Callback<ScrollingObj> callback);

    @POST("/band/getnewscategriesandhubfunctn")
    void getSideMenuInfo(@Body SideMenuModel sideMenuRequest, Callback<BookMarkObj> callback);

    @POST("/band/updatenewscategriesandhubfunctnorder")
    void getUpdateInfo(@Body UpdateModel updateRequest, Callback<BookMarkObj> callback);

    @POST("/firstuse/v2/hubsidemenudisplay")
    void getsideMenuInfo(@Body NewsSideMenu sideMenuRequest, Callback<SideMenuObject> callback);

    @POST("/band/getnewsubcategories")
    void getnewsubcategories(@Body SubPageModel subCatRequest, Callback<SubCatObj> callback);

    @POST("/band/getnewsblocktemplate")
    void getTileTemplateCategories(@Body TileTemplateRequestModel subCatRequest, Callback<TileTemplateModel> callback);

    @GET("/firstuse/getslides")
    void getHubCitiSlides(@Query("hubCitiKey") String hubCitiKey, Callback<HubCitiSlideModel> callback);

    @POST("/thislocation/claimyourbusiness")
    void setClaimRequest(@Body ClaimModel detailsRequest, Callback<ClaimRequestObj> callback);

    @POST("/band/geteventbandslist")
    void setMulBand(@Body MultipleBandModel multipleReq, Callback<MultipleBandObj> callback);

    @POST("/thislocation/getclaimyourbusiness")
    void getCliam(@Body ClaimGetDetails request, Callback<GetClaimObject> callback);

    @POST("/gallery/getallcoupbylocjson")
    void getCoupon(@Body CouponModel request, Callback<CouponObj> callback);

    @POST("/gallery/getmyaccountsjson")
    void getMyCoupon(@Body MyCouponModel request, Callback<MyCouponObj> callback);

    @POST("/gallery/getcouponsmaplocjson")
    void getMap(@Body MapModel request, Callback<MapObj> callback);


    @POST("/gallery/userredeemcouponjson")
    void getRedeemCliam(@Body RedeemGetDetails redeemrequest, Callback<GetRedeemObject> callback);

    @POST("/gallery/addcouponjson")
    void getAddCouponClaim(@Body AddCouponDetails addcouponrequest, Callback<GetAddCouponObject> callback);

    @POST("/thislocation/getcoupondetailjson")
    void getCouponClaim(@Body CouponDetails coupondetailsrequest, Callback<GetCouponDetailsObject> callback);
}
