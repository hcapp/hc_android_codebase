package com.com.hubcity.model;

import com.hubcity.android.businessObjects.BottomButtonBO;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 12/19/2016.
 */
public class MyCouponObj {
    private String responseCode;

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public int getMaxCnt() {
        return maxCnt;
    }

    public void setMaxCnt(int maxCnt) {
        this.maxCnt = maxCnt;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public int getMaxRowNum() {
        return maxRowNum;
    }

    public void setMaxRowNum(int maxRowNum) {
        this.maxRowNum = maxRowNum;
    }

    public ArrayList<BottomButtonBO> getBottomBtnList() {
        return bottomBtnList;
    }

    public void setBottomBtnList(ArrayList<BottomButtonBO> bottomBtnList) {
        this.bottomBtnList = bottomBtnList;
    }


    private String responseText;
    private int maxCnt;
    private int nextPage;
    private int maxRowNum;
    private ArrayList<BottomButtonBO>bottomBtnList;


    public ArrayList<CouponList> getCouponList() {
        return couponList;
    }

    public void setCouponList(ArrayList<CouponList> couponList) {
        this.couponList = couponList;
    }

    private ArrayList<CouponList >couponList;


}
