package com.com.hubcity.model;

import java.util.ArrayList;

/**
 * Created by sharanamma on 1/8/2016.
 */
public class UserInfoModel {

    private ArrayList<UserInfoFieldsModel> listUserDetails;
    private ArrayList<UserEducationOptionsModel> arEducationList;
    private ArrayList<UserMaritalOptionsModel> arMaritalList;
    private ArrayList<UserIncomeRangeOptionsModel> arIncomeRangeList;

    public ArrayList<UserIncomeRangeOptionsModel> getArIncomeRangeList() {
        return arIncomeRangeList;
    }

    public void setArIncomeRangeList(ArrayList<UserIncomeRangeOptionsModel> arIncomeRangeList) {
        this.arIncomeRangeList = arIncomeRangeList;
    }

    public ArrayList<UserInfoFieldsModel> getListUserDetails() {
        return listUserDetails;
    }

    public void setListUserDetails(ArrayList<UserInfoFieldsModel> listUserDetails) {
        this.listUserDetails = listUserDetails;
    }

    public ArrayList<UserEducationOptionsModel> getArEducationList() {
        return arEducationList;
    }

    public void setArEducationList(ArrayList<UserEducationOptionsModel> arEducationList) {
        this.arEducationList = arEducationList;
    }

    public ArrayList<UserMaritalOptionsModel> getArMaritalList() {
        return arMaritalList;
    }

    public void setArMaritalList(ArrayList<UserMaritalOptionsModel> arMaritalList) {
        this.arMaritalList = arMaritalList;
    }
 }
