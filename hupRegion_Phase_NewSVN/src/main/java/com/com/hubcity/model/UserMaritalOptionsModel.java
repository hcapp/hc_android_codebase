package com.com.hubcity.model;

/**
 * Created by sharanamma on 1/8/2016.
 */
public class UserMaritalOptionsModel {

    private String mStatusId;
    private String mStatusName;

    public String getmStatusId() {
        return mStatusId;
    }

    public void setmStatusId(String mStatusId) {
        this.mStatusId = mStatusId;
    }

    public String getmStatusName() {
        return mStatusName;
    }

    public void setmStatusName(String mStatusName) {
        this.mStatusName = mStatusName;
    }


}
