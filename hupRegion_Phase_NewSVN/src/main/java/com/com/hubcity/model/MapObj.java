package com.com.hubcity.model;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 12/28/2016.
 */
public class MapObj {
    public ArrayList<CouponMapLocs> getCouponMapLocs() {
        return couponMapLocs;
    }

    public void setCouponMapLocs(ArrayList<CouponMapLocs> couponMapLocs) {
        this.couponMapLocs = couponMapLocs;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    private String responseCode;
    private String responseText;
    private ArrayList<CouponMapLocs>couponMapLocs;
}
