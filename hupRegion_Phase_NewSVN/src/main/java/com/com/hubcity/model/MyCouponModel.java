package com.com.hubcity.model;

/**
 * Created by subramanya.v on 12/19/2016.
 */
public class MyCouponModel {
    private final String userId;
    private final String hubCitiId;
    private final String sortColumn;
    private final String postalCode;
    private final String type;
    private final String lastVisitedNo;
    private final String sortOrder;
    private final String searchKey;
    private final String latitude;
    private final String longitude;
    private final String catIds;
    private final String cityIds;

    public MyCouponModel(String userId, String hubCitiId, String sortColumn, String postalCode, String type, String lastVisitedNo,
                       String sortOrder, String searchKey, String Latitude, String Longitude, String catIds, String cityIds) {
        this.userId = userId;
        this.hubCitiId = hubCitiId;
        this.sortColumn = sortColumn;
        this.postalCode = postalCode;
        this.type = type;
        this.lastVisitedNo = lastVisitedNo;
        this.sortOrder = sortOrder;
        this.searchKey = searchKey;
        this.latitude = Latitude;
        this.longitude = Longitude;
        this.catIds = catIds;
        this.cityIds = cityIds;
    }
}
