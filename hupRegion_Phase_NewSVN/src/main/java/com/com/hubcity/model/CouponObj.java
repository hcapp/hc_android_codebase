package com.com.hubcity.model;

import com.hubcity.android.businessObjects.BottomButtonBO;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 12/19/2016.
 */
public class CouponObj {
    private String responseCode;
    private String responseText;
    private int maxCnt;
    private int nextPage;
    private int maxRowNum;

    public ArrayList<BottomButtonBO> getBottomBtnList() {
        return bottomBtnList;
    }

    public void setBottomBtnList(ArrayList<BottomButtonBO> bottomBtnList) {
        this.bottomBtnList = bottomBtnList;
    }

    private ArrayList<BottomButtonBO>bottomBtnList;

    public ArrayList<RetailerDetails> getFeaturedCouponsList() {
        return featuredCouponsList;
    }

    public void setFeaturedCouponsList(ArrayList<RetailerDetails> featuredCouponsList) {
        this.featuredCouponsList = featuredCouponsList;
    }

    public ArrayList<RetailerDetails> getNonFeaturedCouponsList() {
        return nonFeaturedCouponsList;
    }

    public void setNonFeaturedCouponsList(ArrayList<RetailerDetails> nonFeaturedCouponsList) {
        this.nonFeaturedCouponsList = nonFeaturedCouponsList;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    private ArrayList<RetailerDetails> featuredCouponsList;
    private ArrayList<RetailerDetails> nonFeaturedCouponsList;
    private String Label;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public int getMaxCnt() {
        return maxCnt;
    }

    public void setMaxCnt(int maxCnt) {
        this.maxCnt = maxCnt;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public int getMaxRowNum() {
        return maxRowNum;
    }

    public void setMaxRowNum(int maxRowNum) {
        this.maxRowNum = maxRowNum;
    }


}
