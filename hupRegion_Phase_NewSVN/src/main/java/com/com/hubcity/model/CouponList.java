package com.com.hubcity.model;

import com.hubcity.android.screens.RetailerDetail;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 12/30/2016.
 */
public class CouponList {
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<RetailerDetails> getList() {
        return list;
    }

    public void setList(ArrayList<RetailerDetails> list) {
        this.list = list;
    }

    private String type;
    private ArrayList<RetailerDetails>list;
}
