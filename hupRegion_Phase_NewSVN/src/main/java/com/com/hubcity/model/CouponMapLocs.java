package com.com.hubcity.model;

/**
 * Created by subramanya.v on 12/28/2016.
 */
public class CouponMapLocs {
    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getRetailLocationId() {
        return retailLocationId;
    }

    public void setRetailLocationId(String retailLocationId) {
        this.retailLocationId = retailLocationId;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRetLatitude() {
        return retLatitude;
    }

    public void setRetLatitude(String retLatitude) {
        this.retLatitude = retLatitude;
    }

    public String getRetLongitude() {
        return retLongitude;
    }

    public void setRetLongitude(String retLongitude) {
        this.retLongitude = retLongitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private String couponId;
    private String couponName;
    private String retailLocationId;
    private String distance;
    private String retLatitude;
    private String retLongitude;
    private String location;
}
