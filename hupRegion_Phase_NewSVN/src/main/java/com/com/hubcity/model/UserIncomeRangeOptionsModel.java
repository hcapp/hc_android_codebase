package com.com.hubcity.model;

/**
 * Created by sharanamma on 1/12/2016.
 */
public class UserIncomeRangeOptionsModel {
    private int icRangeId;

    public int getIcRangeId() {
        return icRangeId;
    }

    public void setIcRangeId(int icRangeId) {
        this.icRangeId = icRangeId;
    }

    public String getIcRangeName() {
        return icRangeName;
    }

    public void setIcRangeName(String icRangeName) {
        this.icRangeName = icRangeName;
    }

    private String icRangeName;
}
