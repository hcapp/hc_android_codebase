package com.com.hubcity.model;

/**
 * Created by sharanamma on 1/12/2016.
 */
public class UserInfoUpdateModel {
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

/*    public String getUnivId() {
        return univId;
    }

    public void setUnivId(String univId) {
        this.univId = univId;
    }*/

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIncomeRangeId() {
        return incomeRangeId;
    }

    public void setIncomeRangeId(String incomeRangeId) {
        this.incomeRangeId = incomeRangeId;
    }

    public String getMartialStatusId() {
        return martialStatusId;
    }

    public void setMartialStatusId(String martialStatusId) {
        this.martialStatusId = martialStatusId;
    }

    public String getEducatonLevelId() {
        return educatonLevelId;
    }

    public void setEducatonLevelId(String educatonLevelId) {
        this.educatonLevelId = educatonLevelId;
    }

    String userId;
    String firstName;
    String lastName;
    String postalCode;
    String gender;
    String dob;
    String phoneNum;
    String deviceId;
    //String univId;
    String email;
    String incomeRangeId;
    String martialStatusId;
    String educatonLevelId;

    public String getHubCitiId()
    {
        return hubCitiId;
    }

    public void setHubCitiId(String hubCitiId)
    {
        this.hubCitiId = hubCitiId;
    }

    String hubCitiId;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String image;
}
