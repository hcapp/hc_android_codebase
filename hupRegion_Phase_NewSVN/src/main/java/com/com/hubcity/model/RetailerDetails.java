package com.com.hubcity.model;

/**
 * Created by subramanya.v on 12/19/2016.
 */
public class RetailerDetails {
    private int couponId;
    private String couponName;
    private String couponImagePath;
    private String bannerTitle;
    private int counts;
    private int retId;
    private String retName;
    private String distance;





    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponImagePath() {
        return couponImagePath;
    }

    public void setCouponImagePath(String couponImagePath) {
        this.couponImagePath = couponImagePath;
    }

    public String getBannerTitle() {
        return bannerTitle;
    }

    public void setBannerTitle(String bannerTitle) {
        this.bannerTitle = bannerTitle;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public int getRetId() {
        return retId;
    }

    public void setRetId(int retId) {
        this.retId = retId;
    }

    public String getRetName() {
        return retName;
    }

    public void setRetName(String retName) {
        this.retName = retName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public String getCouponListId() {
        return couponListId;
    }

    public void setCouponListId(String couponListId) {
        this.couponListId = couponListId;
    }

    private int rowNum;
    private String couponListId;


}
