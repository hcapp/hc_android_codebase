package com.com.hubcity.model;

/**
 * Created by sharanamma on 1/8/2016.
 */
public class UserEducationOptionsModel {
    private String educatLevelId;
    private String educatLevelName;

    public String getEducatLevelName() {
        return educatLevelName;
    }

    public void setEducatLevelName(String educatLevelName) {
        this.educatLevelName = educatLevelName;
    }

    public String getEducatLevelId() {
        return educatLevelId;
    }

    public void setEducatLevelId(String educatLevelId) {
        this.educatLevelId = educatLevelId;
    }
}
