package com.com.hubcity.model;

/**
 * Created by sharanamma on 06-06-2016.
 */
public class CombinationalTempRequest {
    private final String userId;
    private final String hubCitiId;
    private final int lowerLimit;
    private final String dateCreated;
    private final String linkId;
    private final String level;

    public CombinationalTempRequest(String userId, String hubCitiId,int lowerLimit,String dateCreated,String linkID,String level) {
        this.userId = userId;
        this.hubCitiId = hubCitiId;
        this.lowerLimit = lowerLimit;
        this.dateCreated = dateCreated;
        this.linkId = linkID;
        this.level = level;
    }
}
