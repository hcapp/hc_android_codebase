package com.com.hubcity.model;

/**
 * Created by sharanamma on 1/8/2016.
 */
public class UserInfoRequestModel {

    private final String userId;
    private final String hubCitiId;

    public UserInfoRequestModel(String userId, String hubCitiId) {
         this.userId = userId;
         this.hubCitiId = hubCitiId;
    }

}
