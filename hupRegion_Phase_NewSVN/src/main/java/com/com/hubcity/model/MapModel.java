package com.com.hubcity.model;

/**
 * Created by subramanya.v on 12/28/2016.
 */
public class MapModel {


    private final String userId;
    private final String hubCitiId;
    private final String postalCode;
    private final String couponIds;

    public MapModel(String userId, String hubCitiId, String postalCode, String couponIds) {
        this.userId = userId;
        this.hubCitiId = hubCitiId;
        this.postalCode = postalCode;
        this.couponIds = couponIds;

    }


}
