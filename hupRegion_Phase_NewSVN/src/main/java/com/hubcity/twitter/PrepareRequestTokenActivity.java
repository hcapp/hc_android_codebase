package com.hubcity.twitter;

import com.hubcity.android.commonUtil.HubCityLogger;
import com.hubcity.android.screens.MenuPropertiesActivity;
import com.hubcity.android.screens.ShareInformation;
import com.hubcity.android.screens.UserTrackingTask;
import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Prepares a OAuthConsumer and OAuthProvider
 * 
 * OAuthConsumer is configured with the consumer key & consumer secret.
 * OAuthProvider is configured with the 3 OAuth endpoints.
 * 
 * Execute the OAuthRequestTokenTask to retrieve the request, and authorize the
 * request.
 * 
 * After the request is authorized, a callback is made here.
 * 
 */
public class PrepareRequestTokenActivity extends Activity {

	final String tag = getClass().getName();
	private OAuthConsumer consumer;
	private OAuthProvider provider;
	boolean result = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			this.consumer = new CommonsHttpOAuthConsumer(
					Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
			this.provider = new CommonsHttpOAuthProvider(Constants.REQUEST_URL,
					Constants.ACCESS_URL, Constants.AUTHORIZE_URL);
			this.provider.setOAuth10a(true);
			Log.d("CONSUMER_KEY" ,Constants.CONSUMER_KEY);
			Log.d("CONSUMER_SECRET" ,Constants.CONSUMER_SECRET);
		} catch (Exception e) {
			HubCityLogger.e(tag, "Error creating consumer / provider", e);
		}

		HubCityLogger.i(tag, "Starting task to retrieve request token.");
		new OAuthRequestTokenTask(this, consumer, provider).execute();
	}

	/**
	 * Called when the OAuthRequestTokenTask finishes (user has authorized the
	 * request token). The callback URL will be intercepted here.
	 */
	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		final Uri uri = intent.getData();
		if (uri != null
				&& uri.getScheme().equals(Constants.OAUTH_CALLBACK_SCHEME)) {
			HubCityLogger.i(tag, "Callback received : " + uri);
			HubCityLogger.i(tag, "Retrieving Access Token");
			new RetrieveAccessTokenTask(this, consumer, provider, prefs)
					.execute(uri);
			finish();
		}
	}

	@Override
	public void finish() {
		setResult(6252);
		super.finish();
	}

	public class RetrieveAccessTokenTask extends AsyncTask<Uri, Void, Boolean> {

		Context context;
		private OAuthProvider provider;
		private OAuthConsumer consumer;
		private SharedPreferences prefs;
		String exceptionMsg = null;

		public RetrieveAccessTokenTask(Context context, OAuthConsumer consumer,
				OAuthProvider provider, SharedPreferences prefs) {
			this.context = context;
			this.consumer = consumer;
			this.provider = provider;
			this.prefs = prefs;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				Toast.makeText(getBaseContext(), "Tweet sent !",
						Toast.LENGTH_LONG).show();

				if (getIntent().hasExtra("shareDetails")) {
					String[] shareDetails = getIntent().getExtras()
							.getStringArray("shareDetails");
					UserTrackingTask userTracking = new UserTrackingTask(
							PrepareRequestTokenActivity.this, shareDetails[0],
							shareDetails[1], shareDetails[2], shareDetails[3],
							shareDetails[4], shareDetails[5], shareDetails[6]);
					userTracking.execute();
				}

			} else {
				if (exceptionMsg != null && exceptionMsg.contains("duplicate")) {
					Toast.makeText(getApplicationContext(),
							"Tweet not sent - Status Duplicate!",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getApplicationContext(),
							"Tweet not sent - Error!", Toast.LENGTH_LONG)
							.show();
				}
			}

			if (MenuPropertiesActivity.btnClosePopup != null) {
				MenuPropertiesActivity.btnClosePopup.performClick();

			} else {
				if (getIntent().hasExtra("module")
						&& "share".equals(getIntent().getExtras().getString(
								"module"))) {
					ShareInformation.btnClosePopup.performClick();
				}

			}
		}

		/**
		 * Retrieve the oauth_verifier, and store the oauth and
		 * oauth_token_secret for future API calls.
		 */
		@Override
		protected Boolean doInBackground(Uri... params) {
			final Uri uri = params[0];
			result = false;
			final String oauthVerifier = uri
					.getQueryParameter(OAuth.OAUTH_VERIFIER);

			try {
				provider.retrieveAccessToken(consumer, oauthVerifier);

				final Editor edit = prefs.edit();
				edit.putString(OAuth.OAUTH_TOKEN, consumer.getToken());
				edit.putString(OAuth.OAUTH_TOKEN_SECRET,
						consumer.getTokenSecret());
				edit.apply();

				String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
				String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");

				consumer.setTokenWithSecret(token, secret);

				result = executeAfterAccessTokenRetrieval();

				HubCityLogger.i(tag, "OAuth - Access Token Retrieved");

			} catch (Exception e) {
				HubCityLogger.e(tag, "OAuth - Access Token Retrieval Error", e);

			}

			return result;
		}

		private boolean executeAfterAccessTokenRetrieval() {
			Bundle intent = getIntent().getExtras();
			String msg = intent.getString("tweet_msg");
			String image = intent.getString("image");
			boolean result = false;
			try {
				TwitterUtils.sendTweet(prefs, msg,image);
				result = true;
			} catch (Exception e) {
				exceptionMsg = e.getMessage();
				if (e != null && "" + e != null) {
					if (e.getMessage().contains("duplicate")) {
						if (null != getApplicationContext()) {
							Toast.makeText(getApplicationContext(),
									"Tweet not sent - Status Duplicate!",
									Toast.LENGTH_LONG).show();
						}
					} else {
						if (null != getApplicationContext()) {
							Toast.makeText(getApplicationContext(),
									"Tweet not sent - Error!",
									Toast.LENGTH_LONG).show();
						}
					}
				}
				result = false;
			}
			return result;
		}
	}

}
