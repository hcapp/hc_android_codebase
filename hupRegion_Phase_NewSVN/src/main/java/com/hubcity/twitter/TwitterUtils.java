package com.hubcity.twitter;

import oauth.signpost.OAuth;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.hubcity.android.commonUtil.CommonConstants;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class TwitterUtils {

    public static boolean isAuthenticated(SharedPreferences prefs) {

        String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
        String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");

        AccessToken a = new AccessToken(token, secret);
        Twitter twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
                Constants.CONSUMER_SECRET);
        twitter.setOAuthAccessToken(a);

        try {
            twitter.getAccountSettings();
            return true;
        } catch (TwitterException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void sendTweet(SharedPreferences prefs, String msg, String image)
            throws Exception {
        new sendTweetAsy(prefs, msg, image).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private static class sendTweetAsy extends AsyncTask<Boolean, Boolean, Boolean> {
        private final SharedPreferences prefs;
        private final String msg;
        private final String image;

        public sendTweetAsy(SharedPreferences prefs, String msg, String image) {
            this.prefs = prefs;
            this.msg = msg;
            this.image = image;
        }

        @Override
        protected Boolean doInBackground(Boolean... params) {
            String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
            String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");

            AccessToken a = new AccessToken(token, secret);
            Twitter twitter = new TwitterFactory().getInstance();
            twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
                    Constants.CONSUMER_SECRET);
            twitter.setOAuthAccessToken(a);
            // the txt message
            StatusUpdate status = new StatusUpdate(msg);
            // set the image file as media with the message.
            if (image != null) {
                InputStream in = null;
                try {
                    in = new URL(image).openStream();

                    Bitmap imageBitMap = BitmapFactory.decodeStream(in);
                    if (imageBitMap == null) {
                        URL url = new URL(image);
                        HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
                        ucon.setInstanceFollowRedirects(false);
                        URL secondURL = new URL(ucon.getHeaderField("Location"));
                        imageBitMap = BitmapFactory.decodeStream(secondURL.openConnection().getInputStream());
                    }

                    File imageFile = CommonConstants.bitmapToFile(imageBitMap, 1);
                    status.setMedia(imageFile);
                    try {
                        twitter.updateStatus(status);
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    twitter.updateStatus(msg);
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
