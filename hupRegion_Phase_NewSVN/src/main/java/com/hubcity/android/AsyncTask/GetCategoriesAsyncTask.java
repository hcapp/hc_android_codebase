package com.hubcity.android.AsyncTask;

import android.os.AsyncTask;
import android.util.Log;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.model.CategoryDetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sharanamma on 21-12-2016.
 */
public class GetCategoriesAsyncTask extends AsyncTask<String, Void, String> {
    CategoryInterface listener;

    public GetCategoriesAsyncTask(CategoryInterface listener) {
        this.listener = listener;

    }

    public interface CategoryInterface {
        void onResultSuccess(ArrayList<String> categoryList);
    }

    JSONObject jsonObjectCategory = null;
    JSONArray jsonArrayMainCategory = null;
    ArrayList<String> categoryList = new ArrayList<>();

    @Override
    protected String doInBackground(String... params) {
        try {
            String url_user_location_catagory = Properties.url_local_server
                    + Properties.hubciti_version + "firstuse/getuserloccat";

            String url = url_user_location_catagory + "?userId="
                    + UrlRequestParams.getUid() + "&hubCitiId="
                    + UrlRequestParams.getHubCityId();
            Log.d("Category request :", url);
            jsonObjectCategory = ServerConnections
                    .requestWebService(url);
            Log.d("Category response :", jsonObjectCategory.toString());
            if (jsonObjectCategory != null) {

                jsonArrayMainCategory = jsonObjectCategory
                            .getJSONObject("CategoryDetails")
                            .getJSONArray(Constants.TAG_MAIN_PREFERENCES_MENU);

                if (jsonArrayMainCategory != null) {
                    for (int i = 0; i < jsonArrayMainCategory.length(); i++) {
                        JSONObject object = jsonArrayMainCategory.getJSONObject(i);
                        // CategoryDetails item = new CategoryDetails();
                        // item.setParCatId(object.getInt("parCatId"));
                        // item.setParCatName(object.getString("parCatName"));
                        categoryList.add(object.getString("parCatName"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        listener.onResultSuccess(categoryList);
    }
}
