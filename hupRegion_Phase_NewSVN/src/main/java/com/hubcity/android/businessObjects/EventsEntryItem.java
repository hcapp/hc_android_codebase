package com.hubcity.android.businessObjects;


public class EventsEntryItem implements Item {

	public final String startTime;
	public final String startDate;
	public final String eventListId;
	public final String eventId;
	public final String eventName;
	protected final String eventCatId;
	protected final String eventCatName;
	public final String distance;
	public final String imagePath;
	protected final String isOngoing;

	public EventsEntryItem(String startDate, String eventListId,
			String eventId, String eventName, String startTime,
			String eventCatId, String eventCatName, String distance,
			String imagePath, String isOngoing) {
		this.startTime = startTime;
		this.startDate = startDate;
		this.eventListId = eventListId;
		this.eventId = eventId;
		this.eventName = eventName;
		this.eventCatId = eventCatId;
		this.eventCatName = eventCatName;
		this.distance = distance;
		this.imagePath = imagePath;
		this.isOngoing = isOngoing;

	}

	@Override
	public boolean isSection() {
		return false;
	}

}