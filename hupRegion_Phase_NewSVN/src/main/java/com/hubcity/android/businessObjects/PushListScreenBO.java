package com.hubcity.android.businessObjects;

/**
 * Created by supriya.m on 1/4/2016.
 */
public class PushListScreenBO
{
	public String getNewsTitle()
	{
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle)
	{
		this.newsTitle = newsTitle;
	}

	private String newsTitle;
	private String newsLink;

	public String getNewsLink()
	{
		return newsLink;
	}

	public void setNewsLink(String newsLink)
	{
		this.newsLink = newsLink;
	}

	public String getDealId()
	{
		return dealId;
	}

	public void setDealId(String dealId)
	{
		this.dealId = dealId;
	}

	public String getDealRetId()
	{
		return dealRetId;
	}

	public void setDealRetId(String dealRetId)
	{
		this.dealRetId = dealRetId;
	}

	public String getDealRetLocID()
	{
		return dealRetLocID;
	}

	public void setDealRetLocID(String dealRetLocID)
	{
		this.dealRetLocID = dealRetLocID;
	}

	public String getDealName()
	{
		return dealName;
	}

	public void setDealName(String dealName)
	{
		this.dealName = dealName;
	}

	public String getDealType()
	{
		return dealType;
	}

	public void setDealType(String dealType)
	{
		this.dealType = dealType;
	}

	public String getSpecialUrl()
	{
		return specialUrl;
	}

	public void setSpecialUrl(String specialUrl)
	{
		this.specialUrl = specialUrl;
	}

	public String getEndTime()
	{
		return endTime;
	}

	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	private String dealId;
	private String dealRetId;
	private String dealRetLocID;
	private String dealName;
	private String dealType;
	private String endDate;
	private String endTime;
	private String specialUrl;

}
