package com.hubcity.android.businessObjects;

import java.io.Serializable;

public class FilterOptionBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String filterGroupName;
	private String filterName;
	private String filterId;
	private boolean isChecked;

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public String getFilterId() {
		return filterId;
	}

	public void setFilterId(String filterId) {
		this.filterId = filterId;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getFilterGroupName() {
		return filterGroupName;
	}

	public void setFilterGroupName(String filterGroupName) {
		this.filterGroupName = filterGroupName;
	}
}
