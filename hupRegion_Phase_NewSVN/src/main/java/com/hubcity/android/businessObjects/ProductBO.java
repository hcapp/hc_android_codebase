package com.hubcity.android.businessObjects;

public class ProductBO {
	String productDescription;

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getManufacturersName() {
		return manufacturersName;
	}

	public void setManufacturersName(String manufacturersName) {
		this.manufacturersName = manufacturersName;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getSuggestedRetailPrice() {
		return suggestedRetailPrice;
	}

	public void setSuggestedRetailPrice(String suggestedRetailPrice) {
		this.suggestedRetailPrice = suggestedRetailPrice;
	}

	public String getProductWeight() {
		return productWeight;
	}

	public void setProductWeight(String productWeight) {
		this.productWeight = productWeight;
	}

	public String getProductExpDate() {
		return productExpDate;
	}

	public void setProductExpDate(String productExpDate) {
		this.productExpDate = productExpDate;
	}

	public String getWeightUnits() {
		return weightUnits;
	}

	public void setWeightUnits(String weightUnits) {
		this.weightUnits = weightUnits;
	}

	public String getSaleListID() {
		return saleListID;
	}

	public void setSaleListID(String saleListID) {
		this.saleListID = saleListID;
	}

	public String getIsShopCartItem() {
		return isShopCartItem;
	}

	public void setIsShopCartItem(String isShopCartItem) {
		this.isShopCartItem = isShopCartItem;
	}

	public String getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(String regularPrice) {
		this.regularPrice = regularPrice;
	}

	public String getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}

	public String getRowNum() {
		return rowNum;
	}

	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	public String getproductId() {
		return productId;
	}

	public void setproductId(String productId) {
		this.productId = productId;
	}

	String manufacturersName;
	String imagePath;
	String modelNumber;
	String suggestedRetailPrice;
	String productWeight;
	String productExpDate;
	String weightUnits;
	String saleListID;
	String isShopCartItem;
	String regularPrice;
	String salePrice;
	String rowNum;
	String productId;
	String productName;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public ProductBO(String productDescription, String manufacturersName,
			String imagePath, String modelNumber, String suggestedRetailPrice,
			String productWeight, String productExpDate, String weightUnits,
			String saleListID, String isShopCartItem, String regularPrice,
			String salePrice, String rowNum, String productId,
			String productName)

	{
		this.productDescription = productDescription;
		this.manufacturersName = manufacturersName;
		this.imagePath = imagePath;
		this.modelNumber = modelNumber;
		this.suggestedRetailPrice = suggestedRetailPrice;
		this.productWeight = productWeight;
		this.productExpDate = productExpDate;
		this.weightUnits = weightUnits;
		this.saleListID = saleListID;
		this.isShopCartItem = isShopCartItem;
		this.regularPrice = regularPrice;
		this.salePrice = salePrice;
		this.rowNum = rowNum;
		this.productId = productId;
		this.productName = productName;
	}

}
