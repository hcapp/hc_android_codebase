package com.hubcity.android.businessObjects;

public class CuopnsDetailsBO {
	public String getViewableOnWeb() {
		return viewableOnWeb;
	}

	public String getCouponURL() {
		return couponURL;
	}

	public String getRowNum() {
		return rowNum;
	}

	public String getCouponId() {
		return couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public String getCouponDiscountType() {
		return couponDiscountType;
	}

	public String getCouponDiscountAmount() {
		return couponDiscountAmount;
	}

	public String getCouponDiscountPct() {
		return couponDiscountPct;
	}

	public String getCouponShortDescription() {
		return couponShortDescription;
	}

	public String getCouponLongDescription() {
		return couponLongDescription;
	}

	public String getCouponDateAdded() {
		return couponDateAdded;
	}

	public String getRetListId() {
		return retListId;
	}

	public String getCouponListId() {
		return couponListId;
	}

	public String getCouponStartDate() {
		return couponStartDate;
	}

	public String getCouponExpireDate() {
		return couponExpireDate;
	}

	public String getUsage() {
		return usage;
	}

	public String getCouponImagePath() {
		return couponImagePath;
	}

	String viewableOnWeb;
	String couponURL;
	String rowNum;
	String couponId;
	String couponName;
	String couponDiscountType;
	String couponDiscountAmount;
	String couponDiscountPct;
	String couponShortDescription;
	String couponLongDescription;
	String couponDateAdded;
	String retListId;
	String couponListId;
	String couponStartDate;
	String couponExpireDate;
	String usage;
	String couponImagePath;

	public boolean isFeatured() {
		return isFeatured;
	}

	public void setFeatured(boolean featured) {
		isFeatured = featured;
	}

	boolean isFeatured;

	public void setViewableOnWeb(String viewableOnWeb) {
		this.viewableOnWeb = viewableOnWeb;
	}

	public void setCouponURL(String couponURL) {
		this.couponURL = couponURL;
	}

	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public void setCouponDiscountType(String couponDiscountType) {
		this.couponDiscountType = couponDiscountType;
	}

	public void setCouponDiscountAmount(String couponDiscountAmount) {
		this.couponDiscountAmount = couponDiscountAmount;

	}

	public void setCouponDiscountPct(String couponDiscountPct) {
		this.couponDiscountPct = couponDiscountPct;
	}

	public void setCouponShortDescription(String couponShortDescription) {
		this.couponShortDescription = couponShortDescription;
	}

	public void setCouponLongDescription(String couponLongDescription) {
		this.couponLongDescription = couponLongDescription;
	}

	public void setCouponDateAdded(String couponDateAdded) {
		this.couponDateAdded = couponDateAdded;
	}

	public void setRetListId(String retListId) {
		this.retListId = retListId;
	}

	public void setCouponListId(String couponListId) {
		this.couponListId = couponListId;
	}

	public void setCouponStartDate(String couponStartDate) {
		this.couponStartDate = couponStartDate;
	}

	public void setCouponExpireDate(String couponExpireDate) {
		this.couponExpireDate = couponExpireDate;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public void setCouponImagePath(String couponImagePath) {
		this.couponImagePath = couponImagePath;
	}

	public CuopnsDetailsBO(String viewableOnWeb, String couponURL,
			String rowNum, String couponId, String couponName,
			String couponDiscountType, String couponDiscountAmount,
			String couponDiscountPct, String couponShortDescription,
			String couponLongDescription, String couponDateAdded,
			String retListId, String couponListId, String couponStartDate,
			String couponExpireDate, String usage, String couponImagePath,boolean isFeatured) {
		this.couponDiscountAmount = couponDiscountAmount;
		this.viewableOnWeb = viewableOnWeb;
		this.couponURL = couponURL;
		this.rowNum = rowNum;
		this.couponId = couponId;
		this.couponName = couponName;
		this.couponDiscountType = couponDiscountType;
		this.couponDiscountPct = couponDiscountPct;
		this.couponShortDescription = couponShortDescription;
		this.couponLongDescription = couponLongDescription;
		this.couponDateAdded = couponDateAdded;
		this.retListId = retListId;
		this.couponListId = couponListId;
		this.usage = usage;
		this.couponExpireDate = couponExpireDate;
		this.couponStartDate = couponStartDate;
		this.couponListId = couponListId;
		this.couponImagePath = couponImagePath;
		this.isFeatured = isFeatured;
	}
}