package com.hubcity.android.businessObjects;

public class SpecialRetailerEntryItem implements Item {

	public final String pageLink;
	protected final String specialsListId;
	protected final String rowNum;
	public final String sDescription;
	public final String pageId;
	public final String pageTitle;

	public SpecialRetailerEntryItem(String pageLink, String specialsListId,
			String rowNum, String sDescription, String pageId, String pageTitle) {
		this.pageLink = pageLink;
		this.specialsListId = specialsListId;
		this.rowNum = rowNum;
		this.sDescription = sDescription;
		this.pageId = pageId;
		this.pageTitle = pageTitle;

	}

	@Override
	public boolean isSection() {
		return false;
	}

}