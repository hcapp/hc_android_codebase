package com.hubcity.android.businessObjects;

import java.io.Serializable;
import java.util.ArrayList;

public class FilterArdSortScreenBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String filterName;
	private String headerName;
	private boolean isHeader;
	private boolean isChecked;
	private ArrayList<FilterOptionBO> arrFilterOptionBOs = new ArrayList<>();

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public boolean isHeader() {
		return isHeader;
	}

	public void setHeader(boolean isHeader) {
		this.isHeader = isHeader;
	}

	public ArrayList<FilterOptionBO> getArrFilterOptionBOs() {
		return arrFilterOptionBOs;
	}

	public void setArrFilterOptionBOs(
			ArrayList<FilterOptionBO> arrFilterOptionBOs) {
		this.arrFilterOptionBOs = arrFilterOptionBOs;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getHeaderName() {
		return headerName;
	}

	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

}
