package com.hubcity.android.businessObjects;

public class HotDealsDetailsBO {

	String city;
	String rowNumber;
	String apiPartnerId;
	String apiPartnerName;
	String hotDealName;
	String hotDealId;
	String hotDealImagePath;
	String hDshortDescription;
	String hDPrice;
	String hDSalePrice;
	String hotDealListId;
	String hDStartDate;
	String hDEndDate;

	public HotDealsDetailsBO(String city, String rowNumber,
			String apiPartnerId, String apiPartnerName, String hotDealName,
			String hotDealId, String hotDealImagePath,
			String hDshortDescription, String hDPrice, String hDSalePrice,
			String hotDealListId, String hDStartDate, String hDEndDate) {

		this.city = city;
		this.rowNumber = rowNumber;
		this.apiPartnerId = apiPartnerId;
		this.apiPartnerName = apiPartnerName;
		this.hotDealName = hotDealName;
		this.hotDealId = hotDealId;
		this.hotDealImagePath = hotDealImagePath;
		this.hDshortDescription = hDshortDescription;
		this.hDPrice = hDPrice;
		this.hDSalePrice = hDSalePrice;
		this.hotDealListId = hotDealListId;
		this.hDStartDate = hDStartDate;
		this.hDEndDate = hDEndDate;
	}

	public String gethDStartDate() {
		return hDStartDate;
	}

	public String gethDEndDate() {
		return hDEndDate;
	}

	public String getCity() {
		return city;
	}

	public String getRowNumber() {
		return rowNumber;
	}

	public String getApiPartnerId() {
		return apiPartnerId;
	}

	public String getApiPartnerName() {
		return apiPartnerName;
	}

	public String getHotDealName() {
		return hotDealName;
	}

	public String getHotDealId() {
		return hotDealId;
	}

	public String getHotDealImagePath() {
		return hotDealImagePath;
	}

	public String gethDshortDescription() {
		return hDshortDescription;
	}

	public String gethDPrice() {
		return hDPrice;
	}

	public String gethDSalePrice() {
		return hDSalePrice;
	}

	public String getHotDealListId() {
		return hotDealListId;
	}

}
