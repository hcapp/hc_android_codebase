package com.hubcity.android.businessObjects;

import com.hubcity.android.screens.BottomButtons;

/**
 * Holds Bottom button related actions
 * 
 * @author rekha_p
 * 
 */
public class BottomButtonInfoSingleton {

	public BottomButtons bottomButtons;
	static BottomButtonInfoSingleton bottomButtonInfoSingleton;

	private BottomButtonInfoSingleton(BottomButtons bottomButtons) {
		this.bottomButtons = bottomButtons;
	}

	/**
	 * clears the instance of the class
	 */
	static public void clearBottomButtonInfoSingleton() {
		if (bottomButtonInfoSingleton != null) {
			bottomButtonInfoSingleton = null;
		}
	}

	/**
	 * gets singleton of the class
	 * 
	 * @return
	 */
	public static BottomButtonInfoSingleton getBottomButtonsSingleton() {
		if (bottomButtonInfoSingleton != null) {
			return bottomButtonInfoSingleton;
		} else {
			return null;
		}
	}

	/**
	 * creates singleton for the class
	 * 
	 * @param bottomButtons
	 * @return
	 */
	public static BottomButtonInfoSingleton createBottomButtonsSingleton(
			BottomButtons bottomButtons) {
		if (bottomButtonInfoSingleton != null) {
			return bottomButtonInfoSingleton;
		} else {
			bottomButtonInfoSingleton = new BottomButtonInfoSingleton(
					bottomButtons);
			return bottomButtonInfoSingleton;
		}
	}
}
