package com.hubcity.android.businessObjects;

public class SuccessfulRegistrationSingleton {

	static private String bkgndColor;
	static private String fontColor;
	static private String description;
	static private String btnColor;
	static private String btnFontColor;
	static private String title;

	private SuccessfulRegistrationSingleton(String _bkgndColor,
			String _fontColor, String _title, String _description,
			String _btnColor, String _btnFontColor) {
		bkgndColor = _bkgndColor;
		title = _title;
		fontColor = _fontColor;
		description = _description;
		btnColor = _btnColor;
		btnFontColor = _btnFontColor;

	}

	static public void clearSuccessfulRegistrationSingleton() {
		if (mSuccessfulRegistrationSingleton != null) {
			mSuccessfulRegistrationSingleton = null;
		}
	}

	static public SuccessfulRegistrationSingleton getSuccessfulRegistrationSingleton() {

		if (mSuccessfulRegistrationSingleton != null) {
			return mSuccessfulRegistrationSingleton;
		} else {
			return null;
		}
	}

	static SuccessfulRegistrationSingleton mSuccessfulRegistrationSingleton;

	static public SuccessfulRegistrationSingleton getSuccessfulRegistrationSingleton(
			String bkgndColor, String fontColor, String title,
			String description, String btnColor, String btnFontColor) {
		if (mSuccessfulRegistrationSingleton == null) {
			mSuccessfulRegistrationSingleton = new SuccessfulRegistrationSingleton(
					bkgndColor, fontColor, title, description, btnColor,
					btnFontColor);
			return mSuccessfulRegistrationSingleton;
		} else {
			return mSuccessfulRegistrationSingleton;
		}
	}

	public String getBkgndColor() {
		return bkgndColor;
	}

	public String getFontColor() {
		return fontColor;
	}

	public String getDescription() {
		return description;
	}

	public String getBtnColor() {
		return btnColor;
	}

	public String getBtnFontColor() {
		return btnFontColor;
	}

	public String getTitle() {
		return title;
	}

}