package com.hubcity.android.businessObjects;

import android.os.Parcel;

import android.os.Parcelable;

public class MainMenuBO implements Comparable<MainMenuBO>, Parcelable {
	protected String mItemId;
	protected String mItemName;
	protected String linkTypeId;
	protected String linkTypeName;
	protected int position;
	protected String mItemImgUrl;
	protected String linkId;
	protected String level;
	protected String mBtnColor;
	protected String mBtnFontColor;
	protected String smBtnColor;
	protected String smBtnFontColor;
	protected String mGrpBkgrdColor;
	protected String mGrpFntColor;
	protected String sGrpBkgrdColor;
	protected String sGrpFntColor;
	public String mShapeName;
	protected String mFontColor;
	protected String smFontColor;
	protected String mBkgrdImage;

	public String getmBkgrdColor()
	{
		return mBkgrdColor;
	}

	public void setmBkgrdColor(String mBkgrdColor)
	{
		this.mBkgrdColor = mBkgrdColor;
	}

	protected String mBkgrdColor;
	// Added by Supriya to differnciate between submenu group color and
	// background color
	protected String smBkgrdColor;

	public String getSmFontColor() {
		return smFontColor;
	}

	public void setSmFontColor(String smFontColor) {
		this.smFontColor = smFontColor;
	}

	public String getmFontColor() {
		return mFontColor;
	}

	public void setmFontColor(String mFontColor) {
		this.mFontColor = mFontColor;
	}

	public String getmShapeName() {
		return mShapeName;
	}

	public void setmShapeName(String mShapeName) {
		this.mShapeName = mShapeName;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getmBtnColor() {
		return mBtnColor;
	}

	public String getmBtnFontColor() {
		return mBtnFontColor;
	}

	public String getSmBtnColor() {
		return smBtnColor;
	}

	public String getSmBtnFontColor() {
		return smBtnFontColor;
	}

	public void setmBtnColor(String mBtnColor) {
		this.mBtnColor = mBtnColor;
	}

	public void setmBtnFontColor(String mBtnFontColor) {
		this.mBtnFontColor = mBtnFontColor;
	}

	public void setSmBtnColor(String smBtnColor) {
		this.smBtnColor = smBtnColor;
	}

	public void setSmBtnFontColor(String smBtnFontColor) {
		this.smBtnFontColor = smBtnFontColor;
	}

	public void setmBkgrdImage(String mBkgrdImage) {
		this.mBkgrdImage = mBkgrdImage;
	}
	
	public String getmBkgrdImage() {
		return mBkgrdImage;
	}

	public MainMenuBO(String mItemId, String mItemName, String linkTypeId,
			String linkTypeName, int position, String mItemImgUrl,
			String linkId, String mBtnColor, String mBtnFontColor,
			String smBtnColor, String smBtnFontColor, String level,
			String mGrpFntColor, String sGrpFntColor, String mGrpBkgrdColor,
			String sGrpBkgrdColor, String mShapeName, String mBkgrdImage, String mBkgrdColor) {

		this.mItemName = mItemName;
		this.linkTypeId = linkTypeId;
		this.linkTypeName = linkTypeName;
		this.mItemId = mItemId;
		this.position = position;
		this.mItemImgUrl = mItemImgUrl;
		this.linkId = linkId;
		this.level = level;
		this.mShapeName = mShapeName;
		this.mBtnColor = mBtnColor;
		this.mBtnFontColor = mBtnFontColor;
		this.smBtnColor = smBtnColor;
		this.smBtnFontColor = smBtnFontColor;
		this.mGrpFntColor = mGrpFntColor;
		this.sGrpFntColor = sGrpFntColor;
		this.mGrpBkgrdColor = mGrpBkgrdColor;
		this.sGrpBkgrdColor = sGrpBkgrdColor;
		this.mBkgrdImage = mBkgrdImage;
		this.mBkgrdColor = mBkgrdColor;
	}

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}

	public String getmItemId() {
		return mItemId;
	}

	public String getmItemName() {
		return mItemName;
	}

	public String getLinkTypeId() {
		return linkTypeId;
	}

	public String getLinkTypeName() {
		return linkTypeName;
	}

	public int getPosition() {
		return position;
	}

	public String getmItemImgUrl() {
		return mItemImgUrl;
	}

	public void setmItemId(String mItemId) {
		this.mItemId = mItemId;
	}

	public void setmItemName(String mItemName) {
		this.mItemName = mItemName;
	}

	public void setLinkTypeId(String linkTypeId) {
		this.linkTypeId = linkTypeId;
	}

	public void setLinkTypeName(String linkTypeName) {
		this.linkTypeName = linkTypeName;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setmItemImgUrl(String mItemImgUrl) {
		this.mItemImgUrl = mItemImgUrl;
	}

	public String getmGrpBkgrdColor() {
		return mGrpBkgrdColor;
	}

	public void setmGrpBkgrdColor(String mGrpBkgrdColor) {
		this.mGrpBkgrdColor = mGrpBkgrdColor;
	}

	public String getmGrpFntColor() {
		return mGrpFntColor;
	}

	public void setmGrpFntColor(String mGrpFntColor) {
		this.mGrpFntColor = mGrpFntColor;
	}

	public String getsGrpBkgrdColor() {
		return sGrpBkgrdColor;
	}

	public void setsGrpBkgrdColor(String sGrpBkgrdColor) {
		this.sGrpBkgrdColor = sGrpBkgrdColor;
	}

	public String getsGrpFntColor() {
		return sGrpFntColor;
	}

	public void setsGrpFntColor(String sGrpFntColor) {
		this.sGrpFntColor = sGrpFntColor;
	}

	@Override
	public String toString() {
		return this.mItemName;
	}

	@Override
	public int compareTo(MainMenuBO another) {
		return this.mItemName.compareTo(another.mItemName);
	}

	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeInt(position);
		dest.writeString(mItemId);
		dest.writeString(linkTypeId);
		dest.writeString(linkTypeName);
		dest.writeString(mItemImgUrl);
		dest.writeString(mItemName);
		dest.writeString(linkId);
		dest.writeString(mShapeName);
		dest.writeString(mBkgrdImage);

		dest.writeString(mBtnColor);
		dest.writeString(mBtnFontColor);
		dest.writeString(smBtnColor);
		dest.writeString(smBtnFontColor);
		dest.writeString(level);

		dest.writeString(mGrpBkgrdColor);
		dest.writeString(mGrpFntColor);
		dest.writeString(sGrpBkgrdColor);
		dest.writeString(sGrpFntColor);

	}

	private void readFromParcel(Parcel in) {

		position = in.readInt();
		mItemId = in.readString();
		linkTypeId = in.readString();
		linkTypeName = in.readString();
		mItemImgUrl = in.readString();
		mItemName = in.readString();
		linkId = in.readString();
		mShapeName = in.readString();
		mBkgrdImage = in.readString();
		mBtnColor = in.readString();
		mBtnFontColor = in.readString();
		smBtnColor = in.readString();
		smBtnFontColor = in.readString();
		level = in.readString();
		mGrpBkgrdColor = in.readString();
		mGrpFntColor = in.readString();
		sGrpBkgrdColor = in.readString();
		sGrpFntColor = in.readString();

	}

	public MainMenuBO(Parcel in) {
		readFromParcel(in);
	}

	public String getSmBkgrdColor() {
		return smBkgrdColor;
	}

	public void setSmBkgrdColor(String smBkgrdColor) {
		this.smBkgrdColor = smBkgrdColor;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static final Parcelable.Creator<MainMenuBO> CREATOR = new Parcelable.Creator() {
		public MainMenuBO createFromParcel(Parcel in) {
			return new MainMenuBO(in);
		}

		public MainMenuBO[] newArray(int size) {
			return new MainMenuBO[size];
		}
	};
}