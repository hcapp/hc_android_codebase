package com.hubcity.android.businessObjects;

import android.os.Parcel;
import android.os.Parcelable;

public class RetailerBo implements Parcelable {

    public RetailerBo(String rowNumber, String retailerId, String retailerName,
                      String retailLocationId, String distance, String retailAddress,
                      String completeAddress, String logoImagePath,
                      String bannerAdImagePath, String ribbonAdImagePath,
                      String ribbonAdURL, String saleFlag, String latitude,
                      String longitude, String retListId) {

        this.rowNumber = rowNumber;
        this.retailerId = retailerId;
        this.retailerName = retailerName;
        this.retailLocationId = retailLocationId;
        this.logoImagePath = logoImagePath;
        this.bannerAdImagePath = bannerAdImagePath;
        this.ribbonAdImagePath = ribbonAdImagePath;
        this.ribbonAdURL = ribbonAdURL;
        this.saleFlag = saleFlag;
        this.latitude = latitude;
        this.longitude = longitude;
        this.retListId = retListId;
        this.distance = distance;
        this.retailAddress = retailAddress;
        this.completeAddress = completeAddress;

    }

    public RetailerBo(String rowNumber, String retailerId, String retailerName,
                      String retailLocationId,

                      String distance, String retailAddress,

                      String logoImagePath, String bannerAdImagePath,
                      String ribbonAdImagePath, String ribbonAdURL, String saleFlag,
                      String latitude, String longitude, String retListId,
                      String retailerUrl, String productPrice, String phone,
                      String poweredby) {

        this.rowNumber = rowNumber;
        this.retailerId = retailerId;
        this.retailerName = retailerName;
        this.retailLocationId = retailLocationId;
        this.logoImagePath = logoImagePath;
        this.bannerAdImagePath = bannerAdImagePath;
        this.ribbonAdImagePath = ribbonAdImagePath;
        this.ribbonAdURL = ribbonAdURL;
        this.saleFlag = saleFlag;
        this.latitude = latitude;
        this.longitude = longitude;
        this.retListId = retListId;
        this.distance = distance;
        this.retailAddress = retailAddress;
        this.retailerUrl = retailerUrl;
        this.productPrice = productPrice;
        this.phone = phone;
        this.poweredby = poweredby;

    }

    //Added by sharanamma
    public RetailerBo(String rowNumber, String retailerId, String retailerName,
                      String retailLocationId, String distance, String retailAddress1, String retailAddress2,
                      String completeAddress, String logoImagePath,
                      String bannerAdImagePath, String ribbonAdImagePath,
                      String ribbonAdURL, String saleFlag, String latitude,
                      String longitude, String retListId, String city, String state, String postalCode,String locationOpen) {

        this.rowNumber = rowNumber;
        this.retailerId = retailerId;
        this.retailerName = retailerName;
        this.retailLocationId = retailLocationId;
        this.logoImagePath = logoImagePath;
        this.bannerAdImagePath = bannerAdImagePath;
        this.ribbonAdImagePath = ribbonAdImagePath;
        this.ribbonAdURL = ribbonAdURL;
        this.saleFlag = saleFlag;
        this.latitude = latitude;
        this.longitude = longitude;
        this.retListId = retListId;
        this.distance = distance;
        this.retailAddress1 = retailAddress1;
        this.retailAddress2 = retailAddress2;
        this.completeAddress = completeAddress;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.locationOpen = locationOpen;
    }

    String rowNumber;
    String retailerId;
    String retailerName;
    String retailLocationId;
    String logoImagePath;
    String bannerAdImagePath;
    String ribbonAdImagePath;
    String ribbonAdURL;
    String saleFlag;
    String latitude;
    String longitude;
    String retListId;
    String distance;
    String retailAddress;
    String retailAddress1;
    String retailAddress2;
    String retailAddress3;
    String retailAddress4;
    String city;
    String state;
    String postalCode;
    String completeAddress;
    String catName;
    String catId;
    String mapMarkerId;
    String retailerUrl;
    String productPrice;
    String phone;
    String poweredby;
    String locationOpen;

    public String getRowNumber() {
        return rowNumber;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public String getRetailLocationId() {
        return retailLocationId;
    }

    public String getLogoImagePath() {
        return logoImagePath;
    }

    public String getBannerAdImagePath() {
        return bannerAdImagePath;
    }

    public String getRibbonAdImagePath() {
        return ribbonAdImagePath;
    }

    public String getRibbonAdURL() {
        return ribbonAdURL;
    }

    public String getSaleFlag() {
        return saleFlag;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getRetListId() {
        return retListId;
    }

    public void setRowNumber(String rowNumber) {
        this.rowNumber = rowNumber;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }

    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }

    public void setRetailLocationId(String retailLocationId) {
        this.retailLocationId = retailLocationId;
    }

    public void setLogoImagePath(String logoImagePath) {
        this.logoImagePath = logoImagePath;
    }

    public void setBannerAdImagePath(String bannerAdImagePath) {
        this.bannerAdImagePath = bannerAdImagePath;
    }

    public void setRibbonAdImagePath(String ribbonAdImagePath) {
        this.ribbonAdImagePath = ribbonAdImagePath;
    }

    public void setRibbonAdURL(String ribbonAdURL) {
        this.ribbonAdURL = ribbonAdURL;
    }

    public void setSaleFlag(String saleFlag) {
        this.saleFlag = saleFlag;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setRetListId(String retListId) {
        this.retListId = retListId;
    }

    public String getDistance() {
        return distance;
    }

    public String getRetailAddress() {
        return retailAddress;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setRetailAddress(String retailAddress) {
        this.retailAddress = retailAddress;
    }

    public String getCompleteAddress() {
        return completeAddress;
    }

    public void setCompleteAddress(String completeAddress) {
        this.completeAddress = completeAddress;
    }

    public void setRetailAddress2(String retailAddress2) {
        this.retailAddress2 = retailAddress2;
    }

    public String getRetailAddress1() {
        return retailAddress1;
    }

    public void setRetailAddress1(String retailAddress1) {
        this.retailAddress1 = retailAddress1;
    }

    public String getRetailAddress2() {
        return retailAddress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public RetailerBo(Parcel in) {
        latitude = in.readString();
        longitude = in.readString();
        retailerName = in.readString();
        retailAddress = in.readString();
        completeAddress = in.readString();
        retailerId = in.readString();
        ribbonAdImagePath = in.readString();
        ribbonAdURL = in.readString();
        bannerAdImagePath = in.readString();
        retailLocationId = in.readString();

        retailerUrl = in.readString();
        productPrice = in.readString();
        phone = in.readString();
        distance = in.readString();
        poweredby = in.readString();

        retailAddress1 = in.readString();
        retailAddress2 = in.readString();
        city = in.readString();
        state = in.readString();
        postalCode = in.readString();
    }

    public static final Parcelable.Creator<RetailerBo> CREATOR = new Parcelable.Creator<RetailerBo>() {

        @Override
        public RetailerBo createFromParcel(Parcel source) {
            return new RetailerBo(source);
        }

        @Override
        public RetailerBo[] newArray(int size) {
            return new RetailerBo[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(retailerName);
        dest.writeString(retailAddress);
        dest.writeString(completeAddress);
        dest.writeString(retailerId);

        dest.writeString(ribbonAdImagePath);

        dest.writeString(ribbonAdURL);

        dest.writeString(bannerAdImagePath);

        dest.writeString(retailLocationId);

        dest.writeString(retailerUrl);
        dest.writeString(productPrice);
        dest.writeString(phone);
        dest.writeString(distance);
        dest.writeString(poweredby);

        dest.writeString(retailAddress1);
        dest.writeString(retailAddress2);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(postalCode);

    }

    public String getMapMarkerId() {
        return mapMarkerId;
    }

    public void setMapMarkerId(String mapMarkerId) {
        this.mapMarkerId = mapMarkerId;
    }

    public String getRetailerUrl() {
        return retailerUrl;
    }

    public void setRetailerUrl(String retailerUrl) {
        this.retailerUrl = retailerUrl;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPoweredby() {
        return poweredby;
    }

    public void setPoweredby(String poweredby) {
        this.poweredby = poweredby;
    }


}
