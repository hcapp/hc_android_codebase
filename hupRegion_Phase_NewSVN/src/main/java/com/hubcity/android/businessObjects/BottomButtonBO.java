package com.hubcity.android.businessObjects;

public class BottomButtonBO {

	private String position;
	private String bottomBtnID;
	private String bottomBtnImg;
	private String bottomBtnImgOff;
	private String btnLinkTypeID;
	private String btnLinkTypeName;
	private String btnLinkID;

	public BottomButtonBO(String bottomBtnID, String bottomBtnImg,
			String bottomBtnImgOff, String btnLinkTypeID,
			String btnLinkTypeName, String position, String btnLinkID) {
		this.bottomBtnID = bottomBtnID;
		this.bottomBtnImg = bottomBtnImg;
		this.bottomBtnImgOff = bottomBtnImgOff;
		this.btnLinkTypeID = btnLinkTypeID;
		this.btnLinkTypeName = btnLinkTypeName;
		this.position = position;
		this.btnLinkID = btnLinkID;
	}

	public String getBottomBtnID() {
		return bottomBtnID;
	}

	public String getBottomBtnImg() {
		return bottomBtnImg;
	}

	public String getBottomBtnImgOff() {
		return bottomBtnImgOff;
	}

	public String getBtnLinkTypeID() {
		return btnLinkTypeID;
	}

	public String getBtnLinkTypeName() {
		return btnLinkTypeName;
	}

	public void setBottomBtnID(String bottomBtnID) {
		this.bottomBtnID = bottomBtnID;
	}

	public void setBottomBtnImg(String bottomBtnImg) {
		this.bottomBtnImg = bottomBtnImg;
	}

	public void setBottomBtnImgOff(String bottomBtnImgOff) {
		this.bottomBtnImgOff = bottomBtnImgOff;
	}

	public void setBtnLinkTypeID(String btnLinkTypeID) {
		this.btnLinkTypeID = btnLinkTypeID;
	}

	public void setBtnLinkTypeName(String btnLinkTypeName) {
		this.btnLinkTypeName = btnLinkTypeName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getBtnLinkID() {
		return btnLinkID;
	}

	public void setBtnLinkID(String btnLinkID) {
		this.btnLinkID = btnLinkID;
	}

}
