package com.hubcity.android.businessObjects;

public class FundraiserEntryItem implements Item {

	public String fundId = "";
	public String fundName = "";
	public String startDate = "";
	public String fundCatId = "";
	public String fundCatName = "";
	public String imagePath = "";

	public FundraiserEntryItem(String fundId, String fundName,
			String startDate, String fundCatId, String fundCatName,
			String imagePath) {

		this.fundId = fundId;
		this.fundName = fundName;
		this.startDate = startDate;
		this.fundCatId = fundCatId;
		this.fundCatName = fundCatName;
		this.imagePath = imagePath;

	}

	@Override
	public boolean isSection() {
		return false;
	}

}
