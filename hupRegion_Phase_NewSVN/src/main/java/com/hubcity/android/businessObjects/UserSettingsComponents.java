package com.hubcity.android.businessObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserSettingsComponents {

	public static List<String> textFields;
	public static List<String> pickerFields;
	protected static List<String> radioFields;
	protected static List<String> imageFields;
	protected static HashMap<String, String> tagMap;

	public static final String LBL_FIRST_NAME = "First Name";
	public static final String LBL_LAST_NAME = "Last Name";
	public static final String LBL_EMAIL_ADDRESS = "Email";
	public static final String LBL_POSTAL_CODE = "Zip Code";
	public static final String LBL_MOBILE_NUMBER = "Mobile";
	public static final String LBL_DATEOFBIRTH = "Date Of Birth";
	public static final String LBL_MARITAL_STATUS = "Marital Status";
	public static final String LBL_EDUCATION = "Education";
	public static final String LBL_GENDER = "Gender";
	public static final String LBL_INCOME_RANGE = "Income Range";
	public static final String LBL_IMAGE = "Image";

	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String EMAIL_ADDRESS = "email";
	public static final String POSTAL_CODE = "postalCode";
	public static final String MOBILE_NUMBER = "phoneNum";
	public static final String DATEOFBIRTH = "dob";
	public static final String MARITAL_STATUS = "martialStatusId";
	public static final String EDUCATION = "educatonLevelId";
	public static final String GENDER = "gender";
	public static final String INCOME_RANGE = "incomeRangeId";
	public static final String ITEM_NAME = "itemName";
	public static final String USER_DATA = "userData";
	public static final String FIELD_NAME = "fieldName";
	public static final String POSITION = "position";
	public static final String VALUE = "value";
	public static final String DEVICE_ID = "deviceId";

	public static final String PICKER = "P";
	public static final String TEXT_FIELD = "T";
	public static final String RADIO_BUTTON = "R";
	public static final String IMAGE = "I";

	public static final String MALE = "Male";
	public static final String FEMALE = "Female";

	public static final String SELECT = "Select";
	public UserSettingsComponents() {
		super();
		textFields = new ArrayList<>();
		textFields.add(LBL_FIRST_NAME);
		textFields.add(LBL_LAST_NAME);
		textFields.add(LBL_EMAIL_ADDRESS);
		textFields.add(LBL_POSTAL_CODE);
		textFields.add(LBL_MOBILE_NUMBER);
		textFields.add(LBL_DATEOFBIRTH);

		radioFields = new ArrayList<>();
		radioFields.add(LBL_GENDER);

		pickerFields = new ArrayList<>();
		pickerFields.add(LBL_MARITAL_STATUS);
		pickerFields.add(LBL_EDUCATION);
		pickerFields.add(LBL_INCOME_RANGE);

		imageFields = new ArrayList<>();
		imageFields.add(LBL_IMAGE);

		tagMap = new HashMap<>();
		tagMap.put(LBL_FIRST_NAME, FIRST_NAME);
		tagMap.put(LBL_LAST_NAME, LAST_NAME);
		tagMap.put(LBL_EMAIL_ADDRESS, EMAIL_ADDRESS);
		tagMap.put(LBL_POSTAL_CODE, POSTAL_CODE);
		tagMap.put(LBL_MOBILE_NUMBER, MOBILE_NUMBER);
		tagMap.put(LBL_DATEOFBIRTH, DATEOFBIRTH);
		tagMap.put(LBL_GENDER, GENDER);
		tagMap.put(LBL_MARITAL_STATUS, MARITAL_STATUS);
		tagMap.put(LBL_EDUCATION, EDUCATION);
		tagMap.put(LBL_INCOME_RANGE, INCOME_RANGE);
	}

}
