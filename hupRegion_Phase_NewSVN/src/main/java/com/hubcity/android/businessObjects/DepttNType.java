/*******************************************************************************
 * =============================================================================   
 *   Name : BOProfile.java                                                       * 
 *   Part of : SoftPhone					                                     * 
 *   Interface : <__INTERFACE_WITH_OTHER_CLASSES_MODULES__>                      * 
 *   Version : 1.0				                                                 *
 *   Description : <__DESCRIPTION_OF_THIS_CLASS_FILE__>                          * 
 *   <__DESCRIPTION_OF_THIS_CLASS_FILE__>                                        * 
 *                                                                               * 
 *   Copyright (c) 2013 SPAN Infotech.                                           * 
 *   This material, including documentation and any related computer programs, is* 
 *   protected by copyright controlled by SPAN Infotech. All rights are reserved.* 
 *   Copying, including reproducing, storing, adapting or translating, any or all* 
 *   of this material requires the prior written consent of SPAN Infotech. This  * 
 *   material also contains confidential information which may not be disclosed  * 
 *   to others without the prior written consent of SPAN Infotech.               * 
 *   																			 * 
 *   History:                                                                    * 
 *   2013 : venkatesha_kc            		                              	     * 
 *   		 - First Version 				 									 * 
 *   =============================================================================
 ******************************************************************************/
package com.hubcity.android.businessObjects;

public class DepttNType {

	@Override
	public int hashCode() {
		return Integer.parseInt(id);
	}

	@Override
	public String toString() {
		return id;
	}

	public boolean equals(Object obj) {
		return id.equals(((DepttNType) obj).id);
	}

	public DepttNType(String id, String name) {
		this.id = id;
		this.name = name;
	}

	private String name;

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String id;

}