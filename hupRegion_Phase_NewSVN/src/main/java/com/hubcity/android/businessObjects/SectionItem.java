package com.hubcity.android.businessObjects;

public class SectionItem implements Item {

	private final String title;
	private int itemSize;

	public SectionItem(String title) {
		this.title = title;
	}

	public SectionItem(String title, int itemSize) {
		this.title = title;
		this.itemSize = itemSize;
	}

	public int getItemSize() {
		return itemSize;
	}

	public void setItemSize(int itemSize) {
		this.itemSize = itemSize;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public boolean isSection() {
		return true;
	}

}