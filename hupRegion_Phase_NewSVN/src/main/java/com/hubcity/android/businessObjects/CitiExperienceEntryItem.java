package com.hubcity.android.businessObjects;

public class CitiExperienceEntryItem implements Item {
	public final String rowNumber;
	public final String retailerId;
	public final String retailerName;
	public final String retailLocationId;
	public final String logoImagePath;
	public final String bannerAdImagePath;
	public final String ribbonAdImagePath;
	public final String ribbonAdURL;
	public final String saleFlag;
	public final String latitude;
	public final String longitude;
	public final String retListId;
	public final String distance;
	public final String retailAddress;
	public final String retailAddress1;
	public final String retailAddress2;
	public final String retailAddress3;
	public final String retailAddress4;
	public final String city;
	public final String state;
	public final String postalCode;
	public final String completeAddress;
	public final String catName;
	public final String catId;
	public final String locationOpen;

	public CitiExperienceEntryItem(String rowNumber, String retailerId,
			String retailerName, String retailLocationId, String logoImagePath,
			String bannerAdImagePath, String ribbonAdImagePath,
			String ribbonAdURL, String saleFlag, String latitude,
			String longitude, String retListId, String distance,
			String retailAddress, String retailAddress1, String retailAddress2,
			String retailAddress3, String retailAddress4, String city,
			String state, String postalCode, String completeAddress,
			String catName, String catId,String locationOpen) {

		this.rowNumber = rowNumber;
		this.retailerId = retailerId;
		this.retailerName = retailerName;
		this.retailLocationId = retailLocationId;
		this.logoImagePath = logoImagePath;
		this.bannerAdImagePath = bannerAdImagePath;
		this.ribbonAdImagePath = ribbonAdImagePath;
		this.ribbonAdURL = ribbonAdURL;
		this.saleFlag = saleFlag;
		this.latitude = latitude;
		this.longitude = longitude;
		this.retListId = retListId;
		this.distance = distance;
		this.retailAddress = retailAddress;
		this.retailAddress1 = retailAddress1;
		this.retailAddress2 = retailAddress2;
		this.retailAddress3 = retailAddress3;
		this.retailAddress4 = retailAddress4;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.completeAddress = completeAddress;
		this.catName = catName;
		this.catId = catId;
		this.locationOpen =locationOpen;
	}

	@Override
	public boolean isSection() {
		return false;
	}

}
