package com.hubcity.android.businessObjects;

import java.util.ArrayList;


public class BottomButtonListSingleton {

	static public ArrayList<BottomButtonBO> listBottomButton;
	
	static BottomButtonListSingleton mBottomButtonListSingleton;

	private BottomButtonListSingleton(ArrayList<BottomButtonBO> listBottomBtn) {
		listBottomButton = listBottomBtn;
	}

	static public void clearBottomButtonListSingleton() {
		if (mBottomButtonListSingleton != null) {
			mBottomButtonListSingleton = null;
		}
	}

	public static BottomButtonListSingleton getListBottomButton() {
		if (mBottomButtonListSingleton != null) {
			return mBottomButtonListSingleton;
		} else {
			return null;
		}
	}

	public static BottomButtonListSingleton getListBottomButton(
			ArrayList<BottomButtonBO> listBottomButton) {
		if (mBottomButtonListSingleton != null) {
			return mBottomButtonListSingleton;
		} else {
			mBottomButtonListSingleton = new BottomButtonListSingleton(
					listBottomButton);

			return mBottomButtonListSingleton;
		}
	}
}