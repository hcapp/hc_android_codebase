package com.hubcity.android.businessObjects;

public class UserSettingsField {

	private String label;
	private String tag;
	private String value;
	private String componentType;
	private int position;

	public UserSettingsField() {
		super();

	}

	public UserSettingsField(String label, String value, String componentType,
			int position) {
		this();
		this.label = label;
		this.value = value;
		this.componentType = componentType;
		this.position = position;
	}

	public UserSettingsField(String label, String value, int position) {
		this();
		this.label = label;
		this.value = value;
		this.position = position;
		if (UserSettingsComponents.pickerFields.contains(label)) {
			this.componentType = UserSettingsComponents.PICKER;
			if (value.equals(UserSettingsComponents.SELECT)) {
				value = "0";
			}
		} else if (UserSettingsComponents.radioFields.contains(label)) {
			this.componentType = UserSettingsComponents.RADIO_BUTTON;
		} else if (UserSettingsComponents.imageFields.contains(label)) {
			this.componentType = UserSettingsComponents.IMAGE;
		} else {
			this.componentType = UserSettingsComponents.TEXT_FIELD;
		}
		this.tag = UserSettingsComponents.tagMap.get(label);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
