package com.hubcity.android.businessObjects;

/**
 * This class saves the data of FAQs tagged to each of the questions in a
 * category
 * 
 * @author rekha_p
 * 
 */
public class FaqDetails {

	String faqId;
	String catName;
	String question;
	String lowerLimit;


	public FaqDetails(String faqid, String catname, String quest,
			String lowerLmt) {
		faqId = faqid;
		catName = catname;
		question = quest;
		lowerLimit = "";
	}

	public String getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(String lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public String getFaqId() {
		return faqId;
	}

	public void setFaqId(String faqId) {
		this.faqId = faqId;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

}
