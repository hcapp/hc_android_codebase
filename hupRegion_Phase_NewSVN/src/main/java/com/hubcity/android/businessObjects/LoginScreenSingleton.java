package com.hubcity.android.businessObjects;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginScreenSingleton {

	static private String bkgndColor;
	static private String fontColor;
	static private String description;
	static private String btnColor;
	static private String btnFontColor;
	static private String logoImg;
	static private String poweredBy;
	static private String smallLogoUrl;
	static private String hubCitiImgUrl;
	static private String title;
	static private String templateName;
	static private JSONObject jsonObject;

	@SuppressWarnings("static-access")
	private LoginScreenSingleton(JSONObject jsonObject) {
		try{
			bkgndColor = jsonObject.getString("bkgndColor");
			fontColor = jsonObject.getString("fontColor");
			title = jsonObject.getString("title");
			description = jsonObject.getString("description");
			btnColor = jsonObject.getString("btnColor");
			btnFontColor = jsonObject.getString("btnFontColor");
			logoImg = jsonObject.getString("logoImg");
			smallLogoUrl = jsonObject.getString("smallLogo");
			poweredBy = jsonObject.getString("poweredBy");
			templateName = jsonObject.optString("templateName");
		}catch (JSONException e){
			e.printStackTrace();
		}

	}

	static public LoginScreenSingleton getLoginScreenSingleton() {
		if (mLoginScreenSingleton != null) {
			return mLoginScreenSingleton;
		} else {
			return null;
		}
	}

	static LoginScreenSingleton mLoginScreenSingleton;

	static public void clearLoginScreenSingleton() {
		if (mLoginScreenSingleton != null) {
			mLoginScreenSingleton = null;
		}
	}

	static public LoginScreenSingleton getLoginScreenSingleton(
			JSONObject jsonObject) {
		if (mLoginScreenSingleton == null) {
			mLoginScreenSingleton = new LoginScreenSingleton(jsonObject);
		}
		return mLoginScreenSingleton;
	}

	public String getBkgndColor() {
		return bkgndColor;
	}

	public String getFontColor() {
		return fontColor;
	}

	public String getDescription() {
		return description;
	}

	public String getBtnColor() {
		return btnColor;
	}

	public String getBtnFontColor() {
		return btnFontColor;
	}

	public String getLogoImgUrl() {
		return logoImg;
	}

	public String getSmallLogoUrl() {
		return smallLogoUrl;
	}

	public String getHubCitiImgUrl() {
		return hubCitiImgUrl;
	}

	public String getPoweredBy() {
		return poweredBy;
	}

	public String getTitle() {
		return title;
	}
	public static String getTemplateName() {
		return templateName;
	}
}