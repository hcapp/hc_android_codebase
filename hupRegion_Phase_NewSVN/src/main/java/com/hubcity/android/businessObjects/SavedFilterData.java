package com.hubcity.android.businessObjects;

import java.io.Serializable;
import java.util.ArrayList;

public class SavedFilterData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<FilterArdSortScreenBO> arrSavedData = new ArrayList<>();

	public ArrayList<FilterArdSortScreenBO> getArrSavedData() {
		return arrSavedData;
	}

	public void setArrSavedData(ArrayList<FilterArdSortScreenBO> arrSavedData) {
		this.arrSavedData = arrSavedData;
	}

}
