package com.hubcity.android.businessObjects;

public class EntryItem implements Item {

	public final String alertName;
	public final String subtitle;
	public final String imageURL;
	public final String startDate;
	public final String startTime;
	public final String endDate;
	public final String endTime;

	public EntryItem(String alertName, String subtitle, String imageURL,
			String startDate, String startTime, String endDate, String endTime) {
		this.alertName = alertName;
		this.subtitle = subtitle;
		this.imageURL = imageURL;
		this.startDate = startDate;
		this.startTime = startTime;
		this.endDate = endDate;
		this.endTime = endTime;
	}

	@Override
	public boolean isSection() {
		return false;
	}

}