package com.hubcity.android.model;

import java.util.ArrayList;

/**
 * Created by sharanamma on 21-12-2016.
 */
public class CategoryDetails {


        public int getParCatId() {
            return parCatId;
        }

        public void setParCatId(int parCatId) {
            this.parCatId = parCatId;
        }

        public String getParCatName() {
            return parCatName;
        }

        public void setParCatName(String parCatName) {
            this.parCatName = parCatName;
        }

        private int parCatId;
        private String parCatName;

    }

