package com.hubcity.android.model;

/**
 * Created by supriya.m on 7/7/2016.
 */
public class UpdateUserResponseModel
{
	public String getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(String responseCode)
	{
		this.responseCode = responseCode;
	}

	private String responseCode;

	public String getResponseText()
	{
		return responseText;
	}

	public void setResponseText(String responseText)
	{
		this.responseText = responseText;
	}

	private String responseText;
}
