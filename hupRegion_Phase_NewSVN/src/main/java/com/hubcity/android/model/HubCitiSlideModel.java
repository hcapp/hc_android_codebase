package com.hubcity.android.model;

import java.util.ArrayList;

/**
 * Created by sharanamma on 25-08-2016.
 */
public class HubCitiSlideModel {
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    private String responseCode;

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ArrayList<SlidImages> getSlideImages() {
        return slideImages;
    }

    public void setSlideImages(ArrayList<SlidImages> slideImages) {
        this.slideImages = slideImages;
    }

    private String responseText;

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    private String logoPath;
    private ArrayList<SlidImages> slideImages;

    public class SlidImages{
        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public int getType() {
            return Type;
        }

        public void setType(int type) {
            Type = type;
        }

        public String link;

        public String getIsAnythingPageOrWeblink() {
            return isAnythingPageOrWebLink;
        }

        public void setIsAnythingPageOrWeblink(String isAnythingPageOrWeblink) {
            this.isAnythingPageOrWebLink = isAnythingPageOrWeblink;
        }

        public String isAnythingPageOrWebLink;

        public String getIsAnythingPageOrWeblinkValue() {
            return isAnythingPageOrWebLinkValue;
        }

        public void setIsAnythingPageOrWeblinkValue(String isAnythingPageOrWeblinkValue) {
            this.isAnythingPageOrWebLinkValue = isAnythingPageOrWeblinkValue;
        }

        public String isAnythingPageOrWebLinkValue;
        public int Type;
    }
}
