package com.hubcity.android.model;

/**
 * Created by sharanamma on 1/27/2016.
 */
public class CityModel {

    private String cityId;
    private String cityName;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }


}
