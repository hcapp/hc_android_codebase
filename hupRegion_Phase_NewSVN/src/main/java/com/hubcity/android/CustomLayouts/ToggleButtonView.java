package com.hubcity.android.CustomLayouts;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.HubCityContext;
import com.scansee.hubregion.R;

/**
 * Created by sharanamma on 29-07-2016.
 */
public class ToggleButtonView extends LinearLayout implements View.OnClickListener {
    protected TextView mLabelDistance;
    protected TextView mLabelName;
    protected TextView mLabelMap;
    private ToggleViewInterface listener;


    public interface ToggleViewInterface {
        void onToggleClick(String toggleBtnName);
    }
    public ToggleButtonView(Context context) {
        super(context);
    }

    public ToggleButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        listener = (ToggleViewInterface) context;
        setUpComponents(context);
    }

    public ToggleButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void setUpComponents(Context context) {
        setOrientation(HORIZONTAL);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutInflater.inflate(R.layout.component_toggle_buttons, this, true);

        mLabelDistance = (TextView) getChildAt(0);
        mLabelName = (TextView) getChildAt(1);
        mLabelMap = (TextView) getChildAt(2);

        mLabelDistance.setText(getResources().getString(R.string.distance));
        if (Build.VERSION.SDK_INT >= 16) {
            mLabelDistance.setBackground(ContextCompat.getDrawable(context, R.drawable.radio_selected));
        }else{
            mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.radio_selected));
        }

        mLabelDistance.setTypeface(null, Typeface
                .BOLD);
        mLabelDistance.setTextColor(getResources().getColor(R.color.black));

        mLabelName.setText(getResources().getString(R.string.names));
        mLabelMap.setText(getResources().getString(R.string.title_activity_maps));

        mLabelDistance.setOnClickListener(this);
        mLabelName.setOnClickListener(this);
        mLabelMap.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!CommonConstants.disableClick) {
            if (view.getId() != R.id.toggle_map) {

                if(((TextView) view).getText().toString().trim().equalsIgnoreCase("Distance")){
                    HubCityContext.sortSelectedPostion = 1;
                }else if(((TextView) view).getText().toString().trim().equalsIgnoreCase("Name")){
                    HubCityContext.sortSelectedPostion = 2;
                }
            }
            listener.onToggleClick(((TextView) view).getText().toString().trim());
        }
    }
}
