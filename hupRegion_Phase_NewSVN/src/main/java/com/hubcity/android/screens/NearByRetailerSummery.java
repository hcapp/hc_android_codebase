package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

public class NearByRetailerSummery extends CustomTitleBar {
	HashMap<String, String> retailerData;
	ArrayList<HashMap<String, String>> retalerInfoList = new ArrayList<>();

	ListView retailerInfoListView;
	RelativeLayout splashFrame, retailerAdLayout, divider;
	LinearLayout retailerNameLayout;
	String retName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.near_by_retail_summary);

		try {
			title.setSingleLine(false);
			title.setMaxLines(2);

			title.setEllipsize(TextUtils.TruncateAt.END);

			leftTitleImage.setVisibility(View.GONE);
			String retailerUrl = getIntent().getExtras().getString("retailUrl");
			String phone = getIntent().getExtras().getString("phone");
			retName = getIntent().getExtras().getString("retName");
			String lat = getIntent().getExtras().getString("latitude");
			String longitude = getIntent().getExtras().getString("longitude");
			String address = getIntent().getExtras().getString("address");
			title.setText(retName);

			retailerInfoListView = (ListView) findViewById(R.id.retailer_listview);

			retailerData = new HashMap<>();
			retailerData.put(CommonConstants.TAG_PAGE_LINK, "directions");
			retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
			retailerData.put(CommonConstants.TAG_PAGE_TITLE, "Get Directions");
			retailerData.put(CommonConstants.TAG_PAGE_INFO, address);
			retailerData.put(CommonConstants.TAG_RET_LATITIUDE, lat);
			retailerData.put(CommonConstants.TAG_RET_LONGITUDE, longitude);
			retalerInfoList.add(retailerData);

			retailerData = new HashMap<>();
			retailerData.put(CommonConstants.TAG_PAGE_LINK, "call");
			retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
			retailerData.put(CommonConstants.TAG_PAGE_TITLE, "Call Location");
			retailerData.put(CommonConstants.TAG_PAGE_INFO, phone);
			retalerInfoList.add(retailerData);

			retailerData = new HashMap<>();
			retailerData.put(CommonConstants.TAG_PAGE_LINK, "browser");
			retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
			retailerData.put(CommonConstants.TAG_PAGE_TITLE, "Browse Website");
			retailerData.put(CommonConstants.TAG_PAGE_INFO, retailerUrl);
			retalerInfoList.add(retailerData);
			retailerInfoListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long pst) {

                    Intent retailerLink = null;
                    String tempLink = retalerInfoList.get(position).get(
                            CommonConstants.TAG_PAGE_LINK);

                    if ("directions".equalsIgnoreCase(tempLink)) {

                        // Added by - Dileep- For Fetching Retailer lat and long
                        // to drop pin on map
                        HashMap<String, String> selectedData = retalerInfoList
                                .get(position);
                        String latitude = selectedData
                                .get(CommonConstants.TAG_RET_LATITIUDE);
                        String longitude = selectedData
                                .get(CommonConstants.TAG_RET_LONGITUDE);

                        if ((latitude != null && !"N/A".equalsIgnoreCase(latitude))
                                && (longitude != null && !"N/A"
                                        .equalsIgnoreCase(latitude))) {
                            retailerLink = new Intent(NearByRetailerSummery.this,
                                    RetailerMapActivity.class);
                            retailerLink.putExtra("mapLat",
                                    Double.valueOf(latitude));
                            retailerLink.putExtra("mapLng",
                                    Double.valueOf(longitude));
                            retailerLink.putExtra(
                                    CommonConstants.TAG_ADDRESS1,
                                    retalerInfoList.get(position).get(
                                            CommonConstants.TAG_PAGE_INFO));
                            retailerLink.putExtra(CommonConstants.TAG_RETAILE_NAME,
                                    retName);
                            startActivityForResult(retailerLink,
                                    Constants.STARTVALUE);

                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Location details Unavailable",
                                    Toast.LENGTH_SHORT).show();
                        }

                    } else if ("call".equalsIgnoreCase(tempLink)) {
                        try {
                            retailerLink = new Intent(Intent.ACTION_DIAL);
                            retailerLink.setData(Uri.parse("tel:"
                                    + retalerInfoList.get(position).get(
                                            CommonConstants.TAG_PAGE_INFO)));

                            startActivity(retailerLink);
                        } catch (Exception e) {
                            e.printStackTrace();

                            new AlertDialog.Builder(NearByRetailerSummery.this)
                                    .setTitle("Alert")
                                    .setMessage("Call feature not available.!")
                                    .setPositiveButton("OK", null).show();
                        }
                    } else if ("browser".equalsIgnoreCase(tempLink)) {
                        retailerLink = new Intent(NearByRetailerSummery.this,
                                ScanseeBrowserActivity.class);

                        retailerLink.putExtra(CommonConstants.URL, retalerInfoList
                                .get(position).get(CommonConstants.TAG_PAGE_INFO));
                        retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE,
                                retName);
                        startActivityForResult(retailerLink, Constants.STARTVALUE);
                    }

                }

                @Override
                public boolean equals(Object o) {
                    return super.equals(o);
                }

                @Override
                public int hashCode() {
                    return super.hashCode();
                }
            });
			retailerInfoListView.setAdapter(new RetailerListAdapter(
                    NearByRetailerSummery.this, retalerInfoList));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
