package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.ListIterator;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.HubCityContext;
import com.scansee.hubregion.R;

public class TwoColumnMenuTemplate extends MenuPropertiesActivity {
    private static final int TOTAL_COLUMN = 2;
    private int buttonCounter;
    protected static final String DEBUG_TAG = "TwoColumnMenuTemplate";
    private int screenWidth;
    private LinearLayout buttonLinearLayout;
    private ListIterator<MainMenuBO> listIterator;
    private static boolean isOnNewIntent;
    boolean hasExp = false;
    private LinearLayout parentLinearLayout;
    private LinearLayout bannerParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonConstants.hamburgerIsFirst = true;

        Intent intent = getIntent();
        previousMenuLevel = intent.getExtras().getString(
                Constants.MENU_LEVEL_INTENT_EXTRA);

        // lets get the screen size
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int screenHeight = metrics.heightPixels;
        Log.i("screenHeight", "" + screenHeight);

        screenWidth = metrics.widthPixels;


        RelativeLayout parent = new RelativeLayout(this);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        parent.setLayoutParams(param);

        ScrollView scrollView = new ScrollView(this);
        buttonLinearLayout = new LinearLayout(this);
        buttonLinearLayout.setOrientation(LinearLayout.VERTICAL);

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        parentLinearLayout = (LinearLayout) layoutInflater.inflate(
                R.layout.twocolumn_menu_template, null, false);
        bannerParent = (LinearLayout) parentLinearLayout.findViewById(R.id.bannerParent);
        bannerParent.setVisibility(View.GONE);
// Initializing drawer layout
        drawer = (DrawerLayout) parentLinearLayout.findViewById(R.id.drawer_layout);
        LinearLayout ParentIntermideate = new LinearLayout(this);
        ParentIntermideate.setOrientation(LinearLayout.VERTICAL);

        LinearLayout mainLayout = (LinearLayout) parentLinearLayout.findViewById(R.id.menu_list_parent);
        LinearLayout mainParentLayout = (LinearLayout) parentLinearLayout.findViewById(R.id.mainParentLayout);

        LinearLayout.LayoutParams buttonLinearLayoutParentParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        BottomButtonListSingleton mBottomButtonListSingleton;
        mBottomButtonListSingleton = BottomButtonListSingleton
                .getListBottomButton();

        if (mBottomButtonListSingleton != null) {

            ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton.listBottomButton;

            if (listBottomButton != null && listBottomButton.size() > 1)
            {
                CustomNavigation customNavigation = (CustomNavigation) parentLinearLayout.findViewById(R.id.custom_navigation);
                CommonConstants.setPaddingToSideMenu(customNavigation,screenHeight);
                int px = (int) (50 * this.getResources().getDisplayMetrics().density + 0.5f);
                buttonLinearLayoutParentParam.setMargins(0, 0, 0, px);
            }
        }

        scrollView.setLayoutParams(parentParam);
        mainParentLayout.setLayoutParams(buttonLinearLayoutParentParam);
        parentLinearLayout.setLayoutParams(parentParam);
        ParentIntermideate.setLayoutParams(parentParam);
        scrollView.addView(buttonLinearLayout);

        addTitleBar(ParentIntermideate, parent);
        //parent.addView(mainParentLayout);
        ParentIntermideate.addView(mainParentLayout);
        parent.addView(ParentIntermideate);
        mainLayout.addView(scrollView);

        handleIntent(intent);

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            if (mBkgrdColor != null && !"N/A".equals(mBkgrdColor)) {

                buttonLinearLayout.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(mBkgrdColor));
                parentLinearLayout.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                scrollView.setBackgroundColor(Color.parseColor(mBkgrdColor));

            } else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
                new ImageLoaderAsync1(parent).execute(mBkgrdImage);
                new ImageLoaderAsync1(parentLinearLayout).execute(mBkgrdImage);
                new ImageLoaderAsync1(scrollView).execute(mBkgrdImage);

            }

        } else {
            if (smBkgrdColor != null && !"N/A".equals(smBkgrdColor)) {

                buttonLinearLayout.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(smBkgrdColor));
                parentLinearLayout.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                scrollView.setBackgroundColor(Color.parseColor(smBkgrdColor));

            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {
                new ImageLoaderAsync1(parent).execute(smBkgrdImage);
                new ImageLoaderAsync1(parentLinearLayout).execute(smBkgrdImage);
                new ImageLoaderAsync1(scrollView).execute(smBkgrdImage);

            }

        }

        listIterator = mainMenuUnsortedList.listIterator();
        while (listIterator.hasNext()) {
            MainMenuBO mMainMenuBO = listIterator.next();

            if ("City Experience".equalsIgnoreCase(mMainMenuBO
                    .getLinkTypeName())) {
                hasExp = true;
                break;
            }

        }

        createButtonsScreens(mainMenuUnsortedList, hasExp);
        createbottomButtontTab(parent);
        setContentView(parent);
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            HubCityContext.level = 0;
        } else {
            HubCityContext.level = HubCityContext.level + 1;
        }
    }


    class ImageLoaderAsync1 extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync1(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {

            mDialog = ProgressDialog.show(TwoColumnMenuTemplate.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            BitmapDrawable background = new BitmapDrawable(bmImg);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                bmImage.setBackgroundDrawable(background);
            } else {
                bmImage.setBackgroundDrawable(background);
            }

            try {
                if ((this.mDialog != null) && this.mDialog.isShowing()) {
                    this.mDialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                e.printStackTrace();
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                this.mDialog = null;
            }
        }
    }

    @Override
    protected void onPause() {
        listIterator = null;
        super.onPause();
    }

    @Override
    protected void onResume() {
        buttonCounter = 1;

        if (isOnNewIntent == false) {
            handleIntent(getIntent());
            isOnNewIntent = true;
        } else {
            handleIntent(getIntent());
        }
        listIterator = mainMenuUnsortedList.listIterator();

        while (listIterator.hasNext()) {
            MainMenuBO mMainMenuBO = listIterator.next();

            if ("City Experience".equalsIgnoreCase(mMainMenuBO
                    .getLinkTypeName())) {
                hasExp = true;

                break;
            }
        }
        activity = this;
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) parentLinearLayout.findViewById(R.id.custom_navigation), false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }

    private void createButtonsScreens(
            ArrayList<MainMenuBO> mainMenuUnsortedList, boolean isExperiecne) {
        // calculation row & col

        int TOTAL_BUTTON;
        if (isExperiecne) {
            buttonCounter = 1;
            TOTAL_BUTTON = mainMenuUnsortedList.size() - 1;
        } else {
            TOTAL_BUTTON = mainMenuUnsortedList.size();
        }

        int totalRow;
        if (TOTAL_BUTTON % TOTAL_COLUMN != 0) {

            totalRow = TOTAL_BUTTON / TOTAL_COLUMN;
            totalRow = totalRow + 1;
        } else {
            totalRow = TOTAL_BUTTON / TOTAL_COLUMN;
        }
        // create parebt layout

        if (isExperiecne) {

            // manually add the row for exp layout
            LinearLayout rowLayout = createRowLayout();
            Button button = new Button(this);
            button.setOnClickListener(this);
            button = createButtonProperties(button,
                    mainMenuUnsortedList.get(0), isExperiecne,
                    previousMenuLevel);
            rowLayout.addView(button);
            buttonLinearLayout.addView(rowLayout);

        }

        for (int row = 0; row < totalRow; row++) {
            LinearLayout rowLayout = createRowLayout();

            for (int column = 0; column < TOTAL_COLUMN; column++) {
                if (buttonCounter <= TOTAL_BUTTON
                        && buttonCounter < mainMenuUnsortedList.size()) {

                    Button button = new Button(this);
                    button.setOnClickListener(this);

                    MainMenuBO buttonMainMenu = mainMenuUnsortedList
                            .get(buttonCounter);
                    button = createButtonProperties(button, buttonMainMenu,
                            false, previousMenuLevel);

                    rowLayout.addView(button);

                    buttonCounter++;

                }
            }
            buttonLinearLayout.addView(rowLayout);
        }

        buttonLinearLayout.setPadding(0, 0, 0, screenHeight / 11);

    }

    private LinearLayout createRowLayout() {
        LinearLayout rowLayout = new LinearLayout(this);
        rowLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        rowLayout.setLayoutParams(lp);
        return rowLayout;
    }

    private Button createButtonProperties(Button button, MainMenuBO item,
                                          boolean isExperiecne, String level) {
        String btnFontColor, btnColor;
        String itemName = item.getmItemName();
        if (level != null && "1".equals(level)) {
            btnFontColor = item.getmBtnFontColor();
            btnColor = item.getmBtnColor();

        } else {
            btnFontColor = item.getSmBtnFontColor();
            btnColor = item.getSmBtnColor();

        }

        button.setTextSize(13);
        button.setMaxLines(2);
        button.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

        button.setText(itemName);

        if (btnFontColor != null) {
            button.setTextColor(Color.parseColor(btnFontColor));
        } else {
            button.setTextColor(Color.WHITE);
        }

        LinearLayout.LayoutParams buttonLayoutParam;
        if (isExperiecne) {
            buttonLayoutParam = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        } else {
            buttonLayoutParam = new LinearLayout.LayoutParams(
                    (screenWidth) / 2, LayoutParams.MATCH_PARENT);
        }

        button.setTag(itemName);
        button.setLayoutParams(buttonLayoutParam);
        button.setBackgroundResource(R.drawable.twocolumnbtnborder);

        LayerDrawable layer = (LayerDrawable) button.getBackground();

        GradientDrawable drawable = (GradientDrawable) layer
                .findDrawableByLayerId(R.id.item_btn_template);
        if (btnColor != null) {

            drawable.setColor(Color.parseColor(btnColor));

        } else {
            drawable.setColor(Color.BLACK);

        }
        return button;

    }

    @Override
    protected void handleIntent(Intent intent) {
        super.handleIntent(intent);

        if (intent != null) {
            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);

            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");

            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");

            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            if (intent.getExtras().getString("smBkgrdImage") != null
                    && !intent.getExtras().getString("smBkgrdImage")
                    .equals("N/A"))
                mBkgrdImage = intent.getExtras().getString("smBkgrdImage");

            departFlag = intent.getExtras().getString("departFlag");

            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
        }
    }
}
