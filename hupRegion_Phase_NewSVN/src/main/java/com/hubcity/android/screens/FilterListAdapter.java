package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class FilterListAdapter extends BaseAdapter {
	private Activity activity;
	private ArrayList<HashMap<String, String>> filterList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	public FilterListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> filterList) {
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.filterList = filterList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return filterList.size();
	}

	@Override
	public Object getItem(int id) {
		return filterList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		if (convertView == null) {

			view = inflater.inflate(R.layout.lstitem_filter, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) view.findViewById(R.id.filter_name);
			viewHolder.imagePath = (ImageView) view
					.findViewById(R.id.filter_image);

			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();

		}
		viewHolder.name.setText(filterList.get(position).get(
				CityExperienceFilterActivity.TAG_FILTERNAME));
		viewHolder.imagePath.setTag(filterList.get(position).get(
				CityExperienceFilterActivity.TAG_FILTER_IMG_URL));
		customImageLoader.displayImage(
				filterList.get(position).get(
						CityExperienceFilterActivity.TAG_FILTER_IMG_URL),
				activity, viewHolder.imagePath);

		return view;
	}

	public static class ViewHolder {
		protected TextView name;
		protected ImageView imagePath;

	}
}
