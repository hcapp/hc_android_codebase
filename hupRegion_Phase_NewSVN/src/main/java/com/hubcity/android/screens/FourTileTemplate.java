package com.hubcity.android.screens;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 3/3/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class FourTileTemplate extends MenuPropertiesActivity {
    private LinearLayout mParentView;
    private int isDisplayLabel;
    private String btnBkgrdColor;
    private String btnLblColor;
    private ImageView mBannerImageView;
    private ProgressBar mProgressBar;
    private RelativeLayout mBannerHolder;
    private ScrollView mScrollViewParent;
    private int mScreenHeight;
    private LinearLayout mMenuButtons;
    private Context mContext;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;
    private View menuListTemplate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mContext = FourTileTemplate.this;
            CommonConstants.hamburgerIsFirst = true;

            Intent intent = getIntent();
            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);


            RelativeLayout Parent = new RelativeLayout(this);
            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            Parent.setLayoutParams(param);

            LinearLayout ParentIntermideate = new LinearLayout(this);
            ParentIntermideate.setOrientation(LinearLayout.VERTICAL);
            handleIntent(intent);
            bindView();
            setVisibilty();

            LinearLayout.LayoutParams gridViewParentParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            BottomButtonListSingleton mBottomButtonListSingleton;
            mBottomButtonListSingleton = BottomButtonListSingleton
                    .getListBottomButton();

            if (mBottomButtonListSingleton != null) {

                ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                        .listBottomButton;

                if (listBottomButton != null && listBottomButton.size() > 1) {
                    int px = (int) (50 * this.getResources().getDisplayMetrics().density + 0.5f);
                    gridViewParentParam.setMargins(0, 0, 0, px);
                }
            }

            mParentView.setLayoutParams(gridViewParentParam);
            LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            ParentIntermideate.setLayoutParams(parentParam);
            addTitleBar(ParentIntermideate, Parent);
            ParentIntermideate.addView(mParentView);
            Parent.addView(ParentIntermideate);

            createbottomButtontTab(Parent);
            setContentView(Parent);

            ViewTreeObserver observer = mParentView.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {


                    mScreenHeight = mScrollViewParent.getMeasuredHeight();
                    ViewTreeObserver obs = mScrollViewParent.getViewTreeObserver();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }
                    createMenuScreens(mainMenuUnsortedList);
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createMenuScreens(ArrayList<MainMenuBO> mainMenuUnsortedList) {
        try {
            int totalCount = mainMenuUnsortedList.size();
            int menuButtonHeight = mScreenHeight / 4;
            int textViewHeight = menuButtonHeight / 4;
            int itemCount = 0;
            for (int i = 0; i < totalCount; i++) {
                MainMenuBO buttonMainMenu = mainMenuUnsortedList
                        .get(itemCount);

                FrameLayout rowLayout = createRowLayout(menuButtonHeight, itemCount);

                FrameLayout imageLay = createImageView(menuButtonHeight, buttonMainMenu, rowLayout);

                FrameLayout textLay = new FrameLayout(this);
                FrameLayout.LayoutParams frameParam = new FrameLayout.LayoutParams(ViewGroup
                        .LayoutParams.MATCH_PARENT, textViewHeight);
                frameParam.gravity = Gravity.BOTTOM;
                textLay.setLayoutParams(frameParam);

                TextView textAlphaView = new TextView(this);
                FrameLayout.LayoutParams textAlphaParam = new FrameLayout.LayoutParams(ViewGroup
                        .LayoutParams.MATCH_PARENT, textViewHeight);
                textAlphaView.setLayoutParams(textAlphaParam);
                textAlphaView.setAlpha((float) 0.6);
                if(btnBkgrdColor != null) {
                    textAlphaView.setBackgroundColor(Color.parseColor(btnBkgrdColor));
                }
                textLay.addView(textAlphaView);


                TextView text = createTextView(textViewHeight, buttonMainMenu);
                textLay.addView(text);

                imageLay.addView(textLay);

                mMenuButtons.addView(imageLay);
                itemCount++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private TextView createTextView(int textHeight, MainMenuBO buttonMainMenu) {
        TextView text = new TextView(this);
        FrameLayout.LayoutParams textParam = new FrameLayout.LayoutParams(ViewGroup.LayoutParams
                .MATCH_PARENT, textHeight);
        text.setLayoutParams(textParam);
        text.setGravity(Gravity.CENTER_VERTICAL);
        if (isDisplayLabel == 1) {
            text.setTextColor(Color.parseColor(btnLblColor));
            text.setText(buttonMainMenu.getmItemName());
            text.setTypeface(Typeface.DEFAULT_BOLD);
            text.setPadding(15, 0, 0, 0);
            float textSize = 18f;
            text.setTextSize(textSize);
        }
        return text;
    }

    private FrameLayout createImageView(int imageHeight, MainMenuBO buttonMainMenu, FrameLayout
            rowLayout) {
        ImageView image = new ImageView(this);
        FrameLayout.LayoutParams imageParam = new FrameLayout.LayoutParams(ViewGroup.LayoutParams
                .MATCH_PARENT, imageHeight);
        image.setAdjustViewBounds(true);
        image.setScaleType(ImageView.ScaleType.FIT_XY);
        CustomImageLoader mRoundCornerImageLoader = new CustomImageLoader
                (FourTileTemplate.this,
                        false);
        String mMenuItemImg = buttonMainMenu.getmItemImgUrl();
        mRoundCornerImageLoader.displayImage(mMenuItemImg, FourTileTemplate.this,
                image);
        image.setLayoutParams(imageParam);
        rowLayout.addView(image);

        final ProgressBar menuProgressBar = new ProgressBar(this, null, android.R.attr
                .progressBarStyleInverse);
        FrameLayout.LayoutParams progressParam = new FrameLayout.LayoutParams(ViewGroup
                .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        progressParam.gravity = Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL;
        menuProgressBar.setLayoutParams(progressParam);
        rowLayout.addView(menuProgressBar);

        mMenuItemImg = mMenuItemImg.replaceAll(" ", "%20");
        Picasso.with(FourTileTemplate.this).load(mMenuItemImg).into(image, new Callback() {
            @Override
            public void onSuccess() {
                menuProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                menuProgressBar.setVisibility(View.GONE);
            }
        });

        return rowLayout;
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);
        mMainMenuBO.setmBkgrdColor(mBkgrdColor);
        mMainMenuBO.setmBkgrdImage(mBkgrdImage);
        mMainMenuBO.setSmBkgrdColor(smBkgrdColor);
        linkClickListener(mMainMenuBO.getLinkTypeName(), mMainMenuBO);
    }

    private FrameLayout createRowLayout(int imageTextHeight, int itemCount) {
        FrameLayout rowLayout = new FrameLayout(this);
        rowLayout.setOnClickListener(this);
        rowLayout.setTag(itemCount);
        LinearLayout.LayoutParams rowLayParam = new LinearLayout.LayoutParams(ViewGroup
                .LayoutParams.MATCH_PARENT, imageTextHeight);
        rowLayout.setLayoutParams(rowLayParam);
        return rowLayout;
    }

    private void setVisibilty() {
        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if (null != mBannerImg) {
                mBannerImageView.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(FourTileTemplate.this).load(mBannerImg).into(mBannerImageView, new
                        Callback() {
                            @Override
                            public void onSuccess() {
                                mProgressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                mProgressBar.setVisibility(View.GONE);
                            }
                        });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        } else {
            customHorizontalTicker = (CustomHorizontalTicker) menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker) menuListTemplate.findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher) menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext, customHorizontalTicker, customVerticalTicker, bannerParent, customRotate, mBannerHolder);
        }

    }

    private void bindView() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);

        menuListTemplate = layoutInflater.inflate(R.layout.four_image_tile,
                null, false);
        // Initializing drawer layout
        drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);
        mBannerImageView = (ImageView) menuListTemplate
                .findViewById(R.id.top_image);
        mParentView = (LinearLayout) menuListTemplate.findViewById(R.id.fourImageUpDownViewParent);
        mProgressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
        mBannerHolder = (RelativeLayout) menuListTemplate
                .findViewById(R.id.banner_holder);
        bannerParent = (LinearLayout) menuListTemplate.findViewById(R.id.bannerParent);
        mScrollViewParent = (ScrollView) menuListTemplate
                .findViewById(R.id.scrollViewParent);
        mMenuButtons = (LinearLayout) menuListTemplate
                .findViewById(R.id.menuButtons);
    }

    @Override
    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);

            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            smBkgrdImage = intent.getExtras().getString("smBkgrdImage");
            departFlag = intent.getExtras().getString("departFlag");
            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
            mFontColor = intent.getExtras().getString("mFontColor");
            smFontColor = intent.getExtras().getString("smFontColor");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");
            if (intent.hasExtra("isDisplayLabel")) {
                isDisplayLabel = intent.getExtras().getInt("isDisplayLabel");
            }
            if (intent.hasExtra("btnBkgrdColor")) {
                btnBkgrdColor = intent.getExtras().getString("btnBkgrdColor");
            }
            if (intent.hasExtra("btnLblColor")) {
                btnLblColor = intent.getExtras().getString("btnLblColor");
            }

        }

        super.handleIntent(intent);

    }

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activity = this;
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }
}
