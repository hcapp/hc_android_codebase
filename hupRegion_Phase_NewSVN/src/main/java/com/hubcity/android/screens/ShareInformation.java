package com.hubcity.android.screens;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.ImageLoaderAsync;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.twitter.PrepareRequestTokenActivity;
import com.hubcity.twitter.TwitterUtils;
import com.scansee.hubregion.R;

public class ShareInformation {

    private SharedPreferences prefs;
    private PopupWindow pwindo;
    public static Button btnClosePopup, btnRedeem;
    Button btnTwitterShare, btnSMSShare, btnEmailShare;
    Button btnFacebookShare;

    private final Handler mTwitterHandler = new Handler();
    private ProgressDialog mDialog;

    Activity activity;

    String moduleId = "", module = "";
    String retailerId = "", retailLocationId = "";
    String pageId = "";
    HashMap<String, String> shareInfoValues = new HashMap<>();

    // Constructor for Product Details/Events/Fundraisers/Coupons/Hot Deals/Band Summery here moduleId is nothing but bandId
    public ShareInformation(Activity activity, String moduleId, String module) {
        this.activity = activity;
        this.moduleId = moduleId;
        this.module = module;

    }

    // Constructor for Appsite/SplOffer/Anything
    public ShareInformation(Activity activity, String retailerId,
                            String retailLocationId, String pageId, String module) {
        this.activity = activity;
        this.retailerId = retailerId;
        this.retailLocationId = retailLocationId;
        this.pageId = module;
        this.module = module;
        moduleId = pageId;
    }

    public void shareTask(boolean isShareCalled) {
        if (!isShareCalled) {
            new ShareAsyncTask().execute();
        } else {
            initiatePopupWindow();
        }

    }

    /**
     * On click of Share bottom button a popup window is shown
     */
    private void initiatePopupWindow() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.share_screen_popup,
                    (ViewGroup) activity.findViewById(R.id.popup_element));
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            int screenHeight = metrics.heightPixels;
            pwindo = new PopupWindow(layout, screenWidth, screenHeight / 2,
                    true);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    pwindo.showAtLocation(layout, Gravity.BOTTOM, 0, 0);
                }
            });


            TextView headerTxt = (TextView) layout.findViewById(R.id.txtView);
            if (module.equalsIgnoreCase("Coupons")) {
                headerTxt.setText("Share Deals Via");

            } else if (module.equalsIgnoreCase("NewsDetails")) {
                headerTxt.setText("Share " + activity.getString(R.string.share_news) + " Via");
            } else {
                headerTxt.setText("Share " + getmoduleName(module) + " Via");
            }

            btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);

            btnFacebookShare = (Button) layout
                    .findViewById(R.id.btn_facebook_popup);
            btnTwitterShare = (Button) layout
                    .findViewById(R.id.btn_twitter_popup);
            btnSMSShare = (Button) layout.findViewById(R.id.btn_text_popup);
            btnEmailShare = (Button) layout.findViewById(R.id.btn_email_popup);

            btnFacebookShare.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                    shareViaFacebook();

                }
            });
            btnTwitterShare.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    shareViaTwitter();
                }

            });
            btnSMSShare.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                    shareViaSMS();

                }

            });
            btnEmailShare.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                    shareViaEMail();
                }

            });

            btnClosePopup.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getmoduleName(String module) {
        String moduleName = "";

        if ("Specials".equals(module)) {
            moduleName = "Special Offers";

        } else if ("Anything".equals(module)) {
            moduleName = "Anything Page";

        } else if ("Events".equals(module)) {
            moduleName = "Event Details";

        } else if ("Fundraiser".equals(module)) {
            moduleName = "Fundraiser Details";

        } else if ("BandSummery".equals(module)) {
            moduleName = "Band Details";
        } else {
            moduleName = module;

        }

        return moduleName;
    }

    /**
     * Intent call to EmailShareActivity class
     */
    private void shareViaEMail() {

        HashMap<String, String> shareDetailsList = new HashMap<>();
        shareDetailsList.put("module", module);
        shareDetailsList.put("moduleId", moduleId);
        shareDetailsList.put("retailerId", retailerId);
        shareDetailsList.put("retailLocationId", retailLocationId);
        shareDetailsList.put("pageId", pageId);

/*

        UserTrackingTask userTracking = new UserTrackingTask(activity,
                module, "Email", "", moduleId, retailerId,
                retailLocationId, pageId);
        userTracking.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
*/

        EmailAsyncTask emailAsyncTask = new EmailAsyncTask(activity,
                shareDetailsList);
        emailAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        ;
    }

    /**
     * Sharing link via Email
     */
    private void shareViaSMS() {
        StringBuffer stringBuffer = new StringBuffer();
        if (module.equalsIgnoreCase("NewsDetails")) {
            stringBuffer.append(shareInfoValues.get("shareText") + "\n");
        }
        else
        {
            stringBuffer.append(getShareDescription());
        }
        stringBuffer.append(shareInfoValues.get("name"));
        stringBuffer.append("\n");
        if (!shareInfoValues.get("url").equalsIgnoreCase("N/A")) {
            stringBuffer.append(shareInfoValues.get("url"));
        }
        stringBuffer.append("\n");

        if (stringBuffer.length() > 0) {
            Intent smsShare = new Intent(Intent.ACTION_VIEW);
            smsShare.setData(Uri.parse("sms:"));
            try {
                smsShare.putExtra("sms_body",
                        URLDecoder.decode(stringBuffer.toString(), "UTF-8"));
                activity.startActivity(smsShare);

              /*  UserTrackingTask userTracking = new UserTrackingTask(activity,
                        module, "SMS", "", moduleId, retailerId,
                        retailLocationId, pageId);
                userTracking.execute();*/

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                Toast.makeText(activity.getApplicationContext(),
                        "Unable to SMS this Content.", Toast.LENGTH_SHORT)
                        .show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(activity.getApplicationContext(),
                        "SMS Option Not Available", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /**
     * Share link via Facebook
     */
    private void shareViaFacebook() {

        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.fb_share_popup);
        dialog.setTitle("Share via FaceBook");

        final EditText eText = (EditText) dialog.findViewById(R.id.text);

        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        if (shareInfoValues.get("imagePath") != null
                && !"N/A".equalsIgnoreCase(shareInfoValues.get("imagePath"))) {

            new ImageLoaderAsync(image).execute(shareInfoValues
                    .get("imagePath"));
        }

        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog
                .findViewById(R.id.dialogButtonCancel);

        dialogButtonOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new checkUrlStatusAsy(dialog, eText).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        dialogButtonCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    class checkUrlStatusAsy extends AsyncTask<Boolean, Boolean, Boolean> {

        private final Dialog dialog;
        private final EditText eText;
        private Intent facebookIntent;

        public checkUrlStatusAsy(Dialog dialog, EditText eText) {
            this.dialog = dialog;
            this.eText = eText;
        }

        @Override
        protected Boolean doInBackground(Boolean... params) {
            facebookIntent = new Intent(activity,
                    FaceBookActivity.class);
            if (CommonConstants.isExists(shareInfoValues.get("imagePath"))) {
                if (shareInfoValues.get("imagePath") != null
                        && !"N/A".equalsIgnoreCase(shareInfoValues.get("imagePath"))) {
                    facebookIntent.putExtra("imagePath", shareInfoValues
                            .get("imagePath"));
                }

            } else {
                URL url = null;
                try {
                    url = new URL(shareInfoValues.get("imagePath"));
                    HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
                    ucon.setInstanceFollowRedirects(false);
                    URL secondURL = new URL(ucon.getHeaderField("Location"));
                    facebookIntent.putExtra("imagePath", secondURL.toString());

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (shareInfoValues.get("name") != null) {
                facebookIntent.putExtra("description", getShareDescription() + "\n" + shareInfoValues.get("name"));

            } else {
                facebookIntent.putExtra("description", getShareDescription());
            }
            facebookIntent.putExtra("userText", eText.getText().toString());
            facebookIntent.putExtra("module", "share");
            facebookIntent.putExtra("appPlayStoreLink",
                    shareInfoValues.get("url"));
            facebookIntent.putExtra("moduleType", module);
            String[] shareDetails = {module, "Facebook", "", moduleId,
                    retailerId, retailLocationId, pageId};
            facebookIntent.putExtra("shareDetails", shareDetails);
            dialog.dismiss();
            activity.startActivity(facebookIntent);
            super.onPostExecute(aBoolean);
        }
    }

    /**
     * Calls AsyncTask to share link via twitter
     */
    private void shareViaTwitter() {
        prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.fb_share_popup);
        dialog.setTitle("Share via Twitter");
        EditText text = (EditText) dialog.findViewById(R.id.text);
        text.setSingleLine(false);
        text.setEnabled(false);

        if ("NewsDetails".equalsIgnoreCase(module)) {
            text.setText(shareInfoValues.get("name") + "\n"
                    + shareInfoValues.get("url"));
        } else {
            text.setText(getShareDescription() + shareInfoValues.get("name") + "\n"
                    + shareInfoValues.get("url"));
        }


        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        if (shareInfoValues.get("imagePath") != null
                && !"N/A".equalsIgnoreCase(shareInfoValues.get("imagePath"))) {
            new ImageLoaderAsync(image).execute(shareInfoValues
                    .get("imagePath"));
        }

        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog
                .findViewById(R.id.dialogButtonCancel);

        dialogButtonOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new TweetMsg().execute();
            }
        });
        dialogButtonCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    /**
     * AsyncTask to post link on twitter
     *
     * @author rekha_p
     */
    public class TweetMsg extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            try {
                if (TwitterUtils.isAuthenticated(prefs)) {
                    sendTweet();
                } else {
                    Intent i = new Intent(activity.getApplicationContext(),
                            PrepareRequestTokenActivity.class);

                    if ("NewsDetails".equalsIgnoreCase(module)) {
                        i.putExtra("tweet_msg", shareInfoValues.get("name") + "\n"
                                + shareInfoValues.get("url"));
                    } else {
                        i.putExtra("tweet_msg", getShareDescription() +
                                "\n"
                                + shareInfoValues.get("url"));
                    }

                    if (shareInfoValues.get("imagePath") != null
                            && !"N/A".equalsIgnoreCase(shareInfoValues.get("imagePath"))) {
                        i.putExtra("image", shareInfoValues.get("imagePath"));
                    }
                    i.putExtra("module", "share");
                    String[] shareDetails = {module, "Twitter", "", moduleId,
                            retailerId, retailLocationId, pageId};
                    i.putExtra("shareDetails", shareDetails);
                    activity.startActivityForResult(i, 2505);
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }

    }

    private String getShareDescription() {

        StringBuilder description = new StringBuilder();

        if (!"Appsite".equals(module) && !"BandSummery".equalsIgnoreCase(module) && !"BandEventSummery".equalsIgnoreCase(module)
                ) {
            description.append("I found this ");

            description.append(getmoduleName(module));

            description.append(" in ");
            description.append(activity.getResources().getString(
                    R.string.app_name));
            description
                    .append(" mobile app and thought you might be interested:");
        }

        if (!description.equals("")) {
            description.append("\n");
        }
        if ("BandSummery".equalsIgnoreCase(module) || "BandEventSummery".equalsIgnoreCase(module)) {
            description.append(shareInfoValues.get("shareText"));
        }


        return description.toString();
    }

    /**
     * Posts link on twitter
     */
    public void sendTweet() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    TwitterUtils.sendTweet(prefs, getShareDescription()
                            + shareInfoValues.get("name") + "\n"
                            + shareInfoValues.get("url"), shareInfoValues.get("imagePath"));

                    mTwitterHandler.post(mUpdateTwitterNotification);
                } catch (Exception e) {
                    if (e != null && "" + e != null) {
                        if (e.getMessage().contains(
                                "Status is over 140 characters")) {
                            Toast.makeText(
                                    activity.getApplicationContext(),
                                    "Tweet not sent - Status is over 140 characters!",
                                    Toast.LENGTH_LONG).show();
                        } else if (e.getMessage().contains("duplicate")) {
                            Toast.makeText(activity.getApplicationContext(),
                                    "Tweet not sent - Status Duplicate!",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(activity.getApplicationContext(),
                                    "Tweet not sent - Error!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }

            }
        });
    }

    /**
     * Updates status of tweet
     */
    final Runnable mUpdateTwitterNotification = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(activity.getBaseContext(), "Tweet sent !",
                    Toast.LENGTH_LONG).show();
            btnClosePopup.performClick();

        }
    };

    class ShareAsyncTask extends AsyncTask<Void, Void, Void> {

        private JSONObject jsonResponse = null;
        String responseText = null;
        String responseCode = null;

        UrlRequestParams urlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mDialog = ProgressDialog.show(activity, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            String urlParameters = "";
            String url = "";

            if ("Product Details".equals(module)) {
                urlParameters = urlRequestParams
                        .createProductShareParam(moduleId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/shareproductretailerinfo";

            } else if ("Appsite".equals(module)) {
                urlParameters = urlRequestParams.createAppsiteShareParam(
                        retailerId, retailLocationId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/shareappsite";

            } else if ("Specials".equals(module) || "Anything".equals(module)) {
                urlParameters = urlRequestParams.createSplOfferShareParam(
                        retailerId, pageId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharespecialoff";

            } else if ("Events".equals(module)) {
                urlParameters = urlRequestParams
                        .createEventsShareParam(moduleId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/shareevent";

            } else if ("Fundraiser".equals(module)) {
                urlParameters = urlRequestParams
                        .createFundraiserShareParam(moduleId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharefundraiser";

            } else if ("Coupons".equals(module)) {
                urlParameters = urlRequestParams
                        .createCouponsShareParam(moduleId);

                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharecoupon";

            } else if ("NewsDetails".equals(module)) {
                urlParameters = urlRequestParams
                        .createNewsShareParam(moduleId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharenewsbymedia";
            } else if ("HotDeals".equals(module)) {
                urlParameters = urlRequestParams
                        .createHotDealsShareParam(moduleId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharehotdeal";

            } else if ("BandSummery".equals(module)) {
                urlParameters = urlRequestParams
                        .createBandSummeryParam(moduleId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/shareband";

            } else if ("BandEventSummery".equals(module)) {
                urlParameters = urlRequestParams
                        .createBandEventSummeryParam(moduleId);
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharebandevent";
            }


            jsonResponse = mServerConnections.getUrlPostResponse(url,
                    urlParameters, true);

            try {
                if (jsonResponse != null) {

                    if (jsonResponse.has("responseCode")) {
                        responseCode = jsonResponse.getString("responseCode");
                    }

                    if (jsonResponse.has("responseText")) {
                        responseText = jsonResponse.getString("responseText");
                    }

                    shareInfoValues = new HashMap<>();
                    JSONObject shareJson;

                    // For Product Details
                    if (jsonResponse.has("ProductRetailerInfo")) {

                        shareJson = jsonResponse
                                .getJSONObject("ProductRetailerInfo");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }

                            JSONObject prodJson = shareJson
                                    .getJSONObject("productDetail");

                            if (prodJson != null) {

                                if (prodJson.has("productName")) {
                                    shareInfoValues.put("name",
                                            prodJson.getString("productName"));
                                }

                                if (prodJson.has("qrUrl")) {
                                    shareInfoValues.put("url",
                                            prodJson.getString("qrUrl"));
                                }

                                if (prodJson.has("imagePath")) {
                                    shareInfoValues.put("imagePath",
                                            prodJson.getString("imagePath"));
                                }
                            }

                        }
                    }
                    // For News Template

                    try {

                        if (jsonResponse.has("item")) {
                            JSONObject item = jsonResponse
                                    .getJSONObject("item");
                            responseCode = item
                                    .optString("responseCode");
                            responseText = item
                                    .optString("responseText");
                            if (item.has("title")) {
                                shareInfoValues.put("name",
                                        item.optString("title"));
                            }

                            if (item.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        item.optString("qrUrl"));
                            }
                            if (item.has("shareText")) {
                                shareInfoValues.put("shareText",
                                        item.optString("shareText"));
                            }

                            if (item.has("image")) {
                                shareInfoValues.put("imagePath",
                                        item.optString("image"));
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    // For Appsite
                    if (jsonResponse.has("RetailersDetails")
                            && "Appsite".equals(module)) {

                        shareJson = jsonResponse
                                .getJSONObject("RetailersDetails");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }

                            if (shareJson.has("shareText")) {
                                shareInfoValues.put("name",
                                        shareJson.getString("shareText"));
                            }

                            if (shareJson.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        shareJson.getString("qrUrl"));
                            }

                            if (shareJson.has("retImage")) {
                                shareInfoValues.put("imagePath",
                                        shareJson.getString("retImage"));
                            }

                        }
                    }

                    // For Specials
                    if (jsonResponse.has("RetailersDetails")
                            && ("Specials".equals(module) || "Anything"
                            .equals(module))) {

                        shareJson = jsonResponse
                                .getJSONObject("RetailersDetails");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }

                            if (shareJson.has("pageTitle")) {
                                shareInfoValues.put("name",
                                        shareJson.getString("pageTitle"));
                            }

                            if (shareJson.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        shareJson.getString("qrUrl"));
                            }

                            if (shareJson.has("retImage")) {
                                shareInfoValues.put("imagePath",
                                        shareJson.getString("retImage"));
                                // shareInfoValues.put("imagePath",
                                // shareJson.getString("appIcon"));
                            }

                        }
                    }
                    // Added by sharanamma
                    // For Band summery
                    if (jsonResponse.has("RetailersDetails")
                            && ("BandSummery".equals(module))) {
                        shareJson = jsonResponse
                                .getJSONObject("RetailersDetails");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }
                            if (shareJson.has("shareText")) {
                                shareInfoValues.put("shareText",
                                        shareJson.getString("shareText"));
                            }

                            if (shareJson.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        shareJson.getString("qrUrl"));
                            }

                            if (shareJson.has("retImage")) {
                                shareInfoValues.put("imagePath",
                                        shareJson.getString("retImage"));
                            }
                            if (shareJson.has("retName")) {
                                shareInfoValues.put("name",
                                        shareJson.getString("retName"));
                            }
                        }
                    }
                    // For Events
                    if (jsonResponse.has("EventDetails")) {
                        shareJson = jsonResponse.getJSONObject("EventDetails");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }

                            if (shareJson.has("eventName")) {
                                shareInfoValues.put("name",
                                        shareJson.getString("eventName"));
                            }

                            if (shareJson.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        shareJson.getString("qrUrl"));
                            }

                            if (shareJson.has("imagePath")) {
                                shareInfoValues.put("imagePath",
                                        shareJson.getString("imagePath"));
                            }

                        }
                    }
                    // For Band Events summery
                    if (jsonResponse.has("EventDetails") && ("BandEventSummery".equals(module))) {
                        shareJson = jsonResponse.getJSONObject("EventDetails");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }

                            if (shareJson.has("eventName")) {
                                shareInfoValues.put("name",
                                        shareJson.getString("eventName"));
                            }

                            if (shareJson.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        shareJson.getString("qrUrl"));
                            }

                            if (shareJson.has("imagePath")) {
                                shareInfoValues.put("imagePath",
                                        shareJson.getString("imagePath"));
                            }
                            if (shareJson.has("shareText")) {
                                shareInfoValues.put("shareText",
                                        shareJson.getString("shareText"));
                            }

                        }
                    }

                    // For Fundraiser
                    if (jsonResponse.has("Fundraiser")) {
                        shareJson = jsonResponse.getJSONObject("Fundraiser");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }

                            if (shareJson.has("fundName")) {
                                shareInfoValues.put("name",
                                        shareJson.getString("fundName"));
                            }

                            if (shareJson.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        shareJson.getString("qrUrl"));
                            }

                            if (shareJson.has("imagePath")) {
                                shareInfoValues.put("imagePath",
                                        shareJson.getString("imagePath"));
                            }

                        }
                    }

                    // For Coupons
                    if (jsonResponse.has("CouponDetails")) {

                        shareJson = jsonResponse.getJSONObject("CouponDetails");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }

                            if (shareJson.has("couponName")) {
                                shareInfoValues.put("name",
                                        shareJson.getString("couponName"));
                            }

                            if (shareJson.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        shareJson.getString("qrUrl"));
                            }

                            if (shareJson.has("couponImagePath")) {
                                shareInfoValues.put("imagePath",
                                        shareJson.getString("couponImagePath"));
                            }

                        }
                    }

                    // For Hot Deals
                    if (jsonResponse.has("HotDealsDetails")) {

                        shareJson = jsonResponse
                                .getJSONObject("HotDealsDetails");

                        if (shareJson != null) {
                            if (shareJson.has("responseCode")) {
                                responseCode = shareJson
                                        .getString("responseCode");
                            }

                            if (shareJson.has("responseText")) {
                                responseText = shareJson
                                        .getString("responseText");
                            }

                            if (shareJson.has("hotDealName")) {
                                shareInfoValues.put("name",
                                        shareJson.getString("hotDealName"));
                            }

                            if (shareJson.has("qrUrl")) {
                                shareInfoValues.put("url",
                                        shareJson.getString("qrUrl"));
                            }

                            if (shareJson.has("hotDealImagePath")) {
                                shareInfoValues
                                        .put("imagePath", shareJson
                                                .getString("hotDealImagePath"));
                            }

                        }
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mDialog.dismiss();

            if ("10000".equals(responseCode)) {

                initiatePopupWindow();
            } else {
                Toast.makeText(activity, responseText, Toast.LENGTH_SHORT).show();
            }
        }

    }

}
