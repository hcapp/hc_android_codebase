package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class FindSingleCategorySortActivity extends CustomTitleBar implements
		OnClickListener {

	TextView atozGroup;
	TextView typeGroup;

	CheckBox atozGroupCheck;
	CheckBox typeGroupCheck;

	TextView nameSort;
	TextView distanceSort;
	TextView citySort;

	CheckBox nameSortCheck;
	CheckBox distanceSortCheck;
	CheckBox citySortCheck;

	String groupSelection;
	String sortSelection;

	HubCityContext hubCiti;

	Spinner mspinnerType;
	ArrayList<String> groupType;
	HashMap<String, String> mTypeDetails;
	ArrayList<HashMap<String, String>> mList;
	String returnValue = "unSuccessfull";
	ArrayList<String> mSpinnerData;
	String subCatId;

	TableRow mTableRow2GroupAtoZ;
	TableRow mTableRow2GroupType;
	TableRow mTableRowSortName;
	TableRow mTableRowSortDistance;
	TableRow mTableRowCitySort;

	String subCatIdRef;
	String group;
	String sort;
	String mLinkId;
	String catId;
	String catName;

	boolean flag;
	String typeText;
	ArrayList<HashMap<String, String>> catIdList;

	LinearLayout ll;
	CitySort citySorting;
	Activity activity;

	boolean isAtoZ, isAtoZRef;

	private ProgressDialog mDialog;


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_single_category_sort_activity);

		activity = this;

		ll = (LinearLayout) findViewById(R.id.city_ll);
		CitySort.isDefaultCitySort = false;

		backImage.setVisibility(View.GONE);
		title.setText("Group & Sort");
		rightImage.setBackgroundResource(R.drawable.ic_action_accept);
		leftTitleImage.setBackgroundResource(R.drawable.ic_action_cancel);

		hubCiti = (HubCityContext) getApplicationContext();
		groupSelection = hubCiti.getFindSingleGroupSelection();
		sortSelection = hubCiti.getFindSortSelection();

		if (getIntent().hasExtra("sortChoice")) {
			sort = getIntent().getExtras().getString("sortChoice");
		} else {
			sort = sortSelection;
		}

		if (getIntent().hasExtra("groupChoice")) {
			group = getIntent().getExtras().getString("groupChoice");
		} else {
			group = groupSelection;
		}

		mspinnerType = (Spinner) findViewById(R.id.find_singl_ecategory_type_spinner);

		if (getIntent().hasExtra("isAtoZ")) {
			isAtoZRef = getIntent().getExtras().getBoolean("isAtoZ");
			isAtoZ = isAtoZRef;
		}

		if (getIntent().hasExtra("subCatId")) {
			subCatIdRef = getIntent().getExtras().getString("subCatId");
			subCatId = subCatIdRef;
		}

		if (getIntent().hasExtra("mLinkId")) {
			mLinkId = getIntent().getExtras().getString("mLinkId");

		}

		if (getIntent().hasExtra("catId")) {
			catId = getIntent().getExtras().getString("catId");

		}

		if (getIntent().hasExtra("catName")) {
			catName = getIntent().getExtras().getString("catName");
		}

		atozGroup = (TextView) findViewById(R.id.event_textView_group_atoz);
		atozGroup.setOnClickListener(this);

		typeGroup = (TextView) findViewById(R.id.event_textView_group_type);
		typeGroup.setOnClickListener(this);

		atozGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox2);
		typeGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox3);

		if ("atoz".equals(group)) {
			isAtoZ = true;
			atozGroupCheck.setVisibility(View.VISIBLE);
		}
		if ("type".equals(group)) {
			typeGroupCheck.setVisibility(View.VISIBLE);
		}

		nameSort = (TextView) findViewById(R.id.event_textView_sort_name);
		distanceSort = (TextView) findViewById(R.id.event_textView_sort_distance);
		citySort = (TextView) findViewById(R.id.textView_sort_city);

		citySort.setOnClickListener(this);
		nameSort.setOnClickListener(this);
		distanceSort.setOnClickListener(this);

		nameSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox2);
		distanceSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox3);
		citySortCheck = (CheckBox) findViewById(R.id.sortby_city_checkBox);

		if ("name".equals(sort)) {
			nameSortCheck.setVisibility(View.VISIBLE);
		}
		if ("distance".equals(sort)) {
			distanceSortCheck.setVisibility(View.VISIBLE);
		}

		if ("city".equals(sort)) {
			citySortCheck.setVisibility(View.VISIBLE);
			CitySort.isDefaultCitySort = true;
		}

		// Get GPS Values
		getGPSValues();

		// @Rekha
		// Check for Addison Filters

		mTableRow2GroupAtoZ = (TableRow) findViewById(R.id.find_single_category_tableRow3);
		mTableRow2GroupType = (TableRow) findViewById(R.id.find_single_category_tableRow4);
		mTableRowSortName = (TableRow) findViewById(R.id.find_single_category_tableRow7);
		mTableRowSortDistance = (TableRow) findViewById(R.id.find_single_category_tableRow8);
		mTableRowCitySort = (TableRow) findViewById(R.id.sortby_city_tableRow);

		mTableRow2GroupAtoZ.setOnClickListener(this);
		mTableRow2GroupType.setOnClickListener(this);
		mTableRowSortName.setOnClickListener(this);
		mTableRowSortDistance.setOnClickListener(this);
		mTableRowCitySort.setOnClickListener(this);

		mspinnerType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				mspinnerType.setSelection(position);

				if (position != 0) {

					groupSelection = "type";
					subCatId = position + "";
					atozGroupCheck.setVisibility(View.INVISIBLE);
					typeGroupCheck.setVisibility(View.VISIBLE);
					mspinnerType.setVisibility(View.INVISIBLE);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		rightImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
					hubCiti.setFindSingleGroupSelection(groupSelection);
					hubCiti.setFindSortSelection(sortSelection);

					Intent launchintent = new Intent(
							FindSingleCategorySortActivity.this,
							FindSingleCategory.class);
					launchintent.putExtra("subCatId", subCatId);
					launchintent.putExtra("mLinkId", mLinkId);
					launchintent.putExtra("catName", catName);
					launchintent.putExtra("catId", getIntent().getExtras()
							.getString("catId"));
					launchintent.putExtra("mItemId", getIntent().getExtras()
							.getString("mItemId"));
					launchintent.putExtra(
							Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
							getIntent().getExtras().getString(
									Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
					launchintent.putExtra("latitude", getIntent().getExtras()
							.getString("latitude"));
					launchintent.putExtra("longitude", getIntent().getExtras()
							.getString("longitude"));
					launchintent.putExtra("srchKey", getIntent().getExtras()
							.getString("srchKey"));
					launchintent.putExtra("isSort", true);
					launchintent.putExtra("isAtoZ", isAtoZ);

					startActivity(launchintent);
					finish();

			}
		});
		leftTitleImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				hubCiti.setFindGroupSelection(group);
				hubCiti.setFindSortSelection(sort);

				Intent launchintent = new Intent(
						FindSingleCategorySortActivity.this,
						FindSingleCategory.class);

				launchintent.putExtra("subCatId", subCatIdRef);
				launchintent.putExtra("mLinkId", mLinkId);
				launchintent.putExtra("catName", catName);
				launchintent.putExtra("catId", getIntent().getExtras()
						.getString("catId"));
				launchintent.putExtra("mItemId", getIntent().getExtras()
						.getString("mItemId"));
				launchintent.putExtra(
						Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
						getIntent().getExtras().getString(
								Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
				launchintent.putExtra("latitude", getIntent().getExtras()
						.getString("latitude"));
				launchintent.putExtra("longitude", getIntent().getExtras()
						.getString("longitude"));
				launchintent.putExtra("srchKey", getIntent().getExtras()
						.getString("srchKey"));
				launchintent.putExtra("isAtoZ", isAtoZRef);

				startActivity(launchintent);
				finish();

			}
		});

		if (Properties.isRegionApp) {
			mTableRowCitySort.setVisibility(View.VISIBLE);
			citySorting = new CitySort(activity, ll, citySortCheck,
					mTableRowCitySort, "Find Single", getIntent().getExtras()
							.getString("mItemId"), getIntent().getExtras()
							.getString(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA),
					null, getIntent().getExtras().getString("latitude"),
					getIntent().getExtras().getString("longitude"), getIntent()
							.getExtras().getString("catName"), getIntent()
							.getExtras().getString("srchKey"), getIntent()
							.getExtras().getString("catId"), subCatId);
		} else {
			mTableRowCitySort.setVisibility(View.GONE);
		}

	}

	public void onResume() {
		super.onResume();
		mLinkId = Constants.FIND_SINGLE_CAT_LINK_ID;
		new TypeAsyncTask().execute();
	}

	private void createAlert(boolean flag) {

		CharSequence[] items = null;

		AlertDialog.Builder builder = new AlertDialog.Builder(
				FindSingleCategorySortActivity.this);

		catIdList = mList;

		ArrayList<String> data = mSpinnerData;

		if (data != null) {
			items = new CharSequence[data.size()];

			for (int i = 0; i < data.size(); i++) {

				items[i] = data.get(i);

			}
		}

		builder.setTitle(typeText);
		if (flag) {
			builder.setPositiveButton("OK", null);
		}

		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int position) {
				HashMap<String, String> temp = catIdList.get(position);
				groupSelection = "type";
				subCatId = temp.get("typeId");
				atozGroupCheck.setVisibility(View.INVISIBLE);
				typeGroupCheck.setVisibility(View.VISIBLE);

			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		hubCiti.setFindSingleGroupSelection(group);
		hubCiti.setFindSortSelection(sort);
		Intent launchintent = new Intent(FindSingleCategorySortActivity.this,
				FindSingleCategory.class);
		if (subCatIdRef != null) {
			launchintent.putExtra("subCatid", subCatIdRef);
		}

		launchintent.putExtra("mLinkId", mLinkId);
		launchintent.putExtra("catName", catName);
		launchintent.putExtra("catId",
				getIntent().getExtras().getString("catId"));
		launchintent.putExtra("mItemId",
				getIntent().getExtras().getString("mItemId"));
		launchintent.putExtra(
				Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
				getIntent().getExtras().getString(
						Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
		launchintent.putExtra("latitude",
				getIntent().getExtras().getString("latitude"));
		launchintent.putExtra("longitude",
				getIntent().getExtras().getString("longitude"));
		launchintent.putExtra("srchKey",
				getIntent().getExtras().getString("srchKey"));
		launchintent.putExtra("isAtoZ", isAtoZRef);

		startActivity(launchintent);
		finish();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	public class TypeAsyncTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mDialog = ProgressDialog.show(FindSingleCategorySortActivity.this,
					"", Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		protected void onPostExecute(String result) {

			if ("successfull".equals(returnValue)) {
				mTableRow2GroupType.setVisibility(View.VISIBLE);
				typeText = "Type";
				flag = false;
			}

			else {
				mTableRow2GroupType.setVisibility(View.GONE);
				flag = true;

			}

			mDialog.dismiss();
		}

		@Override
		protected String doInBackground(String... arg0) {
			try {
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();

				// Change from GET to POST
				String urlParameters = Properties.url_local_server
						+ Properties.hubciti_version + "find/getsubcategory";
				JSONObject jsonObject = null;
//				JSONObject jsonObject = mServerConnections
//						.getUrlPostResponse(
//								urlParameters,
//								mUrlRequestParams
//										.getFindSubCategoryParam(
//												catId,
//												getIntent().getExtras()
//														.getString("mItemId"),
//												getIntent()
//														.getExtras()
//														.getString(
//																Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)),
//								true);

				if (jsonObject != null) {

					if (jsonObject.has("CategoryDetails")) {

						mList = new ArrayList<>();

						JSONObject elem = jsonObject
								.getJSONObject("CategoryDetails");
						JSONObject elem2 = elem.getJSONObject("ListCatDetails");

						boolean isArray = isJSONArray(elem2, "CategoryDetails");

						mSpinnerData = new ArrayList<>();
						if (isArray) {

							JSONArray elem1 = elem2
									.getJSONArray("CategoryDetails");

							for (int index = 0; index < elem1.length(); index++) {

								mTypeDetails = new HashMap<>();
								JSONObject obj = elem1.getJSONObject(index);

								mTypeDetails.put("typeId",
										obj.getString("catId"));
								mTypeDetails.put("typeName",
										obj.getString("catName"));

								mSpinnerData.add(obj.getString("catName"));
								mList.add(mTypeDetails);
							}

						} else {

							mTypeDetails = new HashMap<>();
							JSONObject obj = elem2
									.getJSONObject("CategoryDetails");

							mTypeDetails.put("typeId", obj.getString("catId"));
							mTypeDetails.put("typeName",
									obj.getString("catName"));

							mSpinnerData.add(obj.getString("catName"));
							mList.add(mTypeDetails);

						}

						returnValue = "successfull";
					} else {

						if (jsonObject.has("response")) {

							JSONObject obj = jsonObject
									.getJSONObject("response");

							typeText = obj.getString("responseText");

							returnValue = "unSuccessFull";

						}

					}

				}

			}

			catch (Exception ex) {
				ex.printStackTrace();
				typeText = "Error in Request Response";
				returnValue = "unSuccessFull";
			}

			return returnValue;
		}
	}


	private void getGPSValues() {
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (locationManager.getAllProviders().contains("gps") && gpsEnabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Criteria criteria = new Criteria();
			String bestProvider = locationManager.getBestProvider(criteria,
					false);

			Location locNew = locationManager.getLastKnownLocation(bestProvider);
			if (locNew != null) {

				CommonConstants.LATITUDE = String.valueOf(locNew.getLatitude());
				CommonConstants.LONGITUDE = String.valueOf(locNew
						.getLongitude());

			} else {
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					CommonConstants.LATITUDE = String.valueOf(locNew
							.getLatitude());
					CommonConstants.LONGITUDE = String.valueOf(locNew
							.getLongitude());
				}
			}

		}
	}

	public boolean isJSONArray(JSONObject jsonObject, String value) {
		boolean isArray = false;

		JSONObject isJSONObject = jsonObject.optJSONObject(value);
		if (isJSONObject == null) {
			JSONArray isJSONArray = jsonObject.optJSONArray(value);
			if (isJSONArray != null) {
				isArray = true;
			}
		}
		return isArray;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.find_single_category_tableRow3:
			groupByAtoz();
			break;
		case R.id.find_single_category_tableRow7:
			sortByName();
			break;
		case R.id.find_single_category_tableRow8:
			sortByDistance();
			break;
		case R.id.find_single_category_tableRow4:
			isAtoZ = false;
			createAlert(flag);
			break;
		case R.id.event_textView_group_atoz:
			groupByAtoz();
			break;
		case R.id.event_textView_group_type:

			isAtoZ = false;
			createAlert(flag);
			break;
		case R.id.event_textView_sort_name:
			sortByName();
			break;
		case R.id.event_textView_sort_distance:
			sortByDistance();
			break;
		case R.id.sortby_city_tableRow:
			sortByCity();
			break;
		case R.id.textView_sort_city:
			sortByCity();
			break;
		default:
			break;

		}

	}

	private void sortByCity() {
		citySortCheck.setVisibility(View.VISIBLE);
		nameSortCheck.setVisibility(View.INVISIBLE);
		distanceSortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);
		citySorting.sortAction();

		sortSelection = "city";

	}

	private void sortByDistance() {
		nameSortCheck.setVisibility(View.INVISIBLE);
		distanceSortCheck.setVisibility(View.VISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);
		sortSelection = "distance";

	}

	private void sortByName() {
		nameSortCheck.setVisibility(View.VISIBLE);
		distanceSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);
		sortSelection = "name";

	}

	private void groupByAtoz() {
		subCatId = null;
		subCatId = null;

		isAtoZ = !isAtoZ;

		if (isAtoZ) {
			atozGroupCheck.setVisibility(View.VISIBLE);
			groupSelection = "atoz";
		} else {
			atozGroupCheck.setVisibility(View.INVISIBLE);
			groupSelection = "";
		}

		typeGroupCheck.setVisibility(View.INVISIBLE);

	}

}
