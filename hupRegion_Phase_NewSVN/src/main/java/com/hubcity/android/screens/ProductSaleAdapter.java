package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.ProductBO;

public class ProductSaleAdapter extends BaseAdapter {
	private ArrayList<ProductBO> hotDealsDetailsList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;
	Activity activity;

	public ProductSaleAdapter(Activity activity,
			ArrayList<ProductBO> hotDealsDetailsList) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.hotDealsDetailsList = hotDealsDetailsList;
		this.activity = activity;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return hotDealsDetailsList.size();

	}

	@Override
	public Object getItem(int id) {
		return hotDealsDetailsList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (convertView == null){
			view = inflater.inflate(R.layout.listitem_currentsales, parent,false);
			holder = new ViewHolder();
			holder.currentsalesPname = (TextView) view
					.findViewById(R.id.retailer_sales_name);
			holder.currentsalesRegularprice = (TextView) view
					.findViewById(R.id.retailer_sales_price_1);
			holder.currentsalesSaleprice = (TextView) view
					.findViewById(R.id.retailer_sales_price_2);
			holder.currentsalesImage = (ImageView) view
					.findViewById(R.id.retailer_sales_image);
			view.setTag(holder);
		}else{
			holder = (ViewHolder)view.getTag();
		}
		holder.currentsalesPname.setText(hotDealsDetailsList.get(position)
				.getProductName());

		holder.currentsalesRegularprice.setText(hotDealsDetailsList.get(
				position).getRegularPrice());
		holder.currentsalesSaleprice.setText(hotDealsDetailsList.get(
				position).getSalePrice());

		holder.currentsalesImage.setTag(hotDealsDetailsList.get(position)
				.getImagePath());
		customImageLoader.displayImage(hotDealsDetailsList.get(position)
				.getImagePath(), activity, holder.currentsalesImage);

		return view;
	}

	public static class ViewHolder {
		protected TextView couponName;
		protected ImageView couponImage;
		protected TextView couponLongdesc;
		protected ImageView currentsalesImage;
		protected TextView currentsalesSaleprice;
		protected TextView currentsalesRegularprice;
		protected TextView currentsalesPname;
		protected TextView specialofferDummydata;
		protected TextView specialofferstext;
		protected TextView googleDummydata;
	}
}
