package com.hubcity.android.screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.SaveUserSettingsAsync;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

import org.json.JSONObject;

/**
 * This class will fetch the user settings and will save the radius to preference
 * Created by supriya.m on 3/10/2016.
 */
public class GetUserSettingsAsync extends AsyncTask<Void, Void, Void> {
    Context mContext;
    boolean isShowDialog = false, isFromSettings = false, enableNotification;
    SharedPreferences preferences;
    ProgressDialog pgDialog;
    String responseText;
    String radius;
    boolean bLocService = false, isFromLoc = false;

    public GetUserSettingsAsync(Context context, boolean isShowDialog, boolean isFromSettings) {
        this.mContext = context;
        this.isShowDialog = isShowDialog;
        this.isFromSettings = isFromSettings;
        preferences = context.getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);
    }

    public GetUserSettingsAsync(Context context, boolean enableNotification) {
        this.mContext = context;
        this.enableNotification = enableNotification;
        preferences = context.getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);
    }

    public GetUserSettingsAsync(boolean isFromLoc, Context context, boolean enableNotification) {
        this.mContext = context;
        this.isFromLoc = isFromLoc;
        this.enableNotification = enableNotification;
        preferences = context.getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isShowDialog) {
            pgDialog = new ProgressDialog(mContext);
            pgDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pgDialog.setMessage(Constants.DIALOG_MESSAGE);
            pgDialog.setCancelable(false);
            pgDialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        UrlRequestParams urlRequestParams = new UrlRequestParams();
        ServerConnections serverConnections = new ServerConnections();
        try {

            String url_get_usersettings = Properties.url_local_server
                    + Properties.hubciti_version
                    + "firstuse/getusersettings";

            String urlParameters = urlRequestParams
                    .getUserSettingsParameters();
            JSONObject jsonRespone = serverConnections.getUrlPostResponse(
                    url_get_usersettings, urlParameters, true);
            Log.d("jsonRespone :", jsonRespone.toString());

            if (jsonRespone != null) {

                if (jsonRespone.has("UserSettings")) {

                    JSONObject elem1 = jsonRespone
                            .getJSONObject("UserSettings");

                    if (elem1 != null) {

                        radius = elem1.getString("localeRadius");
                        if (radius != null && !radius.equals("")) {
                            preferences.edit().putString("radious", radius).apply();
                        }
                        if (elem1.has("bLocService")) {
                            bLocService = elem1.getBoolean("bLocService");

                        }

                        responseText = elem1.getString("responseText");

                    }

                } else if (jsonRespone.has("response")) {

                    JSONObject elem1 = jsonRespone
                            .getJSONObject("response");

                    responseText = elem1.getString("responseText");

                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            responseText = "Exception in Parsing";

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try {
            if (pgDialog != null) {
                pgDialog.dismiss();
            }
            if ("Success".equalsIgnoreCase(responseText)) {
                if (isFromSettings) {
                    ((SettingsLocationServices) mContext).setUserRadius(radius);
                } else if (isFromLoc) {
                    new SaveUserSettingsAsync(mContext, String
                            .valueOf
                                    (enableNotification), false, false).execute(radius);
                } else {
                    new SaveUserSettingsAsync(mContext, String
                            .valueOf
                                    (enableNotification), bLocService, false).execute(radius);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        radius = null;
        responseText = null;
    }

}
