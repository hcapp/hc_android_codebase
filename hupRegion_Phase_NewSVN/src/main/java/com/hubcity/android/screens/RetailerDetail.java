package com.hubcity.android.screens;

/**
 * Created by subramanya.v on 9/20/2016.
 */
public class RetailerDetail
{
	private String bandName;
	private String bandImgPath;
	private String bandID;
	private String sDate;
	private String eDate;
	private String sTime;
	private String eTime;
	private String bEvtName;
	private String bEvtId;
	private String rowNum;
	private String catName;

	public String getBandID()
	{
		return bandID;
	}

	public void setBandID(String bandID)
	{
		this.bandID = bandID;
	}

	public String getBandImgPath()
	{
		return bandImgPath;
	}

	public void setBandImgPath(String bandImgPath)
	{
		this.bandImgPath = bandImgPath;
	}

	public String getBandName()
	{
		return bandName;
	}

	public void setBandName(String bandName)
	{
		this.bandName = bandName;
	}

	public String getbEvtId()
	{
		return bEvtId;
	}

	public void setbEvtId(String bEvtId)
	{
		this.bEvtId = bEvtId;
	}

	public String getbEvtName()
	{
		return bEvtName;
	}

	public void setbEvtName(String bEvtName)
	{
		this.bEvtName = bEvtName;
	}

	public String getCatName()
	{
		return catName;
	}

	public void setCatName(String catName)
	{
		this.catName = catName;
	}

	public String geteDate()
	{
		return eDate;
	}

	public void seteDate(String eDate)
	{
		this.eDate = eDate;
	}

	public String geteTime()
	{
		return eTime;
	}

	public void seteTime(String eTime)
	{
		this.eTime = eTime;
	}

	public String getRowNum()
	{
		return rowNum;
	}

	public void setRowNum(String rowNum)
	{
		this.rowNum = rowNum;
	}

	public String getsDate()
	{
		return sDate;
	}

	public void setsDate(String sDate)
	{
		this.sDate = sDate;
	}

	public String getsTime()
	{
		return sTime;
	}

	public void setsTime(String sTime)
	{
		this.sTime = sTime;
	}


}
