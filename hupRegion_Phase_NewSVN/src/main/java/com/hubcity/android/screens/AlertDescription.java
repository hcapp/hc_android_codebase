package com.hubcity.android.screens;

import java.io.InputStream;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;

public class AlertDescription extends CustomTitleBar {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TextView txtDescription = (TextView) findViewById(R.id.txtDescription);
		TextView txtStartDate = (TextView) findViewById(R.id.txtStartDate);
		TextView txtStartTime = (TextView) findViewById(R.id.txtStatrTime);
		TextView txtEndDate = (TextView) findViewById(R.id.txtEndDate);
		TextView txtEndTime = (TextView) findViewById(R.id.txtEndTime);
		ImageView bmImage = (ImageView) findViewById(R.id.list_item_image);

		String strHeader = "Alert";
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String strDescription = extras.getString("Description");
			String strImageURL = extras.getString("ImageURL");
			String strStartdate = extras.getString("Startdate");
			String strStartTime = extras.getString("StartTime");
			String strEndDate = extras.getString("EndDate");
			String strEndTime = extras.getString("EndTime");
			strHeader = extras.getString("header");

			txtDescription.setText(strDescription);
			txtStartDate.setText(strStartdate);
			txtStartTime.setText(strStartTime);
			txtEndDate.setText(strEndDate);
			txtEndTime.setText(strEndTime);
			new DownloadImageTask(bmImage).execute(strImageURL);
		} else {
			txtDescription.setText("Description not available");
		}

		title.setText(strHeader);
		leftTitleImage.setVisibility(View.INVISIBLE);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}

	}

	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		private RotateAnimation anim;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
			anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF,
					0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setRepeatCount(Animation.INFINITE);
			anim.setDuration(700);
			this.bmImage.setAnimation(null);
			this.bmImage.setImageResource(R.drawable.loading_button);
			this.bmImage.startAnimation(anim);
			this.bmImage.setAnimation(anim);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0].replaceAll(" ", "%20");
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			bmImage.setAnimation(null);
		}
	}
}