package com.hubcity.android.screens;

public class SubMenuDetails {

	String dept = "";
	String type = "";
	String selectedItem = "";
	String selectedCities = "";
	String mLinkId = "";
	String mItemId = "";
	int level;

	public SubMenuDetails(String dept, String type, String selectedItem,
			String selectedCities, String mLinkId, String mItemId, int level) {
		this.dept = dept;
		this.type = type;
		this.selectedItem = selectedItem;
		this.selectedCities = selectedCities;
		this.mLinkId = mLinkId;
		this.mItemId = mItemId;
		this.level = level;
	}

	public String getmItemId() {
		return mItemId;
	}

	public int getLevel() {
		return level;
	}

	public String getmLinkId() {
		return mLinkId;
	}

	public String getDept() {
		return dept;
	}

	public String getType() {
		return type;
	}

	public String getSelectedItem() {
		return selectedItem;
	}

	public String getSelectedCities() {
		return selectedCities;
	}

}
