package com.hubcity.android.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


/**
 * Created by subramanya.v on 4/20/2016.
 */
public class BandEventDetail extends CustomTitleBar {
    private LinearLayout screenParent;
    private int screenWidth;
    private String mBannerImg;
    private ImageView bannerImage;
    private ProgressBar progressBar;
    private RelativeLayout bannerHolder;
    private TextView moreInfo;
    private Button venue;
    private Button eventBandName;
    private EventDetailObj eventSummary;
    private TextView shortDesc;
    private TextView longDesc;
    private TextView startDateView;
    private TextView startTime;
    private TextView address;
    private TextView recurringText;
    private String startDate = null;
    private ImageView calImage;
    private ImageView timeImage;
    private ShareInformation shareInfo;
    private Activity activity;
    private boolean isShareTaskCalled = false;
    private TextView buyTicket;
    private int multipleBands;
    private String ticketUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.band_event_details_layout);
        activity = BandEventDetail.this;
        bindView();
        bindValue();
        setValue();
        setListener();
        addShareIcon();

        ViewTreeObserver observer = screenParent.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                screenWidth = screenParent.getMeasuredWidth();
                setBannerImage();

                ViewTreeObserver obs = screenParent.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    obs.removeGlobalOnLayoutListener(this);
                }
            }

        });
    }

    private void setBannerImage() {
        if (null != mBannerImg && !mBannerImg.equals("") && !(mBannerImg.equalsIgnoreCase
                ("N/A"))) {
            float bannerImageHeight = 422;
            float bannerWidth = 750;
            float bannerHeight = (bannerImageHeight / bannerWidth) * screenWidth;
            ViewGroup.LayoutParams bannerParams = bannerHolder.getLayoutParams();
            bannerParams.height = (int) bannerHeight;

            bannerImage.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            mBannerImg = mBannerImg.replaceAll(" ", "%20");
            Picasso.with(BandEventDetail.this).load(mBannerImg).resize((int) screenWidth,
                    (int)
                            bannerHeight).into(bannerImage, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            bannerHolder.setVisibility(View.GONE);
        }


    }

    private void addShareIcon() {
        // Call for Share Information
        shareInfo = new ShareInformation(activity, eventSummary.getEventId(), "BandEventSummery");

        leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
        leftTitleImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!Constants.GuestLoginId.equals(UrlRequestParams
                        .getUid().trim())) {
                    shareInfo.shareTask(isShareTaskCalled);
                    isShareTaskCalled = true;
                } else {
                    Constants.SignUpAlert(activity);
                    isShareTaskCalled = false;
                }
            }
        });
    }

    private void setListener() {
        buyTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webView = new Intent(BandEventDetail.this, ScanseeBrowserActivity.class);
                webView.putExtra(CommonConstants.URL, ticketUrl);
                startActivity(webView);
            }
        });
        eventBandName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bandScreen;
                if (multipleBands == 0) {
                  CommonConstants.displayToast(activity,eventSummary.getPopUpMsg());
                } else if (multipleBands == 1) {
                    bandScreen = new Intent(BandEventDetail.this, BandDetailScreen
                            .class);
                    bandScreen.putExtra("bandID", eventSummary.getBandIds());
                    startActivity(bandScreen);
                }
                else {
                    bandScreen = new Intent(BandEventDetail.this, MultipleBand.class);
                    bandScreen.putExtra("eventId", eventSummary.getEventId());
                    bandScreen.putExtra("eventName", eventSummary.getEventName());
                    startActivity(bandScreen);
                }

            }
        });

        venue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eventSummary.getIsAppSiteFlag() == 1) {
                    Intent findlocationInfo = new Intent(
                            BandEventDetail.this,
                            RetailerActivity.class);
                    findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
                            eventSummary.getRetailId());
                    findlocationInfo.putExtra(
                            CommonConstants.TAG_RETAILE_LOCATIONID,
                            eventSummary.getRetailLocationId());

                    findlocationInfo.putExtra(
                            CommonConstants.TAG_RETAILER_NAME, eventSummary
                                    .getRetailName());
                    findlocationInfo.putExtra(CommonConstants.TAG_DISTANCE,
                            eventSummary.getDistance());
                    startActivityForResult(findlocationInfo,
                            Constants.STARTVALUE);

                } else {
                    Intent bandScreen = new Intent(BandEventDetail.this, ShowMapMultipleLocations
                            .class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("EventDetails", eventSummary);
                    bandScreen.putExtras(bundle);
                    startActivity(bandScreen);
                }
            }
        });

    }

    private void setValue() {
        title.setText(eventSummary.getEventName());
        if (eventSummary.getRecurringDays() != null && !eventSummary.getRecurringDays().equals
                ("")) {
            recurringText.setVisibility(View.VISIBLE);
            recurringText.setText(eventSummary.getRecurringDays());
        } else {
            recurringText.setVisibility(View.GONE);
        }
        shortDesc.setText(Html.fromHtml(eventSummary.getShortDes()));
        shortDesc.setMovementMethod(LinkMovementMethod.getInstance());
        longDesc.setText(Html.fromHtml(eventSummary.getLongDes()));
        longDesc.setMovementMethod(LinkMovementMethod.getInstance());
        if(multipleBands == 0 || multipleBands == 2 )
        {
            eventBandName.setText(R.string.bands);
        }
        else
        {
            eventBandName.setText(eventSummary.getBandName());
        }


        if (!eventSummary.getRetailName().equalsIgnoreCase("N/A") && !eventSummary.getRetailName()
                .equalsIgnoreCase("")) {
            address.setText(eventSummary.getRetailName());
        } else if (!eventSummary.getEvtLocTitle().equalsIgnoreCase("N/A") && !eventSummary
                .getEvtLocTitle()
                .equalsIgnoreCase("")) {
            address.setText(eventSummary.getEvtLocTitle());
        } else {
            address.setText(eventSummary.getBandName());
        }

        String time = eventSummary.getStartTime();
        if (!(time.equalsIgnoreCase("")) && !(time.equalsIgnoreCase("N/A")) && !(time == null)) {
            startTime.setText(eventSummary.getStartTime());
        } else {
            startTime.setVisibility(View.GONE);
            timeImage.setVisibility(View.GONE);
        }
        String date = eventSummary.getStartDate();
        if ((!date.equalsIgnoreCase("")) && !(date.equalsIgnoreCase("N/A")) && !(time == null)) {
            if ((startDate != null) && !startDate.equalsIgnoreCase("")) {
                if (startDate.contains("-")) {
                    String[] dateFormat = startDate.split("-", 3);
                    String month = getMonthString(dateFormat[0]);
                    startDate = dateFormat[1] + " " + month;
                } else {
                    String[] dateArray = startDate.split(" ", 0);
                    startDate = dateArray[1] + " " + dateArray[0];

                }
                startDateView.setText(startDate);
            }

        } else {
            startDateView.setVisibility(View.GONE);
            calImage.setVisibility(View.GONE);
        }
        String url = eventSummary.getMoreInfoURL();
        if ((!url.equalsIgnoreCase("")) && !(url.equalsIgnoreCase("N/A")) && !(url == null)) {
            moreInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent bandScreen = new Intent(BandEventDetail.this, ScanseeBrowserActivity
                            .class);
                    bandScreen.putExtra(CommonConstants.URL, eventSummary.getMoreInfoURL());
                    startActivity(bandScreen);
                }
            });
        } else {
            moreInfo.setVisibility(View.GONE);
        }
        if(!ticketUrl.isEmpty() && ticketUrl != null && !ticketUrl.equalsIgnoreCase("N/A"))
        {
            buyTicket.setVisibility(View.VISIBLE);
        }

    }

    private void bindValue() {
        try {
            eventSummary = (EventDetailObj) getIntent().getExtras().getSerializable("EventDetails");
        } catch (Exception e) {
            e.printStackTrace();
        }
        mBannerImg = eventSummary.getImagePath();
        startDate = eventSummary.getStartDate();
        ticketUrl = eventSummary.getTicketUrl();
        multipleBands = eventSummary.getBandCntFlag();
    }

    private void bindView() {
        buyTicket = (TextView) findViewById(R.id.buy_ticket);
        shortDesc = (TextView) findViewById(R.id.short_description);
        address = (TextView) findViewById(R.id.address);
        longDesc = (TextView) findViewById(R.id.long_description);
        startDateView = (TextView) findViewById(R.id.start_date);
        startTime = (TextView) findViewById(R.id.start_time);
        recurringText = (TextView) findViewById(R.id.recurring_text);
        screenParent = (LinearLayout) findViewById(R.id.screenParent);
        bannerImage = (ImageView) findViewById(R.id.event_banner_image);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        bannerHolder = (RelativeLayout) findViewById(R.id.banner_holder);
        moreInfo = (TextView) findViewById(R.id.event_more_info);
        venue = (Button) findViewById(R.id.venue);
        eventBandName = (Button) findViewById(R.id.event_band_name);
        calImage = (ImageView) findViewById(R.id.calendar_image);
        timeImage = (ImageView) findViewById(R.id.time_image);


    }

    private String getMonthString(String month) {
        switch (month) {
            case "01":
                return "Jan";
            case "02":
                return "Feb";
            case "03":
                return "Mar";
            case "04":
                return "Apr";
            case "05":
                return "May";
            case "06":
                return "Jun";
            case "07":
                return "Jul";
            case "08":
                return "Aug";
            case "09":
                return "Sep";
            case "10":
                return "Oct";
            case "11":
                return "Nov";
            case "12":
                return "Dec";

            default:
                return "1";
        }

    }
}
