package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.HubCityContext;
import com.scansee.hubregion.R;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * This Class creates a Combo Template Main Screen
 *
 * @author rekha_p
 */
public class ComboTemplateScreen extends MenuPropertiesActivity {
    protected static final String SQUARE = "Square";
    protected static final String RECTANGLE = "Rectangle";
    protected static final String CIRCLE = "Circle";
    protected static final String TEXT = "Text";
    protected static final String LABEL = "Label";
    protected static final int BUTTONS_ROW_COUNT = 4;

    RelativeLayout parent;
    ScrollView scrollView;
    LinearLayout childLinearLayout, mainLinearLayout, buttonLinearLayout;
    int buttonWidth = 0;

    private static boolean isOnNewIntent;
    private LinearLayout bannerParent;
    private Activity mActivity;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        CommonConstants.hamburgerIsFirst = true;
        mActivity = ComboTemplateScreen.this;
        // Gets the previous menu level
        Intent intent = getIntent();
        previousMenuLevel = intent.getExtras().getString(
                Constants.MENU_LEVEL_INTENT_EXTRA);


        // Parent Layout of the screen
        parent = new RelativeLayout(this);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        parent.setLayoutParams(param);

        // Inflating the main layout to the main layout
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        mainLinearLayout = (LinearLayout) layoutInflater.inflate(
                R.layout.twocolumn_menu_template, null, false);
        bannerParent = (LinearLayout) mainLinearLayout.findViewById(R.id.bannerParent);
        bannerParent.setVisibility(View.GONE);

        drawer = (DrawerLayout) mainLinearLayout.findViewById(R.id.drawer_layout);
        // Layout params for main layout and scrollview
        LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        mainLinearLayout.setLayoutParams(parentParam);

        // Scrollview for the buttons
        scrollView = new ScrollView(this);
        scrollView.setLayoutParams(parentParam);

        // Buttons layout for each shape
        buttonLinearLayout = new LinearLayout(this);
        buttonLinearLayout.setOrientation(LinearLayout.VERTICAL);

        // Layout params for buttons layout
        LinearLayout.LayoutParams buttonLinearLayoutParentParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        // Added by sharanamma
        LinearLayout ParentIntermideate = new LinearLayout(this);
        ParentIntermideate.setOrientation(LinearLayout.VERTICAL);
        ParentIntermideate.setLayoutParams(parentParam);

        LinearLayout mainLayout = (LinearLayout) mainLinearLayout.findViewById(R.id.menu_list_parent);
        LinearLayout mainParentLayout = (LinearLayout) mainLinearLayout.findViewById(R.id.mainParentLayout);
        //End

        // Getting the list of Bottombuttons and set the area for bottombuttons
        // bottombuttons
        BottomButtonListSingleton mBottomButtonListSingleton;
        mBottomButtonListSingleton = BottomButtonListSingleton
                .getListBottomButton();

        if (mBottomButtonListSingleton != null) {

            ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton.listBottomButton;

            if (listBottomButton != null && listBottomButton.size() > 1) {
                int px = (int) (50 * this.getResources().getDisplayMetrics().density + 0.5f);
                buttonLinearLayoutParentParam.setMargins(0, 0, 0, px);
            }
        }

//        buttonLinearLayout.setLayoutParams(buttonLinearLayoutParentParam);
        mainParentLayout.setLayoutParams(buttonLinearLayoutParentParam);

        // Layout for the buttons of a group
        childLinearLayout = new LinearLayout(this);
        childLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        childLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        // Adds title bar to main layout
        //addTitleBar(mainLinearLayout, parent);
        // Adds main layout to the parent layout
        //parent.addView(mainLinearLayout);


        //scrollView.addView(buttonLinearLayout);

        addTitleBar(ParentIntermideate, parent);
        //parent.addView(mainParentLayout);
        ParentIntermideate.addView(mainParentLayout);
        parent.addView(ParentIntermideate);
        mainLayout.addView(scrollView);

        // Gets the intent values
        handleIntent(intent);

        // Creates the grouped buttons and adds to scrollview
        createButtonsScreens(mainMenuUnsortedList);

        // Creates bottom buttons
        createbottomButtontTab(parent);

        // Adds scrollview to the main layout
        // mainLinearLayout.addView(scrollView);

        // Sets resources to button layout, main layout and scrollview
        initialBgSettings();

        setContentView(parent);
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            HubCityContext.level = 0;
        } else {
            HubCityContext.level = HubCityContext.level + 1;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Sets resources to button layout, main layout and scrollview
     */
    private void initialBgSettings() {
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {

                new ImageLoaderAsync1(parent, "", false).execute(mBkgrdImage);

                new ImageLoaderAsync1(mainLinearLayout, "", false)
                        .execute(mBkgrdImage);
                new ImageLoaderAsync1(scrollView, "", false)
                        .execute(mBkgrdImage);

            } else if (mBkgrdColor != null && !"N/A".equals(mBkgrdColor)) {

                buttonLinearLayout.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(mBkgrdColor));
                mainLinearLayout.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                scrollView.setBackgroundColor(Color.parseColor(mBkgrdColor));

            }

        } else {
            if (smBkgrdColor != null && !"N/A".equals(smBkgrdColor)) {

                buttonLinearLayout.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(smBkgrdColor));
                mainLinearLayout.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                scrollView.setBackgroundColor(Color.parseColor(smBkgrdColor));

            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {

                new ImageLoaderAsync1(parent, "", false).execute(smBkgrdImage);
                new ImageLoaderAsync1(mainLinearLayout, "", false)
                        .execute(smBkgrdImage);
                new ImageLoaderAsync1(scrollView, "", false)
                        .execute(smBkgrdImage);

            }

        }

    }

    @SuppressWarnings("deprecation")
    /**
     * Creates combo buttons and adds to the main screen
     * @param mainMenuUnsortedList
     */
    private void createButtonsScreens(ArrayList<MainMenuBO> mainMenuUnsortedList) {

        int size = mainMenuUnsortedList.size();
        int count = -1;
        String nextLinkName = "";

        for (int i = 0; i < size; i++) {

            // Related data items
            String linkName = mainMenuUnsortedList.get(i).getLinkTypeName();
            String mItemName = mainMenuUnsortedList.get(i).getmItemName();
            String mShapeName = mainMenuUnsortedList.get(i).getmShapeName();

            String textBgColor;
            String textFntColor;

            if (mainMenuUnsortedList.get(i).getmGrpBkgrdColor() != null) {
                textBgColor = mainMenuUnsortedList.get(i).getmGrpBkgrdColor();
            } else {
                textBgColor = mainMenuUnsortedList.get(i).getsGrpBkgrdColor();
            }

            if (mainMenuUnsortedList.get(i).getmGrpFntColor() != null) {
                textFntColor = mainMenuUnsortedList.get(i).getmGrpFntColor();
            } else {
                textFntColor = mainMenuUnsortedList.get(i).getsGrpFntColor();
            }

            // @Beena : to check the next node in list
            if (i + 1 < size) {
                // Related data items
                nextLinkName = mainMenuUnsortedList.get(i + 1)
                        .getLinkTypeName();
            }
            //

            // If the item is a text or label
            if (linkName.equals(TEXT) || linkName.equals(LABEL)) {
                // @Beena : Check is last element is only Group header
                if (i != (size - 1)) {
                    if (!nextLinkName.equals(TEXT) || !nextLinkName.equals(LABEL)) {
                        count = -1;

                        // After completion of a group adds child to button
                        // layout and
                        // creates a new child layout
                        if (i > 0) {
                            buttonLinearLayout.addView(childLinearLayout);
                            childLinearLayout = new LinearLayout(this);
                            childLinearLayout
                                    .setLayoutParams(new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.FILL_PARENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT));
                            childLinearLayout
                                    .setOrientation(LinearLayout.HORIZONTAL);
                        }

                        // Layout that holds the Label
                        LinearLayout labelLayout = new LinearLayout(this);
                        labelLayout
                                .setLayoutParams(new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.FILL_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT));

                        TextView tv = new TextView(this);
                        TableRow.LayoutParams params = new TableRow.LayoutParams(
                                LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT, 1.0f);
                        params.setMargins(10, 10, 10, 10);
                        tv.setLayoutParams(params);
                        tv.setText(mItemName);

                        if (textFntColor != null) {
                            tv.setTextColor(Color.parseColor(textFntColor));
                        } else {
                            tv.setTextColor(Color.BLACK);
                        }

                        tv.setTypeface(null, Typeface.BOLD);

                        if (linkName.equals(TEXT)) {
                            if (textBgColor != null) {
                                labelLayout.setBackgroundColor(Color
                                        .parseColor(textBgColor));
                            } else {
                                labelLayout.setBackgroundColor(Color.WHITE);
                            }
                        }

                        labelLayout.addView(tv);

                        buttonLinearLayout.addView(labelLayout);
                    }
                }
            } else {

                count++;

                View view = null;
                ImageButton imageButton = null;
                TextView textView = null;

                // If the shape is a circle
                switch (mShapeName) {
                    case CIRCLE: {
                        if (count > 0 && count % BUTTONS_ROW_COUNT == 0) { // If the
                            // row
                            // count
                            // is 4
                            // , the buttons are
                            // created in the next
                            // row
                            buttonLinearLayout.addView(childLinearLayout);
                            childLinearLayout = null;
                            childLinearLayout = new LinearLayout(this);
                            childLinearLayout
                                    .setLayoutParams(new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.FILL_PARENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT));
                            childLinearLayout
                                    .setOrientation(LinearLayout.HORIZONTAL);
                        }

                        childLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                        view = LayoutInflater.from(getBaseContext()).inflate(
                                R.layout.combo_circle, buttonLinearLayout, false); // Inflates

                        // the
                        // combo_circle
                        // xml
                        // to
                        // the
                        // button
                        // layout

                        LinearLayout lLayout = (LinearLayout) view
                                .findViewById(R.id.circleLayout);
                        LayoutParams lParams = lLayout
                                .getLayoutParams();
                        int margin = ((LinearLayout.LayoutParams) lParams).rightMargin
                                + ((LinearLayout.LayoutParams) lParams).leftMargin;

                        imageButton = (ImageButton) view
                                .findViewById(R.id.circleButton);

                        LayoutParams params = imageButton
                                .getLayoutParams();
                        buttonWidth = params.width = params.height = screenWidth
                                / BUTTONS_ROW_COUNT - margin;
                        imageButton.setLayoutParams(params);

                        textView = (TextView) view.findViewById(R.id.circleText);
                        LayoutParams tvParams = textView
                                .getLayoutParams();
                        tvParams.width = buttonWidth;
                        textView.setMaxLines(2);
                        textView.setLayoutParams(tvParams);

                        createButtonProperties(mainMenuUnsortedList.get(i), // call
                                // for
                                // settin
                                // font
                                // and
                                // button
                                // color
                                imageButton, textView);

                        break;
                    }
                    case RECTANGLE:  // If the shape is a
                        // Rectangle
                        childLinearLayout.setOrientation(LinearLayout.VERTICAL);
                        view = LayoutInflater.from(getBaseContext())
                                .inflate(R.layout.combo_rectangle,
                                        buttonLinearLayout, false); // Inflates the

                        // combo_rectangle
                        // xml to the
                        // button layout

                        imageButton = (ImageButton) view
                                .findViewById(R.id.rectButton);
                        imageButton
                                .setBackgroundResource(R.drawable.twocolumnbtnborder);
                        textView = (TextView) view.findViewById(R.id.rectText);
                        textView.setMaxLines(2);

                        createButtonProperties(mainMenuUnsortedList.get(i),// call
                                // for
                                // settin
                                // font
                                // and
                                // button
                                // color
                                imageButton, textView);

                        break;
                    case SQUARE: {

                        if (count > 0 && count % BUTTONS_ROW_COUNT == 0) {// If the
                            // row
                            // count
                            // is 4
                            // ,
                            // the buttons are
                            // created in the next
                            // row
                            buttonLinearLayout.addView(childLinearLayout);
                            childLinearLayout = null;
                            childLinearLayout = new LinearLayout(this);
                            childLinearLayout
                                    .setLayoutParams(new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.FILL_PARENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT));
                            childLinearLayout
                                    .setOrientation(LinearLayout.HORIZONTAL);

                        }

                        childLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                        view = LayoutInflater.from(getBaseContext()).inflate(
                                R.layout.combo_square, buttonLinearLayout, false); // Inflates

                        // the
                        // combo_square
                        // xml
                        // to
                        // the
                        // button
                        // layout

                        // LinearLayout
                        LinearLayout lLayout = (LinearLayout) view
                                .findViewById(R.id.squareLayout);
                        LayoutParams lParams = lLayout
                                .getLayoutParams();
                        int margin = ((LinearLayout.LayoutParams) lParams).rightMargin
                                + ((LinearLayout.LayoutParams) lParams).leftMargin;

                        // Image button
                        imageButton = (ImageButton) view
                                .findViewById(R.id.squareButton);
                        LayoutParams params = imageButton
                                .getLayoutParams();
                        buttonWidth = params.width = params.height = screenWidth
                                / BUTTONS_ROW_COUNT - margin;
                        imageButton.setLayoutParams(params);

                        textView = (TextView) view.findViewById(R.id.squareText);
                        LayoutParams txtViewParams = textView
                                .getLayoutParams();
                        txtViewParams.width = buttonWidth;
                        textView.setMaxLines(2);
                        textView.setLayoutParams(txtViewParams);

                        createButtonProperties(mainMenuUnsortedList.get(i), // call
                                // for
                                // settin
                                // font
                                // and
                                // button
                                // color
                                imageButton, textView);
                        break;
                    }
                }

                imageButton.setTag(mItemName);
                textView.setText(mItemName);

                imageButton.setOnClickListener(this);

                if (null != view) {
                    childLinearLayout.addView(view);
                }

            }
            if (i == size - 1) {
                buttonLinearLayout.addView(childLinearLayout);
            }
        }

        buttonLinearLayout.setPadding(0, 0, 0, screenHeight / 11);
        scrollView.addView(buttonLinearLayout);
    }

    private void createButtonProperties(MainMenuBO item, ImageButton button,
                                        TextView textView) {

        String imgBtnColor = item.getmBtnColor();
        String imgFntColor = item.getmBtnFontColor();
        String btnImageUrl = item.getmItemImgUrl();

        String smImgBgColor = item.getSmBtnColor();
        String smImgFntColor = item.getSmBtnFontColor();

        if (btnImageUrl != null && !"N/A".equals(btnImageUrl)) {

            switch (item.mShapeName) {
                case SQUARE:
                    CustomImageLoader squareImageLoader = new CustomImageLoader(mActivity, false);
                    squareImageLoader.displayImage(btnImageUrl, mActivity, button);

                    break;
                case CIRCLE:
                    CustomImageLoader circleImageLoader = new CustomImageLoader(mActivity, true);
                    circleImageLoader.displayImage(btnImageUrl, mActivity, button);

                    break;
                case RECTANGLE:
                    new ImageLoaderAsync1(button, RECTANGLE, true)
                            .execute(btnImageUrl);
                    break;
            }

        }

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {

            if (item.mShapeName.equals(CIRCLE)
                    || item.mShapeName.equals(SQUARE)) {
                if (mFontColor != null && !"N/A".equals(mFontColor)) {
                    textView.setTextColor(Color.parseColor(mFontColor));
                }

            } else {
                if (imgFntColor != null && !"N/A".equals(imgFntColor)) {
                    textView.setTextColor(Color.parseColor(imgFntColor));
                }
            }

            if (imgBtnColor != null && !"N/A".equals(imgBtnColor)) {
                if (item.mShapeName.equals(RECTANGLE)) {
                    button.setBackgroundColor(Color.parseColor(imgBtnColor));
                }
            }

        } else {

            if (item.mShapeName.equals(CIRCLE)
                    || item.mShapeName.equals(SQUARE)) {
                if (smFontColor != null && !"N/A".equals(smFontColor)) {
                    textView.setTextColor(Color.parseColor(smFontColor));
                }
            } else {
                if (smImgFntColor != null && !"N/A".equals(smImgFntColor)) {
                    textView.setTextColor(Color.parseColor(smImgFntColor));
                }
            }

            if (smImgBgColor != null && !"N/A".equals(smImgBgColor)) {
                if (item.mShapeName.equals(RECTANGLE)) {
                    button.setBackgroundColor(Color.parseColor(smImgBgColor));
                }
            }

        }

    }

    /**
     * Gets bitmap image from the specified url
     *
     * @author rekha_p
     */
    public class ImageLoaderAsync1 extends AsyncTask<String, Void, Bitmap> {

        private ProgressDialog mDialog;

        View view;
        ImageButton imgButton;
        String shape = "";
        boolean isButton;

        public ImageLoaderAsync1(View bmImage, String imgShape,
                                 boolean isImgButton) {

            shape = imgShape;
            isButton = isImgButton;

            if (isButton) {
                imgButton = (ImageButton) bmImage;
            } else {
                view = bmImage;
            }
        }

        @Override
        protected void onPreExecute() {

            mDialog = ProgressDialog.show(ComboTemplateScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            if (isButton) {
                switch (shape) {
                    case CIRCLE:

                        imgButton.setImageBitmap(getCircleBitmap(bmImg));
                        break;
                    case RECTANGLE:

                        imgButton.setImageBitmap(bmImg);
                        break;
                    case SQUARE:
                        imgButton.setImageBitmap(bmImg);
                        break;
                }

            } else {
                BitmapDrawable background = new BitmapDrawable(bmImg);

                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackgroundDrawable(background);
                } else {
                    view.setBackgroundDrawable(background);
                }

            }

            try {
                if ((this.mDialog != null) && this.mDialog.isShowing()) {
                    this.mDialog.dismiss();
                }
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                this.mDialog = null;
            }
        }
    }

    /**
     * Converts the rectangular image to circular image
     *
     * @param bitmap
     * @return
     */
    private Bitmap getCircleBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xffff0000;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth((float) 4);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {

        if (!isOnNewIntent) {
            handleIntent(getIntent());
            isOnNewIntent = true;
        } else {
            handleIntent(getIntent());
        }
        activity = this;
        super.onResume();

        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) mainLinearLayout.findViewById(R.id.custom_navigation), false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }

    @Override
    protected void handleIntent(Intent intent) {
        super.handleIntent(intent);
        if (intent != null) {
            mLinkId = intent.getExtras().getString("mLinkId");
            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);


            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");

            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");

            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");

            mBkgrdImage = intent.getExtras().getString("smBkgrdImage");

            departFlag = intent.getExtras().getString("departFlag");

            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
        }
    }
}
