package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.CuopnsDetailsBO;
import com.hubcity.android.commonUtil.ImageLoaderAsync;

public class CuponListAdapter extends BaseAdapter {
    private ArrayList<CuopnsDetailsBO> specialoffersList;
    private static LayoutInflater inflater = null;

    public CuponListAdapter(Activity activity,
                            ArrayList<CuopnsDetailsBO> specialoffersList) {
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.specialoffersList = specialoffersList;

    }

    @Override
    public int getCount() {
        return specialoffersList.size();

    }

    @Override
    public Object getItem(int id) {
        return specialoffersList.get(id);
    }

    @Override
    public long getItemId(int id) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = inflater.inflate(R.layout.listitem_retailer_coupons, parent,false);
            holder = new ViewHolder();
            holder.couponLongdesc = (TextView) view
                    .findViewById(R.id.coupon_longdescription);
            holder.couponImage = (ImageView) view
                    .findViewById(R.id.coupon_imagepath);
            holder.couponName = (TextView) view
                    .findViewById(R.id.coupon_product_name);
            holder.couponImage.setTag(specialoffersList.get(position)
                    .getCouponImagePath());
            holder.isCouponFeatured = (ImageView)view.findViewById(R.id.coupon_featured_icon);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        new ImageLoaderAsync(holder.couponImage).execute(specialoffersList
                .get(position).getCouponImagePath());
        holder.couponName.setText(specialoffersList.get(position)
                .getCouponName());
        String description = specialoffersList.get(position)
                .getCouponLongDescription();
        if (description != null && !description.equalsIgnoreCase("N/A"))
        holder.couponLongdesc.setText(description);
        if(specialoffersList.get(position).isFeatured()){
            holder.isCouponFeatured.setVisibility(View.VISIBLE);
        }else{
            holder.isCouponFeatured.setVisibility(View.GONE);
        }

        return view;
    }

    public static class ViewHolder {
        protected TextView couponName;
        protected ImageView couponImage;
        protected TextView couponLongdesc;
        protected ImageView isCouponFeatured;
    }
}
