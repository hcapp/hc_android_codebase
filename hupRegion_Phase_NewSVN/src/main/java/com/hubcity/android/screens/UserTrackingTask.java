package com.hubcity.android.screens;

import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

public class UserTrackingTask extends AsyncTask<Void, Void, Void> {
	Activity activity;
	String module = "";
	String shareType = "";
	String tarAddr = "";
	String moduleId = "";
	String retId = "";
	String retLocId = "";
	String pageId = "";

	String responseText = null;
	String responseCode = null;

	UrlRequestParams urlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	public UserTrackingTask(Activity activity, String module, String shareType,
			String tarAddr, String moduleId, String retId, String retLocId,
			String pageId) {
		this.activity = activity;
		this.module = module;
		this.shareType = shareType;
		this.tarAddr = tarAddr;
		this.moduleId = moduleId;
		this.retId = retId;
		this.retLocId = retLocId;
		this.pageId = pageId;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		String urlParameters = getParams();

		try {
			
			String url_share_user_tracking = Properties.url_local_server
					+ Properties.hubciti_version + "ratereview/sendersharetrack";

			JSONObject jsonResponse = mServerConnections.getUrlPostResponse(
					url_share_user_tracking, urlParameters, true);
			Log.d(" UserTrackingTask: ","jsonResponse : "+jsonResponse);
			if (jsonResponse != null) {
				if (jsonResponse.has("responseCode")) {
					responseCode = jsonResponse.getString("responseCode");
				}

				if (jsonResponse.has("responseText")) {
					responseText = jsonResponse.getString("responseText");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}

	private String getParams() {
		String urlParms = "";

		if ("Product Details".equals(module)) {
			urlParms = urlRequestParams.getShareUserTracking(shareType,
					tarAddr, module, moduleId, "", "", "");

		} else if ("Appsite".equals(module)) {
			urlParms = urlRequestParams.getShareUserTracking(shareType,
					tarAddr, module, "", retId, retLocId, "");

		} else if ("Specials".equals(module)) {
			urlParms = urlRequestParams.getShareUserTracking(shareType,
					tarAddr, module, "", retId, "", pageId);

		} else if ("Anything".equals(module)) {
			urlParms = urlRequestParams.getShareUserTracking(shareType,
					tarAddr, module, "", retId, retLocId, pageId);

		} else if ("Events".equals(module)) {
			urlParms = urlRequestParams.getShareUserTracking(shareType,
					tarAddr, module, moduleId, "", "", "");

		} else if ("Fundraiser".equals(module)) {
			urlParms = urlRequestParams.getShareUserTracking(shareType,
					tarAddr, module, moduleId, "", "", "");

		}else if ("BandSummery".equals(module)) {
			urlParms = urlRequestParams.getShareUserTracking(shareType,
					tarAddr, module, moduleId, "", "", "");

		}else if ("BandEventSummery".equals(module)) {
			urlParms = urlRequestParams.getShareUserTracking(shareType,
					tarAddr, module, moduleId, "", "", "");

		}

		return urlParms;
	}
}
