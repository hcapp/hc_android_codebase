package com.hubcity.android.screens;

import com.hubcity.android.businessObjects.Item;

public class FindSingleCategoryEntryItem implements Item
{

	protected final String retailerName;
	protected final String distance;
	protected final String address;
	protected final String address1;
	protected final String address2;
	protected final String city;
	protected final String state;
	protected final String postalCode;
	protected final String saleFlag;
	protected final String url;
	protected final String retailerId;
	protected final String retailerLocId;
	protected final String retListId;
	protected final String latitude;
	protected final String longitude;
	protected final String ribbonAdImagePath;
	protected final String ribbonAdURL;
	protected final String reference;
	protected final String bannerAd;
	protected final String locationOpen;

	public FindSingleCategoryEntryItem(String retailerName, String distance,
			String address,
			String address1,
			String address2, String city, String state, String postalCode, String saleFlag, String
			url, String retailerId,
			String retailerLocId, String retListId, String latitude,
			String longitude, String ribbonAdImagePath, String ribbonAdURL,
			String reference, String bannerAd, String locationOpen)
	{
		this.retailerName = retailerName;
		this.distance = distance;
		this.address = address;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.saleFlag = saleFlag;
		this.url = url;
		this.retailerId = retailerId;
		this.retailerLocId = retailerLocId;
		this.retListId = retListId;
		this.latitude = latitude;
		this.longitude = longitude;
		this.ribbonAdImagePath = ribbonAdImagePath;
		this.ribbonAdURL = ribbonAdURL;
		this.reference = reference;
		this.bannerAd = bannerAd;
		this.locationOpen = locationOpen;
	}

	@Override
	public boolean isSection()
	{
		return false;
	}

}
