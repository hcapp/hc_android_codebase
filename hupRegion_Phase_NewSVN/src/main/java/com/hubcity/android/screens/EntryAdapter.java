package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.ArrayList;

import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.EntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EntryAdapter extends ArrayAdapter<Item> {

	private ArrayList<Item> items;
	private LayoutInflater vi;

	public EntryAdapter(Context context, ArrayList<Item> items) {
		super(context, 0, items);

		this.items = items;
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		final Item i = items.get(position);
		if (i != null) {
			if (i.isSection()) {
				SectionItem si = (SectionItem) i;
				v = vi.inflate(R.layout.list_item_section, parent,false);

				v.setOnClickListener(null);
				v.setOnLongClickListener(null);
				v.setLongClickable(false);

				final TextView sectionView = (TextView) v
						.findViewById(R.id.list_item_section_text);
				sectionView.setText(si.getTitle());
				sectionView.setPadding(2, 2, 2, 0);
				sectionView.setTypeface(null, Typeface.NORMAL);
				sectionView.setTextColor(Color.BLACK);
				sectionView.setTextSize(16);
				sectionView.setGravity(Gravity.LEFT);
				sectionView.setBackgroundColor(0xB2BEB5);

			} else {
				EntryItem ei = (EntryItem) i;
				v = vi.inflate(R.layout.list_item_enty, parent,false);
				final TextView title = (TextView) v
						.findViewById(R.id.list_item_entry_title);
				final TextView subtitle = (TextView) v
						.findViewById(R.id.list_item_entry_summary);
				final ImageView image = (ImageView) v
						.findViewById(R.id.list_item_image);

				if (title != null) {
					title.setText(ei.alertName);
				}
				if (subtitle != null) {
					subtitle.setText(ei.subtitle);
				}
				if (image != null) {
					new DownloadImageTask(image).execute(ei.imageURL);

				}
			}
		}
		return v;

	}

	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		private RotateAnimation anim;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
			anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF,
					0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setRepeatCount(Animation.INFINITE);
			anim.setDuration(700);
			this.bmImage.setAnimation(null);
			this.bmImage.setImageResource(R.drawable.loading_button);
			this.bmImage.startAnimation(anim);
			this.bmImage.setAnimation(anim);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0].replaceAll(" ", "%20");
			Bitmap mIcon11 = null;
			try {
				
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			bmImage.setAnimation(null);
		}
	}
}