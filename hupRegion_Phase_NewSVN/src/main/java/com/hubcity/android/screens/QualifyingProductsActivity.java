package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.CouponsProductListAdapter;
import com.scansee.android.CouponsRetailListAdapter;
import com.scansee.android.RetailerCurrentsalesActivity;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class QualifyingProductsActivity extends CustomTitleBar {
	protected ArrayList<HashMap<String, String>> couponsproductsList = null;
	protected ArrayList<HashMap<String, String>> couponsLocationsList = null;
	protected HashMap<String, String> couponsproductData = null;
	protected HashMap<String, String> historyData = null;
	protected HashMap<String, String> loginData = null;
	protected ListView couponsproductListView;
	protected ArrayList<HashMap<String, String>> selectedProducts = null;
	ProgressBar locationProgress;
	Activity activity;
	CouponsProductListAdapter couponsproductListAdapter;
	CouponsRetailListAdapter couponsretailListAdapter;
	String userID, couponId;
	String addTo;
	String productId;
	String prodFlag = null;
	String locatnFlag = null;
	String type = null;
	Button rightButton;
	RadioButton radioLoc, radioProd;
	RadioGroup radioFindTab;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qualifyingproducts_listview);

		SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
		userID = settings.getString(CommonConstants.USER_ID, "0");

		if (getIntent().getExtras().getString("couponId") != null) {

			couponId = getIntent().getExtras().getString("couponId").trim();
		}

		if (getIntent().getExtras().getString("prodFlag") != null) {

			prodFlag = getIntent().getExtras().getString("prodFlag").trim();
		}

		if (getIntent().getExtras().getString("locatnFlag") != null) {
			locatnFlag = getIntent().getExtras().getString("locatnFlag").trim();
		}

		title.setSingleLine(false);
		title.setMaxLines(2);

		leftTitleImage.setVisibility(View.GONE);
		backImage.bringToFront();
		rightImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (GlobalConstants.isFromNewsTemplate){
					Intent intent = null;
					switch (GlobalConstants.className) {
						case Constants.COMBINATION:
							intent = new Intent(QualifyingProductsActivity.this, CombinationTemplate.class);
							break;
						case Constants.SCROLLING:
							intent = new Intent(QualifyingProductsActivity.this, ScrollingPageActivity.class);
							break;
						case Constants.NEWS_TILE:
							intent = new Intent(QualifyingProductsActivity.this, TwoTileNewsTemplateActivity.class);
							break;
					}
					if (intent != null) {
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
					}
				}else {
					setResult(Constants.FINISHVALUE);
					finish();
				}
			}
		});

		radioFindTab = (RadioGroup) findViewById(R.id.coupon_radio);
		radioLoc = (RadioButton) findViewById(R.id.coupon_radio_location);
		radioProd = (RadioButton) findViewById(R.id.coupon_radio_product);

		if ("1".equals(prodFlag) && "1".equals(locatnFlag)) {
			type = "product";
			radioProd.setSelected(true);
			title.setText("Select Products to Add");

		} else if ("1".equals(prodFlag)) {
			type = "product";
			findViewById(R.id.coupon_radio).setVisibility(View.GONE);
			title.setText("Products");

		} else if ("1".equals(locatnFlag)) {
			type = "location";
			findViewById(R.id.coupon_radio).setVisibility(View.GONE);
			title.setText("Locations");
		}

		radioFindTab.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup arg0, int id) {
				if (id == radioLoc.getId()) {
					type = "location";
					new CouponsLocations().execute();
					title.setText("Location");
				} else if (id == radioProd.getId()) {
					type = "product";
					new CouponsProducts().execute();
					title.setText("Select Products To Add");
				}
			}
		});

		couponsproductListView = (ListView) findViewById(R.id.couponsproduct_list);
		couponsproductListView
				.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						if ("product".equals(type)) {

							Intent navIntent = new Intent(
									QualifyingProductsActivity.this,
									RetailerCurrentsalesActivity.class);
							navIntent
									.putExtra(
											CommonConstants.TAG_CURRENTSALES_PRODUCTID,
											couponsproductsList
													.get(position)
													.get(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
							navIntent
									.putExtra(
											CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
											couponsproductsList
													.get(position)
													.get(CommonConstants.TAG_CURRENTSALES_PRODUCTNAME));

							navIntent.putExtra("Coupons", true);
							startActivityForResult(navIntent, 106);

						} else if ("location".equals(type)) {
							Intent intent = new Intent(
									QualifyingProductsActivity.this,
									RetailerActivity.class);

							if (null != couponsLocationsList.get(position).get(
									"retailerId")) {
								intent.putExtra(
										CommonConstants.TAG_RETAIL_ID,
										couponsLocationsList.get(position).get(
												"retailerId"));
							}

							if (null != couponsLocationsList.get(position).get(
									"retListId")) {
								intent.putExtra(
										CommonConstants.TAG_RETAIL_LIST_ID,
										couponsLocationsList.get(position).get(
												"retListId"));
							}
							if (null != couponsLocationsList.get(position).get(
									"retailLocationId")) {
								intent.putExtra(
										Constants.TAG_RETAILE_LOCATIONID,
										couponsLocationsList.get(position).get(
												"retailLocationId"));
							}

							if (null != couponsLocationsList.get(position).get(
									"retailerName")) {
								intent.putExtra(
										Constants.TAG_APPSITE_NAME,
										couponsLocationsList.get(position).get(
												"retailerName"));
							}

							startActivity(intent);
						}
					}
				});

		if ("product".equalsIgnoreCase(type)) {
			new CouponsProducts().execute();
		} else {
			new CouponsLocations().execute();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	private class CouponsProducts extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		JSONObject responseMenuObject = null;
		JSONArray jsonArrayProductDetail = null;
		private ProgressDialog mDialog;

		UrlRequestParams objUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(QualifyingProductsActivity.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {

			couponsproductsList = new ArrayList<>();
			String result = "false";
			try {
				String urlParameters = objUrlRequestParams
						.getQualifyingCoupDetails(couponId, type, null);
				String url_get_coup_prod_or_loc = Properties.url_local_server
						+ Properties.hubciti_version
						+ "gallery/getcouponproductorlocation";
				jsonObject = mServerConnections.getUrlPostResponse(
						url_get_coup_prod_or_loc, urlParameters, true);

				if (jsonObject != null) {

					try {
						JSONObject productDetail = jsonObject.getJSONObject(
								"CouponsDetails").getJSONObject(
								CommonConstants.FINDPRODUCTSEARCHMENU);

						jsonArrayProductDetail = new JSONArray();
						jsonArrayProductDetail.put(productDetail);
					} catch (Exception e) {
						e.printStackTrace();
						jsonArrayProductDetail = jsonObject.getJSONObject(
								"CouponsDetails").getJSONArray(
								CommonConstants.FINDPRODUCTSEARCHMENU);
					}

					for (int arrayCount = 0; arrayCount < jsonArrayProductDetail
							.length(); arrayCount++) {
						couponsproductData = new HashMap<>();
						responseMenuObject = jsonArrayProductDetail
								.getJSONObject(arrayCount);
						couponsproductData.put("selected", "false");
						couponsproductData.put("isShopCartItem",
								responseMenuObject.getString("isShopCartItem"));
						couponsproductData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTID,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTID));
						couponsproductData
								.put(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME,
										responseMenuObject
												.getString(CommonConstants.FINDPRODUCTSEARCHPRODUCTNAME));
						couponsproductData.put(
								CommonConstants.COUPONPRODUCTDESCRIPTION,
								responseMenuObject
										.getString("productDescription"));
						couponsproductData.put("rowNumber",
								responseMenuObject.getString("rowNumber"));
						couponsproductData.put("prodListId",
								responseMenuObject.getString("prodListId"));
						couponsproductData.put(
								CommonConstants.COUPONPRODUCTIMAGEPATH,
								responseMenuObject
										.getString("productImagePath"));

						couponsproductsList.add(couponsproductData);
					}
					result = "true";

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();
			if ("true".equals(result)) {

				couponsproductListAdapter = new CouponsProductListAdapter(
						QualifyingProductsActivity.this, couponsproductsList);
				couponsproductListView.setAdapter(couponsproductListAdapter);
				couponsproductListAdapter.notifyDataSetChanged();

			} else {
				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						QualifyingProductsActivity.this);
				notificationAlert.setMessage(getString(R.string.norecord))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				notificationAlert.create().show();
			}
		}

	}

	private class CouponsLocations extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		JSONObject responseMenuObject = null;
		JSONArray jsonArrayLocationDetail = null;
		private ProgressDialog mDialog;

		UrlRequestParams objUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(QualifyingProductsActivity.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {

			couponsLocationsList = new ArrayList<>();
			String result = "false";
			try {
				String urlParameters = objUrlRequestParams
						.getQualifyingCoupDetails(couponId, type, null);
				String url_get_coup_prod_or_loc = Properties.url_local_server
						+ Properties.hubciti_version
						+ "gallery/getcouponproductorlocation";
				jsonObject = mServerConnections.getUrlPostResponse(
						url_get_coup_prod_or_loc, urlParameters, true);
				if (jsonObject != null) {

					try {
						JSONObject productDetail = jsonObject.getJSONObject(
								"CouponsDetails").getJSONObject(
								CommonConstants.ALLCOUPONSRETAILERDETAIL);

						jsonArrayLocationDetail = new JSONArray();
						jsonArrayLocationDetail.put(productDetail);
					} catch (Exception e) {
						e.printStackTrace();
						jsonArrayLocationDetail = jsonObject.getJSONObject(
								"CouponsDetails").getJSONArray(
								CommonConstants.ALLCOUPONSRETAILERDETAIL);
					}

					for (int arrayCount = 0; arrayCount < jsonArrayLocationDetail
							.length(); arrayCount++) {
						HashMap<String, String> couponsData = new HashMap<>();

						responseMenuObject = jsonArrayLocationDetail
								.getJSONObject(arrayCount);

						if (responseMenuObject.has("retailerName")) {
							couponsData.put("retailerName", responseMenuObject
									.getString("retailerName"));
						}

						if (responseMenuObject.has("rowNumber")) {
							couponsData.put("rowNumber",
									responseMenuObject.getString("rowNumber"));
						}

						if (responseMenuObject.has("retailerId")) {
							couponsData.put("retailerId",
									responseMenuObject.getString("retailerId"));
						}

						if (responseMenuObject.has("retailLocationId")) {
							couponsData.put("retailLocationId",
									responseMenuObject
											.getString("retailLocationId"));
						}

						if (responseMenuObject.has("retListId")) {
							couponsData.put("retListId",
									responseMenuObject.getString("retListId"));
						}

						couponsData
								.put("completeAddress", responseMenuObject
										.getString("completeAddress"));
						couponsData.put("logoImagePath",
								responseMenuObject.getString("logoImagePath"));

						couponsLocationsList.add(couponsData);
					}
					result = "true";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();
			if ("true".equals(result)) {

				couponsretailListAdapter = new CouponsRetailListAdapter(
						QualifyingProductsActivity.this, couponsLocationsList);
				couponsproductListView.setAdapter(couponsretailListAdapter);
				couponsretailListAdapter.notifyDataSetChanged();
			} else {
				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						QualifyingProductsActivity.this);
				notificationAlert.setMessage(getString(R.string.norecord))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				notificationAlert.create().show();
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void finish() {
		super.finish();
	}

}