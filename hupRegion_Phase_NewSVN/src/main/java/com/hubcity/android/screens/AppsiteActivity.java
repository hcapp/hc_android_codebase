package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.ImageLoaderAsync;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.RetailerProductMediaActivity;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.android.ScanSeePdfViewerActivity;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

public class AppsiteActivity extends CustomTitleBar implements OnClickListener {

	private String appSiteId = null;

	private String  retailLocationID, retailerId, retailerName, retailerMile,
			 retailerAd, retailerAdURL, retListId;
	private ArrayList<HashMap<String, String>> retalerInfoList = new ArrayList<>();
	private HashMap<String, String> retailerData = new HashMap<>();
	private TextView retailerNameTextView;
	private TextView retailerMileTextView;
	private TextView retailerMileTextView2;

	private ImageView retailerADImageView;

	private ListView retailerInfoListView;

	private String latitude = null;
	private String longitude = null;

	private Context context = this;
	private LocationManager locationManager;
	private AlertDialog.Builder specialsAlert;
	private RelativeLayout retailerAdLayout;
	RelativeLayout divider;

	private LinearLayout retailerNameLayout;

	private boolean saleFlag = false;

	private boolean gpsState = false;

	private String mItemId, bottomBtnId;

	private String zipCode = null;
	protected String bannerAdImagePath;
	String isEventExists = "0";
	String isFundExists = "0";

	ShareInformation shareInfo;
	boolean isShareTaskCalled;

	private Activity activity;
	private TextView claimBusiness;
	private String claimTxtMsg = "";
	private String claimExist;
	private String userMailId;
	private String retaileraddress1;
	private boolean isItemClicked = false;
	private String responseText;
	@Override
	protected void onResume() {
		super.onResume();

		List<ApplicationInfo> packages;
		PackageManager pm;
		pm = getPackageManager();
		// get a list of installed apps.
		packages = pm.getInstalledApplications(0);

		ActivityManager mActivityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);

		for (ApplicationInfo packageInfo : packages) {
			if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1
					|| packageInfo.packageName.contains("com.adobe.reader")) {
				continue;
			}

			mActivityManager.killBackgroundProcesses(packageInfo.packageName);
		}
		if(isItemClicked) {
			claimTxtMsg = "";
			CommonConstants.BR_TAG = true;
			getRetailInfoByLocation();
			isItemClicked = false;
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.retailer_summary);

		try {
			activity = AppsiteActivity.this;
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			CommonConstants.BR_TAG = true;

			title.setSingleLine(false);
			title.setMaxLines(2);

			title.setEllipsize(TextUtils.TruncateAt.END);

			if (getIntent().hasExtra(CommonConstants.LINK_ID)) {
                appSiteId = getIntent().getExtras().getString(
                        CommonConstants.LINK_ID);
            } else if (getIntent().hasExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA)) {
                appSiteId = getIntent().getExtras().getString(
                        Constants.BOTTOM_LINK_ID_INTENT_EXTRA);
            }

			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                            Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            } else if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                            Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
                bottomBtnId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }

			claimBusiness = (TextView) findViewById(R.id.claim_business);
			retailerADImageView = (ImageView) findViewById(R.id.retailer_details_image);
			divider = (RelativeLayout) findViewById(R.id.retailer_details_line);
			retailerAdLayout = (RelativeLayout) findViewById(R.id.retailer_ad_layout);
			retailerNameLayout = (LinearLayout) findViewById(R.id.retailer_details_layout);
			retailerNameTextView = (TextView) findViewById(R.id.retailer_details_name);
			retailerMileTextView = (TextView) findViewById(R.id.retailer_details_miles);
			retailerMileTextView2 = (TextView) findViewById(R.id.retailer_details_miles2);
			retailerInfoListView = (ListView) findViewById(R.id.retailer_listview);
			retailerAdLayout.setOnClickListener(this);

			retailerInfoListView.setVisibility(View.GONE);
			retailerADImageView.setVisibility(View.GONE);
			retailerAdLayout.setVisibility(View.GONE);
			retailerNameLayout.setVisibility(View.GONE);
			divider.setVisibility(View.GONE);
			setClickListener();
			getRetailInfoByLocation();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void setClickListener() {
		claimBusiness.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				isItemClicked = true;
				if (Constants.GuestLoginId
						.equals(UrlRequestParams.getUid().trim())) {
					Constants.SignUpAlert(activity);
				}
				else {
					Intent claimScreen = new Intent(AppsiteActivity.this, ClaimBusinessActivity
							.class);
					claimScreen.putExtra("loginEmail", userMailId);
					claimScreen.putExtra("retailLocationID", retailLocationID);
					claimScreen.putExtra("retailerName", retailerName);
					claimScreen.putExtra("retaileraddress1", retaileraddress1);
					startActivity(claimScreen);
				}
			}
		});

		retailerInfoListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long pst) {
				//isItemClicked = true;
				if (retalerInfoList.get(position).get(
						CommonConstants.TAG_PAGE_LINK) != null) {
					Intent retailerLink;
					String tempLink = retalerInfoList.get(position).get(
							CommonConstants.TAG_PAGE_LINK);
					if ("specials".equalsIgnoreCase(tempLink)) {
						if ("See Current Deals!"
								.equalsIgnoreCase(retalerInfoList.get(position)
										.get(CommonConstants.TAG_PAGE_TITLE))) {
							retailerLink = new Intent(AppsiteActivity.this,
									CurrentSpecialsListActivity.class);
							retailerLink.putExtra(
									CommonConstants.TAG_RETAILE_LOCATIONID,
									retailLocationID);
							retailerLink.putExtra(
									CommonConstants.TAG_RETAILE_ID, retailerId);
							retailerLink.putExtra(
									CommonConstants.TAG_RETAIL_LIST_ID,
									retListId);
							retailerLink.putExtra(
									CommonConstants.TAG_RETAILER_NAME,
									retailerName);

							retailerLink.putExtra(
									CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
									retailerAd);
							retailerLink.putExtra("saleFlag", saleFlag);

							retailerLink.putExtra(
									CommonConstants.TAG_RIBBON_AD_URL,
									retailerAdURL);
							retailerLink.putExtra("specialitemName", "coupons");
							retailerLink.putExtra(
									CommonConstants.TAG_RETAILER_NAME,
									retailerName);
							startActivityForResult(retailerLink,
									Constants.STARTVALUE);

						} else {

							specialsAlert = new AlertDialog.Builder(
									AppsiteActivity.this);
							specialsAlert
									.setMessage(
											getString(R.string.specials_pop))
									.setCancelable(true)
									.setPositiveButton(
											getString(R.string.specials_ok),
											new DialogInterface.OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int id) {
													// TODO
												}
											})
									.setOnCancelListener(
											new OnCancelListener() {

												@Override
												public void onCancel(
														DialogInterface dialog) {
													finish();
												}
											});
							specialsAlert.create().show();

						}

					} else if ("directions".equalsIgnoreCase(tempLink)) {

						HashMap<String, String> selectedData = retalerInfoList
								.get(position);
						latitude = selectedData
								.get(CommonConstants.TAG_RET_LATITIUDE);
						longitude = selectedData
								.get(CommonConstants.TAG_RET_LONGITUDE);

						MapDirections mapDirections = new MapDirections(
								locationManager, activity, retalerInfoList.get(
								position).get(
								CommonConstants.TAG_PAGE_INFO),
								latitude, longitude, retailerName);
						mapDirections.getMapDirections();

					} else if ("call".equalsIgnoreCase(tempLink)) {
						try {
							retailerLink = new Intent(Intent.ACTION_DIAL);
							retailerLink.setData(Uri.parse("tel:"
									+ retalerInfoList.get(position).get(
									CommonConstants.TAG_PAGE_INFO)));

							startActivity(retailerLink);
						} catch (Exception e) {
							e.printStackTrace();
							new AlertDialog.Builder(AppsiteActivity.this)
									.setTitle("Alert")
									.setMessage("Call feature not available.!")
									.setPositiveButton("OK", null).show();
						}
					} else if ("browser".equalsIgnoreCase(tempLink)) {
						retailerLink = new Intent(AppsiteActivity.this,
								ScanseeBrowserActivity.class);

						retailerLink.putExtra(
								CommonConstants.URL,
								retalerInfoList.get(position).get(
										CommonConstants.TAG_PAGE_INFO));
						retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE,
								retailerName);
						startActivityForResult(retailerLink,
								Constants.STARTVALUE);
					} else if (tempLink.endsWith(".pdf")) {

						// retailerLink = new Intent(AppsiteActivity.this,
						// ScanseeFileActivity.class);

						retailerLink = new Intent(AppsiteActivity.this,
								ScanSeePdfViewerActivity.class);

						retailerLink.putExtra("isShare", true);
						retailerLink.putExtra("module", "Anything");
						retailerLink.putExtra("retailerId", retailerId);
						retailerLink.putExtra("retailLocationId",
								retailLocationID);
						retailerLink.putExtra("pageId",
								retalerInfoList.get(position).get("pageId"));

						retailerLink.putExtra(
								CommonConstants.URL,
								retalerInfoList.get(position).get(
										CommonConstants.TAG_PAGE_TEMP_LINK));
						retailerLink
								.putExtra(
										RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
										tempLink);
						retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE,
								retailerName);
						startActivityForResult(retailerLink,
								Constants.STARTVALUE);

					} else {
						// @Rekha: Calling events on tap of Events in Retailer
						// summary page
						String index = retalerInfoList.get(position).get(
								"pageCount");

						// If it is the 1st item in <RetailerCreatedPages>
						// and Events flag is true(1)
						// Event screen is called
						if ("1".equals(index) && "1".equals(isEventExists)) {
							Intent intent = new Intent(AppsiteActivity.this,
									EventsListDisplay.class);
							intent.putExtra("retailerId", retailerId);
							intent.putExtra("retListId", retListId);
							intent.putExtra("retailLocationId",
									retailLocationID);
							intent.putExtra("mItemId", mItemId);
							intent.putExtra("bottomBtnId", bottomBtnId);
							intent.putExtra("retailerEvents", "true");
							startActivity(intent);

						}

						// If it is the 1st item in <RetailerCreatedPages>
						// and Fund flag is true(1) or
						// If it is the 2st item in <RetailerCreatedPages>
						// and both Events flag and Fund Flag are true(1)
						// Fundraiser screen is called
						else if (("1".equals(index) && "1".equals(isFundExists))
								|| ("2".equals(index)
								&& "1".equals(isFundExists) && "1"
								.equals(isEventExists))) {
							Intent intent = new Intent(AppsiteActivity.this,
									FundraiserActivity.class);
							intent.putExtra("retailerId", retailerId);
							intent.putExtra("retListId", retListId);
							intent.putExtra("retailLocationId",
									retailLocationID);
							intent.putExtra("mItemId", mItemId);
							intent.putExtra("bottomBtnId", bottomBtnId);
							intent.putExtra("retailerEvents", "true");
							startActivity(intent);
						}
						// If both Fund Flag and Events flag are false(0)
						// Anything page is called
						else {
							callAnythingPage(position);

						}

					}

				}

			}

			@Override
			public boolean equals(Object o) {
				return super.equals(o);
			}

			@Override
			public int hashCode() {
				return super.hashCode();
			}
		});

		leftTitleImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				 //isItemClicked = true;
				if (!Constants.GuestLoginId.equals(UrlRequestParams
						.getUid().trim())) {
					shareInfo.shareTask(isShareTaskCalled);
					isShareTaskCalled = true;
				} else {
					Constants.SignUpAlert(AppsiteActivity.this);
					isShareTaskCalled = false;
				}
			}
		});

	}

	private void callAnythingPage(int position) {
		Intent retailerLink = new Intent(AppsiteActivity.this,
				ScanseeBrowserActivity.class);
		String url = retalerInfoList.get(position).get(
				CommonConstants.TAG_PAGE_TEMP_LINK);
		if (url.contains("/3000")) {
			retailerLink.putExtra(CommonConstants.URL, url);
			retailerLink.putExtra("isEventLogistics", true);
		} else {
			retailerLink.putExtra("isShare", true);
			retailerLink.putExtra("retailerId", retailerId);
			retailerLink.putExtra("retailLocationId", retailLocationID);
			retailerLink.putExtra("pageId",
					retalerInfoList.get(position).get("pageId"));
			retailerLink.putExtra(
					CommonConstants.URL,
					retalerInfoList.get(position).get(
							CommonConstants.TAG_PAGE_TEMP_LINK));
			retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE, "Details");

		}
		startActivityForResult(retailerLink, Constants.STARTVALUE);

	}

	private class GetRetailerInfo extends AsyncTask<String, Void, String> {
		JSONObject jsonObject, reatailerDetail, retailerPagesObj;
		JSONArray retailerPages;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {

			mDialog = ProgressDialog.show(AppsiteActivity.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);

		}

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected String doInBackground(String... params) {

			String urlParameters;

			String result = "false";

			try {
				retalerInfoList = new ArrayList<>();
				if (gpsState) {
					urlParameters = mUrlRequestParams.getAppSiteReqParam(
							mItemId, appSiteId, null, latitude, longitude,
							bottomBtnId);

				} else {

					urlParameters = mUrlRequestParams.getAppSiteReqParam(
							mItemId, appSiteId, zipCode, null, null,
							bottomBtnId);
				}

				String APPSITE_DETAIL = Properties.url_local_server
						+ Properties.hubciti_version
						+ "thislocation/appsitedetails ";

				jsonObject = mServerConnections.getUrlPostResponse(
						APPSITE_DETAIL, urlParameters, true);

				if (jsonObject != null) {
					try {
						if (jsonObject.has("response")) {
							responseText = jsonObject.getJSONObject("response").getString("responseText");
						}
						if (jsonObject.getJSONObject("RetailersDetails").has(
								"eventExist")) {
							isEventExists = jsonObject.getJSONObject(
									"RetailersDetails").getString("eventExist");
						}

						if (jsonObject.getJSONObject("RetailersDetails").has(
								"fundExist")) {
							isFundExists = jsonObject.getJSONObject(
									"RetailersDetails").getString("fundExist");
						}
						if(jsonObject.has("RetailersDetails"))
						{
							JSONObject retailerDetails = jsonObject.getJSONObject
									("RetailersDetails");
							try {
								JSONObject claimBusiness = retailerDetails.getJSONObject
										("claimTxtMsg");
								JSONArray claimArray = claimBusiness.getJSONArray("content");
								for (int i = 0; i < claimArray.length(); i++) {
									claimTxtMsg = claimTxtMsg + claimArray.get(i).toString() +
											" \n ";
								}

								if (claimTxtMsg.endsWith("\n ")) {
									claimTxtMsg = claimTxtMsg.substring(0, claimTxtMsg.length() -
											2);
								}
								claimTxtMsg.trim();
							} catch (JSONException e) {
								e.printStackTrace();
								try {
									if (retailerDetails.has("claimTxtMsg")) {
                                        claimTxtMsg = retailerDetails.getJSONObject("claimTxtMsg").optString("content");
                                        if (claimTxtMsg.endsWith("\n ")) {
                                            claimTxtMsg = claimTxtMsg.substring(0, claimTxtMsg.length() -
                                                    2);
                                        }
                                        claimTxtMsg.trim();
                                    }
								} catch (JSONException e1) {
									e1.printStackTrace();
								}
							}
							if(retailerDetails.has("claimExist")) {
								claimExist = retailerDetails.optString("claimExist");
							}
							if(retailerDetails.has("userMailId")) {
								userMailId =  retailerDetails.optString("userMailId");
							}

						}

						if (jsonObject.getJSONObject("RetailersDetails").has(
								"mainMenuId")) {
							String mainMenuId = jsonObject.getJSONObject(
									"RetailersDetails").getString("mainMenuId");

							Constants.saveMainMenuId(mainMenuId);
						}

						reatailerDetail = jsonObject
								.getJSONObject("RetailersDetails")
								.getJSONObject("retailerDetail")
								.getJSONObject("RetailerDetail");
						if (jsonObject.getJSONObject("RetailersDetails").has(
								"retailerCreatedPageList")) {
							retailerPages = jsonObject
									.getJSONObject("RetailersDetails")
									.getJSONObject("retailerCreatedPageList")
									.optJSONArray("RetailerCreatedPages");

							if (null != retailerPages) {

								retailerPages = jsonObject
										.getJSONObject("RetailersDetails")
										.getJSONObject(
												"retailerCreatedPageList")
										.getJSONArray("RetailerCreatedPages");
							} else {
								retailerPagesObj = jsonObject
										.getJSONObject("RetailersDetails")
										.getJSONObject(
												"retailerCreatedPageList")
										.getJSONObject("RetailerCreatedPages");
							}

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (reatailerDetail.has("retaileraddress1")) {
						retaileraddress1 = reatailerDetail
								.getString("retaileraddress1");
					}
					retailerName = reatailerDetail
							.getString(CommonConstants.TAG_RETAILER_NAME);
					retailerAd = reatailerDetail
							.getString(CommonConstants.TAG_RIBBON_ADIMAGE_PATH);
					if (reatailerDetail.has("retailLocationId")) {
						retailLocationID = reatailerDetail
								.getString("retailLocationId");
					}
					if (reatailerDetail.has("retailerId")) {
						retailerId = reatailerDetail.getString("retailerId");
					}
					if (reatailerDetail.has("retListId")) {
						retListId = reatailerDetail.getString("retListId");
					}
					bannerAdImagePath = reatailerDetail
							.getString(CommonConstants.TAG_BANNER_IMAGE_PATH);

					retailerMile = reatailerDetail
							.getString(CommonConstants.TAG_DISTANCE);
					latitude = reatailerDetail
							.getString(CommonConstants.TAG_FINDLOCATION_SCANSEELAT);
					longitude = reatailerDetail
							.getString(CommonConstants.TAG_FINDLOCATION_SCANSEELON);
					saleFlag = reatailerDetail
							.getBoolean(CommonConstants.TAG_SALE_FLAG);
					// adding specials
					retailerData = new HashMap<>();
					retailerData.put(CommonConstants.TAG_PAGE_LINK, "specials");
					retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);

					if (reatailerDetail
							.getBoolean(CommonConstants.TAG_SALE_FLAG)) {
						retailerData.put(CommonConstants.TAG_PAGE_TITLE,
								"See Current Deals!");
						retalerInfoList.add(retailerData);
					}
					// adding directions data
					if ((reatailerDetail.has(CommonConstants.TAG_ADDRESS1))
							&& !"N/A".equalsIgnoreCase(reatailerDetail
									.getString(CommonConstants.TAG_ADDRESS1))) {
						retailerData = new HashMap<>();
						retailerData.put(CommonConstants.TAG_PAGE_LINK,
								"directions");
						retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
						retailerData.put(CommonConstants.TAG_PAGE_TITLE,
								"Get Directions");
						retailerData
								.put(CommonConstants.TAG_PAGE_INFO,
										reatailerDetail
												.getString(CommonConstants.TAG_ADDRESS1));

						// Added By Dileep
						retailerData
								.put(CommonConstants.TAG_RET_LATITIUDE,
										reatailerDetail
												.getString(CommonConstants.TAG_FINDLOCATION_SCANSEELAT));
						retailerData
								.put(CommonConstants.TAG_RET_LONGITUDE,
										reatailerDetail
												.getString(CommonConstants.TAG_FINDLOCATION_SCANSEELON));

						// Added by Rekha
						retailerAdURL = reatailerDetail
								.getString(CommonConstants.TAG_RIBBON_AD_URL);

						retalerInfoList.add(retailerData);
					}

					// adding call options
					if ((reatailerDetail.has(CommonConstants.TAG_PHONE))
							&& !"N/A".equalsIgnoreCase(reatailerDetail
									.getString(CommonConstants.TAG_PHONE))) {
						retailerData = new HashMap<>();
						retailerData.put(CommonConstants.TAG_PAGE_LINK, "call");
						retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
						retailerData.put(CommonConstants.TAG_PAGE_TITLE,
								"Call Location");
						retailerData.put(CommonConstants.TAG_PAGE_INFO,
								reatailerDetail
										.getString(CommonConstants.TAG_PHONE));
						retalerInfoList.add(retailerData);
					}

					// adding browser options
					if ((reatailerDetail.has(CommonConstants.TAG_RETAILER_URL))
							&& !"N/A"
									.equalsIgnoreCase(reatailerDetail
											.getString(CommonConstants.TAG_RETAILER_URL))) {
						retailerData = new HashMap<>();
						retailerData.put(CommonConstants.TAG_PAGE_LINK,
								"browser");
						retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
						retailerData.put(CommonConstants.TAG_PAGE_TITLE,
								"Browse Website");
						retailerData
								.put(CommonConstants.TAG_PAGE_INFO,
										reatailerDetail
												.getString(CommonConstants.TAG_RETAILER_URL));
						retalerInfoList.add(retailerData);
					}

					// adding other list items
					if (retailerPages != null) {
						for (int pageCount = 0; pageCount < retailerPages
								.length(); pageCount++) {
							retailerData = new HashMap<>();
							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_PAGE_TEMP_LINK)) {
								retailerData
										.put(CommonConstants.TAG_PAGE_LINK,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants.TAG_PAGE_TEMP_LINK));
							}
							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_PAGE_TITLE)) {
								retailerData
										.put(CommonConstants.TAG_PAGE_TITLE,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants.TAG_PAGE_TITLE));
							}

							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_PAGE_IMAGE)) {
								retailerData
										.put(CommonConstants.TAG_PAGE_IMAGE,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants.TAG_PAGE_IMAGE));
							}
							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_PAGE_LINK)) {
								retailerData
										.put(CommonConstants.TAG_PAGE_TEMP_LINK,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants.TAG_PAGE_LINK));
							}

							if (retailerPages.getJSONObject(pageCount).has(
									"pageId")) {
								retailerData.put("pageId",
										retailerPages.getJSONObject(pageCount)
												.getString("pageId"));
							}

							retailerData.put("page", "RetailerCreatedPages");
							retailerData.put("pageCount",
									String.valueOf(pageCount + 1));

							retalerInfoList.add(retailerData);
						}
					} else if (null != retailerPagesObj) {
						retailerData = new HashMap<>();
						if (retailerPagesObj
								.has(CommonConstants.TAG_PAGE_TEMP_LINK)) {
							retailerData
									.put(CommonConstants.TAG_PAGE_LINK,
											retailerPagesObj
													.getString(CommonConstants.TAG_PAGE_TEMP_LINK));
						}

						if (retailerPagesObj.has("pageId")) {
							retailerData.put("pageId",
									retailerPagesObj.getString("pageId"));
						}

						if (retailerPagesObj
								.has(CommonConstants.TAG_PAGE_TITLE)) {
							retailerData
									.put(CommonConstants.TAG_PAGE_TITLE,
											retailerPagesObj
													.getString(CommonConstants.TAG_PAGE_TITLE));
						}

						if (retailerPagesObj
								.has(CommonConstants.TAG_PAGE_IMAGE)) {
							retailerData
									.put(CommonConstants.TAG_PAGE_IMAGE,
											retailerPagesObj
													.getString(CommonConstants.TAG_PAGE_IMAGE));
						}
						if (retailerPagesObj.has(CommonConstants.TAG_PAGE_LINK)) {
							retailerData
									.put(CommonConstants.TAG_PAGE_TEMP_LINK,
											retailerPagesObj
													.getString(CommonConstants.TAG_PAGE_LINK));
						}

						if (retailerPagesObj.has("pageId")) {
							retailerData.put("pageId",
									retailerPagesObj.getString("pageId"));
						}

						retailerData.put("page", "RetailerCreatedPages");
						retailerData.put("pageCount", String.valueOf(1));

						retalerInfoList.add(retailerData);
					}
					result = "true";

				}
			}

			catch (Exception e) {
				e.printStackTrace();
			}

			return result;

		}

		@Override
		protected void onPostExecute(String result) {
			if(claimExist != null) {
				if (claimExist.equalsIgnoreCase("1")) {
					claimBusiness.setVisibility(View.GONE);
				}
			}
			if (mDialog != null) {
				mDialog.dismiss();
			}
			if ("true".equals(result)) {
				claimBusiness.setText(claimTxtMsg);
				title.setText(retailerName);
				retailerNameTextView.setText(retailerName);
				retailerMileTextView.setText(retailerMile);
				retailerMileTextView2.setText(retailerMile);
				retailerInfoListView.setAdapter(new RetailerListAdapter(
						AppsiteActivity.this, retalerInfoList));

				retailerADImageView.setTag(retailerAd);

				new ImageLoaderAsync(retailerADImageView).execute(retailerAd);

				if (bannerAdImagePath != null
						&& !"N/A".equalsIgnoreCase(bannerAdImagePath)) {

					Intent intent = new Intent(AppsiteActivity.this,
							RetailerBannerAddSplashActivity.class);

					intent.putExtra("bannerAdUrl", bannerAdImagePath);

					startActivityForResult(intent, 1);

				} else {
					divider.setVisibility(View.VISIBLE);
					if (retailerAd != null
							&& "N/A".equalsIgnoreCase(retailerAd)) {
						retailerADImageView.setVisibility(View.GONE);
						retailerAdLayout.setVisibility(View.GONE);
						retailerNameLayout.setVisibility(View.VISIBLE);
					} else {

						retailerADImageView.setVisibility(View.VISIBLE);
						retailerAdLayout.setVisibility(View.VISIBLE);
						retailerNameLayout.setVisibility(View.GONE);
					}
					retailerInfoListView.setVisibility(View.VISIBLE);
				}

				// Call for Share Information
				shareInfo = new ShareInformation(AppsiteActivity.this,
						retailerId, retailLocationID, "", "Appsite");
				isShareTaskCalled = false;

				leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);

			}

			else {
				if (responseText != null)
					Toast.makeText(getBaseContext(), responseText,
							Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (retailerAdLayout != null && retailerAdURL != null) {
			Intent retailerLink = new Intent(AppsiteActivity.this,
					ScanseeBrowserActivity.class);
			retailerLink.putExtra(CommonConstants.URL, retailerAdURL);
			startActivity(retailerLink);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {

			if (resultCode == RESULT_OK) {

				divider.setVisibility(View.VISIBLE);
				if (retailerAd != null && "N/A".equalsIgnoreCase(retailerAd)) {
					retailerADImageView.setVisibility(View.GONE);
					retailerAdLayout.setVisibility(View.GONE);
					retailerNameLayout.setVisibility(View.VISIBLE);
				} else {

					retailerADImageView.setVisibility(View.VISIBLE);
					retailerAdLayout.setVisibility(View.VISIBLE);
					retailerNameLayout.setVisibility(View.GONE);
				}
				retailerInfoListView.setVisibility(View.VISIBLE);
			}

		}

		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}
	}

	@SuppressWarnings("deprecation")
	public void getRetailInfoByLocation() {

		String provider = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (provider.contains("gps")) {

			gpsState = true;
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (locNew != null) {

				latitude = String.valueOf(locNew.getLatitude());
				longitude = String.valueOf(locNew.getLongitude());

			} else {
				// N/W Tower Info Start
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					latitude = String.valueOf(locNew.getLatitude());
					longitude = String.valueOf(locNew.getLongitude());
				} else {
					zipCode = Constants.getZipCode();
				}
				// N/W Tower Info end

			}
			new GetRetailerInfo().execute();

		} else {
			zipCode = Constants.getZipCode();

			gpsState = false;
			new GetRetailerInfo().execute();

		}

	}

}
