package com.hubcity.android.screens;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.scansee.newsfirst.CommonMethods;

import java.util.LinkedList;

/**
 * Created by subramanya.v on 9/20/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class MultipleBandAdapter extends BaseAdapter
{
	private final Activity activity;
	private LinkedList<RetailerDetail> retailerDetail;
	private LinkedList<MultipleBandObj> multileBand;


	public MultipleBandAdapter(Activity activity, LinkedList<RetailerDetail> retailerDetail,
			LinkedList<MultipleBandObj> multileBand)
	{
		this.activity = activity;
		this.retailerDetail = retailerDetail;
		this.multileBand = multileBand;
	}

	@Override
	public int getCount()
	{
		return retailerDetail.size();
	}

	@Override
	public Object getItem(int i)
	{
		return retailerDetail.get(i);
	}

	@Override
	public long getItemId(int i)
	{
		return 0;
	}

	@Override
	public View getView(int position, View converView, ViewGroup parent)
	{
		View vi = converView;
		ViewHolder holder;
		int maxRowNum = multileBand.get(0).getMaxRowNum();
		int maxCnt = multileBand.get(0).getMaxCnt();
		int pagPosition = position + 1;
		if (maxRowNum < maxCnt) {
			if (pagPosition == maxRowNum) {
				if(!((MultipleBand)activity).isLoaded)
				{
					((MultipleBand)activity).startPagination(maxRowNum);
				}
			}
		}

		String bandName = retailerDetail.get(position).getBandName();
		String bandImage = retailerDetail.get(position).getBandImgPath();
		String genre = retailerDetail.get(position).getCatName();
		String startTime = retailerDetail.get(position).getsTime();
		String endTime = retailerDetail.get(position).geteTime();
		if (converView == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			vi = inflater.inflate(R.layout.multiple_band_adapter, parent, false);
			holder = new ViewHolder();
			holder.bandName = (TextView) vi.findViewById(R.id.band_name);
			holder.bandImage = (ImageView) vi.findViewById(R.id.band_image);
			holder.genre = (TextView) vi.findViewById(R.id.genre);
			holder.time = (TextView) vi.findViewById(R.id.time);
			holder.progressBar = (ProgressBar) vi.findViewById(R.id.progress);
			vi.setTag(holder);
		} else {
			holder = (ViewHolder) vi.getTag();
		}
		if ( bandName != null && !bandName.isEmpty() && !bandName.equalsIgnoreCase("N/A")) {
			holder.bandName.setText(bandName);
		}
		if ( genre != null && !genre.isEmpty() && !genre.equalsIgnoreCase("N/A")) {
			holder.genre.setText(genre);
		}
		if (startTime != null &&  endTime != null && !startTime.isEmpty()  && !startTime.equalsIgnoreCase("N/A") &&
				!endTime
				.isEmpty() && !endTime.equalsIgnoreCase("N/A")) {
			holder.time.setText(startTime +" "+ "-"+" " + endTime);
		}
		if (bandImage != null && !bandImage.equalsIgnoreCase("") && !bandImage.equalsIgnoreCase
				("N/A")) {
			bandImage = bandImage.replaceAll(" ", "%20");
			new CommonMethods().loadImage(activity, holder.progressBar, bandImage, holder
					.bandImage);
		}
		return vi;
	}

	public void updateList(LinkedList<RetailerDetail> retailerDetail, LinkedList<MultipleBandObj> multileBand)
	{
		this.retailerDetail = retailerDetail;
		this.multileBand = multileBand;
	}

	private class ViewHolder
	{
		TextView bandName;
		ImageView bandImage;
		TextView genre;
		TextView time;
		ProgressBar progressBar;
	}
}
