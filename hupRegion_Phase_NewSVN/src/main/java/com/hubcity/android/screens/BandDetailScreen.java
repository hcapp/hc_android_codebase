package com.hubcity.android.screens;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by subramanya.v on 4/19/2016.
 */
public class BandDetailScreen extends CustomTitleBar implements AdapterView.OnItemClickListener {
    private ListView detailList;
    private LinearLayout screenParent;
    private ImageView screeenImage;
    private int screenWidth;
    private String mBannerImg;
    private RelativeLayout bannerHolder;
    private RelativeLayout splashFrame;
    private ProgressBar progressBar;
    private double latitude;
    private double longitude;
    private ArrayList<BandSummary> bandDetails = new ArrayList<>();
    private ArrayList<List<Integer>> mainListData = new ArrayList<>();
    private Map<String, ArrayList<List<Integer>>> listValue = new LinkedHashMap();
    private String bandURL;
    private String bandImgPath;
    private String mLinkId;
    private String retailerId;
    private String bandID;
    private String bandName;
    private ShareInformation shareInfo;
    private Activity activity;
    private boolean isShareTaskCalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.band_detail_screen);
        activity = BandDetailScreen.this;
        getKeyHash();
        bindView();
        getIntentData();
        setListener();
        addShareIcon();
        screenParent.setVisibility(View.INVISIBLE);
        ViewTreeObserver observer = screenParent.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                screenWidth = screenParent.getMeasuredWidth();

                ViewTreeObserver obs = screenParent.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    //noinspection deprecation
                    obs.removeGlobalOnLayoutListener(this);
                }
                initializBandSummaryScreen();
            }

        });
    }

    private void getKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.scansee.austin", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        screeenImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bandURL != null && !bandURL.equalsIgnoreCase("N/A")&& !bandURL.equalsIgnoreCase("")) {
                    Intent bandScreen = new Intent(BandDetailScreen.this, ScanseeBrowserActivity
                            .class);
                    bandScreen.putExtra(CommonConstants.URL, bandURL);
                    startActivity(bandScreen);
                }
            }
        });
        detailList.setOnItemClickListener(this);
    }

    private void getIntentData() {

        if (getIntent().hasExtra("mLinkId")) {
            mLinkId = getIntent().getExtras().getString("mLinkId");
        }
        if (getIntent().hasExtra("bandID")) {
            bandID = getIntent().getExtras().getString("bandID");
        } else if (getIntent().hasExtra("EventBandID")) {
            bandID = getIntent().getExtras().getString("EventBandID");
        }
        if (getIntent().hasExtra(CommonConstants.TAG_RETAIL_ID)) {
            if (getIntent().getExtras().getString(CommonConstants.TAG_RETAIL_ID) != null) {
                retailerId = getIntent().getExtras().getString(
                        CommonConstants.TAG_RETAIL_ID);
            }
        }
    }

    private void bindView() {
        splashFrame = (RelativeLayout) findViewById(R.id.retailer_splash_frame);
        detailList = (ListView) findViewById(R.id.detail_list);
        screenParent = (LinearLayout) findViewById(R.id.screen_parent);
        screeenImage = (ImageView) findViewById(R.id.screen_image);
        bannerHolder = (RelativeLayout) findViewById(R.id.banner_holder);
        progressBar = (ProgressBar) findViewById(R.id.progress);

    }

    private void initializBandSummaryScreen() {
        BandSummaryAsyTask bandSummaryObj = new BandSummaryAsyTask();
        bandSummaryObj.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void addShareIcon() {
        // Call for Share Information
        shareInfo = new ShareInformation(activity, bandID, "BandSummery");

        leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
        leftTitleImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!Constants.GuestLoginId.equals(UrlRequestParams
                        .getUid().trim())) {
                    shareInfo.shareTask(isShareTaskCalled);
                    isShareTaskCalled = true;
                } else {
                    Constants.SignUpAlert(BandDetailScreen.this);
                    isShareTaskCalled = false;
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String key = (String) listValue.keySet().toArray()[position];

        String listData = String.valueOf(mainListData.get(position).get(0));
        if (key.contains("bandURL")) {
            Intent browserSceen = new Intent(BandDetailScreen.this, ScanseeBrowserActivity
                    .class);
            browserSceen.putExtra(CommonConstants.URL, listData);
            startActivity(browserSceen);
        } else if (key.contains("mail")) {
            sendEmail(listData);
        } else if (key.contains("phone")) {
            try {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"
                        + listData));
                startActivity(callIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ((bandDetails.get(position).getEventExist() == 1) && (bandDetails.get(position)
                .getPageLink().equalsIgnoreCase("N/A"))) {
            Intent eventMap = new Intent(BandDetailScreen.this, EventMapScreen
                    .class);
            eventMap.putExtra("BandSummary", true);
            eventMap.putExtra("bandId", bandID);
            eventMap.putExtra("bandName", bandName);
            startActivity(eventMap);

        } else {
            callAnythingPage(position);
        }

    }

    private void callAnythingPage(int position) {
        String url = bandDetails.get(position).getPageLink();

        Intent retailerLink = new Intent(BandDetailScreen.this,
                ScanseeBrowserActivity.class);

        if (url.contains("/3000")) {
            retailerLink.putExtra(CommonConstants.URL, url);
            retailerLink.putExtra("isEventLogistics", true);

        } else {

           /* retDetailsId = bandDetails.get(position).get(
                    CommonConstants.TAG_RET_DETAILS_ID);*/

            String anythingPageId = bandDetails.get(position).getAnythingPageListId();

			retailerLink.putExtra(CommonConstants.URL, url);
			retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE, "Details");
			retailerLink.putExtra(
					CommonConstants.TAG_ANYTHING_PAGE_LIST_ID,
					anythingPageId);
			retailerLink.putExtra(CommonConstants.LINK_ID, mLinkId);
			retailerLink.putExtra("isShare", false);
			retailerLink.putExtra("retailerId", retailerId);
			retailerLink.putExtra("pageId",
					bandDetails.get(position).getPageId());


          /*  new SendAnythingPageId().execute(bandDetails.get(position).getRetListId(),
          anythingPageId,
                    anythingPageClkStrng);*/
        }
        startActivityForResult(retailerLink, Constants.STARTVALUE);

    }


    private void sendEmail(String retailerEmail) {
        String[] TO = {retailerEmail};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(BandDetailScreen.this, "There is no email client installed.", Toast
                    .LENGTH_SHORT)
                    .show();
        }
    }

    private class BandSummaryAsyTask extends AsyncTask<String, Void, String> {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(BandDetailScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);

            getGPSValues();
        }

        @Override
        protected String doInBackground(String... params) {

            String url_events = Properties.url_local_server
                    + Properties.hubciti_version + "band/bandsummary";
            String Result = null;

            JSONObject urlParameters;
            try {
                urlParameters = mUrlRequestParams
                        .createbandSummary(bandID, latitude, longitude);
                JSONObject jsonResponse = mServerConnections
                        .getUrlJsonPostResponse(url_events, urlParameters);
                if (jsonResponse != null) {
                    if (jsonResponse.getString("responseText").equalsIgnoreCase("Success")) {

                        if (jsonResponse.has("retailerDetail")) {
                            JSONArray retailerDetail = jsonResponse
                                    .getJSONArray("retailerDetail");
                            for (int i = 0; i < retailerDetail.length(); i++) {
                                JSONObject elems = retailerDetail
                                        .getJSONObject(i);
                                if (elems.has("ribbonAdURL")) {
                                    bandURL = elems.optString("ribbonAdURL");
                                }
                                if (elems.has("bandName")) {
                                    bandName = elems.optString("bandName");
                                }
								/*if (elems.has("bandID")) {
									bandID = elems.optString("bandID");
								}*/
                                if (elems.has("bandImgPath")) {
                                    bandImgPath = elems.optString("bandImgPath");
                                }
                                if (elems.has("ribbonAdImagePath")) {
                                    mBannerImg = elems.optString("ribbonAdImagePath");
                                }

                                if (elems.has("contactPhone") && elems.has("phoneImg")) {
                                    if (!(elems
                                            .getString("contactPhone").equalsIgnoreCase("N/A")) &&
                                            !(elems
                                                    .getString("phoneImg").equalsIgnoreCase("N/A")
                                            )) {
                                        ArrayList arrayList = new ArrayList();
                                        arrayList.add(elems.optString("contactPhone"));
                                        arrayList.add(elems.optString("phoneImg"));
                                        mainListData.add(arrayList);
                                        listValue.put("phone", arrayList);

                                        BandSummary summaryObj = new BandSummary();
                                        bandDetails.add(summaryObj);
                                    }
                                }
                                if (elems.has("bandURL") && elems.has("bandURLImgPath")) {
                                    if (!(elems
                                            .getString("bandURL").equalsIgnoreCase("N/A")) &&
                                            !(elems
                                                    .getString("bandURLImgPath").equalsIgnoreCase
                                                            ("N/A"))) {

                                        ArrayList arrayList = new ArrayList();
                                        arrayList.add(elems.optString("bandURL"));
                                        arrayList.add(elems.optString("bandURLImgPath"));
                                        mainListData.add(arrayList);
                                        listValue.put("bandURL", arrayList);

                                        BandSummary summaryObj = new BandSummary();
                                        bandDetails.add(summaryObj);
                                    }
                                }

                                if (elems.has("mail") && elems.has("mailImg")) {
                                    if (!(elems
                                            .getString("mail").equalsIgnoreCase("N/A")) &&
                                            !(elems
                                                    .getString("mailImg").equalsIgnoreCase("N/A")
                                            )) {

                                        ArrayList arrayList = new ArrayList();
                                        arrayList.add(elems.optString("mail"));
                                        arrayList.add(elems.optString("mailImg"));
                                        mainListData.add(arrayList);
                                        listValue.put("mail", arrayList);

                                        BandSummary summaryObj = new BandSummary();
                                        bandDetails.add(summaryObj);
                                    }
                                }

                            }

                        }
                        if (jsonResponse.has("retailerCreatedPageList")) {
                            JSONArray retailerDetail = jsonResponse
                                    .getJSONArray("retailerCreatedPageList");
                            for (int i = 0; i < retailerDetail.length(); i++) {
                                JSONObject elems = retailerDetail
                                        .getJSONObject(i);

                                if (elems.has("pageTitle")) {
                                    ArrayList arrayList = new ArrayList();
                                    arrayList.add(elems.optString("pageTitle"));
                                    arrayList.add(elems.optString("pageImage"));
                                    mainListData.add(arrayList);
                                    listValue.put(elems.optString("pageTitle"), arrayList);

                                    BandSummary summaryObj = new BandSummary();
                                    summaryObj.setPageLink(elems.optString("pageLink"));
                                    summaryObj.setPageTitle(elems.optString("pageTitle"));
                                    summaryObj.setPageImage(elems.optString("pageImage"));
                                    summaryObj.setPageId(elems.optString("pageId"));
                                    summaryObj.setRetListId(elems.optString("retListId"));
                                    summaryObj.setAnythingPageListId(elems.optString
                                            ("anythingPageListId"));
                                    summaryObj.setPageTempLink(elems.optString("pageTempLink"));
                                    if (elems.optString("pageLink").equalsIgnoreCase("N/A")) {
                                        summaryObj.setEventExist(1);
                                    } else {
                                        summaryObj.setEventExist(0);
                                    }
                                    bandDetails.add(summaryObj);
                                }

                            }

                        }
                        Result = "Success";
                    } else if (jsonResponse.getString("responseText").equalsIgnoreCase("No " +
                            "Records" +
                            " Found.")) {
                        Result = "No Records Found.";
                    } else {
                        if (jsonResponse.has("responseText")) {
                            Result = jsonResponse.getString("responseText");
                        }
                    }
                } else {
                    Result = "Technical problem";
                }

            } catch (PackageManager.NameNotFoundException | JSONException e) {
                e.printStackTrace();
            }


            return Result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            title.setText(bandName);
            splashDisplay();
            if (splashFrame.getVisibility() == View.VISIBLE) {
                try {
                    synchronized (this) {
                        wait(3000);
                    }
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } finally {
                    splashFrame.setVisibility(View.INVISIBLE);
                    screenParent.setVisibility(View.VISIBLE);
                }
            } else {
                splashFrame.setVisibility(View.INVISIBLE);
                screenParent.setVisibility(View.VISIBLE);
            }

            if (result.equalsIgnoreCase("Success")) {

                DetailListAdapter detailListObj = new DetailListAdapter(BandDetailScreen.this,
                        mainListData);
                detailList.setAdapter(detailListObj);

                //perform image width and height based on aspect ratio
                if ((null != mBannerImg) && !(mBannerImg.equalsIgnoreCase("N/A")) && !mBannerImg
                        .equals("")) {
                    float bannerHeight = 422;
                    float bannerWidth = 750;
                    bannerHeight = (bannerHeight / bannerWidth) * screenWidth;
                    ViewGroup.LayoutParams bannerParams = bannerHolder.getLayoutParams();
                    bannerParams.height = (int) bannerHeight;

                    screeenImage.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    mBannerImg = mBannerImg.replaceAll(" ", "%20");
                    Picasso.with(BandDetailScreen.this).load(mBannerImg).resize((int) screenWidth,
                            (int)
                                    bannerHeight).into(screeenImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    bannerHolder.setVisibility(View.GONE);
                }

                bannerHolder.setVisibility(View.VISIBLE);
                detailList.setVisibility(View.VISIBLE);

            } else if (result.equalsIgnoreCase("No Records Found.")) {
                CommonConstants.displayToast(activity, getString(R.string.norecord));
            } else {
                CommonConstants.displayToast(activity, "Technical problem");
            }
            mDialog.dismiss();
        }

    }

    private void splashDisplay() {

        if (bandImgPath != null && !("N/A".equalsIgnoreCase(bandImgPath))) {

            bannerHolder.setVisibility(View.GONE);
            detailList.setVisibility(View.GONE);
            splashFrame.setVisibility(View.VISIBLE);

            Intent intent = new Intent(BandDetailScreen.this,
                    RetailerBannerAddSplashActivity.class);

            bandImgPath = bandImgPath.replaceAll(" ", "%20");
            intent.putExtra("bannerAdUrl", bandImgPath);

            startActivityForResult(intent, 1);
        } else {
            splashFrame.setVisibility(View.INVISIBLE);
        }

    }

    private void getGPSValues() {
        LocationManager locationManager = (LocationManager) getSystemService(Context
                .LOCATION_SERVICE);
        @SuppressWarnings("deprecation") String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                latitude = locNew.getLatitude();
                longitude = locNew.getLongitude();

            } else {
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    latitude = locNew.getLatitude();
                    longitude = locNew.getLongitude();
                } else {
                    if (CommonConstants.LATITUDE != null) {
                        latitude = Double.parseDouble(CommonConstants.LATITUDE);
                    }
                    if (CommonConstants.LONGITUDE != null) {
                        longitude = Double.parseDouble(CommonConstants.LONGITUDE);
                    }

                }
            }
        }
    }
}
