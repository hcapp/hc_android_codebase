package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.FundraiserEntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class FundraiserEventsAdapter extends ArrayAdapter<Item> {

	private ArrayList<Item> items;
	private LayoutInflater vi;

	protected CustomImageLoader customImageLoader;
	Activity activity;

	public FundraiserEventsAdapter(Context context, ArrayList<Item> items,
			Activity activity) {
		super(context, 0, items);

		this.items = items;
		this.activity = activity;
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		customImageLoader = new CustomImageLoader(context.getApplicationContext(), false);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		final Item i = items.get(position);

		/***
		 * If the list item reaches to its last position then it will be called
		 * for next pagination
		 ***/
		if (position == items.size() - 1) {
			if (!((FundraiserActivity) activity).isAlreadyLoading) {
				if (((FundraiserActivity) activity).nextPage) {
					((FundraiserActivity) activity).isAlreadyLoading = true;
					((FundraiserActivity) activity).fundsViewMoreButton();
				}
			}
		}

		if (i != null) {
			if (i.isSection()) {
				SectionItem si = (SectionItem) i;
				v = vi.inflate(R.layout.fundraiser_events_list_item_section,
						parent,false);

				v.setOnClickListener(null);
				v.setOnLongClickListener(null);
				v.setLongClickable(false);

				final TextView sectionView = (TextView) v
						.findViewById(R.id.fundraiser_events_list_item_section_text);
				sectionView.setText(si.getTitle());

			} else {
				FundraiserEntryItem ei = (FundraiserEntryItem) i;
				v = vi.inflate(R.layout.fundraiser_events_list_item_entry, parent,false);
				final TextView title = (TextView) v
						.findViewById(R.id.fundraiser_events_list_item_entry_title);
				final TextView subtitle = (TextView) v
						.findViewById(R.id.fundraiser_events_list_item_entry_summary);
				final ImageView image = (ImageView) v
						.findViewById(R.id.fundraiser_events_list_item_main_image);

				if (title != null) {
					title.setText(ei.fundName);
				}
				if (subtitle != null) {

					subtitle.setText(ei.startDate);
				}
				if (image != null) {

					if (ei.imagePath != null) {
						image.setTag(ei.imagePath);
						customImageLoader.displayImage(ei.imagePath, activity, image);
					} else {
						image.setImageBitmap(null);
					}
				}

			}
		}

		return v;

	}

}
