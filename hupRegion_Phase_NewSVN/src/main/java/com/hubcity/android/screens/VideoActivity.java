package com.hubcity.android.screens;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.Toast;
import android.widget.VideoView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

public class VideoActivity extends CustomTitleBar {

    private VideoView mVideoView;
    private String mPath;
    private String current;

    ShareInformation shareInfo;
    boolean isShareTaskCalled;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.play_video_layout);

        title.setSingleLine(true);
        title.setMaxLines(1);
        title.setWidth(290);
        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setText("Details");
        mVideoView = (VideoView) findViewById(R.id.surface_view);

        mPath = getIntent().getExtras().getString(CommonConstants.URL);

        Button mPlay = (Button) findViewById(R.id.play);
        Button mPause = (Button) findViewById(R.id.pause);
        Button mReset = (Button) findViewById(R.id.reset);
        Button mStop = (Button) findViewById(R.id.stop);
        if (UrlRequestParams.getUid().equals("-1")) {
            leftTitleImage.setVisibility(View.GONE);
            rightImage.setVisibility(View.GONE);
        }

        mPlay.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                playVideo();
            }
        });
        mPause.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (mVideoView != null) {
                    mVideoView.pause();
                }
            }
        });
        mReset.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (mVideoView != null) {
                    mVideoView.seekTo(0);
                }
            }
        });
        mStop.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (mVideoView != null) {
                    current = null;
                    mVideoView.stopPlayback();
                }
            }
        });
        runOnUiThread(new Runnable() {
            public void run() {
                playVideo();

            }

        });

        // Call for Share Information
        if (getIntent().hasExtra("isShare")
                && getIntent().getExtras().getBoolean("isShare")) {
            shareInfo = new ShareInformation(VideoActivity.this, "", "",
                    getIntent().getExtras().getString("pageId"), "Anything");
            isShareTaskCalled = false;

            leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
            leftTitleImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!Constants.GuestLoginId.equals(UrlRequestParams
                            .getUid().trim())) {
                        shareInfo.shareTask(isShareTaskCalled);
                        isShareTaskCalled = true;
                    } else {
                        Constants.SignUpAlert(VideoActivity.this);
                        isShareTaskCalled = false;
                    }
                }
            });
        }
    }

    private void playVideo() {
        try {

            if (mPath == null || mPath.length() == 0) {

                Toast.makeText(VideoActivity.this, "Media Path Not Available",
                        Toast.LENGTH_LONG).show();

            } else {
                // If the path has not changed, just start the media player
                if (mPath.equals(current) && mVideoView != null) {
                    mVideoView.start();
                    mVideoView.requestFocus();
                    return;
                }
                new FetchVideoAsync().execute(mPath);

            }
        } catch (Exception e) {
            e.printStackTrace();
            if (mVideoView != null) {
                mVideoView.stopPlayback();
            }
        }
    }


    class FetchVideoAsync extends AsyncTask<String, Void, String> {

        ProgressDialog pgDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pgDialog = ProgressDialog.show(VideoActivity.this, "",
                    "Downloading video..", true);
            pgDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... path) {
            String tempPath = null;
            if (!URLUtil.isNetworkUrl(path[0])) {
                tempPath = path[0];
            } else {
                try {
                    URL url = new URL(path[0]);
                    URLConnection cn = url.openConnection();
                    cn.connect();
                    InputStream stream = cn.getInputStream();
                    if (stream == null) {
                        throw new RuntimeException("stream is null");
                    }
                    File temp = File.createTempFile("mediaplayertmp", "dat");
                    temp.deleteOnExit();
                    tempPath = temp.getAbsolutePath();
                    FileOutputStream out = new FileOutputStream(temp);
                    byte buf[] = new byte[128];
                    do {
                        int numread = stream.read(buf);
                        if (numread <= 0) {
                            break;
                        }
                        out.write(buf, 0, numread);
                    } while (true);
                    try {
                        stream.close();
                        out.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return tempPath;
        }

        @Override
        protected void onPostExecute(String path) {
            super.onPostExecute(path);
            try {
                pgDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            current = mPath;
            mVideoView.setVideoPath(path);
            mVideoView.start();
            mVideoView.requestFocus();
        }
    }
}
