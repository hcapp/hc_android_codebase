package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class GroupedTabMenuScreen extends MenuPropertiesActivity {


    ImageView mBannerImageView;
    private ProgressBar progressBar;
    private RelativeLayout bannerHolder;
    private Context mContext;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;

    @Override
    protected void onPause() {

        menuListTemplate.setBackgroundColor(Color.parseColor("#ffffff"));
        parent.setBackgroundColor(Color.parseColor("#ffffff"));
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = GroupedTabMenuScreen.this;
        CommonConstants.hamburgerIsFirst = true;
        templateName = GROUPED_TAB;
        Intent intent = getIntent();
        previousMenuLevel = intent.getExtras().getString(
                Constants.MENU_LEVEL_INTENT_EXTRA);


        parent = new RelativeLayout(this);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        parent.setLayoutParams(param);

        LinearLayout parentIntermideate = new LinearLayout(this);
        parentIntermideate.setOrientation(LinearLayout.VERTICAL);

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        menuListTemplate = layoutInflater.inflate(R.layout.menu_list_template,
                null, false);
// Initializing drawer layout
        drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);
        LinearLayout.LayoutParams menuListTemplateParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        menuListTemplate.setLayoutParams(menuListTemplateParam);

        BottomButtonListSingleton mBottomButtonListSingleton;
        mBottomButtonListSingleton = BottomButtonListSingleton
                .getListBottomButton();

        if (mBottomButtonListSingleton != null) {

            ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                    .listBottomButton;

            if (listBottomButton != null && listBottomButton.size() > 1) {
                int px = (int) (50 * this.getResources().getDisplayMetrics().density + 0.5f);
                menuListTemplateParam.setMargins(0, 0, 0, px);
            }
        }

        menuListTemplate.setLayoutParams(menuListTemplateParam);

        LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        parentIntermideate.setLayoutParams(parentParam);
        addTitleBar(parentIntermideate, parent);
        // adding banner image
//		mBannerLinearLayout = (LinearLayout) layoutInflater.inflate(
//				R.layout.mbannertwocol, null, false);
        mBannerImageView = (ImageView) menuListTemplate
                .findViewById(R.id.top_image);
        progressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
        bannerHolder = (RelativeLayout) menuListTemplate.findViewById(R.id.banner_holder);
        bannerParent = (LinearLayout) menuListTemplate.findViewById(R.id.bannerParent);
//		parentIntermideate.addView(mBannerLinearLayout);
        parentIntermideate.addView(menuListTemplate);
        parent.addView(parentIntermideate);

        createbottomButtontTab(parent);

        setContentView(parent);

        handleIntent(intent);

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            if (mBkgrdColor != null && !"N/A".equals(mBkgrdColor)) {
                menuListTemplate.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(mBkgrdColor));

            } else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
                new ImageLoaderAsync1(menuListTemplate).execute(mBkgrdImage);

            }

        } else {
            if (smBkgrdColor != null && !"N/A".equals(smBkgrdColor)) {
                menuListTemplate.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(smBkgrdColor));

            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {

                new ImageLoaderAsync1(menuListTemplate).execute(smBkgrdImage);

            }

        }
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            HubCityContext.level = 0;
        } else {
            HubCityContext.level = HubCityContext.level + 1;
        }
        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if (mBannerImg != null) {
                mBannerImageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(GroupedTabMenuScreen.this).load(mBannerImg).into(mBannerImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        } else {
            customHorizontalTicker = (CustomHorizontalTicker) menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker) menuListTemplate.findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher) menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext,customHorizontalTicker, customVerticalTicker, bannerParent, customRotate,bannerHolder);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    class ImageLoaderAsync1 extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync1(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {

            mDialog = ProgressDialog.show(GroupedTabMenuScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            BitmapDrawable background = new BitmapDrawable(bmImg);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                bmImage.setBackgroundDrawable(background);
            } else {
                bmImage.setBackgroundDrawable(background);
            }

            try {
                if ((this.mDialog != null) && this.mDialog.isShowing()) {
                    this.mDialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                e.printStackTrace();
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                this.mDialog = null;
            }
        }
    }

    protected void createListView(ArrayList<MainMenuBO> list,
                                  BaseAdapter adapter) {

        if (list != null) {

            menuList = (ListView) menuListTemplate.findViewById(R.id.menu_list);
            menuList.setAdapter(adapter);
        }
    }

    protected ListView menuList;

    private GroupedTabMenuRowAdapter adapter;
    private View menuListTemplate;

    private ArrayList<MainMenuBO> mainMenuUnsortedList;
    private String mBkgrdColor;

    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);

            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");

            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");

            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");

            mBkgrdImage = intent.getExtras().getString("smBkgrdImage");

            departFlag = intent.getExtras().getString("departFlag");

            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");

            setDataAdapter(mainMenuUnsortedList);
            adapter.notifyDataSetChanged();
        }

        super.handleIntent(intent);

    }

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        super.onNewIntent(intent);
    }

    protected void setDataAdapter(ArrayList<MainMenuBO> mainMenuUnsortedList) {

        adapter = new GroupedTabMenuRowAdapter(this, mainMenuUnsortedList,
                previousMenuLevel);
        createListView(mainMenuUnsortedList, adapter);

        this.menuList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                onTemplateListMenuRowItemClick(arg0, arg1, arg2, arg3);

            }

        });
    }

    private void onTemplateListMenuRowItemClick(final AdapterView<?> arg0,
                                                final View view, final int position, final long id) {
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);


        super.mItemName = mMainMenuBO.getmItemName();
        super.mItemId = mMainMenuBO.getmItemId();
        super.linkTypeId = mMainMenuBO.getLinkTypeId();
        super.linkTypeName = mMainMenuBO.getLinkTypeName();
        super.mMainMenuBOPosition = mMainMenuBO.getPosition();
        super.mItemImgUrl = mMainMenuBO.getmItemImgUrl();
        super.mLinkId = mMainMenuBO.getLinkId();
        super.mBtnColor = mMainMenuBO.getmBtnColor();
        super.mBtnFontColor = mMainMenuBO.getmBtnFontColor();
        super.smBtnColor = mMainMenuBO.getSmBtnColor();
        super.smBtnFontColor = mMainMenuBO.getSmBtnFontColor();

        linkClickListener(linkTypeName);
    }

    @Override
    protected void onResume() {

        handleIntent(getIntent());
        adapter.notifyDataSetChanged();

        setupMainMenuContents(parent);
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            if (mBkgrdColor != null && !"N/A".equals(mBkgrdColor)) {
                menuListTemplate.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));

                parent.setBackgroundColor(Color.parseColor(mBkgrdColor));

            } else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
                new ImageLoaderAsync1(menuListTemplate).execute(mBkgrdImage);

            }

        } else {
            if (smBkgrdColor != null && !"N/A".equals(smBkgrdColor)) {
                menuListTemplate.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));

                parent.setBackgroundColor(Color.parseColor(smBkgrdColor));

            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {

                new ImageLoaderAsync1(menuListTemplate).execute(smBkgrdImage);

            }

        }
        activity = this;
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }

}

class GroupedTabMenuRowAdapter extends BaseAdapter {
    Activity mContext;
    ArrayList<MainMenuBO> mlist;
    CustomImageLoader customImageLoader;
    String level;
    String btnFontColor, btnColor;
    String grpFontColor, grpBkColor;
    String nextItemName;
    String nextLinkTypeName;

    GroupedTabMenuRowAdapter(Activity context, ArrayList<MainMenuBO> list,
                             String menuLevel) {
        mContext = context;
        mlist = list;
        customImageLoader = new CustomImageLoader(context, false);
        level = menuLevel;
    }

    static class ViewHolder {
        ImageView icon;
        ImageView disclosureIcon;
        TextView text;
        LinearLayout menuListTemplateParent;
        protected LinearLayout textViewLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder;

        LayoutInflater li = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = li
                .inflate(R.layout.template_list_menu_row, parent, false);

        mViewHolder = new ViewHolder();

        mViewHolder.textViewLayout = (LinearLayout) convertView
                .findViewById(R.id.textView_layout);
        mViewHolder.textViewLayout
                .setLayoutParams(new LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        mViewHolder.icon = (ImageView) convertView
                .findViewById(R.id.imageView_row_icon);
        mViewHolder.icon.setVisibility(View.GONE);

        mViewHolder.text = (TextView) convertView
                .findViewById(R.id.tv_row_header);

        mViewHolder.disclosureIcon = (ImageView) convertView
                .findViewById(R.id.imageView_disclosure);

        mViewHolder.menuListTemplateParent = (LinearLayout) convertView
                .findViewById(R.id.menu_list_template_parent);

        convertView.setTag(mViewHolder);

        MainMenuBO item = (MainMenuBO) mlist.get(position);

        String itemName = item.getmItemName();
        String linkTypeName = item.getLinkTypeName();

        // @Beena : to check the next node in list
        if (position + 1 < mlist.size()) {

            MainMenuBO nextItem = (MainMenuBO) mlist.get(position + 1);
            nextItemName = nextItem.getmItemName();
            nextLinkTypeName = nextItem.getLinkTypeName();
        }

        if (level != null && "1".equals(level)) {
            btnFontColor = item.getmBtnFontColor();
            btnColor = item.getmBtnColor();
            grpBkColor = item.getmGrpBkgrdColor();
            grpFontColor = item.getmGrpFntColor();

        } else {
            btnFontColor = item.getSmBtnFontColor();
            btnColor = item.getSmBtnColor();
            grpBkColor = item.getsGrpBkgrdColor();
            grpFontColor = item.getsGrpFntColor();

        }

        // @Beena : Check is last element is only Group header
        if (position == mlist.size() - 1) {
            if ("Text".equalsIgnoreCase(linkTypeName)
                    || "Label".equalsIgnoreCase(linkTypeName)) {
                mViewHolder.textViewLayout.setVisibility(View.GONE);
            }
        }
        // @Beena : Check if next element after group is node or Group
        if ("Text".equalsIgnoreCase(linkTypeName)
                || "Label".equalsIgnoreCase(linkTypeName)) {
            if (!"Text".equalsIgnoreCase(nextLinkTypeName)
                    || "Label".equalsIgnoreCase(nextLinkTypeName)) {

                mViewHolder.disclosureIcon.setVisibility(View.GONE);
                if (null != grpBkColor && !"N/A".equals(grpBkColor)) {
                    mViewHolder.textViewLayout.setBackgroundColor(Color
                            .parseColor(grpBkColor));
                } else {
                    mViewHolder.textViewLayout.setBackgroundColor(Color
                            .parseColor("#1A4A6E"));
                }

                if (null != grpFontColor && !"N/A".equals(grpFontColor)) {
                    mViewHolder.text.setTextColor(Color
                            .parseColor(grpFontColor));
                } else {
                    mViewHolder.text.setTextColor(Color.WHITE);
                }

                mViewHolder.textViewLayout
                        .setLayoutParams(new LinearLayout.LayoutParams(
                                LayoutParams.MATCH_PARENT, 50));
                mViewHolder.text.setVisibility(View.VISIBLE);
                mViewHolder.text.setText(itemName);
                mViewHolder.text.setTypeface(null, Typeface.BOLD);
            } else {
                mViewHolder.textViewLayout.setVisibility(View.GONE);
            }

        } else {

            mViewHolder.text.setText(itemName);

            if (btnFontColor != null && !btnFontColor.equals("N/A")) {
                mViewHolder.text.setTextColor(Color.parseColor(btnFontColor));
            } else {
                mViewHolder.text.setTextColor(Color.WHITE);
            }

            if (btnColor != null && !btnColor.equals("N/A")) {
                mViewHolder.menuListTemplateParent.setBackgroundColor(Color
                        .parseColor(btnColor));

            } else {
                mViewHolder.menuListTemplateParent
                        .setBackgroundColor(Color.BLACK);

            }

        }

        return convertView;

    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}