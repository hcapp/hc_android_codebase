package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.HashMap;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class EventHotelDetailActivity extends CustomTitleBar {

	ProgressDialog progDialog;
	String responseText = "No Response from server";
	private static String returnValue = "sucessfull";
	String eventId;
	String retListId;
	String retailLocationId;

	HashMap<String, String> hotelDetails;
	ImageView mImageView;
	TextView mHotelName;
	TextView mHotelrating;
	TextView mHotelAddress;
	TextView mHotelDescription;

	LinearLayout mLinearLayout;
	HotelDetailAsyncTask hotelAsyncTask;
	boolean isFirst;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.events_hotel_detail);

			isFirst = true;

			eventId = getIntent().getStringExtra("eventId");
			retListId = getIntent().getStringExtra("retListId");
			retailLocationId = getIntent().getStringExtra("retailLocationId");
			callHotelAsyncTask();

			title.setText("Details");

			backImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTask();
                    finish();

                }
            });

			rightImage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if (GlobalConstants.isFromNewsTemplate){
						Intent intent = null;
						switch (GlobalConstants.className) {
							case Constants.COMBINATION:
								intent = new Intent(EventHotelDetailActivity.this, CombinationTemplate.class);
								break;
							case Constants.SCROLLING:
								intent = new Intent(EventHotelDetailActivity.this, ScrollingPageActivity.class);
								break;
							case Constants.NEWS_TILE:
								intent = new Intent(EventHotelDetailActivity.this, TwoTileNewsTemplateActivity.class);
								break;
						}
						if (intent != null) {
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
						}
					}else {
						setResult(Constants.FINISHVALUE);
						cancelAsyncTask();
						startMenuActivity();
					}
				}
			});

			leftTitleImage.setVisibility(View.INVISIBLE);

			mLinearLayout = (LinearLayout) findViewById(R.id.events_hotel_detail_layout);
			mLinearLayout.setVisibility(View.INVISIBLE);

			mImageView = (ImageView) findViewById(R.id.events_hotel_detail_main_image);
			mHotelName = (TextView) findViewById(R.id.events_hotel_name);
			mHotelrating = (TextView) findViewById(R.id.events_hotel_rating);
			mHotelAddress = (TextView) findViewById(R.id.events_hotel_address);
			mHotelDescription = (TextView) findViewById(R.id.events_hotel_description);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public class HotelDetailAsyncTask extends AsyncTask<String, Void, String> {

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		protected void onPreExecute() {
			progDialog = new ProgressDialog(EventHotelDetailActivity.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage(Constants.DIALOG_MESSAGE);
			progDialog.setCancelable(false);
			progDialog.show();

		}

		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				if ("sucessfull".equals(returnValue)) {

                    mLinearLayout.setVisibility(View.VISIBLE);

                    mHotelName.setText(hotelDetails.get("retailName"));
                    mHotelrating.setText("Rating: "
                            + hotelDetails.get("rating"));
                    mHotelAddress.setText("Address: "
                            + hotelDetails.get("address"));
                    mHotelDescription.setText(hotelDetails.get("description"));

                    if (hotelDetails.get("imagePath") != null) {
                        new DownloadImageTask(mImageView).execute(hotelDetails
                                .get("imagePath"));
                    }
                } else {
                    progDialog.dismiss();
                    Toast.makeText(EventHotelDetailActivity.this, responseText,
                            Toast.LENGTH_SHORT).show();
                }

				mLinearLayout.setVisibility(View.VISIBLE);

				if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... arg0) {

			try {
				String url_events_hotel_detail = Properties.url_local_server
						+ Properties.hubciti_version
						+ "alertevent/fetcheventhoteldetail";
				String urlParameters = mUrlRequestParams
						.createHotelDetailParameter(eventId, retListId,
								retailLocationId);
				JSONObject jsonResponse = mServerConnections
						.getUrlPostResponse(url_events_hotel_detail,
								urlParameters, true);

				try {
					if (jsonResponse != null) {

						if (jsonResponse.has("EventDetails")) {
							JSONObject elem = jsonResponse
									.getJSONObject("EventDetails");
							hotelDetails = new HashMap<>();
							if (elem != null) {
								if (elem.has("retailName")) {
									hotelDetails.put("retailName",
											elem.get("retailName").toString());
								}
								if (elem.has("price")) {
									hotelDetails.put("price", elem.get("price")
											.toString());
								}
								if (elem.has("address")) {
									hotelDetails.put("address",
											elem.get("address").toString());
								}
								if (elem.has("rowNum")) {
									hotelDetails.put("rowNum",
											elem.get("rowNum").toString());
								}
								if (elem.has("retailLocationId")) {
									hotelDetails
											.put("retailLocationId",
													elem.get("retailLocationId")
															.toString());
								}
								if (elem.has("rating")) {
									hotelDetails.put("rating",
											elem.get("rating").toString());
								}
								if (elem.has("roomCheckUrl")) {
									hotelDetails
											.put("roomCheckUrl",
													elem.get("roomCheckUrl")
															.toString());
								}
								if (elem.has("description")) {
									hotelDetails.put("description",
											elem.get("description").toString());
								}
								if (elem.has("roomBookUrl")) {
									hotelDetails.put("roomBookUrl",
											elem.get("roomBookUrl").toString());
								}
								if (elem.has("packagePrice")) {
									hotelDetails
											.put("packagePrice",
													elem.get("packagePrice")
															.toString());
								}
								if (elem.has("imagePath")) {
									hotelDetails.put("imagePath",
											elem.get("imagePath").toString());
								}

							}

						}

						else {

							JSONObject jsonRecordNotFound = jsonResponse
									.getJSONObject("response");
							responseText = jsonRecordNotFound
									.getString("responseText");
							returnValue = "UNSUCCESS";
							return returnValue;

						}

					}
				} catch (Exception ex) {
					ex.printStackTrace();
					returnValue = "UNSUCCESS";
					return returnValue;
				}

				return null;

			} catch (Exception ex) {
				ex.printStackTrace();

			}

			return null;
		}
	}

	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		private RotateAnimation anim;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
			anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF,
					0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setRepeatCount(Animation.INFINITE);
			anim.setDuration(700);
			this.bmImage.setAnimation(null);
			this.bmImage.setImageResource(R.drawable.loading_button);
			this.bmImage.startAnimation(anim);
			this.bmImage.setAnimation(anim);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0].replaceAll(" ", "%20");
			Bitmap mIcon11 = null;
			try {

				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			bmImage.setAnimation(null);
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			cancelAsyncTask();
			finish();
		}
	}

	private void callHotelAsyncTask() {
		if (isFirst) {
			hotelAsyncTask = new HotelDetailAsyncTask();
			hotelAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;
		}

		if (hotelAsyncTask != null) {
			if (!hotelAsyncTask.isCancelled()) {
				hotelAsyncTask.cancel(true);

			}

			hotelAsyncTask = null;

			hotelAsyncTask = new HotelDetailAsyncTask();
			hotelAsyncTask.execute();
		}

	}

	private void cancelAsyncTask() {
		if (hotelAsyncTask != null && !hotelAsyncTask.isCancelled()) {
			hotelAsyncTask.cancel(true);
		}

		hotelAsyncTask = null;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cancelAsyncTask();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		cancelAsyncTask();
	}

}
