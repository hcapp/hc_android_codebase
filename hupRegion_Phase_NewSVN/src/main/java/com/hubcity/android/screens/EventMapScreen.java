package com.hubcity.android.screens;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.view.View;

import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;

import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CommonMethods;

/**
 * Created by subramanya.v on 4/23/2016.
 */

@SuppressWarnings("DefaultFileTemplate")
public class EventMapScreen extends CustomTitleBar implements
        View.OnClickListener, GoogleMap.OnInfoWindowClickListener, LocationSource,
        LocationSource.OnLocationChangedListener {


    private GoogleMap mMap;
    private String eventTypeId;
    private double latitude, longitude;
    private OnLocationChangedListener mListener;
    private boolean isEvent, isPlayingToday;
    private ListView retailerList;
    private View moreResultsView;
    private RetailerAdpater retailerObject;
    private FrameLayout mapParent;
    private RelativeLayout retailerMap;
    private int screenHeight;
    private boolean isSlideUp = true;
    private ImageView zoom;
    private ShowMapAsyTask eventsAsyncTask;
    private LocationManager locationManager;
    private int maxCount = 0;
    private int nextPage;
    private int maxRowNum = 0;
    private ArrayList<EventDetailObj> retailerData = new ArrayList<>();


    private int lowerLimit = 0;
    private int bandId;
    private String radius;
    private boolean BandSummary = false;
    private boolean isPaginationEnd = true;
    private String postalCode;
    private boolean isFirst = true;
    private String mItemId, cityIds;
    private boolean isRefresh;
    private HubCityContext mHubCiti;
    private HashMap<String, String> resultSet;
    public boolean isLoaded = false;
    private LinearLayout bottomLayout;
    private BottomButtons bb;
    private CustomNavigation customNaviagation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.retailer_map_test);
            CommonConstants.hamburgerIsFirst = true;

            title.setSingleLine(false);

            title.setMaxLines(2);

            isRefresh = true;
            mHubCiti = (HubCityContext) getApplicationContext();
            mHubCiti.setCancelled(false);

            //Clearing filter cached values and sending resultset as null to send fresh request every
            // time
            HubCityContext mHubCity = (HubCityContext) getApplicationContext();
            mHubCity.clearArrayAndAllValues(false);
            HubCityContext.isDoneClicked = false;

            // Initiating Bottom button class
            try {
                //noinspection ConstantConditions
                bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
                bb.setActivityInfo(this, "BandEvents");
            } catch (Exception e) {
                e.printStackTrace();
            }
//        creating comma separated string for user preferred cities
//            cityIds = CommonConstants.getSortedCityIds(new CityDataHelper().getCitiesList(this));
            getIntentData();
            getGPSValues();
            setVisibility();
            bindView();
            setClickListener();
            radius = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).getString
                    ("radious", "");
            ViewTreeObserver observer = retailerMap.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {

                    screenHeight = retailerMap.getMeasuredHeight();

                    RelativeLayout.LayoutParams mapParentParms = new RelativeLayout.LayoutParams
                            (ViewGroup
                                    .LayoutParams.MATCH_PARENT, screenHeight / 4);
                    mapParent.setLayoutParams(mapParentParms);

                    ViewTreeObserver obs = retailerMap.getViewTreeObserver();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }

                    initializeRetailerList();

                }

            });

            int checkGooglePlayServices = GooglePlayServicesUtil.isGooglePlayServicesAvailable
                    (this);
            if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
                // google play services is missing!!!!
                /* Returns status code indicating whether there was an error.�
����Can be one of following in ConnectionResult: SUCCESS, SERVICE_MISSING,
SERVICE_VERSION_UPDATE_REQUIRED, SERVICE_DISABLED, SERVICE_INVALID.
����*/
                GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices, this, 1122).show();
            }
//user for hamburger in modules
            customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setVisibility() {
        divider.setVisibility(View.GONE);
        leftTitleImage.setVisibility(View.GONE);
        drawerIcon.setVisibility(View.VISIBLE);
    }

    private void setClickListener() {
        //noinspection Convert2Lambda
        retailerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (!retailerData.get(position).isHeader()) {
                    Intent bandScreen = new Intent(EventMapScreen.this, BandEventDetail
                            .class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("EventDetails", retailerData.get(position));
                    bandScreen.putExtras(bundle);
                    startActivity(bandScreen);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);

        }

        if (resultCode == 30001 || resultCode == 2) {
            isRefresh = false;

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRefresh = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }

        if (null != bb) {
            bb.setActivityInfo(this, "BandEvents");
        }
        if (bb != null) {
            bb.setOptionsValues(getListValues(), null);
        }
        if (mHubCiti.isCancelled()) {
            return;
        }
        if (!isRefresh) {
            resultSet = mHubCiti.getFilterValues();
            lowerLimit = 0;
            isPaginationEnd = true;
            retailerData = null;
            mMap = null;
            isFirst = true;
            retailerData = new ArrayList<>();
            eventsAsyncTask = new ShowMapAsyTask();
            eventsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }
        isRefresh = false;
    }

    private void bindView() {
        bottomLayout = (LinearLayout) findViewById(R.id.bottom_bar);
        bottomLayout.setVisibility(View.INVISIBLE);
        retailerMap = (RelativeLayout) findViewById(R.id.retailer_map);
        zoom = (ImageView) findViewById(R.id.zoom);
        mapParent = (FrameLayout) findViewById(R.id.map_parent);
        zoom.setImageResource(R.drawable.full_screen);
        zoom.setOnClickListener(this);

        moreResultsView = getLayoutInflater().inflate(
                R.layout.pagination, retailerList, true);
        retailerList = (ListView) findViewById(R.id.retailer_list);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


    }

    private void getIntentData() {
        if (getIntent().hasExtra("isPlayingToday")) {
            if (getIntent().getExtras().getBoolean("isPlayingToday")) {
                isPlayingToday = true;
            }
        }
        if (getIntent().hasExtra("Events")) {
            if (getIntent().getExtras().getBoolean("Events")) {
                isEvent = true;
            }
        }
        try {
            radius = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0)
                    .getString
                            ("radious", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (getIntent().hasExtra("BandSummary")) {
            BandSummary = getIntent().getExtras().getBoolean("BandSummary");

        }
        if (getIntent().hasExtra("mItemId")) {
            mItemId = getIntent().getExtras().getString("mItemId");

        }

        title.setText(R.string.shows);

        if (getIntent().hasExtra("EventTypeId")) {
            eventTypeId = getIntent().getExtras().getString("EventTypeId");
        }
        if (getIntent().hasExtra("bandId")) {
            bandId = Integer.valueOf(getIntent().getExtras().getString("bandId"));
        }
        postalCode = Constants.getZipCode();

        // google props
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }


    private void initializeRetailerList() {
        eventsAsyncTask = new ShowMapAsyTask();
        eventsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public class ShowMapAsyTask extends AsyncTask<String, Void, String> {
        final UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        final ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog mDialog;
        private boolean hasBottomButtons;
        ArrayList<EventDetailObj> retailerChild = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            if (isFirst) {
                mDialog = ProgressDialog.show(EventMapScreen.this, "",
                        Constants.DIALOG_MESSAGE, true);
                mDialog.setCancelable(false);
            }


        }

        @Override
        protected String doInBackground(String... params) {

            String url_events = Properties.url_local_server
                    + Properties.hubciti_version + "band/eventlist";
            String Result = null;
            String sortColumn;
            String sortOrder;
            String eventDate;
            sortColumn = "";
            eventDate = "";
            sortOrder = "ASC";
            String catIds = null;
            if (isPlayingToday) {
                eventDate = CommonMethods.getCurrentDate();
                sortColumn = "mileage";
            }
            cityIds = "";
            if (resultSet != null && !resultSet.isEmpty()) {
                if (resultSet.containsKey("savedSubCatIds")) {
                    catIds = resultSet.get("savedSubCatIds");
                }
                if (resultSet.containsKey("SortBy")) {
                    sortColumn = resultSet.get("SortBy");
                }
                if (resultSet.containsKey("EventDate")) {
                    eventDate = resultSet.get("EventDate");
                }
                if (resultSet.containsKey("savedCityIds")) {
                    if (resultSet.get("savedCityIds") != null && !resultSet.get("savedCityIds").equals("")) {
                        cityIds = resultSet.get("savedCityIds");
                        cityIds = cityIds.replace("All,", "");
                    }
                }
            }

            try {

                JSONObject urlParameters = mUrlRequestParams
                        .createShowMapParameter(lowerLimit, eventTypeId, latitude, longitude,
                                postalCode, bandId,
                                radius, sortColumn, sortOrder, catIds, eventDate, cityIds, mItemId);

                JSONObject jsonResponse = mServerConnections
                        .getUrlJsonPostResponse(url_events, urlParameters);
                if (jsonResponse != null) {
                    if (jsonResponse.getString("responseText").equalsIgnoreCase("Success")) {

                        if (jsonResponse.has("maxCount")) {
                            maxCount = jsonResponse.getInt("maxCount");
                        }
                        if (jsonResponse.has("nextPage")) {
                            nextPage = jsonResponse.getInt("nextPage");
                        }
                        if (jsonResponse.has("maxRowNum")) {
                            maxRowNum = jsonResponse.getInt("maxRowNum");
                        }


                        if (jsonResponse.has("eventGroupList")) {
                            JSONArray eventGroupArray = jsonResponse
                                    .getJSONArray("eventGroupList");
                            for (int group = 0;
                                 group < eventGroupArray.length();
                                 group++) {
                                JSONObject groupElem = eventGroupArray.getJSONObject(group);
                                String groupContent = groupElem.optString("groupContent");
                                if (groupElem.has("eventList")) {
                                    JSONArray eventArray = groupElem
                                            .getJSONArray("eventList");

                                    if (!isPlayingToday) {
                                        retailerChild = addEventHeader(groupContent, retailerChild);
                                    }
                                    for (int eventCount = 0; eventCount < eventArray.length(); eventCount++) {
                                        retailerChild = addEventList(eventArray, eventCount, groupContent, retailerChild);
                                    }
                                    Result = "Success";

                                }
                            }
                        }
                        isLoaded = false;

                        if (jsonResponse.has("bottomBtnList")) {
                            try {
                                if(bb != null) {
                                    ArrayList<BottomButtonBO> bottomButtonList = bb
                                            .parseForBottomButton(jsonResponse);
                                    BottomButtonListSingleton
                                            .clearBottomButtonListSingleton();
                                    BottomButtonListSingleton
                                            .getListBottomButton(bottomButtonList);
                                    hasBottomButtons = true;
                                }
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    } else {
                        if (jsonResponse.has("responseText")) {
                            Result = jsonResponse.getString("responseText");
                        }
                    }
                } else {
                    Result = "Technical problem";

                }
            } catch (PackageManager.NameNotFoundException | JSONException e) {
                e.printStackTrace();
            }


            return Result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if (result.equalsIgnoreCase("Success")) {
                    reaArrangeList(retailerChild);
                    if (hasBottomButtons && isFirst) {
                        bb.createbottomButtontTab(bottomLayout, false);
                        bottomLayout.setVisibility(View.VISIBLE);
                    }
                    if (bb != null) {
                        bb.setOptionsValues(getListValues(), null);
                    }

                    if ((nextPage == 0) && (maxRowNum == maxCount)) {
                        if (isPaginationEnd) {
                            retailerObject = new RetailerAdpater(EventMapScreen
                                    .this, retailerData, maxRowNum, maxCount, BandSummary);
                            retailerList.setAdapter(retailerObject);

                            isPaginationEnd = false;
                        } else {
                            retailerObject.updateList(retailerData, maxRowNum, maxCount);
                            retailerObject.notifyDataSetChanged();
                        }
                        retailerList.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);

                    } else if ((nextPage == 1) && (maxRowNum < maxCount)) {

                        if (isPaginationEnd) {
                            retailerObject = new RetailerAdpater(EventMapScreen
                                    .this, retailerData, maxRowNum, maxCount, BandSummary);
                            retailerList.setAdapter(retailerObject);
                            isPaginationEnd = false;
                        } else {
                            retailerObject.updateList(retailerData, maxRowNum, maxCount);
                            retailerObject.notifyDataSetChanged();
                        }

                        retailerList.removeFooterView(moreResultsView);
                        retailerList.addFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.VISIBLE);
                    } else {
                        retailerObject = new RetailerAdpater(EventMapScreen
                                .this, retailerData, maxRowNum, maxCount, BandSummary);
                        retailerList.setAdapter(retailerObject);

                        retailerList.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);
                    }
                    isFirst = false;


                } else if (result.equalsIgnoreCase("No Records Found.")) {

                    retailerObject = new RetailerAdpater(EventMapScreen
                            .this, retailerData, maxRowNum, maxCount, BandSummary);
                    retailerList.setAdapter(retailerObject);
                    retailerList.removeFooterView(moreResultsView);
                    moreResultsView.setVisibility(View.GONE);
                    Toast.makeText(EventMapScreen.this, getString(R.string.norecord), Toast
                            .LENGTH_LONG).show();

                } else {
                    retailerObject = new RetailerAdpater(EventMapScreen
                            .this, retailerData, maxRowNum, maxCount, BandSummary);
                    retailerList.setAdapter(retailerObject);
                    retailerList.removeFooterView(moreResultsView);
                    moreResultsView.setVisibility(View.GONE);
                    Toast.makeText(EventMapScreen.this, "Technical problem", Toast.LENGTH_LONG).show();
                }

                //to minimize the view for half of the screen map and listview
                if (!isSlideUp) {
                    slideDown();
                }
                initilizeMap();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void reaArrangeList(ArrayList<EventDetailObj> retailerData) {
        if (this.retailerData.size() > 0) {
            int i = 0;
            int mRetailerSize = this.retailerData.size();
            while (retailerData.size() != 0) {
                for (int mEvent = 0; mEvent < this.retailerData.size(); mEvent++) {
                    String sGroupContent = retailerData.get(0).getGroupContent();
                    boolean isHeader = retailerData.get(0).isHeader();
                    String mGroupContent = this.retailerData.get(mEvent).getGroupContent();
                    if (sGroupContent.equalsIgnoreCase(mGroupContent)) {
                        if (!isHeader) {
                            int lastCount = 0;
                            for (int count = 0; count < this.retailerData.size(); count++) {
                                String mGroupHeader = this.retailerData.get(count).getGroupContent();
                                if (sGroupContent.equalsIgnoreCase(mGroupHeader)) {
                                    lastCount = count;
                                }
                            }
                            int insertPos = lastCount + 1;
                            this.retailerData.add(insertPos, retailerData.get(0));
                            retailerData.remove(0);
                            mRetailerSize++;
                            break;
                        } else {
                            retailerData.remove(0);
                            break;
                        }
                    }

                }
                i++;
                if (i == mRetailerSize) {
                    if (mRetailerSize != 0) {
                        this.retailerData.addAll(retailerData);
                        //noinspection CollectionAddedToSelf
                        retailerData.removeAll(retailerData);
                    }
                }
            }
        } else {
            this.retailerData.addAll(retailerData);
        }
    }


    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();
        String className = "BandEvents";
        values.put("mItemId", mItemId);
        if (radius != null && !radius.equals(""))
            values.put("radius", radius);
        else
            values.put("radius", "50");
        values.put("latitude", String.valueOf(latitude));

        values.put("longitude", String.valueOf(longitude));
        values.put("moduleName", "BandEvents");
        values.put("Class", className);
        values.put("BandID", String.valueOf(bandId));
        values.put("BandEventTypeID", eventTypeId);
        return values;
    }

    private ArrayList<EventDetailObj> addEventHeader(String groupContent, ArrayList<EventDetailObj> retailerData) {
        EventDetailObj eventList = new EventDetailObj();
        eventList.setIsHeader(true);
        eventList.setGroupContent(groupContent);
        retailerData.add(eventList);
        return retailerData;
    }

    private ArrayList<EventDetailObj> addEventList(JSONArray eventArray, int eventCount, String groupContent, ArrayList<EventDetailObj> retailerData) {
        JSONObject elems;
        try {
            elems = eventArray
                    .getJSONObject(eventCount);
            EventDetailObj eventList = new EventDetailObj();
            eventList.setIsHeader(false);
            eventList.setGroupContent(groupContent);
            eventList.setBandCntFlag(elems.optInt("bandCntFlag"));
            eventList.setPopUpMsg(elems.optString("popUpMsg"));
            eventList.setTicketUrl(elems.optString("ticketUrl"));
            eventList.setEventId(elems.optString("eventId"));
            eventList.setBandIds(elems.optString("bandIds"));

            eventList.setEventName(elems.optString("eventName"));
            eventList.setEventCatName(elems.optString("eventCatName"));
            eventList.setRecurringDays(elems.optString("recurringDays"));
            eventList.setDistance(elems.optString("distance"));
            eventList.setLatitude(elems.optString("latitude"));
            eventList.setLongitude(elems.optString("longitude"));
            eventList.setAddress(elems.optString("address"));

            eventList.setBandName(elems.optString("bandName"));
            eventList.setShortDes(elems.optString("shortDes"));
            eventList.setListingImgPath(elems.optString("listingImgPath"));
            eventList.setImagePath(elems.optString("imgPath"));
            eventList.setStartTime(elems.optString("startTime"));
            eventList.setLongDes(elems.optString("longDes"));
            eventList.setMoreInfoURL(elems.optString("moreInfoURL"));

            if (elems.has("startDate")) {
                String startDate = elems.optString("startDate");
                String[] dateFormat = startDate.split("-", 3);
                String month = getMonthString(dateFormat[0]);
                startDate = dateFormat[1] + " " + month;
                eventList.setStartDate(startDate);
            }

            eventList.setIsAppSiteFlag(elems.optInt("isAppSiteFlag"));
            eventList.setRetailLocationId(elems.optString
                    ("retailLocationId"));
            eventList.setRetailId(elems.optString("retailId"));
            eventList.setRetailName(elems.optString("retailName"));
            eventList.setEvtLocTitle(elems.optString("evtLocTitle"));

            retailerData.add(eventList);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return retailerData;
    }

    private String getMonthString(String month) {

        switch (month) {
            case "01":
                return "Jan";
            case "02":
                return "Feb";
            case "03":
                return "Mar";
            case "04":
                return "Apr";
            case "05":
                return "May";
            case "06":
                return "Jun";
            case "07":
                return "Jul";
            case "08":
                return "Aug";
            case "09":
                return "Sep";
            case "10":
                return "Oct";
            case "11":
                return "Nov";
            case "12":
                return "Dec";

            default:
                return "1";
        }

    }

    @SuppressWarnings("deprecation")
    private void getGPSValues() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                latitude = locNew.getLatitude();
                longitude = locNew.getLongitude();

            } else {
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    latitude = locNew.getLatitude();
                    longitude = locNew.getLongitude();
                }
            }
        }

    }

    public void paginationRetailerList(int maxRowNum) {
        isLoaded = true;
        this.maxRowNum = maxRowNum;
        lowerLimit = this.maxRowNum;
        mMap = null;
        eventsAsyncTask = new ShowMapAsyTask();
        eventsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    /**
     * function to load map. If map is not created it will create it for you
     */
    private void initilizeMap() {

        if (mMap == null) {


            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            mMap.clear();
            mMap.setOnInfoWindowClickListener(this);

            Double mapLng;
            Double mapLat;
            if (isEvent) {
                mapLat = Double.valueOf(getIntent().getExtras().getString(
                        "latitude"));
                mapLng = Double.valueOf(getIntent().getExtras().getString(
                        "longitude"));

                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(mapLat, mapLng)).title(
                        getIntent().getExtras().getString("address"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        mapLat, mapLng), 14));
                mMap.addMarker(marker);



                if (mMap == null) {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                            .show();
                }

            } else {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                LatLngBounds bounds;
                if (null != retailerData && !retailerData.isEmpty()) {

                    for (int i = 0; i < retailerData.size(); i++) {
                        if (!retailerData.get(i).isHeader()) {
                            String retAddress = "";
                            if (retailerData.get(i).getAddress() != null
                                    && !"".equals(retailerData.get(i)
                                    .getAddress())) {
                                retAddress += retailerData.get(i)
                                        .getAddress();
                            } else {
                                retAddress = "";
                            }

                            if (null != retailerData.get(i)) {

                                // create marker

                                if (retailerData.get(i).getLatitude() != null
                                        && retailerData.get(i).getLongitude() != null &&
                                        !retailerData.get(i).getLongitude().equalsIgnoreCase("")
                                        && !retailerData.get(i).getLatitude()
                                        .equalsIgnoreCase("")) {
                                    MarkerOptions marker;
                                    if (retailerData.get(i).getIsAppSiteFlag() == 1) {
                                        marker = new MarkerOptions()
                                                .position(
                                                        new LatLng(
                                                                Double.valueOf(retailerData
                                                                        .get(i)
                                                                        .getLatitude()),
                                                                Double.valueOf(retailerData
                                                                        .get(i)
                                                                        .getLongitude())))
                                                .title(retailerData.get(i)
                                                        .getEventName()).snippet(retAddress);

                                    } else {
                                        marker = new MarkerOptions()
                                                .position(
                                                        new LatLng(
                                                                Double.valueOf(retailerData
                                                                        .get(i)
                                                                        .getLatitude()),
                                                                Double.valueOf(retailerData
                                                                        .get(i)
                                                                        .getLongitude())))
                                                .title(retailerData.get(i)
                                                        .getEventName()).snippet(retAddress);
                                    }

                                    retailerData.get(i).setMapMarkerId(
                                            mMap.addMarker(marker).getId());


                                    builder.include(marker.getPosition());
                                }

                            }

                        }
                    }
                    bounds = builder.build();
                    int padding = 100; // offset from edges of the map in pixels

                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    mMap.moveCamera(cu);
                }


                // check if map is created successfully or not
                if (mMap == null) {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! unable to create maps",
                            Toast.LENGTH_SHORT).show();
                }
            }


        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        for (int i = 0; i < retailerData.size(); i++) {
            if (!retailerData.get(i).isHeader()) {
                if (retailerData.get(i) != null
                        && marker.getId().equals(
                        retailerData.get(i).getMapMarkerId())) {
                    if (retailerData.get(i).getIsAppSiteFlag() == 1) {
                        Intent findlocationInfo = new Intent(
                                EventMapScreen.this,
                                RetailerActivity.class);
                        findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
                                retailerData.get(i).getRetailId());
                        findlocationInfo.putExtra(
                                CommonConstants.TAG_RETAILE_LOCATIONID,
                                retailerData.get(i).getRetailLocationId());

                        findlocationInfo.putExtra(
                                CommonConstants.TAG_RETAILER_NAME, retailerData
                                        .get(i).getRetailName());
                        findlocationInfo.putExtra(CommonConstants.TAG_DISTANCE,
                                retailerData.get(i).getDistance());
                        startActivityForResult(findlocationInfo,
                                Constants.STARTVALUE);
                    }
                }

            }
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        mListener.onLocationChanged(location);

    }


    @Override
    public void activate(OnLocationChangedListener listener) {
        mListener = listener;
    }

    @Override
    public void deactivate() {
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.right_button:

                finish();
                break;
            case R.id.zoom:

                if (isSlideUp) {
                    bottomLayout.setVisibility(View.GONE);
                    slideUp();
                } else {
                    bottomLayout.setVisibility(View.VISIBLE);
                    slideDown();
                }

            default:
                break;
        }

    }

    private void slideDown() {
        Animation anidelta;

        anidelta = new ResizeAnimation(mapParent, screenHeight,
                (screenHeight / 4), false);

        anidelta.setDuration(300/* animation time */);
        anidelta.setFillAfter(true);
        mapParent.startAnimation(anidelta);
        isSlideUp = true;
        zoom.setImageResource(R.drawable.full_screen);

    }

    private void slideUp() {

        Animation anidelta;

        anidelta = new ResizeAnimation(mapParent, (screenHeight / 4),
                screenHeight, true);

        anidelta.setDuration(300/* animation time */);
        anidelta.setFillAfter(true);
        mapParent.startAnimation(anidelta);
        isSlideUp = false;
        zoom.setImageResource(R.drawable.small_screen);
    }

    public class ResizeAnimation extends Animation {
        private final View mView;
        private final float mToHeight;
        private final float mFromHeight;
        private final boolean mIsSlideUp;


        public ResizeAnimation(FrameLayout frameLayout, float fromHeight, float toHeight, boolean
                isSlideUp) {
            mToHeight = toHeight;
            mFromHeight = fromHeight;
            mView = frameLayout;
            mIsSlideUp = isSlideUp;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float height;
            if (mIsSlideUp) {
                height =
                        (mToHeight - mFromHeight) * interpolatedTime + mFromHeight;
            } else {
                height =
                        mFromHeight - (mFromHeight - mToHeight) * interpolatedTime;
            }
            ViewGroup.LayoutParams p = mView.getLayoutParams();
            p.height = (int) height;
            mView.requestLayout();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        HubCityContext mHubCity = (HubCityContext) getApplicationContext();
        mHubCity.clearArrayAndAllValues(false);
        HubCityContext.isDoneClicked = false;
    }

}
