package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scansee.hubregion.R;

public class OptionsListAdapter extends BaseAdapter {

    Activity activity;
    private ArrayList<String> thislocationList;
    private static LayoutInflater inflater = null;

    public OptionsListAdapter(Activity activity,
                              ArrayList<String> thislocationList) {
        this.activity = activity;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.thislocationList = thislocationList;
    }

    @Override
    public int getCount() {
        return thislocationList.size();
    }

    @Override
    public Object getItem(int id) {
        return thislocationList.get(id);
    }

    @Override
    public long getItemId(int id) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = inflater.inflate(R.layout.listitem_optionlist, parent, false);
            holder = new ViewHolder();
            holder.retailerName = (TextView) view
                    .findViewById(R.id.retailer_name);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.retailerName.setText(thislocationList.get(position));

        return view;
    }

    public static class ViewHolder {
        private TextView retailerName;
    }
}
