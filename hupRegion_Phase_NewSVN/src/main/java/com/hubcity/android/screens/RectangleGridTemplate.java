package com.hubcity.android.screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.ArrayList;

public class RectangleGridTemplate extends MenuPropertiesActivity implements
        OnItemClickListener {

    protected CustomImageLoader customImageLoader;
    public GridView gridview;
    private RectangleViewAdapter rectangleViewAdapter;
    private ArrayList<MainMenuBO> mainMenuUnsortedList;
    private String mBannerImg;
    private ImageView mBannerImageView;
    protected int latitude;
    protected int longitude;
    protected static int reqWidth;
    protected static int reqHeight;
    private LinearLayout gridViewParent;

    private LinearLayout parentIntermideate;
    private ProgressBar progressBar;
    private RelativeLayout bannerHolder;
    private Context mContext;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;
    private View menuListTemplate;

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        super.onNewIntent(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mContext = RectangleGridTemplate.this;
            CommonConstants.hamburgerIsFirst = true;
            Intent intent = getIntent();
            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);


            parent = new RelativeLayout(this);
            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            parent.setLayoutParams(param);

            parentIntermideate = new LinearLayout(this);
            parentIntermideate.setOrientation(LinearLayout.VERTICAL);

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            menuListTemplate = layoutInflater.inflate(R.layout.square_menu_layout,
                   null, false);

// Initializing drawer layout
            drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);

            mBannerImageView = (ImageView) menuListTemplate
                    .findViewById(R.id.top_image);
            mBannerImageView.setScaleType(ScaleType.FIT_XY);
            progressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
            bannerHolder = (RelativeLayout) menuListTemplate.findViewById(R.id.banner_holder);
            bannerParent = (LinearLayout) menuListTemplate.findViewById(R.id.bannerParent);
            gridViewParent = (LinearLayout) menuListTemplate
                    .findViewById(R.id.gridView_parent);

            gridview = (GridView) gridViewParent.findViewById(R.id.gridView);
            gridview.setOnItemClickListener(this);
            LinearLayout.LayoutParams gridViewParentParam = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            BottomButtonListSingleton mBottomButtonListSingleton;
            mBottomButtonListSingleton = BottomButtonListSingleton
                    .getListBottomButton();

            if (mBottomButtonListSingleton != null) {

                ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton.listBottomButton;

                if (listBottomButton != null && listBottomButton.size() > 1) {
                    int px = (int) (40 * this.getResources().getDisplayMetrics().density + 0.5f);
                    gridViewParentParam.setMargins(0, 0, 0, px);
                }
            }

            gridViewParent.setLayoutParams(gridViewParentParam);
            LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            parentIntermideate.setLayoutParams(parentParam);
            addTitleBar(parentIntermideate, parent);
            parentIntermideate.addView(gridViewParent);
            parent.addView(parentIntermideate);

            createbottomButtontTab(parent);
            setContentView(parent);

            handleIntent(intent);

            if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
                HubCityContext.level = 0;
            } else {
                HubCityContext.level = HubCityContext.level + 1;
            }
            setDataAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void backgroundColorSetting() {

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {

            if ((templateBgColor != null)
                    && !("N/A".equalsIgnoreCase(templateBgColor))) {
                gridViewParent.setBackgroundColor(Color
                        .parseColor(templateBgColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(templateBgColor));
                parent.setBackgroundColor(Color.parseColor(templateBgColor));
            } else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
                new ImageLoaderAsync1(gridViewParent).execute(mBkgrdImage);

            }
        } else {
            if ((templateBgColor != null)
                    && !("N/A".equalsIgnoreCase(templateBgColor))) {
                gridViewParent.setBackgroundColor(Color
                        .parseColor(templateBgColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(templateBgColor));
                parent.setBackgroundColor(Color.parseColor(templateBgColor));
            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {
                new ImageLoaderAsync1(gridViewParent).execute(smBkgrdImage);

            }
        }
        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if ((mBannerImg != null) && !("N/A".equalsIgnoreCase(mBannerImg))) {
                progressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(RectangleGridTemplate.this).load(mBannerImg).into(mBannerImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        } else {
            customHorizontalTicker = (CustomHorizontalTicker) menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker) menuListTemplate.findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher) menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext, customHorizontalTicker, customVerticalTicker, bannerParent, customRotate, bannerHolder);
        }


    }

    @Override
    protected void onPause() {
        gridViewParent.setBackgroundColor(Color.parseColor("#ffffff"));
        parentIntermideate.setBackgroundColor(Color.parseColor("#ffffff"));
        parent.setBackgroundColor(Color.parseColor("#ffffff"));
        super.onPause();
    }

    @Override
    protected void onResume() {

        handleIntent(getIntent());
        backgroundColorSetting();
        activity = this;
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }

    class ImageLoaderAsync1 extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync1(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {

            mDialog = ProgressDialog.show(RectangleGridTemplate.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            BitmapDrawable background = new BitmapDrawable(bmImg);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                bmImage.setBackgroundDrawable(background);
            } else {
                bmImage.setBackgroundDrawable(background);
            }

            try {
                if ((this.mDialog != null) && this.mDialog.isShowing()) {
                    this.mDialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                e.printStackTrace();
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                this.mDialog = null;
            }
        }
    }

    protected void handleIntent(Intent intent) {
        super.handleIntent(intent);
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);



            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
            templateBgColor = intent.getExtras().getString("templateBgColor");

            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("smBkgrdImage");
            departFlag = intent.getExtras().getString("departFlag");
            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
            mFontColor = intent.getExtras().getString("mFontColor");
            smFontColor = intent.getExtras().getString("smFontColor");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");

        }

    }

    int height;

    // Set the Data Adapter
    protected void setDataAdapter() {

        gridview.post(new Runnable() {

            @Override
            public void run() {
                String template = getIntent().getExtras().getString("rectangularTemp");

                height = gridview.getHeight();

                if ("4X4 Grid".equalsIgnoreCase(template)) {
                    height = height - (gridview.getHorizontalSpacing() * 3);
                } else {
                    height = height - (gridview.getHorizontalSpacing() * 4);
                }

                rectangleViewAdapter = new RectangleViewAdapter(
                        RectangleGridTemplate.this,
                        R.layout.rectangle_row_grid, mainMenuUnsortedList,
                        previousMenuLevel, mFontColor, smFontColor, height, template, gridview);
                gridview.setAdapter(rectangleViewAdapter);
            }
        });

    }

    private void onTemplateListMenuRowItemClick(final AdapterView<?> arg0,
                                                final View view, final int position, final long id) {
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);

        super.mItemName = mMainMenuBO.getmItemName();
        super.mItemId = mMainMenuBO.getmItemId();
        super.linkTypeId = mMainMenuBO.getLinkTypeId();
        super.linkTypeName = mMainMenuBO.getLinkTypeName();
        super.mMainMenuBOPosition = mMainMenuBO.getPosition();
        super.mItemImgUrl = mMainMenuBO.getmItemImgUrl();
        super.mLinkId = mMainMenuBO.getLinkId();
        super.mBtnColor = mMainMenuBO.getmBtnColor();
        super.mBtnFontColor = mMainMenuBO.getmBtnFontColor();
        super.smBtnColor = mMainMenuBO.getSmBtnColor();
        super.smBtnFontColor = mMainMenuBO.getSmBtnFontColor();
        linkClickListener(linkTypeName);
    }

    @Override
    public void onItemClick(final AdapterView<?> arg0, final View view,
                            final int position, final long id) {
        onTemplateListMenuRowItemClick(arg0, view, position, id);
    }

}
