package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.CouponClaimRedeemExpiredListAdapter1;
import com.scansee.android.HotDealClaimRedeemExpiredListAdapter;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

public class CurrentSpecialActivity extends CustomTitleBar {

	private HashMap<String, String> flagData = new HashMap<>();
	protected ListView specialoffersListView;
	ImageView retailerNameImageView;
	boolean saleFlag = false;
	String responseCode = null;

	String userId, retailLocationID, retailID,
			retailerAdURL, retailerAd, retailerName;

	String retListId = null, mainmenuId = null;

	SharedPreferences settings = null;
	CustomImageLoader customImageLoader;
	Context context = this;

	String mColor = "#FFFFFF";
	String mFontColor = "#000000";

	@Override
	protected void onResume() {
		super.onResume();

		List<ApplicationInfo> packages;
		PackageManager pm;
		pm = getPackageManager();
		// get a list of installed apps.
		packages = pm.getInstalledApplications(0);

		ActivityManager mActivityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);

		for (ApplicationInfo packageInfo : packages) {
			if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1
					|| packageInfo.packageName.contains("com.adobe.reader")) {
				continue;
			}

			mActivityManager.killBackgroundProcesses(packageInfo.packageName);
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.retailer_specials_list);

		try {
			title.setText("Specials");

			// get extras
			if (getIntent().hasExtra(Constants.TAG_RETAILE_LOCATIONID)) {
                retailLocationID = getIntent().getExtras().getString(
                        Constants.TAG_RETAILE_LOCATIONID);
            }
			if (getIntent().hasExtra(Constants.TAG_RETAILE_ID)) {
                retailID = getIntent().getExtras().getString(
                        Constants.TAG_RETAILE_ID);
            }
			if (getIntent().hasExtra(Constants.TAG_RIBBON_ADIMAGE_PATH)) {
                retailerAd = getIntent().getExtras().getString(
                        Constants.TAG_RIBBON_ADIMAGE_PATH);
            }
			if (getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAIL_LIST_ID) != null) {
                retListId = getIntent().getExtras().getString(
                        CommonConstants.TAG_RETAIL_LIST_ID);
            }

			if (getIntent().hasExtra(Constants.MAIN_MENU_ID)) {
                mainmenuId = getIntent().getExtras().getString(
                        Constants.MAIN_MENU_ID);
            }
			if (getIntent().hasExtra("saleFlag")) {
                saleFlag = getIntent().getExtras().getBoolean("saleFlag");
            }
			if (getIntent().hasExtra(CommonConstants.TAG_RIBBON_AD_URL)) {
                retailerAdURL = getIntent().getExtras().getString(
                        CommonConstants.TAG_RIBBON_AD_URL);
            }
			if (getIntent().hasExtra(Constants.TAG_RETAILER_NAME)) {
                retailerName = getIntent().getExtras().getString(
                        Constants.TAG_RETAILER_NAME);
            }

			retailerNameImageView = (ImageView) findViewById(R.id.retailer_specials_image);

			if (retailerAd != null) {
                retailerNameImageView.setTag(retailerAd);
                new CustomImageLoader(retailerNameImageView, CurrentSpecialActivity.this)
                        .setScaledImage(retailerAd);

            }

			if (retailerAd != null && "N/A".equalsIgnoreCase(retailerAd)) {
                retailerNameImageView.setVisibility(View.GONE);

            } else {
                retailerNameImageView.setVisibility(View.VISIBLE);
                retailerNameImageView
                        .setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								if (retailerAdURL != null
										&& !("N/A".equalsIgnoreCase(retailerAdURL))) {
									Intent retailerLink = new Intent(
											CurrentSpecialActivity.this,
											ScanseeBrowserActivity.class);
									retailerLink.putExtra(CommonConstants.URL,
											retailerAdURL);
									startActivity(retailerLink);
								}
							}
						});
            }

			specialoffersListView = (ListView) findViewById(R.id.listview_retailer_specials);
			specialoffersListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {
                    String itemName = specialoffersListView.getAdapter()
                            .getItem(position).toString();

                    navigatetoListActivity(itemName, false);

                }
            });
			new GetFlag().execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void navigatetoListActivity(String itemName, boolean isSingleItem) {

		Log.e("itemNAME", itemName + "");
		if (itemName.equalsIgnoreCase("FundRaisers")) {
			Intent intent = new Intent(CurrentSpecialActivity.this,
					FundraiserActivity.class);
			intent.putExtra("retailerId", retailID);
			intent.putExtra("retListId", retListId);
			intent.putExtra("retailLocationId", retailLocationID);
			intent.putExtra("retailerEvents", "true");
			startActivity(intent);
		} else {
			Intent currentSpecialListIntent = new Intent(
					CurrentSpecialActivity.this,
					CurrentSpecialsListActivity.class);
			currentSpecialListIntent.putExtra("specialitemName", itemName);
			currentSpecialListIntent.putExtra("retailLocationID",
					retailLocationID);
			currentSpecialListIntent.putExtra("retailID", retailID);
			currentSpecialListIntent.putExtra("retailerAd", retailerAd);
			currentSpecialListIntent.putExtra("retListId", retListId);
			currentSpecialListIntent.putExtra("retailerAdURL", retailerAdURL);
			currentSpecialListIntent.putExtra("retailerName", retailerName);
			startActivity(currentSpecialListIntent);
		}

		if (isSingleItem) {
			finish();
		}

	}

	private class GetFlag extends AsyncTask<String, Void, String> {

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();
		private ProgressDialog progDialog;
		private JSONObject flagObject;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			progDialog = new ProgressDialog(CurrentSpecialActivity.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage(Constants.DIALOG_MESSAGE);
			progDialog.setCancelable(false);
			progDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			try {
				String url_ret_spe_deal_off = Properties.url_local_server
						+ Properties.hubciti_version
						+ "thislocation/retspedealoff";
				String urlParameters = mUrlRequestParams
						.getRetSpecialsSalesInfo(retailLocationID, retailID);
				JSONObject xmlResponde = mServerConnections.getUrlPostResponse(
						url_ret_spe_deal_off, urlParameters, true);
				Log.e("urlParams_GETFLAG", urlParameters);
				Log.e("URL_GETFLAG", url_ret_spe_deal_off);
				if (xmlResponde != null) {
					JSONObject hotDealssDetailJson = xmlResponde
							.getJSONObject("SpecialOffersList");

					responseCode = hotDealssDetailJson
							.getString("responseCode");

					if (xmlResponde.has("response")) {
						JSONObject response = xmlResponde
								.getJSONObject("response");
						String responseText = response
								.getString("responseText");
						responseCode = response.getString("responseCode");

						Toast.makeText(getApplicationContext(),
								"getFlag responseText " + responseText,
								Toast.LENGTH_SHORT).show();
					}

					if (hotDealssDetailJson.has("mColor")) {
						mColor = hotDealssDetailJson.getString("mColor");
					}

					if (hotDealssDetailJson.has("mFontColor")) {
						mFontColor = hotDealssDetailJson
								.getString("mFontColor");
					}

					flagObject = xmlResponde.getJSONObject(
							Constants.TAG_PAGE_SPECIALOFFERFLAGRESULTSET)
							.getJSONObject(
									Constants.TAG_PAGE_SPECIALOFFERFLAGMENU);
					flagData = new HashMap<>();
					flagData.put("SpecialNames",
							flagObject.getString("specialNames"));

					result = "true";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				progDialog.dismiss();
				if (result.equalsIgnoreCase("false")) {
                    new AlertDialog.Builder(CurrentSpecialActivity.this)
                            .setTitle("Alert").setMessage(R.string.norecord)
                            .setPositiveButton("OK", null).show();

                } else {

                    String[] specialNamesArray = flagData.get("SpecialNames")
                            .split(",");

                    ArrayList<String> specialNamesList = new ArrayList<>(
                            Arrays.asList(specialNamesArray));

                    specialoffersListView.setBackgroundColor(Color
                            .parseColor(mColor));

                    specialoffersListView.setAdapter(new SpecialNamesListAdapter(
                            CurrentSpecialActivity.this, specialNamesList,
                            mFontColor, mColor));
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Constants.FINISHVALUE) {
			new AlertDialog.Builder(CurrentSpecialActivity.this)
					.setTitle("Alert").setMessage(requestCode + " reqCode")
					.setPositiveButton("OK", null).show();
			Log.e("FINISH", "FINISHHHH");
			setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}