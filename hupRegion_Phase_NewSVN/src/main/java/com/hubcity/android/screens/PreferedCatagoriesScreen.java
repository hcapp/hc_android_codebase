package com.hubcity.android.screens;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PreferedCatagoriesScreen extends CustomTitleBar implements
        OnClickListener {
    private HashMap<String, Object> categoryData = new HashMap<>();
    HashMap<String, Object> categoryListData = new HashMap<>();
    private ArrayList<HashMap<String, Object>> categoryList = null,
            trueList = new ArrayList<>();
    public static final String TAG_PREFERENCES_RESULT_SET = "CategoryDetails";
    public static final String TAG_MAIN_PREFERENCES_MENU = "MainCategory";
    public static final String TAG_MAIN_PREFERENCES_NAME = "parCatName";
    public static final String TAG_MAIN_PREFERENCES_ID = "parCatId";

    public static final String TAG_SUB_PREFERENCES_MENU = "SubCategory";


    String categoryListItemName, userId;
    boolean isItemExpanded, isSaveClicked;
    Integer itemLength;
    protected ListView categoryListView = null;
    protected CategoryListAdapter categoryListAdapter = null;
    protected Button savePrefbtn;
    protected ArrayList<String> catidList = new ArrayList<>();
    protected ArrayList<String> subCatidList = new ArrayList<>();
    SharedPreferences settings;
    static boolean isregis = false;
    private String btnFontColor;
    private String btnColor;
    String strPushRegMsg;
    RadioGroup telluswhatulikeRadio;
    RadioButton radioCategories, radioProd;
    Button footerBackButton;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences);
        strPushRegMsg = this.getIntent().getStringExtra(
                Constants.PUSH_NOTIFY_MSG);
        try {
            telluswhatulikeRadio = (RadioGroup) findViewById(R.id.telluswhatulike_radio);
            radioCategories = (RadioButton) findViewById(R.id.telluswhatulike_radio_categories);
            radioProd = (RadioButton) findViewById(R.id.telluswhatulike_radio_products);
            (findViewById(R.id.bottom_bar)).setBackgroundColor(Color.parseColor(Constants.getNavigationBarValues("titleBkGrdColor")));
            title.setSingleLine(true);
            title.setText("Tell Us What You Like");
            title.setTextSize(14);

            leftTitleImage.setVisibility(View.GONE);
            btnColor = getIntent().getExtras().getString("btnColor");
            btnFontColor = getIntent().getExtras().getString("btnFontColor");
            if (getIntent().getExtras().getBoolean("regacc")) {
                rightImage.setVisibility(View.GONE);
                divider.setVisibility(View.GONE);
                backImage.setVisibility(View.GONE);
            }

            userId = getUid();
            categoryListView = (ListView) findViewById(R.id.menu_preferences_listitem);
            savePrefbtn = (Button) findViewById(R.id.preferences_save);
            savePrefbtn.setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
            savePrefbtn.setOnClickListener(this);
            try {
                isregis = getIntent().getExtras().getBoolean("regacc");
                if (!isregis) {
                    savePrefbtn.setText("Save");
//                    savePrefbtn.setBackgroundResource(R.drawable.ic_action_save);
                }
            } catch (Exception e) {
                e.printStackTrace();
                isregis = false;
            }
            if (isregis) {
                footerBackButton = (Button) findViewById(R.id.preference_back);
                footerBackButton.setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
                footerBackButton.setVisibility(View.VISIBLE);
                footerBackButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        finish();

                    }
                });
            }

            radoibtnChecked();
            telluswhatulikeRadio
                    .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            radoibtnChecked();
                        }
                    });

            categoryListView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    savePrefbtn.setEnabled(true);
                    position = categoryList.indexOf(trueList.get(position));
                    categoryListItemName = (String) categoryList.get(position).get(
                            "itemName");
                    if (categoryListItemName
                            .equalsIgnoreCase(TAG_MAIN_PREFERENCES_MENU)) {
                        itemLength = (Integer) categoryList.get(position).get(
                                "subCatLength");
                        isItemExpanded = (Boolean) categoryList.get(position).get(
                                "expanded");
                        for (int catCount = position + 1; catCount < position
                                + itemLength; catCount++) {
                            if (isItemExpanded) {
                                categoryList.get(catCount).put("visibility", false);
                            } else {
                                categoryList.get(catCount).put("visibility", true);
                            }
                        }
                        if (isItemExpanded && itemLength > 1) {
                            categoryList.get(position + itemLength).put(
                                    "visibility", false);
                        } else if (!isItemExpanded && itemLength > 1) {
                            categoryList.get(position + itemLength).put(
                                    "visibility", true);
                        }
                        categoryList.get(position).put("expanded", !isItemExpanded);

                    } else if (categoryListItemName
                            .equalsIgnoreCase(TAG_SUB_PREFERENCES_MENU)) {
                        String stritemDisp = (String) categoryList.get(position)
                                .get("isAdded");
                        if (position != 0) {
                            if ("1".equalsIgnoreCase(stritemDisp)) {
                                categoryList.get(position).put("isAdded", "0");
                            } else {
                                categoryList.get(position).put("isAdded", "1");
                            }
                        }
                    }

                    filltrueList();

                    categoryListAdapter = new CategoryListAdapter(
                            PreferedCatagoriesScreen.this, trueList, categoryList);
                    Parcelable state = categoryListView.onSaveInstanceState();
                    categoryListView.setAdapter(categoryListAdapter);
                    categoryListView.onRestoreInstanceState(state);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void radoibtnChecked() {
        if (radioCategories.isChecked()) {
            categoryListView.setAdapter(null);
            new GetLocationCategory().execute();
        } else {
            categoryListView.setAdapter(null);
            new GetCategory().execute();
        }
    }

    @Override
    public void onDestroy() {
        categoryListView.setAdapter(null);
        super.onDestroy();
    }

    @SuppressWarnings("static-access")
    public String getUid() {
        SharedPreferences preferences = PreferedCatagoriesScreen.this
                .getSharedPreferences(Constants.PREFERENCE_HUB_CITY,
                        PreferedCatagoriesScreen.this.MODE_PRIVATE);
        String s = preferences.getString(Constants.PREFERENCE_KEY_U_ID, "-1");

        return s;
    }

    private void filltrueList() {
        trueList = new ArrayList<>();
        if (!categoryList.isEmpty()) {
            for (int catCount = 0; catCount < categoryList.size(); catCount++) {
                if (((String) categoryList.get(catCount).get("itemName"))
                        .equalsIgnoreCase(TAG_MAIN_PREFERENCES_MENU)) {
                    itemLength = (Integer) categoryList.get(catCount).get(
                            "subCatLength");
                    int localSum = 0;
                    for (int subcount = catCount + 1; subcount <= catCount
                            + itemLength; subcount++) {
                        localSum += Integer.valueOf((String) categoryList.get(
                                subcount).get("isAdded"));

                    }

                    if (itemLength - localSum == 0) {
                        categoryList.get(catCount).put("isAdded", 1f);
                    } else if (itemLength - localSum == itemLength) {
                        categoryList.get(catCount).put("isAdded", 0f);
                    } else {
                        categoryList.get(catCount).put("isAdded", 0.5f);
                    }
                }
                if ((Boolean) categoryList.get(catCount).get("visibility")) {
                    trueList.add(categoryList.get(catCount));
                }
            }
            float allSelected = 0f;
            float maincatCount = 0f;
            for (int catCount = 1; catCount < categoryList.size(); catCount++) {
                if (((String) categoryList.get(catCount).get("itemName"))
                        .equalsIgnoreCase(TAG_MAIN_PREFERENCES_MENU)) {
                    maincatCount += 1f;
                    allSelected += (Float) categoryList.get(catCount).get(
                            "isAdded");
                }
            }

            if (maincatCount - allSelected == 0) {
                categoryList.get(0).put("isAdded", 1f);
            } else if (maincatCount - allSelected == maincatCount) {
                categoryList.get(0).put("isAdded", 0f);
            } else {
                categoryList.get(0).put("isAdded", 0.5f);
            }

        }
    }

    private class GetCategory extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        JSONObject jsonObjectUserCategory = null;
        JSONArray jsonArrayMainCategory = null,
                jsonArraySubCategory = new JSONArray();
        JSONObject responseMenuObject = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(PreferedCatagoriesScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            categoryList = new ArrayList<>();
            String result = "false";
            try {
                String urlParameters = mUrlRequestParams
                        .createUserCatUrlParameter();
                String url_user_catagory = Properties.url_local_server
                        + Properties.hubciti_version + "firstuse/getusercat";
                jsonObjectUserCategory = mServerConnections.getUrlPostResponse(
                        url_user_catagory, urlParameters, true);

                if (jsonObjectUserCategory != null) {
                    jsonArrayMainCategory = jsonObjectUserCategory
                            .getJSONObject(TAG_PREFERENCES_RESULT_SET)
                            .getJSONArray(TAG_MAIN_PREFERENCES_MENU);
                    categoryData.put(TAG_MAIN_PREFERENCES_NAME, "All");
                    categoryData.put("itemName", TAG_MAIN_PREFERENCES_MENU);
                    categoryData.put("visibility", true);
                    categoryData.put("subCatLength", 0);
                    categoryData.put("expanded", false);
                    categoryData.put("isAdded", 0f);
                    categoryList.add(categoryData);
                    for (int arrayCount = 0; arrayCount < jsonArrayMainCategory
                            .length(); arrayCount++) {
                        categoryData = new HashMap<>();
                        responseMenuObject = jsonArrayMainCategory
                                .getJSONObject(arrayCount);
                        categoryData.put("itemName", TAG_MAIN_PREFERENCES_MENU);
                        categoryData.put("visibility", true);
                        categoryData.put("expanded", false);
                        categoryData.put(TAG_MAIN_PREFERENCES_NAME,
                                responseMenuObject
                                        .getString(TAG_MAIN_PREFERENCES_NAME));
                        categoryData.put(TAG_MAIN_PREFERENCES_ID,
                                responseMenuObject
                                        .getString(TAG_MAIN_PREFERENCES_ID));

                        jsonArraySubCategory = new JSONArray();
                        try {
                            jsonArraySubCategory.put(responseMenuObject
                                    .getJSONObject("SubCategory"));
                        } catch (Exception e) {
                            e.printStackTrace();
                            jsonArraySubCategory = responseMenuObject
                                    .getJSONArray("SubCategory");
                        }
                        categoryData.put("subCatLength",
                                jsonArraySubCategory.length());
                        categoryData.put("isAdded", 0f);
                        categoryList.add(categoryData);
                        for (int subCatCount = 0; subCatCount < jsonArraySubCategory
                                .length(); subCatCount++) {
                            categoryListData = new HashMap<>();

                            categoryListData.put("itemName",
                                    TAG_SUB_PREFERENCES_MENU);
                            categoryListData.put("visibility", false);
                            categoryListData
                                    .put("subCatName", categoryData
                                            .get(TAG_MAIN_PREFERENCES_NAME));
                            categoryListData.put(
                                    "categoryName",
                                    jsonArraySubCategory.getJSONObject(
                                            subCatCount)
                                            .getString("subCatName"));
                            categoryListData.put(
                                    TAG_MAIN_PREFERENCES_ID,
                                    jsonArraySubCategory.getJSONObject(
                                            subCatCount).getString("subCatId"));
                            categoryListData.put(
                                    "isAdded",
                                    jsonArraySubCategory.getJSONObject(
                                            subCatCount).getString("isAdded"));
                            categoryListData.put(
                                    "catId",
                                    jsonArraySubCategory.getJSONObject(
                                            subCatCount).getString("catId"));
                            categoryList.add(categoryListData);
                        }
                    }
                }
                result = "true";
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                mDialog.dismiss();
                if ("true".equals(result)) {
                    filltrueList();

                    if (isregis) {
                        // Showing saved alert
                        Builder findAlert = new Builder(
                                PreferedCatagoriesScreen.this);
                        findAlert.setPositiveButton(
                                getString(R.string.specials_ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        dialog.cancel();
                                    }
                                });
                        findAlert
                                .setMessage(getString(R.string.city_pref_info_msg));
                        findAlert.create().show();
                    }
                    categoryListAdapter = new CategoryListAdapter(
                            PreferedCatagoriesScreen.this, trueList, categoryList);
                    categoryListView.setAdapter(categoryListAdapter);
                } else {
                    Toast.makeText(getBaseContext(), "Error Calling WebService",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetLocationCategory extends AsyncTask<String, Void, String> {

        JSONObject jsonObjectUserCategory = null;
        JSONArray jsonArrayMainCategory = null,
                jsonArraySubCategory = new JSONArray();
        JSONObject responseMenuObject = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(PreferedCatagoriesScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            categoryList = new ArrayList<>();
            String result = "false";
            try {
                String url_user_location_catagory = Properties.url_local_server
                        + Properties.hubciti_version + "firstuse/getuserloccat";
                String url = url_user_location_catagory + "?userId="
                        + UrlRequestParams.getUid() + "&hubCitiId="
                        + UrlRequestParams.getHubCityId();
                jsonObjectUserCategory = ServerConnections
                        .requestWebService(url);
                if (jsonObjectUserCategory != null) {
                    jsonArrayMainCategory = jsonObjectUserCategory
                            .getJSONObject(TAG_PREFERENCES_RESULT_SET)
                            .getJSONArray(TAG_MAIN_PREFERENCES_MENU);
                    categoryData.put(TAG_MAIN_PREFERENCES_NAME, "All");
                    categoryData.put("itemName", TAG_MAIN_PREFERENCES_MENU);
                    categoryData.put("visibility", true);
                    categoryData.put("subCatLength", 0);
                    categoryData.put("expanded", false);
                    categoryData.put("isAdded", 0f);
                    categoryList.add(categoryData);
                    for (int arrayCount = 0; arrayCount < jsonArrayMainCategory
                            .length(); arrayCount++) {
                        categoryData = new HashMap<>();
                        responseMenuObject = jsonArrayMainCategory
                                .getJSONObject(arrayCount);
                        categoryData.put("itemName", TAG_MAIN_PREFERENCES_MENU);
                        categoryData.put("visibility", true);
                        categoryData.put("expanded", false);
                        categoryData.put(TAG_MAIN_PREFERENCES_NAME,
                                responseMenuObject
                                        .getString(TAG_MAIN_PREFERENCES_NAME));
                        categoryData.put(TAG_MAIN_PREFERENCES_ID,
                                responseMenuObject
                                        .getString(TAG_MAIN_PREFERENCES_ID));

                        jsonArraySubCategory = new JSONArray();
                        try {
                            jsonArraySubCategory.put(responseMenuObject
                                    .getJSONObject("SubCategory"));
                        } catch (Exception e) {
                            e.printStackTrace();
                            jsonArraySubCategory = responseMenuObject
                                    .getJSONArray("SubCategory");
                        }
                        categoryData.put("subCatLength",
                                jsonArraySubCategory.length());
                        categoryData.put("isAdded", 0f);
                        categoryList.add(categoryData);
                        for (int subCatCount = 0; subCatCount < jsonArraySubCategory
                                .length(); subCatCount++) {
                            categoryListData = new HashMap<>();

                            categoryListData.put("itemName",
                                    TAG_SUB_PREFERENCES_MENU);
                            categoryListData.put("visibility", false);
                            categoryListData
                                    .put("subCatName", categoryData
                                            .get(TAG_MAIN_PREFERENCES_NAME));
                            categoryListData.put(
                                    "categoryName",
                                    jsonArraySubCategory.getJSONObject(
                                            subCatCount)
                                            .getString("subCatName"));
                            categoryListData.put(
                                    "subCatId",
                                    jsonArraySubCategory.getJSONObject(
                                            subCatCount).getString("subCatId"));
                            categoryListData.put(
                                    "isAdded",
                                    jsonArraySubCategory.getJSONObject(
                                            subCatCount).getString("isAdded"));

                            categoryListData.put("catId", responseMenuObject
                                    .getString(TAG_MAIN_PREFERENCES_ID));

                            categoryList.add(categoryListData);
                        }
                    }
                }
                result = "true";
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                mDialog.dismiss();
                if ("true".equals(result)) {
                    filltrueList();
                    categoryListAdapter = new CategoryListAdapter(
                            PreferedCatagoriesScreen.this, trueList, categoryList);
                    categoryListView.setAdapter(categoryListAdapter);
                } else {
                    Toast.makeText(getBaseContext(), "Error Calling WebService",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class SavePreferences extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        JSONObject jsonObject = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(PreferedCatagoriesScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            categoryList = new ArrayList<>();
            String result = "false";
            String urlParameters = "";
            try {

                if (radioCategories.isChecked()) {
                    urlParameters = mUrlRequestParams
                            .createSavePrefParameterforLocation(catidList,
                                    subCatidList);
                    String url_set_user_loc_catagory = Properties.url_local_server
                            + Properties.hubciti_version
                            + "firstuse/setuserloccat";
                    jsonObject = mServerConnections.getUrlPostResponse(
                            url_set_user_loc_catagory, urlParameters, true);
                } else {
                    urlParameters = mUrlRequestParams
                            .createSavePrefParameter(catidList);
                    String url_set_user_catagory = Properties.url_local_server
                            + Properties.hubciti_version
                            + "firstuse/setusercat";
                    jsonObject = mServerConnections.getUrlPostResponse(
                            url_set_user_catagory, urlParameters, true);
                }

                if (jsonObject != null) {
                    if ("10000".equalsIgnoreCase(jsonObject.getJSONObject(
                            "response").getString("responseCode"))) {
                        result = "true";
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                mDialog.dismiss();

                if (isregis) {
                    if (radioCategories.isChecked()) {
                        radioProd.setChecked(true);
                        radioCategories.setChecked(false);
                        clearData();

                    } else if (radioProd.isChecked()) {
                        Intent prenext = new Intent(PreferedCatagoriesScreen.this,
                                GetStartedActivity.class);
                        if (strPushRegMsg != null) {
                            prenext.putExtra(Constants.PUSH_NOTIFY_MSG,
                                    strPushRegMsg);
                        }
                        prenext.putExtra("btnColor", btnColor);
                        prenext.putExtra("btnFontColor", btnFontColor);
                        startActivity(prenext);

                        // isregis = false;
                    }

                } else {

                    Builder findAlert = new Builder(
                            PreferedCatagoriesScreen.this);
                    findAlert.setTitle(getString(R.string.pref_info))

                            .setPositiveButton(getString(R.string.specials_ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int whichButton) {
                                            dialog.cancel();

                                            if (radioCategories.isChecked()) {
                                                radioProd.setChecked(true);
                                                radioCategories.setChecked(false);
                                                clearData();
                                            } else if (radioProd.isChecked()) {
                                                radoibtnChecked();
                                            }
                                        }
                                    });

                    if ("true".equals(result)) {
                        findAlert.setMessage(getString(R.string.pref_info_msg));
                    } else {
                        findAlert.setMessage(getString(R.string.pref_error_msg));
                    }
                    findAlert.create().show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.preferences_save:
                preferencesSave();
                break;
            default:
                break;
        }

    }

    private void preferencesSave() {
        catidList = new ArrayList<>();
        for (HashMap<String, Object> item : categoryList) {
            if (item.get("itemName").equals(TAG_SUB_PREFERENCES_MENU)
                    && "1".equals(item.get("isAdded"))) {

                if (!catidList.contains((String) item.get("catId"))) {
                    catidList.add((String) item.get("catId"));

                    if (radioCategories.isChecked() && !subCatidList.isEmpty()) {
                        subCatidList.add("|");
                    }
                }

                if (radioCategories.isChecked()) {
                    if ("All".equalsIgnoreCase((String) item
                            .get("categoryName"))) {
                        subCatidList.add("NULL");
                    } else {
                        subCatidList.add((String) item.get("subCatId"));
                    }
                }
            }
        }
        isSaveClicked = true;
        new SavePreferences().execute();

    }

    private void clearData() {
        categoryList = new ArrayList<>();
        trueList = new ArrayList<>();
        categoryListData = new HashMap<>();
        categoryData = new HashMap<>();
        catidList = new ArrayList<>();
        subCatidList = new ArrayList<>();

    }
}
