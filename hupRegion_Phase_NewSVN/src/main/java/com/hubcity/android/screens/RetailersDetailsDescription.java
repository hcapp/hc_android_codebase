package com.hubcity.android.screens;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SpecialRetailerEntryItem;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class RetailersDetailsDescription extends CustomTitleBar implements
		OnClickListener, OnItemClickListener {
	ListDetails objListDetails;
	private ProgressDialog progDialog;
	ArrayList<Item> items = new ArrayList<>();
	protected ListView listview;
	String mItemId, bottomBtnId;
	View paginator;
	ImageButton nextPageBtn, prevPageBtn;
	int lastvisitProdId = 0, minProdId = 0;
	String responseText;
	Button rightButton;
	private static String returnValue = "sucessfull";
	String strretailLocationId, strretailerId;
	boolean isMainmenuBtnClicked = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.retailer_detail_list);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			strretailLocationId = extras.getString("retailLocationId");
			strretailerId = extras.getString("retailerId");
			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
				mItemId = getIntent().getExtras().getString(
						Constants.MENU_ITEM_ID_INTENT_EXTRA);

			}

			if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
				bottomBtnId = getIntent().getExtras().getString(
						Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);

			}
			paginator = getLayoutInflater().inflate(
					R.layout.hot_deals_pagination, listview, true);
			prevPageBtn = (ImageButton) paginator
					.findViewById(R.id.hotdeals_prev_page);
			nextPageBtn = (ImageButton) paginator
					.findViewById(R.id.hotdeals_next_page);
			prevPageBtn.setOnClickListener(this);
			nextPageBtn.setOnClickListener(this);
			listview = (ListView) findViewById(R.id.special_offerList);

			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText(R.string.title_specials);

			leftTitleImage.setVisibility(View.GONE);

			rightImage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (GlobalConstants.isFromNewsTemplate){
						Intent intent = null;
						switch (GlobalConstants.className) {
							case Constants.COMBINATION:
								intent = new Intent(RetailersDetailsDescription.this, CombinationTemplate.class);
								break;
							case Constants.SCROLLING:
								intent = new Intent(RetailersDetailsDescription.this, ScrollingPageActivity.class);
								break;
							case Constants.NEWS_TILE:
								intent = new Intent(RetailersDetailsDescription.this, TwoTileNewsTemplateActivity.class);
								break;
						}
						if (intent != null) {
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
						}
					}else {
						if ("pushnotify".equals(getIntent().getStringExtra("push"))) {
							if (SortDepttNTypeScreen.subMenuDetailsList != null) {
								SortDepttNTypeScreen.subMenuDetailsList.clear();
								SubMenuStack.clearSubMenuStack();
								SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
							}

							callingMainMenu();
						} else {
							setResult(Constants.FINISHVALUE);
							isMainmenuBtnClicked = true;
							finish();
						}
					}

				}
			});
			new SpecialOfferDisplay().execute();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			isMainmenuBtnClicked = true;
			this.setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public class SpecialOfferDisplay extends AsyncTask<String, Void, String> {

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();
			boolean nextPage = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDialog = new ProgressDialog(RetailersDetailsDescription.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage(Constants.DIALOG_MESSAGE);
			progDialog.setCancelable(false);
			progDialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if ("sucessfull".equals(returnValue)) {
				if (minProdId > 1 || nextPage) {

					if (minProdId > 1) {
						prevPageBtn.setVisibility(View.VISIBLE);
					} else {
						prevPageBtn.setVisibility(View.GONE);
					}
					if (nextPage) {
						nextPageBtn.setVisibility(View.VISIBLE);
					} else {
						nextPageBtn.setVisibility(View.GONE);
					}

					listview.addFooterView(paginator);

				} else {
					listview.removeFooterView(paginator);
				}

				items = new ArrayList<>();

				for (int i = 0; i < objListDetails.alertList.size(); i++) {

					items.add(new SpecialRetailerEntryItem(
							objListDetails.alertList.get(i).pageLink,
							objListDetails.alertList.get(i).specialsListId,
							objListDetails.alertList.get(i).rowNum,
							objListDetails.alertList.get(i).sDescription,
							objListDetails.alertList.get(i).pageId,
							objListDetails.alertList.get(i).pageTitle));

				}

				if (objListDetails.alertList.size() > 1) {
					SpecialsRetailerEntryAdapter adapter = new SpecialsRetailerEntryAdapter(
							getApplicationContext(), items);
					listview.setAdapter(adapter);
					listview.setOnItemClickListener(RetailersDetailsDescription.this);

				} else {
					specialsScreen(0);
					finish();
				}

			} else {
				Toast.makeText(getApplicationContext(), responseText,
						Toast.LENGTH_LONG).show();

			}

			try {
				progDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String flagValue = "0";
		Boolean isFlag = false;

		@Override
		protected String doInBackground(String... params) {
			try {
				minProdId = lastvisitProdId;
				String url_getspecialofferlist = Properties.url_local_server
						+ Properties.hubciti_version
						+ "thislocation/getspecialofferlist";
				String urlParameters = mUrlRequestParams.getSpecialOfferList(
						lastvisitProdId + "", strretailLocationId,
						strretailerId, null);
				JSONObject jsonRespone = mServerConnections.getUrlPostResponse(
						url_getspecialofferlist, urlParameters, true);
				if (jsonRespone.has("RetailersDetails")) {
					JSONObject jsonRetailersDetails = jsonRespone
							.getJSONObject("RetailersDetails");
					JSONObject jsonretailersDetail = jsonRetailersDetails
							.getJSONObject("retailerDetail");

					try {
						JSONArray jsonRetailerDetail = jsonretailersDetail
								.getJSONArray("RetailerDetail");

						if (jsonRetailerDetail != null) {
							objListDetails = new ListDetails();
							try {

								if ("1".equalsIgnoreCase(jsonRetailersDetails
										.getString("nextPage"))) {
									nextPage = true;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

							for (int i = 0; i < jsonRetailerDetail.length(); i++) {
								JSONObject elem = jsonRetailerDetail
										.getJSONObject(i);
								if (elem != null) {
									RetailerGroup objRetailerGroup = new RetailerGroup();
									objRetailerGroup.pageLink = elem
											.getString("pageLink");
									objRetailerGroup.specialsListId = elem
											.getString("specialsListId");
									objRetailerGroup.rowNum = elem
											.getString("rowNum");
									objRetailerGroup.sDescription = elem
											.getString("sDescription");
									objRetailerGroup.pageId = elem
											.getString("pageId");
									objRetailerGroup.pageTitle = elem
											.getString("pageTitle");
									objListDetails.alertList
											.add(objRetailerGroup);
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						objListDetails = new ListDetails();
						JSONObject elem = jsonretailersDetail
								.getJSONObject("RetailerDetail");
						if (elem != null) {
							RetailerGroup objRetailerGroup = new RetailerGroup();
							objRetailerGroup.pageLink = elem
									.getString("pageLink");
							objRetailerGroup.specialsListId = elem
									.getString("specialsListId");
							objRetailerGroup.rowNum = elem.getString("rowNum");
							objRetailerGroup.sDescription = elem
									.getString("sDescription");
							objRetailerGroup.pageId = elem.getString("pageId");
							objRetailerGroup.pageTitle = elem
									.getString("pageTitle");
							objListDetails.alertList.add(objRetailerGroup);
						}
					}
					try {
						if (Integer.valueOf(jsonRetailersDetails
								.getString("maxRowNum")) > lastvisitProdId) {
							lastvisitProdId = Integer
									.valueOf(jsonRetailersDetails
											.getString("maxRowNum"));

						}
					} catch (Exception e) {
						e.printStackTrace();
						returnValue = "unsuccess";
					}

				} else {
					JSONObject jsonRecordNotFound = jsonRespone
							.getJSONObject("response");
					responseText = jsonRecordNotFound.getString("responseText");
					returnValue = "UNSUCCESS";
					return returnValue;
				}
			} catch (Exception e) {
				e.printStackTrace();
				returnValue = "unsuccess";

			}
			return returnValue;
		}
	}

	public class ListDetails {
		ArrayList<RetailerGroup> alertList = new ArrayList<>();

	}

	class RetailerGroup {
		String pageLink;
		String specialsListId;
		String rowNum;
		String sDescription;
		String pageId;
		String pageTitle;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.hotdeals_prev_page:
			hotdealsPrevPage();
			break;
		case R.id.hotdeals_next_page:
			new SpecialOfferDisplay().execute();
			break;
		default:
			break;
		}

	}

	private void hotdealsPrevPage() {
		lastvisitProdId = 0 > (minProdId - Constants.RANGE) ? 0 : minProdId
				- Constants.RANGE;
		new SpecialOfferDisplay().execute();

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {

		try {
			specialsScreen(position);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void specialsScreen(int position) {
		SpecialRetailerEntryItem item = (SpecialRetailerEntryItem) items
				.get(position);

		Intent intent = new Intent(getApplicationContext(),
				SpecialOffersScreen.class);
		intent.putExtra("retailerId",
				getIntent().getExtras().getString("retailerId"));
		intent.putExtra("retailLocationId",
				getIntent().getExtras().getString("retailLocationId"));
		intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
		intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
		intent.putExtra("pageId", item.pageId);

		startActivityForResult(intent, 115);

	}

	@Override
	public void finish() {
		super.finish();

	}

}