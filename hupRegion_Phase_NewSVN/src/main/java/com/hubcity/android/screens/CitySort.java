package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TableRow;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.scansee.hubregion.R.color;

public class CitySort {
    boolean isViewAdded;

    Activity activity;
    LinearLayout ll;
    CheckBox cityCheckSort;
    TableRow citySort;

    String className;
    String citiExpID;
    String mItemId, mBottomId;
    String latitude, longitude, catName, srchKey, catIds;
    String radius;
    String retailerId = "", retailerLocationId = "";
    String isRetailerEvents = "";
    String subCatId = "";
    String mLinkId = "";
    String typeId = "";
    String deptId = "";
    String fundId = "";
    String level = "";
    String selectedCities = "";
    boolean isDeals;

    static ArrayList<String> city_name = new ArrayList<>();
    static ArrayList<String> city_id = new ArrayList<>();
    static ArrayList<String> selected_city_id = new ArrayList<>();
    static ArrayList<String> isCitySelected = new ArrayList<>();
    public static boolean isDefaultCitySort = false;

    CityAsyncTask cityAsyncTask;

    // Constructor for City Experience
    public CitySort(Activity _activity, LinearLayout _ll,
                    CheckBox _cityCheckSort, TableRow _citySort, String _className,
                    String _citiExpID) {
        activity = _activity;
        ll = _ll;
        cityCheckSort = _cityCheckSort;
        citySort = _citySort;
        className = _className;
        citiExpID = _citiExpID;

        ll.setVisibility(View.GONE);
        if (!isDefaultCitySort) {
            callCityAsyncTask();
        }
    }

    // Constructor for Events
    public CitySort(Activity _activity, LinearLayout _ll,
                    CheckBox _cityCheckSort, TableRow _citySort, String _className,
                    String _mItemId, String _mBottomId, String _fundId,
                    String _retailerId, String _retailerLocationId,
                    String _isRetailerEvents) {

        activity = _activity;
        ll = _ll;
        cityCheckSort = _cityCheckSort;
        citySort = _citySort;
        className = _className;
        mItemId = _mItemId;
        mBottomId = _mBottomId;
        retailerId = _retailerId;
        retailerLocationId = _retailerLocationId;
        isRetailerEvents = _isRetailerEvents;
        fundId = _fundId;
        ll.setVisibility(View.GONE);

        if (!isDefaultCitySort) {
            callCityAsyncTask();
        }
    }

    // Constructor for Fundraiser
    public CitySort(Activity _activity, LinearLayout _ll,
                    CheckBox _cityCheckSort, TableRow _citySort, String _className,
                    String _mItemId, String _mBottomId, String _fundId,
                    String _retailerId, String _retailerLocationId,
                    String _isRetailerEvents, boolean _isDeals) {

        isDeals = _isDeals;

        activity = _activity;
        ll = _ll;
        cityCheckSort = _cityCheckSort;
        citySort = _citySort;
        className = _className;
        mItemId = _mItemId;
        mBottomId = _mBottomId;
        retailerId = _retailerId;
        retailerLocationId = _retailerLocationId;
        isRetailerEvents = _isRetailerEvents;
        fundId = _fundId;
        ll.setVisibility(View.GONE);

        if (!isDefaultCitySort) {
            callCityAsyncTask();
        }
    }

    // Constructor for Find All
    public CitySort(Activity _activity, LinearLayout _ll,
                    CheckBox _cityCheckSort, TableRow _citySort, String _className,
                    String _mItemId, String _mBottomId, String _radius,
                    String _latitude, String _longitude, String _catName,
                    String _srchKey, String _catIds, String _subCatId) {

        activity = _activity;
        ll = _ll;
        cityCheckSort = _cityCheckSort;
        citySort = _citySort;
        className = _className;
        mItemId = _mItemId;
        mBottomId = _mBottomId;
        radius = _radius;
        latitude = _latitude;
        longitude = _longitude;
        catName = _catName;
        srchKey = _srchKey;
        catIds = _catIds;
        subCatId = _subCatId;

        ll.setVisibility(View.GONE);

        if (!isDefaultCitySort) {
            callCityAsyncTask();
        }

    }

    // Constructor for SubMenu
    public CitySort(Activity _activity, LinearLayout _ll,
                    CheckBox _cityCheckSort, TableRow _citySort, String _className,
                    String _mLinkId, String _typeId, String _deptId, int _level,
                    String _selectedCities) {

        activity = _activity;
        ll = _ll;
        cityCheckSort = _cityCheckSort;
        citySort = _citySort;
        className = _className;
        mLinkId = _mLinkId;
        typeId = _typeId;
        deptId = _deptId;
        level = String.valueOf(_level);

        isViewAdded = false;

        ll.setVisibility(View.GONE);

        selectedCities = _selectedCities;

        callCityAsyncTask();
    }

    public void sortAction() {

        cityCheckSort.setVisibility(View.VISIBLE);
        ll.setVisibility(View.VISIBLE);

        if (!isViewAdded) {
            for (int i = 0; i < city_name.size(); i++) {
                CheckBox cb = new CheckBox(activity.getApplicationContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 10, 10, 10);
                cb.setLayoutParams(params);

                cb.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.checkbox_selector, 0, 0, 0);
                cb.setButtonDrawable(activity.getResources().getDrawable(
                        android.R.color.transparent));
                cb.setCompoundDrawablePadding(10);
                cb.setPadding(10, 10, 10, 10);
                cb.setTextColor(color.black);
                cb.setTag(city_id.get(i));
                cb.setText(city_name.get(i));
                cb.setTextSize(20F);

                if (isDefaultCitySort) {

                    if (selected_city_id.isEmpty()) {
                        if (!"Events".equalsIgnoreCase(className)
                                && !"Fund".equalsIgnoreCase(className)) {
                            if ("0".equals(isCitySelected.get(i))) {
                                cb.setChecked(false);

                            } else {
                                cb.setChecked(true);
                            }
                        } else {
                            cb.setChecked(true);
                        }

                    } else {
                        for (int j = 0; j < selected_city_id.size(); j++) {
                            if (city_id.get(i)
                                    .equals(selected_city_id.get(j))) {
                                cb.setChecked(true);
                                break;
                            } else {
                                cb.setChecked(false);
                            }
                        }
                    }
                } else if (!"Events".equalsIgnoreCase(className)
                        && !"Fund".equalsIgnoreCase(className)) {
                    if (selected_city_id.isEmpty()) {
                        if ("0".equals(isCitySelected.get(i))) {
                            cb.setChecked(false);

                        } else {
                            cb.setChecked(true);
                        }

                    } else {
                        for (int j = 0; j < selected_city_id.size(); j++) {
                            if (city_id.get(i)
                                    .equals(selected_city_id.get(j))) {
                                cb.setChecked(true);
                                break;
                            } else {
                                cb.setChecked(false);
                            }
                        }
                    }
                } else {
                    cb.setChecked(true);
                }

                cb.setVisibility(View.VISIBLE);

                cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {

                        if (isChecked) {
                            if (selected_city_id != null) {
                                selected_city_id.add(buttonView.getTag()
                                        .toString());
                            }

                        } else {
                            if (selected_city_id != null
                                    && !selected_city_id.isEmpty()) {
                                selected_city_id.remove(buttonView.getTag()
                                        .toString());
                            }

                        }

                    }
                });
                ll.addView(cb);
            }

            isViewAdded = true;
        }

    }

    public class CityAsyncTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog mDialog;

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        String responseText = "";
        String responseCode = "";

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(activity, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);

            city_id = new ArrayList<>();
            city_name = new ArrayList<>();
            selected_city_id = new ArrayList<>();
            isCitySelected = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String urlParameters;

            if ("Find All".equals(className) || "Find Single".equals(className)) {
                urlParameters = mUrlRequestParams.createCityListParameterFind(
                        className, mItemId, mBottomId, latitude, longitude,
                        catName, srchKey, catIds, subCatId);
            } else if ("SubMenu".equals(className)) {
                urlParameters = mUrlRequestParams
                        .createCityListParameterSubMenu(className, mLinkId,
                                typeId, deptId, level, "");
            } else {

                if (isDeals) {
                    urlParameters = mUrlRequestParams.createCityListParameter(
                            className, citiExpID, "", "", fundId, retailerId,
                            retailerLocationId, isRetailerEvents, "", srchKey);
                } else {
                    urlParameters = mUrlRequestParams.createCityListParameter(
                            className, citiExpID, mItemId, mBottomId, fundId,
                            retailerId, retailerLocationId, isRetailerEvents, "", srchKey);
                }
            }

            String get_user_city_pref_url = Properties.url_local_server
                    + Properties.hubciti_version + "firstuse/getusercitypref";
            JSONObject jsonResponse = mServerConnections.getUrlPostResponse(
                    get_user_city_pref_url, urlParameters, true);

            try {
                if (jsonResponse.has("City")) {
                    JSONObject json2 = jsonResponse.getJSONObject("City");
                    if (json2.has("responseCode")) {
                        responseCode = json2.getString("responseCode");
                    }

                    if (json2.has("responseText")) {
                        responseText = json2.getString("responseText");
                    }

                    if ("10000".equals(responseCode)) {
                        if (json2.has("cityList")) {
                            JSONObject jsonCity = json2
                                    .getJSONObject("cityList");

                            if (jsonCity.has("City")) {
                                Object cityObj = jsonCity.get("City");

                                if (cityObj instanceof JSONObject) {
                                    JSONObject cityJsonObj = jsonCity
                                            .getJSONObject("City");

                                    if (cityJsonObj.has("cityId")) {
                                        city_id.add(cityJsonObj
                                                .getString("cityId"));
                                        if (!"Events"
                                                .equalsIgnoreCase(className)
                                                && !"Fund"
                                                .equalsIgnoreCase(className)) {

                                            if (cityJsonObj
                                                    .has("isCityChecked")) {

                                                isCitySelected
                                                        .add(cityJsonObj
                                                                .getString("isCityChecked"));

                                                if ("1".equalsIgnoreCase(cityJsonObj
                                                        .getString("isCityChecked"))) {
                                                    selected_city_id
                                                            .add(cityJsonObj
                                                                    .getString("cityId"));
                                                }
                                            }

                                        } else {
                                            selected_city_id.add(cityJsonObj
                                                    .getString("cityId"));
                                        }
                                    }

                                    if (cityJsonObj.has("cityName")) {
                                        city_name.add(cityJsonObj
                                                .getString("cityName"));
                                    }

                                } else if (cityObj instanceof JSONArray) {
                                    JSONArray cityJsonArr = jsonCity
                                            .getJSONArray("City");
                                    for (int i = 0; i < cityJsonArr.length(); i++) {
                                        JSONObject innerObj = cityJsonArr
                                                .getJSONObject(i);
                                        if (innerObj.has("cityId")) {

                                            city_id.add(innerObj
                                                    .getString("cityId"));
                                            if (!"Events"
                                                    .equalsIgnoreCase(className)
                                                    && !"Fund"
                                                    .equalsIgnoreCase(className)) {

                                                if (innerObj
                                                        .has("isCityChecked")) {

                                                    isCitySelected
                                                            .add(innerObj
                                                                    .getString("isCityChecked"));

                                                    if ("1".equalsIgnoreCase(innerObj
                                                            .getString("isCityChecked"))) {
                                                        selected_city_id
                                                                .add(innerObj
                                                                        .getString("cityId"));
                                                    }
                                                }

                                            } else {
                                                selected_city_id.add(innerObj
                                                        .getString("cityId"));
                                            }

                                        }
                                        if (innerObj.has("cityName")) {
                                            city_name.add(innerObj
                                                    .getString("cityName"));
                                        }

                                    }
                                }
                            }
                        }
                    }
                } else {

                    if (jsonResponse.has("response")) {
                        JSONObject json2 = jsonResponse
                                .getJSONObject("response");
                        if (json2.has("responseCode")) {
                            responseCode = json2.getString("responseCode");
                        }

                        if (json2.has("responseText")) {
                            responseText = json2.getString("responseText");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                if (mDialog != null && !activity.isFinishing()) {
                    mDialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mDialog = null;
            }

            if (!"10000".equals(responseCode)) {

                citySort.setVisibility(View.GONE);

            } else {
                if ("SubMenu".equalsIgnoreCase(className)) {
                    if (isDefaultCitySort) {

                        if (selectedCities != null && !selectedCities.isEmpty()) {
                            ArrayList<String> items = new ArrayList<>(
                                    Arrays.asList(selectedCities.split(",")));
                            selected_city_id.clear();
                            selected_city_id = items;
                        }
                    }
                }

            }

        }

    }

    public static String getSelectedCities() {
        String selected_cities = "";

        if (selected_city_id != null && !selected_city_id.isEmpty()) {
            for (int i = 0; i < selected_city_id.size(); i++) {
                selected_cities += selected_city_id.get(i);

                if (i != selected_city_id.size() - 1) {
                    selected_cities += ",";
                }

            }
        }

        return selected_cities;
    }

    private void callCityAsyncTask() {

        if (cityAsyncTask != null) {
            if (!cityAsyncTask.isCancelled()) {
                cityAsyncTask.cancel(true);
            }

            cityAsyncTask = null;
        }

        cityAsyncTask = new CityAsyncTask();
        cityAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

}
