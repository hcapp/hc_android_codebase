package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.CitiExperienceEntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class AustinExpRetailersListFilterAdapter extends BaseAdapter {
	private Activity activity;
	private ArrayList<Item> items;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	public AustinExpRetailersListFilterAdapter(Activity activity,
			ArrayList<Item> items) {
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.items = items;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int id) {
		return items.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view;
		ViewHolder viewHolder = new ViewHolder();

		/***
		 * If the list item reaches to its last position then it will be called
		 * for next pagination
		 ***/
		if (position == items.size() - 1) {
			if (!((CitiExperienceActivity) activity).isAlreadyLoading) {
				if (((CitiExperienceActivity) activity).nextPage) {
					((CitiExperienceActivity) activity).isAlreadyLoading = true;
					((CitiExperienceActivity) activity).enableBottomButton = false;
					((CitiExperienceActivity) activity).viewMoreBtn();
				}
			}
		}

		final Item i = items.get(position);

		if (i != null) {
			if (i.isSection()) {
				SectionItem si = (SectionItem) i;

				view = inflater.inflate(R.layout.citi_experience_type_header,
						parent,false);

				view.setOnClickListener(null);
				view.setOnLongClickListener(null);
				view.setLongClickable(false);

				final TextView sectionView = (TextView) view
						.findViewById(R.id.type_header);
				sectionView.setTextColor(Color.BLACK);
				sectionView.setBackgroundColor(Color.parseColor("#C0C0C0"));
				sectionView.setPadding(5, 5, 5, 5);

				if (!("N/A").equalsIgnoreCase(si.getTitle())
						&& !"".equals(si.getTitle())) {
					sectionView.setText(si.getTitle());
					sectionView.setVisibility(View.VISIBLE);

				} else {
					sectionView.setVisibility(View.GONE);
				}
			} else {
				
				CitiExperienceEntryItem ei = (CitiExperienceEntryItem) i;
				
				view = inflater.inflate(
						R.layout.listitem_austin_exp_get_retailers, parent,false);
				viewHolder.retailerImage = (ImageView) view
						.findViewById(R.id.retailer_image);
				viewHolder.retailerName = (TextView) view
						.findViewById(R.id.retailer_name);
				viewHolder.retailerSpecials = (ImageView) view
						.findViewById(R.id.retailer_specials);
				viewHolder.retailerMileAddress = (TextView) view
						.findViewById(R.id.retailer_address);
				viewHolder.retailerDistance = (TextView) view
						.findViewById(R.id.distance);
				viewHolder.retailerMileAddress2 = (TextView) view
						.findViewById(R.id.retailer_address2);
				viewHolder.retailerStatus = (TextView) view.findViewById(R.id.retailer_status);

				String locationStatus = ei.locationOpen;
				if (locationStatus != null && !locationStatus.isEmpty() && !locationStatus.equalsIgnoreCase("N/A")) {
					viewHolder.retailerStatus.setText(locationStatus);
				}

				viewHolder.retailerName.setText(ei.retailerName);

				String retailerAddress="";
				if (ei.retailAddress1 != null && !ei.retailAddress1.equals("N/A")){
					retailerAddress = ei.retailAddress1;
				}
				if (ei.retailAddress2 != null && !ei.retailAddress2.equals("N/A")){
					retailerAddress = retailerAddress + ", " +ei.retailAddress2;
				}
				viewHolder.retailerMileAddress.setText(retailerAddress);
				if (!"N/A".equals(ei.distance) && ei.distance != null) {
					viewHolder.retailerDistance.setVisibility(View.VISIBLE);
					viewHolder.retailerDistance.setText(ei.distance);
				} else {
					viewHolder.retailerDistance.setVisibility(View.GONE);
				}
				String address = "";
				if (!"N/A".equals(ei.city) && ei.city != null) {
					address = ei.city;
				}
				if (!"N/A".equals(ei.state) && ei.state != null) {
					if (address.isEmpty())
						address = ei.state;
					else
						address = address + ", " + ei.state;
				}
				if (!"N/A".equals(ei.postalCode) && ei.postalCode != null) {
					if (address.isEmpty())
						address = ei.postalCode;
					else
						address = address + ", " + ei.postalCode;
				}
				viewHolder.retailerMileAddress2.setText(address);

				if ("true".equalsIgnoreCase(ei.saleFlag)) {
					viewHolder.retailerSpecials.setVisibility(View.VISIBLE);
				} else {
					viewHolder.retailerSpecials.setVisibility(View.GONE);
				}

				if (!"N/A".equalsIgnoreCase(ei.logoImagePath)) {
					viewHolder.retailerImage.setTag(ei
							.logoImagePath);
					customImageLoader.displayImage(ei.logoImagePath,
							activity, viewHolder.retailerImage);
					customImageLoader.displayImage(ei.bannerAdImagePath,
							activity, new ImageView(activity));
				} else {
					viewHolder.retailerImage.setImageBitmap(null);
				}
			}

		} else {
			view = inflater.inflate(R.layout.listitem_get_retailers_viewmore,
					parent,false);
		}

		return view;
	}

	public static class ViewHolder {
		ImageView retailerImage;
		TextView retailerName;
		ImageView retailerSpecials;
		TextView retailerMileAddress;
		TextView retailerMileAddress2;
		TextView retailerDistance;
		TextView retailerStatus;

	}
}
