package com.hubcity.android.screens;

import java.io.Serializable;

/**
 * Created by subramanya.v on 4/23/2016.
 */
public class BandSummary implements Serializable {
    private String bandName;
    private String bandImgPath;
    private String bandID;
    private String bandURL;
    private String bandURLImgPath;
    private String ribbonAdImagePath;
    private String ribbonAdURL;
    private String contactPhone;
    private String phoneImg;
    private String mail;
    private String mailImg;

    private String pageLink;
    private String pageTitle;
    private String pageImage;
    private String pageId;
    private String retListId;
    private String anythingPageListId;
    private String pageTempLink;
    private int eventExist;

    public int getEventExist() {
        return eventExist;
    }

    public void setEventExist(int eventExist) {
        this.eventExist = eventExist;
    }




    public String getBandID() {
        return bandID;
    }

    public void setBandID(String bandID) {
        this.bandID = bandID;
    }

    public String getAnythingPageListId() {
        return anythingPageListId;
    }

    public void setAnythingPageListId(String anythingPageListId) {
        this.anythingPageListId = anythingPageListId;
    }

    public String getBandName() {
        return bandName;
    }

    public void setBandName(String bandName) {
        this.bandName = bandName;
    }

    public String getBandImgPath() {
        return bandImgPath;
    }

    public void setBandImgPath(String bandImgPath) {
        this.bandImgPath = bandImgPath;
    }

    public String getBandURL() {
        return bandURL;
    }

    public void setBandURL(String bandURL) {
        this.bandURL = bandURL;
    }

    public String getBandURLImgPath() {
        return bandURLImgPath;
    }

    public void setBandURLImgPath(String bandURLImgPath) {
        this.bandURLImgPath = bandURLImgPath;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMailImg() {
        return mailImg;
    }

    public void setMailImg(String mailImg) {
        this.mailImg = mailImg;
    }

    public String getPageImage() {
        return pageImage;
    }

    public void setPageImage(String pageImage) {
        this.pageImage = pageImage;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getPageLink() {
        return pageLink;
    }

    public void setPageLink(String pageLink) {
        this.pageLink = pageLink;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageTempLink() {
        return pageTempLink;
    }

    public void setPageTempLink(String pageTempLink) {
        this.pageTempLink = pageTempLink;
    }

    public String getPhoneImg() {
        return phoneImg;
    }

    public void setPhoneImg(String phoneImg) {
        this.phoneImg = phoneImg;
    }

    public String getRetListId() {
        return retListId;
    }

    public void setRetListId(String retListId) {
        this.retListId = retListId;
    }

    public String getRibbonAdImagePath() {
        return ribbonAdImagePath;
    }

    public void setRibbonAdImagePath(String ribbonAdImagePath) {
        this.ribbonAdImagePath = ribbonAdImagePath;
    }

    public String getRibbonAdURL() {
        return ribbonAdURL;
    }

    public void setRibbonAdURL(String ribbonAdURL) {
        this.ribbonAdURL = ribbonAdURL;
    }

}
