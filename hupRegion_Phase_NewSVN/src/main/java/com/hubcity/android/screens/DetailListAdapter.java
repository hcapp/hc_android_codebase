package com.hubcity.android.screens;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subramanya.v on 4/19/2016.
 */
public class DetailListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<List<Integer>> listValue;
    private Activity mactivity;

    public DetailListAdapter(BandDetailScreen bandDetailScreen, ArrayList<List<Integer>> listValue) {
        mContext = bandDetailScreen;
        this.listValue = listValue;
        mactivity = bandDetailScreen;
    }

    @Override
    public int getCount() {
        return listValue.size();
    }

    @Override
    public Object getItem(int position) {
        return listValue.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;

        if(convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = infalInflater.inflate(
                    R.layout.band_detail_adapter, parent, false);
            holder = new ViewHolder();
            holder.detailName = (TextView) vi.findViewById(R.id.detail_name);
            holder.adapterImage = (ImageView) vi.findViewById(R.id.adapter_image);
            vi.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) vi.getTag();
        }

        String listName = String.valueOf(listValue.get(position).get(0));
        holder.detailName.setText(listName);
        if (listName.equalsIgnoreCase("Events")){
            holder.detailName.setText("Shows");
        }

        new CustomImageLoader(holder.adapterImage, mContext, false).displayImage(String.valueOf(listValue.get(position).get(1)), mactivity, holder.adapterImage);
        return vi;
    }

    private class ViewHolder {
        TextView detailName;
        ImageView adapterImage;
    }
}
