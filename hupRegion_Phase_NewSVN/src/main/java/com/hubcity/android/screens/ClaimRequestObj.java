package com.hubcity.android.screens;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 8/23/2016.
 */
public class ClaimRequestObj
{
	private String responseText;
	private ArrayList<RetDetailList> retDetailList;

	public String getResponseText()
	{
		return responseText;
	}

	public void setResponseText(String responseText)
	{
		this.responseText = responseText;
	}

	public ArrayList<RetDetailList> getRetDetailList()
	{
		return retDetailList;
	}

	public void setRetDetailList(ArrayList<RetDetailList> retDetailList)
	{
		this.retDetailList = retDetailList;
	}


}
