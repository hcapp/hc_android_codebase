package com.hubcity.android.screens;

import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

/**
 * Check if a new of app is available and force updates the app
 *
 * @author rekha_p
 */
public class VersionCheckAsyncTask extends AsyncTask<String, Void, String>
{

	private HashMap<String, String> versionData = new HashMap<>();
	JSONObject jsonObject = null;
	private ProgressDialog mDialog;
	UrlRequestParams urlRequestParams = null;
	ServerConnections mServerConnections = null;
	Activity activity;

	public VersionCheckAsyncTask(Activity actvty)
	{
		activity = actvty;
	}

	@Override
	protected void onPreExecute()
	{
//        mDialog = ProgressDialog.show(activity, "Please Wait..", "Logging In");
//        mDialog.setCancelable(false);
	}

	@Override
	protected String doInBackground(String... params)
	{
		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();
		String result = "false";
		try {

			String version_check_url = Properties.url_local_server
					+ "/ScanSeeVersionCheck/firstUse/versioncheckhubciti";

			String urlParameters = mUrlRequestParams.versionCheckParams();

			jsonObject = mServerConnections.getUrlPostResponse(
					version_check_url, urlParameters, true);

			if (jsonObject != null) {
				jsonObject = jsonObject
						.getJSONObject(CommonConstants.TAG_RESULTSET);

				versionData = new HashMap<>();

				try {
					if (jsonObject.has(CommonConstants.TAG_UPDATEVERSIONTEXT)) {
						versionData
								.put(CommonConstants.TAG_UPDATEVERSIONTEXT,
										jsonObject
												.getString(CommonConstants.TAG_UPDATEVERSIONTEXT));
					}

					if (jsonObject.has(CommonConstants.TAG_LATESTAPPVERURL)) {
						versionData
								.put(CommonConstants.TAG_LATESTAPPVERURL,
										jsonObject
												.getString(CommonConstants.TAG_LATESTAPPVERURL));
					}

					if (jsonObject.has(CommonConstants.TAG_UPDATEFLAG)) {
						versionData
								.put(CommonConstants.TAG_UPDATEFLAG,
										jsonObject
												.getString(CommonConstants.TAG_UPDATEFLAG));
					}

				} catch (Exception e) {
					e.printStackTrace();
					versionData
							.put(CommonConstants.TAG_UPDATEVERSIONTEXT, null);
					versionData.put(CommonConstants.TAG_LATESTAPPVERURL, null);
				}

				result = "true";
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void onPostExecute(String result)
	{
		try {
			mDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			mDialog = null;
		}

		String updateFlag = null;

		if (versionData.get(CommonConstants.TAG_UPDATEFLAG) != null) {
			updateFlag = versionData.get(CommonConstants.TAG_UPDATEFLAG);
		}

		if (updateFlag != null && "true".equalsIgnoreCase(updateFlag)) {
			AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
					activity);
			notificationAlert.setCancelable(false);
			notificationAlert.setMessage(
					versionData.get(CommonConstants.TAG_UPDATEVERSIONTEXT))
					.setPositiveButton(
							activity.getString(R.string.specials_ok),
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int id)
								{
									Intent intent = new Intent(
											Intent.ACTION_VIEW,
											Uri.parse(versionData
													.get(CommonConstants.TAG_LATESTAPPVERURL)));
									activity.startActivity(intent);
									android.os.Process
											.killProcess(android.os.Process
													.myPid());
								}
							});

			notificationAlert.create().show();
		}

//        SharedPreferences setprefs = activity.getSharedPreferences(
//                Constants.PREFERENCE_HUB_CITY, 0);
//        boolean isFirstLaunch = setprefs.getBoolean("isFirstLaunch", true);
//        if (isFirstLaunch) {
//            setprefs.edit().putBoolean("isFirstLaunch", false).apply();
//
//            showPushNotificationAlert(activity);
//
//        }

	}
}