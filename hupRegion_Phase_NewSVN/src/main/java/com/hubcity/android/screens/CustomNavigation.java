package com.hubcity.android.screens;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.HamburgerItemClickNavigation;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CustomExpandableListView;
import com.scansee.newsfirst.DrawerAdpater;
import com.scansee.newsfirst.NavigationItem;
import com.scansee.newsfirst.NewsTemplateDataHolder;
import com.scansee.newsfirst.SideMenuObject;
import com.scansee.newsfirst.SubpageActivty;

import java.util.List;

/**
 * Created by subramanya.v on 8/29/2016.
 */
public class CustomNavigation extends RelativeLayout {
    /*    private NavAdapter navAdapter;*/

    private CustomExpandableListView listView;
    private View view;
    private Context mContext;
    String userStatusText;

    public CustomNavigation(Context context) {
        super(context);
        this.mContext = context;
    }

    public CustomNavigation(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.news_template_drawer_expandable_listview, this, true);
        listView = (CustomExpandableListView) view.findViewById(R.id.expandableListView);

    }

    public CustomNavigation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void populateSideMenu(final SideMenuObject sideMenuObject, final List<NavigationItem> sideMenuList, final DrawerLayout drawer,
                                 final int hubCitiPos) {
        (view.findViewById(R.id.progressBar)).setVisibility(View.GONE);

        listView.setAdapter(new DrawerAdpater(mContext, sideMenuList));
        listView.setVisibility(View.VISIBLE);

        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                                             @Override
                                             public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                                                 drawer.closeDrawer(Gravity.LEFT);
                                                 new HamburgerItemClickNavigation(mContext, sideMenuObject, hubCitiPos).linkClickListener(sideMenuList.get(groupPosition));

                                                 return true;
                                             }
                                         }

        );
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener()

                                         {
                                             @Override
                                             public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                                                         int childPosition, long id) {
//
                                                 String sideMenuCat = sideMenuList.get(groupPosition)
                                                         .getMainCatList().get
                                                                 (childPosition).getSubCatName();
                                                 //if news template
                                                 if (NewsTemplateDataHolder.className != null && NewsTemplateDataHolder.className.equalsIgnoreCase("Scrolling News Template")) {

                                                     gotoSubpage(sideMenuList,sideMenuCat);

                                                     if (drawer.isDrawerOpen(Gravity.LEFT)) {
                                                         drawer.closeDrawer(Gravity.LEFT);
                                                     } else {
                                                         drawer.openDrawer(Gravity.LEFT);
                                                     }
                                                 } else {
                                                     gotoSubpage(sideMenuList, sideMenuCat);
                                                 }
                                                 return false;
                                             }
                                         }

        );
    }

    private void gotoSubpage(List<NavigationItem> sideMenuList, String sideMenuCat) {
        Intent scroling = new Intent(mContext, SubpageActivty
                .class);
        scroling.putExtra("sideMenuCat", sideMenuCat);
        scroling.putExtra("templateType", "Subpage");
        scroling.putExtra("isSideBar", "3");
        scroling.putExtra(Constants.LEVEL, CommonConstants.previousMenuLevel);
        scroling.putExtra(Constants.LINKID, CommonConstants.commonLinkId);
        scroling.putExtra("catTxtColor", sideMenuList.get(0).getCatTxtColor());
        mContext.startActivity(scroling);
    }
}
