package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.ImageLoaderAsync;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.RetailerProductMediaActivity;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.android.ScanSeePdfViewerActivity;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

@SuppressWarnings("WeakerAccess")
public class RetailerActivity extends CustomTitleBar implements OnClickListener
{

	private String retailLocationID;
	private String retailerId;
	private String retailerName;
	private String retailerMile;
	private String retailerBanner;
	private String retailerAd;
	private String retailerAdURL;
	private String retListId;
	private ArrayList<HashMap<String, String>> retalerInfoList = new ArrayList<>();
	private TextView retailerNameTextView;
	private TextView retailerMileTextView;
	private TextView retailerMileTextView2;
	private ImageView retailerADImageView;
	private ListView retailerInfoListView;
	private boolean fromaustin = false;
	private String latitude;
	private String longitude;
	private String reqLatitude;
	private String reqLongitude;
	private String accZipcode;
	private String retDetaiid;
	private final Context context = this;
	private LocationManager locationManager;
	private AlertDialog.Builder specialsAlert;
	private RelativeLayout splashFrame;
	private RelativeLayout retailerAdLayout;
	private FrameLayout mainLayout;
	private LinearLayout retailerNameLayout;
	private boolean fromretpartners = false;
	private boolean saleFlag = false;
	private boolean gpsState = false;
	private boolean fromsearch = false;
	private boolean fromscannow = false;
	private String mainMenuId;
	private String scanTypeId = "0";
	private String isEventExists = "0";
	private String isFundExists = "0";
	private String mLinkId;
	private String mItemId;
	private String mBottomBtnId;
	private String appsiteId;
	private String retDetailsId;
	private String anythingPageId;
	private String anythingPageClkStrng;

	private ShareInformation shareInfo;
	private boolean isShareTaskCalled;
	private Activity activity;
	private TextView claimBusiness;
	private String claimTxtMsg = "";
	private String claimExist;
	private String userMailId;
	private String retaileraddress1;
	private boolean isItemClicked = false;
	private String response;

	@Override
	protected void onResume()
	{
		super.onResume();
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		List<ApplicationInfo> packages;
		PackageManager pm;
		pm = getPackageManager();
		// get a list of installed apps.
		packages = pm.getInstalledApplications(0);

		ActivityManager mActivityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);

		for (ApplicationInfo packageInfo : packages) {
			if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1
					|| packageInfo.packageName.contains("com.adobe.reader")) {
				continue;
			}

			mActivityManager.killBackgroundProcesses(packageInfo.packageName);
		}
		if (isItemClicked) {
			CommonConstants.BR_TAG = true;
			claimTxtMsg = "";
			checkProvider();
			isItemClicked = false;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.retailer_main_page);

		try {
			activity = RetailerActivity.this;
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			CommonConstants.BR_TAG = true;
			setTitleView();
			getIntentData();
			bindView();
			setVisibility();

			if (null == retailerAd) {
				retailerAd = "N/A";
			}
			if (null == retailerBanner) {
				retailerBanner = "N/A";
			}
			retailerAdLayout.setOnClickListener(this);
			setClickListener();
			checkProvider();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void checkProvider()
	{
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		String provider = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (provider.contains("gps")) {
			getGpsValue();
			gpsState = true;
			new GetRetailerInfo().execute();
		} else if (fromsearch) {

			gpsState = false;
			new GetRetailerInfo().execute();
		} else if (fromaustin) {

			new GetRetailerInfo().execute();
		} else if (fromretpartners) {

			new GetRetailerInfo().execute();
		} else if (fromscannow) {
			new GetZipcode().execute();

		} else {
			gpsState = false;
			new GetRetailerInfo().execute();
		}
	}

	private void getGpsValue()
	{
		locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER,
				CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
				CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
				new ScanSeeLocListener());
		Location locNew = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		if (locNew != null) {

			reqLatitude = String.valueOf(locNew.getLatitude());
			reqLongitude = String.valueOf(locNew.getLongitude());

		} else {
			// N/W Tower Info Start
			locNew = locationManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (locNew != null) {
				reqLatitude = String.valueOf(locNew.getLatitude());
				reqLongitude = String.valueOf(locNew.getLongitude());
			}

		}
	}

	private void setClickListener()
	{

		leftTitleImage.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				//isItemClicked = true;
				if (!Constants.GuestLoginId.equals(UrlRequestParams
						.getUid().trim())) {
					shareInfo.shareTask(isShareTaskCalled);
					isShareTaskCalled = true;
				} else {
					Constants.SignUpAlert(RetailerActivity.this);
					isShareTaskCalled = false;
				}
			}
		});

		claimBusiness.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				isItemClicked = true;
				if (Constants.GuestLoginId
						.equals(UrlRequestParams.getUid().trim())) {
					Constants.SignUpAlert(activity);
				} else {
					Intent claimScreen = new Intent(RetailerActivity.this, ClaimBusinessActivity
							.class);
					claimScreen.putExtra("loginEmail", userMailId);
					claimScreen.putExtra("retailLocationID", retailLocationID);
					claimScreen.putExtra("retailerName", retailerName);
					claimScreen.putExtra("retaileraddress1", retaileraddress1);
					startActivity(claimScreen);
				}
			}
		});
		retailerInfoListView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long pst)
			{
				//isItemClicked = true;
				if (retalerInfoList.get(position).get(
						CommonConstants.TAG_PAGE_LINK) != null) {
					Intent retailerLink;
					String tempLink = retalerInfoList.get(position).get(
							CommonConstants.TAG_PAGE_LINK);
					if ("specials".equalsIgnoreCase(tempLink)) {
						if ("See Current Deals!"
								.equalsIgnoreCase(retalerInfoList.get(position)
										.get(CommonConstants.TAG_PAGE_TITLE))) {
							retailerLink = new Intent(RetailerActivity.this,
									CurrentSpecialsListActivity.class);
							retailerLink.putExtra(
									CommonConstants.TAG_RETAILE_LOCATIONID,
									retailLocationID);
							retailerLink.putExtra(
									CommonConstants.TAG_RETAIL_LIST_ID,
									retListId);
							retailerLink.putExtra(Constants.MAIN_MENU_ID,
									mainMenuId);
							retailerLink.putExtra(
									CommonConstants.TAG_RETAILE_ID, retailerId);

							retailerLink.putExtra(
									CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
									retailerAd);
							retailerLink.putExtra("saleFlag", saleFlag);
							retailerLink.putExtra(
									CommonConstants.TAG_RIBBON_AD_URL,
									retailerAdURL);
							retailerLink.putExtra(CommonConstants.LINK_ID,
									mLinkId);
							retailerLink.putExtra("specialitemName", "coupons");
							retailerLink.putExtra(
									CommonConstants.TAG_RETAILER_NAME,
									retailerName);
							startActivity(retailerLink);

						} else {

							specialsAlert = new AlertDialog.Builder(
									RetailerActivity.this);
							specialsAlert
									.setMessage(
											getString(R.string.specials_pop))
									.setCancelable(true)
									.setPositiveButton(
											getString(R.string.specials_ok),
											new DialogInterface.OnClickListener()
											{
												@Override
												public void onClick(
														DialogInterface dialog,
														int id)
												{
												}
											})
									.setOnCancelListener(
											new OnCancelListener()
											{

												@Override
												public void onCancel(
														DialogInterface dialog)
												{
													finish();
												}
											});
							specialsAlert.create().show();

						}

					} else if ("directions".equalsIgnoreCase(tempLink)) {

						MapDirections mapDirections = new MapDirections(
								locationManager, activity, retalerInfoList.get(
								position).get(
								CommonConstants.TAG_PAGE_INFO),
								latitude, longitude, retailerName);

						mapDirections.getMapDirections();

					} else if ("call".equalsIgnoreCase(tempLink)) {
						try {
							retailerLink = new Intent(Intent.ACTION_DIAL);
							retDetailsId = retalerInfoList.get(position).get(
									CommonConstants.TAG_RET_DETAILS_ID);
							anythingPageId = retalerInfoList.get(position).get(
									CommonConstants.TAG_ANYTHING_PAGE_LIST_ID);
							retailerLink.setData(Uri.parse("tel:"
									+ retalerInfoList.get(position).get(
									CommonConstants.TAG_PAGE_INFO)));
							retailerLink.putExtra(CommonConstants.LINK_ID,
									mLinkId);
							anythingPageClkStrng = "call";
							new SendAnythingPageId().execute(retDetailsId,
									anythingPageId, anythingPageClkStrng);
							startActivity(retailerLink);
						} catch (Exception e) {
							e.printStackTrace();
							new AlertDialog.Builder(RetailerActivity.this)
									.setTitle("Alert")
									.setMessage("Call feature not available.!")
									.setPositiveButton("OK", null).show();
						}
					} else if ("browser".equalsIgnoreCase(tempLink)) {
						retailerLink = new Intent(RetailerActivity.this,
								ScanseeBrowserActivity.class);

						retDetailsId = retalerInfoList.get(position).get(
								CommonConstants.TAG_RET_DETAILS_ID);
						anythingPageId = retalerInfoList.get(position).get(
								CommonConstants.TAG_ANYTHING_PAGE_LIST_ID);

						retailerLink.putExtra(
								CommonConstants.URL,
								retalerInfoList.get(position).get(
										CommonConstants.TAG_PAGE_INFO));
						retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE,
								retailerName);
						retailerLink.putExtra(CommonConstants.LINK_ID, mLinkId);
						anythingPageClkStrng = "browser";
						new SendAnythingPageId().execute(retDetailsId,
								anythingPageId, anythingPageClkStrng);

						startActivityForResult(retailerLink,
								Constants.STARTVALUE);
					} else if (tempLink.endsWith(".pdf")) {
						retailerLink = new Intent(RetailerActivity.this,
								ScanSeePdfViewerActivity.class);

						retDetailsId = retalerInfoList.get(position).get(
								CommonConstants.TAG_RET_DETAILS_ID);
						anythingPageId = retalerInfoList.get(position).get(
								CommonConstants.TAG_ANYTHING_PAGE_LIST_ID);
						anythingPageClkStrng = "anypage";
						retailerLink.putExtra(
								CommonConstants.URL,
								retalerInfoList.get(position).get(
										CommonConstants.TAG_PAGE_TEMP_LINK));
						retailerLink
								.putExtra(
										RetailerProductMediaActivity
												.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
										tempLink);
						retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE,
								retailerName);
						retailerLink.putExtra(CommonConstants.LINK_ID, mLinkId);
						new SendAnythingPageId().execute(retDetailsId,
								anythingPageId, anythingPageClkStrng);

						retailerLink.putExtra("isShare", true);
						retailerLink.putExtra("module", "Anything");
						retailerLink.putExtra("retailerId", retailerId);
						retailerLink.putExtra("pageId",
								retalerInfoList.get(position).get("pageId"));

						startActivityForResult(retailerLink,
								Constants.STARTVALUE);

					} else {

						// @Rekha: Calling events on tap of Events in Retailer
						// summary page
						String index = retalerInfoList.get(position).get(
								"pageCount");

						// If it is the 1st item in <RetailerCreatedPages>
						// and Events flag is true(1)
						// Event screen is called
						if ("1".equals(index) && "1".equals(isEventExists)) {
							Intent intent = new Intent(RetailerActivity.this,
									EventsListDisplay.class);
							intent.putExtra("retailerId", retailerId);
							intent.putExtra("retListId", retListId);
							intent.putExtra("retailLocationId",
									retailLocationID);
							intent.putExtra("retailerEvents", "true");
							intent.putExtra(
									Constants.MENU_ITEM_ID_INTENT_EXTRA,
									mItemId);
							intent.putExtra(
									Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
									mBottomBtnId);
							intent.putExtra(CommonConstants.LINK_ID, mLinkId);
							startActivity(intent);

						}

						// If it is the 1st item in <RetailerCreatedPages>
						// and Fund flag is true(1) or
						// If it is the 2st item in <RetailerCreatedPages>
						// and both Events flag and Fund Flag are true(1)
						// Fundraiser screen is called
						else if (("1".equals(index) && "1".equals(isFundExists))
								|| ("2".equals(index)
								&& "1".equals(isFundExists) && "1"
								.equals(isEventExists))) {

							Intent intent = new Intent(RetailerActivity.this,
									FundraiserActivity.class);
							intent.putExtra("retailerId", retailerId);
							intent.putExtra("retListId", retListId);
							intent.putExtra("retailLocationId",
									retailLocationID);
							intent.putExtra("retailerEvents", "true");
							intent.putExtra("mLinkId", mLinkId);
							intent.putExtra(
									Constants.MENU_ITEM_ID_INTENT_EXTRA,
									getIntent()
											.getExtras()
											.getString(
													Constants.MENU_ITEM_ID_INTENT_EXTRA));
							intent.putExtra(
									Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
									getIntent()
											.getExtras()
											.getString(
													Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
							startActivity(intent);

						}
						// If both Fund Flag and Events flag are false(0)
						// Anything page is called
						else {
							callAnythingPage(position);

						}

					}

				}

			}

		});
	}

	private void setVisibility()
	{
		mainLayout.setVisibility(View.INVISIBLE);
		leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
		if (retailerAd != null && "N/A".equalsIgnoreCase(retailerAd)) {
			retailerADImageView.setVisibility(View.GONE);
			retailerAdLayout.setVisibility(View.GONE);
			retailerNameLayout.setVisibility(View.VISIBLE);

		} else {

			retailerADImageView.setVisibility(View.VISIBLE);
			retailerAdLayout.setVisibility(View.VISIBLE);
			retailerNameLayout.setVisibility(View.GONE);

		}

		splashFrame.setVisibility(View.INVISIBLE);
	}

	private void bindView()
	{
		claimBusiness = (TextView) findViewById(R.id.claim_business);
		mainLayout = (FrameLayout) findViewById(R.id.main_layout);
		retailerAdLayout = (RelativeLayout) findViewById(R.id.retailer_ad_layout);
		splashFrame = (RelativeLayout) findViewById(R.id.retailer_splash_frame);
		retailerADImageView = (ImageView) findViewById(R.id.retailer_details_image);
		retailerAdLayout = (RelativeLayout) findViewById(R.id.retailer_ad_layout);
		retailerNameLayout = (LinearLayout) findViewById(R.id.retailer_details_layout);
		retailerNameTextView = (TextView) findViewById(R.id.retailer_details_name);
		retailerMileTextView = (TextView) findViewById(R.id.retailer_details_miles);
		retailerMileTextView2 = (TextView) findViewById(R.id.retailer_details_miles2);
		retailerInfoListView = (ListView) findViewById(R.id.retailer_listview);
	}

	private void setTitleView()
	{
		// Added By-Dileep for displaying title bar
		title.setSingleLine(false);
		title.setMaxLines(2);
		title.setEllipsize(TextUtils.TruncateAt.END);
	}

	private void getIntentData()
	{
		accZipcode = Constants.getZipCode();
		if (getIntent().getExtras()
				.getString(CommonConstants.TAG_RETAILER_NAME) != null) {
			title.setText(getIntent().getExtras().getString(
					CommonConstants.TAG_RETAILER_NAME));

		} else if (getIntent().getExtras()
				.getString(Constants.TAG_APPSITE_NAME) != null) {
			title.setText(getIntent().getExtras().getString(
					Constants.TAG_APPSITE_NAME));
		}
		if (getIntent().getExtras().getString("appSiteId") != null) {
			appsiteId = getIntent().getExtras().getString("appSiteId");
		}
		if (getIntent().getExtras().getString(CommonConstants.TAG_RETAIL_ID) != null) {
			retailerId = getIntent().getExtras().getString(
					CommonConstants.TAG_RETAIL_ID);
		}
		if (getIntent().getExtras().getString(
				CommonConstants.TAG_RETAIL_LIST_ID) != null) {
			retListId = getIntent().getExtras().getString(
					CommonConstants.TAG_RETAIL_LIST_ID);
		}
		if (getIntent().getExtras().getString(Constants.TAG_SCAN_TYPE_ID) != null) {
			scanTypeId = getIntent().getExtras().getString(
					Constants.TAG_SCAN_TYPE_ID);
		}
		if (getIntent().getExtras().getString(Constants.TAG_RETAILE_LOCATIONID) != null) {
			retailLocationID = getIntent().getExtras().getString(
					Constants.TAG_RETAILE_LOCATIONID);
		}

		if (getIntent().getExtras().getString(CommonConstants.TAG_DISTANCE) != null) {
			retailerMile = getIntent().getExtras().getString(
					CommonConstants.TAG_DISTANCE);
		}
		if (getIntent().hasExtra("mLinkId")) {
			mLinkId = getIntent().getExtras().getString("mLinkId");

		}
		if (getIntent().hasExtra("mLinkId")) {
			mLinkId = getIntent().getExtras().getString("mLinkId");

		}
		if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
			mItemId = getIntent().getExtras().getString(
					Constants.MENU_ITEM_ID_INTENT_EXTRA);

		}

		if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
			mBottomBtnId = getIntent().getExtras().getString(
					Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);

		}

		if (getIntent().getExtras().getString(Constants.MAIN_MENU_ID) != null) {
			mainMenuId = getIntent().getExtras().getString(
					Constants.MAIN_MENU_ID);
		}


		if (getIntent().hasExtra("fromsearch")) {
			fromsearch = getIntent().getExtras().getBoolean("fromsearch");
		}
		if (getIntent().hasExtra("fromAustinexpRetailerPartners")) {
			fromretpartners = getIntent().getExtras().getBoolean(
					"fromAustinexpRetailerPartners");
		}
		if (getIntent().hasExtra("fromAustinexp")) {
			fromaustin = getIntent().getExtras().getBoolean("fromAustinexp");
		}
		if (getIntent().hasExtra("ScanNow")) {
			fromscannow = getIntent().getExtras().getBoolean("ScanNow");
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText(R.string.title_scan_now);

			leftTitleImage.setVisibility(View.GONE);
		}

		if (getIntent().hasExtra(CommonConstants.TAG_RETAILE_LOCATIONID)) {
			retailLocationID = getIntent().getExtras().getString(
					CommonConstants.TAG_RETAILE_LOCATIONID);
		}

		if (getIntent().hasExtra(CommonConstants.TAG_BANNER_IMAGE_PATH)) {
			retailerBanner = getIntent().getExtras().getString(
					CommonConstants.TAG_BANNER_IMAGE_PATH);
		}

		if (getIntent().hasExtra(CommonConstants.TAG_RIBBON_ADIMAGE_PATH)) {
			retailerAd = getIntent().getExtras().getString(
					CommonConstants.TAG_RIBBON_ADIMAGE_PATH);
		}

		if (getIntent().hasExtra(CommonConstants.TAG_RIBBON_AD_URL)) {
			retailerAdURL = getIntent().getExtras().getString(
					CommonConstants.TAG_RIBBON_AD_URL);
		}

	}

	private void callAnythingPage(int position)
	{
		String url = retalerInfoList.get(position).get(
				CommonConstants.TAG_PAGE_TEMP_LINK);

		Intent retailerLink = new Intent(RetailerActivity.this,
				ScanseeBrowserActivity.class);

		if (url.contains("/3000")) {
			retailerLink.putExtra(CommonConstants.URL, url);
			retailerLink.putExtra("isEventLogistics", true);

		} else {

			anythingPageClkStrng = "anypage";
			retDetailsId = retalerInfoList.get(position).get(
					CommonConstants.TAG_RET_DETAILS_ID);
			Log.v("",
					"RETAILER DETAILS ID : "
							+ retalerInfoList.get(position).get(
							CommonConstants.TAG_DETAILS_ID));
			anythingPageId = retalerInfoList.get(position).get(
					CommonConstants.TAG_ANYTHING_PAGE_LIST_ID);

			retailerLink.putExtra(CommonConstants.URL, url);
			retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE, "Details");
			retailerLink.putExtra(
					CommonConstants.TAG_ANYTHING_PAGE_LIST_ID,
					retalerInfoList.get(position).get(
							CommonConstants.TAG_ANYTHING_PAGE_LIST_ID));
			retailerLink.putExtra(CommonConstants.LINK_ID, mLinkId);
			retailerLink.putExtra("isShare", true);
			retailerLink.putExtra("retailerId", retailerId);
			retailerLink.putExtra("pageId",
					retalerInfoList.get(position).get("pageId"));
			new SendAnythingPageId().execute(retDetailsId, anythingPageId,
					anythingPageClkStrng);
		}
		startActivityForResult(retailerLink, Constants.STARTVALUE);

	}

	@Override
	public void onDestroy()
	{
		retailerInfoListView.setAdapter(null);
		super.onDestroy();
	}

	private class SendAnythingPageId extends AsyncTask<String, Void, String>
	{
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute()
		{
			mDialog = ProgressDialog.show(RetailerActivity.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		// u try to do this 2screens
		@Override
		protected String doInBackground(String... params)
		{

			String result;
			try {
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();
				retDetailsId = retDetaiid;
				Log.v("", "ID FOR ANYTHING PAGE : " + retDetailsId + " , "
						+ retListId + " , " + anythingPageId + " , "
						+ anythingPageClkStrng);

				String get_retail_item_click = Properties.url_local_server
						+ Properties.hubciti_version
						+ "thislocation/utretsumclick";

				String urlParameters = mUrlRequestParams
						.createUserTrackngAnythngPageParamater(retDetailsId,
								retListId, anythingPageId, anythingPageClkStrng);

				jsonObject = mServerConnections.getUrlPostResponse(
						get_retail_item_click, urlParameters, true);

				if (jsonObject != null) {
					jsonObject = jsonObject.getJSONObject("UserTrackingData");
					if (jsonObject.getString("postalCode") != null) {
						accZipcode = Constants.getZipCode();
					} else {
						accZipcode = Constants.getZipCode();
					}
				}
				result = "true";
			} catch (Exception e) {
				e.printStackTrace();
				result = "true";
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result)
		{
			try {
				mDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	private class GetZipcode extends AsyncTask<String, Void, String>
	{
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute()
		{
			mDialog = ProgressDialog.show(RetailerActivity.this, "",
					"Fetching Location.. Please Wait.. ", true);
			mDialog.setCancelable(false);
		}

		// u try to do this 2screens
		@Override
		protected String doInBackground(String... params)
		{

			String result;
			try {
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();
				String fetchuserlocationpoints = Properties.url_local_server
						+ Properties.hubciti_version
						+ "thislocation/fetchuserlocationpoints?userId=";
				String urlParameters = mUrlRequestParams.getUserZipcode();
				jsonObject = mServerConnections.getJSONFromUrl(false,
						fetchuserlocationpoints + urlParameters, null);
				if (jsonObject != null) {
					jsonObject = jsonObject.getJSONObject("UserLocationPoints");
					if (jsonObject.getString("postalCode") != null) {
						accZipcode = Constants.getZipCode();
					} else {
						accZipcode = Constants.getZipCode();
					}
				}
				result = "true";
			} catch (Exception e) {
				e.printStackTrace();
				result = "true";
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result)
		{
			try {
				mDialog.dismiss();
				new GetRetailerInfo().execute();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	private class GetRetailerInfo extends AsyncTask<String, Void, String>
	{
		JSONObject jsonObject, reatailerDetail, retailerPagesObj;
		JSONArray retailerPages;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute()
		{
			try {
				if (splashFrame.getVisibility() == View.INVISIBLE) {

					if (mDialog != null) {
						if (mDialog.isShowing()) {
							mDialog.dismiss();
						}

						mDialog = null;
					}
					mDialog = ProgressDialog.show(RetailerActivity.this, "",
							Constants.DIALOG_MESSAGE, true);
					mDialog.setCancelable(false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		final UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		final ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected String doInBackground(String... params)
		{

			String result = "false";
			try {
				retalerInfoList = new ArrayList<>();
				if (gpsState) {
					String urlParameters = mUrlRequestParams
							.createGetRetailerParameter(retailerId,
									retailLocationID, retListId, mainMenuId,
									scanTypeId, accZipcode, reqLatitude, reqLongitude,
									appsiteId);

					String url_citi_expr_retailer_detail = Properties.url_local_server
							+ Properties.hubciti_version
							+ "thislocation/retsummary";

					jsonObject = mServerConnections.getUrlPostResponse(
							url_citi_expr_retailer_detail, urlParameters, true);
				} else {
					String urlParameters = mUrlRequestParams
							.createGetRetailerParameter(retailerId,
									retailLocationID, retListId, mainMenuId,
									scanTypeId, accZipcode, null, null,
									appsiteId);

					String url_citi_expr_retailer_detail = Properties.url_local_server
							+ Properties.hubciti_version
							+ "thislocation/retsummary";

					jsonObject = mServerConnections.getUrlPostResponse(
							url_citi_expr_retailer_detail, urlParameters, true);
				}
				if (jsonObject != null) {
					try {

						if (jsonObject.getJSONObject("RetailersDetails").has(
								"eventExist")) {
							isEventExists = jsonObject.getJSONObject(
									"RetailersDetails").getString("eventExist");
						}

						if (jsonObject.getJSONObject("RetailersDetails").has(
								"fundExist")) {
							isFundExists = jsonObject.getJSONObject(
									"RetailersDetails").getString("fundExist");
						}

						if (jsonObject.has("RetailersDetails")) {
							JSONObject retailerDetails = jsonObject.getJSONObject
									("RetailersDetails");
							try {
								JSONObject claimBusiness = retailerDetails.getJSONObject
										("claimTxtMsg");
								JSONArray claimArray = claimBusiness.getJSONArray("content");
								for (int i = 0; i < claimArray.length(); i++) {
									claimTxtMsg = claimTxtMsg + claimArray.get(i).toString() +
											" \n ";
								}

								if (claimTxtMsg.endsWith("\n ")) {
									claimTxtMsg = claimTxtMsg.substring(0, claimTxtMsg.length() -
											2);
								}
								claimTxtMsg.trim();
							} catch (JSONException e) {
								e.printStackTrace();
								try {
									if (retailerDetails.has("claimTxtMsg")) {
                                        claimTxtMsg = retailerDetails.getJSONObject("claimTxtMsg").optString("content");

                                        if (claimTxtMsg.endsWith("\n ")) {
                                            claimTxtMsg = claimTxtMsg.substring(0, claimTxtMsg.length() -
                                                    2);
                                        }
                                        claimTxtMsg.trim();
                                    }
								} catch (JSONException e1) {
									e1.printStackTrace();
								}
							}
							if (retailerDetails.has("claimExist")) {
								claimExist = retailerDetails.optString("claimExist");
							}
							if (retailerDetails.has("userMailId")) {
								userMailId = retailerDetails.optString("userMailId");
							}

						}

						try {
							reatailerDetail = jsonObject
									.getJSONObject("RetailersDetails")
									.getJSONObject("retailerDetail")
									.getJSONObject("RetailerDetail");
						} catch (JSONException e) {
							JSONArray retailerArray = jsonObject
									.getJSONObject("RetailersDetails")
									.getJSONObject("retailerDetail")
									.getJSONArray("RetailerDetail");

							reatailerDetail = retailerArray.getJSONObject(0);
							e.printStackTrace();
						}
						if (reatailerDetail.has("retailerName")) {
							retailerName = reatailerDetail
									.getString("retailerName");
						}
						if (reatailerDetail.has("retaileraddress1")) {
							retaileraddress1 = reatailerDetail
									.getString("retaileraddress1");
						}
						if (reatailerDetail.has("retailerId")) {
							retailerId = reatailerDetail
									.getString("retailerId");
						}

						if (reatailerDetail.has("bannerAdImagePath")) {
							retailerBanner = reatailerDetail
									.getString("bannerAdImagePath");
						}

						if (reatailerDetail.has("retailLocationId")) {
							retailLocationID = reatailerDetail
									.getString("retailLocationId");
						}

						if (jsonObject.getJSONObject("RetailersDetails").has(
								"retailerCreatedPageList")) {
							retailerPages = jsonObject
									.getJSONObject("RetailersDetails")
									.getJSONObject("retailerCreatedPageList")
									.optJSONArray("RetailerCreatedPages");

							if (null != retailerPages) {

								retailerPages = jsonObject
										.getJSONObject("RetailersDetails")
										.getJSONObject(
												"retailerCreatedPageList")
										.getJSONArray("RetailerCreatedPages");
							} else {
								retailerPagesObj = jsonObject
										.getJSONObject("RetailersDetails")
										.getJSONObject(
												"retailerCreatedPageList")
										.getJSONObject("RetailerCreatedPages");
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					retailerAd = reatailerDetail
							.getString(CommonConstants.TAG_RIBBON_ADIMAGE_PATH);
					if (!("N/A".equals(reatailerDetail
							.getString(CommonConstants.TAG_DISTANCE)))) {
						retailerMile = reatailerDetail
								.getString(CommonConstants.TAG_DISTANCE);
					}
					retDetaiid = reatailerDetail
							.getString(CommonConstants.TAG_DETAILS_ID);
					latitude = reatailerDetail
							.getString(CommonConstants.TAG_FINDLOCATION_SCANSEELAT);
					longitude = reatailerDetail
							.getString(CommonConstants.TAG_FINDLOCATION_SCANSEELON);
					saleFlag = reatailerDetail
							.getBoolean(CommonConstants.TAG_SALE_FLAG);
					// adding specials
					HashMap<String, String> retailerData = new HashMap<>();
					retailerData.put(CommonConstants.TAG_PAGE_LINK, "specials");
					retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
					if (reatailerDetail.has("pageId")) {
						retailerData.put("pageId",
								reatailerDetail.getString("pageId"));
					}
					if (reatailerDetail
							.getBoolean(CommonConstants.TAG_SALE_FLAG)) {
						retailerData.put(CommonConstants.TAG_PAGE_TITLE,
								"See Current Deals!");
						retalerInfoList.add(retailerData);
					}
					// adding directions data
					if ((reatailerDetail.has(CommonConstants.TAG_ADDRESS1))
							&& !"N/A".equalsIgnoreCase(reatailerDetail
							.getString(CommonConstants.TAG_ADDRESS1))) {
						retailerData = new HashMap<>();
						retailerData.put(CommonConstants.TAG_PAGE_LINK,
								"directions");
						retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
						retailerData.put(CommonConstants.TAG_PAGE_TITLE,
								"Get Directions");
						retailerData
								.put(CommonConstants.TAG_PAGE_INFO,
										reatailerDetail
												.getString(CommonConstants.TAG_ADDRESS1));

						// Added By Dileep
						retailerData
								.put(CommonConstants.TAG_RET_LATITIUDE,
										reatailerDetail
												.getString(CommonConstants
														.TAG_FINDLOCATION_SCANSEELAT));
						retailerData
								.put(CommonConstants.TAG_RET_LONGITUDE,
										reatailerDetail
												.getString(CommonConstants
														.TAG_FINDLOCATION_SCANSEELON));

						retalerInfoList.add(retailerData);
					}

					// adding call options
					if ((reatailerDetail.has(CommonConstants.TAG_PHONE))
							&& !"N/A".equalsIgnoreCase(reatailerDetail
							.getString(CommonConstants.TAG_PHONE))) {
						retailerData = new HashMap<>();
						retailerData.put(CommonConstants.TAG_PAGE_LINK, "call");
						retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
						retailerData.put(CommonConstants.TAG_PAGE_TITLE,
								"Call Location");
						retailerData.put(CommonConstants.TAG_PAGE_INFO,
								reatailerDetail
										.getString(CommonConstants.TAG_PHONE));
						retalerInfoList.add(retailerData);
					}

					// adding browser options
					if ((reatailerDetail.has(CommonConstants.TAG_RETAILER_URL))
							&& !"N/A"
							.equalsIgnoreCase(reatailerDetail
									.getString(CommonConstants.TAG_RETAILER_URL))) {
						retailerData = new HashMap<>();
						retailerData.put(CommonConstants.TAG_PAGE_LINK,
								"browser");
						retailerData.put(CommonConstants.TAG_PAGE_IMAGE, null);
						retailerData.put(CommonConstants.TAG_PAGE_TITLE,
								"Browse Website");
						retailerData
								.put(CommonConstants.TAG_PAGE_INFO,
										reatailerDetail
												.getString(CommonConstants.TAG_RETAILER_URL));
						retalerInfoList.add(retailerData);
					}

					// adding other list items
					if (retailerPages != null) {
						for (int pageCount = 0; pageCount < retailerPages
								.length(); pageCount++) {
							retailerData = new HashMap<>();
							if (retailerPages.getJSONObject(pageCount).has(
									"pageId")) {
								retailerData.put("pageId",
										retailerPages.getJSONObject(pageCount)
												.getString("pageId"));
							}

							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_PAGE_TEMP_LINK)) {
								retailerData
										.put(CommonConstants.TAG_PAGE_LINK,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants
																		.TAG_PAGE_TEMP_LINK));
							}
							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_PAGE_TITLE)) {
								retailerData
										.put(CommonConstants.TAG_PAGE_TITLE,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants.TAG_PAGE_TITLE));
							}

							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_PAGE_IMAGE)) {
								retailerData
										.put(CommonConstants.TAG_PAGE_IMAGE,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants.TAG_PAGE_IMAGE));
							}
							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_PAGE_LINK)) {
								retailerData
										.put(CommonConstants.TAG_PAGE_TEMP_LINK,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants.TAG_PAGE_LINK));
							}
							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_ANYTHING_PAGE_LIST_ID)) {
								retailerData
										.put(CommonConstants.TAG_ANYTHING_PAGE_LIST_ID,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants
																		.TAG_ANYTHING_PAGE_LIST_ID));
							}

							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_ANYTHING_PAGE_CLICK)) {
								boolean anythingPageClickBool = retailerPages
										.getJSONObject(pageCount)
										.getBoolean(
												CommonConstants.TAG_ANYTHING_PAGE_CLICK);
								anythingPageClkStrng = String
										.valueOf(anythingPageClickBool);
								retailerData
										.put(CommonConstants.TAG_ANYTHING_PAGE_CLICK,
												anythingPageClkStrng);
							}

							if (retailerPages.getJSONObject(pageCount).has(
									CommonConstants.TAG_RET_DETAILS_ID)) {
								retailerData
										.put(CommonConstants.TAG_RET_DETAILS_ID,
												retailerPages
														.getJSONObject(
																pageCount)
														.getString(
																CommonConstants
																		.TAG_RET_DETAILS_ID));
							}

							retailerData.put("RetailerCreatedPages", "1");
							retailerData.put("pageCount",
									String.valueOf(pageCount + 1));

							retalerInfoList.add(retailerData);
						}
					} else if (null != retailerPagesObj) {
						retailerData = new HashMap<>();
						if (retailerPagesObj.has("pageId")) {
							retailerData.put("pageId",
									retailerPagesObj.getString("pageId"));
						}

						if (retailerPagesObj
								.has(CommonConstants.TAG_PAGE_TEMP_LINK)) {
							retailerData
									.put(CommonConstants.TAG_PAGE_LINK,
											retailerPagesObj
													.getString(CommonConstants
															.TAG_PAGE_TEMP_LINK));
						}
						if (retailerPagesObj
								.has(CommonConstants.TAG_PAGE_TITLE)) {
							retailerData
									.put(CommonConstants.TAG_PAGE_TITLE,
											retailerPagesObj
													.getString(CommonConstants.TAG_PAGE_TITLE));
						}

						if (retailerPagesObj
								.has(CommonConstants.TAG_PAGE_IMAGE)) {
							retailerData
									.put(CommonConstants.TAG_PAGE_IMAGE,
											retailerPagesObj
													.getString(CommonConstants.TAG_PAGE_IMAGE));
						}
						if (retailerPagesObj.has(CommonConstants.TAG_PAGE_LINK)) {
							retailerData
									.put(CommonConstants.TAG_PAGE_TEMP_LINK,
											retailerPagesObj
													.getString(CommonConstants.TAG_PAGE_LINK));
						}

						if (retailerPagesObj
								.has(CommonConstants.TAG_ANYTHING_PAGE_LIST_ID)) {
							retailerData
									.put(CommonConstants.TAG_ANYTHING_PAGE_LIST_ID,
											retailerPagesObj
													.getString(CommonConstants
															.TAG_ANYTHING_PAGE_LIST_ID));
						}

						retailerData.put("RetailerCreatedPages", "1");
						retailerData.put("pageCount", String.valueOf(1));

						retalerInfoList.add(retailerData);
					}
					result = "true";

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;

		}

		@Override
		protected void onPostExecute(String result)
		{

			try {
				if (claimExist != null) {
					if (claimExist.equalsIgnoreCase("1")) {
						claimBusiness.setVisibility(View.GONE);
					}
				}

				splashDisplay();

				mainLayout.setVisibility(View.VISIBLE);

				if (splashFrame.getVisibility() == View.VISIBLE) {
					try {
						synchronized (this) {
							wait(3000);
						}
					} catch (InterruptedException ex) {
						ex.printStackTrace();
					} finally {
						splashFrame.setVisibility(View.INVISIBLE);
					}

				}
				if ("true".equals(result)) {
					claimBusiness.setText(claimTxtMsg);
					retailerNameTextView.setText(retailerName);
					retailerMileTextView.setText(retailerMile);
					retailerMileTextView2.setText(retailerMile);
					retailerInfoListView.setAdapter(new RetailerListAdapter(
							RetailerActivity.this, retalerInfoList));

					if (retailerAd != null && "N/A".equalsIgnoreCase(retailerAd)) {
						retailerADImageView.setVisibility(View.GONE);
						retailerAdLayout.setVisibility(View.GONE);
						retailerNameLayout.setVisibility(View.VISIBLE);
					} else {

						retailerADImageView.setVisibility(View.VISIBLE);
						retailerAdLayout.setVisibility(View.VISIBLE);
						retailerNameLayout.setVisibility(View.GONE);
					}

					if (retailerAd != null && !("N/A".equalsIgnoreCase(retailerAd))) {
						new ImageLoaderAsync(retailerADImageView, true)
								.execute(retailerAd);
					}

					// Call for Share Information
					shareInfo = new ShareInformation(RetailerActivity.this,
							retailerId, retailLocationID, "", "Appsite");
					isShareTaskCalled = false;

					leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);

				} else {
					if (jsonObject.has("response")
							&& "10005".equalsIgnoreCase(jsonObject.getJSONObject(
							"response").getString("responseCode"))) {
						response= jsonObject.getJSONObject("response")
								.getString("responseText");
					}

					Toast.makeText(getBaseContext(), response,
							Toast.LENGTH_SHORT).show();
				}

				if (mDialog != null) {
					mDialog.dismiss();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void splashDisplay()
	{
		if (retailerBanner != null && "N/A".equalsIgnoreCase(retailerBanner)) {
			splashFrame.setVisibility(View.INVISIBLE);
		} else {
			retailerInfoListView.setVisibility(View.GONE);
			retailerADImageView.setVisibility(View.GONE);
			retailerAdLayout.setVisibility(View.GONE);
			retailerNameLayout.setVisibility(View.GONE);
			splashFrame.setVisibility(View.VISIBLE);

			Intent intent = new Intent(RetailerActivity.this,
					RetailerBannerAddSplashActivity.class);

			retailerBanner = retailerBanner.replaceAll(" ", "%20");
			intent.putExtra("bannerAdUrl", retailerBanner);

			startActivityForResult(intent, 1);
		}

	}

	@Override
	public void onClick(View view)
	{
		if (retailerAdLayout != null && retailerAdURL != null) {
			Intent retailerLink = new Intent(RetailerActivity.this,
					ScanseeBrowserActivity.class);
			retailerLink.putExtra(CommonConstants.URL, retailerAdURL);
			retailerLink.putExtra("isMap", false);
			startActivity(retailerLink);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == 1) {

			if (resultCode == RESULT_OK) {

				if (retailerAd != null && "N/A".equalsIgnoreCase(retailerAd)) {
					retailerADImageView.setVisibility(View.GONE);
					retailerAdLayout.setVisibility(View.GONE);
					retailerNameLayout.setVisibility(View.VISIBLE);
				} else {

					retailerADImageView.setVisibility(View.VISIBLE);
					retailerAdLayout.setVisibility(View.VISIBLE);
					retailerNameLayout.setVisibility(View.GONE);
				}
				retailerInfoListView.setVisibility(View.VISIBLE);
			}

		}
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			finish();
		}

	}

}