package com.hubcity.android.screens;

import java.util.List;

import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.RetailerProductMediaActivity;
import com.scansee.android.ScanSeePdfViewerActivity;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

public class AnythingPageDisplayActivity extends CustomTitleBar {

    private String anyThingPageId = null;
    private String mItemId = null;
    private String anyThingPageExtLink = null;
    private boolean isExternalLInk = false;
    private boolean isMediaLInk = false;
    private String startDate;
    private String endDate;
    private String sDescription;
    private String longDescription;
    private String pageTitle;
    private String mediaPath;
    private String pageImage;
    private ImageView anyThingPageImageView;
    private TextView startDateView;
    private TextView endDateView;
    private CustomImageLoader customImageLoader;
    private static final String NOTAVAILABLE = "N/A";
    private TextView titleText;
    private WebView longDescView, sDescriptionView;
    private Context context = this;
    private LinearLayout mainLayout;

    private GetAnyThingPageDetails anythingAsyncTask;

    private boolean isFirst,isFromSideNav;

    private ShareInformation shareInfo;
    private boolean isShareTaskCalled;

    @Override
    protected void onResume() {
        super.onResume();

        try {
            List<ApplicationInfo> packages;
            PackageManager pm;
            pm = getPackageManager();
            // get a list of installed apps.
            packages = pm.getInstalledApplications(0);

            ActivityManager mActivityManager = (ActivityManager) context
                    .getSystemService(Context.ACTIVITY_SERVICE);

            for (ApplicationInfo packageInfo : packages) {
                if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1
                        || packageInfo.packageName.contains("com.adobe.reader")) {
                    continue;
                }

                mActivityManager.killBackgroundProcesses(packageInfo.packageName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            // Added By-Dileep for displaying title bar
            title.setSingleLine(true);
            title.setMaxLines(1);
            title.setWidth(290);
            title.setEllipsize(TextUtils.TruncateAt.END);

            isFirst = true;

            backImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAnythingAsyncTask();
                    finish();
                }
            });

//			setContentView(R.layout.activity_anything_page_display);
            setContentView(R.layout.hubciti_anything_page);
            if (UrlRequestParams.getUid().equals("-1")) {
                leftTitleImage.setVisibility(View.GONE);
                rightImage.setVisibility(View.GONE);
            }
            if(getIntent().hasExtra("isFromSideNav")){
                isFromSideNav = getIntent().getExtras().getBoolean("isFromSideNav");
            }
            if (getIntent().hasExtra(CommonConstants.LINK_ID)) {
                anyThingPageId = getIntent().getExtras().getString(
                        CommonConstants.LINK_ID);
            } else if (getIntent().hasExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA)) {
                anyThingPageId = getIntent().getExtras().getString(
                        Constants.BOTTOM_LINK_ID_INTENT_EXTRA);
            }

            if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);

            } else if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }

            mainLayout = (LinearLayout) findViewById(R.id.main_layout);
            mainLayout.setVisibility(View.INVISIBLE);

            sDescriptionView = (WebView) findViewById(R.id.anything_page_short_des);
            titleText = (TextView) findViewById(R.id.anything_page_title_text);
            longDescView = (WebView) findViewById(R.id.anything_page_long_des);
            startDateView = (TextView) findViewById(R.id.anything_page_start_date);
            endDateView = (TextView) findViewById(R.id.anything_page_end_date);
            anyThingPageImageView = (ImageView) findViewById(R.id.anything_page_image);

            findViewById(R.id.anything_page_short_des_lay).setVisibility(
                    View.INVISIBLE);
            findViewById(R.id.anything_page_long_des_lay).setVisibility(
                    View.INVISIBLE);
            findViewById(R.id.anything_page_lay_start_date).setVisibility(
                    View.INVISIBLE);
            findViewById(R.id.anything_page_lay_end_date).setVisibility(
                    View.INVISIBLE);

            callAnythingAsyncTask();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class GetAnyThingPageDetails extends
            AsyncTask<String, Void, String> {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        JSONObject jsonObject = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(AnythingPageDisplayActivity.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        // u try to do this 2screens
        @Override
        protected String doInBackground(String... params) {

            String result = "";
            try {

                JSONObject urlParameters = mUrlRequestParams
                        .getAnythingPageJsonReqParam(anyThingPageId, mItemId);
                String get_anythingpage_info = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/gethubcitianythinginfojson";
                jsonObject = mServerConnections
                        .getUrlJsonPostResponse(
                                get_anythingpage_info, urlParameters);

                if (jsonObject != null) {
                    result = jsonObject
                            .getString(CommonConstants.TAG_RESPONE_CODE);
                    anyThingPageExtLink = jsonObject
                            .getString("pageLink");
                    mediaPath = jsonObject.getString("mediaPath");
                    if (NOTAVAILABLE.equals(anyThingPageExtLink)
                            && NOTAVAILABLE.equals(mediaPath)) {

                        pageTitle = jsonObject
                                .getString("pageTitle");
                        pageImage = jsonObject
                                .getString("logoImagePath");
                        sDescription = jsonObject
                                .getString("sDescription");
                        longDescription = jsonObject
                                .getString("longDescription");
                        startDate = jsonObject
                                .getString("startDate");
                        endDate = jsonObject.getString("endDate");
                    } else if (!NOTAVAILABLE
                            .equals(anyThingPageExtLink)) {
                        isExternalLInk = true;

                    } else if (!NOTAVAILABLE.equals(mediaPath)) {
                        isMediaLInk = true;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            Intent fileAtivity;
            if ("10000".equals(result)) {

                if (isExternalLInk) {

                    if (anyThingPageExtLink.endsWith(".pdf")) {

                        // fileAtivity = new Intent(
                        // AnythingPageDisplayActivity.this,
                        // ScanseeFileActivity.class);
                        fileAtivity = new Intent(
                                AnythingPageDisplayActivity.this,
                                ScanSeePdfViewerActivity.class);
                        try {
                            fileAtivity
                                    .putExtra(
                                            RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
                                            anyThingPageExtLink);
                            fileAtivity.putExtra(
                                    CommonConstants.TAG_PAGE_TITLE, "");
                            fileAtivity.putExtra("module", "Anything");
                            fileAtivity.putExtra("isShare", true);
                            fileAtivity.putExtra("pageId", anyThingPageId);
                            cancelAnythingAsyncTask();
                            finish();
                            startActivityForResult(fileAtivity,
                                    Constants.STARTVALUE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        fileAtivity = new Intent(
                                AnythingPageDisplayActivity.this,
                                ScanseeBrowserActivity.class);
                        fileAtivity.putExtra(CommonConstants.URL,
                                anyThingPageExtLink);
                        fileAtivity
                                .putExtra(CommonConstants.TAG_PAGE_TITLE, "");
                        fileAtivity.putExtra("module", "Anything");
                        fileAtivity.putExtra("isShare", true);
                        fileAtivity.putExtra("isFromSideNav", isFromSideNav);
                        fileAtivity.putExtra("pageId", anyThingPageId);
                        cancelAnythingAsyncTask();
                        finish();
                        startActivityForResult(fileAtivity,
                                Constants.STARTVALUE);
                    }

                } else if (isMediaLInk) {

                    if (mediaPath.endsWith(".pdf")) {

                        // fileAtivity = new Intent(
                        // AnythingPageDisplayActivity.this,
                        // ScanseeFileActivity.class);
                        fileAtivity = new Intent(
                                AnythingPageDisplayActivity.this,
                                ScanSeePdfViewerActivity.class);

                        fileAtivity
                                .putExtra(
                                        RetailerProductMediaActivity.TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
                                        mediaPath);
                        fileAtivity.putExtra("module", "Anything");
                        fileAtivity.putExtra("isShare", true);
                        fileAtivity.putExtra("pageId", anyThingPageId);

                        cancelAnythingAsyncTask();
                        finish();
                        startActivityForResult(fileAtivity,
                                Constants.STARTVALUE);

                    } else if (mediaPath.endsWith(".mp4")) {

                        fileAtivity = new Intent(
                                AnythingPageDisplayActivity.this,
                                VideoActivity.class);
                        mediaPath = mediaPath.replaceAll(" ", "%20");
                        fileAtivity.putExtra(CommonConstants.URL, mediaPath);
                        fileAtivity
                                .putExtra(CommonConstants.TAG_PAGE_TITLE, "");
                        fileAtivity.putExtra("isShare", true);
                        fileAtivity.putExtra("pageId", anyThingPageId);
                        cancelAnythingAsyncTask();
                        finish();
                        startActivityForResult(fileAtivity,
                                Constants.STARTVALUE);

                    } else if (mediaPath.endsWith(".mp3")) {
                        fileAtivity = new Intent(
                                AnythingPageDisplayActivity.this,
                                StreamingMp3Player.class);
                        mediaPath = mediaPath.replaceAll(" ", "%20");
                        fileAtivity.putExtra(CommonConstants.URL, mediaPath);
                        fileAtivity.putExtra("isShare", true);
                        fileAtivity.putExtra("pageId", anyThingPageId);
                        cancelAnythingAsyncTask();
                        finish();
                        startActivityForResult(fileAtivity,
                                Constants.STARTVALUE);
                    } else {

                        fileAtivity = new Intent(
                                AnythingPageDisplayActivity.this,
                                ScanseeBrowserActivity.class);

                        fileAtivity.putExtra(CommonConstants.URL, mediaPath);
                        fileAtivity
                                .putExtra(CommonConstants.TAG_PAGE_TITLE, "");
                        fileAtivity.putExtra("isShare", true);
                        fileAtivity.putExtra("isFromSideNav", isFromSideNav);
                        fileAtivity.putExtra("pageId", anyThingPageId);
                        cancelAnythingAsyncTask();
                        finish();
                        startActivityForResult(fileAtivity,
                                Constants.STARTVALUE);
                    }

                } else {

                    // Call for Share Information
                    shareInfo = new ShareInformation(
                            AnythingPageDisplayActivity.this, "", "",
                            anyThingPageId, "Anything");
                    isShareTaskCalled = false;

                    leftTitleImage
                            .setBackgroundResource(R.drawable.share_btn_down);
                    leftTitleImage
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    if (!Constants.GuestLoginId
                                            .equals(UrlRequestParams.getUid()
                                                    .trim())) {
                                        shareInfo.shareTask(isShareTaskCalled);
                                        isShareTaskCalled = true;
                                    } else {
                                        Constants
                                                .SignUpAlert(AnythingPageDisplayActivity.this);
                                        isShareTaskCalled = false;
                                    }
                                }
                            });

                    title.setText("Details");
                    titleText.setText(pageTitle);
                    findViewById(R.id.anything_page_short_des_lay)
                            .setVisibility(View.VISIBLE);
                    findViewById(R.id.anything_page_long_des_lay)
                            .setVisibility(View.VISIBLE);
                    if (!NOTAVAILABLE.equals(pageImage)
                            && !"".equals(pageImage)) {

                        customImageLoader = new CustomImageLoader(getApplicationContext(),
                                false);
                        anyThingPageImageView.setTag(pageImage);
                        customImageLoader.displayImage(pageImage,
                                AnythingPageDisplayActivity.this,
                                anyThingPageImageView);

                    }

                    String htmlcode = "<html><body style=\"text-align:justify\"> %s </body></Html>";

                    if (null != sDescription) {

                        sDescription = sDescription.replaceAll("&#60;", "<");
                        sDescription = sDescription.replaceAll("&#62;", ">");
//                        sDescriptionView.setText(Html.fromHtml(sDescription));
//                        Linkify.addLinks(sDescriptionView, Linkify.ALL);
                        sDescriptionView.getSettings().setJavaScriptEnabled(true);
                        sDescriptionView.setWebViewClient(new WebViewClient() {
                            public boolean shouldOverrideUrlLoading(
                                    WebView view, String url) {
                                return openUrlInApp(url);
                            }
                        });

                        sDescriptionView.loadData(
                                String.format(htmlcode, sDescription),
                                "text/html", "utf-8");
                    }

                    if (null != longDescription) {
                        longDescription = longDescription.replaceAll("&#60;",
                                "<");
                        longDescription = longDescription.replaceAll("&#62;",
                                ">");
//                        longDescView.setText(Html.fromHtml(longDescription));
//                        Linkify.addLinks(longDescView, Linkify.ALL);
                        longDescView.getSettings().setJavaScriptEnabled(true);
                        longDescView.setWebViewClient(new WebViewClient() {
                            public boolean shouldOverrideUrlLoading(
                                    WebView view, String url) {
                                return openUrlInApp(url);
                            }
                        });

                        longDescView.loadData(
                                String.format(htmlcode, longDescription),
                                "text/html", "utf-8");
                    }

                    if (NOTAVAILABLE.equals(startDate)) {

                        findViewById(R.id.anything_page_lay_start_date)
                                .setVisibility(View.GONE);
                    } else {

                        startDateView.setText(startDate);
                        findViewById(R.id.anything_page_lay_start_date)
                                .setVisibility(View.VISIBLE);
                    }
                    if (NOTAVAILABLE.equals(endDate)) {

                        findViewById(R.id.anything_page_lay_end_date)
                                .setVisibility(View.GONE);
                    } else {

                        endDateView.setText(endDate);
                        findViewById(R.id.anything_page_lay_end_date)
                                .setVisibility(View.VISIBLE);
                    }
                }

            }

            mainLayout.setVisibility(View.VISIBLE);
            try {
                mDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private boolean openUrlInApp(String url){
        if (url != null) {
            if (url.startsWith("tel:")) {
                Intent intent = new Intent(
                        Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
            } else if (url.startsWith("mailto:")) {
                String emailId = url.substring(
                        url.indexOf(":") + 1, url.length());
                Intent emailIntent = new Intent(
                        android.content.Intent.ACTION_SEND);
                emailIntent.setType("message/rfc822");
                emailIntent.putExtra(
                        android.content.Intent.EXTRA_EMAIL,
                        new String[] {emailId});
                startActivity(Intent
                        .createChooser(emailIntent,
                                "Email:"));
            } else if (url.endsWith(".pdf")
                    || url.contains("/PDF/")) {

                Intent intent = new Intent(AnythingPageDisplayActivity.this,
                        ScanSeePdfViewerActivity.class);
                try {
                    intent.putExtra(
                            RetailerProductMediaActivity
                                    .TAG_PRODUCTMEDIA_PRODUCTMEDIAPATH,
                            url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(intent);

            } else {
                // If the url contains play store link then opening the
                // particular app
                if (url.contains("play.google.com/store/apps/")) {
                    String packageName = url.split("=")[1];
                    Constants.openCustomUrl(packageName, AnythingPageDisplayActivity.this);
                } else {
                    Intent intent = new Intent(AnythingPageDisplayActivity.this,
                            ScanseeBrowserActivity.class);
                    intent.putExtra(CommonConstants.URL, url);
                    intent.putExtra("frommupdfInternal", true);
                    startActivity(intent);
                }
            }
            return true;
        } else {
            return false;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            cancelAnythingAsyncTask();
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void callAnythingAsyncTask() {
        if (isFirst) {
            anythingAsyncTask = new GetAnyThingPageDetails();
            anythingAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            isFirst = false;
            return;

        }

        if (anythingAsyncTask != null) {
            if (!anythingAsyncTask.isCancelled()) {
                anythingAsyncTask.cancel(true);

            }

            anythingAsyncTask = null;
        }

        anythingAsyncTask = new GetAnyThingPageDetails();
        anythingAsyncTask.execute();

    }

    private void cancelAnythingAsyncTask() {
        if (anythingAsyncTask != null && !anythingAsyncTask.isCancelled()) {
            anythingAsyncTask.cancel(true);
        }

        anythingAsyncTask = null;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelAnythingAsyncTask();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancelAnythingAsyncTask();
        finish();
    }
}
