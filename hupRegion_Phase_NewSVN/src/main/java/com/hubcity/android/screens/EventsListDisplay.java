package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.EventsEntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.HubCityLogger;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;

public class EventsListDisplay extends CustomTitleBar implements
		OnItemClickListener, OnClickListener
{

	ArrayList<Item> items = new ArrayList<>();

	Button moreInfo;
	int lastvisitProdId = 0;
	int lastPosition;
	ListView listview;
	boolean nextPage = false;
	ProgressDialog progDialog;

	PrepareEventList prepareEventList = new PrepareEventList();
	String sortChoice = "Date";
	View moreResultsView;

	String mItemId;
	String bottomBtnId = null;
	String postalCode = null;

	String group = null;
	String sort = null;

	boolean flag = false;

	LocationManager locationManager;
	String latitude = null;
	String longitude = null;
	String catId = "";
	String catName = "";
	String mLinkId = "";
	String fundId = "";


	ArrayList<PrepareEventList> eventList;

	LinearLayout linearLayout = null;
	BottomButtons bb = null;
	Activity activity;

	boolean hasBottomBtns = false;
	boolean enableBottomButton = true;

	String retailerId = "";
	String retailLocationId = "", retListId = "";

	EventsListAsyncTask eventsAsyncTask;
	boolean isFirst;

	public static ArrayList<String> eventIDs = new ArrayList<>();

	boolean isFirstLoad, isAlreadyLoading, isItemClicked;
	HubCityContext mHubCiti;
	String listType = "Event";
	private CustomNavigation customNaviagation;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.events_activity);
		CommonConstants.hamburgerIsFirst = true;

		activity = EventsListDisplay.this;
		leftTitleImage.setVisibility(View.GONE);
		isFirst = true;
		HubCityContext.savedValues = new HashMap<>();
		isFirstLoad = true;
		isItemClicked = true;
		mHubCiti = (HubCityContext) getApplicationContext();
		mHubCiti.setCancelled(false);
		// Initiating Bottom button class
		if (null != BottomButtonInfoSingleton.getBottomButtonsSingleton()) {
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "");
		}

		linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
		linearLayout.setVisibility(View.INVISIBLE);

		if (getIntent().hasExtra("fundId")) {
			fundId = getIntent().getExtras().getString("fundId");
		}

		if (getIntent().hasExtra("retailerId")) {
			retailerId = getIntent().getExtras().getString("retailerId");
		}
		if (getIntent().hasExtra("listType")) {
			listType = getIntent().getExtras().getString("listType");
		}
		if (listType.equalsIgnoreCase("Event")) {
			title.setText("Events");
		} else {
			title.setText("Band Events");
		}
		if (getIntent().hasExtra("retListId")) {
			retListId = getIntent().getExtras().getString("retListId");
		}
		if (getIntent().hasExtra("retailLocationId")) {
			retailLocationId = getIntent().getExtras().getString(
					"retailLocationId");
		}

		if (getIntent().hasExtra("catName")) {
			catName = getIntent().getExtras().getString("catName");
		}

		if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
				&& getIntent().getExtras().getString(
				Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
			mItemId = getIntent().getExtras().getString(
					Constants.MENU_ITEM_ID_INTENT_EXTRA);
		}

		if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
				&& getIntent().getExtras().getString(
				Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
			bottomBtnId = getIntent().getExtras().getString(
					Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
		}

		if (getIntent().hasExtra("mLinkId")) {
			mLinkId = getIntent().getExtras().getString("mLinkId");
		} else if (getIntent().hasExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA)) {
			mLinkId = getIntent().getExtras().getString(
					Constants.BOTTOM_LINK_ID_INTENT_EXTRA);
		}

		if (getIntent().hasExtra("catId")) {
			catId = getIntent().getExtras().getString("catId");
		}

		moreResultsView = getLayoutInflater().inflate(
				R.layout.events_pagination, listview, true);
		moreInfo = (Button) moreResultsView
				.findViewById(R.id.events_view_more_button);
		moreInfo.setOnClickListener(this);
		listview = (ListView) findViewById(R.id.events_listView);

		/***
		 * Calling the progress dialog for the first time only After that it
		 * will show the bottom bar of list as loader
		 ***/
		progDialog = new ProgressDialog(EventsListDisplay.this);
		progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progDialog.setMessage(Constants.DIALOG_MESSAGE);
		progDialog.setCancelable(false);
		progDialog.show();

		callEventsListAsyncTask();


//		rightImage.setVisibility(View.GONE);
		divider.setVisibility(View.GONE);


		backImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finishAction();

			}
		});

		//user for hamburger in modules
		drawerIcon.setVisibility(View.VISIBLE);
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);

	}

	private HashMap<String, String> getListValues()
	{
		HashMap<String, String> values = new HashMap<>();
		values.put(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);

		values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

		values.put("Class", "Events");

		values.put("fundId", fundId);

		values.put("retailerId", retailerId);

		values.put("retListId", retListId);

		values.put("retailLocationId", retailLocationId);

		values.put("retailerEvents",
				getIntent().getExtras().getString("retailerEvents"));

		values.put("mLinkId", mLinkId);

		values.put("catName", catName);

		values.put("catId", catId);

		return values;

	}

	private boolean byDistanceClicked;

	private void refresh()
	{

		lastvisitProdId = 0;
		if (eventList != null) {
			eventList.clear();
		}
		if (items != null) {
			items.clear();
		}
		enableBottomButton = false;
		prepareEventList = new PrepareEventList();
		progDialog = new ProgressDialog(EventsListDisplay.this);
		progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progDialog.setMessage(Constants.DIALOG_MESSAGE);
		progDialog.setCancelable(false);
		progDialog.show();
		callEventsListAsyncTask();
	}


	public class EventsListAsyncTask extends AsyncTask<String, Void, String>
	{

		String returnValue = "sucessfull";
		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		ListDetails objListDetails = new ListDetails();

		protected void onPreExecute()
		{

			getGPSValues();

		}

		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);

			if ("sucessfull".equals(returnValue)) {

				if (nextPage) {

					try {
						listview.removeFooterView(moreResultsView);
					} catch (Exception ex) {
						ex.printStackTrace();
					}

					listview.addFooterView(moreResultsView);

				} else {
					listview.removeFooterView(moreResultsView);
				}

				eventList = new ArrayList<>();
				eventList = objListDetails.eventList;
				boolean isHeaderExist = false;
				int ind = 0;
				for (int i = 0; i < eventList.size(); i++) {

					for (int index = 0; index < items.size(); index++) {
						if (items.get(index).isSection()) {
							SectionItem item = (SectionItem) items.get(index);

							if (item.getTitle().equals(eventList.get(i).header)) {

								ind = index;
								isHeaderExist = true;
								break;
							}
						}

					}

					if (isHeaderExist) {
						for (int k = ind + 1; i < items.size(); k++) {

							if (items.get(k).isSection()) {
								ind = k - 1;
								break;
							}

							if (k == items.size() - 1) {
								ind = k;
								break;
							}
						}
					}

					if (isHeaderExist) {

						for (int j = 0; j < eventList.get(i).eventLists.size(); j++) {

							items.add(
									++ind,
									new EventsEntryItem(
											eventList.get(i).eventLists.get(j).startDate,
											eventList.get(i).eventLists.get(j).eventListId,
											eventList.get(i).eventLists.get(j).eventId,
											eventList.get(i).eventLists.get(j).eventName,
											eventList.get(i).eventLists.get(j).startTime,
											eventList.get(i).eventLists.get(j).eventCatId,
											eventList.get(i).eventLists.get(j).eventCatName,
											eventList.get(i).eventLists.get(j).distance,
											eventList.get(i).eventLists.get(j).imagePath,
											eventList.get(i).eventLists.get(j).isOngoing));

						}
						isHeaderExist = false;
					} else {
						items.add(new SectionItem(eventList.get(i).header));

						for (int j = 0; j < eventList.get(i).eventLists.size(); j++) {
							items.add(new EventsEntryItem(
									eventList.get(i).eventLists.get(j).startDate,
									eventList.get(i).eventLists.get(j).eventListId,
									eventList.get(i).eventLists.get(j).eventId,
									eventList.get(i).eventLists.get(j).eventName,
									eventList.get(i).eventLists.get(j).startTime,
									eventList.get(i).eventLists.get(j).eventCatId,
									eventList.get(i).eventLists.get(j).eventCatName,
									eventList.get(i).eventLists.get(j).distance,
									eventList.get(i).eventLists.get(j).imagePath,
									eventList.get(i).eventLists.get(j).isOngoing));

						}
					}

				}

				EventsAdapter adapter = new EventsAdapter(
						EventsListDisplay.this, items, activity);
				listview.setAdapter(adapter);
				moreResultsView.setVisibility(View.VISIBLE);
				listview.setOnItemClickListener(EventsListDisplay.this);
				if (isAlreadyLoading) {
					listview.setSelection(lastPosition);
				}
				isAlreadyLoading = false;

				if (bb != null) {
					bb.setOptionsValues(getListValues(), null);
				}

			} else {

				progDialog.dismiss();
				EventsAdapter adapter = new EventsAdapter(
						EventsListDisplay.this, new ArrayList<Item>(), activity);
				listview.setAdapter(adapter);
				listview.removeFooterView(moreResultsView);
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						EventsListDisplay.this);

				alertDialogBuilder.setCancelable(false);
				alertDialogBuilder.setMessage(Html.fromHtml(returnValue
						.replace("\\n", "<br/>")));
				alertDialogBuilder.setPositiveButton("OK",
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{

								dialog.dismiss();
								// finishAction();
							}
						});
				alertDialogBuilder.show();

			}

			if (hasBottomBtns && enableBottomButton) {

				bb.createbottomButtontTab(linearLayout, false);
			}

			linearLayout.setVisibility(View.VISIBLE);

			if (progDialog.isShowing()) {
				progDialog.dismiss();
			}

		}

		@Override
		protected String doInBackground(String... arg0)
		{

			try {
				HashMap<String, String> resultSet;
				resultSet = mHubCiti.getFilterValues();

				String cityIds = "", eventDate = "";

				if (resultSet != null && !resultSet.isEmpty()) {

					if (resultSet.containsKey("SortBy")) {
						sortChoice = resultSet.get("SortBy");
					}

					if (resultSet.containsKey("EventDate")) {
						eventDate = resultSet.get("EventDate");
					}

					if (resultSet.containsKey("savedCityIds")) {
						cityIds = resultSet.get("savedCityIds");
						cityIds = cityIds.replace("All,", "");
					}

					if (resultSet.containsKey("savedSubCatIds")) {
						catId = resultSet.get("savedSubCatIds");
					}

				}

				if (byDistanceClicked) {
					byDistanceClicked = false;
					sortChoice = "Distance";
				}

				String mainMenuId = Constants.getMainMenuId();
				Log.v("", "MENU MAIN ID PREFS ************ : " + mainMenuId);
				if (getIntent().hasExtra("mainMenuId")) {
					mainMenuId = getIntent().getExtras()
							.getString("mainMenuId");
					Log.v("", "MENU MAIN ID FROM SERVER  ************ : "
							+ mainMenuId);
				}

				if (fundId != null && fundId.length() > 0) {
					mItemId = "";
					bottomBtnId = "";
				}

				String url_events = Properties.url_local_server
						+ Properties.hubciti_version + "alertevent/eventlist";

				JSONObject urlParameters = mUrlRequestParams
						.createEventsListParameter(mItemId, lastvisitProdId
										+ "", sortChoice, bottomBtnId, postalCode,
								latitude, longitude, catId, retailerId,
								retListId, retailLocationId, fundId,
								mainMenuId, eventDate, cityIds, listType);

				JSONObject jsonResponse = mServerConnections
						.getUrlJsonPostResponse(url_events, urlParameters);

				HubCityLogger.d("EventsListDisplay", " EventList Request :\n"
						+ urlParameters);
				if (!jsonResponse.getString("responseText").equalsIgnoreCase("No Records Found" +
						".") && (jsonResponse != null)) {

					try {

						// For BottomButton
						if (jsonResponse.has("bottomBtnList")) {

							hasBottomBtns = true;

							try {
								if (bb != null) {
									ArrayList<BottomButtonBO> bottomButtonList = bb
											.parseForBottomButton(jsonResponse);
									BottomButtonListSingleton
											.clearBottomButtonListSingleton();
									BottomButtonListSingleton
											.getListBottomButton(bottomButtonList);
								}

							} catch (JSONException e1) {
								e1.printStackTrace();
							}

						} else {
							hasBottomBtns = false;
						}


						try {

							JSONArray json4 = jsonResponse
									.getJSONArray("categoryList");

							if (json4 != null) {

								for (int i = 0; i < json4.length(); i++) {
									JSONObject elem = json4
											.getJSONObject(i);
									String groupContent = elem
											.getString("groupContent");
									prepareEventList = new PrepareEventList();
									prepareEventList.header = groupContent;

									if (elem != null) {
										try {
											boolean isEventArray = isJSONArray(
													elem, "eventList");
											JSONArray jsonEventsArray = null;
											if (isEventArray) {

												jsonEventsArray = elem
														.getJSONArray("eventList");
												for (int index = 0; index < jsonEventsArray
														.length(); index++) {
													JSONObject elems = jsonEventsArray
															.getJSONObject(index);
													if (elem != null) {

														DetailsByGroup objDetailsByGroup = new
																DetailsByGroup();
														objDetailsByGroup.startTime = elems
																.getString("startTime");
														objDetailsByGroup.startDate = elems
																.getString("startDate");
														if (elems
																.has("eventListId")) {
															objDetailsByGroup.eventListId = elems
																	.getString("eventListId");
														}
														if (elems
																.has("eventId")) {
															objDetailsByGroup.eventId = elems
																	.getString("eventId");
														}
														objDetailsByGroup.eventName = elems
																.getString("eventName");
														objDetailsByGroup.eventCatId = elems
																.getString("eventCatId");
														objDetailsByGroup.eventCatName = elems
																.getString("eventCatName");
														objDetailsByGroup.distance = elems
																.getString("distance");
														objDetailsByGroup.isOngoing = elems
																.getString("isOnGoing");

														if (elems
																.has("imgPath")) {
															if (elems
																	.getString("imgPath") !=
																	null) {
																objDetailsByGroup.imagePath = elems
																		.getString("imgPath");
															}

														}
														prepareEventList.eventLists
																.add(objDetailsByGroup);

													}

												}

												objListDetails.eventList
														.add(prepareEventList);
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}

								}
							}

						} catch (Exception e) {
							e.printStackTrace();
							returnValue = "UNSUCCESS";
							return returnValue;
						}
						try {
							HubCityLogger.d("EventsListDisplay", "maxRowNum :"
									+ jsonResponse.getString("maxRowNum"));

							if (Integer.valueOf(jsonResponse.getString("maxRowNum")) >
									lastvisitProdId) {
								lastvisitProdId = Integer.valueOf(jsonResponse
										.getString("maxRowNum"));

								HubCityLogger.d("EventsListDisplay",
										"lastvisitProdId :" + lastvisitProdId);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (jsonResponse.has("mainMenuId")) {
							mainMenuId = jsonResponse.getString("mainMenuId");
							Log.v("", "MUNI DI : " + mainMenuId);
							Constants.saveMainMenuId(mainMenuId);
						}

						try {

							if ("1".equalsIgnoreCase(jsonResponse
									.getString("nextPage"))) {
								nextPage = true;

								HubCityLogger.d(
										"EventsListDisplay",
										"nextPage jsonValue :"
												+ jsonResponse.getString("nextPage"));
								HubCityLogger.d("EventsListDisplay",
										"nextPage :" + nextPage);

							} else if ("0".equalsIgnoreCase(jsonResponse
									.getString("nextPage"))) {

								nextPage = false;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					} catch (Exception e) {
						e.printStackTrace();

						HubCityLogger.d("EventListDisplayactivity",
								"Exception in Parsing");

						returnValue = "UNSUCCESS";
						return returnValue;
					}
					return returnValue;
				} else {
					if (jsonResponse != null) {
						returnValue = jsonResponse.getString("responseText");
					} else {
						returnValue = "technical problem";
					}
					return returnValue;
				}


			} catch (Exception e) {
				e.printStackTrace();
				HubCityLogger.d("EventListDisplayactivity",
						"Exception in Parsing");

				returnValue = "UNSUCCESS";
				return returnValue;
			}

		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3)
	{
		try {
			isItemClicked = true;
			EventsEntryItem item = (EventsEntryItem) items.get(position);
			Intent eventDescripNav = new Intent(getApplicationContext(),
					EventDescription.class);

			eventIDs.add(item.eventId);


//			eventDescripNav.putExtra("eventDetails",(Serializable)createCalEvent.get(position));

			eventDescripNav.putExtra("hubCitiId", Constants.HUB_CITY_ID);
			eventDescripNav.putExtra("eventName", item.eventName);
			eventDescripNav.putExtra("eventId", item.eventId);
			eventDescripNav.putExtra("ImageURL", item.imagePath);
			eventDescripNav.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
					mItemId);
			eventDescripNav.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
					bottomBtnId);
			if (item.eventListId != null) {
				eventDescripNav.putExtra("eventListId", item.eventListId);
			}
			startActivityForResult(eventDescripNav, Constants.STARTVALUE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void loadMoreData()
	{
		isAlreadyLoading = true;
		enableBottomButton = false;
		callEventsListAsyncTask();
	}

	public boolean isJSONArray(JSONObject jsonObject, String value)
	{
		boolean isArray = false;

		JSONObject isJSONObject = jsonObject.optJSONObject(value);
		if (isJSONObject == null) {
			JSONArray isJSONArray = jsonObject.optJSONArray(value);
			if (isJSONArray != null) {
				isArray = true;
			}
		}
		return isArray;
	}

	class PrepareEventList
	{
		String header;
		ArrayList<DetailsByGroup> eventLists = new ArrayList<>();

	}

	class DetailsByGroup
	{
		String startTime;
		String startDate;
		String eventListId;
		String eventId;
		String eventName;
		String eventCatId;
		String eventCatName;
		String distance;
		String imagePath;
		String isOngoing;
	}

	public class ListDetails
	{
		ArrayList<PrepareEventList> eventList = new ArrayList<>();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId()) {

			case R.id.events_view_more_button:
				eventsViewMoreButton();
				break;
			default:
				break;
		}
	}

	private void eventsViewMoreButton()
	{
		enableBottomButton = false;
		callEventsListAsyncTask();

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{

		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finishAction();
		} else if (resultCode == 2) {
			isItemClicked = false;
		}
	}

	public void onDestroy()
	{

		super.onDestroy();

		HubCityContext mHubCity = (HubCityContext) getApplicationContext();
		HubCityContext.isDoneClicked = false;
		mHubCity.clearArrayAndAllValues(false);
		if (!flag) {
			mHubCity.setGroupSelection("date");
			mHubCity.setSortSelection("date");
			flag = false;
		}

	}

	private void cancelEventsListTask()
	{
		if (eventsAsyncTask != null && !eventsAsyncTask.isCancelled()) {
			eventsAsyncTask.cancel(true);
			eventsAsyncTask = null;
		}

	}

	@SuppressWarnings("deprecation")
	public void getGPSValues()
	{
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		String provider = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (provider.contains("gps")) {

			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (locNew != null) {

				latitude = String.valueOf(locNew.getLatitude());
				longitude = String.valueOf(locNew.getLongitude());

			} else {
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					latitude = String.valueOf(locNew.getLatitude());
					longitude = String.valueOf(locNew.getLongitude());
				} else {
					latitude = CommonConstants.LATITUDE;
					longitude = CommonConstants.LONGITUDE;
				}
			}
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}

		if (!isItemClicked) {
			isFirstLoad = false;
		}

		// Add screen name when needed
		if (null != bb) {
			bb.setActivityInfo(activity, "Events");
			bb.setOptionsValues(getListValues(), null);
		}

		if (mHubCiti.isCancelled()) {
			return;
		}

		if (!isFirstLoad) {
			if (!isItemClicked) {

				items.clear();
				moreResultsView.setVisibility(View.GONE);

				eventList = new ArrayList<>();

				EventsAdapter adapter = new EventsAdapter(
						EventsListDisplay.this, items, activity);
				listview.setAdapter(adapter);

				// Add screen name when needed
				refresh();
			}

		}

		isFirstLoad = false;
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		isFirstLoad = true;
	}

	public void callEventsListAsyncTask()
	{

		try {
			lastPosition = items.size();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (eventsAsyncTask != null) {
			if (!eventsAsyncTask.isCancelled()) {
				eventsAsyncTask.cancel(true);
			}
			eventsAsyncTask = null;
		}

		if (isFirst) {
			eventsAsyncTask = new EventsListAsyncTask();
			eventsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;
		}

		eventsAsyncTask = new EventsListAsyncTask();
		eventsAsyncTask.execute();

	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		finishAction();
	}

	private void finishAction()
	{
		cancelEventsListTask();
		finish();
		mHubCiti.clearSavedValues();
	}
}
