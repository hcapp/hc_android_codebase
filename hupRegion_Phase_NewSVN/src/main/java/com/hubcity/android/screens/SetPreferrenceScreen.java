package com.hubcity.android.screens;

import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.com.hubcity.model.UserEducationOptionsModel;
import com.com.hubcity.model.UserIncomeRangeOptionsModel;
import com.com.hubcity.model.UserInfoFieldsModel;
import com.com.hubcity.model.UserInfoModel;
import com.com.hubcity.model.UserInfoRequestModel;
import com.com.hubcity.model.UserInfoUpdateModel;
import com.com.hubcity.model.UserMaritalOptionsModel;
import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.UserSettingsComponents;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.ImageLoaderAsync;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.model.UpdateUserResponseModel;
import com.scansee.hubregion.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SetPreferrenceScreen extends CustomTitleBar implements
		OnClickListener
{

	private static final String TAG = SetPreferrenceScreen.class.getSimpleName();
	private static final int DATE_PICKER_ID = 1111;
	private int year;
	private int month;
	private int day;
	private EditText emailTextField, datePickerTextField;
	private AlertDialog.Builder alertDialogBuilder;
	private Button saveBtn;
	private boolean isReg;
	private String btnFontColor;
	private String btnColor;
	private final HashMap<String, String> fieldValues = new HashMap<>();
	private RestClient mRestClient;
	private TableLayout setPreferenceTableLayout;
	private UserInfoModel mUserInfoModel;
	private ProgressDialog progDialog;
	PopupWindow changePasswordPopUp;
	Button updatePassowrdBtn;
	Button closePasswordBtn;
	View layoutPopUp;
	String strPushRegMsg;


	private boolean eMailValidation(String emailid)
	{
		Pattern emailPattern = Pattern.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
		Matcher emailMatcher = emailPattern.matcher(emailid);
		return emailMatcher.matches();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.set_preference);
			getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            strPushRegMsg = this.getIntent().getStringExtra(
                    Constants.PUSH_NOTIFY_MSG);
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText("Tell Us About You");
			if (Constants.getNavigationBarValues("titleBkGrdColor") != null) {
				(findViewById(R.id.bottom_bar)).setBackgroundColor(Color.parseColor
						(Constants.getNavigationBarValues("titleBkGrdColor")));
			} else {
				(findViewById(R.id.bottom_bar)).setBackgroundColor(getResources()
						.getColor(R.color.black));
			}
			if (getIntent().getExtras().getBoolean("regacc")) {
				rightImage.setVisibility(View.GONE);
				backImage.setVisibility(View.GONE);
			}
			btnColor = getIntent().getExtras().getString("btnColor");
			btnFontColor = getIntent().getExtras().getString("btnFontColor");

			leftTitleImage.setVisibility(View.GONE);
			divider.setVisibility(View.GONE);

			mRestClient = RestClient.getInstance();
			try {
				isReg = getIntent().getBooleanExtra("regacc", false);
			} catch (Exception e) {
				e.printStackTrace();
				isReg = false;
			}
			fieldValues.clear();
			getUserInfo();
			new UserSettingsComponents();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId()) {
			case R.id.account_privacypolicybtn:
				Intent privacy = new Intent(getApplicationContext(),
						PrivacyPolicyActivity.class);
				startActivity(privacy);
				break;
			case R.id.account_skipbtn:
				finish();
				break;
			case R.id.account_savebtn:
				accountSaveBtn();
				break;

			default:
				break;
		}
	}

	private void accountSaveBtn()
	{
		Log.d(TAG, fieldValues.toString());
		boolean flag = true;
		for (UserInfoFieldsModel field : mUserInfoModel.getListUserDetails()) {
			if (field != null) {
				if (field.isRequiredField()) {
					if (fieldValues.get(field.getFieldName()).trim().isEmpty()) {
						flag = false;
						showDialog("Please enter mandatory fields");
						break;
					} else if (field.getFieldName().equalsIgnoreCase(UserSettingsComponents
							.LBL_INCOME_RANGE)
							&& fieldValues.get(field.getFieldName()).trim().equals("0")
							|| field.getFieldName().equalsIgnoreCase(UserSettingsComponents
							.LBL_MARITAL_STATUS)
							&& fieldValues.get(field.getFieldName()).trim().equals("0")
							|| field.getFieldName().equalsIgnoreCase(UserSettingsComponents
							.LBL_EDUCATION)
							&& fieldValues.get(field.getFieldName()).trim().equals("0")) {
						flag = false;
						showDialog("Please enter mandatory fields");
						break;
					} else if (field.getFieldName().equalsIgnoreCase
							(UserSettingsComponents.LBL_POSTAL_CODE) && fieldValues.get(field
							.getFieldName()).trim().length() < 5) {
						showDialog("Postal code length should be 5 digits");
						flag = false;
						break;
					} else if (field.getFieldName().equalsIgnoreCase
							(UserSettingsComponents.LBL_MOBILE_NUMBER) && fieldValues.get(field
							.getFieldName()).trim().length() < 10) {
						showDialog("Mobile number length should be 10 digits");
						flag = false;
						break;
					} else {
						flag = true;
					}
				}
			}
		}
		if (flag) {
			String email = fieldValues.get(UserSettingsComponents.LBL_EMAIL_ADDRESS);
			if (email != null && !email.isEmpty() && !eMailValidation(email)) {
				alertDialogBuilder = new AlertDialog.Builder(
						SetPreferrenceScreen.this);

				alertDialogBuilder.setMessage("Invalid Email id");
				alertDialogBuilder.setPositiveButton("OK",
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								dialog.dismiss();
								if (emailTextField != null) {
									emailTextField.requestFocus();
								}
							}
						});
				alertDialogBuilder.show();
			} else {
				UserInfoUpdateModel infoObject = new UserInfoUpdateModel();
				infoObject.setUserId(UrlRequestParams.getUid());
				if (Constants.getDeviceId() != null) {
					infoObject.setDeviceId(Constants.getDeviceId());
				}
				if (UrlRequestParams.getHubCityId() != null) {
					infoObject.setHubCitiId(UrlRequestParams.getHubCityId());
				}
				Log.d(TAG, "UrlRequestParams.getUid() : " + UrlRequestParams.getUid());
				Log.d(TAG, "Constants.getDeviceId() : " + Constants.getDeviceId());
				//infoObject.setUnivId();
				for (UserInfoFieldsModel field : mUserInfoModel.getListUserDetails()) {
					createUserInfoObject(infoObject, field.getFieldName(), fieldValues.get(field
							.getFieldName()));
				}
				Log.d(TAG, "on Save infoObject : " + infoObject.toString());
				mRestClient.updateUserDetails(infoObject, new Callback<UpdateUserResponseModel>()
				{
					@Override
					public void success(final UpdateUserResponseModel updateUserResponseModel,
							Response
									response)
					{
						Log.d(TAG, "updateUserDetails : success");
						if (fieldValues.get("Zip Code") != null) {
							Constants.setZipCode(fieldValues.get(UserSettingsComponents
									.LBL_POSTAL_CODE)
									.trim());
						}
						if (isReg) {

							// If regionApp ,Show CityPreferences Screen
							if (Properties.isRegionApp) {
								Intent intent = new Intent(getApplicationContext(),
										CityPreferencesScreen.class);
								intent.putExtra("regacc", true);
								String cityPrefTitle = "cityPrefScreen";
								intent.putExtra("cityPrefTitle", cityPrefTitle);
								intent.putExtra("btnColor", btnColor);
								intent.putExtra("btnFontColor", btnFontColor);
                                if (strPushRegMsg != null) {
                                    intent.putExtra(Constants.PUSH_NOTIFY_MSG,
                                            strPushRegMsg);
                                }
								startActivity(intent);
							}
							// If HubCitiApp ,show CategoryList screen
							else {
								Intent intent = new Intent(getApplicationContext(),
										PreferedCatagoriesScreen.class);
								intent.putExtra("regacc", true);
								String title = "PreferedCatagoriesScreen";
								intent.putExtra("PrefCatScnTitle", title);
								intent.putExtra("btnColor", btnColor);
								intent.putExtra("btnFontColor", btnFontColor);
                                if (strPushRegMsg != null) {
                                    intent.putExtra(Constants.PUSH_NOTIFY_MSG,
                                            strPushRegMsg);
                                }
								startActivity(intent);
							}

						} else {
							alertDialogBuilder = new AlertDialog.Builder(
									SetPreferrenceScreen.this);
							if (updateUserResponseModel.getResponseCode().equals
									("10000")) {
								alertDialogBuilder.setMessage("Saved Successfully");
							} else {
								alertDialogBuilder.setMessage(updateUserResponseModel
										.getResponseText
												());
							}
							alertDialogBuilder.setPositiveButton("OK",
									new DialogInterface.OnClickListener()
									{
										@Override
										public void onClick(DialogInterface dialog,
												int which)
										{
											if (updateUserResponseModel.getResponseCode().equals
													("10000")) {
												dialog.dismiss();
												finish();
											} else {
												dialog.dismiss();
											}
										}
									});
							alertDialogBuilder.show();

						}
					}

					@Override
					public void failure(RetrofitError error)
					{
						Log.d(TAG, "updateUserDetails : RetrofitError");
						if (error.getResponse() != null) {
							Log.d(TAG, "failure : " + error
									.getResponse().getReason());
							showDialog(error
									.getResponse().getReason());
						}
					}
				});
			}
		}

	}

	private void showDialog(String message)
	{
		alertDialogBuilder = new AlertDialog.Builder(
				SetPreferrenceScreen.this);

		alertDialogBuilder.setMessage(message);
		alertDialogBuilder.setPositiveButton("OK",
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog,
							int which)
					{
						dialog.dismiss();
					}
				});
		alertDialogBuilder.show();
	}


	private void getUserInfo()
	{

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		progDialog = new ProgressDialog(this);
		progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progDialog.setMessage("Please Wait..");
		progDialog.setCancelable(false);
		progDialog.show();

		UserInfoRequestModel userInfoRequest = UrlRequestParams.getUserRequest();
		//Log.d(TAG,"userInfoRequest : "+userInfoRequest.toString());
		mRestClient.getUserDetails(userInfoRequest, new Callback<UserInfoModel>()
		{
			@Override
			public void success(UserInfoModel userInfoModel, Response response)
			{
				Log.d(TAG, "########success : " + userInfoModel.toString());
				if (progDialog != null && progDialog.isShowing()) {
					progDialog.dismiss();
				}
				mUserInfoModel = userInfoModel;
				bindViews();
				for (UserInfoFieldsModel object : userInfoModel.getListUserDetails()) {
					if (UserSettingsComponents.LBL_DATEOFBIRTH.equals(object.getFieldName())) {

						String date = object.getValue();
						if ((date != null) && (!date.isEmpty())) {
							String[] dateFormat = date.split("-");
							String setDate;
							if (dateFormat[1].length() < 2) {
								String dateString = getDateString(dateFormat[1]);
								setDate = dateFormat[0] + " " + dateString + "," + " " +
										dateFormat[2];
							} else {
								setDate = dateFormat[0] + " " + dateFormat[1] + "," + " " +
										dateFormat[2];
							}

							fieldValues.put(object.getFieldName(), setDate);
						} else {
							fieldValues.put(object.getFieldName(), object.getValue());
						}
					} else {
						fieldValues.put(object.getFieldName(), object.getValue());
					}

					papulateTextFields(userInfoModel, object);
				}
				if (!isReg) {
					//saveBtn.setBackgroundResource(R.drawable.ic_action_save);
					saveBtn.setText("Save");
				}
			}

			@Override
			public void failure(RetrofitError retrofitError)
			{
				if (progDialog != null && progDialog.isShowing()) {
					progDialog.dismiss();
				}
				if (retrofitError.getResponse() != null) {
					Log.d(TAG, "failure : " + retrofitError
							.getResponse().getReason());
				}
			}
		});
	}

	private String getDateString(String s)
	{
		switch (s) {
			case "1":
				return "0" + s;
			case "2":
				return "0" + s;
			case "3":
				return "0" + s;
			case "4":
				return "0" + s;
			case "5":
				return "0" + s;
			case "6":
				return "0" + s;
			case "7":
				return "0" + s;
			case "8":
				return "0" + s;
			case "9":
				return "0" + s;
			default:
				return "1";
		}
	}

	private void papulateTextFields(UserInfoModel userIfoModel, UserInfoFieldsModel field)
	{

		TableRow row = new TableRow(SetPreferrenceScreen.this);
		TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		row.setLayoutParams(layoutParams);
		TextView textViewFieldName = new TextView(SetPreferrenceScreen.this);
		if (!field.getFieldName().equals(UserSettingsComponents.LBL_IMAGE)) {
			String text = field.getFieldName();
			String colorString = "<font color=#ff0000>" + "*" + "</font><br><br>";
			if ("Mobile".equals(field.getFieldName())) {
				if (field.isRequiredField()) {
					textViewFieldName.setText(Html.fromHtml(text + " #" + colorString));
				} else {
					textViewFieldName.setText(text + " #");
				}
			} else {
				if (field.isRequiredField()) {
					textViewFieldName.setText(Html.fromHtml(text + colorString));
				} else {
					textViewFieldName.setText(text);
				}
			}
		}
		textViewFieldName.setTextColor(Color.BLACK);
		textViewFieldName.setTextSize(16);
		row.addView(textViewFieldName);
		String tag = "";
		if (UserSettingsComponents.textFields.contains(field.getFieldName())) {
			tag = UserSettingsComponents.TEXT_FIELD;
		} else if (field.getFieldName().equals(UserSettingsComponents.LBL_GENDER)) {
			tag = UserSettingsComponents.RADIO_BUTTON;
		} else if (UserSettingsComponents.pickerFields.contains(field.getFieldName())) {
			tag = UserSettingsComponents.PICKER;
		} else if (field.getFieldName().equals(UserSettingsComponents.LBL_IMAGE)) {
			tag = UserSettingsComponents.IMAGE;
		}

		switch (tag) {
			case UserSettingsComponents.TEXT_FIELD:
				final TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams
						.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1.0f);
				final EditText textField = new EditText(
						SetPreferrenceScreen.this);
				if (UserSettingsComponents.LBL_DATEOFBIRTH.equals(field.getFieldName())) {
					String date = field.getValue();
					if ((date != null) && (date != "")) {
						String[] dateFormat = date.split("-");
						year = Integer.parseInt(dateFormat[2]);
						month = getMonthInt(dateFormat[0]);
						day = Integer.parseInt(dateFormat[1]);
						String setDate;
						if (dateFormat[1].length() < 2) {
							String dateString = getDateString(dateFormat[1]);
							setDate = dateFormat[0] + " " + dateString + "," + " " +
									dateFormat[2];
						} else {
							setDate = dateFormat[0] + " " + dateFormat[1] + "," + " " +
									dateFormat[2];
						}
						textField.setText(setDate);
					} else {
						Calendar c = Calendar.getInstance();
						year = c.get(Calendar.YEAR);
						month = c.get(Calendar.MONTH);
						day = c.get(Calendar.DAY_OF_MONTH);

						textField.setText(field.getValue());
					}

				} else {
					textField.setText(field.getValue());
				}
				textField.setTag(field.getFieldName());
				textField.setTextColor(Color.BLACK); // set the color
				textField.setCursorVisible(true);
				textField.setLayoutParams(params);
				textField.setSingleLine(true);
				textField.setTextSize(16);
				textField.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);

				if (UserSettingsComponents.LBL_FIRST_NAME.equals(field.getFieldName()) ||
						UserSettingsComponents.LBL_LAST_NAME.equals(field
								.getFieldName())) {
					textField.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
					textField.setFilters(new InputFilter[] {
							new InputFilter.LengthFilter
									(20)
					});
				}
				if (UserSettingsComponents.LBL_EMAIL_ADDRESS.equals(field.getFieldName())) {
					textField.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
					textField.setFilters(new InputFilter[] {
							new InputFilter.LengthFilter
									(50)

					});
					emailTextField = textField;
				}
				if (UserSettingsComponents.LBL_POSTAL_CODE.equals(field.getFieldName())) {
					textField.setInputType(InputType.TYPE_CLASS_NUMBER);
					textField.setFilters(new InputFilter[] {
							new InputFilter.LengthFilter
									(5)
					});
				}

				if (UserSettingsComponents.LBL_MOBILE_NUMBER.equals(field.getFieldName())) {
					textField.setInputType(InputType.TYPE_CLASS_NUMBER);
					textField.setTextSize(16);
					textField
							.setFilters(new InputFilter[] {
									new InputFilter.LengthFilter(
											10)
							});

				}
				textField.addTextChangedListener(new TextWatcher()
				{

					@Override
					public void afterTextChanged(Editable s)
					{

					}

					@Override
					public void beforeTextChanged(CharSequence s,
							int start, int count, int after)
					{
					}

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count)
					{
						fieldValues.put(textField.getTag().toString(),
								textField.getText().toString().trim());
					}

				});
				if (UserSettingsComponents.LBL_DATEOFBIRTH.equals(field.getFieldName())) {
					textField
							.setFilters(new InputFilter[] {
									new InputFilter.LengthFilter(
											12)
							});
					textField.setInputType(InputType.TYPE_NULL);


					// Button listener to show date picker dialog
					textField.setOnTouchListener(new OnTouchListener()
					{
						@SuppressWarnings("deprecation")
						@Override
						public boolean onTouch(View v, MotionEvent event)
						{
							datePickerTextField = textField;
							showDialog(DATE_PICKER_ID);
							return false;
						}
					});
				}
				row.addView(textField);
				break;
			case UserSettingsComponents.PICKER:
				final Spinner spinner = new Spinner(SetPreferrenceScreen.this);
				spinner.setTag(field.getFieldName());
				spinner.setMinimumHeight(20);
				ArrayList<String> optionsArray = new ArrayList<>();
				optionsArray.add(UserSettingsComponents.SELECT);
				switch (field.getFieldName()) {
					case UserSettingsComponents.LBL_EDUCATION:
						ArrayList<UserEducationOptionsModel> arEducationList = userIfoModel
								.getArEducationList();
						for (UserEducationOptionsModel object : arEducationList) {
							optionsArray.add(object.getEducatLevelName());
						}
						break;
					case UserSettingsComponents.LBL_MARITAL_STATUS:
						ArrayList<UserMaritalOptionsModel> arMaritalList = userIfoModel
								.getArMaritalList();
						for (UserMaritalOptionsModel object : arMaritalList) {
							optionsArray.add(object.getmStatusName());
						}
						break;
					case UserSettingsComponents.LBL_INCOME_RANGE:
						ArrayList<UserIncomeRangeOptionsModel> incomeRangeList = userIfoModel
								.getArIncomeRangeList();
						for (UserIncomeRangeOptionsModel object : incomeRangeList) {
							optionsArray.add(object.getIcRangeName());
						}
						break;
				}
				ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
						SetPreferrenceScreen.this,
						android.R.layout.simple_spinner_item,
						optionsArray);
				arrayAdapter
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinner.setAdapter(arrayAdapter);
				spinner.setSelection(optionsArray.indexOf(field.getValue()));
				spinner.setOnItemSelectedListener(new OnItemSelectedListener()
				{

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View arg1, int pos, long id)
					{
						spinner.setSelection(pos);
						fieldValues.put(spinner.getTag().toString(), String.valueOf(pos));
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0)
					{
					}

				});
				row.addView(spinner);
				break;
			case UserSettingsComponents.IMAGE:
				TableRow.LayoutParams rowParams = new TableRow.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				row.setGravity(Gravity.CENTER);
				row.setLayoutParams(rowParams);
				ImageView imageView = new ImageView(SetPreferrenceScreen.this);
				imageView.setTag(field.getFieldName());
				imageView.setLayoutParams(new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				rowParams.span = 2;
				new ImageLoaderAsync(imageView, true).execute(field.getValue()
						.trim());
				row.addView(imageView, rowParams);
				break;
			case UserSettingsComponents.RADIO_BUTTON:
				final RadioGroup radioGroup = new RadioGroup(
						getApplicationContext());
				radioGroup.setTag(field.getFieldName());
				radioGroup.setOrientation(RadioGroup.HORIZONTAL);
				ArrayList<String> genderOptions = new ArrayList<>();
				genderOptions.add(UserSettingsComponents.MALE);
				genderOptions.add(UserSettingsComponents.FEMALE);

				for (String text : genderOptions) {
					RadioButton radioButton = new RadioButton(getBaseContext());
					radioButton.setText(text);
					radioButton.setTextColor(Color.BLACK);
					radioButton.setId(genderOptions.indexOf(text));
					if (String.valueOf(genderOptions.indexOf(text)).equalsIgnoreCase(field
							.getValue())) {
						radioButton.setChecked(true);
					}
					radioGroup.addView(radioButton);
				}
				row.addView(radioGroup);
				radioGroup
						.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
						{

							@Override
							public void onCheckedChanged(RadioGroup group,
									int checkedId)
							{
								int index = group
										.indexOfChild(findViewById(group
												.getCheckedRadioButtonId()));
								fieldValues.put(radioGroup.getTag().toString(),
										Integer.toString(index));

							}
						});
				break;
		}
		if (setPreferenceTableLayout == null) {
			setPreferenceTableLayout = (TableLayout) findViewById(R.id.setPreferenceTableLayout);
		}
		setPreferenceTableLayout.addView(row,
				new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT));
		if (UserSettingsComponents.LBL_EMAIL_ADDRESS.equals(field.getFieldName())) {
			TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(
					LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT, 1f
			);
			textViewParams.setMargins(4, 4, 4, 4);
			textViewParams.span = 2;
			TextView changePasswordTextView = new TextView(SetPreferrenceScreen.this);
			changePasswordTextView.setText(getResources().getString(R.string
					.account_change_password));
			changePasswordTextView.setSingleLine(true);
			changePasswordTextView.setTextColor(getResources().getColor(R.color.royalblue));
			changePasswordTextView.setGravity(Gravity.END);
			changePasswordTextView.setPadding(2, 2, 2, 2);
			changePasswordTextView.setLayoutParams(textViewParams);
			changePasswordTextView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					CommonConstants.resetPassword(SetPreferrenceScreen.this, true);
				}
			});
			TableRow passwordRow = new TableRow(SetPreferrenceScreen.this);
			passwordRow.setLayoutParams(new TableLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			passwordRow.addView(changePasswordTextView);
			setPreferenceTableLayout.addView(passwordRow,
					new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT));
		}

	}

	private void bindViews()
	{
		saveBtn = (Button) findViewById(R.id.account_savebtn);
		saveBtn.setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
		Button privacyBtn = (Button) findViewById(R.id.account_privacypolicybtn);
		privacyBtn.setTextColor(Color.parseColor(Constants.getNavigationBarValues
				("titleTxtColor")));
		Button skip = (Button) findViewById(R.id.account_skipbtn);
		skip.setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
		skip.setVisibility(View.INVISIBLE);
		saveBtn.setVisibility(View.VISIBLE);
		privacyBtn.setVisibility(View.VISIBLE);
		saveBtn.setEnabled(true);
		privacyBtn.setOnClickListener(SetPreferrenceScreen.this);
		saveBtn.setOnClickListener(SetPreferrenceScreen.this);
		skip.setOnClickListener(SetPreferrenceScreen.this);
		privacyBtn.setOnClickListener(SetPreferrenceScreen.this);
	}

	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id) {
			case DATE_PICKER_ID:
				return datePicker();

			default:
				break;

		}
		return null;
	}

	private DatePickerDialog datePicker()
	{
		String[] dob;
		dob = datePickerTextField.getText().toString().split("-");
		DatePickerDialog dialog;
		if (dob.length > 2) {
			dialog = new DatePickerDialog(this, pickerListener,
					Integer.parseInt(dob[2]), getMonthInt(dob[0]),
					Integer.parseInt(dob[1]));

		} else {

			dialog = new DatePickerDialog(this, pickerListener, year, month,
					day);
		}
		dialog.getDatePicker().setMaxDate(new Date().getTime());
		return dialog;

	}

	private int getMonthInt(String month)
	{

		if ("JANUARY".equalsIgnoreCase(month)) {
			return 0;
		}
		if ("FEBRUARY".equalsIgnoreCase(month)) {
			return 1;
		}
		if ("MARCH".equalsIgnoreCase(month)) {
			return 2;
		}
		if ("APRIL".equalsIgnoreCase(month)) {
			return 3;
		}
		if ("MAY".equalsIgnoreCase(month)) {
			return 4;
		}
		if ("JUNE".equalsIgnoreCase(month)) {
			return 5;
		}
		if ("JULY".equalsIgnoreCase(month)) {
			return 6;
		}
		if ("AUGUST".equalsIgnoreCase(month)) {
			return 7;
		}
		if ("SEPTEMBER".equalsIgnoreCase(month)) {
			return 8;
		}
		if ("OCTOBER".equalsIgnoreCase(month)) {
			return 9;
		}
		if ("NOVEMBER".equalsIgnoreCase(month)) {
			return 10;
		}
		if ("DECEMBER".equalsIgnoreCase(month)) {
			return 11;
		}
		if ("JAN".equalsIgnoreCase(month)) {
			return 0;
		}
		if ("FEB".equalsIgnoreCase(month)) {
			return 1;
		}
		if ("MAR".equalsIgnoreCase(month)) {
			return 2;
		}
		if ("APR".equalsIgnoreCase(month)) {
			return 3;
		}
		if ("MAY".equalsIgnoreCase(month)) {
			return 4;
		}
		if ("JUN".equalsIgnoreCase(month)) {
			return 5;
		}
		if ("JUL".equalsIgnoreCase(month)) {
			return 6;
		}
		if ("AUG".equalsIgnoreCase(month)) {
			return 7;
		}
		if ("SEP".equalsIgnoreCase(month)) {
			return 8;
		}
		if ("OCT".equalsIgnoreCase(month)) {
			return 9;
		}
		if ("NOV".equalsIgnoreCase(month)) {
			return 10;
		}
		if ("DEC".equalsIgnoreCase(month)) {
			return 11;
		}

		return -1;
	}

	private String getMonthString(int month)
	{

		switch (month) {
			case 0:
				return "JAN";
			case 1:
				return "FEB";
			case 2:
				return "MAR";
			case 3:
				return "APR";
			case 4:
				return "MAY";
			case 5:
				return "JUN";
			case 6:
				return "JUL";
			case 7:
				return "AUG";
			case 8:
				return "SEP";
			case 9:
				return "OCT";
			case 10:
				return "NOV";
			case 11:
				return "DEC";

			default:
				return "1";
		}

	}

	private final DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog
			.OnDateSetListener()
	{

		// when dialog box is closed, below method will be called.

		@Override
		public void onDateSet(DatePicker arg0, int selectedYear,
				int selectedMonth, int selectedDay)
		{

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;
			int length = (int) (Math.log10(day) + 1);
			if (length < 2) {
				String dateString = getDateString(String.valueOf(day));
				day = Integer.parseInt(dateString);
				// Show selected date
				datePickerTextField.setText(new StringBuilder().append(getMonthString(month))
						.append(" ").append(dateString).append(", ").append(year)
						.append(""));
			} else {
				// Show selected date
				datePickerTextField.setText(new StringBuilder().append(getMonthString(month))
						.append(" ").append(day).append(", ").append(year)
						.append(""));
			}

			fieldValues.put(datePickerTextField.getTag().toString(), datePickerTextField.getText()
					.toString());

		}
	};

	private void createUserInfoObject(UserInfoUpdateModel object, String key, String value)
	{
		switch (key) {
			case UserSettingsComponents.LBL_FIRST_NAME:
				object.setFirstName(value);
				break;
			case UserSettingsComponents.LBL_LAST_NAME:
				object.setLastName(value);
				break;
			case UserSettingsComponents.LBL_DATEOFBIRTH:
				if(value != null && !value.isEmpty())
				{
					object.setDob(value);
				}
				else {
					object.setDob(null);
				}
				break;
			case UserSettingsComponents.LBL_EDUCATION:
				object.setEducatonLevelId(value);
				break;
			case UserSettingsComponents.LBL_EMAIL_ADDRESS:
				object.setEmail(value);
				break;
			case UserSettingsComponents.LBL_GENDER:
				object.setGender(value);
				break;
			case UserSettingsComponents.LBL_INCOME_RANGE:
				object.setIncomeRangeId(value);
				break;
			case UserSettingsComponents.LBL_MARITAL_STATUS:
				object.setMartialStatusId(value);
				break;
			case UserSettingsComponents.LBL_MOBILE_NUMBER:
				object.setPhoneNum(value);
				break;
			case UserSettingsComponents.LBL_POSTAL_CODE:
				object.setPostalCode(value);
				break;
			case UserSettingsComponents.LBL_IMAGE:
				object.setImage(value);
				break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
