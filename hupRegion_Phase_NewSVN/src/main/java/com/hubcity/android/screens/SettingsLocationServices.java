package com.hubcity.android.screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.SaveUserSettingsAsync;
import com.scansee.hubregion.R;

public class SettingsLocationServices extends CustomTitleBar implements
		OnClickListener
{
	LocationManager locationManager;
	ImageView gps_checkBox;
	ImageView push_checkBox;

	boolean state = false;
	boolean isPushChecked = false;

	EditText mRadius;
	String radius;
	Button mSave;

	ProgressDialog progDialog;
	String returnValue;

	String responseText;

	SharedPreferences preferences;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.application_setting);

		preferences = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);

		new GetUserSettingsAsync(SettingsLocationServices.this, true, true).execute();

		title.setSingleLine(false);
		title.setMaxLines(2);
		title.setText("Settings");
		leftTitleImage.setVisibility(View.GONE);

		mRadius = (EditText) findViewById(R.id.settings_edittext_radius);
		mSave = (Button) findViewById(R.id.settings_button_save);

		mRadius.setOnTouchListener(new View.OnTouchListener()
		{

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				mRadius.setFocusableInTouchMode(true);
				return false;
			}
		});

		mSave.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				if (mRadius.getText().toString() != null
						&& mRadius.getText().toString().length() != 0) {
					radius = mRadius.getText().toString();
					mRadius.setFocusable(false);
					new SaveUserSettingsAsync(SettingsLocationServices.this, String.valueOf
							(isPushChecked),state, true).execute(radius);
				} else {
					callToast();
				}

			}
		});
		// @Rekha: Check Push Notification value

		isPushChecked = preferences.getBoolean("push_status", false);

		push_checkBox = (ImageView) findViewById(R.id.pushsetting);
		push_checkBox.setOnClickListener(this);

		if (isPushChecked) {
			push_checkBox.setImageResource(R.drawable.checkbox_marked);

		} else {
			push_checkBox.setImageResource(R.drawable.checkbox_unmarked);
		}

	}

	protected void callToast()
	{

		Toast.makeText(this, "Please enter the valid Radius",
				Toast.LENGTH_SHORT).show();

	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onResume()
	{
		super.onResume();

		try {
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			gps_checkBox = (ImageView) findViewById(R.id.locsetting);
			String provider = Settings.Secure.getString(getContentResolver(),
					Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			if (provider.contains("gps")) {
				gps_checkBox.setImageResource(R.drawable.checkbox_marked);
				state = true;
			} else {
				gps_checkBox.setImageResource(R.drawable.checkbox_unmarked);
				state = false;
			}
			gps_checkBox.setOnClickListener(this);


		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View view)
	{
		switch (view.getId()) {
			case R.id.locsetting:
				locSetting();
				break;

			case R.id.pushsetting:
				pushSetting();
				break;

			default:
				break;
		}

	}

	private void pushSetting()
	{
		isPushChecked = !isPushChecked;

		if (isPushChecked) {
			push_checkBox.setImageResource(R.drawable.checkbox_marked);

		} else {
			push_checkBox.setImageResource(R.drawable.checkbox_unmarked);
		}
	}

	private void locSetting()
	{
		if (state) {
			state = false;
			gps_checkBox.setImageResource(R.drawable.checkbox_unmarked);
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		} else {
			state = true;
			gps_checkBox.setImageResource(R.drawable.checkbox_marked);
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}

	}

	public void setUserRadius(String radius)
	{
		mRadius.setText(radius);
		mRadius.setSelection(radius.length());
	}

}
