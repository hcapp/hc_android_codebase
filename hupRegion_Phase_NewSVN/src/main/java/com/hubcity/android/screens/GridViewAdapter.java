package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class GridViewAdapter extends ArrayAdapter<MainMenuBO> {

    Activity mContext;
    int resourceId;
    ArrayList<MainMenuBO> data;
    private CustomImageLoader customImageLoader;
    String btnFontColor, btnColor, mFontColor, smFontColor, fontColor;
    String level;
    LinearLayout bannerParent;
    private static int width = -1;
    private static int height = -1;


    // added by suganya
    public GridViewAdapter(Activity context, int layoutResourceId,
                           ArrayList<MainMenuBO> data, String menuLevel, int _width,
                           int _height, String mFontColor, String smFontColor, LinearLayout bannerParent, boolean isSquare) {
        super(context, layoutResourceId, data);
        this.mContext = context;
        this.resourceId = layoutResourceId;
        this.data = data;
        level = menuLevel;
        if (isSquare)
            customImageLoader = new CustomImageLoader(context, false);
        else
            customImageLoader = new CustomImageLoader(context, true);
        width = _width;
        height = _height;
        this.bannerParent = bannerParent;
        this.mFontColor = mFontColor;
        this.smFontColor = smFontColor;
    }

    // added by suganya
    private int dpTopx(int dP) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dP,
                mContext.getResources().getDisplayMetrics());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MainMenuBO item = data.get(position);
        String itmItemImgUrl = item.getmItemImgUrl();

        View itemView = convertView;
        ViewHolder holder = null;

        if (itemView == null) {
            final LayoutInflater layoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = layoutInflater.inflate(resourceId, parent, false);

            holder = new ViewHolder();
            holder.imgItem = (ImageView) itemView.findViewById(R.id.imgItem);
            holder.txtItem = (TextView) itemView.findViewById(R.id.txtItem);
            // added by suganya
            // Default 4 x 3 grid is displayed
            // XXX: dpTopx(50) is for TextView, if the value is changed in
            // XML
            // it shd be changed here too
            LinearLayout.LayoutParams layoutParams;
            if (bannerParent.getVisibility() == View.VISIBLE) {
                layoutParams = new LinearLayout.LayoutParams(
                        (width / 4) - dpTopx(14), (height / 3) - dpTopx(16)
                        - dpTopx(70));
            } else {
                layoutParams = new LinearLayout.LayoutParams(
                        (width / 4) - dpTopx(14), (height / 3) - dpTopx(16)
                        - dpTopx(55));
            }
            layoutParams.gravity = Gravity.CENTER_HORIZONTAL
                    | Gravity.CENTER_VERTICAL;
            layoutParams.setMargins(dpTopx(4), dpTopx(4), dpTopx(4), dpTopx(4));
            holder.imgItem.setLayoutParams(layoutParams);

            itemView.setTag(holder);

        } else {
            holder = (ViewHolder) itemView.getTag();
        }

        String itemName = item.getmItemName();

        customImageLoader.displayImage(itmItemImgUrl, mContext, holder.imgItem);

        holder.txtItem.setText(itemName);

        if (level != null && "1".equals(level)) {
            fontColor = mFontColor;
        } else {
            fontColor = smFontColor;
        }

        if (fontColor != null) {
            holder.txtItem.setTextColor(Color.parseColor(fontColor));
        }

        return itemView;

    }

    static class ViewHolder {
        ImageView imgItem;
        TextView txtItem;
    }
}