package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class OptionSortByList extends CustomTitleBar implements OnClickListener {


    TableRow mTableRowNameSort;
    TableRow mTableRowDistanceSort;
    TableRow mTableRowTypeSort;
    TableRow mTableRowAtoZ;
    TableRow mTableRowCitySort;
    TableRow mTableRowCityList;
    TableRow mTableRowGroupTitle;

    public static String sortChoice = "Distance";
    public static String groupChoice = "";
    public static boolean isSortByType = false;
    public static boolean isSorted = false;

    String name, distance, type, atoz, city;
    TextView nameSort;
    TextView distanceSort;
    TextView typeSort;
    TextView citySort;

    CheckBox nameSortCheck;
    CheckBox distanceSortCheck;
    CheckBox typeSortCheck;
    CheckBox citySortCheck;
    CheckBox atozSortCheck;

    LinearLayout ll;
    Activity activity;

    String className = "";
    CitySort citySorting;
    boolean isFilterFlag;

    boolean isAtoZ;

    // Addison filter parameters
    LinearLayout mainLayout;
    String catName = "";
    String catId = "";
    public static String filterIds = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.option_sort_list);

        isSorted = false;
        activity = this;

        ll = (LinearLayout) findViewById(R.id.city_ll);
        CitySort.isDefaultCitySort = false;

        // @Rekha
        // Check for Addison Filters
        mainLayout = (LinearLayout) findViewById(R.id.main_layout);

        mTableRowNameSort = (TableRow) findViewById(R.id.events_sortby_name_tableRow);
        mTableRowDistanceSort = (TableRow) findViewById(R.id.events_sortby_distance_tableRow);
        mTableRowTypeSort = (TableRow) findViewById(R.id.events_sortby_type_tableRow);
        mTableRowAtoZ = (TableRow) findViewById(R.id.find_single_category_tableRow3);
        mTableRowCitySort = (TableRow) findViewById(R.id.sortby_city_tableRow);
        mTableRowCityList = (TableRow) findViewById(R.id.city_list_tableRow);
        mTableRowGroupTitle = (TableRow) findViewById(R.id.tableRow1);

        mTableRowAtoZ.setOnClickListener(this);
        mTableRowNameSort.setOnClickListener(this);
        mTableRowDistanceSort.setOnClickListener(this);
        mTableRowTypeSort.setOnClickListener(this);
        mTableRowCitySort.setOnClickListener(this);

        nameSort = (TextView) findViewById(R.id.event_textView_sort_name);
        distanceSort = (TextView) findViewById(R.id.event_textView_sort_distance);
        typeSort = (TextView) findViewById(R.id.event_textView_sort_type);
        citySort = (TextView) findViewById(R.id.textView_sort_city);

        citySort.setOnClickListener(this);

        nameSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox2);
        distanceSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox3);
        typeSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox4);
        atozSortCheck = (CheckBox) findViewById(R.id.event_group_checkBox2);
        citySortCheck = (CheckBox) findViewById(R.id.sortby_city_checkBox);

        if (getIntent().hasExtra("Class")) {
            className = getIntent().getExtras().getString("Class");
        }

        if (getIntent().hasExtra("catName")) {
            catName = getIntent().getExtras().getString("catName");
        }

        isFilterFlag = "Filter".equals(className);

        mTableRowAtoZ.setVisibility(View.GONE);
        citySort.setVisibility(View.GONE);
        mTableRowTypeSort.setVisibility(View.GONE);
        mTableRowGroupTitle.setVisibility(View.GONE);

        // check for sort selection by type for city experience
//        if (isSortByType) {
//            mTableRowGroupTitle.setVisibility(View.VISIBLE);
//            mTableRowTypeSort.setVisibility(View.VISIBLE);
//
//            if (Properties.isRegionApp) {
//                mTableRowAtoZ.setVisibility(View.VISIBLE);
//                citySort.setVisibility(View.VISIBLE);
//            }
//
//        }

        this.title.setText("Sort");
        this.backImage.setVisibility(View.GONE);

        this.leftTitleImage.setBackgroundResource(R.drawable.ic_action_cancel);
        this.leftTitleImage.setTag("Cancel");

        this.leftTitleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        this.rightImage.setImageBitmap(null);
        this.rightImage.setBackgroundResource(R.drawable.ic_action_accept);
        this.rightImage.setTag("Done");
        this.rightImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    isSorted = true;
                    name = String.valueOf(nameSortCheck.getVisibility());
                    distance = String.valueOf(distanceSortCheck.getVisibility());
                    city = String.valueOf(citySortCheck.getVisibility());
                    type = String.valueOf(typeSortCheck.getVisibility());
                    atoz = String.valueOf(atozSortCheck.getVisibility());

                    if ("0".equals(name)) {

                        sortChoice = "name";
                    } else if ("0".equals(distance)) {
                        sortChoice = "Distance";
                    } else if ("0".equals(city)) {
                        sortChoice = "city";
                    }

                    if ("0".equals(type)) {
                        groupChoice = "type";
                    } else if ("0".equals(atoz)) {

                        groupChoice = "atoz";
                    } else {
                        groupChoice = "";
                    }

                    setResult(02);
                    finish();

            }
        });

        if (sortChoice != null) {
            if ("name".equals(sortChoice) || "RetailerName".equals(sortChoice)) {
                nameSortCheck.setVisibility(View.VISIBLE);
                distanceSortCheck.setVisibility(View.INVISIBLE);
                citySortCheck.setVisibility(View.INVISIBLE);
            } else if ("Distance".equals(sortChoice)) {
                distanceSortCheck.setVisibility(View.VISIBLE);
                nameSortCheck.setVisibility(View.INVISIBLE);
                citySortCheck.setVisibility(View.INVISIBLE);
            } else if ("city".equals(sortChoice)) {
                citySortCheck.setVisibility(View.VISIBLE);
                nameSortCheck.setVisibility(View.INVISIBLE);
                distanceSortCheck.setVisibility(View.INVISIBLE);

                CitySort.isDefaultCitySort = true;
            }
        }

        if (groupChoice != null) {
            if ("atoz".equals(groupChoice)) {
                isAtoZ = true;
                atozSortCheck.setVisibility(View.VISIBLE);
                typeSortCheck.setVisibility(View.INVISIBLE);
            } else if ("type".equals(groupChoice)) {
                typeSortCheck.setVisibility(View.VISIBLE);
                atozSortCheck.setVisibility(View.INVISIBLE);
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.events_sortby_name_tableRow:
                eventsSortbyName();
                break;
            case R.id.events_sortby_distance_tableRow:
                eventsSortbyDistance();
                break;
            case R.id.sortby_city_tableRow:
                sortbyCity();
                break;
            case R.id.textView_sort_city:
                sortbyCity();
                break;
            case R.id.find_single_category_tableRow3:
                groupByAtoZ();
                break;
            case R.id.events_sortby_type_tableRow:
                groupByType();
                break;
            default:
                break;

        }

    }

    private void groupByType() {
        isAtoZ = false;
        atozSortCheck.setVisibility(View.INVISIBLE);
        typeSortCheck.setVisibility(View.VISIBLE);
    }

    private void groupByAtoZ() {

        atozSortCheck.setVisibility(View.VISIBLE);

        typeSortCheck.setVisibility(View.INVISIBLE);
    }

    private void eventsSortbyName() {
        nameSortCheck.setVisibility(View.VISIBLE);
        distanceSortCheck.setVisibility(View.INVISIBLE);
        citySortCheck.setVisibility(View.INVISIBLE);
        ll.setVisibility(View.GONE);

    }

    private void eventsSortbyDistance() {
        distanceSortCheck.setVisibility(View.VISIBLE);
        nameSortCheck.setVisibility(View.INVISIBLE);
        citySortCheck.setVisibility(View.INVISIBLE);
        ll.setVisibility(View.GONE);

    }

    private void sortbyCity() {
        citySortCheck.setVisibility(View.VISIBLE);
        nameSortCheck.setVisibility(View.INVISIBLE);
        distanceSortCheck.setVisibility(View.INVISIBLE);
        ll.setVisibility(View.GONE);
        citySorting.sortAction();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
