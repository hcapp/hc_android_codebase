package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.FundraiserEntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;

public class FundraiserActivity extends CustomTitleBar implements
        OnClickListener, OnItemClickListener {
    ArrayList<Item> items = new ArrayList<>();

    Button moreInfo;
    int lastvisitProdId = 0, minProdId = 0;
    ListView listview;

    ProgressDialog progDialog;
    String responseText = "";
    PrepareFundraisertList prepareFundraiserList = new PrepareFundraisertList();
    View moreResultsView;
    int previousPosition = 0;

    String mItemId;
    String bottomBtnId = null;
    String postalCode = null;

    String group = null;
    String sort = null;

    boolean flag = false;
    boolean nextPage = false;
    LocationManager locationManager;
    String latitude = null;
    String longitude = null;
    String catId = "";
    String catName = "";
    String mLinkId = "";
    String searchKey = "";
    String mainMenuId = "";
    int isDeptFlag;

    ArrayList<PrepareFundraisertList> fundraiserList;

    LinearLayout linearLayout = null;
    BottomButtons bb = null;
    Activity activity;

    boolean hasBottomBtns = false;
    boolean enableBottomButton = true;
    boolean isFirst, isAlreadyLoading;

    String retailerId = "";
    String retailLocationId = "", retListId = "";
    String deptId = "";

    FundraiserEventListAsyncTask fundListAsyncTask;
    HubCityContext mHubCity;
    private CustomNavigation customNaviagation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fundraiser_activity);

        try {
            CommonConstants.hamburgerIsFirst = true;
            isFirst = true;

            activity = FundraiserActivity.this;

            mHubCity = (HubCityContext) getApplicationContext();

            // Initiating Bottom button class
            if (null != BottomButtonInfoSingleton.getBottomButtonsSingleton()) {
                bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
                // Add screen name when needed
                bb.setActivityInfo(activity, "");
            }

            if (getIntent().hasExtra("deptId")
                    && "department".equalsIgnoreCase(mHubCity
                    .getFundSortSelection())) {
                deptId = getIntent().getExtras().getString("deptId");
            }

            if (getIntent().hasExtra("retailerId")) {
                retailerId = getIntent().getExtras().getString("retailerId");
            }
            if (getIntent().hasExtra("retListId")) {
                retListId = getIntent().getExtras().getString("retListId");
            }
            if (getIntent().hasExtra("retailLocationId")) {
                retailLocationId = getIntent().getExtras().getString(
                        "retailLocationId");
            }

            if (getIntent().hasExtra("catName")) {
                catName = getIntent().getExtras().getString("catName");
            }
            if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }

            if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
                bottomBtnId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }

            if (getIntent().hasExtra("mLinkId")) {
                mLinkId = getIntent().getExtras().getString("mLinkId");
            } else if (getIntent().hasExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA)) {
                mLinkId = getIntent().getExtras().getString(
                        Constants.BOTTOM_LINK_ID_INTENT_EXTRA);
            }

            if (getIntent().hasExtra(Constants.SEARCH_KEY_INTENT_EXTRA)) {
                searchKey = getIntent().getExtras().getString(
                        Constants.SEARCH_KEY_INTENT_EXTRA);
            }
            if (getIntent().hasExtra(Constants.CAT_IDS_INTENT_EXTRA)) {
                catId = getIntent().getExtras().getString(
                        Constants.CAT_IDS_INTENT_EXTRA);
            }

            linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
            linearLayout.setVisibility(View.INVISIBLE);

            moreResultsView = getLayoutInflater().inflate(
                    R.layout.events_pagination, listview, true);
            moreInfo = (Button) moreResultsView
                    .findViewById(R.id.events_view_more_button);
            moreInfo.setOnClickListener(this);
            listview = (ListView) findViewById(R.id.fundraiser_events_listView);
            progDialog = new ProgressDialog(FundraiserActivity.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();
            callFundAsyncTask();

            title.setText("Fundraisers");

//			rightImage.setBackgroundResource(R.drawable.ic_action_sort_by_size);
//			rightImage.setVisibility(View.GONE);
            leftTitleImage.setVisibility(View.INVISIBLE);

//			rightImage.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//
//					Intent sortIntent = new Intent(FundraiserActivity.this,
//							FundraisersSortByActivity.class);
//					sortIntent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
//							mItemId);
//					sortIntent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
//							bottomBtnId);
//					sortIntent.putExtra("isDeptFlag", isDeptFlag);
//					sortIntent.putExtra("retailerId", retailerId);
//					sortIntent.putExtra("retListId", retListId);
//					sortIntent.putExtra("retailLocationId", retailLocationId);
//					sortIntent.putExtra("retailerEvents", getIntent().getExtras()
//							.getString("retailerEvents"));
//					sortIntent.putExtra("mLinkId", mLinkId);
//					sortIntent.putExtra("catName", catName);
//					sortIntent.putExtra("catId", catId);
//					sortIntent.putExtra("deptId", deptId);
//
//					if (getIntent().hasExtra("isDeals")
//							&& getIntent().getExtras().getBoolean("isDeals")) {
//						sortIntent.putExtra("isDeals", getIntent().getExtras()
//								.getBoolean("isDeals"));
//					}
//
//					startActivity(sortIntent);
//
//					cancelFundListTask();
//					finish();
//
//
//				}
//			});

            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelFundListTask();
                    finish();

                }
            });

            //user for hamburger in modules
            drawerIcon.setVisibility(View.VISIBLE);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.events_view_more_button:
                fundsViewMoreButton();
                break;
            default:
                break;
        }
    }

    public void fundsViewMoreButton() {
        enableBottomButton = false;
        callFundAsyncTask();

    }

    @SuppressWarnings("deprecation")
    public void getGPSValues() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                latitude = String.valueOf(locNew.getLatitude());
                longitude = String.valueOf(locNew.getLongitude());

            } else {
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());
                } else {
                    latitude = CommonConstants.LATITUDE;
                    longitude = CommonConstants.LONGITUDE;
                }
            }
        }
    }

    public boolean isJSONArray(JSONObject jsonObject, String value) {
        boolean isArray = false;

        JSONObject isJSONObject = jsonObject.optJSONObject(value);
        if (isJSONObject == null) {
            JSONArray isJSONArray = jsonObject.optJSONArray(value);
            if (isJSONArray != null) {
                isArray = true;
            }
        }
        return isArray;
    }

    class PrepareFundraisertList {
        String header;
        ArrayList<DetailsByGroup> fundraiserLists = new ArrayList<>();

    }

    class DetailsByGroup {
        String fundId;
        String fundName;
        String eventName;
        String startDate;
        String fundCatId;
        String fundCatName;
        String imagePath;
        String isOngoing;
    }

    public class ListDetails {
        ArrayList<PrepareFundraisertList> fundraiserList = new ArrayList<>();
    }

    public class FundraiserEventListAsyncTask extends
            AsyncTask<String, Void, String> {
        String returnValue = "sucessfull";
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        ListDetails objListDetails = new ListDetails();

        protected void onPreExecute() {

            getGPSValues();

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if ("sucessfull".equals(returnValue)) {

                    if (nextPage) {

                        try {
                            listview.removeFooterView(moreResultsView);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        // moreInfo.setVisibility(View.VISIBLE);

                        listview.addFooterView(moreResultsView);

                    } else {
                        listview.removeFooterView(moreResultsView);
                    }

                    fundraiserList = new ArrayList<>();
                    fundraiserList = objListDetails.fundraiserList;
                    boolean isHeaderExist = false;
                    int ind = 0;
                    for (int i = 0; i < fundraiserList.size(); i++) {

                        for (int index = i; index < items.size(); index++) {
                            if (items.get(index).isSection()) {
                                SectionItem item = (SectionItem) items.get(index);

                                if (item.getTitle().equals(
                                        fundraiserList.get(i).header)) {

                                    ind = index;
                                    isHeaderExist = true;
                                    break;
                                }

                            }

                        }

                        if (isHeaderExist) {

                            for (int j = 0; j < fundraiserList.get(i).fundraiserLists
                                    .size(); j++) {

                                items.add(
                                        ++ind,
                                        new FundraiserEntryItem(
                                                fundraiserList.get(i).fundraiserLists
                                                        .get(j).fundId,
                                                fundraiserList.get(i).fundraiserLists
                                                        .get(j).fundName,
                                                fundraiserList.get(i).fundraiserLists
                                                        .get(j).startDate,
                                                fundraiserList.get(i).fundraiserLists
                                                        .get(j).fundCatId,
                                                fundraiserList.get(i).fundraiserLists
                                                        .get(j).fundCatName,
                                                fundraiserList.get(i).fundraiserLists
                                                        .get(j).imagePath));

                            }
                            isHeaderExist = false;
                        } else {
                            items.add(new SectionItem(fundraiserList.get(i).header));

                            for (int j = 0; j < fundraiserList.get(i).fundraiserLists
                                    .size(); j++) {
                                items.add(new FundraiserEntryItem(fundraiserList
                                        .get(i).fundraiserLists.get(j).fundId,
                                        fundraiserList.get(i).fundraiserLists
                                                .get(j).fundName,
                                        fundraiserList.get(i).fundraiserLists
                                                .get(j).startDate,
                                        fundraiserList.get(i).fundraiserLists
                                                .get(j).fundCatId,
                                        fundraiserList.get(i).fundraiserLists
                                                .get(j).fundCatName,
                                        fundraiserList.get(i).fundraiserLists
                                                .get(j).imagePath));

                            }
                        }

                    }
                    if (fundraiserList.size() == 1 && !isAlreadyLoading) {
                        FundraiserEntryItem item = (FundraiserEntryItem) items
                                .get(1);
                        navigateFundraiserActivity(item, "yes");
                    } else {
                        FundraiserEventsAdapter adapter = new FundraiserEventsAdapter(
                                FundraiserActivity.this, items, activity);
                        listview.setAdapter(adapter);
                        isAlreadyLoading = false;
                        listview.setOnItemClickListener(FundraiserActivity.this);
                        listview.setSelection(previousPosition);
                    }

                    if (bb != null) {
                        bb.setOptionsValues(getListValues(), null);
                    }
                } else {

                    progDialog.dismiss();

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            FundraiserActivity.this);

                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setMessage(responseText);
                    alertDialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();
                                    cancelFundListTask();
                                    finish();
                                }
                            });
                    alertDialogBuilder.show();

                }

                if (hasBottomBtns && enableBottomButton) {

                    bb.createbottomButtontTab(linearLayout, false);
                }

                linearLayout.setVisibility(View.VISIBLE);

                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... arg0) {

            HubCityContext mHubCity = (HubCityContext) getApplicationContext();
            String groupBy = mHubCity.getFundGroupSelection();
            String sortBy = mHubCity.getFundSortSelection();

            try {

                String deals_fundraiser_list = Properties.url_local_server
                        + Properties.hubciti_version
                        + "hotdeals/getfundrserlistjson";
//                String deals_fundraiser_list = "http://10.10.221.2:9990/HubCiti_Coupon/hotdeals/getfundrserlistjson";
                JSONObject urlParameters;
                JSONObject jsonResponse;

                if (getIntent().hasExtra("isDeals")
                        && getIntent().getExtras().getBoolean("isDeals")) {
                    urlParameters = mUrlRequestParams
                            .createDealsFundraiserJsonParameter(lastvisitProdId
                                    + "", groupBy, sortBy, "", deptId);
                    jsonResponse = mServerConnections.getUrlJsonPostResponse(
                            deals_fundraiser_list, urlParameters);

                } else {

                    minProdId = lastvisitProdId;
                    catId = "0";
                    String url_fundraiser = Properties.url_local_server
                            + Properties.hubciti_version
                            + "alertevent/getfundrserlistjson";
//                    String url_fundraiser = "http://10.10.221.2:9990/HubCiti_Coupon/alertevent/getfundrserlistjson";
                    urlParameters = mUrlRequestParams
                            .createFundraiserEventsListJsonParameter(mItemId,
                                    "android", lastvisitProdId + "", groupBy,
                                    sortBy, bottomBtnId, postalCode, latitude,
                                    longitude, catId, retailerId, retListId,
                                    retailLocationId, deptId);
                    jsonResponse = mServerConnections.getUrlJsonPostResponse(
                            url_fundraiser, urlParameters);
                }
                if (jsonResponse != null) {

                    responseText = jsonResponse
                            .getString("responseText");
                    // For BottomButton
                    if (jsonResponse.has("bottomBtnList")) {

                        hasBottomBtns = true;

                        try {
                            ArrayList<BottomButtonBO> bottomButtonList = bb
                                    .parseForBottomButton(jsonResponse);
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(bottomButtonList);

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    } else {
                        hasBottomBtns = false;
                    }


                    try {
                        if (jsonResponse.has("isDeptFlag")) {
                            isDeptFlag = Integer.parseInt(jsonResponse
                                    .getString("isDeptFlag"));
                        }

                        if (jsonResponse.has("mainMenuId")) {
                            mainMenuId = jsonResponse.getString("mainMenuId");
                            Constants.saveMainMenuId(mainMenuId);
                        }

                        if (jsonResponse.has("categoryList")) {
                            JSONArray arrayCatList = jsonResponse
                                    .getJSONArray("categoryList");

                            if (arrayCatList != null) {

                                for (int i = 0; i < arrayCatList.length(); i++) {
                                    JSONObject categoryObject = arrayCatList
                                            .getJSONObject(i);
                                    String groupContent = categoryObject
                                            .getString("groupContent");
                                    prepareFundraiserList = new PrepareFundraisertList();
                                    prepareFundraiserList.header = groupContent;
                                    if (categoryObject != null) {
                                        try {
                                            JSONArray jsonFundraiserArray = categoryObject
                                                    .getJSONArray("fundraiserList");
                                            if (jsonFundraiserArray != null) {
                                                for (int index = 0; index < jsonFundraiserArray
                                                        .length(); index++) {
                                                    JSONObject fundraiserObject = jsonFundraiserArray
                                                            .getJSONObject(index);


                                                    DetailsByGroup objDetailsByGroup = new DetailsByGroup();
                                                    objDetailsByGroup.fundId = fundraiserObject
                                                            .getString("fundId");
                                                    objDetailsByGroup.fundName = fundraiserObject
                                                            .getString("fundName");
                                                    objDetailsByGroup.startDate = fundraiserObject
                                                            .getString("startDate");
                                                    objDetailsByGroup.fundCatId = fundraiserObject
                                                            .getString("fundCatId");
                                                    objDetailsByGroup.fundCatName = fundraiserObject
                                                            .getString("fundCatName");

                                                    if (fundraiserObject
                                                            .has("imgPath")) {
                                                        if (fundraiserObject
                                                                .getString("imgPath") != null) {
                                                            objDetailsByGroup.imagePath = fundraiserObject
                                                                    .getString("imgPath");
                                                        }

                                                    }

                                                    prepareFundraiserList.fundraiserLists
                                                            .add(objDetailsByGroup);

                                                }

                                            }

                                            objListDetails.fundraiserList
                                                    .add(prepareFundraiserList);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        responseText = jsonResponse
                                .getString("responseText");
                        return returnValue = "UNSUCCESS";
                    }
                    try {

                        if (Integer.valueOf(jsonResponse.getString("maxRowNum")) > lastvisitProdId) {
                            lastvisitProdId = Integer.valueOf(jsonResponse
                                    .getString("maxRowNum"));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {

                        if ("1".equalsIgnoreCase(jsonResponse
                                .getString("nextPage"))) {
                            nextPage = true;

                        } else if ("0".equalsIgnoreCase(jsonResponse
                                .getString("nextPage"))) {

                            nextPage = false;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if (responseText.equals(""))
                        responseText = "No Records Found.";
                    return returnValue = "UNSUCCESS";
                }
                return returnValue;

            } catch (Exception e) {
                e.printStackTrace();
                if (responseText.equals(""))
                    responseText = "No Records Found.";
                return returnValue = "UNSUCCESS";
            }

        }
    }

    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();
        values.put(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);

        values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

        values.put("Class", "Fundraiser");

        // values.put("fundId", fundId);

        values.put("retailerId", retailerId);

        values.put("retListId", retListId);

        values.put("retailLocationId", retailLocationId);
        values.put("isDeptFlag", String.valueOf(isDeptFlag));
        values.put("deptId", deptId);
        values.put("retailerEvents",
                getIntent().getExtras().getString("retailerEvents"));

        values.put("mLinkId", mLinkId);

        values.put("catName", catName);

        values.put("catId", catId);

        return values;

    }

    public void navigateFundraiserActivity(FundraiserEntryItem item,
                                           String singleitem) {
        Intent fundDescripNav = new Intent(getApplicationContext(),
                FundraiserEventDescription.class);
        fundDescripNav.putExtra("fundID", item.fundId);
        fundDescripNav.putExtra("fundName", item.fundName);
        fundDescripNav.putExtra("fundListId", item.fundCatId);
        fundDescripNav.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        fundDescripNav.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                bottomBtnId);
        fundDescripNav.putExtra("mLinkId", mLinkId);
        fundDescripNav.putExtra("mainMenuId", mainMenuId);
        fundDescripNav.putExtra("singleitem", singleitem);
        startActivity(fundDescripNav);

        if ("yes".equalsIgnoreCase(singleitem)) {
            finish();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        try {
            FundraiserEntryItem item = (FundraiserEntryItem) items
                    .get(position);
            navigateFundraiserActivity(item, "no");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }
        // Add screen name when needed
        if (null != bb) {
            bb.setActivityInfo(activity, "Fundraisers");
            bb.setOptionsValues(getListValues(), null);
        }

    }

    public void onDestroy() {

        super.onDestroy();

        HubCityContext mHubCity = (HubCityContext) getApplicationContext();
        if (!flag) {
            mHubCity.setFundGroupSelection("date");
            mHubCity.setFundSortSelection("date");
            flag = false;
        }

        cancelFundListTask();

    }

    private void cancelFundListTask() {
        if (fundListAsyncTask != null && !fundListAsyncTask.isCancelled()) {
            fundListAsyncTask.cancel(true);
            fundListAsyncTask = null;
        }

    }

    private void callFundAsyncTask() {
        previousPosition = items.size();
        if (isFirst) {
            fundListAsyncTask = new FundraiserEventListAsyncTask();
            fundListAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            isFirst = false;
            return;
        }

        if (fundListAsyncTask != null) {
            if (!fundListAsyncTask.isCancelled()) {
                fundListAsyncTask.cancel(true);
            }
            fundListAsyncTask = null;
        }

        fundListAsyncTask = new FundraiserEventListAsyncTask();
        fundListAsyncTask.execute();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancelFundListTask();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
