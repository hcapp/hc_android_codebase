package com.hubcity.android.screens;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.hubcity.android.businessObjects.LoginScreenSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.businessObjects.SuccessfulRegistrationSingleton;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.model.CityModel;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.CommonMethods;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationActivity extends CustomTitleBar
{

	private EditText etUserName, etEmail;
	private EditText etPassword;
	private EditText etconfirmPassword;
	private ProgressDialog progDialog;
	private Hashtable<String, String> signUpResponseData;
	protected String userName;
	protected String password, email;
	protected String confirmPassword;
	private String bkgndColor;
	private String btnFontColor;
	private String btnColor;

	String description;
	String fontColor;
	//Bitmap hubCitiImg;
	String hubCitiImgUrl;
	String logoImg;
	String smallLogoUrl;
	// Bitmap smallLogo;
	String title;

	private boolean enableButtons = false;
	private TableLayout signupEditFieldsLayout;
	private TextView tvpassword;
	private TextView tvSuccessResponseTitleTitle, tvSuccessResponseTitleDesc;
	private TextView tvUserName, tvEmail;
	private TextView tvConfirmpassword;
	private Button buttongetstarted;
	private Button butonSetPreferences;
	private Button buttonSave;
	private Pattern emailPattern;
	private Matcher emailMatcher;
	private RelativeLayout signupParentLayout;
	String strPushRegMsg = null;
	private ArrayList<CityModel> cityList = new ArrayList<>();
	ImageView loginImageView;
	boolean showLogin = false;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.registration);
			strPushRegMsg = this.getIntent().getStringExtra(
					Constants.PUSH_NOTIFY_MSG);
			if (getIntent().hasExtra("showLogin")) {
				showLogin = getIntent().getBooleanExtra("showLogin",false);
			}
			LoginScreenSingleton loginScreenSingleton = LoginScreenSingleton
					.getLoginScreenSingleton();
			if (loginScreenSingleton != null) {
				smallLogoUrl = loginScreenSingleton.getSmallLogoUrl();
				logoImg = loginScreenSingleton.getLogoImgUrl();
			}

			super.title.setText("Registration");

			super.imageLogo.setVisibility(View.GONE);

			super.leftTitleImage.setVisibility(View.GONE);
			super.rightImage.setVisibility(View.GONE);
			divider.setVisibility(View.GONE);
			loginImageView = (ImageView) findViewById(R.id.login_icon);
			if (Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim()) || showLogin) {
				loginImageView.setVisibility(View.VISIBLE);
				backImage.setVisibility(View.GONE);
				loginImageView.setBackgroundResource(R.drawable.login);
				Constants.GuestLoginId = Constants.GuestLoginIdforSigup = "";
                Constants.bISLoginStatus = false;
//                super.backImage.setTextColor(Color.WHITE);
				loginImageView.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						RegistrationActivity.this.deleteDatabase("HupCity.db");
						MenuAsyncTask.dateModified = "";

						Intent intent = new Intent(RegistrationActivity.this,
								LoginScreen.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						if (strPushRegMsg != null) {
							intent.putExtra(Constants.PUSH_NOTIFY_MSG,
									strPushRegMsg);
						}
						intent.putExtra("chkloc", true);
						RegistrationActivity.this.finish();
						RegistrationActivity.this.startActivity(intent);

					}
				});
			}
			LoginScreenSingleton mSignUpScreenSingleton = LoginScreenSingleton
					.getLoginScreenSingleton();

			signupParentLayout = (RelativeLayout) findViewById(R.id.signup_parent_layout);

			if (mSignUpScreenSingleton != null) {
				bkgndColor = mSignUpScreenSingleton.getBkgndColor();

				btnColor = mSignUpScreenSingleton.getBtnColor();
				btnFontColor = mSignUpScreenSingleton.getBtnFontColor();

				description = mSignUpScreenSingleton.getDescription();
				fontColor = mSignUpScreenSingleton.getFontColor();

				hubCitiImgUrl = mSignUpScreenSingleton.getHubCitiImgUrl();
				logoImg = mSignUpScreenSingleton.getLogoImgUrl();
				smallLogoUrl = mSignUpScreenSingleton.getSmallLogoUrl();

				title = mSignUpScreenSingleton.getTitle();
			}

			tvSuccessResponseTitleTitle = (TextView) findViewById(R.id.tv_success_response_title);
			tvSuccessResponseTitleDesc = (TextView) findViewById(R.id
					.tv_success_response_description);

			tvUserName = (TextView) findViewById(R.id.tv_signup_username_text);
			tvUserName.setText("User Name");

			tvEmail = (TextView) findViewById(R.id.tv_signup_email_text);

			tvpassword = (TextView) findViewById(R.id.tv_signup_password_text);
			tvpassword.setText(R.string.password);

			tvConfirmpassword = (TextView) findViewById(R.id.tv_signup_confirmpassword_text);
			tvConfirmpassword.setText(R.string.confirmpassword_text);

			etUserName = (EditText) findViewById(R.id.signup_username_text);
			etEmail = (EditText) findViewById(R.id.signup_email_text);
			etPassword = (EditText) findViewById(R.id.signup_password_text);
			etconfirmPassword = (EditText) findViewById(R.id.signup_confirmpassword_text);

			int maxLength = 10;
			etPassword.setFilters(new InputFilter[] {
					new InputFilter.LengthFilter(
							maxLength)
			});
			etconfirmPassword
					.setFilters(new InputFilter[] {
							new InputFilter.LengthFilter(
									maxLength)
					});

			butonSetPreferences = (Button) findViewById(R.id.signup_setpreferences_button);
			butonSetPreferences.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{

					Intent setPreferrenceScreenIntent = new Intent(
							RegistrationActivity.this, SetPreferrenceScreen.class);
					setPreferrenceScreenIntent.putExtra("regacc", true);
					setPreferrenceScreenIntent.putExtra("btnColor", btnColor);
					setPreferrenceScreenIntent.putExtra("btnFontColor",
							btnFontColor);
					if (strPushRegMsg != null) {
						setPreferrenceScreenIntent.putExtra(Constants.PUSH_NOTIFY_MSG,
								strPushRegMsg);
					}
					startActivity(setPreferrenceScreenIntent);

				}
			});

			buttongetstarted = (Button) findViewById(R.id.signup_getstarted_button);
			buttongetstarted.setOnClickListener(new OnClickListener()
			                                    {
				                                    @Override
				                                    public void onClick(View v)
				                                    {

					                                    //Added by sharanamma
					                                    if (Properties.isRegionApp) {
						                                    new GetCityList().execute();
					                                    } else {
						                                    checkTemplateType();
					                                    }

				                                    }

			                                    }

			);

			buttonSave = (Button)

					findViewById(R.id.signup_save_button);

			buttonSave.setOnClickListener(new

					                              OnClickListener()
					                              {
						                              @Override
						                              public void onClick(View v)
						                              {

							                              RegistrationActivity.this.deleteDatabase
									                              ("HupCity.db");
							                              MenuAsyncTask.dateModified = "";

							                              if (etUserName.getText().length() != 0
									                              && etPassword.getText().length()
									                              != 0
									                              && etconfirmPassword.getText()
									                              .length() != 0) {
								                              if (etPassword.getText().length()
										                              >= 6
										                              && etconfirmPassword.getText
										                              ().length() >= 6) {
									                              if (etPassword.getText()
											                              .toString()
											                              .equals
													                              (etconfirmPassword.getText().toString())) {
										                              if (etUserName.getText()
												                              .length() == 0) {
											                              AlertDialog.Builder
													                              notificationAlert = new AlertDialog.Builder(
													                              RegistrationActivity.this);
											                              notificationAlert
													                              .setMessage(
															                              getString(R.string.email_dialog))
													                              .setPositiveButton(
															                              getString(R.string.alert_zipcode_yes),
															                              new
																	                              DialogInterface.OnClickListener()
																	                              {
																		                              @Override
																		                              public void onClick(
																				                              DialogInterface dialog,
																				                              int id)
																		                              {
																			                              userName = etUserName
																					                              .getText()
																					                              .toString();
																			                              password = etPassword
																					                              .getText()
																					                              .toString();
																			                              email = etEmail.getText().toString();
																			                              new RegistrationAsynctask()
																					                              .execute(" ");
																		                              }
																	                              })
													                              .setNegativeButton(
															                              getString(R.string.alert_zipcode_no),
															                              new
																	                              DialogInterface.OnClickListener()
																	                              {
																		                              @Override
																		                              public void onClick(
																				                              DialogInterface dialog,
																				                              int id)
																		                              {

																			                              dialog.cancel();
																		                              }

																	                              });
											                              notificationAlert.create
													                              ().show();
										                              } else {
											                              userName =
													                              etUserName
															                              .getText
																	                              ().toString();
											                              password =
													                              etPassword
															                              .getText
																	                              ().toString();
											                              email = etEmail
													                              .getText()
													                              .toString();
											                              if (email.length() ==
													                              0) {
												                              new
														                              RegistrationAsynctask().execute(" ");
											                              } else {
												                              if (CommonMethods.emailValidation
														                              (email)) {
													                              new
															                              RegistrationAsynctask().execute(" ");
												                              } else {
													                              AlertDialog
															                              .Builder
															                              notificationAlert = new
															                              AlertDialog.Builder(
															                              RegistrationActivity
																	                              .this);
													                              notificationAlert.setMessage("Invalid Email id")
															                              .setPositiveButton(
																	                              getString(R
																			                              .string
																			                              .specials_ok),
																	                              new
																			                              DialogInterface.OnClickListener()
																			                              {
																				                              @Override
																				                              public void
																				                              onClick(
																						                              DialogInterface dialog,
																						                              int
																								                              id)
																				                              {
																					                              dialog
																							                              .cancel();

																				                              }
																			                              });
													                              notificationAlert.create()
															                              .show();
												                              }
											                              }

										                              }

									                              } else {
										                              AlertDialog.Builder
												                              notificationAlert =
												                              new AlertDialog
														                              .Builder(
														                              RegistrationActivity
																                              .this);
										                              notificationAlert
												                              .setMessage(
														                              getString(R
																                              .string.password_mismatch))
												                              .setPositiveButton(
														                              getString(R
																                              .string.specials_ok),
														                              new
																                              DialogInterface.OnClickListener()
																                              {
																	                              @Override
																	                              public void
																	                              onClick(
																			                              DialogInterface dialog,
																			                              int id)
																	                              {
																		                              etPassword.setText(null);
																		                              etconfirmPassword
																				                              .setText(null);
																		                              dialog.cancel();

																	                              }
																                              });
										                              notificationAlert.create()
												                              .show();
									                              }

								                              } else {
									                              AlertDialog.Builder
											                              notificationAlert = new
											                              AlertDialog.Builder(
											                              RegistrationActivity
													                              .this);
									                              notificationAlert.setMessage(
											                              getString(R.string
													                              .minimum_char_pass))
											                              .setPositiveButton(
													                              getString(R
															                              .string
															                              .specials_ok),
													                              new
															                              DialogInterface.OnClickListener()
															                              {
																                              @Override
																                              public void
																                              onClick(
																		                              DialogInterface dialog,
																		                              int
																				                              id)
																                              {
																	                              dialog
																			                              .cancel();

																                              }
															                              });
									                              notificationAlert.create()
											                              .show();
								                              }

							                              } else {
								                              AlertDialog.Builder
										                              notificationAlert = new
										                              AlertDialog.Builder(
										                              RegistrationActivity.this);
								                              notificationAlert.setMessage(
										                              getString(R.string
												                              .missing_fields))
										                              .setPositiveButton(getString
														                              (R.string
																                              .specials_ok),
												                              new DialogInterface
														                              .OnClickListener()
												                              {
													                              @Override
													                              public void
													                              onClick(
															                              DialogInterface dialog, int id)
													                              {
														                              dialog
																                              .cancel();

													                              }
												                              });
								                              notificationAlert.create().show();
							                              }
						                              }
					                              }

			);

			signupEditFieldsLayout = (TableLayout)

					findViewById(R.id.signup_edit_fields);

			setViewProperties(bkgndColor, btnColor, btnFontColor, btnFontColor);

			if (!enableButtons)

			{
				signupEditFieldsLayout.setVisibility(View.VISIBLE);

				buttongetstarted.setEnabled(false);
				butonSetPreferences.setEnabled(false);

				tvSuccessResponseTitleTitle.setVisibility(View.GONE);
				tvSuccessResponseTitleDesc.setVisibility(View.GONE);
				GradientDrawable buttongetstartedShape = (GradientDrawable) buttongetstarted
						.getBackground();
				GradientDrawable butonSetPreferencesShape = (GradientDrawable) butonSetPreferences
						.getBackground();
				buttongetstartedShape.setColor(Color.GRAY);
				butonSetPreferencesShape.setColor(Color.GRAY);
			} else

			{
				signupEditFieldsLayout.setVisibility(View.GONE);
				tvSuccessResponseTitleTitle.setVisibility(View.VISIBLE);
				tvSuccessResponseTitleDesc.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startMainMenu()
	{
		if (strPushRegMsg != null) {
			SubMenuStack.clearSubMenuStack();
			Constants.startPushList(strPushRegMsg, RegistrationActivity.this);
		} else {
			startNextActivity();
		}
	}

	private void saveLoginData(boolean state)
	{

		SharedPreferences settings = getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, 0);
		Editor prefEditor = settings.edit();
		prefEditor.putString(Constants.REMEMBER_NAME, null);
		prefEditor.putString(Constants.REMEMBER_PASSWORD, null);
		if (state) {
			prefEditor.putString(Constants.REMEMBER_NAME, etUserName.getText()
					.toString().trim());
			prefEditor.putString(Constants.REMEMBER_PASSWORD, etPassword
					.getText().toString().trim());

		}
		prefEditor.putBoolean(Constants.REMEMBER, state);
		prefEditor.apply();

		if (GlobalConstants.isFromNewsTemplate) {
			settings.edit().putBoolean(
					Constants.PREFERENCE_IS_LOGIN, true).apply();
		}
	}

	private void setViewProperties(String bkgndColor, String btnColor,
			String fontColor, String btnFontColor)
	{
		if (bkgndColor != null) {
			signupParentLayout.setBackgroundColor(Color.parseColor(bkgndColor));
			signupEditFieldsLayout
					.setBackgroundResource(R.drawable.round_corner);
		}

		if (btnColor != null) {

			GradientDrawable buttonSaveInShape = (GradientDrawable) buttonSave
					.getBackground();
			GradientDrawable buttongetstartedShape = (GradientDrawable) buttongetstarted
					.getBackground();
			GradientDrawable butonSetPreferencesShape = (GradientDrawable) butonSetPreferences
					.getBackground();
			buttonSaveInShape.setColor(Color.parseColor(btnColor));
			buttongetstartedShape.setColor(Color.parseColor(btnColor));
			butonSetPreferencesShape.setColor(Color.parseColor(btnColor));
		}
		if (btnFontColor != null) {
			buttonSave.setTextColor(Color.parseColor(btnFontColor));
			buttongetstarted.setTextColor(Color.parseColor(btnFontColor));
			butonSetPreferences.setTextColor(Color.parseColor(btnFontColor));
		}
		if (fontColor != null) {

			if (tvSuccessResponseTitleTitle.getVisibility() == View.VISIBLE) {
				tvSuccessResponseTitleTitle.setTextColor(Color
						.parseColor(fontColor));
			} else {

				tvSuccessResponseTitleTitle.setTextColor(Color.BLACK);
			}

			if (tvSuccessResponseTitleDesc.getVisibility() == View.VISIBLE) {
				tvSuccessResponseTitleDesc.setTextColor(Color
						.parseColor(fontColor));
			} else {

				tvSuccessResponseTitleDesc.setTextColor(Color.BLACK);
			}

			tvUserName.setTextColor(Color.BLACK);
			tvEmail.setTextColor(Color.BLACK);
			tvpassword.setTextColor(Color.BLACK);
			tvConfirmpassword.setTextColor(Color.BLACK);

			etUserName.setTextColor(Color.BLACK);
			etEmail.setTextColor(Color.BLACK);
			etPassword.setTextColor(Color.BLACK);
			etconfirmPassword.setTextColor(Color.BLACK);

		}

	}



	class RegistrationAsynctask extends AsyncTask<String, String, String>
	{

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			progDialog = new ProgressDialog(RegistrationActivity.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage("Signing up ...");
			progDialog.setCancelable(false);
			progDialog.show();
		}

		@Override
		protected String doInBackground(String... params)
		{
			try {
//				String url_sign_up = Properties.url_local_server
//						+ Properties.hubciti_version + "firstuse/signup";
				String url_sign_up = Properties.url_local_server
						+ Properties.hubciti_version + "firstuse/v2/signup";
//				String url_sign_up = "http://10.10.221.225:8080/HubCiti2.6/firstuse/v2/signup";
				String urlParameters = mUrlRequestParams
						.createSignUpUrlParameter(userName, password, email);
				JSONObject xmlResponde = mServerConnections.getUrlPostResponse(
						url_sign_up, urlParameters, true);

				return jsonParseSignUpResponse(xmlResponde);

			} catch (Exception e) {
				e.printStackTrace();
				return UN_SUCCESSFULL;
			}
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			if (result.equalsIgnoreCase(SUCCESSFULL)) {
				if (signUpResponseData != null) {


					saveLoginData(true);

					etUserName.setText("");
					etPassword.setText("");
					etconfirmPassword.setText("");

					enableButtons = true;

					if (!enableButtons) {
						signupEditFieldsLayout.setVisibility(View.VISIBLE);
						tvSuccessResponseTitleTitle.setVisibility(View.GONE);
						tvSuccessResponseTitleDesc.setVisibility(View.GONE);

						GradientDrawable buttongetstartedShape = (GradientDrawable)
								buttongetstarted
										.getBackground();
						GradientDrawable butonSetPreferencesShape = (GradientDrawable)
								butonSetPreferences
										.getBackground();
						buttongetstartedShape.setColor(Color.GRAY);
						butonSetPreferencesShape.setColor(Color.GRAY);
						buttongetstarted.setEnabled(false);
						butonSetPreferences.setEnabled(false);

					} else {
						backImage.setVisibility(View.GONE);
						loginImageView.setVisibility(View.GONE);
						SuccessfulRegistrationSingleton mSuccessfulRegistrationSingleton =
								SuccessfulRegistrationSingleton
										.getSuccessfulRegistrationSingleton();

						progDialog.dismiss();

						buttonSave.setVisibility(View.GONE);

						signupEditFieldsLayout.setVisibility(View.GONE);

						tvSuccessResponseTitleTitle.setVisibility(View.VISIBLE);
						try {
							tvSuccessResponseTitleTitle
									.setText(mSuccessfulRegistrationSingleton != null ?
											mSuccessfulRegistrationSingleton
													.getTitle() : null);
							tvSuccessResponseTitleDesc
									.setVisibility(View.VISIBLE);
							// tvSuccessResponseTitleDesc
							// .setText(mSuccessfulRegistrationSingleton
							// .getDescription());
							tvSuccessResponseTitleDesc
									.setText(R.string.congrats);
							if (mSuccessfulRegistrationSingleton != null) {
								String bkgndColor = mSuccessfulRegistrationSingleton != null ?
										mSuccessfulRegistrationSingleton
												.getBkgndColor() : null;
								String btnColor = mSuccessfulRegistrationSingleton
										.getBtnColor();
								String btnFontColor = mSuccessfulRegistrationSingleton
										.getBtnFontColor();
								String fontColor = mSuccessfulRegistrationSingleton
										.getFontColor();

								setViewProperties(bkgndColor, btnColor, fontColor,
										btnFontColor);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						buttongetstarted.setEnabled(true);
						butonSetPreferences.setEnabled(true);
						backImage.setVisibility(View.GONE);
					}
				}

			} else {// for UN_SUCCESSFULL

				enableButtons = false;
				if (signUpResponseData != null) {
					String responseText = signUpResponseData
							.get("responseText");
					progDialog.dismiss();

					createAlert(responseText);
				}
			}
		}
	}

	private static String SUCCESSFULL = "sucessfull";
	private static String UN_SUCCESSFULL = "un_sucessfull";

	@SuppressWarnings("static-access")
	public String jsonParseSignUpResponse(JSONObject xmlResponde)
	{

		JSONObject localJSONObject;
		signUpResponseData = new Hashtable<>();

		if (xmlResponde == null) {
			return Constants.UN_SUCCESSFULL;
		}
		try {
			if (xmlResponde.has(Constants.SUCCESSFULL_REGISTRATION_RESPONSE)) {

				localJSONObject = xmlResponde
						.getJSONObject(Constants.SUCCESSFULL_REGISTRATION_RESPONSE);

				String userId = localJSONObject.getString("userId");
				signUpResponseData.put("userId", userId);
				Context context = this;
				SharedPreferences preferences = context.getSharedPreferences(
						Constants.PREFERENCE_HUB_CITY, context.MODE_PRIVATE);
				SharedPreferences.Editor e = preferences.edit().putString(
						Constants.PREFERENCE_HUB_CITY_ID,
						localJSONObject.getString("hubCitiId"));
				e.putString(Constants.PREFERENCE_KEY_U_ID, userId);
				e.apply();
				// // according to new web service implementation

				try {
					JSONObject regSuccessUIJSONObject = localJSONObject
							.getJSONObject("regSuccessUI");

					String bkgndColor = regSuccessUIJSONObject
							.getString("bkgndColor");

					String fontColor = regSuccessUIJSONObject
							.getString("fontColor");

					String title = regSuccessUIJSONObject.getString("title");
					String description = regSuccessUIJSONObject
							.getString("description");
					String btnColor = regSuccessUIJSONObject
							.getString("btnColor");
					String btnFontColor = regSuccessUIJSONObject
							.getString("btnFontColor");

					SuccessfulRegistrationSingleton
							.clearSuccessfulRegistrationSingleton();
					SuccessfulRegistrationSingleton mSuccessfulRegistrationSingleton =
							SuccessfulRegistrationSingleton
									.getSuccessfulRegistrationSingleton(bkgndColor,
											fontColor, title, description, btnColor,
											btnFontColor);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				return SUCCESSFULL;

			} else if (xmlResponde.has(Constants.ERROR_REGISTRATION_RESPONSE)) {

				localJSONObject = xmlResponde
						.getJSONObject(Constants.ERROR_REGISTRATION_RESPONSE);

				String responseCode = localJSONObject.getString("responseCode");
				String responseText = localJSONObject.getString("responseText");

				signUpResponseData.put("responseCode", responseCode);
				signUpResponseData.put("responseText", responseText);

				return UN_SUCCESSFULL;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return UN_SUCCESSFULL;
		}
		return UN_SUCCESSFULL;
	}

	@SuppressWarnings("deprecation")
	private void createAlert(String text)
	{
		final AlertDialog alertDialog = new AlertDialog.Builder(
				RegistrationActivity.this).create();
		alertDialog.setMessage(text);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				alertDialog.dismiss();
			}
		});
		alertDialog.show();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if (progDialog != null) {
			progDialog.dismiss();
		}
	}

	@Override
	protected void onStop()
	{
		super.onStop();
	}

	public void startNextActivity()
	{
		String level = "1";
		String mItemId = "0";
		String mLinkId = "0";
		SubMenuStack.clearSubMenuStack();
		MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null, null, 0,
				null, mLinkId, null, null, null, null, level, null, null, null,
				null, null, null, null);
		SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

		MenuAsyncTask menu = new MenuAsyncTask(this);
		menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, level, mItemId,
				"0", "None", "0", "0");
	}

	private class GetCityList extends AsyncTask<String, Void, String>
	{
		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		JSONObject jsonObjectCity = null;
		JSONObject jsonObjectCityList = new JSONObject();
		JSONArray jsonArrayCityList = new JSONArray();
		String responseText = "";

		@Override
		protected String doInBackground(String... params)
		{

			String result = "false";
			try {
				String urlParameters = mUrlRequestParams
						.createCityCatUrlParameter();
				Log.d("urlParameters : ", urlParameters);
				String url_get_city_preferences = Properties.url_local_server
						+ Properties.hubciti_version + "firstuse/getcitypref";
				jsonObjectCity = mServerConnections.getUrlPostResponse(
						url_get_city_preferences, urlParameters, true);
				Log.d("response : ", jsonObjectCity.toString());
				if (jsonObjectCity != null && jsonObjectCity.has("City")) {


					JSONObject cityObject = jsonObjectCity
							.optJSONObject("City");
					responseText = cityObject
							.getString("responseText");
					jsonObjectCityList = cityObject
							.optJSONObject("cityList");
					try {
						jsonArrayCityList = jsonObjectCityList
								.getJSONArray("City");

					} catch (Exception e) {
						jsonArrayCityList.put(jsonObjectCityList
								.getJSONObject("City"));
					}
					CityDataHelper cityObj = new CityDataHelper();
					cityObj.clearCityPreference(RegistrationActivity.this);
					SharedPreferences preference = cityObj.getSharedPreference
							(RegistrationActivity.this);
					SharedPreferences.Editor editor = preference.edit();
					for (int i = 0; i < jsonArrayCityList
							.length(); i++) {

						JSONObject jsonObject = jsonArrayCityList
								.getJSONObject(i);
						if (jsonObject.getInt("isCityChecked") == 1) {
							String key = jsonObject.getString("cityId");
							String value = jsonObject.getString("cityName");
							editor.putString(key, value);
						}

					}
					editor.apply();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;

		}

		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected void onPostExecute(String result)
		{
			if (responseText.equalsIgnoreCase("Success")) {
				checkTemplateType();
			}
		}

	}

	private void checkTemplateType()
	{
		SharedPreferences setprefs = getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, 0);
		setprefs.edit().putBoolean("isFirstLaunch", true).apply();
		if (GlobalConstants.isFromNewsTemplate) {
			if (GlobalConstants.className != null && GlobalConstants.className.equalsIgnoreCase
					(Constants.SCROLLING)) {
				startActivity(new Intent(RegistrationActivity.this, ScrollingPageActivity.class));
			} else if (GlobalConstants.className != null && GlobalConstants.className
					.equalsIgnoreCase(Constants.NEWS_TILE)) {
				startActivity(new Intent(RegistrationActivity.this, TwoTileNewsTemplateActivity
						.class));
			} else {
				startActivity(new Intent(RegistrationActivity.this, CombinationTemplate.class));
			}
		} else {
			startMainMenu();
		}
	}

}