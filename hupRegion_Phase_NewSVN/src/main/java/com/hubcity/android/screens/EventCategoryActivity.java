package com.hubcity.android.screens;

import java.util.ArrayList;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class EventCategoryActivity extends CustomTitleBar implements
        OnItemClickListener {

    LinearLayout linearLayout = null;
    BottomButtons bb = null;
    Activity activity;
    ListView listview;
    ProgressDialog progDialog;
    EventCategoryListAdapter catListAdapter;

    HashMap<String, String> category;
    final ArrayList<HashMap<String, String>> arrayListCat = new ArrayList<>();

    boolean isEventsAll = false;
    boolean hasBottomBtns = false;
    boolean enableBottomButton = true;

    String mItemId = "";
    String bottomBtnId = "";

    int position = 0;

    FetchEventCategoryTask eventCatAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_category);

        try {
            if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }

            if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
                bottomBtnId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }

            title.setText("Events");

            // Change this image later
            leftTitleImage.setBackgroundResource(R.drawable.all_events);
            leftTitleImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    isEventsAll = true;
                    displayEventsList();
                }
            });

            backImage.setVisibility(View.GONE);

            activity = EventCategoryActivity.this;
            // Initiating Bottom button class
            bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
            // Add screen name when needed
            bb.setActivityInfo(activity, "");

            linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
            linearLayout.setVisibility(View.INVISIBLE);

            listview = (ListView) findViewById(R.id.event_category_listview);

            callEventCatAsyncTask();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class FetchEventCategoryTask extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        String responseText = "";
        String responseCode = "";

        boolean nextPage = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDialog = new ProgressDialog(EventCategoryActivity.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                String url_events_category = Properties.url_local_server
                        + Properties.hubciti_version
                        + "alertevent/eventcategory";

                String urlParameters = mUrlRequestParams
                        .createEventsCatListParameter(mItemId, bottomBtnId);

                JSONObject jsonResponse = mServerConnections
                        .getUrlPostResponse(url_events_category, urlParameters,
                                true);

                if (jsonResponse.has("response")) {
                    if (jsonResponse.getJSONObject("response").has(
                            "responseCode")) {
                        responseCode = jsonResponse.getJSONObject("response")
                                .getString("responseCode");
                    }

                    if (jsonResponse.getJSONObject("response").has(
                            "responseText")) {
                        responseText = jsonResponse.getJSONObject("response")
                                .getString("responseText");
                    }
                } else {

                    if (jsonResponse.has("CategoryDetails")) {

                        JSONObject json2 = jsonResponse
                                .getJSONObject("CategoryDetails");

                        if (json2.has("responseCode")) {
                            responseCode = json2.getString("responseCode");
                        }

                        if (json2.has("responseText")) {
                            responseText = json2.getString("responseText");
                        }

                        if ("10000".equals(responseCode)) {

                            // Get Categories
                            if (json2.has("ListCatDetails")) {

                                JSONObject json = json2
                                        .getJSONObject("ListCatDetails");

                                Object jsonCat = json.get("CategoryDetails");

                                if (jsonCat instanceof JSONArray) {
                                    JSONArray categoryList = json
                                            .getJSONArray("CategoryDetails");
                                    for (int i = 0; i < categoryList.length(); i++) {
                                        category = new HashMap<>();

                                        JSONObject elem = categoryList
                                                .getJSONObject(i);

                                        category.put("catId",
                                                elem.getString("catId"));

                                        category.put("catName",
                                                elem.getString("catName"));

                                        category.put("catImgPath",
                                                elem.getString("catImgPath"));

                                        arrayListCat.add(category);

                                    }
                                } else if (json instanceof JSONObject) {
                                    category = new HashMap<>();

                                    JSONObject elem = json
                                            .getJSONObject("CategoryDetails");

                                    category.put("catId",
                                            elem.getString("catId"));

                                    category.put("catName",
                                            elem.getString("catName"));

                                    category.put("catImgPath",
                                            elem.getString("catImgPath"));

                                    arrayListCat.add(category);

                                }
                            }

                            // For BottomButton
                            if (json2.has("bottomBtnList")) {

                                JSONObject jsonObject = json2
                                        .getJSONObject("bottomBtnList");

                                if (jsonObject.has("BottomButton")) {

                                    hasBottomBtns = true;

                                    ArrayList<BottomButtonBO> bottomButtonList = bb
                                            .parseForBottomButton(jsonObject);
                                    BottomButtonListSingleton
                                            .clearBottomButtonListSingleton();
                                    BottomButtonListSingleton
                                            .getListBottomButton(bottomButtonList);

                                } else {
                                    hasBottomBtns = false;
                                }

                            }
                        } else {
                            return responseText;
                        }

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                responseText = "No Records Found.";
                return responseText;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if ("Success".equals(responseText)) {
                    catListAdapter = new EventCategoryListAdapter(
                            EventCategoryActivity.this, arrayListCat);
                    listview.setAdapter(null);
                    listview.setAdapter(catListAdapter);
                    listview.setOnItemClickListener(EventCategoryActivity.this);

                } else {

                    progDialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            EventCategoryActivity.this);

                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setMessage(responseText);
                    alertDialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();
                                    cancelAsyncTask();
                                    finish();
                                }
                            });
                    alertDialogBuilder.show();

                }

                if (hasBottomBtns && enableBottomButton) {

                    bb.createbottomButtontTab(linearLayout, false);
                }

                linearLayout.setVisibility(View.VISIBLE);

                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long arg3) {

        this.position = position;

        isEventsAll = false;
        displayEventsList();

    }

    private void displayEventsList() {
        Intent intent = new Intent(activity, EventsListDisplay.class);
        intent.putExtra("isSingleCategory", false);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

        if (isEventsAll) {
            intent.putExtra("catId", "0");
            intent.putExtra("catName", "All Events");
        } else {
            intent.putExtra("catId", arrayListCat.get(position).get("catId"));
            intent.putExtra("catName", arrayListCat.get(position)
                    .get("catName"));
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, Constants.STARTVALUE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Add screen name when needed
        try {
            bb.setActivityInfo(activity, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callEventCatAsyncTask() {
        if (eventCatAsyncTask != null) {
            if (!eventCatAsyncTask.isCancelled()) {
                eventCatAsyncTask.cancel(true);
            }

            eventCatAsyncTask = null;
        }

        eventCatAsyncTask = new FetchEventCategoryTask();
        eventCatAsyncTask.execute();

    }

    private void cancelAsyncTask() {
        if (eventCatAsyncTask != null && !eventCatAsyncTask.isCancelled()) {
            eventCatAsyncTask.cancel(true);
        }

        eventCatAsyncTask = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelAsyncTask();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancelAsyncTask();
    }
}
