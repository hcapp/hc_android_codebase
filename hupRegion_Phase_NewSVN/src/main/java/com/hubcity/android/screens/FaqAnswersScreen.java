package com.hubcity.android.screens;

import java.lang.ref.WeakReference;
import java.util.Arrays;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CustomTitleBar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This class displays the selected Question along with Answer for the question
 * and forward/Backward button to navigate to next/previous question and answer
 * 
 * @author rekha_p
 * 
 */
public class FaqAnswersScreen extends CustomTitleBar {

	ImageView leftArrow;
	ImageView rightArrow;

	static TextView questionView;
	static WebView answerView;

	static String questions[];
	static int position = 0;

	static String htmlcode;
	static String answer = "";

	String faqIDs[];
	String categoryName;

	FetchAnswersTask fetchAnswers;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.faq_ans_screen);

		try {
			questions = getIntent().getExtras().getStringArray("questions");
			faqIDs = getIntent().getExtras().getStringArray("faqIDs");

			String question = getIntent().getExtras().getString("question");
			answer = getIntent().getExtras().getString("answer");
			categoryName = getIntent().getExtras().getString("categoryName");

			title.setSingleLine(true);
			if (categoryName != null) {
                title.setText(categoryName);
            }
			title.setTextSize(14);

			leftTitleImage.setVisibility(View.GONE);

			rightImage.setVisibility(View.GONE);

			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelTask();
                    finish();
                }
            });

			questionView = (TextView) findViewById(R.id.questions);
			answerView = (WebView) findViewById(R.id.answers);
			answerView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

			WebSettings webSettings = answerView.getSettings();
			webSettings.setJavaScriptEnabled(true);

			htmlcode = "<html><body style=\"text-align:justify\"> %s </body></Html>";

			if (null != answer) {

                answer = answer.replaceAll("&#60;", "<");
                answer = answer.replaceAll("&#62;", ">");

                answerView.setWebViewClient(new MyWebViewClient(
                        FaqAnswersScreen.this));

                answerView.loadData(String.format(htmlcode, answer), "text/html",
                        "utf-8");
                answerView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }

			if (null != question) {

                questionView.setText(question);
                questionView.setBackgroundColor(Color.parseColor("#E6E6E6"));
            }

			leftArrow = (ImageView) findViewById(R.id.left_arrow);
			rightArrow = (ImageView) findViewById(R.id.right_arrow);

			if (questions != null && faqIDs != null) {

                position = Arrays.asList(questions).indexOf(question);
                viewEnablingCheck();
            }

			leftArrow.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (position > 0) {
                        position--;
                        executeTask();
                    }

                    viewEnablingCheck();

                }
            });

			rightArrow.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (position < questions.length - 1) {
                        position++;
                        executeTask();
                    }

                    viewEnablingCheck();

                }
            });
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Checks conditions to enable or disable the left and right arrow
	 */
	private void viewEnablingCheck() {
		boolean isLeftEnabled = false;
		boolean isRightEnabled = false;

		if (questions.length == 1 && faqIDs.length == 1) { // If there are only
															// 1 question and
															// answer
			isLeftEnabled = false;
			isRightEnabled = false;

		} else { // if the 1st question is displayed
			if (position == 0) {
				isLeftEnabled = false;
				isRightEnabled = true;

			} else if (position == questions.length - 1) { // if the last
															// question is
															// displayed
				isLeftEnabled = true;
				isRightEnabled = false;

			} else {
				isLeftEnabled = true;
				isRightEnabled = true;

			}

		}

		if (isLeftEnabled) {
			leftArrow.setImageResource(R.drawable.arrow_left_black);
		} else {
			leftArrow.setImageResource(R.drawable.arrow_left_grey);
		}

		if (isRightEnabled) {
			rightArrow.setImageResource(R.drawable.arrow_right_black);
		} else {
			rightArrow.setImageResource(R.drawable.arrow_right_grey);
		}

	}

	/**
	 * Fetches the answer on click of left or right arrow
	 */
	private void executeTask() {
		FaqScreen.isFaqAnswerScreen = true;

		if (fetchAnswers != null) {
			if (!fetchAnswers.isCancelled()) {
				fetchAnswers.cancel(true);
			}

			fetchAnswers = null;
		}

		fetchAnswers = new FetchAnswersTask(FaqAnswersScreen.this,
				questions[position], faqIDs[position], categoryName, questions,
				faqIDs, "");
		fetchAnswers.execute();
	}

	/**
	 * Stop the AsyncTask if running
	 */
	private void cancelTask() {
		if (fetchAnswers != null && !fetchAnswers.isCancelled()) {
			fetchAnswers.cancel(true);
		}

		fetchAnswers = null;
	}

	/**
	 * Sets the updated textview value
	 * 
	 * @param answer
	 */
	public static void setData(String ans) {
		if (!"".equals(ans)) {
			answer = ans;
		}

		answer = answer.replaceAll("&#60;", "<");
		answer = answer.replaceAll("&#62;", ">");

		answerView.loadData(String.format(htmlcode, answer), "text/html",
				"utf-8");

		questionView.setText(questions[position]);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cancelTask();
			finish();
		}

		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Customized WebClient to handle Email links, HTTP links and Anchor Tags
	 * 
	 * @author rekha_p
	 * 
	 */
	public class MyWebViewClient extends WebViewClient {
		private final WeakReference<Activity> mActivityRef;
		boolean flag = false;

		public MyWebViewClient(Activity activity) {
			mActivityRef = new WeakReference<>(activity);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// Handles Email Link
			if (url.startsWith("mailto:")) {
				final Activity activity = mActivityRef.get();
				if (activity != null) {
					MailTo mt = MailTo.parse(url);
					Intent i = newEmailIntent(activity, mt.getTo(),
							mt.getSubject(), mt.getBody(), mt.getCc());
					activity.startActivity(i);
					view.reload();
					return true;
				}
			} else if (url != null && url.startsWith("http://")) { // Handles
																	// HTTP
																	// links
																	// (Opens
																	// link in
																	// device
																	// browser)
				view.getContext().startActivity(
						new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				return true;
			} else {
				view.loadUrl(url);
			}
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// Handles Anchor Tags
			if (url.contains("#") && !flag) {
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						answerView.loadData(String.format(htmlcode, answer),
								"text/html", "utf-8");
					}
				}, 400);

				flag = true;
			} else {
				flag = false;
			}

		}

		/**
		 * Intent to open Email Composer screen
		 * 
		 * @param context
		 * @param address
		 * @param subject
		 * @param body
		 * @param cc
		 * @return
		 */
		private Intent newEmailIntent(Context context, String address,
				String subject, String body, String cc) {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
			intent.putExtra(Intent.EXTRA_TEXT, body);
			intent.putExtra(Intent.EXTRA_SUBJECT, subject);
			intent.putExtra(Intent.EXTRA_CC, cc);
			intent.setType("message/rfc822");
			return intent;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		cancelTask();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cancelTask();
		finish();
	}
}
