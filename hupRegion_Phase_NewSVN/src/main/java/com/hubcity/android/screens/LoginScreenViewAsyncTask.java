package com.hubcity.android.screens;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.hubcity.android.businessObjects.LoginScreenSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.PrefManager;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.RegistrationIntentService;
import com.scansee.newsfirst.BackgroundTask.GuestUserLoginAsyncTask;

import org.json.JSONObject;


public class LoginScreenViewAsyncTask extends AsyncTask<String, String, String> {


    public static boolean bIsLoginFlag = false;
    private final UrlRequestParams mUrlRequestParams = new UrlRequestParams();
    private final ServerConnections mServerConnections = new ServerConnections();
    private Activity activity;

    private JSONObject xmlResponde;
    private String templateName;
    private String userNameVal;
    private String passwordVal;
    private JSONObject localJSONObject = null;
    private boolean isSignUp = false;

    public LoginScreenViewAsyncTask(Activity activity) {
        this.activity = activity;
    }

    public LoginScreenViewAsyncTask(Activity activity, boolean isSignUp) {
        this.activity = activity;
        this.isSignUp = isSignUp;
    }

    public LoginScreenViewAsyncTask() {
    }

    private void displaySplash() {
        //added by Supriya
        if (activity == null)
            return;
        if (isCredentialsSaved()) {
            new LoginAsyncTask(activity, false, false, null, 0, 0,
                    true, templateName)
                    .execute(userNameVal, passwordVal);
        } else {
            if (templateName != null && !templateName.isEmpty()) {
                if (isSignUp) {
                    Intent intent = new Intent(activity, RegistrationActivity.class);
                    intent.putExtra("showLogin", true);
                    activity.startActivity(intent);
                } else {
                    new GuestUserLoginAsyncTask(activity, templateName).execute();
                }
            } else {
                // startLoginScreen();
                if (isSignUp) {
                    Intent intent = new Intent(activity, RegistrationActivity.class);
                    intent.putExtra("showLogin", true);
                    activity.startActivity(intent);
                } else {
                    if (activity.getIntent()
                            .getStringExtra(Constants.PUSH_NOTIFY_MSG) != null) {
                        startLoginScreen();
                    } else {
                        new LoginAsyncTask(activity, false, true, null, 0, 0,
                                true, templateName)
                                .execute(Constants.USERNAME, Constants.PASSWORD);
                    }
                }

            }
        }

    }

    private boolean isCredentialsSaved() {
        boolean isCredentailSaved = false;
        SharedPreferences settings = activity.getSharedPreferences(Constants.PREFERENCE_HUB_CITY,
                0);
        boolean mStateChecked = settings.getBoolean(Constants.REMEMBER, false);
        userNameVal = settings.getString(Constants.REMEMBER_NAME, null);
        passwordVal = settings.getString(Constants.REMEMBER_PASSWORD, null);

        if (mStateChecked == true && userNameVal != null && passwordVal != null) {
            isCredentailSaved = true;
        }

        return isCredentailSaved;

    }

    private void startLoginScreen() {
        Intent intent = new Intent(activity, LoginScreen.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (activity.getIntent().getStringExtra(Constants.PUSH_NOTIFY_MSG) != null) {
            intent.putExtra("pushtitle",
                    activity.getIntent().getStringExtra("pushtitle"));
            intent.putExtra(Constants.PUSH_NOTIFY_MSG, activity.getIntent()
                    .getStringExtra(Constants.PUSH_NOTIFY_MSG));
        }

        intent.putExtra("chkloc", true);
        activity.finish();
        activity.startActivity(intent);
    }


    @SuppressWarnings("static-access")
    private boolean saveHubCitiId(String value) {

        Context context = HubCityContext.getHubCityContext();
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, context.MODE_PRIVATE);
        SharedPreferences.Editor e = preferences.edit().putString(
                Constants.PREFERENCE_HUB_CITY_ID, value);

        Constants.HUB_CITY_ID = value;

        return e.commit();
    }

    @Override
    protected String doInBackground(String... params) {

        String urlParameters;

        if (activity != null) {
            //Starting push notification service for the first launch to register token to server.
            if (new PrefManager(activity).isFirstTimeLaunch()) {
                Intent intent = new Intent(activity, RegistrationIntentService.class);
                activity.startService(intent);
            }
        }

        try {
            urlParameters = mUrlRequestParams.createLogInViewUrlParameter(
                    Constants.PAGE_TYPE_LOGIN, Properties.HUBCITI_KEY);
            Log.v("", "REQUEST FOR LOGIN : " + urlParameters);
            String url_log_in_view = Properties.getBaseUrl() + "firstuse/loginflow";
            xmlResponde = mServerConnections.getUrlPostResponse(
                    url_log_in_view, urlParameters, true);
            Log.v(" Login : ", "xmlResponde : " + xmlResponde);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        if (xmlResponde != null) {
            try {
                if (xmlResponde.has(Constants.LOGIN_FLOW_DETAILS)) {
                    localJSONObject = xmlResponde
                            .getJSONObject(Constants.LOGIN_FLOW_DETAILS);
                    if (localJSONObject == null) {
                        return null;
                    }

                    String hubCitiId = localJSONObject.getString("hubCitiId");
                    boolean iSsaveHubCitiId = saveHubCitiId(hubCitiId);
                    Log.i("iSsaveHubCitiId", "" + iSsaveHubCitiId);

                    templateName = localJSONObject.optString("templateName");

                    //Getting Navigation bar values and storing it to shared preference to access
                    // from all the screens
                    if (localJSONObject.has("bkImgPath")) {
                        Constants.setNavigationBarValues("bkImgPath", localJSONObject.getString
                                ("bkImgPath"));
                    }
                    if (localJSONObject.has("homeImgPath")) {
                        Constants.setNavigationBarValues("homeImgPath", localJSONObject.getString
                                ("homeImgPath"));
                    }
                    if (localJSONObject.has("hamburgerImg")) {
                        Constants.setNavigationBarValues("hamburgerImg", localJSONObject.getString
                                ("hamburgerImg"));
                    }
                    if (localJSONObject.has("titleTxtColor")) {
                        Constants.setNavigationBarValues("titleTxtColor", localJSONObject
                                .getString("titleTxtColor"));
                    }
                    if (localJSONObject.has("titleBkGrdColor")) {
                        Constants.setNavigationBarValues("titleBkGrdColor", localJSONObject
                                .getString("titleBkGrdColor"));
                    }
                    //Added by sharanamma
                    if (localJSONObject.has("domainName")) {
                       Properties.url_local_server_aboutandprivacy = localJSONObject.getString("domainName");
                        Properties.url_ssqr_server = localJSONObject.getString("domainName");
                    }
                } else if (xmlResponde.has("Response")) {
                    localJSONObject = xmlResponde
                            .getJSONObject("Response");
                    if (localJSONObject != null && localJSONObject.has("hamburgerImg")) {
                        Constants.setNavigationBarValues("hamburgerImg", localJSONObject.getString
                                ("hamburgerImg"));
                    }
                }
                // if no records found

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (localJSONObject != null) {
            LoginScreenSingleton.clearLoginScreenSingleton();
            LoginScreenSingleton.getLoginScreenSingleton(localJSONObject);
        }
        bIsLoginFlag = true;
        displaySplash();
    }
}
