package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.AsyncTask.GetCategoriesAsyncTask;
import com.hubcity.android.CustomLayouts.MultiSelectionSpinner;
import com.hubcity.android.commonUtil.ClaimGetDetails;
import com.hubcity.android.commonUtil.ClaimModel;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.CustomerInfoList;
import com.hubcity.android.commonUtil.GetClaimObject;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CommonMethods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by subramanya.v on 8/19/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class ClaimBusinessActivity extends CustomTitleBar implements View.OnClickListener {
    private EditText etBusinessName, etPhoneNumber, etTypeBusinessView, etWebSiteView, etAdressView, etAdress1View, etCityView, etStateView, etPostal, etCountryView,
            etContactName, etEmailAddress;
    private Button btnSubmit;
    private Activity activity;
    private String business;
    private String typeBusiness;
    private String phone;
    private String website;
    private String address;
    private String address1;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private String loginEmail;
    private String contactName;
    private String emailAddress;
    private String retailLocationID;
    private RestClient mRestClient;
    private WebView webview;
    private ImageView bannerImageView;
    private ProgressBar progressBar;
    private MultiSelectionSpinner categorySpinner;

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final LinkedList<String> category = new LinkedList<>();
    private final LinkedList<String> CategoryURL = new LinkedList<>();
    private EditText mailAddress;
    private EditText mailAddress2;
    private EditText mailCity;
    private EditText mailState;
    private EditText mailPostalCode;
    private EditText mailCountry;
    private EditText mailPhone;
    private String mailOptnAdd1;
    private String mailOptnAdd2;
    private String mailOptnCity;
    private String mailOptnState;
    private String mailOptnPostalCode;
    private String mailOptnPhone;
    private String keyword;
    private EditText eKeyword;
    private String mailOptnCountry;
    private String isMailAddress = "false";
    private ProgressDialog mainDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claim_business);
        try {
            activity = ClaimBusinessActivity.this;
            title.setText(R.string.claim_your_business);
            bindView();
            setItemClickListener();
            getIntentData();
            btnSubmit.setOnClickListener(this);
            mRestClient = RestClient.getInstance();
            fetchClaimFeilds();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setItemClickListener() {
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void fetchClaimFeilds() {
        mainDialog = new ProgressDialog(this);
        mainDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mainDialog.setMessage("Please Wait..");
        mainDialog.setCancelable(false);
        mainDialog.show();

        ClaimGetDetails request = UrlRequestParams.getClaimBusiness(retailLocationID);
        mRestClient.getClaimDetails(request, new Callback<GetClaimObject>() {
            @Override
            public void success(GetClaimObject getClaimObject, Response response) {
                try {
                    String responseText = getClaimObject.getResponseText();
                    ArrayList<CustomerInfoList> claimInfo;
                    claimInfo = getClaimObject.getCustomerInfoList();

                    if (claimInfo != null) {
                        CustomerInfoList claimInfoObject = claimInfo.get(0);
                        business = claimInfoObject.getName();
                        typeBusiness = claimInfoObject.getType();


                        website = claimInfoObject.getWebsite();
                        address = claimInfoObject.getRetAddress();
                        address1 = claimInfoObject.getRetAddress2();
                        city = claimInfoObject.getCity();
                        state = claimInfoObject.getState();
                        postalCode = claimInfoObject.getPostalCode();
                        country = claimInfoObject.getCountry();
                        phone = claimInfoObject.getPhone();
                        mailOptnAdd1 = claimInfoObject.getMailAddress();
                        mailOptnAdd2 = claimInfoObject.getMailAddress2();
                        mailOptnCity = claimInfoObject.getMailCity();
                        mailOptnState = claimInfoObject.getMailState();
                        mailOptnPostalCode = claimInfoObject.getMailPostalCode();
                        mailOptnPhone = claimInfoObject.getMailPhone();
                        mailOptnCountry = claimInfoObject.getMailCountry();
                        keyword = claimInfoObject.getKeywords();


                        setEditFields();
                        setBannerImageOrText(claimInfoObject);
                        getCategoryList();

                    }
                    hideKeyBoard();
                    if (!responseText.equalsIgnoreCase("Success")) {
                        hideProgressBar(mainDialog);
                        displayToast(responseText);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    hideProgressBar(mainDialog);
                    if (error.getResponse() != null) {
                        displayToast(error
                                .getResponse().getReason());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setBannerImageOrText(CustomerInfoList claimInfoObject) {
        if (claimInfoObject.getClaimImg() != null) {
            bannerImageView.setVisibility(View.VISIBLE);
            webview.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            new CommonMethods().loadImage(this, progressBar, claimInfoObject.getClaimImg(), bannerImageView);
        } else if (claimInfoObject.getClaimTxt() != null) {
            bannerImageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            webview.setVisibility(View.VISIBLE);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.loadDataWithBaseURL("", claimInfoObject.getClaimTxt(), "text/html", "utf-8", "");
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    // view.loadUrl(url);
                    Intent intent = new Intent(
                            activity,
                            ScanseeBrowserActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(CommonConstants.URL, url);
                    activity.startActivity(intent);
                    return true;
                }
            });
        }
    }

    private void setEditFields() {
        try {
            if (business != null && !business.isEmpty()) {
                etBusinessName.setText(business.trim());
                etBusinessName.setSelection(etBusinessName.getText().length());
            }
            if (typeBusiness != null && !typeBusiness.isEmpty()) {
                etTypeBusinessView.setText(typeBusiness.trim());
            }
            if (website != null && !website.isEmpty()) {
                etWebSiteView.setText(website.trim());
            }
            if (address != null && !address.isEmpty()) {
                etAdressView.setText(address.trim());
            }
            if (address1 != null && !address1.isEmpty()) {
                etAdress1View.setText(address1.trim());
            }
            if (city != null && !city.isEmpty()) {
                etCityView.setText(city.trim());
            }
            if (state != null && !state.isEmpty()) {
                etStateView.setText(state.trim());
            }
            if (postalCode != null && !postalCode.isEmpty()) {
                etPostal.setText(postalCode.trim());
            }
            if (country != null && !country.isEmpty()) {
                etCountryView.setText(country.trim());
            }
            if (phone != null && !phone.isEmpty()) {
                etPhoneNumber.setText(phone.trim());
            }
            if (keyword != null && !keyword.isEmpty()) {
                eKeyword.setText(keyword.trim());
            }

            if (mailOptnAdd1 != null && !mailOptnAdd1.isEmpty()) {
                mailAddress.setText(mailOptnAdd1.trim());
            }
            if (mailOptnAdd2 != null && !mailOptnAdd2.isEmpty()) {
                mailAddress2.setText(mailOptnAdd2.trim());
            }
            if (mailOptnCity != null && !mailOptnCity.isEmpty()) {
                mailCity.setText(mailOptnCity.trim());
            }
            if (mailOptnState != null && !mailOptnState.isEmpty()) {
                mailState.setText(mailOptnState.trim());
            }
            if (mailOptnPostalCode != null && !mailOptnPostalCode.isEmpty()) {
                mailPostalCode.setText(mailOptnPostalCode.trim());
            }
            if (mailOptnCountry != null && !mailOptnCountry.isEmpty()) {
                mailCountry.setText(mailOptnCountry.trim());
            }
            if (mailOptnPhone != null && !mailOptnPhone.isEmpty()) {
                mailPhone.setText(mailOptnPhone.trim());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getCategoryList() {
        new GetCategoriesAsyncTask(new GetCategoriesAsyncTask.CategoryInterface() {
            @Override
            public void onResultSuccess(ArrayList<String> categoryList) {
                hideProgressBar(mainDialog);
                categorySpinner.setItems(categoryList);
                if (typeBusiness != null && !typeBusiness.isEmpty()) {
                    if (typeBusiness.contains(",")) {
                        List<String> list = Arrays.asList(typeBusiness.split(","));
                        categorySpinner.setSelection(list);
                    } else {
                        if (categoryList.contains(typeBusiness)) {
                            categorySpinner.setSelection(categoryList.indexOf(typeBusiness));
                        }
                    }

                }
                else
                {
                    categorySpinner.removeDefault();
                }
            }
        }).execute();
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("loginEmail")) {
                loginEmail = intent.getExtras().getString("loginEmail");
            }
            if (intent.hasExtra("retailLocationID")) {
                retailLocationID = intent.getExtras().getString("retailLocationID");
            }
        }

    }

    private void bindView() {
        TextView cityText = (TextView) findViewById(R.id.city_text);
        TextView stateText = (TextView) findViewById(R.id.state_text);
        TextView postalText = (TextView) findViewById(R.id.postal_text);
        TextView countryText = (TextView) findViewById(R.id.country_text);
        TextView businessNameText = (TextView) findViewById(R.id.textview_business_name);
        TextView businessTypeText = (TextView) findViewById(R.id.textview_business_type);
        TextView addressText = (TextView) findViewById(R.id.textview_address);
        TextView phoneNumberText = (TextView) findViewById(R.id.textview_phone_number);
        TextView streetAdress = (TextView) findViewById(R.id.street_adress);
        TextView claimTextField = (TextView) findViewById(R.id.claim_text_field);
        TextView textContactName = (TextView) findViewById(R.id.textview_contact_name);
        TextView textEmailAddress = (TextView) findViewById(R.id.textview_email_address);



        /*TextView mailCityText = (TextView) findViewById(R.id.mail_city_text);
        TextView mailStateText = (TextView) findViewById(R.id.mail_state_text);
        TextView mailPostalText = (TextView) findViewById(R.id.mail_postal_text);
        TextView mailCountryText = (TextView) findViewById(R.id.mail_country_text);*/


        setMadatoryText(cityText, activity.getString(R.string.city));
        setMadatoryText(stateText, activity.getString(R.string.province));
        setMadatoryText(postalText, activity.getString(R.string.postal));
        setMadatoryText(countryText, activity.getString(R.string.country));
        setMadatoryText(businessNameText, activity.getString(R.string.business_name));
        setMadatoryText(businessTypeText, activity.getString(R.string.type_business));
        setMadatoryText(addressText, activity.getString(R.string.address));
        setMadatoryText(phoneNumberText, activity.getString(R.string.phone_number));
        setMadatoryText(streetAdress, activity.getString(R.string.street_address));

        setMadatoryText(claimTextField, activity.getString(R.string.contact_information));
        setMadatoryText(textContactName, activity.getString(R.string.contact_name));
        setMadatoryText(textEmailAddress, activity.getString(R.string.email_address));


   /*     setMadatoryText(mailCityText, activity.getString(R.string.city));
        setMadatoryText(mailStateText, activity.getString(R.string.province));
        setMadatoryText(mailPostalText, activity.getString(R.string.postal));
        setMadatoryText(mailCountryText, activity.getString(R.string.country));*/


        etBusinessName = (EditText) findViewById(R.id.business_name);
        etTypeBusinessView = (EditText) findViewById(R.id.type_business);
        etWebSiteView = (EditText) findViewById(R.id.website);
        etAdressView = (EditText) findViewById(R.id.businessAdress);
        etAdress1View = (EditText) findViewById(R.id.address1);
        etCityView = (EditText) findViewById(R.id.city);
        etPostal = (EditText) findViewById(R.id.postal);
        etCountryView = (EditText) findViewById(R.id.country);
        etStateView = (EditText) findViewById(R.id.state);
        etPhoneNumber = (EditText) findViewById(R.id.phone_number);
        etContactName = (EditText) findViewById(R.id.contact_name);
        etEmailAddress = (EditText) findViewById(R.id.email_address);

        mailAddress = (EditText) findViewById(R.id.business_mail_adress);
        mailAddress2 = (EditText) findViewById(R.id.mail_address);
        mailCity = (EditText) findViewById(R.id.mail_city);
        mailState = (EditText) findViewById(R.id.mail_state);
        mailPostalCode = (EditText) findViewById(R.id.mail_postal);
        mailCountry = (EditText) findViewById(R.id.mail_country);
        mailPhone = (EditText) findViewById(R.id.mail_phone_number);
        eKeyword = (EditText) findViewById(R.id.keyword);


        etBusinessName.setOnFocusChangeListener(onFocusChangeListener);
        etTypeBusinessView.setOnFocusChangeListener(onFocusChangeListener);
        etWebSiteView.setOnFocusChangeListener(onFocusChangeListener);
        etAdressView.setOnFocusChangeListener(onFocusChangeListener);
        etAdress1View.setOnFocusChangeListener(onFocusChangeListener);
        etCityView.setOnFocusChangeListener(onFocusChangeListener);
        etPostal.setOnFocusChangeListener(onFocusChangeListener);
        etCountryView.setOnFocusChangeListener(onFocusChangeListener);
        etStateView.setOnFocusChangeListener(onFocusChangeListener);
        etPhoneNumber.setOnFocusChangeListener(onFocusChangeListener);
        etContactName.setOnFocusChangeListener(onFocusChangeListener);
        etEmailAddress.setOnFocusChangeListener(onFocusChangeListener);


        mailAddress.setOnFocusChangeListener(onFocusChangeListener);
        mailAddress2.setOnFocusChangeListener(onFocusChangeListener);
        mailCity.setOnFocusChangeListener(onFocusChangeListener);
        mailState.setOnFocusChangeListener(onFocusChangeListener);
        mailPostalCode.setOnFocusChangeListener(onFocusChangeListener);
        mailCountry.setOnFocusChangeListener(onFocusChangeListener);
        mailPhone.setOnFocusChangeListener(onFocusChangeListener);


        btnSubmit = (Button) findViewById(R.id.submit_btn);
        webview = (WebView) findViewById(R.id.webview);
        bannerImageView = (ImageView) findViewById(R.id.bannerImageView);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        categorySpinner = (MultiSelectionSpinner) findViewById(R.id.spinner);

    }

    private void setMadatoryText(TextView textView, String text) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(text);
        int start = builder.length();
        builder.append("*");
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }

    /**
     * Set cursor to end of text in edittext when user clicks Next on Keyboard.
     */
    private final View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {
                ((EditText) view).setSelection(((EditText) view).getText().length());
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_btn:
                business = etBusinessName.getText().toString().trim();
                //typeBusiness = etTypeBusinessView.getText().toString().trim();
                typeBusiness = categorySpinner.getSelectedItemsAsString();
                website = etWebSiteView.getText().toString().trim();
                address = etAdressView.getText().toString().trim();
                address1 = etAdress1View.getText().toString().trim();
                city = etCityView.getText().toString().trim();
                state = etStateView.getText().toString().trim();
                postalCode = etPostal.getText().toString().trim();
                country = etCountryView.getText().toString().trim();
                phone = etPhoneNumber.getText().toString().trim();
                emailAddress = etEmailAddress.getText().toString().trim();
                contactName = etContactName.getText().toString().trim();

                keyword = eKeyword.getText().toString().trim();
                mailOptnAdd1 = mailAddress.getText().toString().trim();
                mailOptnAdd2 = mailAddress2.getText().toString().trim();
                mailOptnCity = mailCity.getText().toString().trim();
                mailOptnState = mailState.getText().toString().trim();
                mailOptnPostalCode = mailPostalCode.getText().toString().trim();
                mailOptnPhone = mailPhone.getText().toString().trim();
                mailOptnCountry = mailCountry.getText().toString().trim();


                boolean isValid = checkValidation();
                boolean isOptnValid = checkOptnValidation();
                if (isValid && isOptnValid) {
                    callClaimBusinessRequest();
                }
                break;
            default:
                break;

        }
    }

    private boolean checkOptnValidation() {
        boolean mailPhoneValid = CommonConstants.isValidPhoneNumber(mailOptnPhone);
        boolean isValid;
        if (!mailOptnPhone.isEmpty() && !mailPhoneValid) {
            Toast.makeText(activity, activity.getString(R.string.phone_valid), Toast.LENGTH_LONG)
                    .show();
            isValid = false;
        } else if (mailOptnPostalCode != null && !mailOptnPostalCode.isEmpty() && mailOptnPostalCode.length() < 5) {
            Toast.makeText(activity, activity.getString(R.string.zipcode), Toast.LENGTH_LONG)
                    .show();
            isValid = false;
        } else if ((mailOptnAdd1 ==  null || mailOptnAdd1.isEmpty()) && (mailOptnAdd2 == null || mailOptnAdd2.isEmpty()) && (mailOptnCity == null || mailOptnCity.isEmpty())
                && (mailOptnState == null ||mailOptnState.isEmpty())
                && (mailOptnPostalCode == null || mailOptnPostalCode.isEmpty()) && (mailOptnCountry == null || mailOptnCountry.isEmpty())
                && (mailOptnPhone == null ||mailOptnPhone.isEmpty())) {
            isValid = true;
            isMailAddress = "false";
        } else if ((mailOptnAdd1 != null && !mailOptnAdd1.isEmpty()) && (mailOptnAdd2 != null && !mailOptnAdd2.isEmpty()) && (mailOptnCity != null && !mailOptnCity.isEmpty())
                && (mailOptnState != null && !mailOptnState.isEmpty())
                && (mailOptnPostalCode != null && !mailOptnPostalCode.isEmpty()) && (mailOptnCountry != null && !mailOptnCountry.isEmpty())
                && (mailOptnPhone != null && !mailOptnPhone.isEmpty())) {
            isValid = true;
            isMailAddress = "true";
        } else {
            Toast.makeText(activity, activity.getString(R.string.mail_adress_valid), Toast.LENGTH_LONG)
                    .show();
            isValid = false;
        }
        return isValid;
    }

    private void callClaimBusinessRequest() {
        final ProgressDialog progDialog = new ProgressDialog(this);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setMessage("Please Wait..");
        progDialog.setCancelable(false);
        progDialog.show();
        ClaimModel detailsRequest = UrlRequestParams.sendClaimRequest(loginEmail, retailLocationID, business, typeBusiness, website,
                address, address1, city, state, postalCode, country, phone, contactName, emailAddress,keyword,mailOptnAdd1,mailOptnAdd2,mailOptnCity,
                mailOptnState,mailOptnPostalCode,mailOptnCountry,mailOptnPhone,isMailAddress);
        mRestClient.setClaimRequest(detailsRequest, new Callback<ClaimRequestObj>() {
            @Override
            public void success(ClaimRequestObj claimRequestObj, Response response) {
                try {
                    String responseText = claimRequestObj.getResponseText();
                    if (responseText.equalsIgnoreCase("Success")) {
                        String claimCategory = null;
                        String claimCategoryURL = null;
                        for (RetDetailList claimObj : claimRequestObj.getRetDetailList()) {
                            claimCategory = claimObj.getClaimCategory();
                            claimCategoryURL = claimObj.getClaimCategoryURL();
                        }
                        String[] categoryArray = new String[0];
                        String[] categoryURLArray = new String[0];
                        if (claimCategoryURL != null) {
                            categoryURLArray = claimCategoryURL.split(",");
                        }
                        if (claimCategory != null) {
                            categoryArray = claimCategory.split(",");
                        }

                        Collections.addAll(category, categoryArray);
                        Collections.addAll(CategoryURL, categoryURLArray);

                        if (CategoryURL.size() != 0) {
                            Intent appSite = new Intent(ClaimBusinessActivity.this, ScanseeBrowserActivity
                                    .class);
                            appSite.putExtra(CommonConstants.URL, CategoryURL.get(0));
                            startActivity(appSite);
                            finish();
                        }
                    } else {
                        displayToast(responseText);
                    }
                    hideProgressBar(progDialog);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    hideProgressBar(progDialog);
                    if (error.getResponse() != null) {
                        displayToast(error
                                .getResponse().getReason());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void displayToast(String responseText) {
        Toast.makeText(activity, responseText, Toast.LENGTH_LONG).show();
    }

    private void hideProgressBar(ProgressDialog progDialog) {
        try {
            if (progDialog.isShowing()) {
                progDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean checkValidation() {
        boolean phoneValid = CommonConstants.isValidPhoneNumber(phone);
        boolean webUrlValid = CommonConstants.isValidUrl(website);

        boolean isValid = true;
        if ((business == null || business.isEmpty()) || (typeBusiness == null || typeBusiness.isEmpty())
                || (address == null || address.isEmpty()) || (city == null || city
                .isEmpty()) || (state == null || state
                .isEmpty()) || (country == null || country.isEmpty())
                || (postalCode == null || postalCode.isEmpty()) || (phone == null || phone.isEmpty())
                || (emailAddress == null || emailAddress.isEmpty()) || (contactName == null || contactName.isEmpty())) {
            Toast.makeText(activity, activity.getString(R.string.mandatory), Toast.LENGTH_LONG)
                    .show();
            isValid = false;
        } else if (!phoneValid) {
            Toast.makeText(activity, activity.getString(R.string.phone_valid), Toast.LENGTH_LONG)
                    .show();
            isValid = false;
        } else if (website != null  && !website.isEmpty()&& !webUrlValid) {
            Toast.makeText(activity, activity.getString(R.string.website_valid), Toast.LENGTH_LONG)
                    .show();
            isValid = false;
        } else if (postalCode.length() < 5) {
            Toast.makeText(activity, activity.getString(R.string.zipcode), Toast.LENGTH_LONG)
                    .show();
            isValid = false;
        }
        if (emailAddress != null && !emailAddress.isEmpty()) {
            if (!CommonMethods.emailValidation(emailAddress)) {
                Toast.makeText(activity, activity.getString(R.string.email_validation), Toast.LENGTH_LONG)
                        .show();
                isValid = false;
            }
        }

        return isValid;
    }

    private void hideKeyBoard() {
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}
