package com.hubcity.android.screens;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.LoginScreenSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.squareup.picasso.Picasso;

@SuppressLint("SetJavaScriptEnabled")
public class PrivacyPolicyActivity extends CustomTitleBar {

	public static final String DEBUG_TAG = "PrivacyPolicyActivity";
	//private Bitmap hubCitiImg;
	private String hubCitiImgUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.webview);

		try {
			LoginScreenSingleton loginScreenSingleton = LoginScreenSingleton
                    .getLoginScreenSingleton();
			if (loginScreenSingleton != null) {
                hubCitiImgUrl = loginScreenSingleton.getSmallLogoUrl();
                hubCitiImgUrl = hubCitiImgUrl.replace(" ","%20");
            }

			super.title.setVisibility(View.GONE);

			super.imageLogo.setVisibility(View.VISIBLE);
			//super.imageLogo.setImageBitmap(hubCitiImg);
			Picasso.with(this).load(hubCitiImgUrl).placeholder(R.drawable.loading_button).into(super
                    .imageLogo);

//			super.rightImage.setText("Privacy");
//			super.rightImage.setTextSize(16);
//			super.rightImage.setTypeface(null, Typeface.BOLD);
//			rightImage.setTextColor(Color.WHITE);
			((TextView)findViewById(R.id.right_text)).setText("Privacy");
			((TextView)findViewById(R.id.right_text)).setTextSize(16);
			((TextView)findViewById(R.id.right_text)).setTypeface(null, Typeface.BOLD);
			((TextView)findViewById(R.id.right_text)).setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
			rightImage.setVisibility(View.GONE);
			divider.setVisibility(View.GONE);
			super.leftTitleImage.setVisibility(View.GONE);

			WebView webviewprivacypolicy = (WebView) findViewById(R.id.webView1);

			webviewprivacypolicy.getSettings().setJavaScriptEnabled(true);
			String url_privacy_policy = Properties.url_ssqr_server
                    + "/Images/hubciti/html/";
			webviewprivacypolicy.loadUrl(url_privacy_policy
                    + UrlRequestParams.getHubCityId() + "/Privacy_Policy.html");

			webviewprivacypolicy.getSettings().setBuiltInZoomControls(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}