package com.hubcity.android.screens;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hubcity.android.CustomLayouts.ToggleButtonView;
import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.CitiExperienceEntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressLint("NewApi")
public class CitiExperienceActivity extends CustomTitleBar implements
        OnClickListener, ToggleButtonView.ToggleViewInterface {
    public static final String DEBUG_TAG = CitiExperienceActivity.class
            .getName();
    private ListView getRetailersListView;
    private AustinExpRetailersListAdapter getRetailersListAdapter;
    private AustinExpRetailersListFilterAdapter expRetailersListFilterAdapter;
    String preferredRadius, lastVisitedRecord;
    boolean gpsEnabled;
    String gpsLatitude = null, gpsLongitude = null;
    LocationManager locationManager;
    String retAffId = null;
    BottomButtons bb = null;
    MenuItem item;
    protected String citiExpId;
    protected String catIds = "";
    protected String searchKey = null;
    protected String mItemId, mBottomId;
    protected String responseText;
    private ArrayList<RetailerBo> mRetailerList;
    private ArrayList<RetailerBo> finaRetailerList;
    protected CustomImageLoader customImageLoader;
    RetailerListsAsyncTask retailerAsyncTask;
    int previousPosition = 0;
    protected String filterId = null;

    boolean filterFlag = false;
    Activity activity;
    LinearLayout linearLayout = null;
    boolean hasBottomBtns = false;
    boolean enableBottomButton = true;
    String sortColumn = "distance";

    ArrayList<Item> items = new ArrayList<>();
    PrepareEventList prepareEventList = new PrepareEventList();
    ArrayList<PrepareEventList> eventList;
    View moreResultsView;
    Button moreInfo;

    String locSpecials = "0", interests = "";
    HubCityContext hubCiti = null;
    boolean isViewMore = false, isAlreadyLoading, isItemClicked, isFirst = true, isRefresh = false;

    TextView descText;
    String Description;
    TextView mLabelDistance, mLabelName;
    private CustomNavigation customNaviagation;
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.austin_exp_get_retailers);
        CommonConstants.hamburgerIsFirst = true;


        getIntentData();
        bindViews();
        initialize();

        try {
            // Initiating Bottom button class
            bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
            // Add screen name when needed
            bb.setActivityInfo(activity, "City Experience");
        } catch (Exception e) {
            e.printStackTrace();
        }

        backImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finishAction();
            }
        });


        getRetailersListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                isItemClicked = true;

                try {
                    if (filterId == null) {
                        CitiExperienceEntryItem retailerBo = null;
                        try {
                            retailerBo = (CitiExperienceEntryItem) items
                                    .get(position);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (retailerBo != null) {
                            Intent retailerInfo = new Intent(
                                    CitiExperienceActivity.this,
                                    RetailerActivity.class);

                            retailerInfo.putExtra(Constants.TAG_RETAIL_ID,
                                    retailerBo.retailerId);
                            retailerInfo.putExtra(Constants.MAIN_MENU_ID,
                                    mainMenuId);
                            retailerInfo
                                    .putExtra(
                                            Constants.MENU_ITEM_ID_INTENT_EXTRA,
                                            getIntent()
                                                    .getExtras()
                                                    .getString(
                                                            Constants.MENU_ITEM_ID_INTENT_EXTRA));

                            retailerInfo.putExtra(Constants.TAG_RET_LIST_ID,
                                    retailerBo.retListId);
                            retailerInfo.putExtra(CommonConstants.TAG_DISTANCE,
                                    retailerBo.distance);

                            retailerInfo.putExtra(
                                    Constants.TAG_RETAILE_LOCATIONID,
                                    retailerBo.retailLocationId);
                            retailerInfo.putExtra(
                                    CommonConstants.TAG_RETAILER_NAME,
                                    retailerBo.retailerName);

                            retailerInfo.putExtra(
                                    CommonConstants.TAG_BANNER_IMAGE_PATH,
                                    retailerBo.bannerAdImagePath);
                            retailerInfo.putExtra(
                                    CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
                                    retailerBo.ribbonAdImagePath);
                            retailerInfo.putExtra(
                                    CommonConstants.TAG_RIBBON_AD_URL,
                                    retailerBo.ribbonAdURL);
                            retailerInfo.putExtra(
                                    CommonConstants.TAG_RETAILE_LOCATIONID,
                                    retailerBo.retailLocationId);

                            retailerInfo.putExtra(Constants.TAG_SCAN_TYPE_ID,
                                    "0");

                            retailerInfo.putExtra("ZipCode", Constants.getZipCode());
                            startActivityForResult(retailerInfo,
                                    Constants.STARTVALUE);

                        } else {
                            items.remove(position);
                            callRetailerAsyncTask();
                        }

                        hubCiti.setCancelled(false);
                    } else {

                        CitiExperienceEntryItem retailerBo = (CitiExperienceEntryItem) items
                                .get(position);

                        if (retailerBo != null) {
                            Intent retailerInfo = new Intent(
                                    CitiExperienceActivity.this,
                                    RetailerActivity.class);

                            retailerInfo.putExtra(Constants.TAG_RETAIL_ID,
                                    retailerBo.retailerId);
                            retailerInfo.putExtra(Constants.MAIN_MENU_ID,
                                    mainMenuId);
                            retailerInfo.putExtra(Constants.TAG_RET_LIST_ID,
                                    retailerBo.retListId);
                            retailerInfo.putExtra(CommonConstants.TAG_DISTANCE,
                                    retailerBo.distance);
                            retailerInfo
                                    .putExtra(
                                            Constants.MENU_ITEM_ID_INTENT_EXTRA,
                                            getIntent()
                                                    .getExtras()
                                                    .getString(
                                                            Constants.MENU_ITEM_ID_INTENT_EXTRA));
                            retailerInfo.putExtra(
                                    Constants.TAG_RETAILE_LOCATIONID,
                                    retailerBo.retailLocationId);
                            retailerInfo.putExtra(
                                    CommonConstants.TAG_RETAILER_NAME,
                                    retailerBo.retailerName);

                            retailerInfo.putExtra(
                                    CommonConstants.TAG_BANNER_IMAGE_PATH,
                                    retailerBo.bannerAdImagePath);
                            retailerInfo.putExtra(
                                    CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
                                    retailerBo.ribbonAdImagePath);
                            retailerInfo.putExtra(
                                    CommonConstants.TAG_RIBBON_AD_URL,
                                    retailerBo.ribbonAdURL);
                            retailerInfo.putExtra(
                                    CommonConstants.TAG_RETAILE_LOCATIONID,
                                    retailerBo.retailLocationId);

                            retailerInfo.putExtra(Constants.TAG_SCAN_TYPE_ID,
                                    "0");

                            retailerInfo.putExtra("ZipCode", Constants.getZipCode());
                            startActivityForResult(retailerInfo,
                                    Constants.STARTVALUE);

                        } else {
                            mRetailerList.remove(position);
                            callRetailerAsyncTask();
                        }

                        hubCiti.setCancelled(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //user for hamburger in modules
        drawerIcon.setVisibility(View.VISIBLE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
    }

    private void bindViews() {
        linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
        linearLayout.setVisibility(View.INVISIBLE);
        moreResultsView = getLayoutInflater().inflate(
                R.layout.events_pagination, getRetailersListView, true);
        moreInfo = (Button) moreResultsView
                .findViewById(R.id.events_view_more_button);
        moreInfo.setOnClickListener(this);
        getRetailersListView = (ListView) findViewById(R.id.lst_this_location_get_retailers);

        mLabelName = (TextView) this.findViewById(R.id.toggle_name);
        mLabelDistance = (TextView) this.findViewById(R.id.toggle_distance);

    }

    private void initialize() {
        hubCiti = (HubCityContext) getApplicationContext();
        hubCiti.setCancelled(false);
        activity = CitiExperienceActivity.this;
        OptionSortByList.sortChoice = "Distance";
        OptionSortByList.groupChoice = "";
        HubCityContext.savedValues = new HashMap<>();

        title.setSingleLine(false);
        title.setMaxLines(2);
        if (filterName != null && !filterName.isEmpty()) {
            title.setText(filterName);
        } else {
            title.setText("Choose Your" + "\n" + "Location");
        }
//        rightImage.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
        leftTitleImage.setVisibility(View.GONE);

        OptionSortByList.isSortByType = filterId == null;
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent.hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                && getIntent().getExtras().getString(
                Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
            mItemId = getIntent().getExtras().getString(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA);
        }

        if (intent.hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                && getIntent().getExtras().getString(
                Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
            mBottomId = getIntent().getExtras().getString(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
        }

        if (intent.hasExtra(Constants.CITI_EXPID_INTENT_EXTRA)) {
            citiExpId = getIntent().getExtras().getString(
                    Constants.CITI_EXPID_INTENT_EXTRA);
        }
        if (intent.hasExtra(Constants.SEARCH_KEY_INTENT_EXTRA)) {
            searchKey = getIntent().getExtras().getString(
                    Constants.SEARCH_KEY_INTENT_EXTRA);
        }
        if (intent.hasExtra(Constants.CAT_IDS_INTENT_EXTRA)) {
            catIds = getIntent().getExtras().getString(
                    Constants.CAT_IDS_INTENT_EXTRA);
        }
        if (intent.hasExtra(CommonConstants.FILTERCOUNT_EXTRA)) {
            retAffCount = getIntent().getExtras().getString(
                    CommonConstants.FILTERCOUNT_EXTRA);
            filterId = getIntent().getExtras().getString(
                    CommonConstants.FILTERID_EXTRA);
            retAffId = getIntent().getExtras().getString(
                    CommonConstants.FILTERID_EXTRA);
            filterName = getIntent().getExtras().getString(
                    CommonConstants.TAG_FILTERNAME);


            customImageLoader = new CustomImageLoader(getApplicationContext(), false);
            filterFlag = true;
        } else {
            customImageLoader = new CustomImageLoader(getApplicationContext(), false);
            filterFlag = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }
        try {
            bb.setActivityInfo(activity, "City Experience");
            // clear the list items before loading new set of items
            if (bb != null) {
                bb.setOptionsValues(getListValues(), mRetailerList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (hubCiti.isCancelled()) {
            return;
        }

        if (!isRefresh && !isItemClicked) {
            items.clear();
            finaRetailerList = new ArrayList<>();
            if (getRetailersListAdapter != null) {
                moreResultsView.setVisibility(View.GONE);

                getRetailersListAdapter = new AustinExpRetailersListAdapter(
                        CitiExperienceActivity.this, activity, items);

                getRetailersListView.setAdapter(getRetailersListAdapter);

                getRetailersListAdapter.notifyDataSetChanged();
            }
            // Add screen name when needed
            getSortItemName();
            refreshList();
        }
        isRefresh = false;
        isItemClicked = false;
    }

    @Override
    public void onToggleClick(String toggleBtnName) {

        if (toggleBtnName.equalsIgnoreCase("Distance")) {
            finaRetailerList = new ArrayList<>();
            sortColumn = toggleBtnName.toLowerCase();
            mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
            setToggleButtonColor();
            refreshList();
        } else if (toggleBtnName.equalsIgnoreCase("Name")) {
            finaRetailerList = new ArrayList<>();
            sortColumn = "atoz";
            mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
            mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            setToggleButtonColor();
            refreshList();
        } else {
            //should check
            CommonConstants.startMapScreen(activity, mRetailerList, false);
        }
    }

    private void getSortItemName() {

        HashMap<String, String> resultSet;
        resultSet = hubCiti.getFilterValues();
        if (resultSet != null && !resultSet.isEmpty()) {
            if (resultSet.containsKey("SortBy")) {
                sortColumn = resultSet.get("SortBy");
            }
        }
        setToggleButtonColor();
    }

    private void setToggleButtonColor() {
        if (sortColumn.equalsIgnoreCase("Distance")) {
            if (Build.VERSION.SDK_INT >= 16) {
                mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
                mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
            } else {
                mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
                mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
            }

            mLabelName.setTextColor(getResources().getColor(R.color.white));
            mLabelName.setTypeface(null, Typeface.NORMAL);
            mLabelDistance.setTextColor(getResources().getColor(R.color.black));
            mLabelDistance.setTypeface(null, Typeface.BOLD);
        }
        if (sortColumn.equalsIgnoreCase("Name") || sortColumn.equalsIgnoreCase("atoz")) {
            if (Build.VERSION.SDK_INT >= 16) {
                mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
                mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            } else {
                mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
                mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
            }
            mLabelDistance.setTextColor(getResources().getColor(R.color.white));
            mLabelDistance.setTypeface(null, Typeface.NORMAL);
            mLabelName.setTextColor(getResources().getColor(R.color.black));
            mLabelName.setTypeface(null, Typeface.BOLD);
        }
    }

    private void refreshList() {
        lowerLimit = "0";
        if (eventList != null) {
            eventList.clear();
        }
        items.clear();
        isLoadingMore = false;
        isViewMore = false;
        prepareEventList = new PrepareEventList();
        loadRetailers();
    }


    public boolean isJSONArray(JSONObject jsonObject, String value) {
        boolean isArray = false;

        JSONObject isJSONObject = jsonObject.optJSONObject(value);
        if (isJSONObject == null) {
            JSONArray isJSONArray = jsonObject.optJSONArray(value);
            if (isJSONArray != null) {
                isArray = true;
            }
        }
        return isArray;
    }

    class PrepareEventList {
        ArrayList<DetailsByGroup> listdetails = new ArrayList<>();

    }

    public class ListDetails {
        ArrayList<PrepareEventList> eventList = new ArrayList<>();
    }

    private String lowerLimit = "0";
    protected String maxCnt;
    protected String retGroupImgUrl;
    protected boolean nextPage = false;
    protected String nextpage;
    protected String retAffCount, filterName = null;
    protected String mainMenuId;
    private boolean isLoadingMore;

    ArrayList<RetailerBo> listRetailerList = null;

    class RetailerListsAsyncTask extends
            AsyncTask<String, String, ArrayList<RetailerBo>> {
        private JSONObject jsonResponse;

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        private ProgressDialog mDialog;
        ListDetails objListDetails = new ListDetails();

        @Override
        protected void onPreExecute() {
            if (!isLoadingMore) {
                mDialog = ProgressDialog.show(CitiExperienceActivity.this, "",
                        Constants.DIALOG_MESSAGE, true);
                mDialog.setCancelable(false);
                isLoadingMore = true;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<RetailerBo> listRetailerList) {
            super.onPostExecute(listRetailerList);

            if (filterId != null) {

                if (Description != null) {

                    descText = (TextView) findViewById(R.id.desc_text);
                    descText.setVisibility(View.VISIBLE);
                    // descText.setText("\t" + Description);
                    descText.setText(Description);
                }
            }

            if (filterId == null) {

                try {
                    if (nextPage) {
                        try {
                            getRetailersListView
                                    .removeFooterView(moreResultsView);
                            moreResultsView.setVisibility(View.GONE);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        getRetailersListView.addFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.VISIBLE);

                    } else {
                        getRetailersListView.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                eventList = new ArrayList<>();
                eventList = objListDetails.eventList;
                // items = new ArrayList<Item>();
                setValues(eventList);

                CitiExperienceActivity.this.mRetailerList = listRetailerList;

                if (bb != null) {
                    bb.setOptionsValues(getListValues(), mRetailerList);
                }

				/* for unsuccessful response */
                if (responseText != null
                        && !"Success".equalsIgnoreCase(responseText)) {
                    mDialog.dismiss();
                    getRetailersListAdapter = new AustinExpRetailersListAdapter(
                            CitiExperienceActivity.this, activity,
                            new ArrayList<Item>());
                    getRetailersListView.setAdapter(getRetailersListAdapter);
                    getRetailersListView.removeFooterView(moreResultsView);
                    final AlertDialog alertDialog = new AlertDialog.Builder(
                            CitiExperienceActivity.this).create();
                    alertDialog.setMessage(Html.fromHtml(responseText.replace(
                            "\\n", "<br/>")));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    alertDialog.dismiss();
                                    // finishAction();
                                }
                            });
                    alertDialog.show();

                } else {
                    // for successful response

                    if (catIds != null && !"".equals(catIds)) {

                        Intent baCategoryDisplay = new Intent(
                                getApplicationContext(),
                                CityExperienceByCategory.class);
                        setResult(1, baCategoryDisplay);

                        hubCiti.setCancelled(false);

                    }

                    setRetailerListAdapter(listRetailerList, items);
                }
            } else {

                try {
                    if (nextPage) {
                        try {
                            getRetailersListView
                                    .removeFooterView(moreResultsView);
                            moreResultsView.setVisibility(View.GONE);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        getRetailersListView.addFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.VISIBLE);

                    } else {
                        getRetailersListView.removeFooterView(moreResultsView);
                        moreResultsView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                eventList = new ArrayList<>();
                eventList = objListDetails.eventList;
                // items = new ArrayList<Item>();
                setValues(eventList);

                CitiExperienceActivity.this.mRetailerList = listRetailerList;

                if (bb != null) {
                    bb.setOptionsValues(getListValues(), mRetailerList);
                }

				/* for unsuccessful response */
                if (responseText != null
                        && !"Success".equalsIgnoreCase(responseText)) {
                    mDialog.dismiss();
                    expRetailersListFilterAdapter = new AustinExpRetailersListFilterAdapter(
                            CitiExperienceActivity.this, new ArrayList<Item>());

                    getRetailersListView
                            .setAdapter(expRetailersListFilterAdapter);
                    getRetailersListView.removeFooterView(moreResultsView);
                    final AlertDialog alertDialog = new AlertDialog.Builder(
                            CitiExperienceActivity.this).create();
                    alertDialog.setMessage(Html.fromHtml(responseText.replace(
                            "\\n", "<br/>")));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    alertDialog.dismiss();
                                    // finishAction();
                                }
                            });
                    alertDialog.show();

                } else {
                    // for successful response

                    if (catIds != null && !"".equals(catIds)
                            && !"0".equals(catIds)) {

                        Intent baCategoryDisplay = new Intent(
                                getApplicationContext(),
                                CityExperienceByCategory.class);
                        setResult(1, baCategoryDisplay);

                        hubCiti.setCancelled(false);

                    }
                    setRetailerListAdapterForFilter(listRetailerList, items);
                }
            }
            if (bb != null) {
                if (hasBottomBtns && enableBottomButton) {
                    bb.createbottomButtontTab(linearLayout, false);
                    enableBottomButton = false;
                }
            }

            linearLayout.setVisibility(View.VISIBLE);
            // Added to disable toggle click
            CommonConstants.disableClick = false;
            try {
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected ArrayList<RetailerBo> doInBackground(String... params) {


            try {

                String sortOrder = "ASC", cityIds = "";

                HashMap<String, String> resultSet;
                resultSet = hubCiti.getFilterValues();

                if (resultSet != null && !resultSet.isEmpty()) {

                    if (resultSet.containsKey("locSpecials")) {
                        locSpecials = resultSet.get("locSpecials");
                    }

                    if (resultSet.containsKey("savedinterests")) {
                        interests = resultSet.get("savedinterests");
                    }

                    if (resultSet.containsKey("savedCityIds")) {
                        cityIds = resultSet.get("savedCityIds");
                        cityIds = cityIds.replace("All,", "");
                    }

                    if (resultSet.containsKey("savedSubCatIds")) {
                        catIds = resultSet.get("savedSubCatIds");
                    }

                }
                objListDetails = new ListDetails();
                if (filterId == null) {


                    JSONObject jsonUrlParameters = mUrlRequestParams.createCityExperienceJsonParam(citiExpId,
                            catIds, searchKey, lowerLimit, mItemId,
                            mBottomId, Constants.getZipCode(), gpsLatitude,
                            gpsLongitude, sortColumn, sortOrder,
                            locSpecials, interests, cityIds);
                    String url_citi_expr = Properties.url_local_server
                            + Properties.hubciti_version
                            + "thislocation/citiexpretjson";
                    jsonResponse = mServerConnections.getUrlJsonPostResponse(
                            url_citi_expr, jsonUrlParameters);
                    if (jsonResponse != null) {


                        try {


                            if (jsonResponse.has("responseText")) {
                                responseText = jsonResponse
                                        .getString("responseText");
                            }

                            if (jsonResponse.has("maxCnt")) {
                                maxCnt = jsonResponse
                                        .getString("maxCnt");
                            }

                            if (jsonResponse.has("lastRowNum")) {
                                lowerLimit = jsonResponse
                                        .getString("lastRowNum");
                                Log.v("", "LAST ROW NUMBER : " + lowerLimit);
                            }

                            if (jsonResponse.has("retGroupImg")) {
                                retGroupImgUrl = jsonResponse
                                        .getString("retGroupImg");
                            }

                            // For BottomButton
                            if (jsonResponse
                                    .has("bottomBtnList")) {


                                hasBottomBtns = true;

                                try {
                                    ArrayList<BottomButtonBO> bottomButtonList = bb
                                            .parseForBottomButton(jsonResponse);
                                    BottomButtonListSingleton
                                            .clearBottomButtonListSingleton();
                                    BottomButtonListSingleton
                                            .getListBottomButton(bottomButtonList);

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                            } else {
                                hasBottomBtns = false;
                            }


                            if (jsonResponse.has("retAffCount")) {
                                if (jsonResponse
                                        .has("retAffCount")) {
                                    retAffCount = jsonResponse
                                            .getString("retAffCount");
                                }
                                if (jsonResponse.has("retAffId")) {
                                    retAffId = jsonResponse
                                            .getString("retAffId");
                                }
                                if (jsonResponse
                                        .has("retAffName")) {
                                    filterName = jsonResponse
                                            .getString("retAffName");
                                }
                            }
                            if (jsonResponse.has("mainMenuId")) {
                                mainMenuId = jsonResponse
                                        .getString("mainMenuId");
                                Constants.saveMainMenuId(mainMenuId);
                            }


                            if (jsonResponse
                                    .has("retailerDetail")) {

                                JSONArray retailersList = new JSONArray();

                                try {
                                    retailersList
                                            .put(jsonResponse
                                                    .getJSONObject("retailerDetail"));
                                } catch (Exception e) {
                                    retailersList = jsonResponse
                                            .getJSONArray("retailerDetail");
                                }

                                listRetailerList = new ArrayList<>();

                                if (retailersList != null) {
                                    prepareEventList = new PrepareEventList();
                                    for (int i = 0; i < retailersList
                                            .length(); i++) {
                                        JSONObject retailer = retailersList
                                                .getJSONObject(i);
                                        listRetailerList = addToRetailerArrayList(
                                                retailer, listRetailerList);
                                    }
                                    objListDetails.eventList
                                            .add(prepareEventList);
                                }

                                try {
                                    if (listRetailerList != null) {
                                        finaRetailerList
                                                .addAll(listRetailerList);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();

                                }

                                // for Next button in the
                                // list
                                try {

                                    if ("1".equalsIgnoreCase(jsonResponse
                                            .getString("nextPage"))) {
                                        nextPage = true;

                                    } else if ("0"
                                            .equalsIgnoreCase(jsonResponse
                                                    .getString("nextPage"))) {

                                        nextPage = false;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                } else {
                    // For Filters

                    JSONObject jsonUrlParameters = mUrlRequestParams
                            .createFilterJsonParam(catIds,
                                    filterId, lowerLimit, Constants.getZipCode(), gpsLatitude,
                                    gpsLongitude, sortColumn, sortOrder,
                                    locSpecials, searchKey, mBottomId, mItemId);

                    String url_citi_expr_partner_ret = Properties.url_local_server
                            + Properties.hubciti_version
                            + "thislocation/partnerretjson";
//                    String url_citi_expr_partner_ret ="http://10.10.221.2:9990/HubCiti_Coupon/thislocation/partnerretjson";
                    jsonResponse = mServerConnections.getUrlJsonPostResponse(
                            url_citi_expr_partner_ret, jsonUrlParameters);

                    if (jsonResponse != null) {

                        try {

                            responseText = jsonResponse
                                    .getString("responseText");
                            if (jsonResponse.has("maxCnt")) {
                                maxCnt = jsonResponse
                                        .getString("maxCnt");
                            }
                            if (jsonResponse.has("nextPage")) {
                                nextpage = jsonResponse
                                        .getString("nextPage");
                            }
                            if (jsonResponse.has("retGroupImg")) {
                                retGroupImgUrl = jsonResponse
                                        .getString("retGroupImg");
                            }
                            if (jsonResponse.has("sDescription")) {
                                Description = jsonResponse
                                        .getString("sDescription");
                            } else {
                                Description = null;
                            }
                            // For BottomButton
                            if (jsonResponse.has("bottomBtnList")) {
                                hasBottomBtns = true;
                                try {
                                    ArrayList<BottomButtonBO> bottomButtonList = bb
                                            .parseForBottomButton(jsonResponse);
                                    BottomButtonListSingleton
                                            .clearBottomButtonListSingleton();
                                    BottomButtonListSingleton
                                            .getListBottomButton(bottomButtonList);

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                            } else {
                                hasBottomBtns = false;
                            }

                            if (jsonResponse.has("retAffCount")) {
                                if (jsonResponse
                                        .has("retAffCount")) {
                                    retAffCount = jsonResponse
                                            .getString("retAffCount");
                                }
                                if (jsonResponse.has("retAffId")) {
                                    retAffId = jsonResponse
                                            .getString("retAffId");
                                }
                                if (jsonResponse
                                        .has("retAffName")) {
                                    filterName = jsonResponse
                                            .getString("retAffName");
                                }
                            }
                            if (jsonResponse.has("mainMenuId")) {
                                mainMenuId = jsonResponse
                                        .getString("mainMenuId");
                                Constants.saveMainMenuId(mainMenuId);
                            }


                            JSONArray retailersList = jsonResponse
                                    .optJSONArray("retailers");
                            listRetailerList = new ArrayList<>();
                            prepareEventList = new PrepareEventList();
                            if (retailersList != null) {
                                int ival = finaRetailerList.size();
                                lowerLimit = String.valueOf(ival
                                        + retailersList.length());
                                for (int i = 0; i < retailersList.length(); i++) {
                                    JSONObject retailer = retailersList
                                            .getJSONObject(i);
                                    listRetailerList = addToRetailerArrayList(
                                            retailer, listRetailerList);
                                }

                                objListDetails.eventList
                                        .add(prepareEventList);
                            } else {
                                // means it is a single Retailer
                                lowerLimit = String.valueOf(1);
                                JSONObject retailer = jsonResponse
                                        .optJSONObject("retailers");
                                listRetailerList = addToRetailerArrayList(
                                        retailer, listRetailerList);

                                objListDetails.eventList
                                        .add(prepareEventList);
                            }

                            if (listRetailerList != null) {
                                finaRetailerList.addAll(listRetailerList);
                            }
                            // END-to add paginatioList to the final list

                            // for Next button in the list
                            nextPage = "1".equalsIgnoreCase(nextpage);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        return finaRetailerList;

                    }
                }

            } catch (Exception e1) {
                e1.printStackTrace();
            }

            return finaRetailerList;
        }
    }

    private ArrayList<RetailerBo> addToRetailerArrayList(JSONObject retailer,
                                                         ArrayList<RetailerBo> listRetailer) throws JSONException {

        String rowNumber = "", retailerId = "", retailerName = "", retailLocationId = "", distance
                = "", retailAddress = "", retailAddress1 = "", retailAddress2 = "", completeAddress = "", logoImagePath = "",
                bannerAdImagePath = "", ribbonAdImagePath = "", ribbonAdURL = "", saleFlag = "",
                latitude = "", longitude = "", retListId = "", city = "", state = "", postalCode = "", locationOpen = "";

        DetailsByGroup objDetailsByGroup = new DetailsByGroup();

        if (retailer.has("rowNumber")) {
            rowNumber = retailer.getString("rowNumber");
            objDetailsByGroup.rowNumber = rowNumber;
        }

        if (retailer.has("retailerId")) {
            retailerId = retailer.getString("retailerId");
            objDetailsByGroup.retailerId = retailerId;
        }

        if (retailer.has("retailerName")) {
            retailerName = retailer.getString("retailerName");
            objDetailsByGroup.retailerName = retailerName;
        }

        if (retailer.has("retailLocationId")) {
            retailLocationId = retailer.getString("retailLocationId");
            objDetailsByGroup.retailLocationId = retailLocationId;
        }

        if (retailer.has("distance")) {
            distance = retailer.getString("distance");
            objDetailsByGroup.distance = distance;
        }
        if (retailer.has("retaileraddress")) {
            retailAddress1 = retailer.getString("retaileraddress");
            objDetailsByGroup.retailAddress = retailer
                    .getString("retaileraddress");
        }

        if (retailer.has("retaileraddress1")) {
            retailAddress1 = retailer.getString("retaileraddress1");
            objDetailsByGroup.retailAddress1 = retailer
                    .getString("retaileraddress1");
        }

        if (retailer.has("retaileraddress2")) {
            retailAddress2 = retailer.getString("retaileraddress2");
            objDetailsByGroup.retailAddress2 = retailer
                    .getString("retaileraddress2");
        }
        if (retailer.has("retailAddress1")) {
            retailAddress1 = retailer.getString("retailAddress1");
            objDetailsByGroup.retailAddress1 = retailer
                    .getString("retailAddress1");
        }

        if (retailer.has("retailAddress2")) {
            retailAddress2 = retailer.getString("retailAddress2");
            objDetailsByGroup.retailAddress2 = retailer
                    .getString("retailAddress2");
        }
        if (retailer.has("retaileraddress3")) {
            objDetailsByGroup.retailAddress3 = retailer
                    .getString("retaileraddress3");
        }

        if (retailer.has("retaileraddress4")) {
            objDetailsByGroup.retailAddress4 = retailer
                    .getString("retaileraddress4");
        }

        if (retailer.has("city")) {
            objDetailsByGroup.city = retailer.getString("city");
        }

        if (retailer.has("state")) {
            objDetailsByGroup.state = retailer.getString("state");
        }

        if (retailer.has("postalCode")) {
            objDetailsByGroup.postalCode = retailer.getString("postalCode");
        }

        if (retailer.has("completeAddress")) {
            completeAddress = retailer.getString("completeAddress");
            objDetailsByGroup.completeAddress = completeAddress;
        }

        if (retailer.has("logoImagePath")) {
            logoImagePath = retailer.getString("logoImagePath");
            objDetailsByGroup.logoImagePath = logoImagePath;
        }

        if (retailer.has("bannerAdImagePath")) {
            bannerAdImagePath = retailer.getString("bannerAdImagePath");
            objDetailsByGroup.bannerAdImagePath = bannerAdImagePath;
        }

        if (retailer.has("ribbonAdImagePath")) {
            ribbonAdImagePath = retailer.getString("ribbonAdImagePath");
            objDetailsByGroup.ribbonAdImagePath = ribbonAdImagePath;
        }

        if (retailer.has("ribbonAdURL")) {
            ribbonAdURL = retailer.getString("ribbonAdURL");
            objDetailsByGroup.ribbonAdURL = ribbonAdURL;
        }

        if (retailer.has("retLatitude")) {
            latitude = retailer.getString("retLatitude");
            objDetailsByGroup.latitude = latitude;

        } else if (retailer.has("latitude")) {
            latitude = retailer.getString("latitude");
            objDetailsByGroup.latitude = latitude;

        }

        if (retailer.has("retLongitude")) {
            longitude = retailer.getString("retLongitude");
            objDetailsByGroup.longitude = longitude;

        } else if (retailer.has("longitude")) {
            longitude = retailer.getString("longitude");
            objDetailsByGroup.longitude = longitude;

        }

        if (retailer.has("saleFlag")) {
            saleFlag = retailer.getString("saleFlag");
            objDetailsByGroup.saleFlag = saleFlag;
        }

        if (retailer.has("retListId")) {
            retListId = retailer.getString("retListId");
            objDetailsByGroup.retListId = retListId;
        }

        if (retailer.has("catName")) {
            objDetailsByGroup.catName = retailer.getString("catName");
        }

        if (retailer.has("catId")) {
            objDetailsByGroup.catId = retailer.getString("catId");
        }
        if (retailer.has("locationOpen")) {
            objDetailsByGroup.locationOpen = retailer.getString("locationOpen");
            locationOpen = retailer.getString("locationOpen");
        }

        prepareEventList.listdetails.add(objDetailsByGroup);

        RetailerBo retailerObject = new RetailerBo(rowNumber, retailerId,
                retailerName, retailLocationId, distance, retailAddress1, retailAddress2,
                completeAddress, logoImagePath, bannerAdImagePath,
                ribbonAdImagePath, ribbonAdURL, saleFlag, latitude, longitude,
                retListId, city, state, postalCode, locationOpen);

        listRetailerList.add(retailerObject);
        return listRetailerList;

    }

    class DetailsByGroup {
        String rowNumber;
        String retailerId;
        String retailerName;
        String retailLocationId;
        String logoImagePath;
        String bannerAdImagePath;
        String ribbonAdImagePath;
        String ribbonAdURL;
        String saleFlag;
        String latitude;
        String longitude;
        String retListId;
        String distance;
        String retailAddress;
        String retailAddress1;
        String retailAddress2;
        String retailAddress3;
        String retailAddress4;
        String city;
        String state;
        String postalCode;
        String completeAddress;
        String catName;
        String catId;
        String locationOpen;

    }

    public void setValues(ArrayList<PrepareEventList> eventList) {
        TextView sortTextView = (TextView) findViewById(R.id.sort_text);

        if (!isViewMore && !"".equalsIgnoreCase(sortColumn)) {

            String sortText = "";

            if ("distance".equalsIgnoreCase(sortColumn)) {
                sortText = "Sorted By Distance";
            } else if ("atoz".equalsIgnoreCase(sortColumn)) {
                sortText = "Sorted By Name";
            }
            /*
             * SectionItem sItem = new SectionItem(sortText, 0);
			 * items.add(sItem);
			 */
            sortTextView.setText(sortText);
        }

        for (int i = 0; i < eventList.size(); i++) {

            for (int j = 0; j < eventList.get(i).listdetails.size(); j++) {
                items.add(new CitiExperienceEntryItem(
                        eventList.get(i).listdetails.get(j).rowNumber,
                        eventList.get(i).listdetails.get(j).retailerId,
                        eventList.get(i).listdetails.get(j).retailerName,
                        eventList.get(i).listdetails.get(j).retailLocationId,
                        eventList.get(i).listdetails.get(j).logoImagePath,
                        eventList.get(i).listdetails.get(j).bannerAdImagePath,
                        eventList.get(i).listdetails.get(j).ribbonAdImagePath,
                        eventList.get(i).listdetails.get(j).ribbonAdURL,
                        eventList.get(i).listdetails.get(j).saleFlag, eventList
                        .get(i).listdetails.get(j).latitude, eventList
                        .get(i).listdetails.get(j).longitude, eventList
                        .get(i).listdetails.get(j).retListId, eventList
                        .get(i).listdetails.get(j).distance, eventList
                        .get(i).listdetails.get(j).retailAddress,
                        eventList.get(i).listdetails.get(j).retailAddress1,
                        eventList.get(i).listdetails.get(j).retailAddress2,
                        eventList.get(i).listdetails.get(j).retailAddress3,
                        eventList.get(i).listdetails.get(j).retailAddress4,
                        eventList.get(i).listdetails.get(j).city, eventList
                        .get(i).listdetails.get(j).state, eventList
                        .get(i).listdetails.get(j).postalCode,
                        eventList.get(i).listdetails.get(j).completeAddress,
                        eventList.get(i).listdetails.get(j).catName, eventList
                        .get(i).listdetails.get(j).catId, eventList.get(i).listdetails.get(j).locationOpen));

            }

        }

    }

    public void setRetailerListAdapter(ArrayList<RetailerBo> listRetailerList,
                                       ArrayList<Item> items) {

        if (!items.isEmpty()) {
            getRetailersListAdapter = new AustinExpRetailersListAdapter(
                    CitiExperienceActivity.this, activity, items);

            getRetailersListView.setAdapter(getRetailersListAdapter);
            if (isAlreadyLoading) {
                getRetailersListView.setSelection(previousPosition);
            }

        }
        isAlreadyLoading = false;
    }

    public void setRetailerListAdapterForFilter(
            ArrayList<RetailerBo> listRetailerList, ArrayList<Item> items) {
        if (!listRetailerList.isEmpty()) {
            if (!listRetailerList.isEmpty()) {

                expRetailersListFilterAdapter = new AustinExpRetailersListFilterAdapter(
                        CitiExperienceActivity.this, items);

                getRetailersListView.setAdapter(expRetailersListFilterAdapter);
                if (isAlreadyLoading) {
                    getRetailersListView.setSelection(previousPosition);
                }
            }
        }
        isAlreadyLoading = false;
    }

    @Override
    public void onDestroy() {
        if (getRetailersListAdapter != null) {
            getRetailersListAdapter.customImageLoader.stopThread();
            getRetailersListView.setAdapter(null);
        }
        super.onDestroy();
        HubCityContext hubCityContext = (HubCityContext) getApplicationContext();
        hubCityContext.clearArrayAndAllValues(false);
        HubCityContext.isDoneClicked = false;
        cancelAsyncTask();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.events_view_more_button:
                viewMoreBtn();
                break;
            default:
                break;
        }

    }

    protected void viewMoreBtn() {
        enableBottomButton = false;
        isViewMore = true;
        getRetailersListView.removeFooterView(moreResultsView);
        getRetailersListView.addFooterView(moreResultsView);
        moreResultsView.setVisibility(View.VISIBLE);
        // Added to disable toggle click
        CommonConstants.disableClick = true;
        callRetailerAsyncTask();

    }

    @Override
    protected void onPause() {
        super.onPause();
        isRefresh = true;
    }

    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<>();

        if (filterFlag) {

            values.put("filterFlag", String.valueOf(filterFlag));

            values.put("Class", "filter");

            values.put(CommonConstants.TAG_FILTERNAME, filterName);

        } else {
            values.put("Class", "Citi Exp");
        }

        values.put("srchKey", searchKey);

        values.put(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);

        values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mBottomId);

        values.put(CommonConstants.FILTERCOUNT_EXTRA, retAffCount);

        values.put(Constants.CITI_EXPID_INTENT_EXTRA, citiExpId);

        values.put(CommonConstants.TAG_FILTERID, retAffId);

        values.put("latitude", gpsLatitude);

        values.put("longitude", gpsLongitude);

        return values;

    }

    @SuppressWarnings("deprecation")
    private void loadRetailers() {

        preferredRadius = "20";
        lastVisitedRecord = "0";

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {

            gpsEnabled = true;
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                gpsLatitude = String.valueOf(locNew.getLatitude());
                gpsLongitude = String.valueOf(locNew.getLongitude());

            } else {
                // N/W Tower Info Start
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    gpsLatitude = String.valueOf(locNew.getLatitude());
                    gpsLongitude = String.valueOf(locNew.getLongitude());
                } else {
                    gpsLatitude = CommonConstants.LATITUDE;
                    gpsLongitude = CommonConstants.LONGITUDE;
                }
                // N/W Tower Info end

            }
            callRetailerAsyncTask();


        } else {
            gpsEnabled = false;
            callRetailerAsyncTask();
        }

    }

    private void callRetailerAsyncTask() {

        previousPosition = items.size();
        if (isFirst) {
            retailerAsyncTask = new RetailerListsAsyncTask();
            retailerAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            isFirst = false;
            return;
        }

        if (retailerAsyncTask != null) {
            if (!retailerAsyncTask.isCancelled()) {
                retailerAsyncTask.cancel(true);
            }

            retailerAsyncTask = null;
        }

        retailerAsyncTask = new RetailerListsAsyncTask();
        retailerAsyncTask.execute();

    }

    private void cancelAsyncTask() {
        if (retailerAsyncTask != null && !retailerAsyncTask.isCancelled()) {
            retailerAsyncTask.cancel(true);
        }

        retailerAsyncTask = null;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            finishAction();
        }

        if (resultCode == 20002) {
            isRefresh = true;

        } else if (resultCode == 30001 || resultCode == 2) {
            isRefresh = false;
        }

        if (resultCode == 3000) {
            finishAction();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void finishAction() {
        cancelAsyncTask();
        hubCiti.clearSavedValues();
        finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAction();
    }

    @Override
    public void onStop() {
        super.onStop();
        getRetailersListView.setVisibility(View.GONE);// fixes weird bug with
        // header view
    }

    @Override
    public void onStart() {
        if (getRetailersListView.getVisibility() == View.GONE) {
            getRetailersListView.setVisibility(View.VISIBLE);
        }
        super.onStart();
    }

}