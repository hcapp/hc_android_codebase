package com.hubcity.android.screens;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.hubcity.android.commonUtil.Constants;

public class AlertDialogActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Push Notification");
        //This is to check whether the user is in Login screen or not so that we can show proper alert according to that
        if (!Constants.showLoginMsg)
            builder.setMessage("Please SignUp to view the Push Notification");
        else
            builder.setMessage("Please login to view the Push Notification");
        builder.setPositiveButton("View",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Do somethingprivate void startSignUpActivity() {
                        if (!Constants.showLoginMsg) {
                            Intent csintent = new Intent(AlertDialogActivity.this,
                                    RegistrationActivity.class);
                            csintent.putExtra(
                                    Constants.PUSH_NOTIFY_MSG,
                                    AlertDialogActivity.this.getIntent()
                                            .getStringExtra(
                                                    Constants.PUSH_NOTIFY_MSG));
                            startActivity(csintent);
                        }
                        dialog.dismiss();
                        finish();
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        dialog.dismiss();
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
