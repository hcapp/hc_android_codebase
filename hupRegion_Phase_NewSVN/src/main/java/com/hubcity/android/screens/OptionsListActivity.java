package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.businessObjects.SavedFilterData;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.android.FilterAndSortScreen;
import com.scansee.hubregion.R;

public class OptionsListActivity extends CustomTitleBar {

	private ArrayList<RetailerBo> mRetailerList;
	protected String retAffCount;
	String filterName;
	Boolean filterFlag = false;
	OptionsListAdapter optionsListAdapter;
	protected String mItemId, mBottomId;
	protected String citiExpId;
	String retAffId = null;
	protected static Button btnClosePopup, btnRedeem;
	Button btnTwitterShare, btnSMSShare, btnEmailShare;
	Button btnFacebookShare;
	SavedFilterData savedFilterData;
	String className = "";
	Bundle SavedData;
	boolean isShowMap;

	HashMap<String, String> moduleValues = new HashMap<>();

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(null);
		setContentView(R.layout.optionlist_activity);
		try {
			mRetailerList = (getIntent().getExtras()
					.getParcelableArrayList("mRetailerList"));
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		this.title.setText("Map/Sort");

		Intent intent = getIntent();
		try {
			SavedData = intent.getExtras();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (intent.hasExtra("values")) {
			moduleValues = (HashMap<String, String>) intent
					.getSerializableExtra("values");
		}

		if (moduleValues != null && !moduleValues.isEmpty()) {
			className = moduleValues.get("Class");

		} else {

			if (getIntent().hasExtra("Class")) {
				className = getIntent().getExtras().getString("Class");
			}
		}

		if ("Citi Exp".equalsIgnoreCase(className)) {
			if (moduleValues.containsKey("filterFlag")) {
				filterFlag = Boolean.parseBoolean(moduleValues
						.get("filterFlag"));
			}

			if (moduleValues.containsKey(CommonConstants.FILTERCOUNT_EXTRA)) {
				retAffCount = moduleValues
						.get(CommonConstants.FILTERCOUNT_EXTRA);
			}

			if (moduleValues.containsKey("retAffName")) {
				filterName = moduleValues.get("retAffName");
			}

			if (moduleValues.containsKey(Constants.MENU_ITEM_ID_INTENT_EXTRA)
					&& moduleValues.get(Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
				mItemId = moduleValues.get(Constants.MENU_ITEM_ID_INTENT_EXTRA);
			}

			if (moduleValues.containsKey(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
					&& moduleValues.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
				mBottomId = moduleValues
						.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
			}

			if (moduleValues.containsKey(Constants.CITI_EXPID_INTENT_EXTRA)) {
				citiExpId = moduleValues.get(Constants.CITI_EXPID_INTENT_EXTRA);
			}

			retAffId = moduleValues.get(CommonConstants.FILTERID_EXTRA);

		}

		ListView getRetailersListView = (ListView) findViewById(R.id.lst_this_location_get_retailers);

		ArrayList<String> thislocationList = new ArrayList<>();
		thislocationList.add("  Show Map");
		thislocationList.add("  Sort");
		optionsListAdapter = new OptionsListAdapter(OptionsListActivity.this,
				thislocationList);

		getRetailersListView.setAdapter(optionsListAdapter);

		backImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isShowMap) {
					isShowMap = false;
				}

				finish();
			}
		});

		getRetailersListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
									long arg3) {

				if ("Show Map".equalsIgnoreCase(arg0.getItemAtPosition(arg2)
						.toString().trim())) {
					if (mRetailerList != null && mRetailerList.size() > 0) {
						isShowMap = true;

						Intent showMap = new Intent(getApplicationContext(),
								ShowMapMultipleLocations.class);

						if (filterFlag) {
							showMap.putExtra(CommonConstants.TAG_FILTERNAME,
									filterName);
						}

						showMap.putParcelableArrayListExtra("mRetailerList",
								mRetailerList);
						startActivity(showMap);
					} else {
						Toast.makeText(
								OptionsListActivity.this,
								getResources().getString(
										R.string.alert_no_retailer),
								Toast.LENGTH_SHORT).show();
					}

				} else if ("Sort".equalsIgnoreCase(arg0.getItemAtPosition(arg2)
						.toString().trim())) {

					HashMap<String, String> values = new HashMap<>();

					Intent sortbylist = new Intent();

					if ("Find".equals(className)) {

						values.put("catName", moduleValues.get("selectedCat"));

						values.put("subCatId", moduleValues.get("subCatId"));

						values.put("mItemId", moduleValues.get("mItemId"));
						values.put("mLinkId", moduleValues.get("mLinkId"));
						values.put(
								Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
								moduleValues
										.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));

						values.put("sortChoice", moduleValues.get("sortChoice"));
						values.put("catId", moduleValues.get("selectedCatId"));

						values.put("typeName", moduleValues.get("typeName"));

						values.put("latitude", moduleValues.get("latitude"));

						values.put("longitude", moduleValues.get("longitude"));

						if (moduleValues.containsKey("srchKey")) {
							values.put("srchKey", moduleValues.get("srchKey"));
						}

					} else if ("FindSingleCat".equals(className)) {

						values.put("sortChoice", moduleValues.get("sortChoice"));

						values.put("mLinkId", moduleValues.get("mLinkId"));

						values.put("catName", moduleValues.get("catName"));

						values.put("catId", moduleValues.get("catId"));

						values.put("mItemId", moduleValues.get("mItemId"));

						values.put(
								Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
								moduleValues
										.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));

						values.put("latitude", moduleValues.get("latitude"));

						values.put("longitude", moduleValues.get("longitude"));

						values.put("srchKey", moduleValues.get("srchKey"));

						if (moduleValues.containsKey("subCatId")) {
							values.put("subCatId", moduleValues.get("subCatId"));
						}

					} else if ("Find All".equals(className)) {

						if (moduleValues.containsKey("srchKey")) {
							values.put("srchKey", moduleValues.get("srchKey"));
						}

						values.put("mItemId", moduleValues.get("mItemId"));

						values.put(
								Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
								moduleValues
										.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));

						if (moduleValues.containsKey("catName")) {
							values.put("catName", moduleValues.get("catName"));
						}

						if (moduleValues.containsKey("selectedCatId")) {
							values.put("selectedCatId",
									moduleValues.get("selectedCatId"));
						}
						values.put("catId", moduleValues.get("selectedCatId"));
						values.put("mLinkId", moduleValues.get("mLinkId"));
						values.put("latitude", moduleValues.get("latitude"));

						values.put("longitude", moduleValues.get("longitude"));

						values.put("subCatId", moduleValues.get("subCatId"));

					} else if ("Citi Exp".equals(className)) {
						values.put(Constants.CITI_EXPID_INTENT_EXTRA, citiExpId);

						values.put("latitude", moduleValues.get("latitude"));
						try {
							values.put("mLinkId", moduleValues.get("mLinkId"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						values.put("longitude", moduleValues.get("longitude"));

						if (moduleValues.containsKey("srchKey")) {
							values.put("srchKey", moduleValues.get("srchKey"));
						}

					} else if ("filter".equals(className)) {

						values.put("latitude", moduleValues.get("latitude"));

						values.put("longitude", moduleValues.get("longitude"));
						try {
							values.put("mLinkId", moduleValues.get("mLinkId"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						values.put(CommonConstants.TAG_FILTERID,
								moduleValues.get(CommonConstants.TAG_FILTERID));

						if (moduleValues.containsKey("srchKey")) {
							values.put("srchKey", moduleValues.get("srchKey"));
						}

					}

					if ("Whats NearBy".equalsIgnoreCase(className)) {
						sortbylist.putExtra("Class", className);
						sortbylist.setClass(getApplicationContext(),
								OptionSortByList.class);
					} else {
						values.put("Class", className);
						sortbylist.putExtra("values", values);
						sortbylist.setClass(getApplicationContext(),
								FilterAndSortScreen.class);
					}
					sortbylist.putExtras(SavedData);
					startActivityForResult(sortbylist, 01);
				}

			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 2) {
			setResult(30001, data);
			finish();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		if (isShowMap) {
			isShowMap = false;
		}

		finish();
	}

}
