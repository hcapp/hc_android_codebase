package com.hubcity.android.screens;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.FaqDetails;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

/**
 * This class displays the List of FAQ categories with a drop down for each
 * category displaying the questions associate with them
 *
 * @author rekha_p
 */
public class FaqScreen extends CustomTitleBar {

    protected static boolean isFaqAnswerScreen = false;
    private ProgressDialog mDialog;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;

    ArrayList<HashMap<String, String>> arrayListCat = new ArrayList<>();
    ArrayList<HashMap<String, String>> faqQuestionList = new ArrayList<>();
    ArrayList<HashMap<String, String>> arrayListQues;

    HashMap<String, ArrayList<HashMap<String, String>>> categoryData = new HashMap<>();
    HashMap<String, ArrayList<HashMap<String, String>>> listDataChild;
    HashMap<String, String> category, questions;
    List<String> listDataHeader;
    List<String> questList;

    UrlRequestParams urlRequestParams = null;
    ServerConnections mServerConnections = null;

    Activity activity;

    BottomButtons bb = null;
    LinearLayout linearLayout = null;
    boolean hasBottomBtns = false;
    boolean enableBottomButton = true;
    EditText faqSearchEditText;
    boolean isExpand;

    String bottomBtnId, mItemId;
    String mainMenuId = "", faqSearchText = "";

    int position = 0;
    boolean isSettings = false;

    boolean isFirst;

    CategoryListTask catListAsyncTask;
    QuestionsListTask questListAsyncTask;
    GetMainMenuId mainMenuAsyncTask;
    private CustomNavigation customNaviagation;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_screen);

        try {
            CommonConstants.hamburgerIsFirst = true;

            activity = FaqScreen.this;
            isFirst = true;

            linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);

            try {

                // Initiating Bottom button class
                bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
                // Add screen name when needed
                bb.setActivityInfo(activity, "");
            } catch (Exception e) {

            }

            title.setSingleLine(true);
            title.setText("FAQs");
            title.setTextSize(14);

            bottomBtnId = getIntent().getExtras().getString("bottomBtnId");
            mItemId = getIntent().getExtras().getString("mItemId");
            isSettings = getIntent().getExtras().getBoolean("isSettings");

            leftTitleImage.setVisibility(View.GONE);

            urlRequestParams = new UrlRequestParams();
            mServerConnections = new ServerConnections();

            listDataHeader = new ArrayList<>();

            questList = new ArrayList<>();
            arrayListQues = new ArrayList<>();

            // get the listview
            expListView = (ExpandableListView) findViewById(R.id.lvExp);

            // Action on click of each category
            expListView.setOnGroupClickListener(new OnGroupClickListener() {

                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {

                    position = groupPosition;
                    clearData();
                    listDataChild.put(listDataHeader.get(groupPosition),
                            faqQuestionList);
                    if (!parent.isGroupExpanded(groupPosition)) {
                        callQuestListAsyncTask(
                                arrayListCat.get(groupPosition).get("categoryName"),
                                arrayListCat.get(groupPosition).get("categoryId"));
                    }
                    // try {
                    // listDataChild.remove(listDataHeader.get(groupPosition));
                    // } catch (Exception e) {
                    // e.printStackTrace();
                    // }
                    return false;
                }
            });

            // Action on click of each question in the category
            expListView.setOnChildClickListener(new OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                                            int groupPosition, int childPosition, long id) {

                    FaqDetails faqDetails = (FaqDetails) v.getTag();
                    // Yet to be got from server
                    String faqListId = "";

                    ArrayList<HashMap<String, String>> catData = categoryData
                            .get(faqDetails.getCatName());

                    String questions[] = new String[catData.size()];
                    String faqIDs[] = new String[catData.size()];

                    if (!catData.isEmpty()) {
                        for (int i = 0; i < catData.size(); i++) {

                            questions[i] = catData.get(i).get("question");
                            faqIDs[i] = catData.get(i).get("faqId");

                        }
                    }

                    isFaqAnswerScreen = false;
                    final FetchAnswersTask fetchAnswers = new FetchAnswersTask(
                            activity, faqDetails.getQuestion(), faqDetails
                            .getFaqId(), faqDetails.getCatName(),
                            questions, faqIDs, faqListId);
                    fetchAnswers.execute();

                    return false;
                }
            });

            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTasks();
                    finish();
                }
            });
            searchFAQ();
            // preparing list data
            callCatListAsyncTask();

            //user for hamburger in modules
            drawerIcon.setVisibility(View.VISIBLE);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void searchFAQ() {
        faqSearchEditText = (EditText) findViewById(R.id.faq_edit_text);
        faqSearchEditText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        faqSearchEditText
                .setOnEditorActionListener(new OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            faqSearchText = faqSearchEditText.getText()
                                    .toString();
                            callCatListAsyncTask();
                        }
                        hideKeyboardItem();
                        return true;
                    }
                });

        (findViewById(R.id.faq_cancel))
                .setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (faqSearchEditText != null) {
                            // faqSearchEditText.setText("");
                            hideKeyboardItem();
                        }

                    }
                });
        hideKeyboardItem();
    }

    private void hideKeyboardItem() {
        InputMethodManager imm = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(faqSearchEditText.getWindowToken(), 0);
    }

    /**
     * This AsyncTask fetched Categories from the server
     *
     * @author rekha_p
     */
    public class CategoryListTask extends AsyncTask<Void, Void, Boolean> {

        private JSONObject jsonResponse = null;
        String responseText = null;
        String responseCode = null;
        boolean nextPage = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = ProgressDialog.show(FaqScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            Boolean result = false;

            try {

//				String faq_url = Properties.url_local_server
//						+ Properties.hubciti_version + "firstuse/faqcategory?";
                String faq_url = Properties.url_local_server
                        + Properties.hubciti_version + "firstuse/faqcategoryjson?";
                String urlParameters = faq_url + "hubCitiId="
                        + UrlRequestParams.getHubCityId() + "&lowerLimit=0"
                        + "&userId=" + UrlRequestParams.getUid()
                        + "&platform=android" + "&searchKey="
                        + URLEncoder.encode(faqSearchText, "UTF-8");

                jsonResponse = mServerConnections.getJSONFromGetRequest(
                        urlParameters);

                if (jsonResponse != null) {

                    // For BottomButton
                    if (jsonResponse.has("bottomBtnList")) {

                        hasBottomBtns = true;

                        try {
                            ArrayList<BottomButtonBO> bottomButtonList = bb
                                    .parseForBottomButton(jsonResponse);
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(bottomButtonList);

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                    } else {
                        hasBottomBtns = false;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            try {
                mDialog.dismiss();

                try {
                    responseCode = this.jsonResponse.getString("responseCode");
                    responseText = this.jsonResponse.getString("responseText");

                    if ("10000".equals(responseCode)) {

                        result = true;
                        expListView.setVisibility(View.VISIBLE);
                    } else {
                        displayAlert(responseText);
                        expListView.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (result) {
                    prepareListData(jsonResponse);

                    if (!isSettings) {
                        callMainMenuAsyncTask();
                    }

                }
                if (hasBottomBtns && enableBottomButton) {

                    bb.createbottomButtontTab(linearLayout, false);
                    enableBottomButton = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

	/*
     * Preparing the list data
	 */

    private void prepareListData(JSONObject jsonObject) {

        listDataChild = new HashMap<>();
        listDataHeader = new ArrayList<>();
        arrayListCat = new ArrayList<>();

        try {

            if (jsonObject.has("faqCatList")) {
                JSONArray categoryList = jsonObject.getJSONArray("faqCatList");
                for (int i = 0; i < categoryList.length(); i++) {
                    category = new HashMap<>();
                    JSONObject innerObj = categoryList.getJSONObject(i);

                    try {
                        category.put("categoryName", URLDecoder.decode(
                                innerObj.getString("categoryName"), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    category.put("categoryId", innerObj.getString("categoryId"));

                    arrayListCat.add(category);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (arrayListCat != null && !arrayListCat.isEmpty()) {
            // Adding child data
            for (int i = 0; i < arrayListCat.size(); i++) {

                listDataHeader.add(arrayListCat.get(i).get("categoryName"));
                listDataChild.put(listDataHeader.get(i), faqQuestionList);

                if (arrayListCat.size() == 1) {
                    callQuestListAsyncTask(
                            arrayListCat.get(0).get("categoryName"),
                            arrayListCat.get(0).get("categoryId"));
                    position = 0;
                    isExpand = true;
                    // expListView.expandGroup(0);

                }
            }
        } else {

            displayAlert("No Categories Found");
        }

        listAdapter = new ExpandableListAdapter(this, listDataHeader,
                listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        listAdapter.notifyDataSetChanged();

    }

    /**
     * Displays an Alert Dialog
     *
     * @param message message to be displayed in the dialog
     */
    private void displayAlert(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                FaqScreen.this);

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        cancelAsyncTasks();
                    }
                });
        alertDialogBuilder.show();
    }

    /**
     * This AsyncTask fetches Questions for each Category
     *
     * @author rekha_p
     */
    public class QuestionsListTask extends AsyncTask<Void, Void, Void> {

        private JSONObject jsonResponse = null;
        String responseCode = null;
        String responseText = null;
        String categoryName, categoryId;
        boolean nextPageFlag = false;

        public QuestionsListTask(String catName, String catId) {
            categoryName = catName;
            categoryId = catId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = ProgressDialog.show(FaqScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String urlParameters = urlRequestParams.createFaqDisplayParam(
                    categoryId, faqSearchText, mainMenuId);
            String faq_display_url = Properties.url_local_server
                    + Properties.hubciti_version + "firstuse/faqdisplay";
            jsonResponse = mServerConnections.getUrlPostResponse(
                    faq_display_url, urlParameters, true);

            if (jsonResponse != null) {
                try {

                    if (jsonResponse.has("FAQDetails")) {
                        jsonResponse = jsonResponse.getJSONObject("FAQDetails");

                    } else if (jsonResponse.has("response")) {
                        jsonResponse = jsonResponse.getJSONObject("response");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                mDialog.dismiss();

                try {
                    responseCode = this.jsonResponse.getString("responseCode");
                    responseText = this.jsonResponse.getString("responseText");

                    if ("10000".equals(responseCode)) {
                        //categoryData = new HashMap<String, ArrayList<HashMap<String, String>>>();
                        arrayListQues = new ArrayList<>();
                        faqQuestionList = new ArrayList<>();
                        Object obj = jsonResponse.get("FAQDetails");

                        if (obj instanceof JSONArray) {
                            JSONArray questionsList = jsonResponse
                                    .getJSONArray("FAQDetails");
                            for (int i = 0; i < questionsList.length(); i++) {
                                questions = new HashMap<>();
                                JSONObject innerObj = questionsList
                                        .getJSONObject(i);

                                if (innerObj.has("faqId")) {
                                    questions.put("faqId",
                                            innerObj.getString("faqId"));
                                }

                                if (innerObj.has("question")) {
                                    questions.put("question",
                                            URLDecoder.decode(
                                                    innerObj.getString("question"),
                                                    "UTF-8"));
                                }

                                if (innerObj.has("answer")) {
                                    questions.put("answer",
                                            innerObj.getString("answer"));
                                }

                                arrayListQues.add(questions);

                            }
                        } else if (obj instanceof JSONObject) {
                            questions = new HashMap<>();
                            JSONObject questionsList = jsonResponse
                                    .getJSONObject("FAQDetails");

                            if (questionsList.has("faqId")) {
                                questions.put("faqId",
                                        questionsList.getString("faqId"));
                            }

                            if (questionsList.has("question")) {
                                questions.put("question", URLDecoder.decode(
                                        questionsList.getString("question"),
                                        "UTF-8"));
                            }

                            if (questionsList.has("answer")) {
                                questions.put("answer",
                                        questionsList.getString("answer"));
                            }

                            arrayListQues.add(questions);

                        }

                        if (arrayListQues != null && !arrayListQues.isEmpty()) {
                            for (int i = 0; i < arrayListQues.size(); i++) {

                                HashMap<String, String> question = new HashMap<>();

                                question.put("faqId",
                                        arrayListQues.get(i).get("faqId"));
                                question.put("question",
                                        arrayListQues.get(i).get("question"));
                                try {
                                    question.put("category",
                                            listDataHeader.get(position));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                faqQuestionList.add(question);

                            }

                            listDataChild.put(listDataHeader.get(position),
                                    faqQuestionList);

                            if (!categoryData.containsKey(listDataHeader
                                    .get(position))) {
                                categoryData.put(listDataHeader.get(position),
                                        faqQuestionList);
                            }

                        }

                    } else {
                        displayAlert(responseText);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                listAdapter.notifyDataSetChanged();

                if (isExpand) {
                    expListView.expandGroup(0);
                    isExpand = false;
                }
                if (position != 0 && position == listDataHeader.size() - 1)
                    expListView.smoothScrollToPosition(expListView
                            .getLastVisiblePosition() + 1);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * This AsyncTask fetches the MainMenuId on passing bottomButtonId or
     * mItemId depending on the screen
     *
     * @author rekha_p
     */
    public class GetMainMenuId extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;

        @Override
        protected String doInBackground(String... params) {

            String result = "false";
            String id = "";
            try {
                UrlRequestParams mUrlRequestParams = new UrlRequestParams();
                ServerConnections mServerConnections = new ServerConnections();

                if (bottomBtnId != null && !"".equals(bottomBtnId)) {
                    id = bottomBtnId;
                } else if (mItemId != null && !"".equals(mItemId)) {
                    id = mItemId;
                }
                String get_mainmenu_id = Properties.url_local_server
                        + Properties.hubciti_version
                        + "firstuse/utgetmainmenuid";
                String urlParameters = mUrlRequestParams.getMainMenuId(id);
                jsonObject = mServerConnections.getUrlPostResponse(
                        get_mainmenu_id, urlParameters, true);

                if (jsonObject != null) {
                    jsonObject = jsonObject
                            .getJSONObject(CommonConstants.TAG_RESULTSET_MAINMENUID);

                    mainMenuId = jsonObject
                            .getString(CommonConstants.TAG_MAINMENUID);
                    Constants.saveMainMenuId(mainMenuId);
                    result = "true";

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

    }

    /**
     * Clears the List Items before adding new data
     */
    private void clearData() {
        if (questList != null) {
            questList = null;
        }

        if (arrayListQues != null) {
            arrayListQues = null;
        }

        if (faqQuestionList != null) {
            faqQuestionList = null;
        }

        faqQuestionList = new ArrayList<>();
        questList = new ArrayList<>();
        arrayListQues = new ArrayList<>();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            cancelAsyncTasks();
            finish();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }

        if (bb != null) {
            bb.setActivityInfo(activity, "FAQ");
        }
    }

    private void callCatListAsyncTask() {
        if (isFirst) {
            catListAsyncTask = new CategoryListTask();
            catListAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            isFirst = false;
            return;
        }

        if (catListAsyncTask != null) {
            if (!catListAsyncTask.isCancelled()) {
                catListAsyncTask.cancel(true);
            }

            catListAsyncTask = null;
        }

        catListAsyncTask = new CategoryListTask();
        catListAsyncTask.execute();

    }

    private void callQuestListAsyncTask(String catName, String catId) {

        if (questListAsyncTask != null) {
            if (!questListAsyncTask.isCancelled()) {
                questListAsyncTask.cancel(true);
            }

            questListAsyncTask = null;
        }

        questListAsyncTask = new QuestionsListTask(catName, catId);
        questListAsyncTask.execute();

    }

    private void callMainMenuAsyncTask() {

        if (mainMenuAsyncTask != null) {
            if (!mainMenuAsyncTask.isCancelled()) {
                mainMenuAsyncTask.cancel(true);
            }

            mainMenuAsyncTask = null;
        }

        mainMenuAsyncTask = new GetMainMenuId();
        mainMenuAsyncTask.execute();

    }

    private void cancelAsyncTasks() {

        if (catListAsyncTask != null && !catListAsyncTask.isCancelled()) {
            catListAsyncTask.cancel(true);
        }

        catListAsyncTask = null;

        if (questListAsyncTask != null && !questListAsyncTask.isCancelled()) {
            questListAsyncTask.cancel(true);
        }

        questListAsyncTask = null;

        if (mainMenuAsyncTask != null && !mainMenuAsyncTask.isCancelled()) {
            mainMenuAsyncTask.cancel(true);
        }

        mainMenuAsyncTask = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        cancelAsyncTasks();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelAsyncTasks();
    }

}