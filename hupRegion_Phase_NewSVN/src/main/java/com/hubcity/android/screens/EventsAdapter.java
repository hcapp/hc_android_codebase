package com.hubcity.android.screens;

import java.util.ArrayList;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.EventsEntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EventsAdapter extends ArrayAdapter<Item> {

	Activity activity;
	private ArrayList<Item> items;
	private LayoutInflater vi;

	protected CustomImageLoader customImageLoader;

	public EventsAdapter(Context context, ArrayList<Item> items,
			Activity activity) {
		super(context, 0, items);

		this.activity = activity;
		this.items = items;
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		customImageLoader = new CustomImageLoader(context.getApplicationContext(), false);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder viewHolder = null;

		final Item i = items.get(position);

		/***
		 * If the list item reaches to its last position then it will be called
		 * for next pagination
		 ***/
		if (position == items.size() - 1) {
			if (!((EventsListDisplay) activity).isAlreadyLoading) {
				if (((EventsListDisplay) activity).nextPage) {
					((EventsListDisplay) activity).loadMoreData();
				}
			}
		}

		if (i != null) {
			if (i.isSection()) {
				SectionItem si = (SectionItem) i;

				v = vi.inflate(R.layout.events_list_item_section, parent,false);

				v.setOnClickListener(null);
				v.setOnLongClickListener(null);
				v.setLongClickable(false);

				final TextView sectionView = (TextView) v
						.findViewById(R.id.events_list_item_section_text);
				sectionView.setGravity(Gravity.LEFT);
				sectionView.setText(si.getTitle());

			} else {

				viewHolder = new ViewHolder();

				EventsEntryItem ei = (EventsEntryItem) i;

				v = vi.inflate(R.layout.events_list_item_entry, parent,false);
				viewHolder.title = (TextView) v
						.findViewById(R.id.events_list_item_entry_title);
				viewHolder.subtitle = (TextView) v
						.findViewById(R.id.events_list_item_entry_summary);
				viewHolder.image = (ImageView) v
						.findViewById(R.id.events_list_item_main_image);

				if (viewHolder.title != null) {
					viewHolder.title.setText(ei.eventName);
				}
				if (viewHolder.subtitle != null) {

					viewHolder.subtitle.setText(ei.startDate + "  "
							+ ei.startTime + "  " + ei.distance);
				}
				if (viewHolder.image != null) {

					if (ei.imagePath != null) {
						viewHolder.image.setTag(ei.imagePath);
						customImageLoader.displayImage(ei.imagePath, activity,
								viewHolder.image);
					} else {
						viewHolder.image.setImageBitmap(null);
					}
				}

			}
		}

		return v;

	}

	public static class ViewHolder {
		protected ImageView image;
		protected TextView title;
		protected TextView subtitle;

	}

}