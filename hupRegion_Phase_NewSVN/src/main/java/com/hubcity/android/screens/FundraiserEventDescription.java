package com.hubcity.android.screens;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.net.MailTo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class FundraiserEventDescription extends CustomTitleBar implements
        OnClickListener {

    public ArrayList<HashMap<String, String>> popUplistData;
    ProgressDialog progDialog;
    String responseText = "Connection time out";
    String responseCode = "";
    private static String returnValue = "sucessfull";

    JSONObject jsonObject;

    String mFundId = "";
    String mFundListId = "";

    HashMap<String, String> fundDetails = new HashMap<>();

    TextView eventStartEndDate;
    TextView shortDesc;
    TextView fundraiserName;
    TextView fundraiserGoal;
    TextView currentLevel;

    ImageView fundRaiserImg;

    WebView longDescWebView;
    String longDesc = "";

    ImageView moreInfoImgView, eventInfoImgView;

    LinearLayout fund_description_layout;
    LinearLayout shortDescLayout;
    FrameLayout moreInfoLayout, eventInfoLayout;
    ImageButton moreInfoButton, eventInfoButton, locButton;

    Activity activity;
    BottomButtons bb = null;

    boolean enableBottomButton = true;
    int eventFlag = 0, moreInfoFlag = 0;

    String htmlcode = "";
    String mItemId = "";
    String mBottomId = "";
    String mLinkId = "";
    String fundId = "";
    String mainMenuId = "";

    FundraiserEventDetailsAsyncTask fundDetailsTask;
    ImageLoaderAsync imageLoaderTask;

    String isAppSiteFlag = "";

    LinearLayout purchaseProdLayout;
    TextView purchaseProdValue;
    String purchaseProdText;

    ShareInformation shareInfo;
    boolean isShareTaskCalled;

    PopupWindow pwindo;
    Button btnCloseLocPopup;
    HashMap<String, String> locationDetails = new HashMap<>();
    LocationManager locationManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fundraiser_event_description);

        try {
            isAppSiteFlag = "";
            activity = FundraiserEventDescription.this;
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            title.setText("Fundraiser");

            mItemId = getIntent().getExtras().getString(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA);
            mBottomId = getIntent().getExtras().getString(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            mLinkId = getIntent().getExtras().getString("mLinkId");
            fundId = getIntent().getExtras().getString("fundID");
            mainMenuId = getIntent().getExtras().getString("mainMenuId");

            moreInfoLayout = (FrameLayout) findViewById(R.id.more_info_layout);
            eventInfoLayout = (FrameLayout) findViewById(R.id.event_info_layout);

            moreInfoButton = (ImageButton) findViewById(R.id.more_info_Button);
            eventInfoButton = (ImageButton) findViewById(R.id.event_info_Button);
            locButton = (ImageButton) findViewById(R.id.loc_Button);

            moreInfoButton.setOnClickListener(this);
            eventInfoButton.setOnClickListener(this);
            locButton.setOnClickListener(this);

            moreInfoLayout.setVisibility(View.GONE);
            eventInfoLayout.setVisibility(View.GONE);

            moreInfoImgView = (ImageView) findViewById(R.id.more_info_left_icon);
            eventInfoImgView = (ImageView) findViewById(R.id.event_info_left_icon);
            fundRaiserImg = (ImageView) findViewById(R.id.fund_imageDescription);

            fundraiserName = (TextView) findViewById(R.id.fund_title);
            eventStartEndDate = (TextView) findViewById(R.id.fund_date);
            shortDesc = (TextView) findViewById(R.id.fund_textDescription_short);
            fundraiserGoal = (TextView) findViewById(R.id.fundraising_goal);
            currentLevel = (TextView) findViewById(R.id.currentlevel);
            fund_description_layout = (LinearLayout) findViewById(R.id.funds_Descriptor_linearLayout2);
            shortDescLayout = (LinearLayout) findViewById(R.id.short_desc_layout);
            fund_description_layout.setVisibility(View.INVISIBLE);

            purchaseProdLayout = (LinearLayout) findViewById(R.id.purchase_prod);
            purchaseProdValue = (TextView) findViewById(R.id.purchase_prod_value);
            purchaseProdValue.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    callBrowserIntent(purchaseProdValue.getText().toString().trim());

                }
            });

            longDescWebView = (WebView) findViewById(R.id.fund_textdesc_long);
            setWebViewSettings(longDescWebView, 16);

            mFundId = getIntent().getStringExtra("fundID");
            mFundListId = getIntent().getStringExtra("fundListId");

            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelFundDetailTask();
                    finish();

                }
            });

            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (GlobalConstants.isFromNewsTemplate) {
                        Intent intent = null;
                        switch (GlobalConstants.className) {
                            case Constants.COMBINATION:
                                intent = new Intent(FundraiserEventDescription.this, CombinationTemplate.class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(FundraiserEventDescription.this, ScrollingPageActivity.class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(FundraiserEventDescription.this, TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    } else {
                        setResult(Constants.FINISHVALUE);
                        startMenuActivity();
                    }
                }
            });

            loadFundDetails();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.more_info_Button:
                callBrowserIntent(fundDetails.get("moreInfoURL"));
                break;

            case R.id.event_info_Button:
                callEventsScreen();
                break;

            case R.id.loc_Button:
                new LocationAsyncTask().execute();
                break;

            default:
                break;
        }

    }

    private void callEventsScreen() {
        Intent intent = new Intent(FundraiserEventDescription.this,
                EventsListDisplay.class);
        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mBottomId);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra("mLinkId", mLinkId);
        intent.putExtra("fundId", fundId);
        intent.putExtra("mainMenuId", mainMenuId);
        startActivity(intent);

    }

    public class LocationAsyncTask extends AsyncTask<Void, Void, Void> {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        String resCode = "", resText = "";
        HashMap<String, String> gpsValues = new HashMap<>();

        protected void onPreExecute() {

            gpsValues = mUrlRequestParams.getGPSValues(activity);

            progDialog = new ProgressDialog(FundraiserEventDescription.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                locationDetails = new HashMap<>();

                isAppSiteFlag = "";
                String url_fundraiser_locations = Properties.url_local_server
                        + Properties.hubciti_version
                        + "alertevent/getfundloclist";

                String urlParameters = mUrlRequestParams
                        .createFundraiserLocations(fundId,
                                gpsValues.get("latitude"),
                                gpsValues.get("longitude"));

                JSONObject jsonResponse = mServerConnections
                        .getUrlPostResponse(url_fundraiser_locations,
                                urlParameters, true);

                if (jsonResponse != null) {

                    try {
                        if (jsonResponse.has("Fundraiser")) {
                            JSONObject jsonObject = jsonResponse
                                    .getJSONObject("Fundraiser");

                            resCode = "10000";

                            if (jsonObject.has("isAppSiteFlag")) {
                                isAppSiteFlag = jsonObject
                                        .getString("isAppSiteFlag");
                            }

                            JSONObject jsonFundObject = jsonObject
                                    .getJSONObject("fundraiserList")
                                    .getJSONObject("Fundraiser");

                            if (jsonFundObject.has("completeAddress")) {
                                locationDetails.put("completeAddress",
                                        jsonFundObject
                                                .getString("completeAddress"));
                            }

                            if (jsonFundObject.has("latitude")) {
                                locationDetails.put("latitude",
                                        jsonFundObject.getString("latitude"));
                            }

                            if (jsonFundObject.has("longitude")) {
                                locationDetails.put("longitude",
                                        jsonFundObject.getString("longitude"));
                            }

                            if (jsonFundObject.has("appSiteId")) {
                                locationDetails.put("appSiteId",
                                        jsonFundObject.getString("appSiteId"));
                            }

                            if (jsonFundObject.has("appSiteName")) {
                                locationDetails
                                        .put("appSiteName", jsonFundObject
                                                .getString("appSiteName"));
                            }

                            if (jsonFundObject.has("retailId")) {
                                locationDetails.put("retailId",
                                        jsonFundObject.getString("retailId"));
                            }

                            if (jsonFundObject.has("retailLocationId")) {
                                locationDetails.put("retailLocationId",
                                        jsonFundObject
                                                .getString("retailLocationId"));
                            }

                            if (jsonFundObject.has("imagePath")) {
                                locationDetails.put("imagePath",
                                        jsonFundObject.getString("imagePath"));
                            }

                            if (jsonFundObject.has("retailerName")) {
                                locationDetails.put("retailerName",
                                        jsonFundObject
                                                .getString("retailerName"));
                            }

                        } else {
                            JSONObject jsonRecordNotFound = jsonResponse
                                    .getJSONObject("response");
                            resText = jsonRecordNotFound
                                    .getString("responseText");
                            resCode = jsonRecordNotFound
                                    .getString("responseText");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                progDialog.dismiss();

                if ("10000".equals(resCode)) {
                    initiateLocationWindow();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initiateLocationWindow() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) FundraiserEventDescription.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater
                    .inflate(
                            R.layout.events_location_popup,
                            (ViewGroup) findViewById(R.id.events_location_popup_element));

            DisplayMetrics metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            layout.setBackgroundColor(Color.WHITE);

            ArrayList<HashMap<String, String>> listData = new ArrayList<>();
            listData.add(locationDetails);

            pwindo = new PopupWindow(layout, screenWidth - 50,
                    ViewGroup.LayoutParams.WRAP_CONTENT, true);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
                }
            });


            ListView mLocationList = (ListView) layout
                    .findViewById(R.id.events_location_listview);

            TextView txtHeader = (TextView) layout
                    .findViewById(R.id.events_location_popup_title);

            if ("".equals(isAppSiteFlag) || isAppSiteFlag == null) {
                txtHeader.setVisibility(View.GONE);
            } else {
                txtHeader.setVisibility(View.VISIBLE);
            }

            EventsAppSiteArrayAdapter adapter = null;
            if (popUplistData == null) {
                popUplistData = listData;
            }

            adapter = new EventsAppSiteArrayAdapter(
                    FundraiserEventDescription.this, popUplistData,
                    isAppSiteFlag, true);

            mLocationList.setAdapter(adapter);
            btnCloseLocPopup = (Button) layout
                    .findViewById(R.id.btn_location_close_popup);
            btnCloseLocPopup.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });

            mLocationList.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int pos, long arg3) {

                    String lat = null;
                    String lon = null;

                    if ("".equals(isAppSiteFlag) || isAppSiteFlag == null) {
                        if (locationDetails.get("latitude") != null) {
                            lat = locationDetails.get("latitude");
                        }

                        if (locationDetails.get("longitude") != null) {
                            lon = locationDetails.get("longitude");
                        }

                        MapDirections mapDirections = new MapDirections(
                                locationManager, activity, locationDetails
                                .get("completeAddress"), lat, lon, "");
                        mapDirections.getMapDirections();

                        pwindo.dismiss();

                    } else if ("0".equals(isAppSiteFlag)
                            || "1".equals(isAppSiteFlag)) {
                        // @Rekha: If isAppSiteFlag is "1" pull up Appsite
                        Intent intent = new Intent(
                                FundraiserEventDescription.this,
                                RetailerActivity.class);

                        if (null != locationDetails.get("retailId")) {
                            intent.putExtra(CommonConstants.TAG_RETAIL_ID,
                                    locationDetails.get("retailId"));
                        }

                        if (null != locationDetails
                                .get(CommonConstants.TAG_RETAIL_LIST_ID)) {
                            intent.putExtra(
                                    CommonConstants.TAG_RETAIL_LIST_ID,
                                    locationDetails
                                            .get(CommonConstants.TAG_RETAIL_LIST_ID));
                        }
                        if (null != locationDetails.get("retailLocationId")) {
                            intent.putExtra(Constants.TAG_RETAILE_LOCATIONID,
                                    locationDetails.get("retailLocationId"));
                        }
                        if (null != locationDetails.get("appSiteId")) {
                            intent.putExtra("appSiteId",
                                    locationDetails.get("appSiteId"));
                        }
                        if (null != locationDetails.get("appSiteName")) {
                            intent.putExtra(Constants.TAG_APPSITE_NAME,
                                    locationDetails.get("appSiteName"));

                        } else if (null != locationDetails.get("retailerName")) {
                            intent.putExtra(Constants.TAG_APPSITE_NAME,
                                    locationDetails.get("retailerName"));

                        }

                        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                                mItemId);

                        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                                mBottomId);

                        startActivity(intent);
                        pwindo.dismiss();

                    }

                    pwindo.dismiss();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class FundraiserEventDetailsAsyncTask extends
            AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        protected void onPreExecute() {
            progDialog = new ProgressDialog(FundraiserEventDescription.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if ("sucessfull".equals(returnValue)) {

                    if (1 == moreInfoFlag) {
                        moreInfoLayout.setVisibility(View.VISIBLE);
                    }

                    if (1 == eventFlag) {
                        eventInfoLayout.setVisibility(View.VISIBLE);
                    }

                    if (null != fundDetails.get("title")) {
                        fundraiserName.setText(fundDetails.get("title"));
                    } else {
                        fundraiserName.setText("");
                    }

                    if (null != fundDetails.get("imgPath")
                            && !"".equals(fundDetails.get("imgPath"))) {
                        imageLoaderTask = new ImageLoaderAsync(fundRaiserImg);
                        imageLoaderTask.execute(fundDetails.get("imgPath"));
                    } else {
                        fundRaiserImg.setVisibility(View.GONE);
                    }

                    if (null != fundDetails.get("sDescription")
                            && !"".equals(fundDetails.get("sDescription"))) {
                        shortDescLayout.setVisibility(View.VISIBLE);
                        shortDesc.setText(fundDetails.get("sDescription"));
                    } else {
                        shortDescLayout.setVisibility(View.GONE);
                    }

                    if (null != fundDetails.get("lDescription")
                            && !"".equals(fundDetails.get("lDescription"))) {
                        longDescWebView.setVisibility(View.VISIBLE);

                        longDesc = fundDetails.get("lDescription");
                        htmlcode = "<html><body style=\"text-align:justify\"> %s </body></Html>";

                        if (null != longDescWebView) {

                            longDesc = longDesc.replaceAll("&#60;", "<");
                            longDesc = longDesc.replaceAll("&#62;", ">");

                            longDescWebView.loadData(
                                    String.format(htmlcode, longDesc), "text/html",
                                    "utf-8");
                        }
                    } else {
                        longDescWebView.setVisibility(View.GONE);
                    }

                    if (null != fundDetails.get("purchaseProducts")
                            && !"".equals(fundDetails.get("purchaseProducts"))) {

                        purchaseProdLayout.setVisibility(View.VISIBLE);
                        purchaseProdText = fundDetails.get("purchaseProducts");
                        purchaseProdValue.setText(purchaseProdText);
                    } else {
                        purchaseProdLayout.setVisibility(View.GONE);
                    }

                    if (null != fundDetails.get("fundGoal")
                            && !"".equals(fundDetails.get("fundGoal"))) {
                        fundraiserGoal.setVisibility(View.VISIBLE);
                        fundraiserGoal.setText("Fundraising Goal: "
                                + fundDetails.get("fundGoal"));
                    } else {
                        fundraiserGoal.setVisibility(View.GONE);
                    }

                    if (null != fundDetails.get("currentLevel")
                            && !"".equals(fundDetails.get("currentLevel"))) {
                        currentLevel.setVisibility(View.VISIBLE);
                        currentLevel.setText("Current Level: "
                                + fundDetails.get("currentLevel"));
                    } else {
                        currentLevel.setVisibility(View.GONE);
                    }

                    String startDate = "";
                    String endDate = "";

                    if (null != fundDetails.get("sDate")
                            && !"".equals(fundDetails.get("sDate"))
                            && !"N/A".equalsIgnoreCase(fundDetails.get("sDate"))) {
                        startDate = "Start Date :" + fundDetails.get("sDate");
                    }

                    if (null != fundDetails.get("eDate")
                            && !"".equals(fundDetails.get("eDate"))
                            && !"N/A".equalsIgnoreCase(fundDetails.get("eDate"))) {
                        endDate = "End Date :" + fundDetails.get("eDate");
                    }
                    eventStartEndDate.setText(startDate + " " + endDate);

                    // Call for Share Information
                    shareInfo = new ShareInformation(
                            FundraiserEventDescription.this, getIntent()
                            .getExtras().getString("fundID"), "Fundraiser");
                    isShareTaskCalled = false;

                    leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
                    leftTitleImage.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (!Constants.GuestLoginId.equals(UrlRequestParams
                                    .getUid().trim())) {
                                shareInfo.shareTask(isShareTaskCalled);
                                isShareTaskCalled = true;
                            } else {
                                Constants
                                        .SignUpAlert(FundraiserEventDescription.this);
                                isShareTaskCalled = false;
                            }
                        }
                    });

                } else {
                    progDialog.dismiss();

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            FundraiserEventDescription.this);

                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setMessage(responseText);
                    alertDialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();
                                    cancelFundDetailTask();
                                    finish();
                                }
                            });
                    alertDialogBuilder.show();
                }

                fund_description_layout.setVisibility(View.VISIBLE);

                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... arg0) {

            try {
                String url_fundraiser_description = Properties.url_local_server
                        + Properties.hubciti_version
                        + "alertevent/getfundrsdetail";
                String urlParameters = mUrlRequestParams
                        .createFundraiserEventDesc(fundId, mFundListId);
                JSONObject jsonResponse = mServerConnections
                        .getUrlPostResponseWithHtmlContent(
                                url_fundraiser_description, urlParameters, true);

                return jsonParseFundraiserDetailResponse(jsonResponse);

            } catch (Exception e) {
                e.printStackTrace();
                return returnValue = "UNSUCCESS";
            }

        }

    }

    public String jsonParseFundraiserDetailResponse(JSONObject jsonResponse) {
        Log.v("", "FUNDRAISER DETAIL : " + jsonResponse);
        if (jsonResponse != null) {

            try {

                if (jsonResponse.has("Fundraiser")) {
                    JSONObject jsonObject = null;
                    JSONArray jsonArray = null;
                    try {
                        jsonObject = jsonResponse.getJSONObject("Fundraiser");
                    } catch (Exception e) {
                        jsonArray = jsonResponse.getJSONArray("Fundraiser");
                    }

                    if (jsonObject != null) {

                        if (jsonObject.has("title")) {
                            fundDetails.put("title",
                                    jsonObject.getString("title"));
                        }

                        if (jsonObject.has("fundName")) {
                            fundDetails.put("fundName",
                                    jsonObject.getString("fundName"));
                        }

                        if (jsonObject.has("sDescription")) {
                            fundDetails.put("sDescription",
                                    jsonObject.getString("sDescription"));

                        }

                        if (jsonObject.has("lDescription")) {
                            fundDetails.put("lDescription",
                                    jsonObject.getString("lDescription"));

                        }
                        if (jsonObject.has("hubCitiId")) {
                            fundDetails.put("hubCitiId",
                                    jsonObject.getString("hubCitiId"));

                        }
                        if (jsonObject.has("imgPath")) {
                            fundDetails.put("imgPath",
                                    jsonObject.getString("imgPath"));
                        }

                        if (jsonObject.has("sDate")) {
                            fundDetails.put("sDate",
                                    jsonObject.getString("sDate"));
                        }
                        if (jsonObject.has("eDate")) {
                            fundDetails.put("eDate",
                                    jsonObject.getString("eDate"));
                        }
                        if (jsonObject.has("isAppSiteFlag")) {
                            isAppSiteFlag = jsonObject
                                    .getString("isAppSiteFlag");
                            fundDetails.put("isAppSiteFlag", isAppSiteFlag);
                        }

                        moreInfoFlag = 0;
                        eventFlag = 0;

                        if (jsonObject.has("moreInfoURL")) {
                            if (!"".equals(jsonObject.getString("moreInfoURL"))) {
                                moreInfoFlag = 1;
                                fundDetails.put("moreInfoURL",
                                        jsonObject.getString("moreInfoURL"));
                            }
                        }

                        if (jsonObject.has("orgztnHosting")) {
                            fundDetails.put("orgztnHosting",
                                    jsonObject.getString("orgztnHosting"));
                        }
                        if (jsonObject.has("purchaseProducts")) {
                            fundDetails.put("purchaseProducts",
                                    jsonObject.getString("purchaseProducts"));
                        }
                        if (jsonObject.has("currentLevel")) {
                            fundDetails.put("currentLevel",
                                    jsonObject.getString("currentLevel"));
                        }
                        if (jsonObject.has("fundGoal")) {
                            fundDetails.put("fundGoal",
                                    jsonObject.getString("fundGoal"));
                        }
                        if (jsonObject.has("isEventFlag")) {
                            fundDetails.put("isEventFlag",
                                    jsonObject.getString("isEventFlag"));
                            eventFlag = Integer.parseInt(jsonObject
                                    .getString("isEventFlag"));
                        }
                    } else {

                        if (jsonArray.getJSONObject(0).has("title")) {
                            fundDetails.put("title", jsonArray.getJSONObject(0)
                                    .getString("title"));
                        }

                        if (jsonArray.getJSONObject(0).has("fundName")) {
                            fundDetails.put("fundName", jsonArray
                                    .getJSONObject(0).getString("fundName"));
                        }

                        if (jsonArray.getJSONObject(0).has("sDescription")) {
                            fundDetails
                                    .put("sDescription",
                                            jsonArray.getJSONObject(0)
                                                    .getString("sDescription"));

                        }

                        if (jsonArray.getJSONObject(0).has("lDescription")) {
                            fundDetails
                                    .put("lDescription",
                                            jsonArray.getJSONObject(0)
                                                    .getString("lDescription"));

                        }
                        if (jsonArray.getJSONObject(0).has("hubCitiId")) {
                            fundDetails.put("hubCitiId", jsonArray
                                    .getJSONObject(0).getString("hubCitiId"));

                        }
                        if (jsonArray.getJSONObject(0).has("imgPath")) {
                            fundDetails.put(
                                    "imgPath",
                                    jsonArray.getJSONObject(0).getString(
                                            "imgPath"));
                        }

                        if (jsonArray.getJSONObject(0).has("sDate")) {
                            fundDetails.put("sDate", jsonArray.getJSONObject(0)
                                    .getString("sDate"));
                        }
                        if (jsonArray.getJSONObject(0).has("eDate")) {
                            fundDetails.put("eDate", jsonArray.getJSONObject(0)
                                    .getString("eDate"));
                        }
                        if (jsonArray.getJSONObject(0).has("isAppSiteFlag")) {
                            isAppSiteFlag = jsonArray.getJSONObject(0)
                                    .getString("isAppSiteFlag");
                            fundDetails.put("isAppSiteFlag", isAppSiteFlag);
                        }

                        moreInfoFlag = 0;
                        eventFlag = 0;

                        if (jsonArray.getJSONObject(0).has("moreInfoURL")) {
                            if (!"".equals(jsonArray.getJSONObject(0)
                                    .getString("moreInfoURL"))) {
                                moreInfoFlag = 1;
                                fundDetails.put(
                                        "moreInfoURL",
                                        jsonArray.getJSONObject(0).getString(
                                                "moreInfoURL"));
                            }
                        }

                        if (jsonArray.getJSONObject(0).has("orgztnHosting")) {
                            fundDetails.put("orgztnHosting", jsonArray
                                    .getJSONObject(0)
                                    .getString("orgztnHosting"));
                        }
                        if (jsonArray.getJSONObject(0).has("purchaseProducts")) {
                            fundDetails.put(
                                    "purchaseProducts",
                                    jsonArray.getJSONObject(0).getString(
                                            "purchaseProducts"));
                        }
                        if (jsonArray.getJSONObject(0).has("currentLevel")) {
                            fundDetails
                                    .put("currentLevel",
                                            jsonArray.getJSONObject(0)
                                                    .getString("currentLevel"));
                        }
                        if (jsonArray.getJSONObject(0).has("fundGoal")) {
                            fundDetails.put("fundGoal", jsonArray
                                    .getJSONObject(0).getString("fundGoal"));
                        }
                        if (jsonArray.getJSONObject(0).has("isEventFlag")) {
                            fundDetails.put("isEventFlag", jsonArray
                                    .getJSONObject(0).getString("isEventFlag"));
                            eventFlag = Integer.parseInt(jsonArray
                                    .getJSONObject(0).getString("isEventFlag"));
                        }

                    }

                } else {

                    JSONObject jsonRecordNotFound = jsonResponse
                            .getJSONObject("response");
                    responseText = jsonRecordNotFound.getString("responseText");
                    return returnValue = "UNSUCCESS";

                }
            } catch (Exception ex) {

                ex.printStackTrace();
                finish();
            }

        }

        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (imageLoaderTask != null && !imageLoaderTask.isCancelled()) {
            imageLoaderTask.cancel(true);
            imageLoaderTask = null;
        }
        cancelFundDetailTask();
    }

    private void cancelFundDetailTask() {
        if (fundDetailsTask != null && !fundDetailsTask.isCancelled()) {
            fundDetailsTask.cancel(true);
        }
    }

    /**
     * Customized WebClient to handle Email links, HTTP links and Anchor Tags
     *
     * @author rekha_p
     */
    public class MyWebViewClient extends WebViewClient {
        private final WeakReference<Activity> mActivityRef;
        boolean flag = false;

        public MyWebViewClient(Activity activity) {
            mActivityRef = new WeakReference<>(activity);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // Handles Email Link
            if (url.startsWith("mailto:")) {
                final Activity activity = mActivityRef.get();
                if (activity != null) {
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(activity, mt.getTo(),
                            mt.getSubject(), mt.getBody(), mt.getCc());
                    activity.startActivity(i);
                    view.reload();
                    return true;
                }
            } else if (url != null
                    && (url.startsWith("http://") || url.startsWith("https://"))) {
                // Handles HTTP links (Opens link in device browser)
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // Handles Anchor Tags
            if (url.contains("#") && flag == false) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        longDescWebView.loadData(
                                String.format(htmlcode, longDesc), "text/html",
                                "utf-8");
                    }
                }, 400);

                flag = true;
            } else {
                flag = false;
            }

        }

        /**
         * Intent to open Email Composer screen
         *
         * @param context
         * @param address
         * @param subject
         * @param body
         * @param cc
         * @return
         */
        private Intent newEmailIntent(Context context, String address,
                                      String subject, String body, String cc) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
            intent.putExtra(Intent.EXTRA_TEXT, body);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_CC, cc);
            intent.setType("message/rfc822");
            return intent;
        }
    }

    private void callBrowserIntent(String url) {

        Intent retailerLink = new Intent(FundraiserEventDescription.this,
                ScanseeBrowserActivity.class);
        retailerLink.putExtra(CommonConstants.URL, url);
        startActivity(retailerLink);

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebViewSettings(WebView webView, int fontSize) {
        webView.setVerticalScrollBarEnabled(false);
        webView.setLayerType(View.LAYER_TYPE_NONE, null);

        WebSettings webSettings = webView.getSettings();
        webSettings.setDefaultFontSize(fontSize);
        webSettings.setJavaScriptEnabled(true);

        webView.setWebViewClient(new MyWebViewClient(
                FundraiserEventDescription.this));

        webView.setBackgroundColor(Color.parseColor("#FFFFFF"));

        // Optimize loading times.

        webSettings.setAppCacheEnabled(false);
        webSettings.setBlockNetworkImage(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setGeolocationEnabled(false);
        webSettings.setNeedInitialFocus(false);
        webSettings.setSaveFormData(false);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancelFundDetailTask();
        finish();
    }

    class ImageLoaderAsync extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {

            mDialog = ProgressDialog.show(FundraiserEventDescription.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            BitmapDrawable background = new BitmapDrawable(bmImg);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                bmImage.setBackgroundDrawable(background);
            } else {
                bmImage.setBackgroundDrawable(background);
            }

            try {
                if ((this.mDialog != null) && this.mDialog.isShowing()) {
                    this.mDialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                e.printStackTrace();
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                this.mDialog = null;
            }
        }
    }

    private void loadFundDetails() {
        if (fundDetailsTask != null) {
            fundDetailsTask.cancel(true);
            fundDetailsTask = null;
        }

        fundDetailsTask = new FundraiserEventDetailsAsyncTask();
        fundDetailsTask.execute();

    }

}
