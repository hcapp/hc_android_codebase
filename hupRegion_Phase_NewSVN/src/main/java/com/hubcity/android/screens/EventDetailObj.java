package com.hubcity.android.screens;

import java.io.Serializable;

/**
 * Created by subramanya.v on 4/22/2016.
 */
public class EventDetailObj implements Serializable
{
	String eventName;
	String eventCatName;
	String distance;
	String latitude;
	String longitude;
	String address;
	String mapMarkerId;
	String bandName;
	private String groupContent;
	private boolean isHeader;

	public String getGroupContent() {
		return groupContent;
	}

	public void setGroupContent(String groupContent) {
		this.groupContent = groupContent;
	}

	public boolean isHeader() {
		return isHeader;
	}

	public void setIsHeader(boolean isHeader) {
		this.isHeader = isHeader;
	}




	public String getEventId()
	{
		return eventId;
	}

	public void setEventId(String eventId)
	{
		this.eventId = eventId;
	}


	public String getBandIds() {
		return bandIds;
	}

	public void setBandIds(String bandIds) {
		this.bandIds = bandIds;
	}

	String bandIds;
	String eventId;
	String startDate;
	String imagePath;

	public String getListingImgPath() {
		return listingImgPath;
	}

	public void setListingImgPath(String listingImgPath) {
		this.listingImgPath = listingImgPath;
	}

	private String listingImgPath;
	String shortDes;
	String longDes;
	String startTime;
	String moreInfoURL;
	String recurringDays;
	int isAppSiteFlag;
	String retailLocationId;
	String retailId;
	String retailName;
	String evtLocTitle;
	String ticketUrl;
	int bandCntFlag;
	String popUpMsg;

	public String getPopUpMsg()
	{
		return popUpMsg;
	}

	public void setPopUpMsg(String popUpMsg)
	{
		this.popUpMsg = popUpMsg;
	}

	public int getBandCntFlag()
	{
		return bandCntFlag;
	}

	public void setBandCntFlag(int bandCntFlag)
	{
		this.bandCntFlag = bandCntFlag;
	}



	public String getTicketUrl() {
		return ticketUrl;
	}

	public void setTicketUrl(String ticketUrl) {
		this.ticketUrl = ticketUrl;
	}

	public String getRecurringDays()
	{
		return recurringDays;
	}

	public void setRecurringDays(String recurringDays)
	{
		this.recurringDays = recurringDays;
	}

	public String getEvtLocTitle()
	{
		return evtLocTitle;
	}

	public void setEvtLocTitle(String evtLocTitle)
	{
		this.evtLocTitle = evtLocTitle;
	}


	public String getRetailLocationId()
	{
		return retailLocationId;
	}

	public void setRetailLocationId(String retailLocationId)
	{
		this.retailLocationId = retailLocationId;
	}

	public int getIsAppSiteFlag()
	{
		return isAppSiteFlag;
	}

	public void setIsAppSiteFlag(int isAppSiteFlag)
	{
		this.isAppSiteFlag = isAppSiteFlag;
	}

	public String getRetailId()
	{
		return retailId;
	}

	public void setRetailId(String retailId)
	{
		this.retailId = retailId;
	}

	public String getRetailName()
	{
		return retailName;
	}

	public void setRetailName(String retailName)
	{
		this.retailName = retailName;
	}

	public String getShortDes()
	{
		return shortDes;
	}

	public void setShortDes(String shortDes)
	{
		this.shortDes = shortDes;
	}

	public String getImagePath()
	{
		return imagePath;
	}

	public void setImagePath(String imagePath)
	{
		this.imagePath = imagePath;
	}

	public String getLongDes()
	{
		return longDes;
	}

	public void setLongDes(String longDes)
	{
		this.longDes = longDes;
	}

	public String getStartTime()
	{
		return startTime;
	}

	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}

	public String getMoreInfoURL()
	{
		return moreInfoURL;
	}

	public void setMoreInfoURL(String moreInfoURL)
	{
		this.moreInfoURL = moreInfoURL;
	}


	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}


	public String getBandName()
	{
		return bandName;
	}

	public void setBandName(String bandName)
	{
		this.bandName = bandName;
	}


	public String getMapMarkerId()
	{
		return mapMarkerId;
	}

	public void setMapMarkerId(String mapMarkerId)
	{
		this.mapMarkerId = mapMarkerId;
	}


	public String getLatitude()
	{
		return latitude;
	}

	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}


	public String getEventCatName()
	{
		return eventCatName;
	}

	public void setEventCatName(String eventCatName)
	{
		this.eventCatName = eventCatName;
	}

	public String getEventName()
	{
		return eventName;
	}

	public void setEventName(String eventName)
	{
		this.eventName = eventName;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		this.distance = distance;
	}


}
