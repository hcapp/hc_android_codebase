package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.CuopnsDetailsBO;
import com.hubcity.android.businessObjects.HotDealsDetailsBO;
import com.hubcity.android.businessObjects.ProductBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.CouponClaimRedeemExpiredListAdapter1;
import com.scansee.android.CouponsClaimedUsedExpiredActivty;
import com.scansee.android.CouponsDetailActivity;
import com.scansee.android.HotDealClaimRedeemExpiredActivty;
import com.scansee.android.HotDealClaimRedeemExpiredListAdapter;
import com.scansee.android.HotDealsDetailsActivity;
import com.scansee.android.RetailerCurrentsalesActivity;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CurrentSpecialsListActivity extends CustomTitleBar implements
        OnClickListener {
    protected HotDealClaimRedeemExpiredListAdapter hotDealClaimedRedeemExpiredListAdapter;

    protected ArrayList<HashMap<String, String>> specialoffersList = null;
    private HashMap<String, String> specialOfferData = new HashMap<>();
    private HashMap<String, String> flagData = new HashMap<>();

    protected ListView specialoffersListView;
    SpecialOfferListAdapter specialofferListAdapter;
    protected CouponClaimRedeemExpiredListAdapter1 couponsListAdapter;

    String responseCode = null;

    String userId, retailLocationID, retailID,
            retailerAdURL, retailerAd, retailerName;
    int minProdId = 0;
    String retListId = null, mainmenuId = null;
    boolean speOffClick = false;
    SharedPreferences settings = null;
    CustomImageLoader customImageLoader;

    Intent navIntent = null;
    RadioGroup couponRadio;
    RadioButton radioLoc, radioProd;
    Button moreInfo;
    @SuppressWarnings("rawtypes")
    ArrayList hotdealsListbyLocations, hotdealsListbyProducts;
    View moreResultsView;
    int maxCount = 0;
    int maxRowNum = 0;
    Context context = this;

    // For Bottom Buttons
    LinearLayout linearLayout = null, navBottombar = null;
    BottomButtons bb = null;
    Activity activity;
    boolean hasBottomBtns = false;
    boolean enableBottomButton = true;

    private String lastVisitedNo = "0";

    @Override
    protected void onResume() {
        super.onResume();

        List<ApplicationInfo> packages;
        PackageManager pm;
        pm = getPackageManager();
        // get a list of installed apps.
        packages = pm.getInstalledApplications(0);

        ActivityManager mActivityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);

        for (ApplicationInfo packageInfo : packages) {
            if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1
                    || packageInfo.packageName.contains("com.adobe.reader")) {
                continue;
            }

            mActivityManager.killBackgroundProcesses(packageInfo.packageName);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.retailer_specials_itemslist);

        try {
            activity = CurrentSpecialsListActivity.this;
            // Initiating Bottom button class
            bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
            // Add screen name when needed
            bb.setActivityInfo(activity, "");

            linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
            linearLayout.setVisibility(View.INVISIBLE);

            title.setText("Sales");
            leftTitleImage.setVisibility(View.GONE);

            navBottombar = (LinearLayout) findViewById(R.id.navBottombar);
            navBottombar.setVisibility(View.GONE);


            couponRadio = (RadioGroup) findViewById(R.id.coupon_radio);

            radioLoc = (RadioButton) findViewById(R.id.coupon_radio_location);
            radioProd = (RadioButton) findViewById(R.id.coupon_radio_product);
            couponRadio
                    .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            specialoffersListView.setAdapter(null);
                            if ("Hot Deals".equals(title.getText())) {
                                if (radioLoc.isChecked()) {
                                    lastvisitProdIdLocation = 0;
                                    hotdealsListbyLocations = new ArrayList<>();
                                } else if (radioProd.isChecked()) {
                                    lastvisitProdIdProducts = 0;
                                    hotdealsListbyProducts = new ArrayList<>();
                                }
                            } else if ("Coupons".equals(title.getText())) {
                                specialoffersList = new ArrayList<>();
                                lastvisitProdIdProducts = 0;
                            }

                        }
                    });

            // get extras
            Intent i = getIntent();
            String itemName = i.getStringExtra("specialitemName");
            retailerAd = i.getStringExtra("retailerAd");
            retailLocationID = i.getStringExtra("retailLocationID");
            retailID = i.getStringExtra("retailID");
            retListId = i.getStringExtra("retListId");
            retailerAdURL = i.getStringExtra("retailerAdURL");
            retailerName = i.getStringExtra("retailerName");

            if (getIntent().hasExtra(Constants.TAG_RETAILE_LOCATIONID)) {
                retailLocationID = getIntent().getExtras().getString(
                        Constants.TAG_RETAILE_LOCATIONID);
            }
            if (getIntent().hasExtra(Constants.TAG_RETAILE_ID)) {
                retailID = getIntent().getExtras().getString(
                        Constants.TAG_RETAILE_ID);
            }
            if (getIntent().hasExtra(Constants.TAG_RIBBON_ADIMAGE_PATH)) {
                retailerAd = getIntent().getExtras().getString(
                        Constants.TAG_RIBBON_ADIMAGE_PATH);
            }
            if (getIntent().getExtras().getString(
                    CommonConstants.TAG_RETAIL_LIST_ID) != null) {
                retListId = getIntent().getExtras().getString(
                        CommonConstants.TAG_RETAIL_LIST_ID);
            }

            if (getIntent().hasExtra(Constants.MAIN_MENU_ID)) {
                mainmenuId = getIntent().getExtras().getString(
                        Constants.MAIN_MENU_ID);
            }

            if (getIntent().hasExtra(CommonConstants.TAG_RIBBON_AD_URL)) {
                retailerAdURL = getIntent().getExtras().getString(
                        CommonConstants.TAG_RIBBON_AD_URL);
            }
            if (getIntent().hasExtra(Constants.TAG_RETAILER_NAME)) {
                retailerName = getIntent().getExtras().getString(
                        Constants.TAG_RETAILER_NAME);
            }

            radioLoc.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    lastvisitProdIdLocation = 0;

                    // nextPageLocation = false;
                    hotdealsListbyLocations = new ArrayList<>();

                    progDialog = new ProgressDialog(
                            CurrentSpecialsListActivity.this);
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDialog.setMessage(Constants.DIALOG_MESSAGE);
                    progDialog.setCancelable(false);
                    progDialog.show();
                    nextPage = false;
                    if ("Hot Deals".equals(title.getText())) {

                        new HotDealsClaimedByLocation().execute();

                    } else if ("Coupons".equals(title.getText())) {
                        new FindCoupons().execute();

                    }

                }
            });

            radioProd.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    lastvisitProdIdProducts = 0;
                    lastvisitProdIdLocation = 0;
                    // nextPageProducts = false;
                    progDialog = new ProgressDialog(
                            CurrentSpecialsListActivity.this);
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDialog.setMessage(Constants.DIALOG_MESSAGE);
                    progDialog.setCancelable(false);
                    progDialog.show();
                    nextPage = false;
                    hotdealsListbyProducts = new ArrayList<>();
                    if ("Hot Deals".equals(title.getText())) {
                        new HotDealsClaimedByLocation().execute();

                    } else if ("Coupons".equals(title.getText())) {

                        new FindCoupons().execute();
                    }
                }
            });

            moreResultsView = getLayoutInflater().inflate(
                    R.layout.events_pagination, specialoffersListView, true);
            moreInfo = (Button) moreResultsView
                    .findViewById(R.id.events_view_more_button);
            moreInfo.setOnClickListener(this);

            specialoffersListView = (ListView) findViewById(R.id.listview_retailer_specials);
            specialoffersListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    try {
                        specialoffersListView.getAdapter();
                        String itemName = specialoffersListView.getAdapter()
                                .getItem(position).toString();

                        if (itemName.contains("coupons")) {
                            HashMap<String, String> selectedData = specialoffersList
                                    .get(position);
                            String clickedItem = selectedData.get("itemName");
                            Intent navIntent = new Intent(
                                    CurrentSpecialsListActivity.this,
                                    CouponsDetailActivity.class);

                            if ("coupons".equalsIgnoreCase(clickedItem)) {
                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSCOUPONNAME,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSCOUPONNAME));
                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSCOUPONID,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSCOUPONID));
                                if (radioLoc.isSelected()) {
                                    navIntent.putExtra(
                                            CommonConstants.TAB_SELECTED,
                                            CommonConstants.TAB_LOCATION);
                                } else {
                                    navIntent.putExtra(
                                            CommonConstants.TAB_SELECTED,
                                            CommonConstants.TAB_PRODUCT);
                                }
                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSDESCRIPTION,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSDESCRIPTION));
                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSIMAGEPATH,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSIMAGEPATH));
                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSSTARTDATE,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSSTARTDATE));
                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSEXPIRATIONDATE,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSEXPIRATIONDATE));
                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT));

                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSVIEWABLE,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSVIEWABLE));
                                navIntent
                                        .putExtra(
                                                CommonConstants.ALLCOUPONSURL,
                                                selectedData
                                                        .get(CommonConstants.ALLCOUPONSURL));
                                navIntent.putExtra("type", claimRedeemExpiredValue);

                                startActivityForResult(navIntent, 100);
                            }
                        } else if (itemName.contains("CuopnsDetailsBO")) {
                            CuopnsDetailsBO selectedItem = (CuopnsDetailsBO) specialoffersListView
                                    .getAdapter().getItem(position);
                            navigateCouponsActivity(selectedItem, "no");
                        } else if (itemName.contains("HotDealsDetailsBO")) {
                            HotDealsDetailsBO selectedItem = (HotDealsDetailsBO) specialoffersListView
                                    .getAdapter().getItem(position);

                            navigateHotDealsActivity(selectedItem, "no");

                        } else if (itemName.contains("ProductBO")) {
                            ProductBO selectedItem = (ProductBO) specialoffersListView
                                    .getAdapter().getItem(position);
                            navigateSalesActivity(selectedItem, "no");

                        } else if (itemName.contains("details")) {

                            @SuppressWarnings("unchecked")
                            HashMap<String, String> selectedData = (HashMap<String, String>) specialoffersListView
                                    .getAdapter().getItem(position);

                            Intent dealDetail = new Intent(
                                    CurrentSpecialsListActivity.this,
                                    HotDealsDetailsActivity.class);
                            if (selectedData.get("hotDealId") != null) {
                                dealDetail.putExtra("hotDealId",
                                        selectedData.get("hotDealId"));
                            }
                            if (selectedData.get("city") != null) {
                                dealDetail.putExtra("city",
                                        selectedData.get("city"));
                            }
                            if (selectedData.get("hDshortDescription") != null) {
                                dealDetail.putExtra("hDshortDescription",
                                        selectedData.get("hDshortDescription"));
                            }
                            if (selectedData.get("hotDealName") != null) {
                                dealDetail.putExtra("hotDealName",
                                        selectedData.get("hotDealName"));
                            }
                            if (selectedData.get("hDPrice") != null) {
                                dealDetail.putExtra("hDPrice",
                                        selectedData.get("hDPrice"));
                            }
                            if (selectedData.get("hDSalePrice") != null) {
                                dealDetail.putExtra("hDSalePrice",
                                        selectedData.get("hDSalePrice"));
                            }
                            if (selectedData.get("hDEndDate") != null) {
                                dealDetail.putExtra("hDEndDate",
                                        selectedData.get("hDEndDate"));
                            }
                            if (selectedData.get("hDStartDate") != null) {
                                dealDetail.putExtra("hDStartDate",
                                        selectedData.get("hDStartDate"));
                            }
                            if (selectedData.get("hotDealListId") != null) {
                                dealDetail.putExtra("hotDealListId",
                                        selectedData.get("hotDealListId"));
                            }
                            dealDetail.putExtra("singleitem", "no");
                            startActivityForResult(dealDetail, Constants.STARTVALUE);

                        } else {
                            HashMap<String, String> selectedItem = specialoffersList
                                    .get(position);

                            navigateSpecialOffersActivity(selectedItem, "no");

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            });

            if (itemName.equalsIgnoreCase("Coupons")) {
                menuNavCoupons();
            } else if (itemName.equalsIgnoreCase("Deals")) {
                menuNavHotdeals();
            } else if (itemName.equalsIgnoreCase("FundRaisers")) {
                // TODO
            } else if (itemName.equalsIgnoreCase("Sales")) {
                menuNavSales();
            } else if (itemName.equalsIgnoreCase("Special Offers")) {
                menuNavSpecials();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void navigateSalesActivity(ProductBO selectedItem, String singleitem) {
        navIntent = new Intent(CurrentSpecialsListActivity.this,
                RetailerCurrentsalesActivity.class);
        navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_PRODUCTID,
                selectedItem.getproductId());
        navIntent.putExtra(CommonConstants.TAG_RETAILE_ID, retailID);
        navIntent.putExtra(Constants.TAG_RIBBON_ADIMAGE_PATH, retailerAd);
        navIntent.putExtra(Constants.TAG_RIBBON_AD_URL, retailerAdURL);
        navIntent.putExtra(Constants.TAG_RETAILER_NAME, retailerName);
        navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_SALEPRICE,
                selectedItem.getSalePrice());
        navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_REGULARPRICE,
                selectedItem.getRegularPrice());
        navIntent.putExtra(CommonConstants.TAG_CURRENTSALES_PRODUCTNAME,
                selectedItem.getProductName());
        navIntent.putExtra("singleitem", singleitem);
        startActivity(navIntent);

        if ("yes".equalsIgnoreCase(singleitem)) {
            finish();
        }
    }

    public void navigateCouponsActivity(CuopnsDetailsBO selectedItem,
                                        String singleitem) {
        navIntent = new Intent(CurrentSpecialsListActivity.this,
                CouponsDetailActivity.class);
        navIntent.putExtra(CommonConstants.ALLCOUPONSCOUPONNAME,
                selectedItem.getCouponName());

        navIntent.putExtra(CommonConstants.ALLCOUPONSCOUPONID,
                selectedItem.getCouponId());
        navIntent.putExtra(CommonConstants.ALLCOUPONSVIEWABLE,
                selectedItem.getViewableOnWeb());

        if ("Green".equalsIgnoreCase(selectedItem.getUsage())) {
            navIntent.putExtra("type", CommonConstants.COUPON_TYPE_USED);
            navIntent.putExtra(CommonConstants.ALLCOUPONSFAVFLAG, "true");
        } else {
            navIntent.putExtra("type", CommonConstants.COUPON_TYPE_ALL);
            navIntent.putExtra(CommonConstants.ALLCOUPONSFAVFLAG, "false");
        }

        navIntent.putExtra(CommonConstants.ALLCOUPONSDESCRIPTION,
                selectedItem.getCouponLongDescription());
        navIntent.putExtra(CommonConstants.ALLCOUPONSIMAGEPATH,
                selectedItem.getCouponImagePath());
        navIntent.putExtra(CommonConstants.ALLCOUPONSSTARTDATE,
                selectedItem.getCouponStartDate());
        navIntent.putExtra(CommonConstants.ALLCOUPONSEXPIRATIONDATE,
                selectedItem.getCouponExpireDate());
        navIntent.putExtra(CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT,
                selectedItem.getCouponDiscountAmount());

        navIntent.putExtra(CommonConstants.ALLCOUPONSURL,
                selectedItem.getCouponURL());
        navIntent.putExtra("singleitem", singleitem);
        startActivity(navIntent);

        if ("yes".equalsIgnoreCase(singleitem)) {
            finish();
        }
    }

    public void navigateHotDealsActivity(HotDealsDetailsBO selectedItem,
                                         String singleitem) {

        Intent dealDetail = new Intent(CurrentSpecialsListActivity.this,
                HotDealsDetailsActivity.class);
        dealDetail.putExtra("hotDealId", selectedItem.getHotDealId());
        dealDetail.putExtra("city", selectedItem.getCity());
        dealDetail.putExtra("hDshortDescription",
                selectedItem.gethDshortDescription());
        dealDetail.putExtra("hotDealName", selectedItem.getHotDealName());

        dealDetail.putExtra("hDPrice", selectedItem.gethDPrice());
        dealDetail.putExtra("hDSalePrice", selectedItem.gethDSalePrice());

        dealDetail.putExtra("hDEndDate", selectedItem.gethDEndDate());
        dealDetail.putExtra("hDStartDate", selectedItem.gethDStartDate());
        dealDetail.putExtra("hotDealListId", selectedItem.getHotDealListId());
        dealDetail.putExtra("singleitem", singleitem);
        startActivity(dealDetail);

        if ("yes".equalsIgnoreCase(singleitem)) {
            finish();
        }
    }

    public void navigateSpecialOffersActivity(
            HashMap<String, String> selectedItem, String singleitem) {
        speOffClick = true;
        new UTSpecialOffersClick().execute();

        Intent intent = new Intent(CurrentSpecialsListActivity.this,
                SpecialOffersScreen.class);
        intent.putExtra("retailerId", retailID);
        intent.putExtra("retailLocationId", retailLocationID);
        intent.putExtra("pageId", selectedItem.get("pageId"));
        startActivity(intent);

        if ("yes".equalsIgnoreCase(singleitem)) {
            finish();
        }

    }

    private class GetProductDetailAsyncTask extends
            AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog progDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDialog = new ProgressDialog(CurrentSpecialsListActivity.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setCancelable(false);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.show();
        }

        private ArrayList<ProductBO> productBOList;
        private ProductSaleAdapter mProductSaleAdapter;

        @Override
        protected String doInBackground(String... params) {
            String result = "false";
            try {

                String url_get_products = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/getProducts";

                String urlParameters = mUrlRequestParams.getProductDetail(
                        retailLocationID, retListId, retailID, lastVisitedNo,
                        mainmenuId);

                JSONObject xmlResponde = mServerConnections.getUrlPostResponse(
                        url_get_products, urlParameters, true);
                if (xmlResponde != null) {
                    try {

                        JSONObject couponDetailsDetailJson = xmlResponde
                                .getJSONObject("ProductDetails");
                        responseCode = couponDetailsDetailJson
                                .getString("responseCode");
                        productBOList = new ArrayList<>();

                        if (xmlResponde.has("response")) {
                            JSONObject response = xmlResponde
                                    .getJSONObject("response");
                            responseCode = response.getString("responseCode");

                        } else if ("10000".equalsIgnoreCase(responseCode)) {
                            if (xmlResponde.has("ProductDetails")) {

                                JSONObject productDetails = xmlResponde
                                        .getJSONObject("ProductDetails");

                                JSONArray productDetailsJsonArray = productDetails
                                        .optJSONArray("ProductDetail");
                                if (productDetailsJsonArray != null) {

                                    for (int i = 0; i < productDetailsJsonArray
                                            .length(); i++) {

                                        ProductBO productBO = parseSingleProductDetailsJson(productDetailsJsonArray
                                                .getJSONObject(i));
                                        productBOList.add(productBO);
                                    }
                                } else {
                                    JSONObject cuopnsDetailsJsonObj = productDetails
                                            .optJSONObject("ProductDetail");
                                    ProductBO productBO = parseSingleProductDetailsJson(cuopnsDetailsJsonObj);
                                    productBOList.add(productBO);
                                }
                            }

                            result = "true";
                        } else if ("10005".equalsIgnoreCase(xmlResponde
                                .getJSONObject("response").getString(
                                        "responseCode"))) {
                            responseCode = xmlResponde
                                    .getJSONObject("response").getString(
                                            "responseCode");
                            result = "false";
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        private ProductBO parseSingleProductDetailsJson(JSONObject jsonObject)
                throws JSONException {

            if (jsonObject != null) {
                String productDescription = jsonObject
                        .getString("productDescription");
                String manufacturersName = jsonObject
                        .getString("manufacturersName");
                String imagePath = jsonObject.getString("imagePath");
                String modelNumber = jsonObject.getString("modelNumber");
                String suggestedRetailPrice = jsonObject
                        .getString("suggestedRetailPrice");
                String productWeight = jsonObject.getString("productWeight");
                String productExpDate = jsonObject.getString("productExpDate");
                String weightUnits = jsonObject.getString("weightUnits");

                String saleListID = jsonObject.getString("saleListId");
                String isShopCartItem = jsonObject.getString("isShopCartItem");
                String regularPrice = jsonObject.getString("regularPrice");
                String salePrice = jsonObject.getString("salePrice");
                String rowNum = jsonObject.getString("row__Num");
                String productId = jsonObject.getString("productId");
                String productName = jsonObject.getString("productName");

                return new ProductBO(productDescription, manufacturersName,
                        imagePath, modelNumber, suggestedRetailPrice,
                        productWeight, productExpDate, weightUnits, saleListID,
                        isShopCartItem, regularPrice, salePrice, rowNum,
                        productId, productName);

            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progDialog.dismiss();
                linearLayout.setVisibility(View.VISIBLE);

                if ("true".equals(result)) {
                    findViewById(R.id.no_coupons_found).setVisibility(View.GONE);
                    Log.e("PRODUCTSIZE ", productBOList.size() + "  SIZEEE");
                    if (productBOList.size() == 1) {
                        ProductBO selectedItem = productBOList.get(0);

                        navigateSalesActivity(selectedItem, "yes");
                    } else {
                        mProductSaleAdapter = new ProductSaleAdapter(
                                CurrentSpecialsListActivity.this, productBOList);
                        specialoffersListView.setAdapter(mProductSaleAdapter);
                    }
                } else {
                    findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class RetailerCoupon extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog progDialog;
        private ArrayList<CuopnsDetailsBO> couponDetailsDetailJsonList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDialog = new ProgressDialog(CurrentSpecialsListActivity.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "false";
            try {
                String url_retl_occoup = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/retloccoup";

                String urlParameters = mUrlRequestParams
                        .getRetailerCupons(retailLocationID, retListId,
                                retailID, mainmenuId, null);
                JSONObject xmlResponde = mServerConnections.getUrlPostResponse(
                        url_retl_occoup, urlParameters, true);

                if (xmlResponde != null) {
                    try {

                        JSONObject couponDetailsDetailJson = xmlResponde
                                .getJSONObject("CouponDetails");
                        responseCode = couponDetailsDetailJson
                                .getString("responseCode");
                        couponDetailsDetailJsonList = new ArrayList<>();

                        if (xmlResponde.has("response")) {
                            JSONObject response = xmlResponde
                                    .getJSONObject("response");
                            String responseText = response
                                    .getString("responseText");
                            responseCode = response.getString("responseCode");

                            Looper.prepare();
                            Toast.makeText(
                                    getApplicationContext(),
                                    "CouponDetails responseText "
                                            + responseText, Toast.LENGTH_SHORT)
                                    .show();
                            Looper.loop();
                        } else if ("10000".equalsIgnoreCase(responseCode)) {
                            if (xmlResponde.has("CouponDetails")) {

                                JSONObject coupDetails = couponDetailsDetailJson
                                        .getJSONObject("coupDetails");

                                JSONArray productDetailsJsonArray = coupDetails
                                        .optJSONArray("CouponDetails");
                                if (productDetailsJsonArray != null) {

                                    for (int i = 0; i < productDetailsJsonArray
                                            .length(); i++) {

                                        CuopnsDetailsBO cuopnsDetailsBO = parseSingleCouponDetailsJson(productDetailsJsonArray
                                                .getJSONObject(i));
                                        couponDetailsDetailJsonList
                                                .add(cuopnsDetailsBO);
                                    }
                                } else {
                                    JSONObject cuopnsDetailsJsonObj = coupDetails
                                            .optJSONObject("CouponDetails");
                                    CuopnsDetailsBO cuopnsDetailsBO = parseSingleCouponDetailsJson(cuopnsDetailsJsonObj);
                                    couponDetailsDetailJsonList
                                            .add(cuopnsDetailsBO);
                                }
                            }

                            result = "true";
                        } else if ("10005".equalsIgnoreCase(xmlResponde
                                .getJSONObject("response").getString(
                                        "responseCode"))) {
                            responseCode = xmlResponde
                                    .getJSONObject("response").getString(
                                            "responseCode");
                            result = "false";
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        private CuopnsDetailsBO parseSingleCouponDetailsJson(
                JSONObject couponDetailsJsonObj) throws JSONException {

            if (couponDetailsJsonObj != null) {

                String viewableOnWeb = couponDetailsJsonObj
                        .getString("viewableOnWeb");
                String couponURL = couponDetailsJsonObj.getString("couponURL");
                String rowNum = couponDetailsJsonObj.getString("rowNum");
                String couponId = couponDetailsJsonObj.getString("couponId");
                String couponName = couponDetailsJsonObj
                        .getString("couponName");
                String couponDiscountType = couponDetailsJsonObj
                        .getString("couponDiscountType");
                String couponDiscountAmount = couponDetailsJsonObj
                        .getString("couponDiscountAmount");
                String couponDiscountPct = couponDetailsJsonObj
                        .getString("couponDiscountPct");
                String couponShortDescription = couponDetailsJsonObj
                        .optString("couponShortDescription");
                String couponLongDescription = couponDetailsJsonObj
                        .getString("couponLongDescription");
                String couponDateAdded = couponDetailsJsonObj
                        .getString("couponDateAdded");
                String couponListId = couponDetailsJsonObj
                        .getString("couponListId");
                String couponStartDate = couponDetailsJsonObj
                        .getString("couponStartDate");
                String couponExpireDate = couponDetailsJsonObj
                        .getString("couponExpireDate");
                String usage = couponDetailsJsonObj.getString("usage");
                String couponImagePath = couponDetailsJsonObj
                        .getString("couponImagePath");
                boolean isFeatured = couponDetailsJsonObj.optBoolean("isFeatured");

                return new CuopnsDetailsBO(viewableOnWeb, couponURL, rowNum,
                        couponId, couponName, couponDiscountType,
                        couponDiscountAmount, couponDiscountPct,
                        couponShortDescription, couponLongDescription,
                        couponDateAdded, retListId, couponListId,
                        couponStartDate, couponExpireDate, usage,
                        couponImagePath, isFeatured);
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progDialog.dismiss();
                linearLayout.setVisibility(View.VISIBLE);

                if ("true".equals(result)) {
                    findViewById(R.id.no_coupons_found).setVisibility(View.GONE);
                    Log.e("COUPONSIZE", couponDetailsDetailJsonList.size()
                            + "   size");
                    if (couponDetailsDetailJsonList.size() == 1) {
                        CuopnsDetailsBO selectedItem = couponDetailsDetailJsonList
                                .get(0);

                        navigateCouponsActivity(selectedItem, "yes");
                    } else {
                        CuponListAdapter mCuponListAdapter = new CuponListAdapter(
                                CurrentSpecialsListActivity.this,
                                couponDetailsDetailJsonList);
                        specialoffersListView.setAdapter(mCuponListAdapter);
                    }
                } else {
                    findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class RetHotdealsAsyncTask extends AsyncTask<String, Void, String> {
        @SuppressWarnings("rawtypes")
        ArrayList hotDealsDetailsList;
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog progDialog;
        private RetailerHotDealAdapter mRetailerHotDealAdapter;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            progDialog = new ProgressDialog(CurrentSpecialsListActivity.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();
        }

        @SuppressWarnings("unchecked")
        @Override
        protected String doInBackground(String... params) {
            String result = "false";
            String responseCode;
            specialoffersList = new ArrayList<>();
            JSONObject xmlResponde;
            try {
                String url_get_ret_hot_deals = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/getrethotdeals";
                String urlParameters = mUrlRequestParams.getRetHotdeals(
                        retailLocationID, retListId, retailID, null);
                xmlResponde = mServerConnections.getUrlPostResponse(
                        url_get_ret_hot_deals, urlParameters, true);
                if (xmlResponde != null) {
                    try {

                        JSONObject hotDealssDetailJson = xmlResponde
                                .getJSONObject("HotDealsDetails");
                        responseCode = hotDealssDetailJson
                                .getString("responseCode");
                        hotDealsDetailsList = new ArrayList<>();

                        if (xmlResponde.has("response")) {
                            JSONObject response = xmlResponde
                                    .getJSONObject("response");
                            String responseText = response
                                    .getString("responseText");
                            responseCode = response.getString("responseCode");

                            Looper.prepare();
                            Toast.makeText(
                                    getApplicationContext(),
                                    "RetHotdealsAsyncTask responseText "
                                            + responseText, Toast.LENGTH_SHORT)
                                    .show();
                            Looper.loop();
                        } else if ("10000".equalsIgnoreCase(responseCode)) {

                            if (xmlResponde.has("HotDealsDetails")) {

                                JSONObject hdDetails = hotDealssDetailJson
                                        .getJSONObject("hdDetails");

                                JSONArray hotDealsDetailsJsonArray = hdDetails
                                        .optJSONArray("HotDealsDetails");
                                if (hotDealsDetailsJsonArray != null) {

                                    for (int i = 0; i < hotDealsDetailsJsonArray
                                            .length(); i++) {

                                        HotDealsDetailsBO hotDealsDetailsBO = parseSingleHotDealsDetailsJson(hotDealsDetailsJsonArray
                                                .getJSONObject(i));
                                        hotDealsDetailsList
                                                .add(hotDealsDetailsBO);
                                    }
                                } else {
                                    JSONObject hotDealsDetailsJsonObj = hdDetails
                                            .optJSONObject("HotDealsDetails");
                                    HotDealsDetailsBO hotDealsDetailsBO = parseSingleHotDealsDetailsJson(hotDealsDetailsJsonObj);
                                    hotDealsDetailsList.add(hotDealsDetailsBO);
                                }
                            }

                            result = "true";
                        } else if ("10005".equalsIgnoreCase(xmlResponde
                                .getJSONObject("response").getString(
                                        "responseCode"))) {
                            responseCode = xmlResponde
                                    .getJSONObject("response").getString(
                                            "responseCode");
                            result = "false";
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        private HotDealsDetailsBO parseSingleHotDealsDetailsJson(
                JSONObject hotDealsDetailsJsonObj) throws JSONException {

            if (hotDealsDetailsJsonObj != null) {
                String hDStartDate = "";
                String hDEndDate = "";
                String city = hotDealsDetailsJsonObj.getString("city");
                String rowNumber = hotDealsDetailsJsonObj
                        .getString("rowNumber");
                String apiPartnerId = hotDealsDetailsJsonObj
                        .getString("apiPartnerId");
                String apiPartnerName = hotDealsDetailsJsonObj
                        .getString("apiPartnerName");
                String hotDealName = hotDealsDetailsJsonObj
                        .getString("hotDealName");
                String hotDealId = hotDealsDetailsJsonObj
                        .getString("hotDealId");
                String hotDealImagePath = hotDealsDetailsJsonObj
                        .getString("hotDealImagePath");
                String hDshortDescription = hotDealsDetailsJsonObj
                        .getString("hDshortDescription");
                String hDPrice = hotDealsDetailsJsonObj.getString("hDPrice");
                String hDSalePrice = hotDealsDetailsJsonObj
                        .getString("hDSalePrice");
                String hotDealListId = hotDealsDetailsJsonObj
                        .getString("hotDealListId");
                if (hotDealsDetailsJsonObj.has("hDStartDate")) {
                    hDStartDate = hotDealsDetailsJsonObj
                            .getString("hDStartDate");
                }
                if (hotDealsDetailsJsonObj.has("hDEndDate")) {
                    hDEndDate = hotDealsDetailsJsonObj.getString("hDEndDate");
                }

                return new HotDealsDetailsBO(city, rowNumber, apiPartnerId,
                        apiPartnerName, hotDealName, hotDealId,
                        hotDealImagePath, hDshortDescription, hDPrice,
                        hDSalePrice, hotDealListId, hDStartDate, hDEndDate);
            } else {
                return null;
            }
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(String result) {
            try {
                progDialog.dismiss();
                linearLayout.setVisibility(View.VISIBLE);

                if ("true".equals(result)) {
                    findViewById(R.id.no_coupons_found).setVisibility(View.GONE);

                    Log.e("DELASLENGTH ", hotDealsDetailsList.size()
                            + "  lengthhhh");

                    if (hotDealsDetailsList.size() == 1) {

                        HotDealsDetailsBO selectedItem = (HotDealsDetailsBO) hotDealsDetailsList
                                .get(0);

                        navigateHotDealsActivity(selectedItem, "yes");

                    } else {

                        mRetailerHotDealAdapter = new RetailerHotDealAdapter(
                                CurrentSpecialsListActivity.this,
                                hotDealsDetailsList);
                        specialoffersListView.setAdapter(mRetailerHotDealAdapter);
                    }
                } else {
                    findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private class SpecialOfferList extends AsyncTask<String, Void, String> {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog progDialog;
        private JSONArray jsonOffersArray;
        private Object jsonOffersObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDialog = new ProgressDialog(CurrentSpecialsListActivity.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();
        }

        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... params) {

            specialoffersList = new ArrayList<>();
            String result = "false";
            try {
                String url_get_special_offer_list = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/getspecialofferlist";

                String urlParameters = mUrlRequestParams.getSpecialOffersInfo(
                        retailLocationID, retListId, retailID, mainmenuId,
                        lastVisitedNo);
                jsonObject = mServerConnections.getUrlPostResponse(
                        url_get_special_offer_list, urlParameters, true);

                if (jsonObject != null) {
                    try {
                        try {
                            jsonOffersArray = new JSONArray();
                            jsonOffersObject = jsonObject
                                    .getJSONObject("RetailersDetails")
                                    .getJSONObject("retailerDetail")
                                    .getJSONObject(
                                            CommonConstants.TAG_PAGE_RETAILERDETAIL);
                            jsonOffersArray.put(jsonOffersObject);
                        } catch (Exception e) {
                            e.printStackTrace();

                            jsonOffersArray = jsonObject
                                    .getJSONObject("RetailersDetails")
                                    .getJSONObject("retailerDetail")
                                    .getJSONArray(
                                            CommonConstants.TAG_PAGE_RETAILERDETAIL);
                        }
                        for (int count = 0; count < jsonOffersArray.length(); count++) {
                            specialOfferData = new HashMap<>();
                            specialOfferData
                                    .put(CommonConstants.TAG_PAGE_LINK,
                                            jsonOffersArray
                                                    .getJSONObject(count)
                                                    .getString(
                                                            CommonConstants.TAG_PAGE_LINK));
                            specialOfferData
                                    .put(CommonConstants.TAG_PAGE_TITLE,
                                            jsonOffersArray
                                                    .getJSONObject(count)
                                                    .getString(
                                                            CommonConstants.TAG_PAGE_TITLE));
                            specialOfferData.put("pageId", jsonOffersArray
                                    .getJSONObject(count).getString("pageId"));

                            specialOfferData.put("itemName",
                                    "specialOffersData");
                            specialoffersList.add(specialOfferData);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        jsonOffersArray = null;
                    }
                    result = "true";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                if (jsonObject.has("response")
                        && "10005".equalsIgnoreCase(jsonObject.getJSONObject(
                        "response").getString("responseCode"))) {
                    responseCode = jsonObject.getJSONObject("response")
                            .getString("responseCode");

                    result = "false";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progDialog.dismiss();
                linearLayout.setVisibility(View.VISIBLE);

                if ("true".equals(result)) {
                    findViewById(R.id.no_coupons_found).setVisibility(View.GONE);
                    Log.d("speccialOfferListSize", "" + specialoffersList.size());
                    if (specialoffersList.size() == 1) {
                        HashMap<String, String> selectedItem = specialoffersList
                                .get(0);

                        navigateSpecialOffersActivity(selectedItem, "yes");

                    } else {
                        specialofferListAdapter = new SpecialOfferListAdapter(
                                CurrentSpecialsListActivity.this, specialoffersList);
                        specialoffersListView.setAdapter(specialofferListAdapter);
                    }
                } else {
                    findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class UTSpecialOffersClick extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;

        @Override
        protected String doInBackground(String... params) {

            String result = "false";
            try {
                result = "true";

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

    }

    @Override
    public void onClick(View v) {
        specialoffersList = new ArrayList<>();
        findViewById(R.id.no_coupons_found).setVisibility(View.GONE);

        switch (v.getId()) {


            default:
                break;
        }

    }

    /*
     * Navigates to Deals Used/Expired/Claimed items
     */
    private void dealsGallery(String type) {

        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            Intent navIntent = new Intent(activity,
                    HotDealClaimRedeemExpiredActivty.class);
            navIntent.putExtra(CommonConstants.TAB_SELECTED, type);

            activity.startActivity(navIntent);
        } else {
            Constants.SignUpAlert(CurrentSpecialsListActivity.this);
        }

    }

    /*
     * Navigates to Coupons Claimed/Used/Expired items
     */
    private void couponsGallery(String type) {

        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            Intent navIntent = new Intent(activity,
                    CouponsClaimedUsedExpiredActivty.class);
            navIntent.putExtra(CommonConstants.TAB_SELECTED, type);
            activity.startActivity(navIntent);

        } else {
            Constants.SignUpAlert(CurrentSpecialsListActivity.this);
        }

    }


    private void menuNavHotdeals() {
        lastvisitProdIdProducts = 0;
        lastvisitProdIdLocation = 0;
        couponRadio.setVisibility(View.GONE);
        if ("false".equalsIgnoreCase(flagData.get(Constants.TAG_HOTDEALFLAG))) {
            findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
        }
        specialoffersListView.setAdapter(null);
        new RetHotdealsAsyncTask().execute();
        title.setText("Hot Deals");
        //navBottombar.setVisibility(View.VISIBLE);

    }

    private void menuNavCoupons() {
        lastvisitProdIdProducts = 0;
        lastvisitProdIdLocation = 0;
        couponRadio.setVisibility(View.GONE);
        specialoffersListView.setAdapter(null);
        new RetailerCoupon().execute();
        title.setText(activity.getString(R.string.coupons));
        //navBottombar.setVisibility(View.VISIBLE);

    }

    private void menuNavSpecials() {
        lastvisitProdIdProducts = 0;
        lastvisitProdIdLocation = 0;
        couponRadio.setVisibility(View.GONE);
        new SpecialOfferList().execute();
        specialoffersListView.setAdapter(null);
        title.setText("Specials");

    }


    private void menuNavSales() {
        lastvisitProdIdProducts = 0;
        lastvisitProdIdLocation = 0;
        couponRadio.setVisibility(View.GONE);
        new GetProductDetailAsyncTask().execute();
        specialoffersListView.setAdapter(null);
        title.setText("Sales");


    }

    ProgressDialog progDialog;
    int lastvisitProdIdLocation = 0, minProdIdLocation = 0;
    int lastvisitProdIdProducts = 0, minProdIdProducts = 0;

    // boolean nextPageProducts;
    // boolean nextPageLocation;
    boolean nextPage = false;

    public class HotDealsClaimedByLocation extends
            AsyncTask<String, Void, String> {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        JSONObject jsonObject = null;
        HashMap<String, String> categoryData = null;
        HashMap<String, String> retailerData = null;

        @SuppressWarnings("rawtypes")
        ArrayList hotdealsListbyLocations = new ArrayList<>();
        String result = "true";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                progDialog.dismiss();
                linearLayout.setVisibility(View.VISIBLE);

                hotDealClaimedRedeemExpiredListAdapter = new HotDealClaimRedeemExpiredListAdapter(
                        CurrentSpecialsListActivity.this, hotdealsListbyLocations);
                specialoffersListView
                        .setAdapter(hotDealClaimedRedeemExpiredListAdapter);
                findViewById(R.id.no_coupons_found).setVisibility(View.GONE);
                if ("true".equals(result)) {

                    if (nextPage) {

                        try {
                            specialoffersListView.removeFooterView(moreResultsView);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        // moreInfo.setVisibility(View.VISIBLE);

                        specialoffersListView.addFooterView(moreResultsView);

                    } else {
                        specialoffersListView.removeFooterView(moreResultsView);
                    }
                    specialoffersListView
                            .setAdapter(hotDealClaimedRedeemExpiredListAdapter);
                } else {
                    specialoffersListView.setAdapter(null);
                    findViewById(R.id.no_coupons_found).setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @SuppressWarnings("unchecked")
        @Override
        protected String doInBackground(String... params) {
            try {
                String urlParameters = mUrlRequestParams
                        .getHotDealsByLocationAndProducts("",
                                claimRedeemExpiredValue, mainmenuId,
                                lastvisitProdIdLocation + "", null);
                String get_hotdeals_bylocation = Properties.url_local_server
                        + Properties.hubciti_version + "gallery/gallhdbyloc";
                jsonObject = mServerConnections.getUrlPostResponse(
                        get_hotdeals_bylocation, urlParameters, true);
                if (jsonObject != null) {
                    try {

                        nextPage = "1".equalsIgnoreCase(jsonObject.getJSONObject(
                                "HotDealsListResultSet").getString("nextPage"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
                                "response").getString("responseCode"))) {
                            result = jsonObject.getJSONObject("response")
                                    .getString("responseText");
                        }
                    }

                    try {
                        if (jsonObject.getJSONObject("HotDealsListResultSet")
                                .has("maxRowNum")) {
                            if (Integer.valueOf(jsonObject.getJSONObject(
                                    "HotDealsListResultSet").getString(
                                    "maxRowNum")) > lastvisitProdIdLocation) {
                                lastvisitProdIdLocation = Integer
                                        .parseInt(jsonObject.getJSONObject(
                                                "HotDealsListResultSet")
                                                .getString("maxRowNum"));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (jsonObject.has("HotDealsListResultSet")) {
                        minProdIdLocation = lastvisitProdIdLocation;
                        JSONObject jsonHotDealsList = null;
                        JSONObject jsonRetDetailsList = null;

                        jsonRetDetailsList = jsonObject
                                .getJSONObject("HotDealsListResultSet")
                                .getJSONObject("retDetailsList")
                                .getJSONObject("RetailersDetails");
                        retailerData = new HashMap<>();
                        retailerData.put("itemName", "apiPartnerName");
                        retailerData.put("apiPartnerName",
                                jsonRetDetailsList.getString("retName"));
                        retailerData.put("apiPartnerId",
                                jsonRetDetailsList.getString("retId"));
                        hotdealsListbyLocations.add(retailerData);
                        try {

                            jsonHotDealsList = jsonObject
                                    .getJSONObject("HotDealsListResultSet")
                                    .getJSONObject("retDetailsList")
                                    .getJSONObject("RetailersDetails")
                                    .getJSONObject("retDetailsList")
                                    .getJSONObject("RetailersDetails");
                            categoryData = new HashMap<>();
                            categoryData.put("itemName", "categoryName");
                            categoryData.put("categoryName", jsonHotDealsList
                                    .getString("retailerAddress"));

                            hotdealsListbyLocations.add(categoryData);
                            try {
                                hotdealsListbyLocations
                                        .add(getJSONValuesLocation(jsonHotDealsList
                                                .getJSONObject("hDDetailsList")
                                                .getJSONObject(
                                                        "HotDealsDetails")));
                            } catch (Exception e) {
                                e.printStackTrace();
                                JSONArray jsonRetailerList = jsonHotDealsList
                                        .getJSONObject("hDDetailsList")
                                        .getJSONArray("HotDealsDetails");
                                for (int i = 0; i < jsonRetailerList.length(); i++) {
                                    JSONObject elem = jsonRetailerList
                                            .getJSONObject(i);
                                    hotdealsListbyLocations
                                            .add(getJSONValuesLocation(elem));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            JSONArray jsonRetailerArray = jsonObject
                                    .getJSONObject("HotDealsListResultSet")
                                    .getJSONObject("retDetailsList")
                                    .getJSONObject("RetailersDetails")
                                    .getJSONObject("retDetailsList")
                                    .getJSONArray("RetailersDetails");
                            for (int i = 0; i < jsonRetailerArray.length(); i++) {
                                JSONObject elem = jsonRetailerArray
                                        .getJSONObject(i);
                                categoryData = new HashMap<>();
                                categoryData.put("itemName", "categoryName");
                                categoryData.put("categoryName",
                                        elem.getString("retailerAddress"));
                                hotdealsListbyLocations.add(categoryData);
                                try {
                                    hotdealsListbyLocations
                                            .add(getJSONValuesLocation(jsonHotDealsList
                                                    .getJSONObject(
                                                            "hDDetailsList")
                                                    .getJSONObject(
                                                            "HotDealsDetails")));
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    try {
                                        JSONArray jsonRetailerList = elem
                                                .getJSONObject("hDDetailsList")
                                                .getJSONArray("HotDealsDetails");
                                        for (int ind = 0; ind < jsonRetailerList
                                                .length(); ind++) {
                                            JSONObject elemr = jsonRetailerList
                                                    .getJSONObject(ind);
                                            hotdealsListbyLocations
                                                    .add(getJSONValuesLocation(elemr));
                                        }
                                    } catch (Exception ed) {
                                        ed.printStackTrace();
                                        JSONObject jsonRetailerList = elem
                                                .getJSONObject("hDDetailsList")
                                                .getJSONObject(
                                                        "HotDealsDetails");
                                        hotdealsListbyLocations
                                                .add(getJSONValuesLocation(jsonRetailerList));
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                result = "false";
            }

            return result;
        }
    }

    private class FindCoupons extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;

        HashMap<String, String> hotData = null;
        // private ProgressDialog mDialog;

        UrlRequestParams objUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            String result = "false";
            minProdId = lastvisitProdIdProducts;

            if (radioProd.isChecked()) {
                result = parseClaimedExpiredUsedbyProd();
            } else if (radioLoc.isChecked()) {
                result = parseJSONClaimUsedExpByLoc();
            }
            return result;
        }

        public boolean isJSONArray(JSONObject jsonObject, String value) {
            boolean isArray = false;

            JSONObject isJSONObject = jsonObject.optJSONObject(value);
            if (isJSONObject == null) {
                JSONArray isJSONArray = jsonObject.optJSONArray(value);
                if (isJSONArray != null) {
                    isArray = true;
                }
            }

            return isArray;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progDialog.dismiss();
                linearLayout.setVisibility(View.VISIBLE);

                findViewById(R.id.no_coupons_found).setVisibility(View.GONE);
                specialoffersListView.setAdapter(null);
                if (radioLoc.isChecked()) {
                    if ("true".equals(result)) {
                        Parcelable state = specialoffersListView
                                .onSaveInstanceState();
                        couponsListAdapter = new CouponClaimRedeemExpiredListAdapter1(
                                CurrentSpecialsListActivity.this, specialoffersList);
                        specialoffersListView.setAdapter(couponsListAdapter);
                        specialoffersListView.onRestoreInstanceState(state);

                    } else {
                        specialoffersListView.setAdapter(null);
                        findViewById(R.id.no_coupons_found).setVisibility(
                                View.VISIBLE);
                    }
                } else if (radioProd.isChecked()) {
                    if ("true".equals(result)) {
                        Parcelable state = specialoffersListView
                                .onSaveInstanceState();
                        couponsListAdapter = new CouponClaimRedeemExpiredListAdapter1(
                                CurrentSpecialsListActivity.this, specialoffersList);
                        specialoffersListView.setAdapter(couponsListAdapter);
                        specialoffersListView.onRestoreInstanceState(state);

                    } else {
                        specialoffersListView.setAdapter(null);
                        findViewById(R.id.no_coupons_found).setVisibility(
                                View.VISIBLE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public String parseClaimedExpiredUsedbyProd() {

            String result = "false";
            try {
                minProdId = lastvisitProdIdProducts;

                String urlParameters = objUrlRequestParams
                        .getCoupnClaimRedeemExpired(claimRedeemExpiredValue,
                                String.valueOf(lastvisitProdIdProducts), null);
                String url_get_clm_use_exp_coup_by_prod = Properties.url_local_server
                        + Properties.hubciti_version + "gallery/gallcoupbyprod";
                jsonObject = mServerConnections.getUrlPostResponse(
                        url_get_clm_use_exp_coup_by_prod, urlParameters, true);

                if (jsonObject != null) {
                    try {
                        nextPage = "1".equalsIgnoreCase(jsonObject.getJSONObject(
                                "CouponsDetails").getString("nextPageFlag"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
                                "response").getString("responseCode"))) {
                            result = jsonObject.getJSONObject("response")
                                    .getString("responseText");

                        }
                    }

                    JSONObject jsonCoupons = null;
                    if (jsonObject.has("CouponsDetails")) {
                        jsonCoupons = jsonObject
                                .getJSONObject("CouponsDetails");
                    } else if (jsonObject.has("CouponDetails")) {
                        jsonCoupons = jsonObject.getJSONObject("CouponDetails");
                    }

                    JSONArray jsonCategoryListArray = null;

                    maxCount = jsonCoupons.getInt("maxCnt");
                    maxRowNum = jsonCoupons.getInt("maxRowNum");

                    boolean isCategoryArray = isJSONArray(
                            jsonCoupons.getJSONObject("categoryInfoList"),
                            "CategoryInfo");
                    if (isCategoryArray) {
                        jsonCategoryListArray = jsonCoupons.getJSONObject(
                                "categoryInfoList")
                                .getJSONArray("CategoryInfo");
                        for (int index = 0; index < jsonCategoryListArray
                                .length(); index++) {
                            JSONObject jsonCategory = jsonCategoryListArray
                                    .getJSONObject(index);
                            specialOfferData = new HashMap<>();
                            specialOfferData.put("itemName", "category");
                            if (jsonCategory.has("categoryName")) {
                                specialOfferData
                                        .put(CommonConstants.ALLCOUPONSCOUPONCATNAME,
                                                jsonCategory
                                                        .getString("categoryName"));
                            }
                            if (jsonCategory.has("categoryId")) {
                                specialOfferData.put(
                                        CommonConstants.ALLCOUPONSCATID,
                                        jsonCategory.getString("categoryId"));
                            }
                            specialoffersList.add(specialOfferData);
                            boolean isCouponDetailsArray = isJSONArray(
                                    jsonCategory
                                            .getJSONObject("couponDetailsList"),
                                    "CouponDetails");
                            if (isCouponDetailsArray) {
                                JSONArray jsonCouponListArray = jsonCategory
                                        .getJSONObject("couponDetailsList")
                                        .getJSONArray("CouponDetails");
                                for (int couIndex = 0; couIndex < jsonCouponListArray
                                        .length(); couIndex++) {
                                    JSONObject jsonCoupon = jsonCouponListArray
                                            .getJSONObject(couIndex);
                                    if (jsonCoupon != null) {
                                        specialOfferData = new HashMap<>();
                                        specialOfferData.put("itemName",
                                                "coupons");
                                        specialOfferData
                                                .put(CommonConstants.ALLCOUPONSCATID,
                                                        jsonCategory
                                                                .getString("categoryId"));
                                        if (jsonCoupon.has("couponName")) {
                                            specialOfferData
                                                    .put("couponName",
                                                            jsonCoupon
                                                                    .getString("couponName"));
                                        }
                                        if (jsonCoupon
                                                .has("couponDiscountAmount")) {
                                            specialOfferData
                                                    .put("couponDiscountAmount",
                                                            jsonCoupon
                                                                    .getString("couponDiscountAmount"));
                                        }
                                        if (jsonCoupon.has("couponURL")) {
                                            specialOfferData
                                                    .put("couponURL",
                                                            jsonCoupon
                                                                    .getString("couponURL"));
                                        }
                                        if (jsonCoupon.has("couponExpireDate")) {
                                            specialOfferData
                                                    .put("couponExpireDate",
                                                            jsonCoupon
                                                                    .getString("couponExpireDate"));
                                        }
                                        if (jsonCoupon.has("coupDesc")) {
                                            specialOfferData
                                                    .put("coupDesc",
                                                            jsonCoupon
                                                                    .getString("coupDesc"));
                                        }
                                        if (jsonCoupon.has("couponDiscountPct")) {
                                            specialOfferData
                                                    .put("couponDiscountPct",
                                                            jsonCoupon
                                                                    .getString("couponDiscountPct"));
                                        }
                                        if (jsonCoupon.has("couponStartDate")) {
                                            specialOfferData
                                                    .put("couponStartDate",
                                                            jsonCoupon
                                                                    .getString("couponStartDate"));
                                        }
                                        if (jsonCoupon.has("couponImagePath")) {
                                            specialOfferData
                                                    .put("couponImagePath",
                                                            jsonCoupon
                                                                    .getString("couponImagePath"));
                                        }

                                        if (jsonCoupon.has("couponNewFlag")) {
                                            specialOfferData.put(
                                                    "couponNewFlag",
                                                    jsonCoupon
                                                            .getInt("newFlag")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("couponId")) {
                                            specialOfferData.put(
                                                    "couponId",
                                                    jsonCoupon
                                                            .getInt("couponId")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("usedFlag")) {
                                            specialOfferData.put(
                                                    "couponUsed",
                                                    jsonCoupon
                                                            .getInt("usedFlag")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("couponListId")) {
                                            specialOfferData
                                                    .put("couponListId",
                                                            jsonCoupon
                                                                    .getInt("userCoupGallId")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("redeemFlag")) {
                                            specialOfferData
                                                    .put("redeemFlag",
                                                            jsonCoupon
                                                                    .getInt("redeemFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("claimFlag")) {
                                            specialOfferData
                                                    .put("claimFlag",
                                                            jsonCoupon
                                                                    .getInt("claimFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("coupDistance")) {
                                            specialOfferData
                                                    .put("coupDistance",
                                                            jsonCoupon
                                                                    .getDouble("distance")
                                                                    + "");
                                        }
                                        specialoffersList.add(specialOfferData);
                                    }
                                }
                            } else {
                                JSONObject jsonCoupon = jsonCategory
                                        .getJSONObject("couponDetailsList")
                                        .getJSONObject("CouponDetails");
                                if (jsonCoupon != null) {
                                    specialOfferData = new HashMap<>();
                                    specialOfferData.put("itemName", "coupons");
                                    if (jsonCoupon.has("couponName")) {
                                        specialOfferData
                                                .put("couponName",
                                                        jsonCoupon
                                                                .getString("couponName"));
                                    }
                                    if (jsonCoupon.has("couponDiscountAmount")) {
                                        specialOfferData
                                                .put("couponDiscountAmount",
                                                        jsonCoupon
                                                                .getString("couponDiscountAmount"));
                                    }
                                    if (jsonCoupon.has("couponURL")) {
                                        specialOfferData
                                                .put("couponURL", jsonCoupon
                                                        .getString("couponURL"));
                                    }
                                    if (jsonCoupon.has("couponExpireDate")) {
                                        specialOfferData
                                                .put("couponExpireDate",
                                                        jsonCoupon
                                                                .getString("couponExpireDate"));
                                    }
                                    if (jsonCoupon.has("coupDesc")) {
                                        specialOfferData.put("coupDesc",
                                                jsonCoupon
                                                        .getString("coupDesc"));
                                    }
                                    if (jsonCoupon.has("couponDiscountPct")) {
                                        specialOfferData
                                                .put("couponDiscountPct",
                                                        jsonCoupon
                                                                .getString("couponDiscountPct"));
                                    }
                                    if (jsonCoupon.has("couponStartDate")) {
                                        specialOfferData
                                                .put("couponStartDate",
                                                        jsonCoupon
                                                                .getString("couponStartDate"));
                                    }
                                    if (jsonCoupon.has("couponImagePath")) {
                                        specialOfferData
                                                .put("couponImagePath",
                                                        jsonCoupon
                                                                .getString("couponImagePath"));
                                    }

                                    if (jsonCoupon.has("couponNewFlag")) {
                                        specialOfferData.put("couponNewFlag",
                                                jsonCoupon.getInt("newFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("couponId")) {
                                        specialOfferData.put("couponId",
                                                jsonCoupon.getInt("couponId")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("usedFlag")) {
                                        specialOfferData.put("couponUsed",
                                                jsonCoupon.getInt("usedFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("userCoupGallId")) {
                                        specialOfferData
                                                .put("couponListId",
                                                        jsonCoupon
                                                                .getInt("userCoupGallId")
                                                                + "");
                                    }
                                    if (jsonCoupon.has("redeemFlag")) {
                                        specialOfferData.put("redeemFlag",
                                                jsonCoupon.getInt("redeemFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("claimFlag")) {
                                        specialOfferData.put("claimFlag",
                                                jsonCoupon.getInt("claimFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("coupDistance")) {
                                        specialOfferData.put(
                                                "coupDistance",
                                                jsonCoupon
                                                        .getDouble("distance")
                                                        + "");
                                    }
                                    specialoffersList.add(specialOfferData);
                                }
                            }
                        }

                    } else {

                        JSONObject jsonCategory = jsonCoupons.getJSONObject(
                                "categoryInfoList").getJSONObject(
                                "CategoryInfo");
                        specialOfferData = new HashMap<>();
                        specialOfferData.put("itemName", "category");
                        if (jsonCategory.has("categoryName")) {
                            specialOfferData.put(
                                    CommonConstants.ALLCOUPONSCOUPONCATNAME,
                                    jsonCategory.getString("categoryName"));
                        }
                        if (jsonCategory.has("categoryId")) {
                            specialOfferData.put(
                                    CommonConstants.ALLCOUPONSCATID,
                                    jsonCategory.getString("categoryId"));
                        }
                        specialoffersList.add(specialOfferData);
                        boolean isCouponDetailsArray = isJSONArray(
                                jsonCategory.getJSONObject("couponDetailsList"),
                                "CouponDetails");
                        if (isCouponDetailsArray) {
                            JSONArray jsonCouponListArray = jsonCategory
                                    .getJSONObject("couponDetailsList")
                                    .getJSONArray("CouponDetails");
                            for (int couIndex = 0; couIndex < jsonCouponListArray
                                    .length(); couIndex++) {
                                JSONObject jsonCoupon = jsonCouponListArray
                                        .getJSONObject(couIndex);
                                if (jsonCoupon != null) {
                                    specialOfferData = new HashMap<>();
                                    specialOfferData.put("itemName", "coupons");
                                    if (jsonCoupon.has("couponName")) {
                                        specialOfferData
                                                .put("couponName",
                                                        jsonCoupon
                                                                .getString("couponName"));
                                    }
                                    if (jsonCoupon.has("couponDiscountAmount")) {
                                        specialOfferData
                                                .put("couponDiscountAmount",
                                                        jsonCoupon
                                                                .getString("couponDiscountAmount"));
                                    }
                                    if (jsonCoupon.has("couponURL")) {
                                        specialOfferData
                                                .put("couponURL", jsonCoupon
                                                        .getString("couponURL"));
                                    }
                                    if (jsonCoupon.has("couponExpireDate")) {
                                        specialOfferData
                                                .put("couponExpireDate",
                                                        jsonCoupon
                                                                .getString("couponExpireDate"));
                                    }
                                    if (jsonCoupon.has("coupDesc")) {
                                        specialOfferData.put("coupDesc",
                                                jsonCoupon
                                                        .getString("coupDesc"));
                                    }
                                    if (jsonCoupon.has("couponDiscountPct")) {
                                        specialOfferData
                                                .put("couponDiscountPct",
                                                        jsonCoupon
                                                                .getString("couponDiscountPct"));
                                    }
                                    if (jsonCoupon.has("couponStartDate")) {
                                        specialOfferData
                                                .put("couponStartDate",
                                                        jsonCoupon
                                                                .getString("couponStartDate"));
                                    }
                                    if (jsonCoupon.has("couponImagePath")) {
                                        specialOfferData
                                                .put("couponImagePath",
                                                        jsonCoupon
                                                                .getString("couponImagePath"));
                                    }

                                    if (jsonCoupon.has("couponNewFlag")) {
                                        specialOfferData.put("couponNewFlag",
                                                jsonCoupon.getInt("newFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("couponId")) {
                                        specialOfferData.put("couponId",
                                                jsonCoupon.getInt("couponId")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("usedFlag")) {
                                        specialOfferData.put("couponUsed",
                                                jsonCoupon.getInt("usedFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("userCoupGallId")) {
                                        specialOfferData
                                                .put("couponListId",
                                                        jsonCoupon
                                                                .getInt("userCoupGallId")
                                                                + "");
                                    }
                                    if (jsonCoupon.has("redeemFlag")) {
                                        specialOfferData.put("redeemFlag",
                                                jsonCoupon.getInt("redeemFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("claimFlag")) {
                                        specialOfferData.put("claimFlag",
                                                jsonCoupon.getInt("claimFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("coupDistance")) {
                                        specialOfferData.put(
                                                "coupDistance",
                                                jsonCoupon
                                                        .getDouble("distance")
                                                        + "");
                                    }
                                    specialoffersList.add(specialOfferData);
                                }
                            }
                        } else {
                            JSONObject jsonCoupon = jsonCategory.getJSONObject(
                                    "couponDetailsList").getJSONObject(
                                    "CouponDetails");
                            if (jsonCoupon != null) {
                                specialOfferData = new HashMap<>();
                                specialOfferData.put("itemName", "coupons");
                                if (jsonCoupon.has("couponName")) {
                                    specialOfferData.put("couponName",
                                            jsonCoupon.getString("couponName"));
                                }
                                if (jsonCoupon.has("couponDiscountAmount")) {
                                    specialOfferData
                                            .put("couponDiscountAmount",
                                                    jsonCoupon
                                                            .getString("couponDiscountAmount"));
                                }
                                if (jsonCoupon.has("couponURL")) {
                                    specialOfferData.put("couponURL",
                                            jsonCoupon.getString("couponURL"));
                                }
                                if (jsonCoupon.has("couponExpireDate")) {
                                    specialOfferData
                                            .put("couponExpireDate",
                                                    jsonCoupon
                                                            .getString("couponExpireDate"));
                                }
                                if (jsonCoupon.has("coupDesc")) {
                                    specialOfferData.put("coupDesc",
                                            jsonCoupon.getString("coupDesc"));
                                }
                                if (jsonCoupon.has("couponDiscountPct")) {
                                    specialOfferData
                                            .put("couponDiscountPct",
                                                    jsonCoupon
                                                            .getString("couponDiscountPct"));
                                }
                                if (jsonCoupon.has("couponStartDate")) {
                                    specialOfferData
                                            .put("couponStartDate",
                                                    jsonCoupon
                                                            .getString("couponStartDate"));
                                }
                                if (jsonCoupon.has("couponImagePath")) {
                                    specialOfferData
                                            .put("couponImagePath",
                                                    jsonCoupon
                                                            .getString("couponImagePath"));
                                }

                                if (jsonCoupon.has("couponNewFlag")) {
                                    specialOfferData.put("couponNewFlag",
                                            jsonCoupon.getInt("newFlag") + "");
                                }
                                if (jsonCoupon.has("couponId")) {
                                    specialOfferData.put("couponId",
                                            jsonCoupon.getInt("couponId") + "");
                                }
                                if (jsonCoupon.has("usedFlag")) {
                                    specialOfferData.put("couponUsed",
                                            jsonCoupon.getInt("usedFlag") + "");
                                }
                                if (jsonCoupon.has("userCoupGallId")) {
                                    specialOfferData.put("couponListId",
                                            jsonCoupon.getInt("userCoupGallId")
                                                    + "");
                                }
                                if (jsonCoupon.has("redeemFlag")) {
                                    specialOfferData.put("redeemFlag",
                                            jsonCoupon.getInt("redeemFlag")
                                                    + "");
                                }
                                if (jsonCoupon.has("claimFlag")) {
                                    specialOfferData
                                            .put("claimFlag",
                                                    jsonCoupon
                                                            .getInt("claimFlag")
                                                            + "");
                                }
                                if (jsonCoupon.has("coupDistance")) {
                                    specialOfferData.put("coupDistance",
                                            jsonCoupon.getDouble("distance")
                                                    + "");
                                }
                                specialoffersList.add(specialOfferData);
                            }
                        }

                    }
                    if (maxCount > lastvisitProdIdProducts) {
                        lastvisitProdIdProducts = maxRowNum;
                    }
                    if (nextPage) {
                        // nextPage = false;
                        hotData = new HashMap<>();
                        hotData.put("itemName", "viewMore");
                        hotData.put(
                                CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
                                maxRowNum + "");
                        specialoffersList.add(hotData);
                    }
                    result = "true";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;

        }

        public String parseJSONClaimUsedExpByLoc() {

            minProdId = lastvisitProdIdProducts;
            String result = "false";
            try {

                String urlParameters = objUrlRequestParams
                        .getCoupnClaimRedeemExpired(claimRedeemExpiredValue,
                                String.valueOf(lastvisitProdIdProducts), null);
                String url_get_clm_use_exp_coup_by_loc = Properties.url_local_server
                        + Properties.hubciti_version + "gallery/gallcoupbyloc";
                jsonObject = mServerConnections.getUrlPostResponse(
                        url_get_clm_use_exp_coup_by_loc, urlParameters, true);

                if (jsonObject != null) {

                    try {
                        nextPage = "1".equalsIgnoreCase(jsonObject.getJSONObject(
                                "CouponsDetails").getString("nextPageFlag"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
                                "response").getString("responseCode"))) {
                            result = jsonObject.getJSONObject("response")
                                    .getString("responseText");
                            result = "false";
                            return result;
                        }
                    }

                    JSONObject jsonCoupons = null;
                    if (jsonObject.has("CouponsDetails")) {
                        jsonCoupons = jsonObject
                                .getJSONObject("CouponsDetails");
                    } else if (jsonObject.has("CouponDetails")) {
                        jsonCoupons = jsonObject.getJSONObject("CouponDetails");
                    }

                    if (jsonCoupons.has("maxCnt")) {
                        maxCount = jsonCoupons.getInt("maxCnt");
                    }

                    if (jsonCoupons.has("maxRowNum")) {
                        maxRowNum = jsonCoupons.getInt("maxRowNum");
                    }

                    JSONObject jsonretDetailsList = null;
                    JSONArray jsonretDetailsListArray = null;

                    boolean isRetailersArray = isJSONArray(
                            jsonCoupons.getJSONObject("retDetailsList"),
                            "RetailersDetails");
                    if (isRetailersArray) {
                        jsonretDetailsListArray = jsonCoupons.getJSONObject(
                                "retDetailsList").getJSONArray(
                                "RetailersDetails");

                        for (int index = 0; index < jsonretDetailsListArray
                                .length(); index++) {
                            JSONObject jsonRetdetailsObj = jsonretDetailsListArray
                                    .getJSONObject(index).getJSONObject(
                                            "retDetailsList");
                            int retId = jsonretDetailsListArray.getJSONObject(
                                    index).getInt("retId");
                            String retname = jsonretDetailsListArray
                                    .getJSONObject(index).getString("retName");
                            specialOfferData = new HashMap<>();
                            specialOfferData.put("itemName", "category");
                            if (jsonretDetailsListArray.getJSONObject(index)
                                    .has("retId")) {
                                specialOfferData.put(
                                        CommonConstants.ALLCOUPONSCATID, retId
                                                + "");
                            }
                            specialOfferData.put("cateName", retname);
                            specialoffersList.add(specialOfferData);

                            JSONArray jsonRetailerArray = null;
                            boolean isRetailerjsonArray = isJSONArray(
                                    jsonRetdetailsObj, "RetailersDetails");
                            if (isRetailerjsonArray) {
                                jsonRetailerArray = jsonRetdetailsObj
                                        .getJSONArray("RetailersDetails");
                                for (int retIndex = 0; retIndex < jsonRetailerArray
                                        .length(); retIndex++) {
                                    JSONObject jsonRetailer = jsonRetailerArray
                                            .getJSONObject(retIndex);

                                    specialOfferData = new HashMap<>();
                                    specialOfferData
                                            .put("itemName", "location");
                                    if (jsonRetailer.has("postalCode")) {
                                        specialOfferData.put(
                                                "postalCode",
                                                jsonRetailer
                                                        .getInt("postalCode")
                                                        + "");
                                    }
                                    if (jsonRetailer.has("minRetDist")) {
                                        specialOfferData
                                                .put("minRetDist",
                                                        jsonRetailer
                                                                .getDouble("minRetDist")
                                                                + "");
                                    }
                                    if (jsonRetailer.has("retailerAddress")) {
                                        specialOfferData
                                                .put(CommonConstants.ALLCOUPONSLOCATION,
                                                        jsonRetailer
                                                                .getString("retailerAddress"));
                                    }
                                    if (jsonRetailer.has("retLocId")) {
                                        specialOfferData.put("retLocId",
                                                jsonRetailer.getInt("retLocId")
                                                        + "");
                                    }
                                    if (jsonRetailer.has("city")) {
                                        specialOfferData.put("city",
                                                jsonRetailer.getString("city"));
                                    }
                                    specialoffersList.add(specialOfferData);

                                    boolean isArrayCouponDetails = isJSONArray(
                                            jsonRetailer
                                                    .getJSONObject("couponDetailsList"),
                                            "CouponDetails");
                                    JSONArray jsonCouponDetailsListArray = null;

                                    if (isArrayCouponDetails) {
                                        jsonCouponDetailsListArray = jsonRetailer
                                                .getJSONObject(
                                                        "couponDetailsList")
                                                .getJSONArray("CouponDetails");
                                        for (int couponIndx = 0; couponIndx < jsonCouponDetailsListArray
                                                .length(); couponIndx++) {
                                            JSONObject jsonCoupon = jsonCouponDetailsListArray
                                                    .getJSONObject(couponIndx);
                                            if (jsonCoupon != null) {
                                                specialOfferData = new HashMap<>();
                                                specialOfferData.put(
                                                        "itemName", "coupons");
                                                specialOfferData
                                                        .put(CommonConstants.ALLCOUPONSCATID,
                                                                jsonretDetailsListArray
                                                                        .getJSONObject(
                                                                                index)
                                                                        .getInt("retId")
                                                                        + "");
                                                if (jsonCoupon
                                                        .has("couponName")) {
                                                    specialOfferData
                                                            .put("couponName",
                                                                    jsonCoupon
                                                                            .getString("couponName"));
                                                }
                                                if (jsonCoupon
                                                        .has("couponDiscountAmount")) {
                                                    specialOfferData
                                                            .put("couponDiscountAmount",
                                                                    jsonCoupon
                                                                            .getString("couponDiscountAmount"));
                                                }
                                                if (jsonCoupon.has("couponURL")) {
                                                    specialOfferData
                                                            .put("couponURL",
                                                                    jsonCoupon
                                                                            .getString("couponURL"));
                                                }
                                                if (jsonCoupon
                                                        .has("couponExpireDate")) {
                                                    specialOfferData
                                                            .put("couponExpireDate",
                                                                    jsonCoupon
                                                                            .getString("couponExpireDate"));
                                                }
                                                if (jsonCoupon.has("coupDesc")) {
                                                    specialOfferData
                                                            .put("coupDesc",
                                                                    jsonCoupon
                                                                            .getString("coupDesc"));
                                                }
                                                if (jsonCoupon
                                                        .has("couponDiscountPct")) {
                                                    specialOfferData
                                                            .put("couponDiscountPct",
                                                                    jsonCoupon
                                                                            .getString("couponDiscountPct"));
                                                }
                                                if (jsonCoupon
                                                        .has("couponStartDate")) {
                                                    specialOfferData
                                                            .put("couponStartDate",
                                                                    jsonCoupon
                                                                            .getString("couponStartDate"));
                                                }
                                                if (jsonCoupon
                                                        .has("couponImagePath")) {
                                                    specialOfferData
                                                            .put("couponImagePath",
                                                                    jsonCoupon
                                                                            .getString("couponImagePath"));
                                                }

                                                if (jsonCoupon
                                                        .has("couponNewFlag")) {
                                                    specialOfferData
                                                            .put(CommonConstants.ALLCOUPONSNEWFLAG,
                                                                    jsonCoupon
                                                                            .getInt("newFlag")
                                                                            + "");
                                                }
                                                if (jsonCoupon.has("couponId")) {
                                                    specialOfferData
                                                            .put("couponId",
                                                                    jsonCoupon
                                                                            .getInt("couponId")
                                                                            + "");
                                                }
                                                if (jsonCoupon.has("used")) {
                                                    specialOfferData
                                                            .put("couponUsed",
                                                                    jsonCoupon
                                                                            .getInt("used")
                                                                            + "");
                                                }
                                                if (jsonCoupon
                                                        .has("couponListId")) {
                                                    specialOfferData
                                                            .put("couponListId",
                                                                    jsonCoupon
                                                                            .getInt("couponListId")
                                                                            + "");
                                                }
                                                if (jsonCoupon
                                                        .has("redeemFlag")) {
                                                    specialOfferData
                                                            .put("redeemFlag",
                                                                    jsonCoupon
                                                                            .getInt("redeemFlag")
                                                                            + "");
                                                }
                                                if (jsonCoupon.has("claimFlag")) {
                                                    specialOfferData
                                                            .put("claimFlag",
                                                                    jsonCoupon
                                                                            .getInt("claimFlag")
                                                                            + "");
                                                }
                                                if (jsonCoupon
                                                        .has("coupDistance")) {
                                                    specialOfferData
                                                            .put("coupDistance",
                                                                    jsonCoupon
                                                                            .getDouble("distance")
                                                                            + "");
                                                }
                                                specialoffersList
                                                        .add(specialOfferData);
                                            }
                                        }
                                    } else {
                                        JSONObject jsonCoupon = jsonRetailer
                                                .getJSONObject(
                                                        "couponDetailsList")
                                                .getJSONObject("CouponDetails");
                                        if (jsonCoupon != null) {
                                            specialOfferData = new HashMap<>();
                                            specialOfferData.put("itemName",
                                                    "coupons");
                                            if (jsonCoupon.has("couponName")) {
                                                specialOfferData
                                                        .put("couponName",
                                                                jsonCoupon
                                                                        .getString("couponName"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponDiscountAmount")) {
                                                specialOfferData
                                                        .put("couponDiscountAmount",
                                                                jsonCoupon
                                                                        .getString("couponDiscountAmount"));
                                            }
                                            if (jsonCoupon.has("couponURL")) {
                                                specialOfferData
                                                        .put("couponURL",
                                                                jsonCoupon
                                                                        .getString("couponURL"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponExpireDate")) {
                                                specialOfferData
                                                        .put("couponExpireDate",
                                                                jsonCoupon
                                                                        .getString("couponExpireDate"));
                                            }
                                            if (jsonCoupon.has("coupDesc")) {
                                                specialOfferData
                                                        .put("coupDesc",
                                                                jsonCoupon
                                                                        .getString("coupDesc"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponDiscountPct")) {
                                                specialOfferData
                                                        .put("couponDiscountPct",
                                                                jsonCoupon
                                                                        .getString("couponDiscountPct"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponStartDate")) {
                                                specialOfferData
                                                        .put("couponStartDate",
                                                                jsonCoupon
                                                                        .getString("couponStartDate"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponImagePath")) {
                                                specialOfferData
                                                        .put("couponImagePath",
                                                                jsonCoupon
                                                                        .getString("couponImagePath"));
                                            }

                                            if (jsonCoupon.has("couponNewFlag")) {
                                                specialOfferData
                                                        .put(CommonConstants.ALLCOUPONSNEWFLAG,
                                                                jsonCoupon
                                                                        .getInt("newFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("couponId")) {
                                                specialOfferData
                                                        .put("couponId",
                                                                jsonCoupon
                                                                        .getInt("couponId")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("couponUsed")) {
                                                specialOfferData.put(
                                                        "couponUsed",
                                                        jsonCoupon
                                                                .getInt("used")
                                                                + "");
                                            }
                                            if (jsonCoupon.has("couponListId")) {
                                                specialOfferData
                                                        .put("couponListId",
                                                                jsonCoupon
                                                                        .getInt("couponListId")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("redeemFlag")) {
                                                specialOfferData
                                                        .put("redeemFlag",
                                                                jsonCoupon
                                                                        .getInt("redeemFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("claimFlag")) {
                                                specialOfferData
                                                        .put("claimFlag",
                                                                jsonCoupon
                                                                        .getInt("claimFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("coupDistance")) {
                                                specialOfferData
                                                        .put("coupDistance",
                                                                jsonCoupon
                                                                        .getDouble("distance")
                                                                        + "");
                                            }
                                            specialoffersList
                                                    .add(specialOfferData);
                                        }

                                    }
                                }

                            } else {

                                JSONObject jsonRetailer = jsonRetdetailsObj
                                        .getJSONObject("RetailersDetails");
                                specialOfferData = new HashMap<>();
                                specialOfferData.put("itemName", "location");
                                if (jsonRetailer.has("postalCode")) {
                                    specialOfferData.put("postalCode",
                                            jsonRetailer.getInt("postalCode")
                                                    + "");
                                }
                                if (jsonRetailer.has("minRetDist")) {
                                    specialOfferData.put(
                                            "minRetDist",
                                            jsonRetailer
                                                    .getDouble("minRetDist")
                                                    + "");
                                }
                                if (jsonRetailer.has("retailerAddress")) {
                                    specialOfferData
                                            .put(CommonConstants.ALLCOUPONSLOCATION,
                                                    jsonRetailer
                                                            .getString("retailerAddress"));
                                }
                                if (jsonRetailer.has("retLocId")) {
                                    specialOfferData.put("retLocId",
                                            jsonRetailer.getInt("retLocId")
                                                    + "");
                                }
                                if (jsonRetailer.has("city")) {
                                    specialOfferData.put("city",
                                            jsonRetailer.getString("city"));
                                }
                                specialoffersList.add(specialOfferData);

                                boolean isArrayCouponDetails = isJSONArray(
                                        jsonRetailer
                                                .getJSONObject("couponDetailsList"),
                                        "CouponDetails");
                                JSONArray jsonCouponDetailsListArray = null;

                                if (isArrayCouponDetails) {
                                    jsonCouponDetailsListArray = jsonRetailer
                                            .getJSONObject("couponDetailsList")
                                            .getJSONArray("CouponDetails");
                                    for (int couponIndx = 0; couponIndx < jsonCouponDetailsListArray
                                            .length(); couponIndx++) {
                                        JSONObject jsonCoupon = jsonCouponDetailsListArray
                                                .getJSONObject(couponIndx);
                                        if (jsonCoupon != null) {
                                            specialOfferData = new HashMap<>();
                                            specialOfferData.put("itemName",
                                                    "coupons");
                                            if (jsonCoupon.has("couponName")) {
                                                specialOfferData
                                                        .put("couponName",
                                                                jsonCoupon
                                                                        .getString("couponName"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponDiscountAmount")) {
                                                specialOfferData
                                                        .put("couponDiscountAmount",
                                                                jsonCoupon
                                                                        .getString("couponDiscountAmount"));
                                            }
                                            if (jsonCoupon.has("couponURL")) {
                                                specialOfferData
                                                        .put("couponURL",
                                                                jsonCoupon
                                                                        .getString("couponURL"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponExpireDate")) {
                                                specialOfferData
                                                        .put("couponExpireDate",
                                                                jsonCoupon
                                                                        .getString("couponExpireDate"));
                                            }
                                            if (jsonCoupon.has("coupDesc")) {
                                                specialOfferData
                                                        .put("coupDesc",
                                                                jsonCoupon
                                                                        .getString("coupDesc"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponDiscountPct")) {
                                                specialOfferData
                                                        .put("couponDiscountPct",
                                                                jsonCoupon
                                                                        .getString("couponDiscountPct"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponStartDate")) {
                                                specialOfferData
                                                        .put("couponStartDate",
                                                                jsonCoupon
                                                                        .getString("couponStartDate"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponImagePath")) {
                                                specialOfferData
                                                        .put("couponImagePath",
                                                                jsonCoupon
                                                                        .getString("couponImagePath"));
                                            }

                                            if (jsonCoupon.has("couponNewFlag")) {
                                                specialOfferData
                                                        .put(CommonConstants.ALLCOUPONSNEWFLAG,
                                                                jsonCoupon
                                                                        .getInt("newFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("couponId")) {
                                                specialOfferData
                                                        .put("couponId",
                                                                jsonCoupon
                                                                        .getInt("couponId")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("couponUsed")) {
                                                specialOfferData.put(
                                                        "couponUsed",
                                                        jsonCoupon
                                                                .getInt("used")
                                                                + "");
                                            }
                                            if (jsonCoupon.has("couponListId")) {
                                                specialOfferData
                                                        .put("couponListId",
                                                                jsonCoupon
                                                                        .getInt("couponListId")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("redeemFlag")) {
                                                specialOfferData
                                                        .put("redeemFlag",
                                                                jsonCoupon
                                                                        .getInt("redeemFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("claimFlag")) {
                                                specialOfferData
                                                        .put("claimFlag",
                                                                jsonCoupon
                                                                        .getInt("claimFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("coupDistance")) {
                                                specialOfferData
                                                        .put("coupDistance",
                                                                jsonCoupon
                                                                        .getDouble("distance")
                                                                        + "");
                                            }
                                            specialoffersList
                                                    .add(specialOfferData);
                                        }
                                    }
                                } else {
                                    JSONObject jsonCoupon = jsonRetailer
                                            .getJSONObject("couponDetailsList")
                                            .getJSONObject("CouponDetails");
                                    if (jsonCoupon != null) {
                                        specialOfferData = new HashMap<>();
                                        specialOfferData.put("itemName",
                                                "coupons");
                                        if (jsonCoupon.has("couponName")) {
                                            specialOfferData
                                                    .put("couponName",
                                                            jsonCoupon
                                                                    .getString("couponName"));
                                        }
                                        if (jsonCoupon
                                                .has("couponDiscountAmount")) {
                                            specialOfferData
                                                    .put("couponDiscountAmount",
                                                            jsonCoupon
                                                                    .getString("couponDiscountAmount"));
                                        }
                                        if (jsonCoupon.has("couponURL")) {
                                            specialOfferData
                                                    .put("couponURL",
                                                            jsonCoupon
                                                                    .getString("couponURL"));
                                        }
                                        if (jsonCoupon.has("couponExpireDate")) {
                                            specialOfferData
                                                    .put("couponExpireDate",
                                                            jsonCoupon
                                                                    .getString("couponExpireDate"));
                                        }
                                        if (jsonCoupon.has("coupDesc")) {
                                            specialOfferData
                                                    .put("coupDesc",
                                                            jsonCoupon
                                                                    .getString("coupDesc"));
                                        }
                                        if (jsonCoupon.has("couponDiscountPct")) {
                                            specialOfferData
                                                    .put("couponDiscountPct",
                                                            jsonCoupon
                                                                    .getString("couponDiscountPct"));
                                        }
                                        if (jsonCoupon.has("couponStartDate")) {
                                            specialOfferData
                                                    .put("couponStartDate",
                                                            jsonCoupon
                                                                    .getString("couponStartDate"));
                                        }
                                        if (jsonCoupon.has("couponImagePath")) {
                                            specialOfferData
                                                    .put("couponImagePath",
                                                            jsonCoupon
                                                                    .getString("couponImagePath"));
                                        }

                                        if (jsonCoupon.has("couponNewFlag")) {
                                            specialOfferData
                                                    .put(CommonConstants.ALLCOUPONSNEWFLAG,
                                                            jsonCoupon
                                                                    .getInt("newFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("couponId")) {
                                            specialOfferData.put(
                                                    "couponId",
                                                    jsonCoupon
                                                            .getInt("couponId")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("couponUsed")) {
                                            specialOfferData.put("couponUsed",
                                                    jsonCoupon.getInt("used")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("couponListId")) {
                                            specialOfferData
                                                    .put("couponListId",
                                                            jsonCoupon
                                                                    .getInt("couponListId")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("redeemFlag")) {
                                            specialOfferData
                                                    .put("redeemFlag",
                                                            jsonCoupon
                                                                    .getInt("redeemFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("claimFlag")) {
                                            specialOfferData
                                                    .put("claimFlag",
                                                            jsonCoupon
                                                                    .getInt("claimFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("coupDistance")) {
                                            specialOfferData
                                                    .put("coupDistance",
                                                            jsonCoupon
                                                                    .getDouble("distance")
                                                                    + "");
                                        }
                                        specialoffersList.add(specialOfferData);
                                    }

                                }
                            }
                        }
                    } else {
                        jsonretDetailsList = jsonCoupons.getJSONObject(
                                "retDetailsList").getJSONObject(
                                "RetailersDetails");

                        JSONObject jsonRetdetailsObj = jsonretDetailsList
                                .getJSONObject("retDetailsList");
                        specialOfferData = new HashMap<>();
                        int retId = jsonretDetailsList.getInt("retId");
                        String retname = jsonretDetailsList
                                .getString("retName");

                        specialOfferData.put("itemName", "category");
                        specialOfferData.put(CommonConstants.ALLCOUPONSCATID,
                                retId + "");
                        specialOfferData.put("cateName", retname);
                        specialoffersList.add(specialOfferData);

                        boolean isArrayjsonRetailer = isJSONArray(
                                jsonRetdetailsObj, "RetailersDetails");
                        if (isArrayjsonRetailer) {
                            JSONArray jsonRetailerArray = jsonRetdetailsObj
                                    .getJSONArray("RetailersDetails");
                            for (int retIndex = 0; retIndex < jsonRetailerArray
                                    .length(); retIndex++) {
                                JSONObject jsonRetailer = jsonRetailerArray
                                        .getJSONObject(retIndex);

                                specialOfferData = new HashMap<>();
                                specialOfferData.put("itemName", "location");
                                if (jsonRetailer.has("postalCode")) {
                                    specialOfferData.put("postalCode",
                                            jsonRetailer.getInt("postalCode")
                                                    + "");
                                }
                                if (jsonRetailer.has("minRetDist")) {
                                    specialOfferData.put(
                                            "minRetDist",
                                            jsonRetailer
                                                    .getDouble("minRetDist")
                                                    + "");
                                }
                                if (jsonRetailer.has("retailerAddress")) {
                                    specialOfferData
                                            .put(CommonConstants.ALLCOUPONSLOCATION,
                                                    jsonRetailer
                                                            .getString("retailerAddress"));
                                }
                                if (jsonRetailer.has("retLocId")) {
                                    specialOfferData.put("retLocId",
                                            jsonRetailer.getInt("retLocId")
                                                    + "");
                                }
                                if (jsonRetailer.has("city")) {
                                    specialOfferData.put("city",
                                            jsonRetailer.getString("city"));
                                }
                                specialoffersList.add(specialOfferData);

                                boolean isArrayCouponDetails = isJSONArray(
                                        jsonRetailer
                                                .getJSONObject("couponDetailsList"),
                                        "CouponDetails");
                                JSONArray jsonCouponDetailsListArray = null;
                                if (isArrayCouponDetails) {
                                    jsonCouponDetailsListArray = jsonRetailer
                                            .getJSONObject("couponDetailsList")
                                            .getJSONArray("CouponDetails");
                                    for (int couponIndx = 0; couponIndx < jsonCouponDetailsListArray
                                            .length(); couponIndx++) {
                                        JSONObject jsonCoupon = jsonCouponDetailsListArray
                                                .getJSONObject(couponIndx);

                                        if (jsonCoupon != null) {
                                            specialOfferData = new HashMap<>();
                                            specialOfferData.put("itemName",
                                                    "coupons");
                                            if (jsonCoupon.has("couponName")) {
                                                specialOfferData
                                                        .put("couponName",
                                                                jsonCoupon
                                                                        .getString("couponName"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponDiscountAmount")) {
                                                specialOfferData
                                                        .put("couponDiscountAmount",
                                                                jsonCoupon
                                                                        .getString("couponDiscountAmount"));
                                            }
                                            if (jsonCoupon.has("couponURL")) {
                                                specialOfferData
                                                        .put("couponURL",
                                                                jsonCoupon
                                                                        .getString("couponURL"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponExpireDate")) {
                                                specialOfferData
                                                        .put("couponExpireDate",
                                                                jsonCoupon
                                                                        .getString("couponExpireDate"));
                                            }
                                            if (jsonCoupon.has("coupDesc")) {
                                                specialOfferData
                                                        .put("coupDesc",
                                                                jsonCoupon
                                                                        .getString("coupDesc"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponDiscountPct")) {
                                                specialOfferData
                                                        .put("couponDiscountPct",
                                                                jsonCoupon
                                                                        .getString("couponDiscountPct"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponStartDate")) {
                                                specialOfferData
                                                        .put("couponStartDate",
                                                                jsonCoupon
                                                                        .getString("couponStartDate"));
                                            }
                                            if (jsonCoupon
                                                    .has("couponImagePath")) {
                                                specialOfferData
                                                        .put("couponImagePath",
                                                                jsonCoupon
                                                                        .getString("couponImagePath"));
                                            }

                                            if (jsonCoupon.has("couponNewFlag")) {
                                                specialOfferData
                                                        .put(CommonConstants.ALLCOUPONSNEWFLAG,
                                                                jsonCoupon
                                                                        .getInt("newFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("couponId")) {
                                                specialOfferData
                                                        .put("couponId",
                                                                jsonCoupon
                                                                        .getInt("couponId")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("couponUsed")) {
                                                specialOfferData.put(
                                                        "couponUsed",
                                                        jsonCoupon
                                                                .getInt("used")
                                                                + "");
                                            }
                                            if (jsonCoupon.has("couponListId")) {
                                                specialOfferData
                                                        .put("couponListId",
                                                                jsonCoupon
                                                                        .getInt("couponListId")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("redeemFlag")) {
                                                specialOfferData
                                                        .put("redeemFlag",
                                                                jsonCoupon
                                                                        .getInt("redeemFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("claimFlag")) {
                                                specialOfferData
                                                        .put("claimFlag",
                                                                jsonCoupon
                                                                        .getInt("claimFlag")
                                                                        + "");
                                            }
                                            if (jsonCoupon.has("coupDistance")) {
                                                specialOfferData
                                                        .put("coupDistance",
                                                                jsonCoupon
                                                                        .getDouble("distance")
                                                                        + "");
                                            }
                                            specialoffersList
                                                    .add(specialOfferData);
                                        }

                                    }
                                } else {
                                    JSONObject jsonCoupon = jsonRetailer
                                            .getJSONObject("couponDetailsList")
                                            .getJSONObject("CouponDetails");

                                    if (jsonCoupon != null) {
                                        specialOfferData = new HashMap<>();
                                        specialOfferData.put("itemName",
                                                "coupons");
                                        if (jsonCoupon.has("couponName")) {
                                            specialOfferData
                                                    .put("couponName",
                                                            jsonCoupon
                                                                    .getString("couponName"));
                                        }
                                        if (jsonCoupon
                                                .has("couponDiscountAmount")) {
                                            specialOfferData
                                                    .put("couponDiscountAmount",
                                                            jsonCoupon
                                                                    .getString("couponDiscountAmount"));
                                        }
                                        if (jsonCoupon.has("couponURL")) {
                                            specialOfferData
                                                    .put("couponURL",
                                                            jsonCoupon
                                                                    .getString("couponURL"));
                                        }
                                        if (jsonCoupon.has("couponExpireDate")) {
                                            specialOfferData
                                                    .put("couponExpireDate",
                                                            jsonCoupon
                                                                    .getString("couponExpireDate"));
                                        }
                                        if (jsonCoupon.has("coupDesc")) {
                                            specialOfferData
                                                    .put("coupDesc",
                                                            jsonCoupon
                                                                    .getString("coupDesc"));
                                        }
                                        if (jsonCoupon.has("couponDiscountPct")) {
                                            specialOfferData
                                                    .put("couponDiscountPct",
                                                            jsonCoupon
                                                                    .getString("couponDiscountPct"));
                                        }
                                        if (jsonCoupon.has("couponStartDate")) {
                                            specialOfferData
                                                    .put("couponStartDate",
                                                            jsonCoupon
                                                                    .getString("couponStartDate"));
                                        }
                                        if (jsonCoupon.has("couponImagePath")) {
                                            specialOfferData
                                                    .put("couponImagePath",
                                                            jsonCoupon
                                                                    .getString("couponImagePath"));
                                        }

                                        if (jsonCoupon.has("couponNewFlag")) {
                                            specialOfferData
                                                    .put(CommonConstants.ALLCOUPONSNEWFLAG,
                                                            jsonCoupon
                                                                    .getInt("newFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("couponId")) {
                                            specialOfferData.put(
                                                    "couponId",
                                                    jsonCoupon
                                                            .getInt("couponId")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("used")) {
                                            specialOfferData.put("couponUsed",
                                                    jsonCoupon.getInt("used")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("couponListId")) {
                                            specialOfferData
                                                    .put("couponListId",
                                                            jsonCoupon
                                                                    .getInt("couponListId")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("redeemFlag")) {
                                            specialOfferData
                                                    .put("redeemFlag",
                                                            jsonCoupon
                                                                    .getInt("redeemFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("claimFlag")) {
                                            specialOfferData
                                                    .put("claimFlag",
                                                            jsonCoupon
                                                                    .getInt("claimFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("coupDistance")) {
                                            specialOfferData
                                                    .put("coupDistance",
                                                            jsonCoupon
                                                                    .getDouble("distance")
                                                                    + "");
                                        }
                                        specialoffersList.add(specialOfferData);
                                    }

                                }

                            }

                        } else {

                            JSONObject jsonRetailer = jsonRetdetailsObj
                                    .getJSONObject("RetailersDetails");
                            specialOfferData = new HashMap<>();
                            specialOfferData.put("itemName", "location");
                            if (jsonRetailer.has("postalCode")) {
                                specialOfferData.put("postalCode",
                                        jsonRetailer.getInt("postalCode") + "");
                            }
                            if (jsonRetailer.has("minRetDist")) {
                                specialOfferData.put("minRetDist",
                                        jsonRetailer.getDouble("minRetDist")
                                                + "");
                            }
                            if (jsonRetailer.has("retailerAddress")) {
                                specialOfferData.put(
                                        CommonConstants.ALLCOUPONSLOCATION,
                                        jsonRetailer
                                                .getString("retailerAddress"));
                            }
                            if (jsonRetailer.has("retLocId")) {
                                specialOfferData.put("retLocId",
                                        jsonRetailer.getInt("retLocId") + "");
                            }
                            if (jsonRetailer.has("city")) {
                                specialOfferData.put("city",
                                        jsonRetailer.getString("city"));
                            }
                            specialoffersList.add(specialOfferData);

                            boolean isArrayCouponDetails = isJSONArray(
                                    jsonRetailer
                                            .getJSONObject("couponDetailsList"),
                                    "CouponDetails");
                            JSONArray jsonCouponDetailsListArray = null;
                            if (isArrayCouponDetails) {
                                jsonCouponDetailsListArray = jsonRetailer
                                        .getJSONObject("couponDetailsList")
                                        .getJSONArray("CouponDetails");
                                for (int couponIndx = 0; couponIndx < jsonCouponDetailsListArray
                                        .length(); couponIndx++) {
                                    JSONObject jsonCoupon = jsonCouponDetailsListArray
                                            .getJSONObject(couponIndx);

                                    if (jsonCoupon != null) {
                                        specialOfferData = new HashMap<>();
                                        specialOfferData.put("itemName",
                                                "coupons");
                                        if (jsonCoupon.has("couponName")) {
                                            specialOfferData
                                                    .put("couponName",
                                                            jsonCoupon
                                                                    .getString("couponName"));
                                        }
                                        if (jsonCoupon
                                                .has("couponDiscountAmount")) {
                                            specialOfferData
                                                    .put("couponDiscountAmount",
                                                            jsonCoupon
                                                                    .getString("couponDiscountAmount"));
                                        }
                                        if (jsonCoupon.has("couponURL")) {
                                            specialOfferData
                                                    .put("couponURL",
                                                            jsonCoupon
                                                                    .getString("couponURL"));
                                        }
                                        if (jsonCoupon.has("couponExpireDate")) {
                                            specialOfferData
                                                    .put("couponExpireDate",
                                                            jsonCoupon
                                                                    .getString("couponExpireDate"));
                                        }
                                        if (jsonCoupon.has("coupDesc")) {
                                            specialOfferData
                                                    .put("coupDesc",
                                                            jsonCoupon
                                                                    .getString("coupDesc"));
                                        }
                                        if (jsonCoupon.has("couponDiscountPct")) {
                                            specialOfferData
                                                    .put("couponDiscountPct",
                                                            jsonCoupon
                                                                    .getString("couponDiscountPct"));
                                        }
                                        if (jsonCoupon.has("couponStartDate")) {
                                            specialOfferData
                                                    .put("couponStartDate",
                                                            jsonCoupon
                                                                    .getString("couponStartDate"));
                                        }
                                        if (jsonCoupon.has("couponImagePath")) {
                                            specialOfferData
                                                    .put("couponImagePath",
                                                            jsonCoupon
                                                                    .getString("couponImagePath"));
                                        }

                                        if (jsonCoupon.has("couponNewFlag")) {
                                            specialOfferData
                                                    .put(CommonConstants.ALLCOUPONSNEWFLAG,
                                                            jsonCoupon
                                                                    .getInt("newFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("couponId")) {
                                            specialOfferData.put(
                                                    "couponId",
                                                    jsonCoupon
                                                            .getInt("couponId")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("couponUsed")) {
                                            specialOfferData.put("couponUsed",
                                                    jsonCoupon.getInt("used")
                                                            + "");
                                        }
                                        if (jsonCoupon.has("couponListId")) {
                                            specialOfferData
                                                    .put("couponListId",
                                                            jsonCoupon
                                                                    .getInt("couponListId")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("redeemFlag")) {
                                            specialOfferData
                                                    .put("redeemFlag",
                                                            jsonCoupon
                                                                    .getInt("redeemFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("claimFlag")) {
                                            specialOfferData
                                                    .put("claimFlag",
                                                            jsonCoupon
                                                                    .getInt("claimFlag")
                                                                    + "");
                                        }
                                        if (jsonCoupon.has("coupDistance")) {
                                            specialOfferData
                                                    .put("coupDistance",
                                                            jsonCoupon
                                                                    .getDouble("distance")
                                                                    + "");
                                        }
                                        specialoffersList.add(specialOfferData);
                                    }

                                }
                            } else {
                                JSONObject jsonCoupon = jsonRetailer
                                        .getJSONObject("couponDetailsList")
                                        .getJSONObject("CouponDetails");

                                if (jsonCoupon != null) {
                                    specialOfferData = new HashMap<>();
                                    specialOfferData.put("itemName", "coupons");
                                    if (jsonCoupon.has("couponName")) {
                                        specialOfferData
                                                .put("couponName",
                                                        jsonCoupon
                                                                .getString("couponName"));
                                    }
                                    if (jsonCoupon.has("couponDiscountAmount")) {
                                        specialOfferData
                                                .put("couponDiscountAmount",
                                                        jsonCoupon
                                                                .getString("couponDiscountAmount"));
                                    }
                                    if (jsonCoupon.has("couponURL")) {
                                        specialOfferData
                                                .put("couponURL", jsonCoupon
                                                        .getString("couponURL"));
                                    }
                                    if (jsonCoupon.has("couponExpireDate")) {
                                        specialOfferData
                                                .put("couponExpireDate",
                                                        jsonCoupon
                                                                .getString("couponExpireDate"));
                                    }
                                    if (jsonCoupon.has("coupDesc")) {
                                        specialOfferData.put("coupDesc",
                                                jsonCoupon
                                                        .getString("coupDesc"));
                                    }
                                    if (jsonCoupon.has("couponDiscountPct")) {
                                        specialOfferData
                                                .put("couponDiscountPct",
                                                        jsonCoupon
                                                                .getString("couponDiscountPct"));
                                    }
                                    if (jsonCoupon.has("couponStartDate")) {
                                        specialOfferData
                                                .put("couponStartDate",
                                                        jsonCoupon
                                                                .getString("couponStartDate"));
                                    }
                                    if (jsonCoupon.has("couponImagePath")) {
                                        specialOfferData
                                                .put("couponImagePath",
                                                        jsonCoupon
                                                                .getString("couponImagePath"));
                                    }

                                    if (jsonCoupon.has("couponNewFlag")) {
                                        specialOfferData
                                                .put(CommonConstants.ALLCOUPONSNEWFLAG,
                                                        jsonCoupon
                                                                .getInt("newFlag")
                                                                + "");
                                    }
                                    if (jsonCoupon.has("couponId")) {
                                        specialOfferData.put("couponId",
                                                jsonCoupon.getInt("couponId")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("used")) {
                                        specialOfferData.put("couponUsed",
                                                jsonCoupon.getInt("used") + "");
                                    }
                                    if (jsonCoupon.has("couponListId")) {
                                        specialOfferData.put(
                                                "couponListId",
                                                jsonCoupon
                                                        .getInt("couponListId")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("redeemFlag")) {
                                        specialOfferData.put("redeemFlag",
                                                jsonCoupon.getInt("redeemFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("claimFlag")) {
                                        specialOfferData.put("claimFlag",
                                                jsonCoupon.getInt("claimFlag")
                                                        + "");
                                    }
                                    if (jsonCoupon.has("coupDistance")) {
                                        specialOfferData.put(
                                                "coupDistance",
                                                jsonCoupon
                                                        .getDouble("distance")
                                                        + "");
                                    }
                                    specialoffersList.add(specialOfferData);
                                }

                            }
                        }
                    }

                }
                if (maxCount > lastvisitProdIdProducts) {
                    lastvisitProdIdProducts = maxRowNum;
                }
                if (nextPage) {
                    // nextPage = false;
                    hotData = new HashMap<>();
                    hotData.put("itemName", "viewMore");
                    hotData.put(
                            CommonConstants.FINDPRODUCTSEARCHPRODUCTROWNUMBER,
                            maxRowNum + "");
                    specialoffersList.add(hotData);
                }
                result = "true";

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;

        }
    }

    String claimRedeemExpiredValue;

    public HashMap<String, String> getJSONValuesLocation(JSONObject jsonHotDeal) {
        HashMap<String, String> hotData = new HashMap<>();
        hotData.put("itemName", "details");
        try {

            if (jsonHotDeal.has("hDDesc")) {
                hotData.put("hDDesc", jsonHotDeal.getString("hDDesc"));
            }
            if (jsonHotDeal.has("hdURL")) {
                hotData.put("hdURL", jsonHotDeal.getString("hdURL"));
            }
            if (jsonHotDeal.has("hDEndDate")) {
                hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
            }
            if (jsonHotDeal.has("newFlag")) {
                hotData.put("newFlag", jsonHotDeal.getString("newFlag"));
            }
            if (jsonHotDeal.has("hotDealImagePath")) {
                hotData.put("hotDealImagePath",
                        jsonHotDeal.getString("hotDealImagePath"));
            }
            if (jsonHotDeal.has("hDExpDate")) {
                hotData.put("hDExpDate", jsonHotDeal.getString("hDExpDate"));
            }
            if (jsonHotDeal.has("hDStartDate")) {
                hotData.put("hDStartDate", jsonHotDeal.getString("hDStartDate"));
            }
            if (jsonHotDeal.has("hDDiscountAmount")) {
                hotData.put("hDDiscountAmount",
                        jsonHotDeal.getString("hDDiscountAmount"));
            }
            if (jsonHotDeal.has("rowNumber")) {
                hotData.put("rowNumber", jsonHotDeal.getString("rowNumber"));
            }
            if (jsonHotDeal.has("hotDealId")) {
                hotData.put("hotDealId", jsonHotDeal.getString("hotDealId"));
            }
            if (jsonHotDeal.has("city")) {
                hotData.put("city", jsonHotDeal.getString("city"));
            }
            if (jsonHotDeal.has("hDDiscountPct")) {
                hotData.put("hDDiscountPct",
                        jsonHotDeal.getString("hDDiscountPct"));
            }
            if (jsonHotDeal.has("hotDealListId")) {
                hotData.put("hotDealListId",
                        jsonHotDeal.getString("hotDealListId"));
            }
            if (jsonHotDeal.has("hotDealName")) {
                hotData.put("hotDealName", jsonHotDeal.getString("hotDealName"));
            }
            if (jsonHotDeal.has("extFlag")) {
                hotData.put("extFlag", jsonHotDeal.getString("extFlag"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return hotData;
    }


    public HashMap<String, String> getJSONValuesProducts(JSONObject jsonHotDeal) {
        HashMap<String, String> hotData = new HashMap<>();
        hotData.put("itemName", "details");
        try {

            if (jsonHotDeal.has("hDDesc")) {
                hotData.put("hDDesc", jsonHotDeal.getString("hDDesc"));
            }
            if (jsonHotDeal.has("hdURL")) {
                hotData.put("hdURL", jsonHotDeal.getString("hdURL"));
            }
            if (jsonHotDeal.has("hDEndDate")) {
                hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
            }
            if (jsonHotDeal.has("newFlag")) {
                hotData.put("newFlag", jsonHotDeal.getString("newFlag"));
            }
            if (jsonHotDeal.has("hotDealImagePath")) {
                hotData.put("hotDealImagePath",
                        jsonHotDeal.getString("hotDealImagePath"));
            }
            if (jsonHotDeal.has("hDEndDate")) {
                hotData.put("hDEndDate", jsonHotDeal.getString("hDEndDate"));
            }
            if (jsonHotDeal.has("hDStartDate")) {
                hotData.put("hDStartDate", jsonHotDeal.getString("hDStartDate"));
            }
            if (jsonHotDeal.has("hDDiscountAmount")) {
                hotData.put("hDDiscountAmount",
                        jsonHotDeal.getString("hDDiscountAmount"));
            }
            if (jsonHotDeal.has("rowNumber")) {
                hotData.put("rowNumber", jsonHotDeal.getString("rowNumber"));
            }
            if (jsonHotDeal.has("hotDealId")) {
                hotData.put("hotDealId", jsonHotDeal.getString("hotDealId"));
            }
            if (jsonHotDeal.has("city")) {
                hotData.put("city", jsonHotDeal.getString("city"));
            }
            if (jsonHotDeal.has("hDDiscountPct")) {
                hotData.put("hDDiscountPct",
                        jsonHotDeal.getString("hDDiscountPct"));
            }
            if (jsonHotDeal.has("hotDealListId")) {
                hotData.put("hotDealListId",
                        jsonHotDeal.getString("hotDealListId"));
            }
            if (jsonHotDeal.has("hotDealName")) {
                hotData.put("hotDealName", jsonHotDeal.getString("hotDealName"));
            }
            if (jsonHotDeal.has("extFlag")) {
                hotData.put("extFlag", jsonHotDeal.getString("extFlag"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return hotData;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}