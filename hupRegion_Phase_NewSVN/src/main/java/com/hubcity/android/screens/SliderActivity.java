package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.webkit.WebSettings;
import android.widget.Toast;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.PrefManager;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.model.HubCitiSlideModel;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.scansee.hubregion.RegistrationIntentService;
import com.scansee.newsfirst.CommonMethods;
import com.scansee.newsfirst.ConnectionUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sharanamma on 18-08-2016.
 */
public class SliderActivity extends AppCompatActivity {
    private static final String TAG = SliderActivity.class.getSimpleName();
    private ArrayList<HubCitiSlideModel.SlidImages> slideImagesList = new ArrayList<>();
    private ConnectionUtils objConnectionUtils;
    private ProgressDialog progressBar;
    private Activity activity;
    private TextView[] dotsView;
    private ViewPager viewPager;
    private LinearLayout dotsLayout;
    private int listSize = 0;
    private String logoPath = null;
    private PrefManager objPrefManager;
    TextView skipTextView, signUpTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);
        LoginScreenViewAsyncTask mLoginScreenViewAsyncTask = new LoginScreenViewAsyncTask();
        mLoginScreenViewAsyncTask.execute(" ");
        objConnectionUtils = new ConnectionUtils(this);
        activity = SliderActivity.this;
        objPrefManager = new PrefManager(this);
        bindView();
        getSlides();
    }

    private void bindView() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        skipTextView = (TextView) findViewById(R.id.skipView);
        signUpTextView = (TextView) findViewById(R.id.signUpView);
    }

    private void addBottomDots(int currentPage) {
        dotsView = new TextView[listSize];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dotsView.length; i++) {
            dotsView[i] = new TextView(this);
            dotsView[i].setText(Html.fromHtml("&#8226;"));
            dotsView[i].setTextSize(35);
            dotsView[i].setTextColor(getResources().getColor(R.color.gray));
            dotsLayout.addView(dotsView[i]);
        }

        if (dotsView.length > 0)
            dotsView[currentPage].setTextColor(getResources().getColor(R.color.white));
    }

    private void getSlides() {
        if (objConnectionUtils.getConnectivityStatus() == Constants
                .TYPE_NOT_CONNECTED) {
            Toast.makeText(this, getResources().getString(R.string.enable_internet), Toast
                    .LENGTH_SHORT).show();
        } else {
            progressBar = ConnectionUtils.showLoading(activity);
            callSlidesAPI();
        }
    }

    private void callSlidesAPI() {
        String hubCityKey = getResources().getString(R.string.hubcity_key);
        RestClient.getInstance().getSlides(hubCityKey, new Callback<HubCitiSlideModel>() {

            @Override
            public void success(HubCitiSlideModel hubCitiSlideModel, Response response) {
                try {
                    Log.d(TAG, " Success ");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        if (activity.isDestroyed()) {
                            return;
                        }
                    }
                    if (progressBar != null && progressBar.isShowing())
                        progressBar.dismiss();
                    if (hubCitiSlideModel.getResponseCode().equalsIgnoreCase("10000")) {
                        logoPath = hubCitiSlideModel.getLogoPath();
                        slideImagesList = hubCitiSlideModel.getSlideImages();
                        if (slideImagesList == null) {
                            listSize = 0;
                        } else {
                            listSize = slideImagesList.size();
                            if (listSize > 1)
                                addBottomDots(0);
                        }
                        viewPager.setAdapter(new SliderViewPagerAdapter(SliderActivity.this));
                        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
                    } else {
                        Toast.makeText(activity, hubCitiSlideModel.getResponseText(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError retrofitError) {
                if (progressBar != null && progressBar.isShowing())
                    progressBar.dismiss();
                if (retrofitError.getResponse() != null) {
                    Log.d(TAG, "failure : " + retrofitError
                            .getResponse().getReason());
                }
            }
        });
    }

    class SliderViewPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public SliderViewPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (slideImagesList == null) {
                return 1;
            } else {
                return listSize;
            }
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View itemView;
            itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
            ImageView imageview = (ImageView) itemView.findViewById(R.id.imageview);
            FrameLayout tapIconLayout = (FrameLayout) itemView.findViewById(R.id.overlap_layout);
            ImageView logoImageView = (ImageView) itemView.findViewById(R.id.imageView_logo);
            if (slideImagesList != null) {

                logoImageView.setVisibility(View.GONE);
                if (slideImagesList.get(position).getType() == 1) {
                    imageview.setVisibility(View.VISIBLE);
                    final ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id
                            .progress);
                    new CommonMethods().loadImage(mContext, progressBar, slideImagesList.get(position).getLink(), imageview);

                }


                container.addView(itemView);
            } else {
                firstTimeLaunch();
                imageview.setVisibility(View.GONE);
                logoImageView.setVisibility(View.VISIBLE);

                if (logoPath != null && !logoPath.isEmpty()) {
                   /* HubCitiSlideModel hubCitiSlideModel = null;
                    logoPath = hubCitiSlideModel.getLogoPath();*/
                    System.out.println("logoPath : " + logoPath);
                    final ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id
                            .progressLogo);
                    new CommonMethods().loadImage(mContext, progressBar, logoPath, logoImageView);
                }
                container.addView(itemView);
            }

            skipTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    firstTimeLaunch();
                    new LoginScreenViewAsyncTask(activity).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, " ");
                }
            });


            signUpTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    firstTimeLaunch();
                    new LoginScreenViewAsyncTask(activity,true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, " ");
                }
            });


            if (slideImagesList != null && slideImagesList.get(position).getIsAnythingPageOrWeblink() != null) {

                tapIconLayout.setVisibility(View.VISIBLE);

                imageview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CommonConstants.isFromSliderScreen = true;
                        if (slideImagesList.get(position).getIsAnythingPageOrWeblink().equalsIgnoreCase(Constants.Weblink)) {
                            Intent intent = new Intent(
                                    activity,
                                    ScanseeBrowserActivity.class);
                            intent.putExtra(CommonConstants.URL, slideImagesList.get(position).getIsAnythingPageOrWeblinkValue());
                            activity.startActivity(intent);
                        } else {
                            Intent intent = new Intent(activity, AnythingPageDisplayActivity.class);
                            intent.putExtra(CommonConstants.LINK_ID, slideImagesList.get(position).getIsAnythingPageOrWeblinkValue());
                            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, 0);
                            intent.putExtra("isShare", false);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            activity.startActivity(intent);
                        }
                    }


                });
            }


            return itemView;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {

            if (slideImagesList != null && listSize > 1) {
                addBottomDots(position);
                if (position == listSize - 1) {
                    firstTimeLaunch();
                }
            }

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void firstTimeLaunch() {
        if (objPrefManager.isFirstTimeLaunch()) {
            objPrefManager.setFirstTimeLaunch(false);
            Intent intent = new Intent(activity, RegistrationIntentService.class);
            activity.startService(intent);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        objConnectionUtils = null;
        objPrefManager = null;
    }
}
