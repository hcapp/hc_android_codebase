package com.hubcity.android.screens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.scansee.newsfirst.CommonMethods;
import com.scansee.newsfirst.ScrollingPageActivity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.TreeMap;

/**
 * Created by subramanya.v on 4/13/2016.
 */
public class RetailerAdpater extends BaseAdapter {
    private Context mContext;
    private ArrayList<EventDetailObj> retailerDetails;
    private int maxRowNum;
    private int maxCount;
    private boolean bandSummary;


    public RetailerAdpater(EventMapScreen showMapMultipleLocationsTest, ArrayList<EventDetailObj>
            retailerDetails, int maxRowNum, int maxCount, boolean bandSummary) {
        mContext = showMapMultipleLocationsTest;
        this.retailerDetails = retailerDetails;
        this.maxRowNum = maxRowNum;
        this.maxCount = maxCount;
        this.bandSummary = bandSummary;
    }

    @Override
    public int getCount() {
        return retailerDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return retailerDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView EventName = null, EventCat = null, EventSubCat = null, startDateView = null, mile = null, header = null, venue;
        ImageView eventImage = null;
        ProgressBar progressBar = null;

        int pagPosition = position + 1;
        if (maxRowNum < maxCount) {
            if (pagPosition == maxRowNum) {
                if (!((EventMapScreen) mContext).isLoaded) {
                    ((EventMapScreen) mContext).paginationRetailerList(maxRowNum);
                }
            }
        }
        String bandName = retailerDetails.get(position).getBandName();
        String eventName = retailerDetails.get(position).getEventName();
        String startDate = retailerDetails.get(position).getStartDate();
        String eventCatName = retailerDetails.get(position).getEventCatName();
        String distance = retailerDetails.get(position).getDistance();
        String groupContent = retailerDetails.get(position).getGroupContent();
        String image = retailerDetails.get(position).getListingImgPath();
        String venueName = retailerDetails.get(position).getEvtLocTitle();
        int bandCount = retailerDetails.get(position).getBandCntFlag();

        LayoutInflater infalInflater = (LayoutInflater) this.mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (retailerDetails.get(position).isHeader()) {
            convertView = infalInflater.inflate(
                    R.layout.header_view, parent, false);
            header = (TextView) convertView.findViewById(R.id.header);
            if (groupContent != null && !groupContent.equalsIgnoreCase("")) {
                header.setText(groupContent);
            }
        } else {
            convertView = infalInflater.inflate(
                    R.layout.retailer_adapter, parent, false);
            EventName = (TextView) convertView.findViewById(R.id.event_name);
            eventImage = (ImageView) convertView.findViewById(R.id.event_image);
            progressBar = (ProgressBar) convertView.findViewById(R.id.progress);
            EventCat = (TextView) convertView.findViewById(R.id.event_cat);
            EventSubCat = (TextView) convertView.findViewById(R.id.event_subcategory);
            startDateView = (TextView) convertView.findViewById(R.id.start_date);
            venue = (TextView) convertView.findViewById(R.id.venue_name);
            mile = (TextView) convertView.findViewById(R.id.mile);

            if (bandName != null && !bandName.equalsIgnoreCase("") && bandCount != 0) {
                EventSubCat.setText(bandName);
            } else {
                EventSubCat.setVisibility(View.GONE);
            }


            if (venueName != null && !venueName.equalsIgnoreCase("") && !venueName.equalsIgnoreCase("N/A")) {
                venue.setText("Location: " + venueName);
            }
            if (eventName != null && !eventName.equalsIgnoreCase("")) {
                EventName.setText(eventName);
            }
            if (startDate != null && !startDate.equalsIgnoreCase("")) {
                startDateView.setText(startDate);
            }
            if (eventCatName != null && !eventCatName.equalsIgnoreCase("")) {
                EventCat.setText(eventCatName);
            }
            if (distance != null && !distance.equalsIgnoreCase("")) {
                mile.setText(distance);
            }

            if (image != null && !image.equalsIgnoreCase("")) {
                image = image.replaceAll(" ", "%20");
                assert convertView != null;
                new CommonMethods().loadImage(mContext, progressBar, image, eventImage);
            }

        }

        return convertView;
    }


    public void updateList(ArrayList<EventDetailObj> retailerDetails, int maxRowNum,
                           int maxCount) {
        this.retailerDetails = retailerDetails;
        this.maxRowNum = maxRowNum;
        this.maxCount = maxCount;
    }


}
