package com.hubcity.android.screens;

import android.os.AsyncTask;

import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

import org.json.JSONObject;

/**
 * Created by supriya.m on 3/22/2016.
 * This AsyncTask is to update retailer impression
 */
public class RetailerImpressionUpdateAsync extends AsyncTask<Void, Void, Void>
{
	private String catName, retailerIds, retailerLocIds;

	public RetailerImpressionUpdateAsync(String catName, String retailerIds, String retailerLocIds)
	{
		this.catName = catName;
		this.retailerIds = retailerIds;
		this.retailerLocIds = retailerLocIds;
	}

	@Override
	protected Void doInBackground(Void... params)
	{
		try {
			UrlRequestParams mUrlRequestParams = new UrlRequestParams();
			ServerConnections mServerConnections = new ServerConnections();
			String retailerImpressionUrl = Properties.url_local_server
					+ Properties.hubciti_version
					+ "hotdeals/retclickimpression";
//			String retailerImpressionUrl = "http://10.10.220.82:8080/HubCiti2.4" +
//					".3/hotdeals/retclickimpression";
			JSONObject jsonParam = mUrlRequestParams.getRetailerImpressionRequest(catName,
					retailerIds, retailerLocIds);
			mServerConnections.getUrlJsonPostResponse(retailerImpressionUrl, jsonParam);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
