package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class EventsSortByActivity extends CustomTitleBar implements
		OnClickListener {


	String groupChoice;
	String sortChoice;
	CheckBox dateGroupCheck;
	CheckBox atozGroupCheck;
	CheckBox typeGroupCheck;
	TextView citySort;
	CheckBox dateSortCheck;
	CheckBox nameSortCheck;
	CheckBox distanceSortCheck;
	CheckBox citySortCheck;

	String group;
	String sort;

	TableRow mTableRowDate;
	TableRow mTableRowAtoZ;
	TableRow mTableRowType;
	TableRow mTableRowDateSort;
	TableRow mTableRowNameSort;
	TableRow mTableRowDistanceSort;
	TableRow mTableRowCitySort;

	LinearLayout ll;

	CitySort citySorting;
	Activity activity;

	String mItemId, mBottomId;

	void setContentViewWithCustomTitleBar() {

		setContentView(R.layout.events_sort_activity);

		activity = this;

		ll = (LinearLayout) findViewById(R.id.city_ll);
		CitySort.isDefaultCitySort = false;

		if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
				&& getIntent().getExtras().getString(
						Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
			mItemId = getIntent().getExtras().getString(
					Constants.MENU_ITEM_ID_INTENT_EXTRA);

		} else if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
				&& getIntent().getExtras().getString(
						Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
			mBottomId = getIntent().getExtras().getString(
					Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
		}

		HubCityContext hubciti = (HubCityContext) getApplicationContext();
		groupChoice = hubciti.getGroupSelection();
		sortChoice = hubciti.getSortSelection();

		group = groupChoice;
		sort = sortChoice;

		backImage.setVisibility(View.GONE);
		this.title.setText("Group&Sort");
		this.leftTitleImage.setBackgroundResource(R.drawable.ic_action_cancel);
		this.leftTitleImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				HubCityContext mHubCity = (HubCityContext) getApplicationContext();
				mHubCity.setGroupSelection(group);
				mHubCity.setSortSelection(sort);

				Intent updateEventList = new Intent(EventsSortByActivity.this,
						EventsListDisplay.class);
				updateEventList.putExtra("isSingleCategory", getIntent()
						.getExtras().getBoolean("isSingleCategory"));
				updateEventList.putExtra(
						Constants.MENU_ITEM_ID_INTENT_EXTRA,
						getIntent().getExtras().getString(
								Constants.MENU_ITEM_ID_INTENT_EXTRA));
				updateEventList.putExtra(
						Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
						getIntent().getExtras().getString(
								Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
				updateEventList.putExtra("mLinkId", getIntent().getExtras()
						.getString("mLinkId"));
				updateEventList.putExtra("catName", getIntent().getExtras()
						.getString("catName"));
				updateEventList.putExtra("catId", getIntent().getExtras()
						.getString("catId"));
				updateEventList.putExtra("fundId", getIntent().getExtras()
						.getString("fundId"));
				updateEventList.putExtra("retailerId", getIntent().getExtras()
						.getString("retailerId"));
				updateEventList.putExtra("retListId", getIntent().getExtras()
						.getString("retListId"));
				updateEventList.putExtra("retailLocationId", getIntent()
						.getExtras().getString("retailLocationId"));
				updateEventList.putExtra("retailerEvents", getIntent()
						.getExtras().getString("retailerEvents"));

				startActivity(updateEventList);
				finish();
			}
		});

		this.rightImage.setBackgroundResource(R.drawable.ic_action_accept);
		this.rightImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

					HubCityContext mHubCity = (HubCityContext) getApplicationContext();
					mHubCity.setGroupSelection(groupChoice);
					mHubCity.setSortSelection(sortChoice);

					Intent updateEventList = new Intent(EventsSortByActivity.this,
							EventsListDisplay.class);
					updateEventList.putExtra("isSingleCategory", getIntent()
							.getExtras().getBoolean("isSingleCategory"));
					updateEventList.putExtra(
							Constants.MENU_ITEM_ID_INTENT_EXTRA,
							getIntent().getExtras().getString(
									Constants.MENU_ITEM_ID_INTENT_EXTRA));
					updateEventList.putExtra(
							Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
							getIntent().getExtras().getString(
									Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
					updateEventList.putExtra("mLinkId", getIntent().getExtras()
							.getString("mLinkId"));
					updateEventList.putExtra("catName", getIntent().getExtras()
							.getString("catName"));
					updateEventList.putExtra("catId", getIntent().getExtras()
							.getString("catId"));
					updateEventList.putExtra("fundId", getIntent().getExtras()
							.getString("fundId"));
					updateEventList.putExtra("retailerId", getIntent().getExtras()
							.getString("retailerId"));
					updateEventList.putExtra("retListId", getIntent().getExtras()
							.getString("retListId"));
					updateEventList.putExtra("retailLocationId", getIntent()
							.getExtras().getString("retailLocationId"));
					updateEventList.putExtra("retailerEvents", getIntent()
							.getExtras().getString("retailerEvents"));

					startActivity(updateEventList);

					finish();

			}
		});

		mTableRowDate = (TableRow) findViewById(R.id.events_groupby_date_tableRow);
		mTableRowAtoZ = (TableRow) findViewById(R.id.events_groupby_atoz_tableRow);
		mTableRowType = (TableRow) findViewById(R.id.events_groupby_type_tableRow);

		mTableRowDate.setOnClickListener(this);
		mTableRowAtoZ.setOnClickListener(this);
		mTableRowType.setOnClickListener(this);

		mTableRowDateSort = (TableRow) findViewById(R.id.events_sortby_date_tableRow);
		mTableRowNameSort = (TableRow) findViewById(R.id.events_sortby_name_tableRow);
		mTableRowDistanceSort = (TableRow) findViewById(R.id.events_sortby_distance_tableRow);
		mTableRowCitySort = (TableRow) findViewById(R.id.sortby_city_tableRow);


		mTableRowDateSort.setOnClickListener(this);
		mTableRowNameSort.setOnClickListener(this);
		mTableRowDistanceSort.setOnClickListener(this);
		mTableRowCitySort.setOnClickListener(this);


		dateGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox1);
		atozGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox2);
		typeGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox3);

		if ("date".equals(group)) {
			dateGroupCheck.setVisibility(View.VISIBLE);
		} else if ("atoz".equals(group)) {
			atozGroupCheck.setVisibility(View.VISIBLE);
		} else if ("type".equals(group)) {
			typeGroupCheck.setVisibility(View.VISIBLE);
		}

		citySort = (TextView) findViewById(R.id.textView_sort_city);

		citySort.setOnClickListener(this);

		dateSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox1);
		nameSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox2);
		distanceSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox3);
		citySortCheck = (CheckBox) findViewById(R.id.sortby_city_checkBox);

		if ("date".equals(sort)) {
			dateSortCheck.setVisibility(View.VISIBLE);
		} else if ("name".equals(sort)) {
			nameSortCheck.setVisibility(View.VISIBLE);
		} else if ("distance".equals(sort)) {
			distanceSortCheck.setVisibility(View.VISIBLE);
		} else if ("city".equals(sort)) {
			citySortCheck.setVisibility(View.VISIBLE);
			CitySort.isDefaultCitySort = true;
		}

		if (Properties.isRegionApp) {
			mTableRowCitySort.setVisibility(View.VISIBLE);
			citySorting = new CitySort(activity, ll, citySortCheck,
					mTableRowCitySort, "Events", mItemId, mBottomId,
					getIntent().getExtras().getString("fundId"), getIntent()
							.getExtras().getString("retailerId"), getIntent()
							.getExtras().getString("retailLocationId"),
					getIntent().getExtras().getString("retailerEvents"));
		} else {
			mTableRowCitySort.setVisibility(View.GONE);
		}

	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentViewWithCustomTitleBar();

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		HubCityContext mHubCity = (HubCityContext) getApplicationContext();
		mHubCity.setGroupSelection(group);
		mHubCity.setSortSelection(sort);

		Intent updateEventList = new Intent(EventsSortByActivity.this,
				EventsListDisplay.class);
		updateEventList.putExtra("isSingleCategory", getIntent().getExtras()
				.getBoolean("isSingleCategory"));
		updateEventList.putExtra(
				Constants.MENU_ITEM_ID_INTENT_EXTRA,
				getIntent().getExtras().getString(
						Constants.MENU_ITEM_ID_INTENT_EXTRA));
		updateEventList.putExtra(
				Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
				getIntent().getExtras().getString(
						Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
		updateEventList.putExtra("mLinkId",
				getIntent().getExtras().getString("mLinkId"));
		updateEventList.putExtra("catName",
				getIntent().getExtras().getString("catName"));
		updateEventList.putExtra("catId",
				getIntent().getExtras().getString("catId"));
		updateEventList.putExtra("fundId",
				getIntent().getExtras().getString("fundId"));
		updateEventList.putExtra("retailerId", getIntent().getExtras()
				.getString("retailerId"));
		updateEventList.putExtra("retListId", getIntent().getExtras()
				.getString("retListId"));
		updateEventList.putExtra("retailLocationId", getIntent().getExtras()
				.getString("retailLocationId"));
		updateEventList.putExtra("retailerEvents", getIntent().getExtras()
				.getString("retailerEvents"));
		startActivity(updateEventList);

		finish();

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.events_groupby_date_tableRow:
			eventsGroupbyDateTableRow();
			break;

		case R.id.events_groupby_atoz_tableRow:
			eventsGroupbyAtozTableRow();
			break;

		case R.id.events_groupby_type_tableRow:
			eventsGroupbyTypeTableRow();
			break;

		case R.id.events_sortby_date_tableRow:
			eventsSortbyDateTableRow();
			break;

		case R.id.events_sortby_name_tableRow:
			eventsSortbyNameTableRow();
			break;

		case R.id.events_sortby_distance_tableRow:
			eventsSortbyDistanceTableRow();
			break;

		case R.id.sortby_city_tableRow:
			eventsSortbyCityTableRow();
			break;

		case R.id.textView_sort_city:
			eventsSortbyCityTableRow();
			break;

		default:
			break;

		}

	}

	private void eventsSortbyCityTableRow() {
		distanceSortCheck.setVisibility(View.INVISIBLE);
		dateSortCheck.setVisibility(View.INVISIBLE);
		nameSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.VISIBLE);
		ll.setVisibility(View.GONE);
		citySorting.sortAction();

		sortChoice = "city";
	}

	private void eventsGroupbyAtozTableRow() {
		atozGroupCheck.setVisibility(View.VISIBLE);
		typeGroupCheck.setVisibility(View.INVISIBLE);
		dateGroupCheck.setVisibility(View.INVISIBLE);

		groupChoice = "atoz";

	}

	private void eventsGroupbyTypeTableRow() {
		typeGroupCheck.setVisibility(View.VISIBLE);
		dateGroupCheck.setVisibility(View.INVISIBLE);
		atozGroupCheck.setVisibility(View.INVISIBLE);

		groupChoice = "type";

	}

	private void eventsGroupbyDateTableRow() {
		dateGroupCheck.setVisibility(View.VISIBLE);
		atozGroupCheck.setVisibility(View.INVISIBLE);
		typeGroupCheck.setVisibility(View.INVISIBLE);

		groupChoice = "date";

	}

	private void eventsSortbyDateTableRow() {
		dateSortCheck.setVisibility(View.VISIBLE);
		nameSortCheck.setVisibility(View.INVISIBLE);
		distanceSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);

		sortChoice = "date";

	}

	private void eventsSortbyNameTableRow() {
		nameSortCheck.setVisibility(View.VISIBLE);
		dateSortCheck.setVisibility(View.INVISIBLE);
		distanceSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);

		sortChoice = "name";

	}

	private void eventsSortbyDistanceTableRow() {
		distanceSortCheck.setVisibility(View.VISIBLE);
		dateSortCheck.setVisibility(View.INVISIBLE);
		nameSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);

		sortChoice = "distance";

	}

}
