package com.hubcity.android.screens;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.scansee.hubregion.R;

@SuppressWarnings("deprecation")
public class FaceBookActivity extends Activity {

	private Facebook facebook;
	private SharedPreferences mPrefs;
	private ProgressDialog mProgress;
	AlertDialog.Builder alertDialogBuilder;
	String appPlayStoreLink;
	Session.StatusCallback statusCallback = new SessionStatusCallback();
	private String module = null,imagePath = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		getIntentExtras();
		facebook = new Facebook(getResources().getString(
				R.string.facebook_app_id));

		loginToFacebook();
	}
	private void getIntentExtras(){
		Intent intent = getIntent();
		appPlayStoreLink = getIntent().getExtras()
				.getString("appPlayStoreLink");
		if (intent.hasExtra("imagePath")){
			imagePath = getIntent().getExtras()
					.getString("imagePath");
		}
		if (intent.hasExtra("moduleType")){
			module = getIntent().getExtras()
					.getString("moduleType");
		}

	}

	/**
	 * Function to login into facebook
	 */
	public void loginToFacebook() {

		Session session = Session.getActiveSession();
		if (session == null) {
			Session.openActiveSession(FaceBookActivity.this, true,
					statusCallback);
		} else if (session.isOpened()) {

			mPrefs = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor editor = mPrefs.edit();
			editor.putString("access_token", Session.getActiveSession()
					.getAccessToken());
			editor.putLong("access_expires", Session.getActiveSession()
					.getExpirationDate().getTime());
			editor.apply();
			new PostToWall().execute();

		} else if ("OPENING".equals(session.getState().name())) {
			Session.openActiveSession(FaceBookActivity.this, true,
					statusCallback);

		} else if ("CLOSED_LOGIN_FAILED".equals(session.getState().name())) {
			alertDialogBuilder = new AlertDialog.Builder(FaceBookActivity.this);

			alertDialogBuilder.setMessage("Unable to Login into Facebook");
			alertDialogBuilder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						}
					});
			alertDialogBuilder.show();

		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_CANCELED) {
			finish();
		} else {
			Session.getActiveSession().onActivityResult(FaceBookActivity.this,
					requestCode, resultCode, data);
		}

	}

	private class PostToWall extends AsyncTask<String, Void, String> {
		String response = null;

		@Override
		protected void onPreExecute() {
			mProgress = new ProgressDialog(FaceBookActivity.this);
			mProgress.setMessage("Posting .....");
			mProgress.setCancelable(false);
			mProgress.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {

				mPrefs = getPreferences(MODE_PRIVATE);
				String accessToken = mPrefs.getString("access_token", null);
				long expires = mPrefs.getLong("access_expires", 0);

				if (accessToken != null) {
					facebook.setAccessToken(accessToken);
				}

				if (expires != 0) {
					facebook.setAccessExpires(expires);
				}

				facebook.authorize(FaceBookActivity.this,
						new String[]{"publish_actions", "manage_pages"},

						new DialogListener() {

							@Override
							public void onComplete(Bundle values) {
							}

							@Override
							public void onFacebookError(FacebookError error) {
							}

							@Override
							public void onError(DialogError e) {
							}

							@Override
							public void onCancel() {
							}
						});

				Bundle parameters = new Bundle();

				parameters.putString("message", getIntent().getExtras()
						.getString("userText"));

				parameters.putString("link", appPlayStoreLink);
				if ("BandSummery".equalsIgnoreCase(module) || "BandEventSummery".equalsIgnoreCase(module)
						|| "NewsDetails".equalsIgnoreCase(module) && imagePath!= null) {
					parameters.putString("picture", imagePath);
					//parameters.putString("message",name);

				}
				response = facebook.request("me/feed", parameters, "POST");
				return response;

			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			mProgress.cancel();

			try {
				Log.d("Facebook share : ",result);

				JSONObject json = null;

				if (result != null) {
					json = new JSONObject(result);
				}

				if (result == null || "".equals(result) || json.has("error")) {
					alertDialogBuilder = new AlertDialog.Builder(
							FaceBookActivity.this);

					alertDialogBuilder
							.setMessage("Error Occurred While Sharing..");
					alertDialogBuilder.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									finish();
								}
							});
					alertDialogBuilder.show();
				} else {

					if (getIntent().hasExtra("module")
							&& "share".equals(getIntent().getExtras()
									.getString("module"))) {

						if (getIntent().hasExtra("shareDetails")) {
							String[] shareDetails = getIntent().getExtras()
									.getStringArray("shareDetails");
							UserTrackingTask userTracking = new UserTrackingTask(
									FaceBookActivity.this, shareDetails[0],
									shareDetails[1], shareDetails[2],
									shareDetails[3], shareDetails[4],
									shareDetails[5], shareDetails[6]);
							userTracking.execute();
						}

					}

					alertDialogBuilder = new AlertDialog.Builder(
							FaceBookActivity.this);

					alertDialogBuilder.setMessage("Shared");
					alertDialogBuilder.setCancelable(false);
					alertDialogBuilder.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									finish();
								}
							});
					alertDialogBuilder.show();

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

	}

	private class SessionStatusCallback implements Session.StatusCallback {

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			// Check if Session is Opened or not
			processSessionStatus(session, state, exception);
		}
	}

	public void processSessionStatus(Session session, SessionState state,
			Exception exception) {

		try {
			if (session != null && session.isOpened()) {

				mPrefs = getPreferences(MODE_PRIVATE);
				SharedPreferences.Editor editor = mPrefs.edit();
				editor.putString("access_token", Session.getActiveSession()
						.getAccessToken());
				editor.putLong("access_expires", Session.getActiveSession()
						.getExpirationDate().getTime());
				editor.apply();
				new PostToWall().execute();

			} else if (session != null
					&& "CLOSED_LOGIN_FAILED".equals(session.getState().name())) {
				alertDialogBuilder = new AlertDialog.Builder(
						FaceBookActivity.this);

				alertDialogBuilder.setMessage("Unable to Login into Facebook");
				alertDialogBuilder.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								finish();
							}
						});
				alertDialogBuilder.show();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
