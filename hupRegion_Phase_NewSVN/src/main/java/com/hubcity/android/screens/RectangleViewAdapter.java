package com.hubcity.android.screens;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

import java.util.ArrayList;

public class RectangleViewAdapter extends ArrayAdapter<MainMenuBO> {

    private final GridView gridview;
    Activity mContext;
    int resourceId;
    ArrayList<MainMenuBO> data;
    private CustomImageLoader customImageLoader;
    String btnFontColor, btnColor, mFontColor, smFontColor, fontColor;
    String level;
    int height;
    String template;


    private static int width = -1;

    public RectangleViewAdapter(Activity context, int layoutResourceId,
            ArrayList<MainMenuBO> data, String menuLevel, String mFontColor,
            String smFontColor, int height, String template, GridView gridview) {
        super(context, layoutResourceId, data);
        this.mContext = context;
        this.resourceId = layoutResourceId;
        this.data = data;
        this.mFontColor = mFontColor;
        this.smFontColor = smFontColor;
        level = menuLevel;
        customImageLoader = new CustomImageLoader(context, false);
        this.height = height;
        DisplayMetrics metrics = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;
        this.template = template;
        this.gridview = gridview;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MainMenuBO item = data.get(position);
        String itmItemImgUrl = item.getmItemImgUrl();

        View itemView = convertView;
        ViewHolder holder = null;

        if (itemView == null) {
            final LayoutInflater layoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = layoutInflater.inflate(resourceId, parent, false);

            holder = new ViewHolder();
            holder.imgItem = (ImageView) itemView.findViewById(R.id.imgItem);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    width / 4, height / 3);

            if ("4X4 Grid".equalsIgnoreCase(template)) {
                layoutParams.width = (width / 4) - (gridview.getVerticalSpacing()*4);
                layoutParams.height = (height / 4) - (gridview.getHorizontalSpacing()*4);

            } else {
                layoutParams.width = (width / 4) - (layoutParams.leftMargin * 5);
            }

            holder.imgItem.setScaleType(ScaleType.FIT_XY);
            holder.imgItem.setLayoutParams(layoutParams);

            itemView.setTag(holder);

        } else {
            holder = (ViewHolder) itemView.getTag();
        }

        customImageLoader.displayImage(itmItemImgUrl, mContext, holder.imgItem);

        if (level != null && "1".equals(level)) {
            fontColor = mFontColor;

        } else {
            fontColor = smFontColor;

        }

        return itemView;

    }

    static class ViewHolder {
        ImageView imgItem;
        TextView txtItem;
    }
}
