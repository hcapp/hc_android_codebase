package com.hubcity.android.screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;


import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;

import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.RoundedCornerImageLoader;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.ArrayList;


/**
 * Created by subramanya.v on 2/1/2016.
 */

public class MainMenuTwoImageTemplate extends MenuPropertiesActivity {

    private ImageView mBannerImageView;
    private ImageView mRightImage;
    private ImageView mLeftImage;
    private TextView mLeftText;
    private TextView mRightText;
    private int isDisplayLabel;
    private LinearLayout mTwoImageViewParent;
    private LinearLayout mTwoImageViewChild;
    private RelativeLayout mRightLayout;
    private ProgressBar progressBar;
    private RelativeLayout bannerHolder;
    int screenHeight;
    String tempBkgrdImg;
    int imageHeight;
    private Context mContext;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;
    private View menuListTemplate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = MainMenuTwoImageTemplate.this;
        try {
            CommonConstants.hamburgerIsFirst = true;
            Intent intent = getIntent();
            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);


            RelativeLayout Parent = new RelativeLayout(this);
            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            Parent.setLayoutParams(param);

            LinearLayout ParentIntermideate = new LinearLayout(this);
            ParentIntermideate.setOrientation(LinearLayout.VERTICAL);
            handleIntent(intent);
            bindView();
            ViewTreeObserver observer = mTwoImageViewChild.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    //suggested by client based on aspect ratio for killeen image changes
                   /* parentWidth = mTwoImageViewParent.getWidth();
                    float w = 1440;
                    float h = 2384;
                    parentHeight = (h / w) * parentWidth;
                    ViewGroup.LayoutParams params = mTwoImageViewParent.getLayoutParams();
                    params.height = (int) parentHeight;

                    float bannerImageHeight = 225;
                    float bannerWidth = 1440;
                    bannerHeight = (bannerImageHeight / bannerWidth) * parentWidth;
                    ViewGroup.LayoutParams bannerParams = bannerHolder.getLayoutParams();
                    // Changes the height and width to the specified pixels
                    bannerParams.height = (int) bannerHeight;
                    if (null != mBannerImg) {
                        mBannerImageView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                        mBannerImg = mBannerImg.replaceAll(" ", "%20");
                        Picasso.with(MainMenuTwoImageTemplate.this).load(mBannerImg).resize((int)
                         parentWidth, (int) bannerHeight).into(mBannerImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        bannerHolder.setVisibility(View.GONE);
                    }*/

                    //Dont remove below code
                    screenHeight = (int) ((mLeftImage.getWidth()) * 1.02);
                    mLeftImage.setMaxHeight(screenHeight);
                    mRightImage.setMaxHeight(screenHeight);

                    ViewTreeObserver obs = mTwoImageViewChild.getViewTreeObserver();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }
                }

            });


            setListener();
            setVisibility();

            LinearLayout.LayoutParams gridViewParentParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            BottomButtonListSingleton mBottomButtonListSingleton;
            mBottomButtonListSingleton = BottomButtonListSingleton
                    .getListBottomButton();

            if (mBottomButtonListSingleton != null) {

                ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                        .listBottomButton;

                if (listBottomButton != null && listBottomButton.size() > 1) {
                    int px = (int) (53 * this.getResources().getDisplayMetrics().density + 0.5f);
                    gridViewParentParam.setMargins(0, 0, 0, px);
                }
            }

            mTwoImageViewParent.setLayoutParams(gridViewParentParam);
            LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            ParentIntermideate.setLayoutParams(parentParam);
            addTitleBar(ParentIntermideate, Parent);
            ParentIntermideate.addView(mTwoImageViewParent);
            Parent.addView(ParentIntermideate);

            createbottomButtontTab(Parent);
            setContentView(Parent);

            RoundedCornerImageLoader CustomImageLoader = new RoundedCornerImageLoader
                    (MainMenuTwoImageTemplate.this,
                            false);
            if (mainMenuUnsortedList.size() < 2) {
                mRightLayout.setVisibility(View.GONE);
            } else {
                MainMenuBO itemRight = mainMenuUnsortedList.get(1);
                String mItemRightImgUrl = itemRight.getmItemImgUrl();
                String mItemRightName = itemRight.getmItemName();
                mRightText.setText(mItemRightName);
                String rightButColor;
                if (itemRight.getmBtnColor() != null && !"N/A".equals(itemRight.getmBtnColor())) {
                    rightButColor = itemRight.getmBtnFontColor();
                    mRightText.setTextColor(Color.parseColor(rightButColor));
                } else {
                    if (itemRight.getSmBtnColor() != null && !"N/A".equals(itemRight.getSmBtnColor()
                    )) {
                        rightButColor = itemRight.getSmBtnFontColor();
                        mRightText.setTextColor(Color.parseColor(rightButColor));
                    }
                }

                mRightImage.setMaxHeight(imageHeight);
                CustomImageLoader.displayImage(mItemRightImgUrl, MainMenuTwoImageTemplate.this,
                        mRightImage);
            }
            MainMenuBO itemLeft = mainMenuUnsortedList.get(0);
            String mItemLeftImgUrl = itemLeft.getmItemImgUrl();
            String mItemLeftName = itemLeft.getmItemName();
            mLeftText.setText(mItemLeftName);
            String leftButColor;
            if (itemLeft.getmBtnColor() != null && !"N/A".equals(itemLeft.getmBtnColor())) {
                leftButColor = itemLeft.getmBtnFontColor();
                mLeftText.setTextColor(Color.parseColor(leftButColor));
            } else {
                if (itemLeft.getSmBtnColor() != null && !"N/A".equals(itemLeft.getSmBtnColor())) {
                    leftButColor = itemLeft.getSmBtnFontColor();
                    mLeftText.setTextColor(Color.parseColor(leftButColor));
                }
            }

            CustomImageLoader.displayImage(mItemLeftImgUrl, MainMenuTwoImageTemplate.this,
                    mLeftImage);
            if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
                HubCityContext.level = 0;
            } else {
                HubCityContext.level = HubCityContext.level + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void bindView() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        menuListTemplate = layoutInflater.inflate(R.layout.two_image_template,
                null, false);

// Initializing drawer layout
        drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);
        mRightLayout = (RelativeLayout) menuListTemplate
                .findViewById(R.id.twoImageViewChildRight);
        bannerHolder = (RelativeLayout) menuListTemplate.findViewById(R.id.banner_holder);
        bannerParent = (LinearLayout)menuListTemplate. findViewById(R.id.bannerParent);
        mBannerImageView = (ImageView) menuListTemplate
                .findViewById(R.id.top_image);
        mBannerImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        progressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
        mRightImage = (ImageView) menuListTemplate
                .findViewById(R.id.rightImage);
        mLeftImage = (ImageView) menuListTemplate
                .findViewById(R.id.leftImage);
        mLeftText = (TextView) menuListTemplate
                .findViewById(R.id.leftText);
        mRightText = (TextView) menuListTemplate
                .findViewById(R.id.rightText);
        mTwoImageViewParent = (LinearLayout) menuListTemplate
                .findViewById(R.id.twoImageViewParent);

        mTwoImageViewChild = (LinearLayout) menuListTemplate
                .findViewById(R.id.twoImageViewChild);
        if (tempBkgrdImg != null && !tempBkgrdImg.equals("")) {
            new ImageLoaderAsync(mTwoImageViewChild).execute(tempBkgrdImg);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        activity = this;
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        super.onNewIntent(intent);
    }

    private void setVisibility() {

        if (isDisplayLabel == 1) {
            mLeftText.setVisibility(View.VISIBLE);
            mRightText.setVisibility(View.VISIBLE);
        }

        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if (null != mBannerImg) {
                mBannerImageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(MainMenuTwoImageTemplate.this).load(mBannerImg).into(mBannerImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        }else {
            customHorizontalTicker = (CustomHorizontalTicker)menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker)menuListTemplate. findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher)menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext,customHorizontalTicker,customVerticalTicker,bannerParent,customRotate, bannerHolder);
        }

    }

    private void setListener() {
        mLeftImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener(0);


            }
        });
        mLeftText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener(0);


            }
        });
        mRightImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener(1);
            }
        });
        mRightText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener(1);
            }
        });

    }

    private void onClickListener(int position) {
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);
        mMainMenuBO.setmBkgrdColor(mBkgrdColor);
        mMainMenuBO.setmBkgrdImage(mBkgrdImage);
        mMainMenuBO.setSmBkgrdColor(smBkgrdColor);
        linkClickListener(mMainMenuBO.getLinkTypeName(), mMainMenuBO);
    }


    class ImageLoaderAsync extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(MainMenuTwoImageTemplate.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {

//suggested by client for killeen image dimension changes based on aspect ratio
              /*  InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
                mIcon11 = Bitmap.createScaledBitmap(mIcon11, (int) parentWidth, (int) parentHeight, true);*/

                //Actual implementation
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {
            try {
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                BitmapDrawable background = new BitmapDrawable(bmImg);
                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    bmImage.setBackgroundDrawable(background);
                } else {
                    bmImage.setBackgroundDrawable(background);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);

            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("smBkgrdImage");
            departFlag = intent.getExtras().getString("departFlag");
            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
            mFontColor = intent.getExtras().getString("mFontColor");
            smFontColor = intent.getExtras().getString("smFontColor");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");
            if (intent.hasExtra("isDisplayLabel")) {
                isDisplayLabel = intent.getExtras().getInt("isDisplayLabel");
            }
            if (intent.hasExtra("TempBkgrdImg")) {
                tempBkgrdImg = intent.getExtras().getString("TempBkgrdImg");
            }

        }

        super.handleIntent(intent);

    }


}

