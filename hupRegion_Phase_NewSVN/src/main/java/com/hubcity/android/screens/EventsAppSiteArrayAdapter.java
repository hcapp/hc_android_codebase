package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class EventsAppSiteArrayAdapter extends
		ArrayAdapter<HashMap<String, String>> {


	private ArrayList<HashMap<String, String>> items;
	private LayoutInflater vi;

	boolean isSpecials, isFundraiser;
	String isAppsiteFlag;
	Activity activity;

	public EventsAppSiteArrayAdapter(Activity activity,
			ArrayList<HashMap<String, String>> items, boolean isSpecials) {
		super(activity, 0, items);
		this.activity = activity;
		this.items = items;
		this.isSpecials = isSpecials;
		vi = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


	}

	public EventsAppSiteArrayAdapter(Context context,
			ArrayList<HashMap<String, String>> items, String isAppsiteFlag,
			boolean isFundraiser) {
		super(context, 0, items);

		this.items = items;
		this.isAppsiteFlag = isAppsiteFlag;
		this.isFundraiser = isFundraiser;
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		final HashMap<String, String> item = items.get(position);
		if (item != null) {

			v = vi.inflate(R.layout.events_appsite_row_item, parent,false);

			final LinearLayout lLayout = (LinearLayout) v
					.findViewById(R.id.events_appsite_layout);

			final TextView title = (TextView) v
					.findViewById(R.id.events_appsite_name);
			title.setEnabled(false);

			final TextView address1 = (TextView) v
					.findViewById(R.id.events_appsite_address1);
			address1.setEnabled(false);

			final TextView address2 = (TextView) v
					.findViewById(R.id.events_appsite_address2);
			address2.setEnabled(false);

			final ImageView imageView = (ImageView) v
					.findViewById(R.id.event_icon);
			imageView.setEnabled(false);

			if (isSpecials) {
				address2.setVisibility(View.GONE);

				if (title != null && item.get("retailerName") != null) {
					title.setText(item.get("retailerName"));
				}

				if (item.get("completeAddress") != null) {
					address1.setText(item.get("completeAddress"));
				}

				lLayout.setGravity(Gravity.LEFT);

				if (imageView != null) {
					imageView.setVisibility(View.VISIBLE);
					if (item.get("retImagePath") != null) {
						new DownloadImageTask(imageView).execute(item
								.get("retImagePath"));
					}

				}

			} else if (isFundraiser) {
				lLayout.setGravity(Gravity.LEFT);
				imageView.setVisibility(View.GONE);
				title.setVisibility(View.VISIBLE);

				if (title != null) {
					if ("1".equals(isAppsiteFlag)) {
						if (item.get("appSiteName") != null) {
							title.setText(item.get("appSiteName"));
						}

					} else if ("0".equals(isAppsiteFlag)) {

						if (item.get("retailerName") != null) {
							title.setText(item.get("retailerName"));
						}
					} else {
						title.setVisibility(View.INVISIBLE);
					}
				}

				if (!"".equals(isAppsiteFlag) || null != isAppsiteFlag) {

					if (imageView != null && item.get("imagePath") != null) {
						imageView.setVisibility(View.VISIBLE);
						try {
							String imagePath = item.get("imagePath").replace(
									" ", "%20");
							new DownloadImageTask(imageView).execute(imagePath);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				} else {
					lLayout.setGravity(Gravity.CENTER);
				}

				if (item.get("completeAddress") != null) {
					address1.setText(item.get("completeAddress"));
				}

			} else {

				address2.setVisibility(View.VISIBLE);

				if (title != null && item.get("appSiteName") != null) {
					title.setText(item.get("appSiteName"));
				}

				// New Changes
				if (address1 != null) {
					if ("1".equals(isAppsiteFlag)) {
						// Image with Address
						lLayout.setGravity(Gravity.LEFT);

						if (imageView != null) {
							imageView.setVisibility(View.VISIBLE);
							if (item.get("logoImagePath") != null) {
								new DownloadImageTask(imageView).execute(item
										.get("logoImagePath"));
							}

						}

						if (title != null) {
							title.setVisibility(View.VISIBLE);
						}

						if (address2 != null) {
							address2.setVisibility(View.GONE);
						}

						if (item.get("address") != null) {
							address1.setText(item.get("address"));

						}

					} else if ("0".equals(isAppsiteFlag)) {

						lLayout.setGravity(Gravity.CENTER);

						if (imageView != null) {
							imageView.setVisibility(View.GONE);
						}

						if (title != null) {
							title.setVisibility(View.GONE);
						}

						if (address2 != null) {
							address2.setVisibility(View.VISIBLE);
						}

						if (item.get("address1") != null) {
							address1.setText(item.get("address1"));

						}

						if (item.get("address2") != null) {
							address2.setText(item.get("address2"));
						}

					}

				}

			}
		}

		return v;

	}

	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		private RotateAnimation anim;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
			anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF,
					0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setRepeatCount(Animation.INFINITE);
			anim.setDuration(700);
			this.bmImage.setAnimation(null);
			this.bmImage.setImageResource(R.drawable.loading_button);
			this.bmImage.startAnimation(anim);
			this.bmImage.setAnimation(anim);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0].replaceAll(" ", "%20");
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			bmImage.setAnimation(null);
		}
	}

}
