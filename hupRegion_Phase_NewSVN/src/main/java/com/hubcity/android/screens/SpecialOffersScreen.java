package com.hubcity.android.screens;

import java.lang.ref.WeakReference;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

public class SpecialOffersScreen extends CustomTitleBar {

    String retailerId = "", retailLocationId = "", pageId = "";
    String extLinkFlag = "", externalLink = "";
    String htmlcode = "<html><body style=\"text-align:justify\"> %s </body></Html>";

    ShareInformation shareInfo;
    boolean isShareTaskCalled;

    ArrayList<HashMap<String, String>> locationArray = new ArrayList<>();
    HashMap<String, String> retailerInfo = new HashMap<>();
    String mItemId, bottomBtnId;
    LinearLayout parentLayout;
    TextView specialsName, specialsDates, specialsPromotions;
    ImageView specialsImage;
    WebView shortDescription, longDescription;
    Button locationsBtn;
    Button btnCloseLocPopup;
    private PopupWindow pwindo;
    public ArrayList<HashMap<String, String>> popUplistData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.special_offers);

        try {
            parentLayout = (LinearLayout) findViewById(R.id.parent_layout);
            parentLayout.setVisibility(View.INVISIBLE);

            title.setText("Details");

            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (SubMenuStack.getSubMenuStack() != null
                            && SubMenuStack.getSubMenuStack().size() > 0) {
                        finish();
                    } else {
                        callingMainMenu();
                    }
                }
            });

            if (getIntent().hasExtra("retailerId")) {
                retailerId = getIntent().getExtras().getString("retailerId");
            }

            if (getIntent().hasExtra("retailLocationId")) {
                retailLocationId = getIntent().getExtras().getString(
                        "retailLocationId");
            }

            if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);

            }

            if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)) {
                bottomBtnId = getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);

            }

            if (getIntent().hasExtra("pageId")) {
                pageId = getIntent().getExtras().getString("pageId");
            } else if (getIntent().hasExtra("hotDealId")) {
                pageId = getIntent().getExtras().getString("hotDealId");
            }

            specialsName = (TextView) findViewById(R.id.special_offers_name);
            specialsDates = (TextView) findViewById(R.id.special_offer_dates);
            specialsPromotions = (TextView) findViewById(R.id.specials_promotions);

            specialsImage = (ImageView) findViewById(R.id.specials_image);
            shortDescription = (WebView) findViewById(R.id.specials_short_desc);
            longDescription = (WebView) findViewById(R.id.specials_long_desc);
            locationsBtn = (Button) findViewById(R.id.specials_location);
            locationsBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    new LocationsAsyncTask().execute();
                }
            });

            WebSettings sWebSettings = shortDescription.getSettings();
            sWebSettings.setJavaScriptEnabled(true);
            shortDescription.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

            WebSettings lWebSettings = longDescription.getSettings();
            lWebSettings.setJavaScriptEnabled(true);
            longDescription.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

            new SpecialOfferDetailAsyncTask().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class LocationsAsyncTask extends AsyncTask<Void, Void, Void> {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog progDialog;

        String responseCode = "10000", responseText = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDialog = new ProgressDialog(SpecialOffersScreen.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setCancelable(false);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                String special_offers_locations_url = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/getspecialoffloclist";

                String urlParameters = mUrlRequestParams
                        .getSpecialOffersLocations(retailerId, pageId);

                JSONObject xmlResponse = mServerConnections.getUrlPostResponse(
                        special_offers_locations_url, urlParameters, true);
                if (xmlResponse != null) {
                    if (xmlResponse.has("RetailerDetail")) {
                        JSONObject locationsJson = xmlResponse
                                .getJSONObject("RetailerDetail");

                        if (locationsJson.has("responseCode")) {
                            responseCode = locationsJson
                                    .getString("responseCode");
                        }

                        if (locationsJson.has("retDetailList")) {
                            JSONArray retailerDetailJson = new JSONArray();

                            try {
                                retailerDetailJson.put(locationsJson
                                        .getJSONObject("retDetailList")
                                        .getJSONObject("RetailerDetail"));

                            } catch (Exception e) {
                                retailerDetailJson = locationsJson
                                        .getJSONObject("retDetailList")
                                        .getJSONArray("RetailerDetail");
                            }

                            locationArray = new ArrayList<>();

                            for (int i = 0; i < retailerDetailJson.length(); i++) {
                                HashMap<String, String> retailerDetailHash = new HashMap<>();
                                JSONObject retailerInfo = retailerDetailJson
                                        .getJSONObject(i);

                                if (retailerInfo.has("retailerId")) {
                                    retailerDetailHash.put("retailerId",
                                            retailerInfo
                                                    .getString("retailerId"));
                                }

                                if (retailerInfo.has("retailerName")) {
                                    retailerDetailHash.put("retailerName",
                                            retailerInfo
                                                    .getString("retailerName"));
                                }

                                if (retailerInfo.has("retailLocationId")) {
                                    retailerDetailHash
                                            .put("retailLocationId",
                                                    retailerInfo
                                                            .getString("retailLocationId"));
                                }

                                if (retailerInfo.has("completeAddress")) {
                                    retailerDetailHash
                                            .put("completeAddress",
                                                    retailerInfo
                                                            .getString("completeAddress"));
                                }

                                if (retailerInfo.has("rowNum")) {
                                    retailerDetailHash.put("rowNum",
                                            retailerInfo.getString("rowNum"));
                                }

                                if (retailerInfo.has("retImagePath")) {
                                    retailerDetailHash.put("retImagePath",
                                            retailerInfo
                                                    .getString("retImagePath"));
                                }

                                locationArray.add(retailerDetailHash);

                            }

                        }

                    } else {
                        responseCode = xmlResponse.getJSONObject("response")
                                .getString("responseCode");
                        responseText = xmlResponse.getJSONObject("response")
                                .getString("responseText");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                progDialog.dismiss();

                if ("10000".equals(responseCode)) {
                    initiateLocationWindow(locationArray);
                } else {
                    displayAlert(responseText);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void initiateLocationWindow(
            final ArrayList<HashMap<String, String>> listData) {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) SpecialOffersScreen.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater
                    .inflate(
                            R.layout.events_location_popup,
                            (ViewGroup) findViewById(R.id.events_location_popup_element));

            DisplayMetrics metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            layout.setBackgroundColor(Color.WHITE);

            pwindo = new PopupWindow(layout, screenWidth - 50,
                    ViewGroup.LayoutParams.WRAP_CONTENT, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            ListView mLocationList = (ListView) layout
                    .findViewById(R.id.events_location_listview);
            EventsAppSiteArrayAdapter adapter = null;
            if (popUplistData == null) {
                popUplistData = listData;
                adapter = new EventsAppSiteArrayAdapter(
                        SpecialOffersScreen.this, popUplistData, true);
            } else {
                adapter = new EventsAppSiteArrayAdapter(
                        SpecialOffersScreen.this, popUplistData, true);
            }

            mLocationList.setAdapter(adapter);
            btnCloseLocPopup = (Button) layout
                    .findViewById(R.id.btn_location_close_popup);
            btnCloseLocPopup.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });

            mLocationList.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int pos, long arg3) {

                    Intent intent = new Intent(SpecialOffersScreen.this,
                            RetailerActivity.class);

                    if (null != listData.get(pos).get(
                            CommonConstants.TAG_RETAIL_ID)) {
                        intent.putExtra(CommonConstants.TAG_RETAIL_ID, listData
                                .get(pos).get("retailerId"));
                    }

                    if (null != listData.get(pos).get("retailLocationId")) {
                        intent.putExtra(Constants.TAG_RETAILE_LOCATIONID,
                                listData.get(pos).get("retailLocationId"));
                    }

                    if (null != listData.get(pos).get("retailerName")) {
                        intent.putExtra(Constants.TAG_APPSITE_NAME, listData
                                .get(pos).get("retailerName"));
                    }

                    intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                            mItemId);
                    intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                            bottomBtnId);

                    startActivity(intent);
                    pwindo.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class SpecialOfferDetailAsyncTask extends
            AsyncTask<Void, Void, Void> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        private ProgressDialog progDialog;

        String responseCode = "10000", responseText = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDialog = new ProgressDialog(SpecialOffersScreen.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setCancelable(false);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                String special_offers_details_url = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/getspecialofferdetails";

                String urlParameters = mUrlRequestParams
                        .getSpecialOffersDetail(retailerId, retailLocationId,
                                pageId);

                JSONObject xmlResponse = mServerConnections
                        .getUrlPostResponseWithHtmlContent(
                                special_offers_details_url, urlParameters, true);
                if (xmlResponse != null) {

                    if (xmlResponse.has("RetailerDetail")) {

                        JSONObject specialDetailsJson = xmlResponse
                                .getJSONObject("RetailerDetail");

                        if (specialDetailsJson.has("responseCode")) {
                            responseCode = specialDetailsJson
                                    .getString("responseCode");
                        }

                        if (specialDetailsJson.has("extLinkFlag")) {
                            extLinkFlag = specialDetailsJson
                                    .getString("extLinkFlag");
                        }

                        if (specialDetailsJson.has("externalLink")) {
                            externalLink = specialDetailsJson
                                    .getString("externalLink");
                        }

                        if (specialDetailsJson.has("retDetailList")) {
                            JSONObject retailerInfoJson = specialDetailsJson
                                    .getJSONObject("retDetailList")
                                    .getJSONObject("RetailerDetail");
                            if (retailerInfoJson.has("retailerId")) {
                                retailerInfo.put("retailerId", retailerInfoJson
                                        .getString("retailerId"));
                            }

                            if (retailerInfoJson.has("retailerPageQRURL")) {
                                retailerInfo
                                        .put("retailerPageQRURL",
                                                retailerInfoJson
                                                        .getString("retailerPageQRURL"));
                            }

                            if (retailerInfoJson.has("pageId")) {
                                retailerInfo.put("pageId",
                                        retailerInfoJson.getString("pageId"));
                            }

                            if (retailerInfoJson.has("pageTitle")) {
                                retailerInfo
                                        .put("pageTitle", retailerInfoJson
                                                .getString("pageTitle"));
                            }

                            if (retailerInfoJson.has("shortDesc")) {
                                retailerInfo
                                        .put("shortDesc", retailerInfoJson
                                                .getString("shortDesc"));
                            }

                            if (retailerInfoJson.has("longDesc")) {
                                retailerInfo.put("longDesc",
                                        retailerInfoJson.getString("longDesc"));
                            }

                            if (retailerInfoJson.has("imageName")) {
                                retailerInfo
                                        .put("imageName", retailerInfoJson
                                                .getString("imageName"));
                            }

                            if (retailerInfoJson.has("startDate")) {
                                retailerInfo
                                        .put("startDate", retailerInfoJson
                                                .getString("startDate"));
                            }

                            if (retailerInfoJson.has("retImagePath")) {
                                retailerInfo.put("retImagePath",
                                        retailerInfoJson
                                                .getString("retImagePath"));
                            }

                            if (retailerInfoJson.has("endDate")) {
                                retailerInfo.put("endDate",
                                        retailerInfoJson.getString("endDate"));
                            }
                        }

                    } else {
                        responseCode = xmlResponse.getJSONObject("response")
                                .getString("responseCode");
                        responseText = xmlResponse.getJSONObject("response")
                                .getString("responseText");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                if ("10000".equals(responseCode)) {

                    // Call for Share Information
                    shareInfo = new ShareInformation(SpecialOffersScreen.this,
                            retailerId, retailLocationId, pageId, "Specials");
                    isShareTaskCalled = false;

                    leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
                    leftTitleImage.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (!Constants.GuestLoginId.equals(UrlRequestParams
                                    .getUid().trim())) {
                                shareInfo.shareTask(isShareTaskCalled);
                                isShareTaskCalled = true;
                            } else {
                                Constants.SignUpAlert(SpecialOffersScreen.this);
                                isShareTaskCalled = false;
                            }
                        }
                    });

                    if ("1".equals(extLinkFlag)) {
                        parentLayout.setVisibility(View.INVISIBLE);

                        Intent browserIntent = new Intent(SpecialOffersScreen.this,
                                ScanseeBrowserActivity.class);
                        browserIntent.putExtra(CommonConstants.URL, externalLink);
                        browserIntent.putExtra("isShare", true);
                        browserIntent.putExtra("module", "Specials");
                        browserIntent.putExtra("retailerId", retailerId);
                        browserIntent.putExtra("pageId", pageId);
                        startActivity(browserIntent);
                        finish();

                    } else if ("0".equals(extLinkFlag)) {
                        if (retailerInfo.get("pageTitle") != null
                                && !"".equals(retailerInfo.get("pageTitle"))) {

                            specialsName.setText(retailerInfo.get("pageTitle"));
                        }

                        if (retailerInfo.get("startDate") != null
                                && !"".equals(retailerInfo.get("startDate"))) {

                            specialsDates.setText("Starts: "
                                    + retailerInfo.get("startDate"));
                        }

                        if (retailerInfo.get("retImagePath") != null
                                && !"".equals(retailerInfo.get("retImagePath"))) {

                            String imagePath = retailerInfo.get("retImagePath");
                            new CustomImageLoader(specialsImage, SpecialOffersScreen.this)
                                    .setScaledImage(imagePath);
                        }

                        if (retailerInfo.get("shortDesc") != null
                                && !"".equals(retailerInfo.get("shortDesc"))) {

                            shortDescription.setVisibility(View.VISIBLE);

                            String sDesc = retailerInfo.get("shortDesc");
                            sDesc = sDesc.replaceAll("&#60;", "<");
                            sDesc = sDesc.replaceAll("&#62;", ">");

                            shortDescription.setWebViewClient(new MyWebViewClient(
                                    SpecialOffersScreen.this, shortDescription,
                                    sDesc));

                            shortDescription.loadData(
                                    String.format(htmlcode, sDesc), "text/html",
                                    "utf-8");
                            shortDescription.setBackgroundColor(Color
                                    .parseColor("#FFFFFF"));
                        } else {
                            shortDescription.setVisibility(View.GONE);
                        }

                        if (retailerInfo.get("longDesc") != null
                                && !"".equals(retailerInfo.get("longDesc"))) {

                            longDescription.setVisibility(View.VISIBLE);

                            String lDesc = retailerInfo.get("longDesc");
                            lDesc = lDesc.replaceAll("&#60;", "<");
                            lDesc = lDesc.replaceAll("&#62;", ">");

                            longDescription.setWebViewClient(new MyWebViewClient(
                                    SpecialOffersScreen.this, longDescription,
                                    lDesc));

                            longDescription.loadData(
                                    String.format(htmlcode, lDesc), "text/html",
                                    "utf-8");
                            longDescription.setBackgroundColor(Color
                                    .parseColor("#FFFFFF"));
                        } else {

                            longDescription.setVisibility(View.GONE);
                        }

                        if (retailerInfo.get("endDate") != null
                                && !"".equals(retailerInfo.get("endDate"))
                                && !"N/A".equalsIgnoreCase(retailerInfo
                                .get("endDate"))) {

                            specialsPromotions.setText("Promotion Ends: "
                                    + retailerInfo.get("endDate"));
                        }

                    }
                } else {
                    displayAlert(responseText);
                }

                progDialog.dismiss();
                parentLayout.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void displayAlert(String responseText) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SpecialOffersScreen.this);

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(responseText);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialogBuilder.show();

    }

    public class MyWebViewClient extends WebViewClient {
        private final WeakReference<Activity> mActivityRef;
        WebView webView;
        String text;
        boolean flag = false;

        public MyWebViewClient(Activity activity, WebView _webView, String _text) {
            mActivityRef = new WeakReference<>(activity);
            webView = _webView;
            text = _text;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // Handles Email Link
            if (url.startsWith("mailto:")) {
                final Activity activity = mActivityRef.get();
                if (activity != null) {
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(activity, mt.getTo(),
                            mt.getSubject(), mt.getBody(), mt.getCc());
                    activity.startActivity(i);
                    view.reload();
                    return true;
                }
            } else if (url != null && url.startsWith("http://")) { // Handles
                // HTTP
                // links
                // (Opens
                // link in
                // device
                // browser)
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // Handles Anchor Tags
            if (url.contains("#") && !flag) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        webView.loadData(String.format(htmlcode, text),
                                "text/html", "utf-8");
                    }
                }, 400);

                flag = true;
            } else {
                flag = false;
            }

        }

        /**
         * Intent to open Email Composer screen
         *
         * @param context
         * @param address
         * @param subject
         * @param body
         * @param cc
         * @return
         */
        private Intent newEmailIntent(Context context, String address,
                                      String subject, String body, String cc) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
            intent.putExtra(Intent.EXTRA_TEXT, body);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_CC, cc);
            intent.setType("message/rfc822");
            return intent;
        }
    }

}
