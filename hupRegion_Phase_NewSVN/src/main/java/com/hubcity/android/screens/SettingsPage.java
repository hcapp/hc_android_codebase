package com.hubcity.android.screens;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

import org.json.JSONObject;

import java.util.ArrayList;

public class SettingsPage extends CustomTitleBar implements OnClickListener
{

	String mItemId = null;
	String mLinkId = null;
	String responseCode = null;
	String userId, retailerName;
	String retListId = null;
	SharedPreferences settings = null;
	CustomImageLoader customImageLoader;
	LinearLayout faq;
	TextView faq_separator;
	FaqFlagAsyncTask faqFlagAsyncTask;
	SharedPreferences csShared;
	int isFaqExists;
	private CustomNavigation customNaviagation;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		try {
			CommonConstants.hamburgerIsFirst = true;

			faq = (LinearLayout) findViewById(R.id.l4);
			faq.setVisibility(View.GONE);

			faq_separator = (TextView) findViewById(R.id.faq_separator);
			faq_separator.setVisibility(View.GONE);
			csShared = this.getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);
			faqFlagAsyncTask = new FaqFlagAsyncTask();
			faqFlagAsyncTask.execute();

			CityPreferencesScreen.bIsSendingTimeStamp = false;
			LinearLayout uInfo = (LinearLayout) findViewById(R.id.l1);
			LinearLayout settings = (LinearLayout) findViewById(R.id.l2);
			LinearLayout preference = (LinearLayout) findViewById(R.id.l3);
			LinearLayout cityPreferences = (LinearLayout) findViewById(R.id.l5);
			LinearLayout csNotification = (LinearLayout) findViewById(R.id.l6);
			TextView cityPreflLineSep = (TextView) findViewById(R.id.city_pref_line_sep);
			if (Properties.isRegionApp) {
				cityPreferences.setVisibility(View.VISIBLE);
				cityPreflLineSep.setVisibility(View.VISIBLE);

			} else {
				cityPreferences.setVisibility(View.GONE);
				cityPreflLineSep.setVisibility(View.GONE);
			}

//		If any notification message saved in preference then displaying the notification list item
			ArrayList<String> arrPushMsg = Constants.getPushArray("gcm_msg");
			if (arrPushMsg != null && arrPushMsg.size() > 0) {
				csNotification.setVisibility(View.VISIBLE);
			} else {
				csNotification.setVisibility(View.GONE);
			}
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText("App Settings");

			leftTitleImage.setVisibility(View.GONE);

			backImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					startMenu();
				}
			});

			uInfo.setOnClickListener(this);
			settings.setOnClickListener(this);
			preference.setOnClickListener(this);
			faq.setOnClickListener(this);
			cityPreferences.setOnClickListener(this);
			csNotification.setOnClickListener(this);

			//user for hamburger in modules
			drawerIcon.setVisibility(View.VISIBLE);
			drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
			customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onClick(View view)
	{
		switch (view.getId()) {

			case R.id.l1:
				startActivityForResult(
						new Intent(this, SetPreferrenceScreen.class).putExtra(
								"regacc", false), Constants.STARTVALUE);
				break;
			case R.id.l2:
				startActivityForResult(new Intent(this,
								SettingsLocationServices.class).putExtra("regacc", false),
						Constants.STARTVALUE);
				break;
			case R.id.l3:
				startActivityForResult(new Intent(this,
								PreferedCatagoriesScreen.class).putExtra("regacc", false),
						Constants.STARTVALUE);
				break;
			case R.id.l4:
				// Starts FAQ screen from the Settings Screen
				startActivityForResult(
						new Intent(this, FaqScreen.class).putExtra("regacc", false)
								.putExtra("isSettings", true), Constants.STARTVALUE);
				break;
			case R.id.l5:
				startActivityForResult(
						new Intent(this, CityPreferencesScreen.class).putExtra(
								"regacc", false), Constants.STARTVALUE);
				break;
			case R.id.l6:

				navigateToPushScreen();
				break;
			default:
				break;

		}
	}

	private void navigateToPushScreen()
	{
		try {
			ArrayList<String> arrPushMsg = Constants.getPushArray("gcm_msg");
			Intent intent;
			if (arrPushMsg.size() > 1) {
				intent = new Intent(this,
						PushNotificationListScreen.class);
			} else {

				JSONObject jsonObject = new JSONObject(arrPushMsg.get(0));
				if (jsonObject.has("rssFeedList") && jsonObject.has("dealList")) {
					intent = new Intent(this,
							PushNotificationListScreen.class);

				} else if (jsonObject.has("rssFeedList")) {
					String newsLink = jsonObject.getJSONArray("rssFeedList")
							.getJSONObject(0)

							.optString("link");
					intent = new Intent(
							this,
							ScanseeBrowserActivity.class);
					intent.putExtra(CommonConstants.URL, newsLink);
				} else {
					intent = Constants.getNotificationDealIntent(jsonObject, this);
				}
			}
			intent.putExtra("Screen", "Settings");
			startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		cancelFaqAsyncTask();
		finish();
	}

	private void cancelFaqAsyncTask()
	{
		if (faqFlagAsyncTask != null && !faqFlagAsyncTask.isCancelled()) {
			faqFlagAsyncTask.cancel(true);
		}

		faqFlagAsyncTask = null;
	}

	private void startMenu()
	{

		cancelFaqAsyncTask();
		finish();
	}

	class FaqFlagAsyncTask extends AsyncTask<Void, Void, Void>
	{

		String returnValue = "sucessfull";
		String responseText = "";
		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();
		ProgressDialog progDialog;

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();

			progDialog = new ProgressDialog(SettingsPage.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage(Constants.DIALOG_MESSAGE);
			progDialog.setCancelable(false);
			progDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params)
		{

			String url_faqs_flag = Properties.url_local_server
					+ Properties.hubciti_version + "firstuse/getfaqsetting";

			String urlParameters = mUrlRequestParams.createFAQsFlagParameter();

			JSONObject jsonResponse = mServerConnections.getUrlPostResponse(
					url_faqs_flag, urlParameters, true);

			try {

				if (jsonResponse.has("UserDetails")) {

					JSONObject json = jsonResponse.getJSONObject("UserDetails");

					if (json.has("isFaqExist")) {
						isFaqExists = json.getInt("isFaqExist");
					}

				} else {
					JSONObject json = jsonResponse.getJSONObject("response");
					responseText = json.getString("responseText");
					returnValue = "UNSUCCESS";
				}
			} catch (Exception e) {
				e.printStackTrace();
				responseText = "No Records Found.";
				returnValue = "UNSUCCESS";
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			super.onPostExecute(result);

			try {
				if ("sucessfull".equals(returnValue)) {
					if (isFaqExists == 1) {
						faq.setVisibility(View.VISIBLE);
						faq_separator.setVisibility(View.VISIBLE);
					} else {
						faq.setVisibility(View.GONE);
						faq_separator.setVisibility(View.GONE);
					}

				} else {
					faq.setVisibility(View.INVISIBLE);
					faq_separator.setVisibility(View.INVISIBLE);
				}

				progDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}

	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		cancelFaqAsyncTask();
	}

}