package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.hubcity.android.businessObjects.DepttNType;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.commonUtil.Utils;
import com.scansee.hubregion.R;

public class SortDepttNTypeScreen extends CustomTitleBar implements
		OnClickListener {

	public static ArrayList<SubMenuDetails> subMenuDetailsList = new ArrayList<>();
	public static final String ALPHABETICALLY = "Alphabetically";
	public static final String NAME = "Name";
	public static final String DEPTT = "Department";
	public static final String TYPE = "Type";
	public static final String CITY = "City";

	private static String UN_SUCCESSFULL = "un_sucessfull";

	String mItemId, mLinkId;
	public static boolean isSortDone;
	public static boolean isSubMenuSort;

	CheckBox atozGroupCheck;

	TextView atozGroup;
	TextView nameSort;
	TextView deptSort;
	TextView typeGroup;
	TextView citySort;

	CheckBox nameSortCheck;
	CheckBox deptSortCheck;
	CheckBox typeSortCheck;
	CheckBox citySortCheck;

	public String groupSelection = "";
	String sortSelection;

	TableRow mTableRowGroupAtoZ;
	TableRow mTableRowSortName;
	TableRow mTableRowSortType;
	TableRow mTableRowSortDept;
	TableRow mTableRowCitySort;
	TableRow mTableRowCityList;

	TextView atozText;

	CheckBox checkBox;
	LinearLayout ll;
	CitySort citySorting;
	Activity activity;

	Set<DepttNType> depttSet; // since type of object of DEPTT and TYPE is
								// same,so we
	// are considering only DEptt as TYPe also

	Set<DepttNType> typeSet;
	protected int selectedGroup;

	String strlinkid = null;
	String typeId = "", deptId = "", selectedCities = "", selectedItem = NAME,
			linkId = "";
	int level = SubMenuStack.getSubMenuStack().size();
	int index = subMenuDetailsList.size() - 1;
	SQLiteDatabase csSqlite;
	String strSelectedName = "";
	String strSelectedCities = "", strSortOrder = "";
	boolean isAlphabetical;

	private void setContentViewWithtitleBar() {
		csSqlite = Utils.creatingDB(this);
		creatingTable();

		setContentView(R.layout.profiles);

		this.title.setText("Group & Sort");
		this.backImage.setVisibility(View.GONE);

		this.leftTitleImage.setBackgroundResource(R.drawable.ic_action_cancel);
		this.leftTitleImage.setTag("Cancel");

		this.leftTitleImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (csSqlite != null) {
					csSqlite.close();
				}
				finish();

			}
		});

		this.rightImage.setBackgroundResource(R.drawable.ic_action_accept);
		this.rightImage.setTag("Done");
		this.rightImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

					String sortOrder = "None";

					if ("atoz".equals(groupSelection)) {
						sortOrder = "ASC";
					}

					// Create MenuAsynkTask
					MainMenuBO mainMenuBO = SubMenuStack.getTopElement();

					if (mainMenuBO != null) {

						isSortDone = true;
						selectedCities = "";

						if (CITY.equalsIgnoreCase(selectedItem)) {
							selectedCities = CitySort.getSelectedCities();
						}

						if (CITY.equalsIgnoreCase(selectedItem)
								|| NAME.equalsIgnoreCase(selectedItem)) {
							typeId = "0";
							deptId = "0";
						}

						if (TYPE.equalsIgnoreCase(selectedItem)) {
							deptId = "0";
						}

						if (DEPTT.equalsIgnoreCase(selectedItem)) {
							typeId = "0";
						}
						isSubMenuSort = true;
						if (subMenuDetailsList != null
								&& !subMenuDetailsList.isEmpty()) {
							if (level == subMenuDetailsList.get(index).getLevel()) {
								subMenuDetailsList.remove(subMenuDetailsList.size() - 1);
							}
						}
						Log.v("", "LINK IN SORT MENU : " + linkId);
						SubMenuDetails subMenuDetails = new SubMenuDetails(deptId,
								typeId, selectedItem, selectedCities, mainMenuBO
								.getLinkId(), mainMenuBO.getmItemId(),
								level);
						subMenuDetailsList.add(subMenuDetails);

						Log.v("",
								"LINK IN SORT MENU AFTER: "
										+ mainMenuBO.getLinkId());
						MenuAsyncTask sortmenu = new MenuAsyncTask(
								SortDepttNTypeScreen.this);
						sortmenu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
								String.valueOf(level), mainMenuBO.getmItemId(),
								mainMenuBO.getLinkId(), sortOrder, deptId, typeId,
								"SubMenuSort", groupSelection);

						if (!gettingSortedvales(mLinkId)) {
							insertingDbValues(mLinkId, selectedItem,
									selectedCities, groupSelection);
						} else {
							updatingDbValues(mLinkId, selectedItem, selectedCities,
									groupSelection);
						}

					}

					// END --MenuAsynkTask


			}

		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		int RESULTCODE = 11;
		if (requestCode == RESULTCODE && requestCode == RESULTCODE) {
			setResult(RESULTCODE);
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	public void creatingTable() {
		if (csSqlite != null) {
			csSqlite.execSQL("create table if not exists sorting("
					+ tableField() + ")");
		}
	}

	public String tableField() {
		return "linkid TEXT,selectedItem TEXT,selectedCities TEXT,sortOrder TEXT";
	}

	public void insertingDbValues(String strlin, String strselecteditem,
			String strselectedcities, String sortorder) {
		ContentValues csContent = new ContentValues();
		csContent.put("linkid", strlin);
		csContent.put("selectedItem", strselecteditem);
		csContent.put("selectedCities", strselectedcities);
		csContent.put("sortOrder", sortorder);
		csSqlite.insert("sorting", null, csContent);
	}

	public void updatingDbValues(String strlin, String strselecteditem,
			String strselectedcities, String sortorder) {
		ContentValues csContent = new ContentValues();
		csContent.put("linkid", strlin);
		csContent.put("selectedItem", strselecteditem);
		csContent.put("selectedCities", strselectedcities);
		csContent.put("sortOrder", sortorder);
		csSqlite.update("sorting", csContent, "linkid=" + strlin, null);
	}

	public boolean gettingSortedvales(String linkid) {
		boolean bIsflag = false;
		Cursor csCur = csSqlite.rawQuery("select * from sorting where linkid="
				+ linkid, null);
		if (csCur != null && csCur.getCount() > 0) {
			csCur.moveToFirst();
			strlinkid = csCur.getString(csCur.getColumnIndex("linkid"));
			strSelectedName = csCur.getString(csCur
					.getColumnIndex("selectedItem"));
			strSelectedCities = csCur.getString(csCur
					.getColumnIndex("selectedCities"));
			strSortOrder = csCur.getString(csCur.getColumnIndex("sortOrder"));
			csCur.moveToNext();
			csCur.close();
			bIsflag = true;
		} else {
			bIsflag = false;
		}
		return bIsflag;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentViewWithtitleBar();

		activity = this;

		ll = (LinearLayout) findViewById(R.id.city_ll);
		CitySort.isDefaultCitySort = false;

		isSubMenuSort = false;

		String departFlag = getIntent().getExtras().getString("departFlag");
		String typeFlag = getIntent().getExtras().getString("typeFlag");

		mItemId = getIntent().getExtras().getString("mItemId");
		mLinkId = getIntent().getExtras().getString("mLinkId");
		Log.v("", "LINK ID FOR SORT : " + mLinkId);

		mTableRowGroupAtoZ = (TableRow) findViewById(R.id.atoz_tableRow);
		mTableRowGroupAtoZ.setOnClickListener(this);

		atozText = (TextView) findViewById(R.id.atoz_textView);
		atozText.setOnClickListener(this);

		mTableRowSortName = (TableRow) findViewById(R.id.name_tableRow);
		mTableRowSortType = (TableRow) findViewById(R.id.type_tableRow);
		mTableRowSortDept = (TableRow) findViewById(R.id.department_tableRow);
		mTableRowCitySort = (TableRow) findViewById(R.id.sortby_city_tableRow);
		mTableRowCityList = (TableRow) findViewById(R.id.city_list_tableRow);

		mTableRowSortName.setOnClickListener(this);
		mTableRowSortType.setOnClickListener(this);
		mTableRowSortDept.setOnClickListener(this);
		mTableRowCitySort.setOnClickListener(this);

		nameSort = (TextView) findViewById(R.id.name_textView);
		deptSort = (TextView) findViewById(R.id.department_textView);
		typeGroup = (TextView) findViewById(R.id.type_textView);
		citySort = (TextView) findViewById(R.id.textView_sort_city);

		nameSort.setOnClickListener(this);
		deptSort.setOnClickListener(this);
		typeGroup.setOnClickListener(this);
		citySort.setOnClickListener(this);

		nameSortCheck = (CheckBox) findViewById(R.id.name_checkBox);
		deptSortCheck = (CheckBox) findViewById(R.id.department_checkBox);
		typeSortCheck = (CheckBox) findViewById(R.id.type_checkBox);
		citySortCheck = (CheckBox) findViewById(R.id.sortby_city_checkBox);

		atozGroupCheck = (CheckBox) findViewById(R.id.atoz_checkBox);

		if ("".equals(groupSelection)) {
			atozGroupCheck.setVisibility(View.INVISIBLE);
		} else if ("atoz".equals(groupSelection)) {
			atozGroupCheck.setVisibility(View.VISIBLE);
		}

		boolean isType, isDeptt;
		if (departFlag != null && "0".equalsIgnoreCase(departFlag)) {
			isDeptt = false;
		} else {
			isDeptt = true;
		}

		if (typeFlag != null && "0".equalsIgnoreCase(typeFlag)) {
			isType = false;
		} else {
			isType = true;
		}

		if (isType) {
			mTableRowSortType.setVisibility(View.VISIBLE);
		} else {
			mTableRowSortType.setVisibility(View.GONE);
		}

		if (isDeptt) {
			mTableRowSortDept.setVisibility(View.VISIBLE);
		} else {
			mTableRowSortDept.setVisibility(View.GONE);
		}

		setDefaultValues();

	}

	private void setDefaultValues() {

		typeId = "0";
		deptId = "0";
		selectedCities = "";

		if (gettingSortedvales(mLinkId)) {
			selectedItem = strSelectedName;
			linkId = strlinkid;
			groupSelection = strSortOrder;
		} else {
			selectedItem = NAME;
			linkId = mLinkId;
			groupSelection = "";
		}

		Log.v("", "PREFERENCE SORT BY NAME : " + selectedItem + " , " + CITY);
		if (CITY.equalsIgnoreCase(selectedItem)) {
			citySortCheck.setVisibility(View.VISIBLE);
			CitySort.isDefaultCitySort = true;

		} else if (NAME.equalsIgnoreCase(selectedItem)) {
			nameSortCheck.setVisibility(View.VISIBLE);

		} else if (DEPTT.equalsIgnoreCase(selectedItem)) {
			deptSortCheck.setVisibility(View.VISIBLE);

		} else if (TYPE.equalsIgnoreCase(selectedItem)) {
			typeSortCheck.setVisibility(View.VISIBLE);
		}
		if ("".equals(groupSelection)) {
			isAlphabetical = false;
			atozGroupCheck.setVisibility(View.INVISIBLE);
		} else if ("atoz".equals(groupSelection)) {
			isAlphabetical = true;
			atozGroupCheck.setVisibility(View.VISIBLE);
		}
		if (subMenuDetailsList != null && !subMenuDetailsList.isEmpty()) {

			Log.v("", "SUBMENUDETAILS LEVEL : " + level + " , "
					+ subMenuDetailsList.get(index).getLevel());
			if (level == subMenuDetailsList.get(index).getLevel()) {

				typeId = subMenuDetailsList.get(index).getType();
				deptId = subMenuDetailsList.get(index).getDept();
				Log.v("", "SUBMENUDETAILS NAME : " + selectedItem);
				if (CITY.equals(selectedItem)) {
					if (gettingSortedvales(mLinkId)) {
						selectedCities = strSelectedCities;
					} else {
						selectedCities = subMenuDetailsList.get(index)
								.getSelectedCities();

					}

				}

				linkId = subMenuDetailsList.get(index).getmLinkId();

			}
		}
		if (Properties.isRegionApp) {

			mTableRowCitySort.setVisibility(View.VISIBLE);

			citySorting = new CitySort(activity, ll, citySortCheck,
					mTableRowCitySort, "SubMenu", linkId, typeId, deptId,
					level, selectedCities);
		} else {
			mTableRowCitySort.setVisibility(View.GONE);
		}

	}

	class DepttTypeListAsynkTask extends AsyncTask<String, String, String> {

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();
		private ProgressDialog progDialog;
		private String groupType;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDialog = new ProgressDialog(SortDepttNTypeScreen.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage(Constants.DIALOG_MESSAGE);
			progDialog.setCancelable(false);
			progDialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			progDialog.dismiss();
			if ("Success".equalsIgnoreCase(result)) {

				if (groupType.equalsIgnoreCase(DEPTT) && depttSet != null) {
					createListAlertDialog(depttSet, DEPTT);
				}

				else if (groupType.equalsIgnoreCase(TYPE) && typeSet != null) {
					createListAlertDialog(typeSet, TYPE);
				}
			}

			else {// for UN_SUCCESSFULL
				if (logInResponseData != null) {
					String responseText = logInResponseData.get("responseText");
					progDialog.dismiss();
					Utils.createAlert(SortDepttNTypeScreen.this, responseText);
				}
			}
		}

		@Override
		protected String doInBackground(String... params) {
			groupType = params[0];

			String fName = "";

			if ("Department".equalsIgnoreCase(groupType)) {
				fName = "dept";

			} else if ("Type".equalsIgnoreCase(groupType)) {
				fName = "type";

			}

			try {
				String url_citi_dept_and_menu_type = Properties.url_local_server
						+ Properties.hubciti_version
						+ "firstuse/deptandmenutype";
				String urlParameters = mUrlRequestParams.getDepttAndType(fName);
				JSONObject xmlResponde = mServerConnections.getUrlPostResponse(
						url_citi_dept_and_menu_type, urlParameters, true);
				return jsonParseResponse(xmlResponde);

			} catch (Exception e) {
				e.printStackTrace();
				return UN_SUCCESSFULL;
			}
		}

		private Hashtable<String, String> logInResponseData;
		private String mItemTypeName;
		private String mItemTypeId;
		private String departmentName;
		private String departmentId;

		// private void
		public String jsonParseResponse(JSONObject xmlResponde) {

			JSONObject localJSONObject;
			logInResponseData = new Hashtable<>();

			depttSet = new HashSet<>();
			typeSet = new HashSet<>();

			if (xmlResponde == null) {
				return UN_SUCCESSFULL;
			}

			try {
				if (xmlResponde.has("Menu")) {
					localJSONObject = xmlResponde.getJSONObject("Menu");
					JSONObject mItemListObj = localJSONObject
							.getJSONObject("mItemList");
					JSONArray mMenuItemList = mItemListObj
							.optJSONArray("MenuItem");

					if (mMenuItemList == null) {

						JSONObject menuItemJsonObj = mItemListObj
								.optJSONObject("MenuItem");

						if (menuItemJsonObj.has("mItemTypeName")) {
							mItemTypeName = menuItemJsonObj
									.getString("mItemTypeName");
						}
						if (menuItemJsonObj.has("mItemTypeId")) {
							mItemTypeId = menuItemJsonObj
									.getString("mItemTypeId");

						}
						if (menuItemJsonObj.has("departmentName")) {
							departmentName = menuItemJsonObj
									.getString("departmentName");
						}
						if (menuItemJsonObj.has("departmentId")) {
							departmentId = menuItemJsonObj
									.getString("departmentId");
						}

						if (departmentName != null) {
							depttSet.add(new DepttNType(departmentId,
									departmentName));
						}

						if (mItemTypeName != null) {
							typeSet.add(new DepttNType(mItemTypeId,
									mItemTypeName));
						}

					} else {

						for (int i = 0; i < mMenuItemList.length(); i++) {
							JSONObject menuItemJsonObj = mMenuItemList
									.getJSONObject(i);

							if (menuItemJsonObj.has("mItemTypeName")) {
								mItemTypeName = menuItemJsonObj
										.getString("mItemTypeName");
							}
							if (menuItemJsonObj.has("mItemTypeId")) {
								mItemTypeId = menuItemJsonObj
										.getString("mItemTypeId");
							}
							if (menuItemJsonObj.has("departmentName")) {
								departmentName = menuItemJsonObj
										.getString("departmentName");
							}
							if (menuItemJsonObj.has("departmentId")) {
								departmentId = menuItemJsonObj
										.getString("departmentId");
							}

							if (departmentName != null) {
								depttSet.add(new DepttNType(departmentId,
										departmentName));
							}

							if (mItemTypeName != null) {
								typeSet.add(new DepttNType(mItemTypeId,
										mItemTypeName));
							}

						}
					}
					String SUCCESSFULL = "Success";
					return SUCCESSFULL;
				} else if (xmlResponde
						.has(Constants.ERROR_REGISTRATION_RESPONSE)) {
					localJSONObject = xmlResponde
							.getJSONObject(Constants.ERROR_REGISTRATION_RESPONSE);
					String responseCode = localJSONObject
							.getString("responseCode");
					String responseText = localJSONObject
							.getString("responseText");
					logInResponseData.put("responseCode", responseCode);
					logInResponseData.put("responseText", responseText);
					return UN_SUCCESSFULL;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return UN_SUCCESSFULL;
			}
			return UN_SUCCESSFULL;
		}

		@SuppressWarnings("rawtypes")
		private void createListAlertDialog(final Set<DepttNType> set,
				final String text1) {
			String text = null;
			if (text1.equalsIgnoreCase(DEPTT)) {
				text = "Select " + DEPTT;
			} else if (text1.equalsIgnoreCase(TYPE)) {
				text = "Select " + TYPE;
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(
					SortDepttNTypeScreen.this);
			builder.setTitle(text);
			List<DepttNType> sortlist = new ArrayList<>(set);
			Collections.sort(sortlist, new Comparator<DepttNType>() {

				@Override
				public int compare(DepttNType lhs, DepttNType rhs) {
					return lhs.getName().compareTo(rhs.getName());
				}

			});

			Iterator iterator = sortlist.iterator();
			CharSequence[] items = new CharSequence[set.size()];

			ArrayList<DepttNType> arrayList = new ArrayList<>();
			int i = 0;
			while (iterator.hasNext()) {
				DepttNType deptt = (DepttNType) iterator.next();
				items[i] = deptt.getName();
				arrayList.add(deptt);

				i++;
			}

			final ArrayList<DepttNType> arrayList1 = arrayList;

			builder.setItems(items, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {

					DepttNType deptt = arrayList1.get(item);// here we are
					// considering Deptt and
					// type business object
					// is same ,BECOUSE all
					// members of same
					// typeAISAIS

					if (text1.equalsIgnoreCase(DEPTT)) {
						deptId = deptt.getId();

					} else if (text1.equalsIgnoreCase(TYPE)) {
						typeId = deptt.getId();

					}
				}
			});

			AlertDialog alert = builder.create();
			alert.show();

		}
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.name_textView:
			sortByName();
			break;

		case R.id.department_textView:
			sortByDepartment();
			break;

		case R.id.type_textView:
			sortByType();
			break;

		case R.id.textView_sort_city:
			sortByCity();
			break;

		case R.id.name_tableRow:
			sortByName();
			break;

		case R.id.department_tableRow:
			sortByDepartment();
			break;

		case R.id.type_tableRow:
			sortByType();
			break;

		case R.id.sortby_city_tableRow:
			sortByCity();
			break;

		case R.id.atoz_tableRow:
			atozGroupCheck();
			break;

		case R.id.atoz_textView:
			atozGroupCheck();
			break;

		default:
			break;

		}

	}

	private void atozGroupCheck() {
		isAlphabetical = !isAlphabetical;

		if (isAlphabetical) {
			atozGroupCheck.setVisibility(View.VISIBLE);
			groupSelection = "atoz";
		} else {
			atozGroupCheck.setVisibility(View.INVISIBLE);
			groupSelection = "";
		}
	}

	private void sortByName() {
		nameSortCheck.setVisibility(View.VISIBLE);
		deptSortCheck.setVisibility(View.INVISIBLE);
		typeSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);

		ll.setVisibility(View.GONE);
		selectedItem = NAME;
	}

	private void sortByDepartment() {
		nameSortCheck.setVisibility(View.INVISIBLE);
		deptSortCheck.setVisibility(View.VISIBLE);
		typeSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);

		ll.setVisibility(View.GONE);
		selectedItem = DEPTT;

		new DepttTypeListAsynkTask().execute(DEPTT);
	}

	private void sortByType() {
		nameSortCheck.setVisibility(View.INVISIBLE);
		deptSortCheck.setVisibility(View.INVISIBLE);
		typeSortCheck.setVisibility(View.VISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);

		ll.setVisibility(View.GONE);
		selectedItem = TYPE;

		new DepttTypeListAsynkTask().execute(TYPE);
	}

	private void sortByCity() {
		nameSortCheck.setVisibility(View.INVISIBLE);
		deptSortCheck.setVisibility(View.INVISIBLE);
		typeSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.VISIBLE);

		ll.setVisibility(View.GONE);
		citySorting.sortAction();
		selectedItem = CITY;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		finish();
	}

}
