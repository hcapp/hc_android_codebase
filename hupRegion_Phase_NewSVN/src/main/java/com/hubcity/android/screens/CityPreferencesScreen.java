package com.hubcity.android.screens;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.model.CityModel;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CityPreferencesScreen extends CustomTitleBar implements
        OnClickListener {
    public static boolean bIsSendingTimeStamp = false;
    static boolean isregis = false;
    private String btnFontColor;
    private String btnColor;
    protected Button saveCityPrefbtn;
    protected ListView cityPrefListView = null;
    protected CityPrefListAdapter cityListAdapter = null;
    protected String responseText;
    String strPushRegMsg;
    private HashMap<String, Object> cityListDataHashMap = new HashMap<>();
    private ArrayList<HashMap<String, Object>> cityListArrayList = new ArrayList<>();

    protected ArrayList<String> citiIdList = new ArrayList<>();
    Button footerBackButton;
    private ArrayList<CityModel> cityList = new ArrayList<>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.city_preferences);
        strPushRegMsg = this.getIntent().getStringExtra(
                Constants.PUSH_NOTIFY_MSG);
        try {
            bIsSendingTimeStamp = false;
            title.setSingleLine(true);
            title.setText("City Preferences");
            title.setTextSize(14);
            leftTitleImage.setVisibility(View.GONE);
            btnColor = getIntent().getExtras().getString("btnColor");
            btnFontColor = getIntent().getExtras().getString("btnFontColor");
            (findViewById(R.id.bottom_bar)).setBackgroundColor(Color.parseColor(Constants.getNavigationBarValues("titleBkGrdColor")));
            if (getIntent().getExtras().getBoolean("regacc")) {
                rightImage.setVisibility(View.GONE);
                divider.setVisibility(View.GONE);
                backImage.setVisibility(View.GONE);
            }

            cityPrefListView = (ListView) findViewById(R.id.city_preferences_listitem);
            saveCityPrefbtn = (Button) findViewById(R.id.city_preferences_save);
            saveCityPrefbtn.setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
            saveCityPrefbtn.setOnClickListener(this);
            try {
                isregis = getIntent().getExtras().getBoolean("regacc");
                if (isregis) {
                } else {
//                    saveCityPrefbtn
//                            .setBackgroundResource(R.drawable.ic_action_save);
                    saveCityPrefbtn.setText("Save");
                }
            } catch (Exception e) {
                isregis = false;
            }
            if (isregis) {
                footerBackButton = (Button) findViewById(R.id.city_preference_back);
                footerBackButton.setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
                footerBackButton.setVisibility(View.VISIBLE);
                footerBackButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        finish();

                    }
                });
            }

            new GetCityList().execute();

            cityPrefListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    int isSelected = (Integer) cityListAdapter.cityArrayList.get(
                            position).get("isCityChecked");

                    if (position == 0 && isSelected == 1) {
                        for (int i = 1; i < cityListAdapter.cityArrayList.size(); i++) {
                            cityListAdapter.cityArrayList.get(i).put(
                                    "isCityChecked", 0);
                            cityListAdapter.count = 0;
                        }
                    } else if (position == 0 && isSelected == 0) {
                        if (cityListAdapter.count < cityListAdapter.cityArrayList
                                .size() && cityListAdapter.count != 0) {
                            for (int i = 1; i < cityListAdapter.cityArrayList
                                    .size(); i++) {
                                cityListAdapter.cityArrayList.get(i).put(
                                        "isCityChecked", 0);
                                cityListAdapter.count = 0;
                            }
                        } else if (cityListAdapter.count == 0) {
                            for (int i = 1; i < cityListAdapter.cityArrayList
                                    .size(); i++) {
                                cityListAdapter.cityArrayList.get(i).put(
                                        "isCityChecked", 1);
                                cityListAdapter.count = cityListAdapter.cityArrayList
                                        .size() - 1;

                            }
                        }
                    } else if (position != 0 && isSelected == 1) {
                        cityListAdapter.cityArrayList.get(position).put(
                                "isCityChecked", 0);
                        if (cityListAdapter.count > 0) {
                            cityListAdapter.count--;
                        }

                    } else if (position != 0 && isSelected == 0) {
                        cityListAdapter.cityArrayList.get(position).put(
                                "isCityChecked", 1);
                        if (cityListAdapter.count < cityListAdapter.cityArrayList
                                .size() - 1) {
                            cityListAdapter.count++;
                        }

                    }

                    if (cityListAdapter.count < cityListAdapter.cityArrayList
                            .size() - 1) {
                        if (cityListAdapter.count < 1) {
                            cityListAdapter.cityArrayList.get(0).put(
                                    "isCityChecked", 0);
                        }
                    } else {
                        cityListAdapter.cityArrayList.get(0)
                                .put("isCityChecked", 1);
                    }

                    cityListAdapter = new CityPrefListAdapter(
                            CityPreferencesScreen.this,
                            cityListAdapter.cityArrayList, cityListAdapter.count);
                    Parcelable stateMain = cityPrefListView.onSaveInstanceState();
                    cityPrefListView.setAdapter(cityListAdapter);
                    cityPrefListView.onRestoreInstanceState(stateMain);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // @Beena:for saving the selected cities
    private class SavePreferences extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        JSONObject jsonObject = null;
        private ProgressDialog mDialog;
        private boolean isCitiesSelected = false;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(CityPreferencesScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            String result = "false";
            try {
                isCitiesSelected = citiIdList.size() != 0;
                String urlParameters = mUrlRequestParams
                        .createSaveCityPrefParameter(citiIdList,
                                isCitiesSelected);
                String url_update_city_preferences = Properties.url_local_server
                        + Properties.hubciti_version
                        + "firstuse/updatecitypref";
                jsonObject = mServerConnections.getUrlPostResponse(
                        url_update_city_preferences, urlParameters, true);
                if (jsonObject != null) {
                    if ("10000".equalsIgnoreCase(jsonObject.getJSONObject(
                            "response").getString("responseCode"))) {
                        result = "true";
                        getAllCities();
                        Constants.IS_PREFERENCE_CHANGED = true;
                        Constants.IS_SUB_MENU_REFRESHED = true;
                    }
                }

            } catch (Exception e) {
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                mDialog.dismiss();
                // @Beena:If coming from registration screen, launch category
                // preferences screen
                if (isregis) {
                    Intent prenext = new Intent(CityPreferencesScreen.this,
                            PreferedCatagoriesScreen.class);
                    prenext.putExtra("regacc", true);
                    prenext.putExtra("btnColor", btnColor);
                    prenext.putExtra("btnFontColor", btnFontColor);
                    if (strPushRegMsg != null) {
                        prenext.putExtra(Constants.PUSH_NOTIFY_MSG,
                                strPushRegMsg);
                    }
                    startActivity(prenext);
                } else {
                    // show dialog for success/unsucessful saving of selected cities
                    Builder findAlert = new Builder(
                            CityPreferencesScreen.this);

                    findAlert.setPositiveButton(getString(R.string.specials_ok),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    dialog.cancel();
                                    finish();
                                }
                            });

                    if ("true".equals(result)) {
                        HubCityContext.savedSubMenuCityIds = new HashMap<>();
                        CityPreferencesScreen.this.deleteDatabase("HupCity.db");
                        HubCityContext hubCityContext = (HubCityContext) getApplicationContext();
                        hubCityContext.clearArrayAndAllValues(true);
                        bIsSendingTimeStamp = true;
                        findAlert
                                .setMessage(getString(R.string.city_pref_info_msg));
                    } else {
                        findAlert
                                .setMessage(getString(R.string.city_pref_error_msg));
                    }
                    findAlert.create().show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.city_preferences_save:
                citiIdList = new ArrayList<>();
                addSelectedCities();
                CityPreferencesScreen.this.deleteDatabase("HupCity.db");
                MenuAsyncTask.dateModified = "";
                new SavePreferences().execute();

                break;
            default:
                break;
        }
    }

    // @Beena : Add selected cities, CitiIds to arraylist
    private void addSelectedCities() {
        cityList.clear();
        for (HashMap<String, Object> item : cityListAdapter.cityArrayList) {
            if ((item.get("isCityChecked").equals(1))) {
                if (!item.get("cityId").equals("#0")) {
                    citiIdList.add((String) item.get("cityId"));
                    CityModel cityItem = new CityModel();
                    Log.d("city id :", item.get("cityId").toString());
                    cityItem.setCityId(item.get("cityId").toString());
                    cityItem.setCityName(item.get("cityName").toString());
                    cityList.add(cityItem);
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        cityPrefListView.setAdapter(null);
        super.onDestroy();
    }

    // @Beena:for getting list of cities which are associated with region App

    private class GetCityList extends AsyncTask<String, Void, String> {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        JSONObject jsonObjectCity = null;
        JSONObject jsonObjectCityList = new JSONObject();
        JSONArray jsonArrayCityList = new JSONArray();
        JSONObject responseMenuObject = null;
        int count = 0;
        private ProgressDialog mDialog;

        @Override
        protected String doInBackground(String... params) {
            cityListArrayList = new ArrayList<>();
            String result = "false";
            try {
                String urlParameters = mUrlRequestParams
                        .createCityCatUrlParameter();
                String url_get_city_preferences = Properties.url_local_server
                        + Properties.hubciti_version + "firstuse/getcitypref";
                jsonObjectCity = mServerConnections.getUrlPostResponse(
                        url_get_city_preferences, urlParameters, true);
                if (jsonObjectCity != null) {
                    if (jsonObjectCity.has("City")) {
                        JSONObject cityObject = jsonObjectCity
                                .getJSONObject("City");

                        if (cityObject.has("response")) {

                            JSONObject responseJsonObj;
                            try {
                                responseJsonObj = cityObject
                                        .getJSONObject("response");
                                responseText = responseJsonObj
                                        .getString("responseText");

                            } catch (JSONException e) {
                            }

                        } else if (cityObject.has("cityList")) {
                            try {

                                responseText = cityObject
                                        .getString("responseText");

                                jsonObjectCityList = cityObject
                                        .getJSONObject("cityList");

                                try {

                                    jsonArrayCityList.put(jsonObjectCityList
                                            .getJSONObject("City"));
                                } catch (Exception e) {

                                    jsonArrayCityList = jsonObjectCityList
                                            .getJSONArray("City");
                                }

                                cityListDataHashMap.put("cityName", "All");
                                cityListDataHashMap.put("isCityChecked", 1);
                                cityListDataHashMap.put("cityId", "#0");
                                cityListArrayList.add(cityListDataHashMap);
                                for (int arrayCount = 0; arrayCount < jsonArrayCityList
                                        .length(); arrayCount++) {
                                    cityListDataHashMap = new HashMap<>();
                                    responseMenuObject = jsonArrayCityList
                                            .getJSONObject(arrayCount);
                                    cityListDataHashMap.put("cityName",
                                            responseMenuObject
                                                    .getString("cityName"));
                                    cityListDataHashMap.put("isCityChecked",
                                            responseMenuObject
                                                    .getInt("isCityChecked"));
                                    cityListDataHashMap.put("cityId",
                                            responseMenuObject
                                                    .getString("cityId"));
                                    cityListArrayList.add(cityListDataHashMap);
                                }

                            } catch (JSONException e) {

                            }
                        }
                    } else {
                        JSONObject responseObj;
                        try {
                            responseObj = jsonObjectCity
                                    .getJSONObject("response");
                            responseText = responseObj
                                    .getString("responseText");

                        } catch (JSONException e) {
                        }
                    }
                }
                result = "true";
            } catch (Exception e) {
            }
            return result;

        }

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(CityPreferencesScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                mDialog.dismiss();
                if ("true".equals(result) && responseText.equals("Success")) {
                    int listCount = CheckSelectedCities(count);
                    cityListAdapter = new CityPrefListAdapter(
                            CityPreferencesScreen.this, cityListArrayList,
                            listCount);
                    cityPrefListView.setAdapter(cityListAdapter);

                } else {

                    Builder notificationAlert = new Builder(
                            CityPreferencesScreen.this);
                    notificationAlert.setMessage(responseText).setPositiveButton(
                            getString(R.string.specials_ok),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            });
                    notificationAlert.create().show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private int CheckSelectedCities(int count) {
        try {
            for (int i = 1; i < cityListArrayList.size(); i++) {
                if ((Integer) cityListArrayList.get(i).get("isCityChecked") == 1) {
                    if (count < cityListArrayList.size()) {
                        count++;
                    }
                }
            }

        } catch (Exception e) {

        }
        return count;
    }

    private void getAllCities() {
        if (cityList.size() == 0) {
            for (HashMap<String, Object> hashMapObj : cityListArrayList) {
                if (!hashMapObj.get("cityName").toString().equalsIgnoreCase("All")) {
                    CityModel item = new CityModel();
                    item.setCityId(hashMapObj.get("cityId").toString());
                    item.setCityName(hashMapObj.get("cityName").toString());
                    cityList.add(item);
                }
            }
        }
        new CityDataHelper().storePreferedCities(CityPreferencesScreen.this, cityList);
    }

}
