package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

public class CityExperienceFilterActivity extends CustomTitleBar implements
        OnClickListener {
    private ArrayList<HashMap<String, String>> filterList = null;
    String citiExpId;
    private static final String TAG_CITYEXPERIENCE = "CitiExperience";
    public static final String TAG_FILTERNAME = "retAffName";
    public static final String TAG_FILTERID = "retAffId";
    public static final String TAG_FILTER_IMG_URL = "retAffImg";
    public static final String TAG_RESPONE = "response";
    public static final String CITYEXP_IMG_URL = "cityExpImgUlr";
    public static final String FILTERCOUNT_EXTRA = "retAffCount";
    ListView filterlistView;
    FilterListAdapter filterListAdapter;
    String cityExpImgUlr;
    protected String mItemId, mBottomBtnId;
    private String mBkgrdColor;
    private String mBkgrdImage;
    private String mBtnColor;
    private String mBtnFontColor;

    private String appPlaystoreLInk, appleStoreLink, appIconImg;

    Activity activity;
    BottomButtons bb = null;
    LinearLayout linearLayout = null;
    boolean hasBottomBtns = false;
    boolean enableBottomButton = true;
    private CustomNavigation customNaviagation;
    private String responseText;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonConstants.hamburgerIsFirst = true;
        mContext = CityExperienceFilterActivity.this;
        title.setSingleLine(false);
        title.setMaxLines(2);
       /* title.setText("Choose Filter :");*/
        title.setText("Select Interest Area:");
        leftTitleImage.setVisibility(View.GONE);

        setContentView(R.layout.city_exp_filter);

        activity = CityExperienceFilterActivity.this;

        linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);

        try {
            // Initiating Bottom button class
            bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
            // Add screen name when needed
            bb.setActivityInfo(activity, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        citiExpId = getIntent().getExtras().getString(
                Constants.CITI_EXPID_INTENT_EXTRA);
        if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                && getIntent().getExtras().getString(
                Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
            mItemId = getIntent().getExtras().getString(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA);
        }

        if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                && getIntent().getExtras().getString(
                Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
            mBottomBtnId = getIntent().getExtras().getString(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
        }

        if (getIntent().hasExtra("mBkgrdColor")) {
            mBkgrdColor = getIntent().getExtras().getString("mBkgrdColor");
        }
        if (getIntent().hasExtra("mBkgrdImage")) {
            mBkgrdImage = getIntent().getExtras().getString("mBkgrdImage");
        }
        if (getIntent().hasExtra("mBtnColor")) {
            mBtnColor = getIntent().getExtras().getString("mBtnColor");
        }
        if (getIntent().hasExtra("mBtnFontColor")) {
            mBtnFontColor = getIntent().getExtras().getString("mBtnFontColor");
        }

        appPlaystoreLInk = getIntent().getExtras().getString(
                Constants.APP_STORE_LINK_INTENT_EXTRA);
        appleStoreLink = getIntent().getExtras().getString("downLoadLink");
        appIconImg = getIntent().getExtras().getString(
                Constants.APP_ICON_INTENT_EXTRA);

        cityExpImgUlr = getIntent().getExtras().getString(CITYEXP_IMG_URL);
        filterlistView = (ListView) findViewById(R.id.lst_city_exp_filter);

        new GetFiltes().execute();
        filterlistView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long pst) {
                if (filterList.get(position).get(TAG_FILTERID) != null) {
                    getRetailerforpartner(
                            filterList.get(position).get(TAG_FILTERID),
                            filterList.get(position).get(TAG_FILTERNAME));
                }

            }

        });
        //user for hamburger in modules
        drawerIcon.setVisibility(View.VISIBLE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
    }

    private class GetFiltes extends AsyncTask<String, Void, String> {

        ServerConnections mServerConnections = new ServerConnections();
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;
        JSONObject responseMenuObject = null;

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(CityExperienceFilterActivity.this,
                    "", Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {

            filterList = new ArrayList<>();
            String result = "false";
            try {
                String get_cityexp_filter_url = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/getpartners?citiExperId=";
                if (mItemId != null) {
                    jsonObject = mServerConnections.getJSONFromUrl(false,
                            get_cityexp_filter_url + citiExpId + "&userId="
                                    + UrlRequestParams.getUid() + "&menuItemId=" + mItemId, null);
                } else if (mBottomBtnId != null) {
                    jsonObject = mServerConnections.getJSONFromUrl(false,
                            get_cityexp_filter_url + citiExpId + "&userId="
                                    + UrlRequestParams.getUid() + "&bottomBtnId=" + mBottomBtnId, null);
                }

                if (jsonObject != null) {

                    if (jsonObject.has(TAG_RESPONE)) {

                        int responeCode = Integer.valueOf(jsonObject
                                .getJSONObject(TAG_RESPONE).getString(
                                        CommonConstants.TAG_RESPONE_CODE));
                        switch (responeCode) {
                            case 10005:
                                result = jsonObject
                                        .getJSONObject(TAG_RESPONE)
                                        .getString(CommonConstants.TAG_RESPONE_TEXT);
                                responseText = result;

                                break;
                            case 10002:
                                result = jsonObject
                                        .getJSONObject(TAG_RESPONE)
                                        .getString(CommonConstants.TAG_RESPONE_TEXT);
                                responseText = result;
                                break;
                            case 10001:
                                result = jsonObject
                                        .getJSONObject(TAG_RESPONE)
                                        .getString(CommonConstants.TAG_RESPONE_TEXT);
                                responseText = result;
                                break;
                            default:
                                break;
                        }

                    } else {

                        try {
                            jsonArray = jsonObject
                                    .getJSONObject(TAG_CITYEXPERIENCE)
                                    .getJSONArray(TAG_CITYEXPERIENCE);
                        } catch (Exception e) {
                            jsonArray = null;
                            e.printStackTrace();
                        }
                        if (jsonArray != null) {
                            for (int arrayCount = 0; arrayCount < jsonArray
                                    .length(); arrayCount++) {
                                HashMap<String, String> filterData = new HashMap<String, String>();
                                responseMenuObject = jsonArray
                                        .getJSONObject(arrayCount);
                                filterData.put(TAG_FILTERNAME, responseMenuObject
                                        .getString(TAG_FILTERNAME));
                                filterData.put(TAG_FILTERID,
                                        responseMenuObject.getString(TAG_FILTERID));
                                filterData.put(TAG_FILTER_IMG_URL,
                                        responseMenuObject
                                                .getString(TAG_FILTER_IMG_URL));
                                filterList.add(filterData);
                            }
                        } else {
                            try {
                                responseText = jsonObject
                                        .getJSONObject(TAG_CITYEXPERIENCE).optString("responseText");

                                JSONObject filterObject = jsonObject
                                        .getJSONObject(TAG_CITYEXPERIENCE)
                                        .getJSONObject(TAG_CITYEXPERIENCE);


                                HashMap<String, String> filterData = new HashMap<String, String>();

                                filterData.put(TAG_FILTERNAME, filterObject
                                        .getString(TAG_FILTERNAME));
                                filterData.put(TAG_FILTERID,
                                        filterObject.getString(TAG_FILTERID));
                                filterData.put(TAG_FILTER_IMG_URL,
                                        filterObject
                                                .getString(TAG_FILTER_IMG_URL));
                                filterList.add(filterData);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        result = "true";
                    }

                    // For BottomButton
                    if (jsonObject.getJSONObject(TAG_CITYEXPERIENCE).has(
                            "bottomBtnList")) {
                        if (jsonObject.getJSONObject(TAG_CITYEXPERIENCE)
                                .getJSONObject("bottomBtnList")
                                .has("BottomButton")) {
                            JSONObject jsonResponsee = jsonObject
                                    .getJSONObject(TAG_CITYEXPERIENCE)
                                    .getJSONObject("bottomBtnList");
                            hasBottomBtns = true;

                            try {
                                ArrayList<BottomButtonBO> bottomButtonList = bb
                                        .parseForBottomButton(jsonResponsee);
                                BottomButtonListSingleton
                                        .clearBottomButtonListSingleton();
                                BottomButtonListSingleton
                                        .getListBottomButton(bottomButtonList);

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }

                        } else {
                            hasBottomBtns = false;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            mDialog.dismiss();
            if (responseText != null && !"Success".equals(responseText)) {
                CommonConstants.displayToast(mContext, responseText);
            }
            if ("true".equals(result)) {
                if (filterList.size() == 1) {
                    HashMap<String, String> filterValue = filterList.get(0);
                    if (filterValue.get(TAG_FILTERID) != null) {
                        getRetailerforpartner(
                                filterValue.get(TAG_FILTERID),
                                filterValue.get(TAG_FILTERNAME));
                        finish();
                    }
                } else {
                    filterListAdapter = new FilterListAdapter(
                            CityExperienceFilterActivity.this, filterList);
                    filterlistView.setAdapter(null);
                    filterlistView.setAdapter(filterListAdapter);
                }
            }

            if (hasBottomBtns && enableBottomButton) {

                bb.createbottomButtontTab(linearLayout, false);
                enableBottomButton = false;
            }
        }
    }

    private void getRetailerforpartner(String filterId, String filterName) {
        Intent intent = new Intent(CityExperienceFilterActivity.this,
                CitiExperienceActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mBottomBtnId);
        intent.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, citiExpId);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, "0");
        intent.putExtra(CITYEXP_IMG_URL, cityExpImgUlr);

        intent.putExtra("mBkgrdColor", mBkgrdColor);
        intent.putExtra("mBkgrdImage", mBkgrdImage);
        intent.putExtra("mBtnColor", mBtnColor);
        intent.putExtra("mBtnFontColor", mBtnFontColor);

        intent.putExtra(Constants.APP_STORE_LINK_INTENT_EXTRA, appPlaystoreLInk);
        intent.putExtra("downLoadLink", appleStoreLink);

        intent.putExtra(Constants.APP_ICON_INTENT_EXTRA, appIconImg);

        // sending filter count as 2 , coz this code will be excecuted only if
        // filter count is > 1
        intent.putExtra(FILTERCOUNT_EXTRA, "2");
        intent.putExtra(TAG_FILTERID, filterId);
        intent.putExtra(TAG_FILTERNAME, filterName);
        startActivityForResult(intent, Constants.STARTVALUE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.right_button:
                break;

            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(20002);
        }
        if (requestCode == Constants.STARTVALUE) {
            setResult(20002);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi(customNaviagation);
            CommonConstants.hamburgerIsFirst = false;
        }
        // Add screen name when needed
        if (bb != null) {
            bb.setActivityInfo(activity, "Filters");
        }
    }

}
