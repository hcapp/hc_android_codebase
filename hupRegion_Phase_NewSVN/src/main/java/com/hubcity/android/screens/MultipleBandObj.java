package com.hubcity.android.screens;

import com.hubcity.android.businessObjects.BottomButtonBO;
import java.util.ArrayList;


/**
 * Created by subramanya.v on 9/20/2016.
 */
public class MultipleBandObj
{
private String responseText;
	private String bottomBtn;
	private int maxCnt;
	private int nextPage;
	private String mainMenuId;
	ArrayList<RetailerDetail> retailerDetail ;
	private int maxRowNum;
	private ArrayList<BottomButtonBO>bottomBtnList;

	public ArrayList<BottomButtonBO> getBottomBtnList()
	{
		return bottomBtnList;
	}

	public void setBottomBtnList(ArrayList<BottomButtonBO> bottomBtnList)
	{
		this.bottomBtnList = bottomBtnList;
	}


	public int getMaxRowNum()
	{
		return maxRowNum;
	}

	public void setMaxRowNum(int maxRowNum)
	{
		this.maxRowNum = maxRowNum;
	}

	public int getNextPage()
	{
		return nextPage;
	}

	public void setNextPage(int nextPage)
	{
		this.nextPage = nextPage;
	}

	public String getBottomBtn()
	{
		return bottomBtn;
	}

	public void setBottomBtn(String bottomBtn)
	{
		this.bottomBtn = bottomBtn;
	}

	public String getMainMenuId()
	{
		return mainMenuId;
	}

	public void setMainMenuId(String mainMenuId)
	{
		this.mainMenuId = mainMenuId;
	}

	public int getMaxCnt()
	{
		return maxCnt;
	}

	public void setMaxCnt(int maxCnt)
	{
		this.maxCnt = maxCnt;
	}

	public String getResponseText()
	{
		return responseText;
	}

	public void setResponseText(String responseText)
	{
		this.responseText = responseText;
	}

	public ArrayList<RetailerDetail> getRetailerDetail()
	{
		return retailerDetail;
	}

	public void setRetailerDetail(ArrayList<RetailerDetail> retailerDetail)
	{
		this.retailerDetail = retailerDetail;
	}


}
