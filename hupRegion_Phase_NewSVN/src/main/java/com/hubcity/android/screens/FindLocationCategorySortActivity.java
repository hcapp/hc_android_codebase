package com.hubcity.android.screens;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.HubCityLogger;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.FindLocationActivity;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FindLocationCategorySortActivity extends CustomTitleBar implements
		OnClickListener {

	private ProgressDialog mDialog;

	TextView groupByText;
	TextView atozGroup;
	TextView typeGroup;
	TextView nameSort;
	TextView distanceSort;
	TextView citySort;

	CheckBox atozGroupCheck;
	CheckBox typeGroupCheck;
	CheckBox nameSortCheck;
	CheckBox distanceSortCheck;
	CheckBox citySortCheck;
	CheckBox checkBox;

	String sortChoice = "";
	String groupChoice = "";
	String returnValue = "unSuccessfull";
	String subCatId;
	String subCatIdRef, typeNameRef, groupChoiceRef, sortChoiceRef;
	String mLinkId;
	String catId;
	String catName;
	String typeName;
	String typeText;
	Spinner mspinnerType;
	HashMap<String, String> mTypeDetails;
	ArrayList<HashMap<String, String>> mList;
	ArrayList<String> mSpinnerData;
	ArrayList<HashMap<String, String>> catIdList;

	TableRow mTableRow2GroupAtoZ;
	TableRow mTableRow2GroupType;
	TableRow mTableRowSortName;
	TableRow mTableRowSortDistance;
	TableRow mTableRowCitySort;
	TableRow mTableRowCityList;

	CitySort citySorting;

	LinearLayout ll;
	Activity activity;

	boolean flag;
	boolean isAtoZ, isAtoZRef;

	// Addison filter parameters
	LinearLayout mainLayout;


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_single_category_sort_activity);

		activity = this;

		ll = (LinearLayout) findViewById(R.id.city_ll);
		CitySort.isDefaultCitySort = false;

		// @Rekha
		// Check for Addison Filters
		mainLayout = (LinearLayout) findViewById(R.id.main_layout);

		title.setText("Group & Sort");
		rightImage.setBackgroundResource(R.drawable.ic_action_accept);
		leftTitleImage.setBackgroundResource(R.drawable.ic_action_cancel);

		groupByText = (TextView) findViewById(R.id.event_textView_group_title);

		mspinnerType = (Spinner) findViewById(R.id.find_singl_ecategory_type_spinner);

		if (getIntent().hasExtra("subCatId")) {
			subCatIdRef = getIntent().getExtras().getString("subCatId");
			subCatId = subCatIdRef;
		}

		if (getIntent().hasExtra("typeName")) {
			typeNameRef = getIntent().getExtras().getString("typeName");
			typeName = typeNameRef;
		}

		if (getIntent().hasExtra("groupChoice")) {
			groupChoiceRef = getIntent().getExtras().getString("groupChoice");
			groupChoice = groupChoiceRef;
		}

		if (getIntent().hasExtra("sortChoice")) {
			sortChoiceRef = getIntent().getExtras().getString("sortChoice");
			sortChoice = sortChoiceRef;
		}

		if (getIntent().hasExtra("mLinkId")) {
			mLinkId = getIntent().getExtras().getString("mLinkId");
		}

		if (getIntent().hasExtra("selectedCatId")) {
			catId = getIntent().getExtras().getString("selectedCatId");
		}

		if (getIntent().hasExtra("selectedCat")) {
			catName = getIntent().getExtras().getString("selectedCat");
		}

		atozGroup = (TextView) findViewById(R.id.event_textView_group_atoz);
		atozGroup.setOnClickListener(this);

		typeGroup = (TextView) findViewById(R.id.event_textView_group_type);
		typeGroup.setOnClickListener(this);

		atozGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox2);
		typeGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox3);

		backImage.setVisibility(View.GONE);

		if ("type".equals(groupChoice)) {
			typeGroupCheck.setVisibility(View.VISIBLE);
		}

		if ("atoz".equals(groupChoice)) {
			isAtoZ = true;
			atozGroupCheck.setVisibility(View.VISIBLE);
		}

		groupByText.setVisibility(View.VISIBLE);

		nameSort = (TextView) findViewById(R.id.event_textView_sort_name);
		distanceSort = (TextView) findViewById(R.id.event_textView_sort_distance);
		citySort = (TextView) findViewById(R.id.textView_sort_city);

		citySort.setOnClickListener(this);
		nameSort.setOnClickListener(this);
		distanceSort.setOnClickListener(this);

		nameSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox2);
		distanceSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox3);
		citySortCheck = (CheckBox) findViewById(R.id.sortby_city_checkBox);

		if ("name".equals(sortChoice)) {
			nameSortCheck.setVisibility(View.VISIBLE);
		}

		if ("distance".equals(sortChoice)) {
			distanceSortCheck.setVisibility(View.VISIBLE);
		}

		if ("city".equals(sortChoice)) {
			citySortCheck.setVisibility(View.VISIBLE);
			CitySort.isDefaultCitySort = true;
		}

		mTableRow2GroupAtoZ = (TableRow) findViewById(R.id.find_single_category_tableRow3);
		mTableRow2GroupType = (TableRow) findViewById(R.id.find_single_category_tableRow4);
		mTableRowSortName = (TableRow) findViewById(R.id.find_single_category_tableRow7);
		mTableRowSortDistance = (TableRow) findViewById(R.id.find_single_category_tableRow8);
		mTableRowCitySort = (TableRow) findViewById(R.id.sortby_city_tableRow);
		mTableRowCityList = (TableRow) findViewById(R.id.city_list_tableRow);

		mTableRow2GroupAtoZ.setOnClickListener(this);
		mTableRow2GroupType.setOnClickListener(this);
		mTableRowSortName.setOnClickListener(this);
		mTableRowSortDistance.setOnClickListener(this);
		mTableRowCitySort.setOnClickListener(this);

		if (Properties.isRegionApp) {
			citySort.setVisibility(View.VISIBLE);
		} else {
			citySort.setVisibility(View.GONE);
		}

		mspinnerType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				mspinnerType.setSelection(position);

				if (position != 0) {
					groupChoice = "type";
					subCatId = position + "";

					if (getIntent().getExtras().containsKey("Class")) {
						if ("Find".equals(getIntent().getExtras().getString(
								"Class"))) {
							if (Properties.isRegionApp) {
								atozGroupCheck.setVisibility(View.VISIBLE);
							} else {
								atozGroupCheck.setVisibility(View.GONE);
							}
						}
					} else {
						atozGroupCheck.setVisibility(View.INVISIBLE);
					}

					typeGroupCheck.setVisibility(View.VISIBLE);
					mspinnerType.setVisibility(View.INVISIBLE);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		rightImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

					Intent launchintent = new Intent();

					launchintent.setClass(FindLocationCategorySortActivity.this,
							FindLocationActivity.class);
					launchintent.putExtra("selectedCat", getIntent().getExtras()
							.getString("selectedCat"));
					launchintent.putExtra("selectedCatId", getIntent().getExtras()
							.getString("selectedCatId"));
					launchintent.putExtra("mItemId", getIntent().getExtras()
							.getString("mItemId"));
					launchintent.putExtra("mLinkId", mLinkId);
					launchintent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
							getIntent().getExtras().getString("bottomBtnId"));
					launchintent.putExtra("typeName", typeName);
					launchintent.putExtra("sortChoice", sortChoice);
					launchintent.putExtra("groupChoice", groupChoice);
					launchintent.putExtra("subCatId", subCatId);
					launchintent.putExtra("srchKey", getIntent().getExtras()
							.getString("srchKey"));
					launchintent.putExtra("latitude", getIntent().getExtras()
							.getString("latitude"));
					launchintent.putExtra("longitude", getIntent().getExtras()
							.getString("longitude"));
					launchintent.putExtra("isAtoZ", isAtoZ);

					startActivity(launchintent);
					finish();


			}
		});

		leftTitleImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Intent launchintent = new Intent();
				launchintent.setClass(FindLocationCategorySortActivity.this,
						FindLocationActivity.class);
				launchintent.putExtra("selectedCat", getIntent().getExtras()
						.getString("selectedCat"));
				launchintent.putExtra("selectedCatId", getIntent().getExtras()
						.getString("selectedCatId"));
				launchintent.putExtra("mItemId", getIntent().getExtras()
						.getString("mItemId"));
				launchintent.putExtra("mLinkId", mLinkId);
				launchintent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
						getIntent().getExtras().getString("bottomBtnId"));
				launchintent.putExtra("typeName", typeNameRef);
				launchintent.putExtra("sortChoice", sortChoiceRef);
				launchintent.putExtra("groupChoice", groupChoiceRef);
				launchintent.putExtra("subCatId", subCatIdRef);
				launchintent.putExtra("srchKey", getIntent().getExtras()
						.getString("srchKey"));
				launchintent.putExtra("latitude", getIntent().getExtras()
						.getString("latitude"));
				launchintent.putExtra("longitude", getIntent().getExtras()
						.getString("longitude"));
				launchintent.putExtra("isAtoZ", isAtoZRef);

				startActivity(launchintent);
				finish();

			}
		});

		if (Properties.isRegionApp) {
			mTableRowCitySort.setVisibility(View.VISIBLE);
			mTableRow2GroupAtoZ.setVisibility(View.VISIBLE);

			citySorting = new CitySort(activity, ll, citySortCheck,
					mTableRowCitySort, "Find All", getIntent().getExtras()
							.getString("mItemId"), "0", null, getIntent()
							.getExtras().getString("latitude"), getIntent()
							.getExtras().getString("longitude"), getIntent()
							.getExtras().getString("selectedCat"), getIntent()
							.getExtras().getString("srchKey"), getIntent()
							.getExtras().getString("selectedCatId"), subCatId);
		} else {
			mTableRowCitySort.setVisibility(View.GONE);
			mTableRow2GroupAtoZ.setVisibility(View.GONE);
		}

	}

	public void onResume() {
		super.onResume();
		mLinkId = Constants.FIND_CAT_LINK_ID;
		new TypeAsyncTask().execute();
	}

	private void createAlert(boolean flag) {

		CharSequence[] items = null;

		AlertDialog.Builder builder = new AlertDialog.Builder(
				FindLocationCategorySortActivity.this);

		catIdList = mList;

		ArrayList<String> data = mSpinnerData;

		if (data != null) {
			items = new CharSequence[data.size()];

			for (int i = 0; i < data.size(); i++) {

				items[i] = data.get(i);

			}
		}

		builder.setTitle(typeText);
		if (flag) {
			builder.setPositiveButton("OK", null);
		}

		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int position) {
				HashMap<String, String> temp = catIdList.get(position);
				groupChoice = "type";
				subCatId = temp.get("typeId");
				typeName = temp.get("typeName");

				atozGroupCheck.setVisibility(View.GONE);
				typeGroupCheck.setVisibility(View.VISIBLE);

			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		Intent launchintent = new Intent();
		launchintent.setClass(FindLocationCategorySortActivity.this,
				FindLocationActivity.class);
		launchintent.putExtra("selectedCat",
				getIntent().getExtras().getString("selectedCat"));
		launchintent.putExtra("selectedCatId", getIntent().getExtras()
				.getString("selectedCatId"));
		launchintent.putExtra("mLinkId", mLinkId);
		launchintent.putExtra("mItemId",
				getIntent().getExtras().getString("mItemId"));
		launchintent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
				getIntent().getExtras().getString("bottomBtnId"));
		launchintent.putExtra("typeName", typeNameRef);
		launchintent.putExtra("sortChoice", sortChoiceRef);
		launchintent.putExtra("groupChoice", groupChoiceRef);

		if (subCatIdRef != null) {
			launchintent.putExtra("subCatId", subCatIdRef);
		}
		launchintent.putExtra("srchKey",
				getIntent().getExtras().getString("srchKey"));
		launchintent.putExtra("latitude",
				getIntent().getExtras().getString("latitude"));
		launchintent.putExtra("longitude",
				getIntent().getExtras().getString("longitude"));
		launchintent.putExtra("isAtoZ", isAtoZRef);

		startActivity(launchintent);
		finish();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	public class TypeAsyncTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			getGPSValues();
			mDialog = ProgressDialog.show(
					FindLocationCategorySortActivity.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		protected void onPostExecute(String result) {

			if ("successfull".equals(returnValue)) {
				mTableRow2GroupType.setVisibility(View.VISIBLE);
				typeText = "Type";
				flag = false;
			}

			else {
				mTableRow2GroupType.setVisibility(View.GONE);

				if (getIntent().getExtras().containsKey("Class")) {
					if ("Find".equals(getIntent().getExtras()
							.getString("Class"))) {

						if (Properties.isRegionApp) {
							groupByText.setVisibility(View.VISIBLE);
						} else {
							groupByText.setVisibility(View.GONE);
						}
					}
				}

				flag = true;

			}

			try {
				mDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... arg0) {
			try {
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();

				String url = Properties.url_local_server
						+ Properties.hubciti_version + "find/getsubcategory";

				HubCityLogger.d("FindLocationCategorySortActivity", url);

				String urlParams = "";

//				urlParams = mUrlRequestParams.getFindSubCategoryParam(
//						getIntent().getExtras().getString("selectedCatId"),
//						getIntent().getExtras().getString("mItemId"),
//						getIntent().getExtras().getString(
//								Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));

				JSONObject jsonObject = mServerConnections.getUrlPostResponse(
						url, urlParams, true);

				HubCityLogger.d("FindLocationCategorySortActivity",
						jsonObject.toString());

				if (jsonObject != null) {

					if (jsonObject.has("CategoryDetails")) {

						mList = new ArrayList<>();

						JSONObject elem = jsonObject
								.getJSONObject("CategoryDetails");
						JSONObject elem2 = elem.getJSONObject("ListCatDetails");

						boolean isArray = isJSONArray(elem2, "CategoryDetails");

						mSpinnerData = new ArrayList<>();

						if (isArray) {

							JSONArray elem1 = elem2
									.getJSONArray("CategoryDetails");

							for (int index = 0; index < elem1.length(); index++) {

								mTypeDetails = new HashMap<>();
								JSONObject obj = elem1.getJSONObject(index);

								mTypeDetails.put("typeId",
										obj.getString("catId"));
								mTypeDetails.put("typeName",
										obj.getString("catName"));

								mSpinnerData.add(obj.getString("catName"));
								mList.add(mTypeDetails);
							}

						} else {

							mTypeDetails = new HashMap<>();
							JSONObject obj = elem2
									.getJSONObject("CategoryDetails");

							mTypeDetails.put("typeId", obj.getString("catId"));
							mTypeDetails.put("typeName",
									obj.getString("catName"));

							mSpinnerData.add(obj.getString("catName"));
							mList.add(mTypeDetails);

						}

						returnValue = "successfull";
					} else {

						if (jsonObject.has("response")) {

							JSONObject obj = jsonObject
									.getJSONObject("response");

							typeText = obj.getString("responseText");

							returnValue = "unSuccessFull";

						}

					}

				}

			}

			catch (Exception ex) {
				ex.printStackTrace();
				typeText = "Error in Request Response";
				returnValue = "unSuccessFull";
			}

			return returnValue;
		}

	}

	public void getGPSValues() {
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (locationManager.getAllProviders().contains("gps") && gpsEnabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Criteria criteria = new Criteria();
			String bestProvider = locationManager.getBestProvider(criteria,
					false);
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			locNew = locationManager.getLastKnownLocation(bestProvider);

			if (locNew != null) {
				CommonConstants.LATITUDE = String.valueOf(locNew.getLatitude());
				CommonConstants.LONGITUDE = String.valueOf(locNew
						.getLongitude());

			} else {
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					CommonConstants.LATITUDE = String.valueOf(locNew
							.getLatitude());
					CommonConstants.LONGITUDE = String.valueOf(locNew
							.getLongitude());
				}
			}
		}
	}

	public boolean isJSONArray(JSONObject jsonObject, String value) {
		boolean isArray = false;

		JSONObject isJSONObject = jsonObject.optJSONObject(value);
		if (isJSONObject == null) {
			JSONArray isJSONArray = jsonObject.optJSONArray(value);
			if (isJSONArray != null) {
				isArray = true;
			}
		}
		return isArray;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.find_single_category_tableRow3:
			groupByAtoz();
			break;
		case R.id.find_single_category_tableRow7:
			nameTableView();
			break;
		case R.id.find_single_category_tableRow8:
			distanceTableView();
			break;
		case R.id.find_single_category_tableRow4:
			isAtoZ = false;
			createAlert(flag);
			break;
		case R.id.event_textView_group_atoz:
			groupByAtoz();
			break;
		case R.id.event_textView_group_type:
			checkBox = (CheckBox) v.findViewById(R.id.event_group_checkBox3);
			isAtoZ = false;
			createAlert(flag);
			break;
		case R.id.event_textView_sort_name:
			textViewSortName();
			break;
		case R.id.event_textView_sort_distance:
			textViewSortDistance();
			break;
		case R.id.sortby_city_tableRow:
			sortbyCityTableRow();
			break;
		case R.id.textView_sort_city:
			sortbyCityTableRow();
			break;

		default:
			break;
		}

	}

	private void distanceTableView() {
		nameSortCheck.setVisibility(View.INVISIBLE);
		distanceSortCheck.setVisibility(View.VISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);
		sortChoice = "distance";

	}

	private void nameTableView() {
		nameSortCheck.setVisibility(View.VISIBLE);
		distanceSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);
		sortChoice = "name";

	}

	private void sortbyCityTableRow() {
		nameSortCheck.setVisibility(View.INVISIBLE);
		distanceSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.VISIBLE);
		ll.setVisibility(View.GONE);
		citySorting.sortAction();
		sortChoice = "city";
	}

	private void textViewSortDistance() {
		nameSortCheck.setVisibility(View.INVISIBLE);
		distanceSortCheck.setVisibility(View.VISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);
		sortChoice = "distance";
	}

	private void textViewSortName() {
		nameSortCheck.setVisibility(View.VISIBLE);
		distanceSortCheck.setVisibility(View.INVISIBLE);
		citySortCheck.setVisibility(View.INVISIBLE);
		ll.setVisibility(View.GONE);
		sortChoice = "name";

	}

	private void groupByAtoz() {
		subCatId = null;

		isAtoZ = !isAtoZ;

		if (isAtoZ) {
			atozGroupCheck.setVisibility(View.VISIBLE);
			groupChoice = "atoz";
		} else {
			atozGroupCheck.setVisibility(View.INVISIBLE);
			groupChoice = "";
		}

		typeGroupCheck.setVisibility(View.INVISIBLE);

	}

}
