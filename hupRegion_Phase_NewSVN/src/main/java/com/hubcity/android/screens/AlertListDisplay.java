package com.hubcity.android.screens;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.EntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AlertListDisplay extends CustomTitleBar implements
        OnItemClickListener, OnClickListener {

    private ArrayList<Item> items = new ArrayList<>();
    private ListView listview;
    private String mItemId;
    private View paginator;
    private ImageButton nextPageBtn;
    private ImageButton prevPageBtn;
    private int lastvisitProdId = 0;
    private int minProdId = 0;
    private ArrayList<PrepareAlertList> alertList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                && getIntent().getExtras().getString(
                Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
            mItemId = getIntent().getExtras().getString(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA);

        } else if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                && getIntent().getExtras().getString(
                Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
            mItemId = getIntent().getExtras().getString(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
        }

        new AlertListAsyncTask().execute();

        title.setText("Alerts");
        leftTitleImage.setVisibility(View.INVISIBLE);
        paginator = getLayoutInflater().inflate(R.layout.hot_deals_pagination,
                listview, true);
        prevPageBtn = (ImageButton) paginator
                .findViewById(R.id.hotdeals_prev_page);
        nextPageBtn = (ImageButton) paginator
                .findViewById(R.id.hotdeals_next_page);
        prevPageBtn.setOnClickListener(this);
        nextPageBtn.setOnClickListener(this);
        listview = (ListView) findViewById(R.id.listView_main);

    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long arg3) {
        EntryItem item = (EntryItem) items.get(position);
        Intent alertDescripNav = new Intent(getApplicationContext(),
                AlertDescription.class);
        alertDescripNav.putExtra("header", item.alertName);
        alertDescripNav.putExtra("Description", item.subtitle);
        alertDescripNav.putExtra("ImageURL", item.imageURL);
        alertDescripNav.putExtra("Startdate", item.startDate);
        alertDescripNav.putExtra("StartTime", item.startTime);
        alertDescripNav.putExtra("EndDate", item.endDate);
        alertDescripNav.putExtra("EndTime", item.endTime);

        startActivityForResult(alertDescripNav, Constants.STARTVALUE);

    }

    private static String returnValue = "sucessfull";
    private ListDetails objListDetails;

    private String responseText;

    class PrepareAlertList {
        String header;
        ArrayList<DetailsByGroup> alertLists = new ArrayList<>();

    }

    class DetailsByGroup {
        String startTime;
        String startDate;
        String alertId;
        String hubCitiId;
        String alertListId;
        String endDate;
        String shortDes;
        String severityId;
        String endTime;
        String alertName;
        Bitmap serverImage;

    }

    public class ListDetails {
        ArrayList<PrepareAlertList> alertList = new ArrayList<>();

    }

    private ProgressDialog progDialog;

    UrlRequestParams mUrlRequestParams = new UrlRequestParams();
    ServerConnections mServerConnections = new ServerConnections();


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public class AlertListAsyncTask extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        boolean nextPage = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDialog = new ProgressDialog(AlertListDisplay.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                progDialog.dismiss();
                if ("sucessfull".equals(returnValue)) {
                    if (minProdId > 1 || nextPage) {

                        if (minProdId > 1) {
                            prevPageBtn.setVisibility(View.VISIBLE);
                        } else {
                            prevPageBtn.setVisibility(View.GONE);
                        }
                        if (nextPage) {
                            nextPageBtn.setVisibility(View.VISIBLE);
                        } else {
                            nextPageBtn.setVisibility(View.GONE);
                        }

                        listview.addFooterView(paginator);

                    } else {
                        listview.removeFooterView(paginator);
                    }

                    alertList = new ArrayList<>();
                    alertList = objListDetails.alertList;
                    items = new ArrayList<>();

                    for (int i = 0; i < alertList.size(); i++) {

                        items.add(new SectionItem(alertList.get(i).header));
                        for (int j = 0; j < alertList.get(i).alertLists.size(); j++) {
                            items.add(new EntryItem(alertList.get(i).alertLists
                                    .get(j).alertName, alertList.get(i).alertLists
                                    .get(j).shortDes, alertList.get(i).alertLists
                                    .get(j).severityId, alertList.get(i).alertLists
                                    .get(j).startDate, alertList.get(i).alertLists
                                    .get(j).startTime, alertList.get(i).alertLists
                                    .get(j).endDate, alertList.get(i).alertLists
                                    .get(j).endTime));
                        }
                    }
                    EntryAdapter adapter = new EntryAdapter(
                            getApplicationContext(), items);
                    listview.setAdapter(adapter);
                    listview.setOnItemClickListener(AlertListDisplay.this);

                } else {
                    Toast.makeText(getApplicationContext(), responseText,
                            Toast.LENGTH_LONG).show();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                minProdId = lastvisitProdId;
                String url_alert = Properties.url_local_server
                        + Properties.hubciti_version + "alertevent/alertlist";
                String urlParameters = mUrlRequestParams
                        .createAlertListParameter(mItemId, "android",
                                lastvisitProdId + "");
                JSONObject jsonRespone = mServerConnections.getUrlPostResponse(
                        url_alert, urlParameters, true);

                try {
                    if (jsonRespone.has("AlertDetails")) {
                        JSONObject json2 = jsonRespone
                                .getJSONObject("AlertDetails");
                        JSONObject json3 = json2.getJSONObject("categoryList");
                        try {
                            JSONArray json4 = json3
                                    .getJSONArray("CategoryInfo");

                            if (json4 != null) {

                                objListDetails = new ListDetails();
                                for (int i = 0; i < json4.length(); i++) {
                                    JSONObject elem = json4.getJSONObject(i);
                                    String categoryName = elem
                                            .getString("categoryName");

                                    PrepareAlertList objPrepareAlertList = new PrepareAlertList();
                                    objPrepareAlertList.header = categoryName;

                                    JSONObject elems = elem
                                            .getJSONObject("alertList");
                                    try {
                                        JSONObject prods = elems
                                                .getJSONObject("AlertDetails");
                                        if (prods != null) {
                                            DetailsByGroup objDetailsByGroup = new DetailsByGroup();
                                            objDetailsByGroup.startTime = prods
                                                    .getString("startTime");
                                            objDetailsByGroup.startDate = prods
                                                    .getString("startDate");
                                            objDetailsByGroup.alertId = prods
                                                    .getString("alertId");
                                            objDetailsByGroup.hubCitiId = prods
                                                    .getString("hubCitiId");
                                            objDetailsByGroup.alertListId = prods
                                                    .getString("alertListId");
                                            objDetailsByGroup.endDate = prods
                                                    .getString("endDate");
                                            objDetailsByGroup.shortDes = prods
                                                    .getString("shortDes");
                                            if ("3".equals(prods
                                                    .getString("severityId"))) {
                                                objDetailsByGroup.severityId = json2
                                                        .getString("red");
                                            } else if ("2"
                                                    .equals(prods
                                                            .getString("severityId"))) {
                                                objDetailsByGroup.severityId = json2
                                                        .getString("yellow");
                                            } else if ("1"
                                                    .equals(prods
                                                            .getString("severityId"))) {
                                                objDetailsByGroup.severityId = json2
                                                        .getString("green");
                                            }

                                            objDetailsByGroup.endTime = prods
                                                    .getString("endTime");
                                            objDetailsByGroup.alertName = prods
                                                    .getString("alertName");
                                            objPrepareAlertList.alertLists
                                                    .add(objDetailsByGroup);
                                            objListDetails.alertList
                                                    .add(objPrepareAlertList);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        JSONArray prods = elems
                                                .getJSONArray("AlertDetails");
                                        if (prods != null) {
                                            for (int j = 0; j < prods
                                                    .length(); j++) {
                                                JSONObject innerElem = prods
                                                        .getJSONObject(j);
                                                if (innerElem != null) {
                                                    DetailsByGroup objDetailsByGroup = new DetailsByGroup();
                                                    objDetailsByGroup.startTime = innerElem
                                                            .getString("startTime");
                                                    objDetailsByGroup.startDate = innerElem
                                                            .getString("startDate");
                                                    objDetailsByGroup.alertId = innerElem
                                                            .getString("alertId");
                                                    objDetailsByGroup.hubCitiId = innerElem
                                                            .getString("hubCitiId");
                                                    objDetailsByGroup.alertListId = innerElem
                                                            .getString("alertListId");
                                                    objDetailsByGroup.endDate = innerElem
                                                            .getString("endDate");
                                                    objDetailsByGroup.shortDes = innerElem
                                                            .getString("shortDes");
                                                    if ("3".equals(innerElem
                                                            .getString("severityId"))) {
                                                        objDetailsByGroup.severityId = json2
                                                                .getString("red");
                                                    } else if ("2"
                                                            .equals(innerElem
                                                                    .getString("severityId"))) {
                                                        objDetailsByGroup.severityId = json2
                                                                .getString("yellow");
                                                    } else if ("1"
                                                            .equals(innerElem
                                                                    .getString("severityId"))) {
                                                        objDetailsByGroup.severityId = json2
                                                                .getString("green");
                                                    }

                                                    objDetailsByGroup.endTime = innerElem
                                                            .getString("endTime");
                                                    objDetailsByGroup.alertName = innerElem
                                                            .getString("alertName");
                                                    objPrepareAlertList.alertLists
                                                            .add(objDetailsByGroup);
                                                }
                                            }
                                            objListDetails.alertList
                                                    .add(objPrepareAlertList);
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            JSONObject elem = json3
                                    .getJSONObject("CategoryInfo");
                            objListDetails = new ListDetails();
                            String categoryName = elem
                                    .getString("categoryName");

                            PrepareAlertList objPrepareAlertList = new PrepareAlertList();
                            objPrepareAlertList.header = categoryName;

                            JSONObject elems = elem
                                    .getJSONObject("alertList");
                            try {
                                JSONObject prods = elems
                                        .getJSONObject("AlertDetails");
                                if (prods != null) {
                                    DetailsByGroup objDetailsByGroup = new DetailsByGroup();
                                    objDetailsByGroup.startTime = prods
                                            .getString("startTime");
                                    objDetailsByGroup.startDate = prods
                                            .getString("startDate");
                                    objDetailsByGroup.alertId = prods
                                            .getString("alertId");
                                    objDetailsByGroup.hubCitiId = prods
                                            .getString("hubCitiId");
                                    objDetailsByGroup.alertListId = prods
                                            .getString("alertListId");
                                    objDetailsByGroup.endDate = prods
                                            .getString("endDate");
                                    objDetailsByGroup.shortDes = prods
                                            .getString("shortDes");
                                    if ("3".equals(prods
                                            .getString("severityId"))) {
                                        objDetailsByGroup.severityId = json2
                                                .getString("red");
                                    } else if ("2".equals(prods
                                            .getString("severityId"))) {
                                        objDetailsByGroup.severityId = json2
                                                .getString("yellow");
                                    } else if ("1".equals(prods
                                            .getString("severityId"))) {
                                        objDetailsByGroup.severityId = json2
                                                .getString("green");
                                    }

                                    objDetailsByGroup.endTime = prods
                                            .getString("endTime");
                                    objDetailsByGroup.alertName = prods
                                            .getString("alertName");
                                    objPrepareAlertList.alertLists
                                            .add(objDetailsByGroup);
                                    objListDetails.alertList
                                            .add(objPrepareAlertList);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                JSONArray prods = elems
                                        .getJSONArray("AlertDetails");
                                if (prods != null) {
                                    for (int j = 0; j < prods.length(); j++) {
                                        JSONObject innerElem = prods
                                                .getJSONObject(j);
                                        if (innerElem != null) {
                                            DetailsByGroup objDetailsByGroup = new DetailsByGroup();
                                            objDetailsByGroup.startTime = innerElem
                                                    .getString("startTime");
                                            objDetailsByGroup.startDate = innerElem
                                                    .getString("startDate");
                                            objDetailsByGroup.alertId = innerElem
                                                    .getString("alertId");
                                            objDetailsByGroup.hubCitiId = innerElem
                                                    .getString("hubCitiId");
                                            objDetailsByGroup.alertListId = innerElem
                                                    .getString("alertListId");
                                            objDetailsByGroup.endDate = innerElem
                                                    .getString("endDate");
                                            objDetailsByGroup.shortDes = innerElem
                                                    .getString("shortDes");
                                            if ("3".equals(innerElem
                                                    .getString("severityId"))) {
                                                objDetailsByGroup.severityId = json2
                                                        .getString("red");
                                            } else if ("2"
                                                    .equals(innerElem
                                                            .getString("severityId"))) {
                                                objDetailsByGroup.severityId = json2
                                                        .getString("yellow");
                                            } else if ("1"
                                                    .equals(innerElem
                                                            .getString("severityId"))) {
                                                objDetailsByGroup.severityId = json2
                                                        .getString("green");
                                            }

                                            objDetailsByGroup.endTime = innerElem
                                                    .getString("endTime");
                                            objDetailsByGroup.alertName = innerElem
                                                    .getString("alertName");
                                            objPrepareAlertList.alertLists
                                                    .add(objDetailsByGroup);
                                        }
                                    }
                                    objListDetails.alertList
                                            .add(objPrepareAlertList);
                                }
                            }

                        }
                        try {
                            if (Integer.valueOf(json2.getString("maxRowNum")) > lastvisitProdId) {
                                lastvisitProdId = Integer.valueOf(json2
                                        .getString("maxRowNum"));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {

                            if ("1".equalsIgnoreCase(json2
                                    .getString("nextPage"))) {
                                nextPage = true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        JSONObject jsonRecordNotFound = jsonRespone
                                .getJSONObject("response");
                        responseText = jsonRecordNotFound
                                .getString("responseText");
                        returnValue = "UNSUCCESS";
                        return returnValue;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    returnValue = "UNSUCCESS";
                    return returnValue;
                }
                return returnValue;

            } catch (Exception e) {
                e.printStackTrace();
                returnValue = "UNSUCCESS";
                return returnValue;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.hotdeals_prev_page:
                hotdealsPrevPage();
                break;
            case R.id.hotdeals_next_page:
                new AlertListAsyncTask().execute();
                break;
            default:
                break;
        }
    }

    private void hotdealsPrevPage() {
        lastvisitProdId = 0 > (minProdId - Constants.RANGE) ? 0 : minProdId
                - Constants.RANGE;
        new AlertListAsyncTask().execute();

    }

}
