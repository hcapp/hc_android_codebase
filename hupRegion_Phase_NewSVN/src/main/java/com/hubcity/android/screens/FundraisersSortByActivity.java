package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.hubcity.android.businessObjects.Deptt;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

public class FundraisersSortByActivity extends CustomTitleBar implements
        OnClickListener {
    public static final String DEPTT = "Department";
    Set<Deptt> depttSet;

    ListView groupListView;
    ListView sortListView;
    Button done;
    Button cancel;
    ArrayList<String> groupList;
    ArrayList<String> sortList;
    ArrayList<String> groupNames;
    ArrayList<String> sortNames;
    String groupChoice;
    String sortChoice;

    TextView dateGroup;
    TextView atozGroup;
    TextView typeGroup;
    TextView departmentGroup;

    CheckBox dateGroupCheck;
    CheckBox atozGroupCheck;
    CheckBox typeGroupCheck;
    TextView departmentGroupCheck;

    TextView dateSort;
    TextView nameSort;
    TextView citySort;

    CheckBox dateSortCheck;
    CheckBox nameSortCheck;
    CheckBox citySortCheck;

    String group;
    String sort;

    TableRow mTableRowDate;
    TableRow mTableRowAtoZ;
    TableRow mTableRowType;
    TableRow mTableRowdepartment;
    TableRow mTableRowDateSort;
    TableRow mTableRowNameSort;
    TableRow mTableRowCitySort;
    TableRow mTableRowCityList;
    boolean bIsDepartmentPresent = false;
    LinearLayout ll;

    CitySort citySorting;
    Activity activity;

    String mItemId = "";
    String mBottomId = "";

    int isDeptFlag;

    DeptListAsyncTask deptListAsyncTask;

    void setContentViewWithCustomTitleBar() {

        setContentView(R.layout.fundraiser_sort_activity);

        activity = this;

        ll = (LinearLayout) findViewById(R.id.city_ll);
        CitySort.isDefaultCitySort = false;

        HubCityContext hubciti = (HubCityContext) getApplicationContext();
        groupChoice = hubciti.getFundGroupSelection();
        sortChoice = hubciti.getFundSortSelection();

        group = groupChoice;
        sort = sortChoice;

        backImage.setVisibility(View.GONE);
        this.title.setText("Group&Sort");
        this.leftTitleImage.setBackgroundResource(R.drawable.ic_action_cancel);
        this.leftTitleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HubCityContext mHubCity = (HubCityContext) getApplicationContext();
                mHubCity.setFundGroupSelection(group);
                mHubCity.setFundSortSelection(sort);

                Intent updateFundraiserList = new Intent(
                        FundraisersSortByActivity.this,
                        FundraiserActivity.class);
                updateFundraiserList.putExtra(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        getIntent().getExtras().getString(
                                Constants.MENU_ITEM_ID_INTENT_EXTRA));
                updateFundraiserList.putExtra(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        getIntent().getExtras().getString(
                                Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
                updateFundraiserList.putExtra("mLinkId", getIntent()
                        .getExtras().getString("mLinkId"));
                updateFundraiserList.putExtra("catName", getIntent()
                        .getExtras().getString("catName"));
                updateFundraiserList.putExtra("catId", getIntent().getExtras()
                        .getString("catId"));

                updateFundraiserList.putExtra("retailerId", getIntent()
                        .getExtras().getString("retailerId"));
                updateFundraiserList.putExtra("retListId", getIntent()
                        .getExtras().getString("retListId"));
                updateFundraiserList.putExtra("retailLocationId", getIntent()
                        .getExtras().getString("retailLocationId"));
                updateFundraiserList.putExtra("retailerEvents", getIntent()
                        .getExtras().getString("retailerEvents"));
                updateFundraiserList.putExtra("deptId", getIntent().getExtras()
                        .getString("deptId"));

                if (getIntent().hasExtra("isDeals")
                        && getIntent().getExtras().getBoolean("isDeals")) {
                    updateFundraiserList.putExtra("isDeals", getIntent()
                            .getExtras().getBoolean("isDeals"));
                }

                startActivity(updateFundraiserList);
                finish();
            }
        });
        this.rightImage.setImageBitmap(null);
        this.rightImage.setBackgroundResource(R.drawable.ic_action_accept);
        this.rightImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String deptID = "";
                if (mHashMapForSorting != null && !mHashMapForSorting.isEmpty()) {
                    deptID = mHashMapForSorting.get(DEPTT).toString();
                }

                if ("department".equals(sortChoice) && "".equals(deptID)) {
                    if (getIntent().hasExtra("deptId")) {
                        deptID = getIntent().getExtras().getString("deptId");
                    }
                }

                HubCityContext mHubCity = (HubCityContext) getApplicationContext();
                mHubCity.setFundGroupSelection(groupChoice);
                mHubCity.setFundSortSelection(sortChoice);

                Intent updateFundList = new Intent(
                        FundraisersSortByActivity.this,
                        FundraiserActivity.class);
                updateFundList.putExtra(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        getIntent().getExtras().getString(
                                Constants.MENU_ITEM_ID_INTENT_EXTRA));
                updateFundList.putExtra(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        getIntent().getExtras().getString(
                                Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
                updateFundList.putExtra("mLinkId", getIntent().getExtras()
                        .getString("mLinkId"));
                updateFundList.putExtra("catName", getIntent().getExtras()
                        .getString("catName"));
                updateFundList.putExtra("catId", getIntent().getExtras()
                        .getString("catId"));
                updateFundList.putExtra("retailerId", getIntent().getExtras()
                        .getString("retailerId"));
                updateFundList.putExtra("retListId", getIntent().getExtras()
                        .getString("retListId"));
                updateFundList.putExtra("retailLocationId", getIntent()
                        .getExtras().getString("retailLocationId"));
                updateFundList.putExtra("deptId", deptID);

                updateFundList.putExtra("retailerEvents", getIntent()
                        .getExtras().getString("retailerEvents"));

                if (getIntent().hasExtra("isDeals")
                        && getIntent().getExtras().getBoolean("isDeals")) {
                    updateFundList.putExtra("isDeals", getIntent().getExtras()
                            .getBoolean("isDeals"));
                }

                startActivity(updateFundList);

                finish();

            }
        });

        mTableRowDate = (TableRow) findViewById(R.id.events_groupby_date_tableRow);
        mTableRowAtoZ = (TableRow) findViewById(R.id.events_groupby_atoz_tableRow);
        mTableRowType = (TableRow) findViewById(R.id.events_groupby_type_tableRow);

        mTableRowDate.setOnClickListener(this);
        mTableRowAtoZ.setOnClickListener(this);
        mTableRowType.setOnClickListener(this);

        mTableRowDateSort = (TableRow) findViewById(R.id.events_sortby_date_tableRow);
        mTableRowNameSort = (TableRow) findViewById(R.id.events_sortby_name_tableRow);
        mTableRowCitySort = (TableRow) findViewById(R.id.sortby_city_tableRow);
        mTableRowCityList = (TableRow) findViewById(R.id.city_list_tableRow);
        mTableRowdepartment = (TableRow) findViewById(R.id.events_sortby_department_tableRow);

        if (1 == isDeptFlag) {
            mTableRowdepartment.setVisibility(View.VISIBLE);

        } else if (0 == isDeptFlag) {
            mTableRowdepartment.setVisibility(View.GONE);

        }

        mTableRowdepartment.setOnClickListener(this);
        mTableRowDateSort.setOnClickListener(this);
        mTableRowNameSort.setOnClickListener(this);
        mTableRowCitySort.setOnClickListener(this);

        dateGroup = (TextView) findViewById(R.id.event_textView_group_date);
        atozGroup = (TextView) findViewById(R.id.event_textView_group_atoz);
        typeGroup = (TextView) findViewById(R.id.event_textView_group_type);

        dateGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox1);
        atozGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox2);
        typeGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox3);

        if ("date".equals(group)) {
            dateGroupCheck.setVisibility(View.VISIBLE);
        } else if ("atoz".equals(group)) {
            atozGroupCheck.setVisibility(View.VISIBLE);
        } else if ("type".equals(group)) {
            typeGroupCheck.setVisibility(View.VISIBLE);
        }

        dateSort = (TextView) findViewById(R.id.event_textView_sort_date);
        nameSort = (TextView) findViewById(R.id.event_textView_sort_name);
        citySort = (TextView) findViewById(R.id.textView_sort_city);
        departmentGroup = (TextView) findViewById(R.id.event_textView_sort_department);

        citySort.setOnClickListener(this);

        dateSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox1);
        nameSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox2);
        citySortCheck = (CheckBox) findViewById(R.id.sortby_city_checkBox);
        departmentGroupCheck = (CheckBox) findViewById(R.id.event_sort_checkBox4);

        if ("date".equals(sort)) {
            dateSortCheck.setVisibility(View.VISIBLE);
        } else if ("name".equals(sort)) {
            nameSortCheck.setVisibility(View.VISIBLE);
        } else if ("city".equals(sort)) {
            citySortCheck.setVisibility(View.VISIBLE);
            CitySort.isDefaultCitySort = true;
        } else if ("department".equals(sort)) {
            departmentGroupCheck.setVisibility(View.VISIBLE);
        }

        if (Properties.isRegionApp) {
            mTableRowCitySort.setVisibility(View.VISIBLE);

            boolean isDeals = false;
            if (getIntent().hasExtra("isDeals")) {
                isDeals = getIntent().getExtras().getBoolean("isDeals");
            }

            // Change this for Fundraiser
            citySorting = new CitySort(activity, ll, citySortCheck,
                    mTableRowCitySort, "Fund", mItemId, mBottomId, "",
                    getIntent().getExtras().getString("retailerId"),
                    getIntent().getExtras().getString("retailLocationId"),
                    getIntent().getExtras().getString("retailerEvents"),
                    isDeals);
            mTableRowdepartment.setVisibility(View.VISIBLE);
            deptListAsyncTask = new DeptListAsyncTask();
            deptListAsyncTask.execute();
        } else {
            mTableRowCitySort.setVisibility(View.GONE);

        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().hasExtra("isDeptFlag")) {
            isDeptFlag = getIntent().getExtras().getInt("isDeptFlag");
        }
        mItemId = getIntent().getExtras().getString(
                Constants.MENU_ITEM_ID_INTENT_EXTRA);

        mBottomId = getIntent().getExtras().getString(
                Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);

        setContentViewWithCustomTitleBar();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        HubCityContext mHubCity = (HubCityContext) getApplicationContext();
        mHubCity.setFundGroupSelection(group);
        mHubCity.setFundSortSelection(sort);

        Intent updateEventList = new Intent(FundraisersSortByActivity.this,
                FundraiserActivity.class);
        updateEventList.putExtra(
                Constants.MENU_ITEM_ID_INTENT_EXTRA,
                getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA));
        updateEventList.putExtra(
                Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                getIntent().getExtras().getString(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
        updateEventList.putExtra("mLinkId",
                getIntent().getExtras().getString("mLinkId"));
        updateEventList.putExtra("catName",
                getIntent().getExtras().getString("catName"));
        updateEventList.putExtra("catId",
                getIntent().getExtras().getString("catId"));
        updateEventList.putExtra("retailerId", getIntent().getExtras()
                .getString("retailerId"));
        updateEventList.putExtra("retListId", getIntent().getExtras()
                .getString("retListId"));
        updateEventList.putExtra("retailLocationId", getIntent().getExtras()
                .getString("retailLocationId"));
        updateEventList.putExtra("retailerEvents", getIntent().getExtras()
                .getString("retailerEvents"));
        updateEventList.putExtra("deptId",
                getIntent().getExtras().getString("deptId"));

        if (getIntent().hasExtra("isDeals")
                && getIntent().getExtras().getBoolean("isDeals")) {
            updateEventList.putExtra("isDeals", getIntent().getExtras()
                    .getBoolean("isDeals"));
        }

        startActivity(updateEventList);

        finish();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.events_groupby_date_tableRow:
                groupByDate();
                break;

            case R.id.events_groupby_atoz_tableRow:
                groupByAtoZ();
                break;

            case R.id.events_groupby_type_tableRow:
                groupByType();
                break;

            case R.id.events_sortby_department_tableRow:
                sortByDept();
                break;

            case R.id.events_sortby_date_tableRow:
                sortByDate();
                break;

            case R.id.events_sortby_name_tableRow:
                sortByName();
                break;

            case R.id.sortby_city_tableRow:
                eventsSortbyCityTableRow();
                break;

            case R.id.textView_sort_city:
                eventsSortbyCityTableRow();
                break;

            default:
                break;
        }

    }

    private void eventsSortbyCityTableRow() {
        nameSortCheck.setVisibility(View.INVISIBLE);
        dateSortCheck.setVisibility(View.INVISIBLE);
        citySortCheck.setVisibility(View.VISIBLE);
        departmentGroupCheck.setVisibility(View.INVISIBLE);
        ll.setVisibility(View.GONE);
        citySorting.sortAction();

        sortChoice = "city";
    }

    private void sortByName() {
        nameSortCheck.setVisibility(View.VISIBLE);
        dateSortCheck.setVisibility(View.INVISIBLE);
        citySortCheck.setVisibility(View.INVISIBLE);
        departmentGroupCheck.setVisibility(View.INVISIBLE);
        ll.setVisibility(View.GONE);

        sortChoice = "name";

    }

    private void sortByDate() {
        dateSortCheck.setVisibility(View.VISIBLE);
        nameSortCheck.setVisibility(View.INVISIBLE);
        citySortCheck.setVisibility(View.INVISIBLE);
        departmentGroupCheck.setVisibility(View.INVISIBLE);
        ll.setVisibility(View.GONE);

        sortChoice = "date";

    }

    private void sortByDept() {
        departmentGroupCheck.setVisibility(View.VISIBLE);
        dateSortCheck.setVisibility(View.INVISIBLE);
        nameSortCheck.setVisibility(View.INVISIBLE);
        citySortCheck.setVisibility(View.INVISIBLE);
        ll.setVisibility(View.GONE);

        sortChoice = "department";
        if (bIsDepartmentPresent) {

            if (depttSet != null) {
                createListAlertDialog(depttSet, DEPTT);
            }

        }

    }

    private void groupByType() {
        typeGroupCheck.setVisibility(View.VISIBLE);
        dateGroupCheck.setVisibility(View.INVISIBLE);
        atozGroupCheck.setVisibility(View.INVISIBLE);

        groupChoice = "type";

    }

    private void groupByAtoZ() {
        atozGroupCheck.setVisibility(View.VISIBLE);
        typeGroupCheck.setVisibility(View.INVISIBLE);
        dateGroupCheck.setVisibility(View.INVISIBLE);

        groupChoice = "atoz";

    }

    private void groupByDate() {
        dateGroupCheck.setVisibility(View.VISIBLE);
        atozGroupCheck.setVisibility(View.INVISIBLE);
        typeGroupCheck.setVisibility(View.INVISIBLE);
        groupChoice = "date";

    }

    class DeptListAsyncTask extends AsyncTask<String, String, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        private ProgressDialog progDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDialog = new ProgressDialog(FundraisersSortByActivity.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if ("Success".equalsIgnoreCase(result)) {

                if (depttSet != null) {
                    bIsDepartmentPresent = true;
                } else {
                    bIsDepartmentPresent = false;
                    mTableRowdepartment.setVisibility(View.GONE);
                }
            } else {// for UN_SUCCESSFULL
                progDialog.dismiss();
                bIsDepartmentPresent = false;
                mTableRowdepartment.setVisibility(View.GONE);
            }

            if (progDialog.isShowing()) {
                progDialog.dismiss();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            try {

                String retailerId = "";
                String retailLocationId = "";

                if (getIntent().hasExtra("retailerId")) {
                    retailerId = getIntent().getExtras()
                            .getString("retailerId");
                }

                if (getIntent().hasExtra("retailLocationId")) {
                    retailLocationId = getIntent().getExtras().getString(
                            "retailLocationId");
                }

                // Get MenuID
                String urlParameters = "";

                if (getIntent().hasExtra("isDeals")
                        && getIntent().getExtras().getBoolean("isDeals")) {
                    urlParameters = mUrlRequestParams.getDept("", "", "", "");

                } else {
                    urlParameters = mUrlRequestParams.getDept(mItemId,
                            mBottomId, retailerId, retailLocationId);

                }
                String url_fundraiser_dept = Properties.url_local_server
                        + Properties.hubciti_version
                        + "alertevent/fundrserdeptlist";
                JSONObject xmlResponde = mServerConnections.getUrlPostResponse(
                        url_fundraiser_dept, urlParameters, true);
                return jsonParseResponse(xmlResponde);

            } catch (Exception e) {
                return UN_SUCCESSFULL;
            }
        }

        private Hashtable<String, String> logInResponseData;
        private String departmentName;
        private String departmentId;

        public String jsonParseResponse(JSONObject xmlResponde) {

            JSONObject localJSONObject;
            logInResponseData = new Hashtable<>();

            depttSet = new HashSet<>();

            if (xmlResponde == null) {
                return UN_SUCCESSFULL;
            }

            try {
                if (xmlResponde.has("Menu")) {
                    localJSONObject = xmlResponde.getJSONObject("Menu");
                    JSONObject mItemListObj = localJSONObject
                            .getJSONObject("mItemList");
                    JSONArray mMenuItemList = mItemListObj
                            .optJSONArray("MenuItem");

                    if (mMenuItemList == null) {

                        JSONObject menuItemJsonObj = mItemListObj
                                .optJSONObject("MenuItem");

                        if (menuItemJsonObj.has("departmentName")) {
                            departmentName = menuItemJsonObj
                                    .getString("departmentName");
                        }
                        if (menuItemJsonObj.has("departmentId")) {
                            departmentId = menuItemJsonObj
                                    .getString("departmentId");
                        }

                        if (departmentName != null) {
                            depttSet.add(new Deptt(departmentId, departmentName));
                        }

                    } else {

                        for (int i = 0; i < mMenuItemList.length(); i++) {
                            JSONObject menuItemJsonObj = mMenuItemList
                                    .getJSONObject(i);

                            if (menuItemJsonObj.has("departmentName")) {
                                departmentName = menuItemJsonObj
                                        .getString("departmentName");
                            }
                            if (menuItemJsonObj.has("departmentId")) {
                                departmentId = menuItemJsonObj
                                        .getString("departmentId");
                            }

                            if (departmentName != null) {
                                depttSet.add(new Deptt(departmentId,
                                        departmentName));
                            }

                        }
                    }
                    return SUCCESSFULL;
                } else if (xmlResponde
                        .has(Constants.ERROR_REGISTRATION_RESPONSE)) {
                    localJSONObject = xmlResponde
                            .getJSONObject(Constants.ERROR_REGISTRATION_RESPONSE);
                    String responseCode = localJSONObject
                            .getString("responseCode");
                    String responseText = localJSONObject
                            .getString("responseText");
                    logInResponseData.put("responseCode", responseCode);
                    logInResponseData.put("responseText", responseText);
                    return UN_SUCCESSFULL;
                }
            } catch (Exception e) {
                return UN_SUCCESSFULL;
            }
            return UN_SUCCESSFULL;
        }

    }

    @SuppressWarnings("rawtypes")
    private void createListAlertDialog(final Set<Deptt> set, final String text1) {
        String text = null;
        if (text1.equalsIgnoreCase(DEPTT)) {
            text = "Select " + DEPTT;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(
                FundraisersSortByActivity.this);
        builder.setTitle(text);
        List<Deptt> sortlist = new ArrayList<>(set);
        Collections.sort(sortlist, new Comparator<Deptt>() {

            @Override
            public int compare(Deptt lhs, Deptt rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }

        });

        Iterator iterator = sortlist.iterator();
        CharSequence[] items = new CharSequence[set.size()];

        ArrayList<Deptt> arrayList = new ArrayList<>();
        int i = 0;
        while (iterator.hasNext()) {
            Deptt deptt = (Deptt) iterator.next();
            items[i] = deptt.getName();
            arrayList.add(deptt);

            i++;
        }

        final ArrayList<Deptt> arrayList1 = arrayList;

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @SuppressWarnings("unchecked")
            public void onClick(DialogInterface dialog, int item) {

                Deptt deptt = arrayList1.get(item);// here we are
                // considering Deptt and
                // type business object
                // is same ,BECOUSE all
                // members of same
                // typeAISAIS

                if (text1.equalsIgnoreCase(DEPTT)) {
                    mHashMapForSorting.put(DEPTT, deptt.getId());

                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (deptListAsyncTask != null && !deptListAsyncTask.isCancelled()) {
            deptListAsyncTask.cancel(true);
            deptListAsyncTask = null;
        }
    }

    private static String SUCCESSFULL = "Success";
    private static String UN_SUCCESSFULL = "un_sucessfull";
    @SuppressWarnings("rawtypes")
    private HashMap mHashMapForSorting = new HashMap();

    @Override
    protected void onStop() {
        if (mHashMapForSorting != null) {
            mHashMapForSorting.clear();
        }

        super.onStop();
    }
}
