package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import com.scansee.hubregion.R;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class CityPrefListAdapter extends BaseAdapter {
    private CityPreferencesScreen activity;
    ArrayList<HashMap<String, Object>> cityArrayList = new ArrayList<>();

    private static LayoutInflater inflater = null;
    int count;

    public CityPrefListAdapter(Activity activity,
                               ArrayList<HashMap<String, Object>> cityArrayList, int count) {
        this.activity = (CityPreferencesScreen) activity;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.cityArrayList = cityArrayList;
        this.count = count;
    }

    @Override
    public int getCount() {
        return cityArrayList.size();
    }

    @Override
    public Object getItem(int position) {

        return cityArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;

        if (cityArrayList != null) {
            view = inflater.inflate(R.layout.city_preferences_listitem, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.cityPreferenceText = (TextView) view
                    .findViewById(R.id.city_preference_text);

            viewHolder.cityPrefCheckBox = (CheckBox) view
                    .findViewById(R.id.city_preference_checkbox);
            viewHolder.cityPrefCheckBox.setFocusable(false);
            viewHolder.cityPrefCheckBox.setClickable(true);
            viewHolder.cityPrefCheckBox
                    .setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            activity.saveCityPrefbtn.setEnabled(true);

                            int position = (Integer) v.getTag();
                            int isSelected = (Integer) cityArrayList.get(
                                    (Integer) v.getTag()).get("isCityChecked");

                            // @Beena: if 0th listitem checkbox position is
                            // selected ,uncheck all the other items
                            if (position == 0 && isSelected == 1) {
                                for (int i = 1; i < cityArrayList.size(); i++) {
                                    cityArrayList.get(i)
                                            .put("isCityChecked", 0);
                                    count = 0;
                                }
                            }

                            // if 0th listitem checkbox position is unselected
                            // ,check all the items
                            else if (position == 0 && isSelected == 0) {
                                if (count < cityArrayList.size() && count != 0) {
                                    for (int i = 1; i < cityArrayList.size(); i++) {
                                        cityArrayList.get(i).put(
                                                "isCityChecked", 0);
                                        count = 0;
                                    }
                                } else if (count == 0) {
                                    for (int i = 1; i < cityArrayList.size(); i++) {
                                        cityArrayList.get(i).put(
                                                "isCityChecked", 1);
                                        count = cityArrayList.size() - 1;

                                    }
                                }
                            }

                            // check other listitem checkbox position selection
                            // , If selected ,uncheck and vice versa
                            else if (position != 0 && isSelected == 1) {
                                cityArrayList.get(position).put(
                                        "isCityChecked", 0);
                                if (count > 0) {
                                    count--;
                                }
                            } else if (position != 0 && isSelected == 0) {
                                cityArrayList.get(position).put(
                                        "isCityChecked", 1);
                                if (count < cityArrayList.size() - 1) {
                                    count++;
                                }

                            }

                            // Based on ListCount ,Change the 0th Checkbox
                            // Selection
                            if (count < cityArrayList.size() - 1) {
                                if (count < 1) {
                                    cityArrayList.get(0)
                                            .put("isCityChecked", 0);
                                }

                            } else {

                                cityArrayList.get(0).put("isCityChecked", 1);
                            }

                            // refresh the adapter with updated listCount
                            activity.cityListAdapter = new CityPrefListAdapter(
                                    activity, cityArrayList, count);
                            Parcelable stateMain = activity.cityPrefListView
                                    .onSaveInstanceState();
                            activity.cityPrefListView
                                    .setAdapter(activity.cityListAdapter);
                            activity.cityPrefListView
                                    .onRestoreInstanceState(stateMain);

                        }
                    });

            viewHolder.cityPrefCheckBox.setTag(position);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        // add the citi names based on position
            viewHolder.cityPreferenceText.setText((CharSequence) (cityArrayList != null ? cityArrayList.get(
                    position).get("cityName") : null));

        // check whether item is selected and change the image
        if ((Integer) cityArrayList.get(position).get("isCityChecked") == 1) {
            viewHolder.cityPrefCheckBox.setChecked(true);

        } else {
            viewHolder.cityPrefCheckBox.setChecked(false);
        }

        // check whether half of the items are selected and change the image
        if (count >= 1 && count < cityArrayList.size() - 1) {
            if (position == 0) {
                viewHolder.cityPrefCheckBox.setChecked(false);
                // or remove the checkmark
                // viewHolder.cityPrefCheckBox.setImageResource(R.drawable.checkbox_unmarked);
            }
        } else if (count == 0) {
            if (position == 0) {
                viewHolder.cityPrefCheckBox.setChecked(false);
            }
        }

        return view;
    }

    public static class ViewHolder {
        protected TextView cityPreferenceText;
        protected CheckBox cityPrefCheckBox;
    }
}
