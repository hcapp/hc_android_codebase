package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.Html;
import android.util.Log;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.newsfirst.CommonMethods;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class EmailAsyncTask extends AsyncTask<Void, Void, Void> {
    private ProgressDialog mDialog;
    static final int RESULT_OK = 10000;

    UrlRequestParams urlRequestParams = new UrlRequestParams();
    ServerConnections mServerConnections = new ServerConnections();

    ArrayList<File> imageAttachmentFiles = new ArrayList<>();

    HashMap<String, String> shareDetailsList = new HashMap<>();
    HashMap<String, String> shareDetails = new HashMap<>();

    Activity activity;

    String responseText = "";
    String responseCode = "";
    String url = "";
    String urlParameters = "";


    public EmailAsyncTask(Activity activity,
                          HashMap<String, String> shareDetails) {
        this.shareDetails = shareDetails;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = ProgressDialog.show(activity, "", Constants.DIALOG_MESSAGE,
                true);
        mDialog.setCancelable(false);
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (shareDetails == null) {

            urlParameters = urlRequestParams.createEmailShareParam();
            url = Properties.url_local_server + Properties.hubciti_version
                    + "ratereview/shareapplink";
        } else {
            if ("Product Details".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams
                        .createProductEmailShare(shareDetails.get("moduleId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/shareProductInfo";

            } else if ("Appsite".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams.createAppsiteEmailShare(
                        shareDetails.get("retailerId"),
                        shareDetails.get("retailLocationId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/shareappsiteemail";

            } else if ("Specials".equals(shareDetails.get("module"))
                    || "Anything".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams.createSplOfferEmailShare(
                        shareDetails.get("retailerId"),
                        shareDetails.get("pageId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharespecialoffemail ";

            } else if ("Events".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams
                        .createEventsEmailShare(shareDetails.get("moduleId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/shareeventemail";

            } else if ("Fundraiser".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams
                        .createFundraiserEmailShare(shareDetails
                                .get("moduleId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharefundraiseremail";

            } else if ("Coupons".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams
                        .createCouponsEmailShare(shareDetails.get("moduleId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharecouponbyemail";

            } else if ("NewsDetails".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams
                        .createNewsEmailShare(shareDetails.get("moduleId"));

                  url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharenewsbyemail";

            } else if ("HotDeals".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams
                        .createHotDealsEmailShare(shareDetails.get("moduleId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharehotdealbyemail";

            } else if ("BandSummery".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams
                        .createBandSummeryParam(shareDetails.get("moduleId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharebandemail";

            } else if ("BandEventSummery".equals(shareDetails.get("module"))) {
                urlParameters = urlRequestParams
                        .createBandEventSummeryParam(shareDetails.get("moduleId"));
                url = Properties.url_local_server + Properties.hubciti_version
                        + "ratereview/sharebandeventemail";
            }

        }
        Log.d("Email Async task : ", "url : " + url);
        Log.d("Email Async task : ", "urlParameters : " + urlParameters);

        JSONObject jsonResponse = mServerConnections
                .getUrlPostResponseWithHtmlContent(url, urlParameters, true);

        try {
            if (jsonResponse != null) {

                if (jsonResponse.has("share")) {

                    JSONObject jsonShare = jsonResponse.getJSONObject("share");

                    if (jsonShare.has("responseCode")) {
                        responseCode = jsonShare.getString("responseCode");
                    }

                    if (jsonShare.has("responseText")) {
                        responseText = jsonShare.getString("responseText");
                    }

                    if (jsonShare.has("shareText")) {
                        shareDetailsList.put("shareText",
                                jsonShare.getString("shareText"));
                    }

                    if (jsonShare.has("imagePath")) {
                        shareDetailsList.put("imagePath",
                                jsonShare.getString("imagePath"));
                    }

                    if (jsonShare.has("iTunesImg")) {
                        shareDetailsList.put("iTunesImg",
                                jsonShare.getString("iTunesImg"));
                    }

                    if (jsonShare.has("androidImg")) {
                        shareDetailsList.put("androidImg",
                                jsonShare.getString("androidImg"));
                    }

                    if (jsonShare.has("subject")) {
                        shareDetailsList.put("subject",
                                jsonShare.getString("subject"));
                    }

                } else {
                    if (jsonResponse.has("responseCode")) {
                        responseCode = jsonResponse.getString("responseCode");
                    }

                    if (jsonResponse.has("responseText")) {
                        responseText = jsonResponse.getString("responseText");
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        try {
            mDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ("10000".equals(responseCode)) {
            synchronized (this) {
                try {
                    ArrayList<String> imgList = new ArrayList<>();
                    if (shareDetailsList.containsKey("imagePath")) {
                        imgList.add(shareDetailsList.get("imagePath"));
                    }

                    if (shareDetailsList.containsKey("iTunesImg")) {
                        imgList.add(shareDetailsList.get("iTunesImg"));
                    }

                    if (shareDetailsList.containsKey("androidImg")) {
                        imgList.add(shareDetailsList.get("androidImg"));
                    }

                    imageAttachmentFiles = new DownloadImageTask().execute(
                            imgList).get();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

            final Intent shareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            shareIntent.setType("text/html");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT,
                    shareDetailsList.get("subject"));

            String body = shareDetailsList.get("shareText");
            body = body.replaceAll("&#60;", "<");
            body = body.replaceAll("&#62;", ">");

            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    Html.fromHtml(body));

            ArrayList<Uri> uris = new ArrayList<>();

            for (int i = 0; i < imageAttachmentFiles.size(); i++) {
                if (imageAttachmentFiles.get(i) != null) {
                    Uri uri = Uri.fromFile(imageAttachmentFiles.get(i));
                    uris.add(uri);
                }

            }

            shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);


            activity.startActivityForResult(
                    Intent.createChooser(shareIntent, "Send email..."),
                    RESULT_OK);
        }
    }

    class DownloadImageTask extends
            AsyncTask<ArrayList<String>, Void, ArrayList<File>> {

        ArrayList<File> imageFileList = new ArrayList<>();

        protected ArrayList<File> doInBackground(ArrayList<String>... urls) {
            ArrayList<String> urldisplay = urls[0];
            Bitmap mIcon11;
            File imageFile;
            try {

                for (int i = 0; i < urldisplay.size(); i++) {

                    String imgUrl = urldisplay.get(i).replaceAll(" ", "%20");
                    InputStream in = new java.net.URL(imgUrl).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                    if (mIcon11 == null) {
                        URL url = new URL(imgUrl);
                        HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
                        ucon.setInstanceFollowRedirects(false);
                        URL secondURL = new URL(ucon.getHeaderField("Location"));
                        mIcon11 = BitmapFactory.decodeStream(secondURL.openConnection().getInputStream());
                    }
                    imageFile = CommonConstants.bitmapToFile(mIcon11, i + 1);
                    imageFileList.add(imageFile);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return imageFileList;
        }


    }

}
