package com.hubcity.android.screens;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

/**
 * This AsyncTask fetches the Answer for the selected question
 * 
 * @author rekha_p
 * 
 */
public class FetchAnswersTask extends AsyncTask<Void, Void, Void> {

	private JSONObject jsonResponse = null;
	String responseText = null;
	String responseCode = null;

	String faqId;
	String faqListId;
	String question, answer, categoryName;
	String questions[] = {};
	String faqIDs[] = {};

	Activity activity;
	ProgressDialog mDialog;
	UrlRequestParams urlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	/**
	 * Constructor
	 * 
	 * @param actvty
	 *            Activity instance
	 * @param quest
	 *            Selected question
	 * @param faqid
	 *            FaqID associated with the question
	 * @param catName
	 *            Category to which the selected question belongs
	 * @param quests
	 *            Array of all the questions of the category
	 * @param faqID
	 *            Array of all the FaqIds of the category
	 * @param faqListid
	 * 
	 */
	public FetchAnswersTask(Activity actvty, String quest, String faqid,
			String catName, String quests[], String faqID[], String faqListid) {

		activity = actvty;

		questions = quests;
		faqIDs = faqID;

		faqListId = faqListid;
		faqId = faqid;

		question = quest;
		categoryName = catName;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mDialog = ProgressDialog.show(activity, "", Constants.DIALOG_MESSAGE,
				true);
		mDialog.setCancelable(false);
	}

	@Override
	protected Void doInBackground(Void... params) {

		String urlParameters = urlRequestParams.createFaqDetailsParam(faqId,
				faqListId);
		String faq_details_url = Properties.url_local_server
				+ Properties.hubciti_version + "firstuse/faqdetails";
		jsonResponse = mServerConnections.getUrlPostResponseWithHtmlContent(
				faq_details_url, urlParameters, true);

		if (jsonResponse != null) {
			try {

				if (jsonResponse.has("FAQDetails")) {
					jsonResponse = jsonResponse.getJSONObject("FAQDetails");
				} else if (jsonResponse.has("response")) {
					jsonResponse = jsonResponse.getJSONObject("response");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		try {
			responseCode = jsonResponse.getString("responseCode");
			responseText = jsonResponse.getString("responseText");

			if ("10000".equals(responseCode)) {

				if (jsonResponse.has("answer")) {
					try {
						answer = URLDecoder.decode(
								jsonResponse.getString("answer"), "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}

				if (FaqScreen.isFaqAnswerScreen) {
					FaqAnswersScreen.setData(answer);

				} else {

					final Intent intent = new Intent(activity,
							FaqAnswersScreen.class);

					if (question != null) {
						intent.putExtra("question", question);
					}

					if (answer != null) {
						intent.putExtra("answer", answer);
					}

					if (categoryName != null) {
						intent.putExtra("categoryName", categoryName);
					}

					if (questions != null) {
						intent.putExtra("questions", questions);
					}

					if (faqIDs != null) {
						intent.putExtra("faqIDs", faqIDs);
					}

					activity.startActivity(intent);

				}

			} else {
				mDialog.dismiss();
				displayAlert(responseText);

			}
			mDialog.dismiss();
		} catch (JSONException e) {
			e.printStackTrace();
		}



	}

	/**
	 * Displays an Alert Dialog
	 * 
	 * @param message
	 *            message to be displayed in the dialog
	 */
	private void displayAlert(String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		alertDialogBuilder.setCancelable(false);
		alertDialogBuilder.setMessage(message);
		alertDialogBuilder.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// activity.finish();
					}
				});
		alertDialogBuilder.show();
	}

}
