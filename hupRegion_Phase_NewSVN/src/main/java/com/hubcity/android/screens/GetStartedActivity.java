package com.hubcity.android.screens;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.hubcity.android.commonUtil.Constants;
import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class GetStartedActivity extends Activity implements OnClickListener {

    String strPushRegMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getstarted);
        strPushRegMsg = this.getIntent().getStringExtra(
                Constants.PUSH_NOTIFY_MSG);
        Bundle intent = getIntent().getExtras();
        String btnColor = null;
        if(getIntent().hasExtra("btnColor")) {
             btnColor = intent.getString("btnColor");
        }
        String btnFontColor = null;
        if(getIntent().hasExtra("btnFontColor")) {
             btnFontColor = intent.getString("btnFontColor");
        }
        Button getStarted = (Button) findViewById(R.id.getstartedbtn);
        GradientDrawable getStartedShape = (GradientDrawable) getStarted
                .getBackground();
        if (btnColor != null) {
            getStartedShape.setColor(Color.parseColor(btnColor));
        }
        if (btnFontColor != null) {
            getStarted.setTextColor(Color.parseColor(btnFontColor));
        }
        getStarted.setText("Get Started");
        getStarted.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.getstartedbtn:
                SharedPreferences setprefs = getSharedPreferences(
                        Constants.PREFERENCE_HUB_CITY, 0);
                setprefs.edit().putBoolean("isFirstLaunch", true).apply();
                startNextActivity();
                break;
            default:
                break;
        }
    }

    public void startNextActivity() {
        SharedPreferences setprefs = getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, 0);
        setprefs.edit().putBoolean("isFirstLaunch", true).apply();
        String level = "1";
        String mItemId = "0";
        String mLinkId = "0";
        Constants.SHOWGPSALERT = true;
        if (strPushRegMsg != null) {
            SubMenuStack.clearSubMenuStack();
            Constants.startPushList(strPushRegMsg, GetStartedActivity.this);
        } else {
            if (GlobalConstants.isFromNewsTemplate) {
                if (GlobalConstants.className != null && GlobalConstants.className.equalsIgnoreCase(Constants.SCROLLING)) {
                    startActivity(new Intent(GetStartedActivity.this, ScrollingPageActivity.class));
                } else if (GlobalConstants.className != null && GlobalConstants.className.equalsIgnoreCase(Constants.NEWS_TILE)) {
                    startActivity(new Intent(GetStartedActivity.this, TwoTileNewsTemplateActivity.class));
                } else {
                    startActivity(new Intent(GetStartedActivity.this, CombinationTemplate.class));
                }
            } else {
                SubMenuStack.clearSubMenuStack();
                MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null, null, 0,
                        null, mLinkId, null, null, null, null, level, null, null, null,
                        null, null, null, null);
                SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

                new MenuAsyncTask(this).execute(level, mItemId, "0", "None", "0", "0");
            }
        }
    }
}
