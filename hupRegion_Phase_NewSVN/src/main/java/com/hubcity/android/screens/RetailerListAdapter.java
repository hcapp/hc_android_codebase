package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;

public class RetailerListAdapter extends BaseAdapter
{
	Activity activity;
	private ArrayList<HashMap<String, String>> retailerList;
	private static LayoutInflater inflater = null;
	String phoneNo = null;

	public RetailerListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> retailerList)
	{
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.retailerList = retailerList;
	}

	@Override
	public int getCount()
	{
		return retailerList.size();
	}

	@Override
	public Object getItem(int id)
	{
		return retailerList.get(id);
	}

	@Override
	public long getItemId(int id)
	{
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		ViewHolder holder;
		if (convertView == null) {
			view = inflater.inflate(R.layout.listitem_retailer_infos, parent, false);
			holder = new ViewHolder();
			holder.pageImage = (ImageView) view
					.findViewById(R.id.retailer_item_image);
			holder.pageTitle = (TextView) view
					.findViewById(R.id.retailer_item_name);
			holder.pageInfo = (TextView) view
					.findViewById(R.id.retailer_item_detail);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		holder.pageTitle.setText(retailerList.get(position).get(
				CommonConstants.TAG_PAGE_TITLE));
		holder.pageInfo.setText(retailerList.get(position).get(
				CommonConstants.TAG_PAGE_INFO));
		if (retailerList.get(position).get(CommonConstants.TAG_PAGE_IMAGE) != null
				&& !"N/A".equals(retailerList.get(position).get(
				CommonConstants.TAG_PAGE_IMAGE))) {
			holder.pageImage.setTag(retailerList.get(position).get(
					CommonConstants.TAG_PAGE_IMAGE));
			new CustomImageLoader(holder.pageImage, activity, false).displayImage(retailerList.get
					(position).get(
					CommonConstants.TAG_PAGE_IMAGE), activity, holder.pageImage);
//			new ImageLoaderAsync(holder.pageImage)
//					.execute(retailerList.get(position).get(
//							CommonConstants.TAG_PAGE_IMAGE));
		} else if (retailerList.get(position)
				.get(CommonConstants.TAG_PAGE_LINK) != null) {
			String tempLink = retailerList.get(position).get(
					CommonConstants.TAG_PAGE_LINK);
			if ("specials".equalsIgnoreCase(tempLink)) {
				holder.pageImage
						.setImageResource(R.drawable.retailers_curnt_spcls);

			} else if ("directions".equalsIgnoreCase(tempLink)) {
				holder.pageImage
						.setImageResource(R.drawable.retailers_get_directions);

			} else if ("call".equalsIgnoreCase(tempLink)) {
				holder.pageImage
						.setImageResource(R.drawable.retailers_phone_icon);
				phoneNo = retailerList.get(position).get(
						CommonConstants.TAG_PAGE_INFO);
				if (null != phoneNo && !"N/A".equals(phoneNo)
						&& !"".equals(phoneNo) && phoneNo.length() == 10) {
					phoneNo = "(" + phoneNo.substring(0, 3) + ") "
							+ phoneNo.substring(3, 6) + "-"
							+ phoneNo.substring(6, 10);
					holder.pageInfo.setText(phoneNo);
				}

			} else if ("browser".equalsIgnoreCase(tempLink)) {

				holder.pageImage
						.setImageResource(R.drawable.retailers_browse);
			}
		}

		return view;
	}

	public static class ViewHolder
	{
		protected TextView pageTitle;
		protected ImageView pageImage;
		protected TextView pageInfo;
	}
}
