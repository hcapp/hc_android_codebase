package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class SquareViewAdapter extends ArrayAdapter<MainMenuBO>
{

	Activity mContext;
	int resourceId;
	ArrayList<MainMenuBO> data;
	private CustomImageLoader customImageLoader;
	String btnFontColor, btnColor, mFontColor, smFontColor, fontColor;
	String level;

	private static int width = -1;

	public SquareViewAdapter(Activity context, int layoutResourceId,
			ArrayList<MainMenuBO> data, String menuLevel, String mFontColor,
			String smFontColor)
	{
		super(context, layoutResourceId, data);
		this.mContext = context;
		this.resourceId = layoutResourceId;
		this.data = data;
		this.mFontColor = mFontColor;
		this.smFontColor = smFontColor;
		level = menuLevel;
		customImageLoader = new CustomImageLoader(context, false);

		DisplayMetrics metrics = new DisplayMetrics();
		mContext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		width = metrics.widthPixels;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		MainMenuBO item = data.get(position);
		String itmItemImgUrl = item.getmItemImgUrl();

		View itemView = convertView;
		ViewHolder holder = null;

		if (itemView == null) {
			final LayoutInflater layoutInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			itemView = layoutInflater.inflate(resourceId, parent, false);

			holder = new ViewHolder();
			holder.imgItem = (ImageView) itemView.findViewById(R.id.imgItem);
			holder.txtItem = (TextView) itemView.findViewById(R.id.txtItem);

			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					width / 4, width / 4);
			layoutParams.setMargins(5, 5, 5, 5);
			layoutParams.width = (width / 4) - (layoutParams.leftMargin * 5);

			holder.imgItem.setLayoutParams(layoutParams);

			itemView.setTag(holder);

		} else {
			holder = (ViewHolder) itemView.getTag();
		}

		String itemName = item.getmItemName();

		customImageLoader.displayImage(itmItemImgUrl, mContext, holder.imgItem);

		holder.txtItem.setText(itemName);

		if (level != null && "1".equals(level)) {
			fontColor = mFontColor;

		} else {
			fontColor = smFontColor;

		}

		if (fontColor != null) {
			holder.txtItem.setTextColor(Color.parseColor(fontColor));
		}

		return itemView;

	}

	static class ViewHolder
	{
		ImageView imgItem;
		TextView txtItem;
	}
}