package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hubcity.android.businessObjects.PushListScreenBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.scansee.android.CouponsDetailActivity;
import com.scansee.android.HotDealsDetailsActivity;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class PushNotificationListScreen extends CustomTitleBar {
    ListView listview = null;
    ArrayList<String> list = null, linklist = null;
    ArrayList<PushListScreenBO> arrPushList = new ArrayList<>();
    String names[] = null, dealsID[] = null, dealsRetID[] = null, dealsRetLocID[] = null, links[]
            = null,
            dealsName[] = null, dealsType[] = null, splUrl[] = null,
            strpushmsg = null, StrLink = null, strNotifyMsg = null;
    TextView csPushtxt = null;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);

        try {
            title.setText("Notification List");
            csPushtxt = (TextView) findViewById(R.id.pustxt);
            strpushmsg = this.getIntent().getStringExtra(Constants.PUSH_NOTIFY_MSG);
            Log.v("", "SERVVICE SUCCESS  : " + strpushmsg);
            listview = (ListView) findViewById(R.id.notification_list);
            list = new ArrayList<>();
            linklist = new ArrayList<>();
            ArrayList<String> arrPushMsg = Constants.getPushArray("gcm_msg");
            if (arrPushMsg != null && arrPushMsg.size() > 0) {
                new pushMsgRefresh().execute();
            } else {

                csPushtxt.setVisibility(View.VISIBLE);
            }
            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (SubMenuStack.getSubMenuStack() != null
                            && SubMenuStack.getSubMenuStack().size() > 0) {
                        finish();
                    } else {
                        setBackPush();
                    }
                }
            });
            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // is there any way to write here so that i can avoid
                    // putting in each subclass having edittext ?
                    // u got it?
                    if (GlobalConstants.isFromNewsTemplate) {
                        Intent intent = null;
                        switch (GlobalConstants.className) {
                            case Constants.COMBINATION:
                                intent = new Intent(PushNotificationListScreen.this, CombinationTemplate.class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(PushNotificationListScreen.this, ScrollingPageActivity.class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(PushNotificationListScreen.this, TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    } else {
                        setBackPush();
                    }
                }
            });
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {
                    final String selected_item = (String) parent
                            .getItemAtPosition(position);

                    if (arrPushList.get(position).getDealName() != null) {
                        Intent intent = null;
                        if ("Coupons".equals(arrPushList.get(position).getDealType())) {
                            intent = new Intent(PushNotificationListScreen.this,
                                    CouponsDetailActivity.class);
                            intent.putExtra("couponId", arrPushList.get(position).getDealId());
                            intent.putExtra("couponName", arrPushList.get(position).getDealName());
                            intent.putExtra("push", "pushnotify");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if ("Hotdeals".equals(arrPushList.get(position).getDealType())) {
                            intent = new Intent(PushNotificationListScreen.this,
                                    HotDealsDetailsActivity.class);
                            intent.putExtra("hotDealId", arrPushList.get(position).getDealId());
                            intent.putExtra("hotDealName", arrPushList.get(position).getDealName
                                    ());
                            intent.putExtra("push", "pushnotify");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if ("SpecialOffers".equals(arrPushList.get(position).getDealType()
                        )) {
                            if (arrPushList.get(position).getSpecialUrl() != null && !arrPushList
                                    .get
                                            (position).getSpecialUrl().equals("")) {
                                Intent retailerLink = new Intent(
                                        PushNotificationListScreen.this,
                                        ScanseeBrowserActivity.class);
                                retailerLink.putExtra(CommonConstants.URL, arrPushList.get
                                        (position).getSpecialUrl());
                                startActivity(retailerLink);
                            } else {
                                intent = new Intent(PushNotificationListScreen.this,
                                        SpecialOffersScreen.class);
                                intent.putExtra("pageId", arrPushList.get(position).getDealId());
                                if (arrPushList.get(position).getDealRetId() != null && !"".equals
                                        (arrPushList.get(position).getDealRetId())) {
                                    intent.putExtra("retailerId", arrPushList.get(position)
                                            .getDealRetId());
                                }
                                if (arrPushList.get(position).getDealRetLocID() != null && !""
                                        .equals
                                                (arrPushList.get(position).getDealRetLocID())) {
                                    intent.putExtra("retailLocationId", arrPushList.get(position)
                                            .getDealRetLocID());
                                }
                                intent.putExtra("hotDealName", arrPushList.get(position)
                                        .getDealName
                                                ());
                                intent.putExtra("push", "pushnotify");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }

                    } else {
                        Intent retailerLink = new Intent(
                                PushNotificationListScreen.this,
                                ScanseeBrowserActivity.class);
                        retailerLink.putExtra(CommonConstants.URL, arrPushList.get(position)
                                .getNewsLink());
                        startActivity(retailerLink);
                    }
                }

            });
            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setBackPush() {
        // @Ramachandran
        if (SortDepttNTypeScreen.subMenuDetailsList != null) {
            SortDepttNTypeScreen.subMenuDetailsList.clear();
            SubMenuStack.clearSubMenuStack();
            SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
        }
        LoginScreenViewAsyncTask.bIsLoginFlag = true;
        callingMainMenu();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (SubMenuStack.getSubMenuStack() != null
                && SubMenuStack.getSubMenuStack().size() > 0) {
            finish();
        } else {
            setBackPush();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        try {
            client.connect();
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW, // TODO: choose an action type.
                    "PushNotificationListScreen Page", // TODO: Define a title for the content
                    // shown.
                    // TODO: If you have web page content that matches this app activity's content,
                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),
                    // TODO: Make sure this auto-generated app deep link URI is correct.
                    Uri.parse("android-app://com.scansee.hubregion/com.hubcity.android" +
                            ".screens/http/host/path")
            );
            AppIndex.AppIndexApi.start(client, viewAction);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        try {
            Action viewAction = Action.newAction(
                    Action.TYPE_VIEW, // TODO: choose an action type.
                    "PushNotificationListScreen Page", // TODO: Define a title for the content
                    // shown.
                    // TODO: If you have web page content that matches this app activity's content,
                    // make sure this auto-generated web page URL is correct.
                    // Otherwise, set the URL to null.
                    Uri.parse("http://host/path"),
                    // TODO: Make sure this auto-generated app deep link URI is correct.
                    Uri.parse("android-app://com.scansee.hubregion/com.hubcity.android" +
                            ".screens/http/host/path")
            );
            AppIndex.AppIndexApi.end(client, viewAction);
            client.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ArrayList<String> arrPushMsg;

    @SuppressWarnings("rawtypes")
    private class pushMsgRefresh extends AsyncTask {
        ProgressDialog csProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            csProgress = new ProgressDialog(PushNotificationListScreen.this);
            csProgress.setMessage("Loading..");
            csProgress.show();
        }

        @Override
        protected Object doInBackground(Object... params) {
            try {
                arrPushMsg = Constants.getPushArray("gcm_msg");
                arrPushList = new ArrayList<>();
                for (int i = 0; i < arrPushMsg.size(); i++) {
                    parsingJson(new JSONObject(arrPushMsg.get(i)), i);
                }
//				If the deal has expired then removing it from saved push and saving the array newly
                Constants.removePushArray("gcm_msg");
                Constants.savePushArray(arrPushMsg, "gcm_msg");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            try {
                csPushtxt.setVisibility(View.GONE);
                Collections.reverse(arrPushList);
                if (arrPushList != null && arrPushList.size() > 0) {
                    NotificationArrayAdapter adapter = new NotificationArrayAdapter(
                            PushNotificationListScreen.this
                    );
                    listview.setAdapter(adapter);
                } else {
                    Toast.makeText(PushNotificationListScreen.this, "Deal has expired.", Toast
                            .LENGTH_SHORT).show();
                }
                if (csProgress != null) {
                    csProgress.dismiss();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void parsingJson(JSONObject json, int position) {
        if (json != null) {

            try {
                if (json.has("rssFeedList")) {
                    JSONArray csjsonArr = json.getJSONArray("rssFeedList");
                    names = new String[csjsonArr.length()];
                    links = new String[csjsonArr.length()];
                    for (int index = 0; index < csjsonArr.length(); index++) {
                        PushListScreenBO pushListScreenBO = new PushListScreenBO();
                        pushListScreenBO.setNewsTitle(csjsonArr.getJSONObject(index)
                                .getString("title"));
                        pushListScreenBO.setNewsLink(csjsonArr.getJSONObject(index)
                                .getString("link"));
                        arrPushList.add(pushListScreenBO);
                    }
                }

                if (json.has("dealList")) {
                    JSONArray csjsonArrdeal = json.getJSONArray("dealList");
                    for (int index = 0; index < csjsonArrdeal.length(); index++) {
                        PushListScreenBO pushListScreenBO = new PushListScreenBO();
                        pushListScreenBO.setDealId(csjsonArrdeal.getJSONObject(index)
                                .getString("dealId"));
                        if (csjsonArrdeal.getJSONObject(index).has("retailerId")) {
                            pushListScreenBO.setDealRetId(csjsonArrdeal.getJSONObject(index)
                                    .getString("retailerId"));
                        }
                        if (csjsonArrdeal.getJSONObject(index).has("retailLocationId")) {
                            pushListScreenBO.setDealRetLocID(csjsonArrdeal.getJSONObject(index)
                                    .getString("retailLocationId"));
                        }

                        if (csjsonArrdeal.getJSONObject(index).has("endDate")) {
                            pushListScreenBO.setEndDate(csjsonArrdeal.getJSONObject(index)
                                    .getString("endDate"));
                        }

                        if (csjsonArrdeal.getJSONObject(index).has("endTime")) {
                            pushListScreenBO.setEndTime(csjsonArrdeal.getJSONObject(index)
                                    .getString("endTime"));
                        }
                        pushListScreenBO.setDealName(csjsonArrdeal.getJSONObject(index)
                                .getString("dealName"));
                        pushListScreenBO.setDealType(csjsonArrdeal.getJSONObject(index)
                                .getString("type"));
                        if (csjsonArrdeal.getJSONObject(index).has("splUrl")
                                && csjsonArrdeal.getJSONObject(index)
                                .getString("splUrl") != null) {
                            pushListScreenBO.setSpecialUrl(csjsonArrdeal.getJSONObject(index)
                                    .getString("splUrl"));
                        }
                        String expiredDate = pushListScreenBO.getEndDate() + " " +
                                "" + pushListScreenBO.getEndTime();
//						If the deal has expired then removing the deal else adding
                        if (!(Properties.HUBCITI_KEY.equals("Tyler19")||Properties.HUBCITI_KEY.equals("shrini2113")
                                ||Properties.HUBCITI_KEY.equals("Tyler Test75"))) {
                            if (Constants.getDealExpirationState(expiredDate)) {
                                Constants.removePushArrayByPosition("gcm_msg", position);
                                arrPushMsg.remove(position);
                            } else {
                                arrPushList.add(pushListScreenBO);
                            }
                        } else {
                            arrPushList.add(pushListScreenBO);
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private class NotificationArrayAdapter extends BaseAdapter {

        LayoutInflater layoutInflater;

        public NotificationArrayAdapter(Context context) {
            layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public int getCount() {
            return arrPushList.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder holder;
            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.push_notification_list, parent, false);
                holder = new ViewHolder();
                holder.textView = (TextView) view.findViewById(R.id.push_list_text);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            if (arrPushList.get(position).getNewsTitle() != null && !arrPushList.get(position)
                    .getNewsTitle().equals("")) {
                holder.textView.setText(arrPushList.get(position).getNewsTitle());
            } else {
                holder.textView.setText(arrPushList.get(position).getDealName());
            }

            return view;
        }
    }

    public static class ViewHolder {
        private TextView textView;
    }

}
