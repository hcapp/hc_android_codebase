package com.hubcity.android.screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.RoundedCornerImageLoader;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by subramanya.v on 2/15/2016.
 */
public class SquareImageTemplate extends MenuPropertiesActivity implements View.OnClickListener {
    private LinearLayout mFourImageViewParent;
    private LinearLayout mButtonLinearLayout;
    private ScrollView mscrollViewParent;
    private ProgressBar progressBar;
    private ImageView mBannerImageView;
    private RelativeLayout mBannerHolder;
    private static final int TOTAL_COLUMN = 2;
    private int isDisplayLabel;
    private int screenHeight;
    private int screenWeight;
    private int paddingLeft;
    private int paddingRight;
    String tempBkgrdImg;
    private View menuListTemplate;
    private Context mContext;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mContext = SquareImageTemplate.this;
            CommonConstants.hamburgerIsFirst = true;

            Intent intent = getIntent();
            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);


            RelativeLayout Parent = new RelativeLayout(this);
            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            Parent.setLayoutParams(param);

            LinearLayout ParentIntermideate = new LinearLayout(this);
            ParentIntermideate.setOrientation(LinearLayout.VERTICAL);
            handleIntent(intent);
            bindView();
            setVisibilty();


            LinearLayout.LayoutParams gridViewParentParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            BottomButtonListSingleton mBottomButtonListSingleton;
            mBottomButtonListSingleton = BottomButtonListSingleton
                    .getListBottomButton();

            if (mBottomButtonListSingleton != null) {

                ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                        .listBottomButton;

                if (listBottomButton != null && listBottomButton.size() > 1) {
                    int px = (int) (52 * this.getResources().getDisplayMetrics().density + 0.5f);
                    gridViewParentParam.setMargins(0, 0, 0, px);
                }
            }

            mFourImageViewParent.setLayoutParams(gridViewParentParam);
            LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            ParentIntermideate.setLayoutParams(parentParam);
            addTitleBar(ParentIntermideate, Parent);
            ParentIntermideate.addView(mFourImageViewParent);
            Parent.addView(ParentIntermideate);

            createbottomButtontTab(Parent);
            setContentView(Parent);


            ViewTreeObserver observer = mscrollViewParent.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {


                    screenHeight = mscrollViewParent.getMeasuredHeight();
                    screenWeight = mscrollViewParent.getMeasuredWidth();
                    ViewTreeObserver obs = mscrollViewParent.getViewTreeObserver();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }
                    createImageScreens(mainMenuUnsortedList);
                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setVisibilty() {
        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if (null != mBannerImg) {
                mBannerImageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(SquareImageTemplate.this).load(mBannerImg).into(mBannerImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        } else {
            customHorizontalTicker = (CustomHorizontalTicker) menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker) menuListTemplate.findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher) menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext, customHorizontalTicker, customVerticalTicker, bannerParent, customRotate, mBannerHolder);
        }
    }

    private void bindView() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        menuListTemplate = layoutInflater.inflate(R.layout.square_image_template,
                null, false);
        // Initializing drawer layout
        drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);
        mFourImageViewParent = (LinearLayout) menuListTemplate
                .findViewById(R.id.fourImageViewParent);
        mButtonLinearLayout = (LinearLayout) menuListTemplate
                .findViewById(R.id.buttonLinearLayout);
        mscrollViewParent = (ScrollView) menuListTemplate
                .findViewById(R.id.scrollViewParent);
        mBannerHolder = (RelativeLayout) menuListTemplate
                .findViewById(R.id.banner_holder);
        bannerParent = (LinearLayout) menuListTemplate.findViewById(R.id.bannerParent);
        mBannerImageView = (ImageView) menuListTemplate
                .findViewById(R.id.top_image);
        mBannerImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        progressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
        if (tempBkgrdImg != null && !tempBkgrdImg.equals("")) {
            new ImageLoaderAsync(mscrollViewParent).execute(tempBkgrdImg);
        }

    }

    private void createImageScreens(ArrayList<MainMenuBO> mainMenuUnsortedList) {
        int TOTAL_BUTTON = mainMenuUnsortedList.size();

        int totalRow;
        if (TOTAL_BUTTON % TOTAL_COLUMN != 0) {
            totalRow = TOTAL_BUTTON / TOTAL_COLUMN;
            totalRow = totalRow + 1;
        } else {
            totalRow = TOTAL_BUTTON / TOTAL_COLUMN;
        }
        int itemCount = 0;
        for (int row = 0; row < totalRow; row++) {
            LinearLayout rowLayout = createRowLayout();
            rowLayout.setWeightSum(10.0f);
            int colCount = 0;
            for (int column = 0; column < TOTAL_COLUMN; column++) {
                if (itemCount <= TOTAL_BUTTON
                        && itemCount < mainMenuUnsortedList.size()) {
                    ImageView image = new ImageView(this);
                    image.setTag(itemCount);
                    image.setOnClickListener(this);
                    image.setAdjustViewBounds(true);

                    MainMenuBO buttonMainMenu = mainMenuUnsortedList
                            .get(itemCount);
                    LinearLayout colLayout = new LinearLayout(this);
                    LinearLayout.LayoutParams colLayoutparam = new LinearLayout.LayoutParams(
                            0, ViewGroup.LayoutParams.MATCH_PARENT);
                    colLayoutparam.weight = 5.0f;

                    if (colCount == 0) {
                        colLayout.setPadding(100, 100, 50, 0);
                    } else {
                        colLayout.setPadding(50, 100, 100, 0);
                    }


                    colLayout.setOrientation(LinearLayout.VERTICAL);
                    colLayout.setLayoutParams(colLayoutparam);


                    paddingLeft = colLayout.getPaddingLeft();
                    paddingRight = colLayout.getPaddingRight();


                    if (isDisplayLabel == 1) {
                        TextView buttonText = new TextView(this);
                        buttonText.setTag(itemCount);
                        buttonText = createButtonTextProperties(buttonText, buttonMainMenu);
                        image = createImageProperties(image, buttonMainMenu);
                        colLayout.addView(image);
                        colLayout.addView(buttonText);
                    } else {
                        image = createImageProperties(image, buttonMainMenu);
                        colLayout.addView(image);
                    }
                    rowLayout.addView(colLayout);
                    itemCount++;
                    colCount++;
                }
            }
            mButtonLinearLayout.addView(rowLayout);
        }


    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);
        mMainMenuBO.setmBkgrdColor(mBkgrdColor);
        mMainMenuBO.setmBkgrdImage(mBkgrdImage);
        mMainMenuBO.setSmBkgrdColor(smBkgrdColor);
        linkClickListener(mMainMenuBO.getLinkTypeName(), mMainMenuBO);
    }

    private TextView createButtonTextProperties(TextView buttonText, MainMenuBO buttonMainMenu) {

        buttonText.setOnClickListener(this);
        buttonText.setText(buttonMainMenu.getmItemName());
        buttonText.setTextSize(16f);
        buttonText.setGravity(Gravity.TOP | Gravity.CENTER);
        LinearLayout.LayoutParams buttonpar = new LinearLayout.LayoutParams(
                ((screenWeight / 2) - paddingLeft - paddingRight), ViewGroup.LayoutParams.MATCH_PARENT);
        buttonText.setPadding(0, 2, 0, 0);
        if ("1".equals(previousMenuLevel)) {
            String buttonColour = buttonMainMenu.getmBtnFontColor();
            buttonText.setTextColor(Color.parseColor(buttonColour));
        } else {
            String buttonColour = buttonMainMenu.getSmBtnFontColor();
            buttonText.setTextColor(Color.parseColor(buttonColour));
        }
        buttonText.setLayoutParams(buttonpar);
        return buttonText;
    }


    private ImageView createImageProperties(ImageView Image, MainMenuBO buttonMainMenu) {

        String mItemRightImgUrl = buttonMainMenu.getmItemImgUrl();

        LinearLayout.LayoutParams buttonLayoutParam;
        buttonLayoutParam = new LinearLayout.LayoutParams(
                ((screenWeight / 2) - paddingLeft - paddingRight), ((screenWeight / 2) - paddingLeft - paddingRight));


        Image.setScaleType(ImageView.ScaleType.FIT_XY);
        RoundedCornerImageLoader mRoundCornerImageLoader = new RoundedCornerImageLoader
                (SquareImageTemplate.this,
                        false);
        mRoundCornerImageLoader.displayImage(mItemRightImgUrl, SquareImageTemplate.this,
                Image);
        Image.setLayoutParams(buttonLayoutParam);
        return Image;
    }

    @Override
    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);


            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("smBkgrdImage");
            departFlag = intent.getExtras().getString("departFlag");
            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
            mFontColor = intent.getExtras().getString("mFontColor");
            smFontColor = intent.getExtras().getString("smFontColor");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");
            if (intent.hasExtra("isDisplayLabel")) {
                isDisplayLabel = intent.getExtras().getInt("isDisplayLabel");
            }
            if (intent.hasExtra("TempBkgrdImg")) {
                tempBkgrdImg = intent.getExtras().getString("TempBkgrdImg");
            }

        }

        super.handleIntent(intent);

    }

    class ImageLoaderAsync extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(SquareImageTemplate.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {


                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {
            try {
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                BitmapDrawable background = new BitmapDrawable(bmImg);
                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    bmImage.setBackgroundDrawable(background);
                } else {
                    bmImage.setBackgroundDrawable(background);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        super.onNewIntent(intent);
    }

    private LinearLayout createRowLayout() {
        LinearLayout rowLayout = new LinearLayout(this);
        rowLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, screenHeight / 2);
        rowLayout.setLayoutParams(lp);
        return rowLayout;
    }

    @Override
    protected void onResume() {
        super.onResume();
        activity = this;
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }

}
