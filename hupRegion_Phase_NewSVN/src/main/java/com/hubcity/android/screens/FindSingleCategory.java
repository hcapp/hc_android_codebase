package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.hubcity.android.CustomLayouts.ToggleButtonView;
import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.FindLocationActivity;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class FindSingleCategory extends CustomTitleBar implements
		OnClickListener, ToggleButtonView.ToggleViewInterface
{

	String searchKey;
	boolean gpsEnabled = false;

	private ArrayList<HashMap<String, String>> findlocationList = new ArrayList<>();
	private ArrayList<HashMap<String, String>> retailersList = new ArrayList<>();

	TextView moreInfo;
	View moreResultsView;

	protected ListView findlocationListView;
	String accZipcode = null;

	Button mCancel;
	String userID;
	String latitude = null;
	String longitude = null;
	String catName;
	int lastVisitedNo = 0;
	String subCatId = "";

	LocationManager locationManager;
	MenuItem item;
	String mItemId, mBottomId;
	Activity activity;
	boolean isViewMore;
	boolean isRefresh;
	ArrayList<String> arrRetailerId;
	ArrayList<String> arrRetailerLocId;
	String responseText, filterIds;

	String lastVistedProductNo = "0";
	EditText retailerEditBox;
	String locationText;

	ArrayList<Item> items = new ArrayList<>();
	String sortby = "Distance";

	String mLinkId;
	boolean isSearchresult = false;
	LinearLayout linearLayout = null;
	BottomButtons bb = null;
	boolean hasBottomBtns = false;
	boolean enableBottomButton = true;
	String strMainMenuId = "";
	FindLocationCategory findLocCat;
	boolean isFirst;
	HubCityContext mHubCiti;
	String locSpecials = "0", interests = "";
	private ProgressDialog mDialog;
	boolean nextPage, isAlreadyLoading, isItemClicked;
	TextView mLabelDistance, mLabelName;
	private TextView sortTextView;
	FindSingleCategoryListAdapter adapter;
	private CustomNavigation customNaviagation;
	private int visibleItemCount = 0;
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_single_category_activity);
		CommonConstants.hamburgerIsFirst = true;
		isRefresh = true;
		isFirst = true;
		isViewMore = false;
		mHubCiti = (HubCityContext) getApplicationContext();
		mHubCiti.setCancelled(false);
		HubCityContext.savedValues = new HashMap<>();
		// Initiating Bottom button class
		activity = FindSingleCategory.this;
		try {
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		linearLayout = (LinearLayout) findViewById(R.id.findParentLayout);
		linearLayout.setVisibility(View.INVISIBLE);

		if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
				&& getIntent().getExtras().getString(
				Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
			mItemId = getIntent().getExtras().getString(
					Constants.MENU_ITEM_ID_INTENT_EXTRA);
		} else if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
				&& getIntent().getExtras().getString(
				Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
			mBottomId = getIntent().getExtras().getString(
					Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
		}

		if (getIntent().hasExtra("catName")) {
			catName = getIntent().getExtras().getString("catName");
			HubCityContext hubciti = (HubCityContext) getApplicationContext();
			hubciti.setFindSingleCatName(catName);
		}

		if (getIntent().hasExtra("mLinkId")) {
			mLinkId = getIntent().getExtras().getString("mLinkId");

		} else if (getIntent().hasExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA)) {
			mLinkId = getIntent().getExtras().getString(
					Constants.BOTTOM_LINK_ID_INTENT_EXTRA);
		}

		Constants.FIND_SINGLE_CAT_LINK_ID = mLinkId;

		if (getIntent().hasExtra("subCatId")) {
			subCatId = getIntent().getExtras().getString("subCatId");

		}

		if (getIntent().hasExtra("srchKey")) {
			searchKey = getIntent().getExtras().getString("srchKey");

		}
		mLabelName = (TextView) this.findViewById(R.id.toggle_name);
		sortTextView = (TextView) findViewById(R.id.sort_text);
		mLabelDistance = (TextView) this.findViewById(R.id.toggle_distance);
		title.setText(catName);
		leftTitleImage.setVisibility(View.INVISIBLE);
//		rightImage.setVisibility(View.GONE);
		divider.setVisibility(View.GONE);

		retailerEditBox = (EditText) findViewById(R.id.find_retailer_edit_text);
		mCancel = (Button) findViewById(R.id.find_retailer_cancel);
		mCancel.setOnClickListener(this);

		retailerEditBox.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		retailerEditBox.setOnEditorActionListener(new OnEditorActionListener()
		{

			@Override
			public boolean onEditorAction(TextView textView, int actionId,
										  KeyEvent event)
			{
				onClick(textView);
				hideKeyboardItem();
				return true;
			}
		});

		retailerEditBox.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence sequence, int start,
									  int before, int count)
			{
				String value = sequence.toString();
				locationText = value.trim();
			}

			@Override
			public void afterTextChanged(Editable arg0)
			{
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
										  int arg2, int arg3)
			{
			}

		});

		backImage.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				hideKeyboardItem();
				finishAction();
			}
		});

		SharedPreferences settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
		userID = settings.getString(CommonConstants.USER_ID, "0");

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		findlocationListView = (ListView) findViewById(R.id.find_location_listview_list);

		moreResultsView = getLayoutInflater().inflate(
				R.layout.listitem_get_retailers_viewmore, findlocationListView,
				false);
		moreInfo = (TextView) moreResultsView.findViewById(R.id.viewMore);
		moreInfo.setOnClickListener(this);

		findlocationListView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id)
			{
				try {
					isItemClicked = true;
					findNavigation(position);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		hideKeyBoard();

		if (gpsEnabled) {
			getGPSValues();
		}



		if (gpsEnabled) {
			getGPSValues();
		}  else {
			if (!getIntent().hasExtra("isSort")
					&& !getIntent().getExtras().getBoolean("isSort")) {
				new AlertDialog.Builder(this).setTitle("Alert")
						.setMessage(R.string.find_alert_nozipcode)
						.setPositiveButton("OK", null).show();
			}
		}
		accZipcode = Constants.getZipCode();


		filterIds = "";

		if (getIntent().hasExtra("filterIds")) {
			filterIds = getIntent().getExtras().getString("filterIds");

		}



		/***
		 * Calling the progress dialog for the first time only After that it
		 * will show the bottom bar of list as loader
		 ***/
		mDialog = ProgressDialog.show(FindSingleCategory.this, "",
				Constants.DIALOG_MESSAGE, true);
		mDialog.show();
		mDialog.setCancelable(false);
		callFindAsyncTask();

		//user for hamburger in modules
		drawerIcon.setVisibility(View.VISIBLE);
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		customNaviagation = (CustomNavigation) findViewById(R.id.custom_navigation);
		findlocationListView.setOnScrollListener(new AbsListView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				FindSingleCategory.this.visibleItemCount = visibleItemCount;
				Log.d("visibleItemCount : ",""+visibleItemCount);
			}
		});

	}

	@Override
	public void onToggleClick(String toggleBtnName)
	{
		if (toggleBtnName.equalsIgnoreCase("Distance")) {
			sortTextView.setText(R.string.sort_by_distance);
			sortby = "distance";
			setToggleButtonColor();
			refresh();
		} else if (toggleBtnName.equalsIgnoreCase("Name")) {
			sortTextView.setText(R.string.sort_by_name);
			sortby = "atoz";
			setToggleButtonColor();
			refresh();
		} else {
			CommonConstants.startMapScreen(FindSingleCategory.this, getRetailerDetails(), false);
		}
	}
	private void setToggleButtonColor()
	{
		if (sortby.equalsIgnoreCase("Distance")) {
			sortTextView.setText(R.string.sort_by_distance);
			if (Build.VERSION.SDK_INT >= 16) {
				mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
				mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
			}else{
				mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
				mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
			}

			mLabelName.setTextColor(getResources().getColor(R.color.white));
			mLabelName.setTypeface(null, Typeface.NORMAL);
			mLabelDistance.setTextColor(getResources().getColor(R.color.black));
			mLabelDistance.setTypeface(null, Typeface.BOLD);
		}
		if (sortby.equalsIgnoreCase("Name") || sortby.equalsIgnoreCase("atoz")) {
			sortTextView.setText(R.string.sort_by_name);
			if (Build.VERSION.SDK_INT >= 16) {
				mLabelDistance.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
				mLabelName.setBackground(ContextCompat.getDrawable(this, R.drawable.radio_selected));
			}else{
				mLabelDistance.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_unselected));
				mLabelName.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.radio_selected));
			}
			mLabelDistance.setTextColor(getResources().getColor(R.color.white));
			mLabelDistance.setTypeface(null, Typeface.NORMAL);
			mLabelName.setTextColor(getResources().getColor(R.color.black));
			mLabelName.setTypeface(null, Typeface.BOLD);
		}
	}
	private void getSortItemName(){

		HashMap<String, String> resultSet;
		resultSet = ((HubCityContext)getApplicationContext()).getFilterValues();
		if (resultSet != null && !resultSet.isEmpty()) {
			if (resultSet.containsKey("SortBy")) {
				sortby = resultSet.get("SortBy");
			}
		}
		setToggleButtonColor();
	}



	private void findNavigation(int position)
	{

		Item i = items.get(position);

		FindSingleCategoryEntryItem ei = (FindSingleCategoryEntryItem) i;

		Intent findlocationInfo = new Intent(FindSingleCategory.this,
				RetailerActivity.class);
		findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
				ei.retailerId);

		findlocationInfo.putExtra(CommonConstants.TAG_RETAILE_LOCATIONID,
				ei.retailerLocId);
		findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_LIST_ID,
				ei.retListId);
		findlocationInfo.putExtra(Constants.MAIN_MENU_ID, strMainMenuId);
		findlocationInfo
				.putExtra(CommonConstants.TAG_DISTANCE, ei.distance);
		findlocationInfo.putExtra(CommonConstants.TAG_FINDLOCATION_LAT,
				ei.latitude);
		findlocationInfo.putExtra(CommonConstants.TAG_FINDLOCATION_LON,
				ei.longitude);
		try {
			findlocationInfo.putExtra("zipCode", accZipcode);
			findlocationInfo.putExtra("fromsearch", true);
		} catch (Exception e) {
			e.printStackTrace();

		}
		findlocationInfo.putExtra(CommonConstants.TAG_RETAILER_NAME,
				ei.retailerName);

		findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
				ei.ribbonAdImagePath);
		findlocationInfo.putExtra(CommonConstants.TAG_RIBBON_AD_URL,
				ei.ribbonAdURL);
		findlocationInfo.putExtra(
				CommonConstants.TAG_FINDLOCATION_REFERENCE, ei.reference);
		findlocationInfo.putExtra(CommonConstants.TAG_BANNER_IMAGE_PATH,
				ei.bannerAd);

		findlocationInfo.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
				mItemId);

		findlocationInfo.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
				mBottomId);

		findlocationInfo.putExtra("Find", true);
		startActivityForResult(findlocationInfo, Constants.STARTVALUE);

		mHubCiti.setCancelled(false);



	}

	private void hideKeyBoard()
	{
		FindSingleCategory.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private void hideKeyboardItem()
	{
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(retailerEditBox.getWindowToken(), 0);
	}

	@Override
	public void onDestroy()
	{

		HubCityContext mHubCityContext = (HubCityContext) getApplicationContext();
		mHubCityContext.clearArrayAndAllValues(false);
		mHubCityContext.setFindGroupSelection("atoz");
		mHubCityContext.setFindSortSelection("distance");

		if (locationManager != null) {
			locationManager.removeUpdates(new ScanSeeLocListener());
		}
		HubCityContext.isDoneClicked = false;
		cancelFindAsyn();

		super.onDestroy();
	}

	private class FindLocationCategory extends AsyncTask<String, Void, String>
	{

		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params)
		{

			String result = "false";
			if (isSearchresult) {

				retailersList.clear();
			}

			try {
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();

				HashMap<String, String> resultSet;
				resultSet = mHubCiti.getFilterValues();

				String cityIds = "", filterId = "", filterValueId = "";
				String sorted_cities = "";
				sorted_cities = Constants.getSortedCityIds(new CityDataHelper().getCitiesList
						(FindSingleCategory.this));
				if (resultSet != null && !resultSet.isEmpty()) {

					if (resultSet.containsKey("locSpecials")) {
						locSpecials = resultSet.get("locSpecials");
					}

					if (resultSet.containsKey("savedinterests")) {
						interests = resultSet.get("savedinterests");
					}

					if (resultSet.containsKey("savedOptionFilterIds")) {
						filterId = resultSet.get("savedOptionFilterIds");
					}

					if (resultSet.containsKey("savedFValueIds")) {
						filterValueId = resultSet.get("savedFValueIds");
					}

					if (resultSet.containsKey("savedCityIds")) {
						cityIds = resultSet.get("savedCityIds");
						cityIds = cityIds.replace("All,", "");
					}

					if (resultSet.containsKey("savedSubCatIds")) {
						subCatId = resultSet.get("savedSubCatIds");
					}

				}

				if (cityIds != null && !cityIds.isEmpty()) {
					sorted_cities = Constants.getSortedCityIds(cityIds);
				}

				String categoryName = mHubCiti.getFindSingleCatName();
				String srchKey;
				srchKey = searchKey;

				String radius = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).getString
						("radious", "");
				lastVistedProductNo = lastVisitedNo + "";

				String get_find_singlecategory_retailers = Properties.url_local_server
						+ Properties.hubciti_version
						+ "find/singlecatretailersjson";

				JSONObject urlParameters = mUrlRequestParams
						.getFindSingleCategoryRetailersRequest(mItemId,
								mBottomId, categoryName, lastVistedProductNo,
								latitude, longitude, radius, srchKey, subCatId,
								sortby, filterId, filterValueId, locSpecials,
								interests, sorted_cities, CommonConstants.convertDeviceTimeToUTC
										());
				// urlParameters = URLEncoder.encode(urlParameters, "utf-8");

				JSONObject jsonObject = mServerConnections.getUrlJsonPostResponse(
						get_find_singlecategory_retailers, urlParameters);

				if (jsonObject != null) {
					try {

						try {
							responseText = jsonObject.getString("responseText");
						} catch (Exception e2) {
							responseText = "No Records Found";
						}
						// For BottomButton

						if (jsonObject.has("bottomBtnList")) {

							hasBottomBtns = true;

							try {
								ArrayList<BottomButtonBO> bottomButtonList = bb
										.parseForBottomButton(jsonObject);
								BottomButtonListSingleton
										.clearBottomButtonListSingleton();
								BottomButtonListSingleton
										.getListBottomButton(bottomButtonList);

							} catch (JSONException e1) {
								e1.printStackTrace();
							}

						} else {
							hasBottomBtns = false;
						}

						if (!"Success".equals(responseText)) {
							result = "false";
							isSearchresult = false;

							return result;
						}

						if (jsonObject.has("mainMenuId")) {
							strMainMenuId = jsonObject.getString(
									"mainMenuId");
							Constants.saveMainMenuId(strMainMenuId);
							Log.v("", "MAIN MENU ID : " + strMainMenuId);
						}


						if (jsonObject.has("retailerDetail")) {

							JSONArray retElement = new JSONArray();

							try {
								retElement
										.put(jsonObject
												.getJSONObject("retailerDetail"));
							} catch (Exception e) {
								retElement = jsonObject
										.getJSONArray("retailerDetail");
							}

							retailersList = new ArrayList<>();
							arrRetailerId = new ArrayList<>();
							arrRetailerLocId = new ArrayList<>();

							for (int i = 0; i < retElement.length(); i++) {

								HashMap<String, String> scanseeData = new HashMap<>();

								JSONObject ret = retElement
										.getJSONObject(i);

								if (ret.has(CommonConstants
										.TAG_FINDSINGLECATEGORY_GROUP_CONTENT)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDSINGLECATEGORY_GROUP_CONTENT,
													ret.getString(CommonConstants
															.TAG_FINDSINGLECATEGORY_GROUP_CONTENT));
								}

								if (ret.has(CommonConstants
										.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH)) {
									scanseeData
											.put(CommonConstants.TAG_BANNER_IMAGE_PATH,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH));
								}
								if (ret.has(CommonConstants
										.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDLOCATION_SCANSEERIBBONADURL,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEERIBBONADURL));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDLOCATION_SCANSEERETAILERID,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEERETAILERID));
//									Added this to update retailer impression
									arrRetailerId.add(ret.getString(CommonConstants
											.TAG_FINDLOCATION_SCANSEERETAILERID));
								}
								if (ret.has(CommonConstants
										.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID));
//									Added this to update retailer impression
									arrRetailerLocId.add(ret.getString(CommonConstants
											.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID));
								}
								if (ret.has(CommonConstants
										.TAG_FINDLOCATION_SCANSEERETAILERLISTID)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDLOCATION_SCANSEERETAILERLISTID,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEERETAILERLISTID));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_RETAILERNAME)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_RETAILERNAME,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_RETAILERNAME));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEEADDRESS1));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS2)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDLOCATION_SCANSEEADDRESS2,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEEADDRESS2));
								}
								if (ret.has(CommonConstants.TAG_RETAILER_OPEN_CLOSE)) {
									scanseeData
											.put(CommonConstants
															.TAG_RETAILER_OPEN_CLOSE,
													ret.getString(CommonConstants
															.TAG_RETAILER_OPEN_CLOSE));
								}
								// City
								if (ret
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEECITY)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_SCANSEECITY,
													ret
															.getString(
																	CommonConstants
																			.TAG_FINDLOCATION_SCANSEECITY));
								}

								// State
								if (ret
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEESTATE)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_SCANSEESTATE,
													ret
															.getString(
																	CommonConstants
																			.TAG_FINDLOCATION_SCANSEESTATE));
								}
								// Postal Code
								if (ret
										.has(CommonConstants.TAG_FINDLOCATION_SCANSEEPOSTALCODE)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDLOCATION_SCANSEEPOSTALCODE,
													ret
															.getString(
																	CommonConstants
																			.TAG_FINDLOCATION_SCANSEEPOSTALCODE));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEEDISTANCE));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_SCANSEELAT)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_LAT,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEELAT));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_SCANSEELON)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_LON,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEELON));
								}
								if (ret.has(CommonConstants.TAG_FINDLOCATION_RETAILERNAME)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_NAME,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_RETAILERNAME));
								}
								if (ret.has(CommonConstants
										.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS,
													ret.getString(CommonConstants
															.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS));
								}

								if (ret.has(CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEESALEFLAG));
								}
								if (ret.has(CommonConstants.TAG_FINDSINGLECATEGORY_LATITUDE)) {
									scanseeData
											.put(CommonConstants.TAG_FINDSINGLECATEGORY_LATITUDE,
													ret.getString(CommonConstants
															.TAG_FINDSINGLECATEGORY_LATITUDE));

								}
								if (ret.has(CommonConstants.TAG_FINDSINGLECATEGORY_LONGITUDE)) {
									scanseeData
											.put(CommonConstants.TAG_FINDSINGLECATEGORY_LONGITUDE,
													ret.getString(CommonConstants
															.TAG_FINDSINGLECATEGORY_LONGITUDE));
								}
								if (ret.has(CommonConstants
										.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH)) {
									scanseeData
											.put(CommonConstants
															.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH));
								}

								if (ret.has(CommonConstants.TAG_FINDLOCATION_REFERENCE)) {
									scanseeData
											.put(CommonConstants.TAG_FINDLOCATION_REFERENCE,
													ret.getString(CommonConstants
															.TAG_FINDLOCATION_REFERENCE));
								}

								retailersList.add(scanseeData);

							}
						}


						if (jsonObject.has("nextPage")) {
							if ("1".equals(jsonObject.getString("nextPage"))) {

								nextPage = true;

								if (Integer.valueOf(jsonObject
										.getString("maxRowNum")) > lastVisitedNo) {
									lastVisitedNo = Integer.valueOf(jsonObject
											.getString("maxRowNum"));
								}

							} else {
								nextPage = false;
							}
						}

						result = "true";

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result)
		{

			if ("true".equals(result)) {

//				Calling AsyncTask to update retailer impression
				new RetailerImpressionUpdateAsync(mHubCiti.getFindSingleCatName(), TextUtils.join
						(",", arrRetailerId), TextUtils.join(",", arrRetailerLocId)).execute();


				if (isSearchresult) {

					items.clear();
					isSearchresult = false;
				}

				findlocationList = new ArrayList<>();
				findlocationList = retailersList;

				if (!isViewMore && !"".equalsIgnoreCase(sortby)) {
					String sortText = "";

					if ("distance".equalsIgnoreCase(sortby)) {
						sortText = "Sorted By Distance";
					} else if ("atoz".equalsIgnoreCase(sortby)) {
						sortText = "Sorted By Name";

					}
					sortTextView.setText(sortText);
				}

				for (int i = 0; i < findlocationList.size(); i++) {

					items.add(new FindSingleCategoryEntryItem(
							findlocationList
									.get(i)
									.get(CommonConstants.TAG_FINDLOCATION_RETAILERNAME),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEEDISTANCE),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS1),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEEADDRESS2),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEECITY),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEESTATE),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEEPOSTALCODE),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEESALEFLAG),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID),
							findlocationList
									.get(i)

									.get(CommonConstants
											.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLISTID),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDSINGLECATEGORY_LATITUDE),

							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDSINGLECATEGORY_LONGITUDE),
							findlocationList
									.get(i)

									.get(CommonConstants
											.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH),
							findlocationList
									.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL),
							findlocationList.get(i)

									.get(CommonConstants.TAG_FINDLOCATION_REFERENCE),
							findlocationList.get(i)

									.get(CommonConstants.TAG_BANNER_IMAGE_PATH), findlocationList
							.get(i)
							.get(CommonConstants.TAG_RETAILER_OPEN_CLOSE)
					));
				}

				adapter = new FindSingleCategoryListAdapter(
						getApplicationContext(), items, sortby,
						findlocationList, activity);
				findlocationListView.setAdapter(adapter);
				isAlreadyLoading = false;

				if (null != lastVistedProductNo) {
					int selCount = Integer.parseInt(lastVistedProductNo) - visibleItemCount + 1;
					findlocationListView.setSelection(Integer
							.valueOf(selCount));
				}

				retailerEditBox.setText(searchKey);
				retailerEditBox
						.setSelection(retailerEditBox.getText().length());

				if (bb != null) {
					bb.setOptionsValues(getListValues(), getRetailerDetails());
				}
				try {
					if (nextPage) {

						try {
							moreResultsView.setVisibility(View.VISIBLE);
							findlocationListView
									.removeFooterView(moreResultsView);
						} catch (Exception ex) {
							ex.printStackTrace();
						}

						findlocationListView.addFooterView(moreResultsView);

					} else {
						findlocationListView.removeFooterView(moreResultsView);
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {

				try {
					mDialog.dismiss();
					ArrayList<Item> noItems = new ArrayList<>();
					adapter = new FindSingleCategoryListAdapter(
							getApplicationContext(), noItems, sortby,
							findlocationList, activity);
					findlocationListView.setAdapter(adapter);
					try {
						findlocationListView.removeFooterView(moreResultsView);
					} catch (Exception e) {
						e.printStackTrace();
					}
					new AlertDialog.Builder(FindSingleCategory.this)
							.setMessage(
									Html.fromHtml(responseText.replace("\\n",
											"<br/>")))
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener()
									{
										@Override
										public void onClick(DialogInterface dialog,
															int which)
										{
											dialog.dismiss();
											// finishAction();
										}
									}).show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			// Added to disable toggle click
			CommonConstants.disableClick = false;

			try {
				if (hasBottomBtns && enableBottomButton) {

					bb.createbottomButtontTab(linearLayout, false);
				}
				linearLayout.setVisibility(View.VISIBLE);

				if (mDialog.isShowing()) {
					mDialog.dismiss();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onClick(View v)
	{

		switch (v.getId()) {

			case R.id.find_retailer_cancel:
				findRetailerCancel();
				break;
			case R.id.find_retailer_edit_text:
				findRetailerEditText();
				break;
			case R.id.viewMore:
				enableBottomButton = false;
				isViewMore = true;
				callFindAsyncTask();
				break;
			default:
				break;
		}
	}

	private ArrayList<RetailerBo> getRetailerDetails()
	{
		ArrayList<RetailerBo> mRetailerList = new ArrayList<>();

		for (int i = 0; i < findlocationList.size(); i++) {

			String retailerName = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_RETAILERNAME);
			String retailAddress = findlocationList.get(i).get(
					CommonConstants.TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS);
			String latitude = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_LAT);
			String longitude = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_LON);
			String retailId = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERID);
			String ribbonAdImagePath = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH);
			String ribbonAdURL = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERIBBONADURL);
			String locationID = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID);
			String bannerAd = findlocationList.get(i).get(
					CommonConstants.TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH);
			RetailerBo retailerObject = new RetailerBo(null, retailId,
					retailerName, locationID, null, retailAddress, null, null,
					bannerAd, ribbonAdImagePath, ribbonAdURL, null, latitude,
					longitude, null);
			mRetailerList.add(retailerObject);

		}

		return mRetailerList;

	}

	private HashMap<String, String> getListValues()
	{
		HashMap<String, String> values = new HashMap<>();

		values.put("Class", "FindSingleCat");

		values.put("sortChoice", sortby);

		values.put("mLinkId", mLinkId);

		values.put("catName", catName);

		values.put("catId", mLinkId);

		values.put("mItemId", mItemId);

		values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mBottomId);

		values.put("latitude", latitude);

		values.put("longitude", longitude);

		values.put("srchKey", searchKey);
		if (subCatId != null) {
			values.put("subCatId", subCatId);
		}

		return values;

	}

	private void findRetailerEditText()
	{
		hideKeyboardItem();
		if (retailerEditBox.getText().toString() != null
				&& !"".equals(retailerEditBox.getText().toString())) {
			rightImage.setEnabled(true);
			searchKey = retailerEditBox.getText().toString();
			lastVisitedNo = 0;
			isSearchresult = true;
			enableBottomButton = false;

			refresh();
		}
	}

	private void findRetailerCancel()
	{
		if (retailerEditBox.getText().toString() != null) {
			retailerEditBox.setText("");
			hideKeyboardItem();
		}

	}

	public boolean isJSONArray(JSONObject jsonObject, String value)
	{
		boolean isArray = false;

		JSONObject isJSONObject = jsonObject.optJSONObject(value);
		if (isJSONObject == null) {
			JSONArray isJSONArray = jsonObject.optJSONArray(value);
			if (isJSONArray != null) {
				isArray = true;
			}
		}
		return isArray;
	}

	@SuppressWarnings("deprecation")
	public void getGPSValues()
	{
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		String provider = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (provider.contains("gps")) {

			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (locNew != null) {

				latitude = String.valueOf(locNew.getLatitude());
				longitude = String.valueOf(locNew.getLongitude());

			} else {
				// N/W Tower Info Start
				locNew = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (locNew != null) {
					latitude = String.valueOf(locNew.getLatitude());
					longitude = String.valueOf(locNew.getLongitude());
				} else {
					latitude = CommonConstants.LATITUDE;
					longitude = CommonConstants.LONGITUDE;
				}

			}
		}
		accZipcode = Constants.getZipCode();
	}

	void callFindAsyncTask()
	{

		if (isFirst) {
			findLocCat = new FindLocationCategory();
			findLocCat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;
		}

		if (findLocCat != null) {
			if (!findLocCat.isCancelled()) {
				findLocCat.cancel(true);
			}
			findLocCat = null;
		}

		findLocCat = new FindLocationCategory();
		findLocCat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}

	private void cancelFindAsyn()
	{
		if (findLocCat != null && !findLocCat.isCancelled()) {
			findLocCat.cancel(true);
		}
		findLocCat = null;

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);

		}

		if (resultCode == 30001 || resultCode == 2) {
			isRefresh = false;

		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private void finishAction()
	{
		cancelFindAsyn();
		finish();
		mHubCiti.clearSavedValues();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		if (CommonConstants.hamburgerIsFirst) {
			callSideMenuApi(customNaviagation);
			CommonConstants.hamburgerIsFirst = false;
		}
		// Add screen name when needed
		if (bb != null) {
			bb.setActivityInfo(activity, "FindSingleCategory-");
			bb.setOptionsValues(getListValues(), getRetailerDetails());
		}

		if (mHubCiti.isCancelled()) {
			return;
		}

		if (!isRefresh) {
			if (!isItemClicked) {
				items.clear();

				moreResultsView.setVisibility(View.GONE);
				findlocationList = new ArrayList<>();

				adapter = new FindSingleCategoryListAdapter(
						getApplicationContext(), items, sortby,
						findlocationList, activity);
				findlocationListView.setAdapter(adapter);
				isViewMore = false;
				getSortItemName();
				refresh();
			}
		}

		isRefresh = false;
		isItemClicked = false;
	}

	private void refresh()
	{
		items.clear();
		findlocationList.clear();
		retailersList.clear();
		// Add screen name when needed
		lastVisitedNo = 0;
		enableBottomButton = false;
		/***
		 * Calling the progress dialog for the first time only After that it
		 * will show the bottom bar of list as loader
		 ***/
		mDialog = ProgressDialog.show(FindSingleCategory.this, "",
				Constants.DIALOG_MESSAGE, true);
		mDialog.show();
		mDialog.setCancelable(false);
		callFindAsyncTask();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		isRefresh = true;
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		finishAction();
	}

}
