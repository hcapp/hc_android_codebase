package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CustomImageLoader;

public class EventCategoryListAdapter extends BaseAdapter {
	private Activity activity;
	private ArrayList<HashMap<String, String>> eventCatList;
	private static LayoutInflater inflater = null;
	protected CustomImageLoader customImageLoader;

	public EventCategoryListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> findList) {
		this.activity = activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.eventCatList = findList;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return eventCatList.size();
	}

	@Override
	public Object getItem(int id) {
		return eventCatList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		if (convertView == null) {

			view = inflater.inflate(R.layout.listitem_find_category, parent,false);
			viewHolder = new ViewHolder();
			viewHolder.name = (TextView) view
					.findViewById(R.id.find_category_name);
			viewHolder.imagePath = (ImageView) view
					.findViewById(R.id.find_category_image);

			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();

		}
		viewHolder.name.setText(eventCatList.get(position).get("catName"));
		viewHolder.imagePath.setTag(eventCatList.get(position)
				.get("catImgPath"));
		customImageLoader.displayImage(eventCatList.get(position).get("catImgPath"),
				activity, viewHolder.imagePath);

		return view;
	}

	public static class ViewHolder {
		protected TextView name;
		protected ImageView imagePath;

	}
}
