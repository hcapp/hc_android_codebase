package com.hubcity.android.screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;


import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by subramanya.v on 3/4/2016.
 */
public class TwoTileTemplate extends MenuPropertiesActivity {

    LinearLayout parentView;
    private int isDisplayLabel;
    private ImageView mBannerImageView;
    private ProgressBar progressBar;
    private RelativeLayout mBannerHolder;
    private ScrollView scrollView;
    private int mScreenHeight;
    private LinearLayout scrollViewChild;
    private String btnLblColor, tempBkgrdImg;
    private View menuListTemplate;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = TwoTileTemplate.this;
        activity = TwoTileTemplate.this;
        CommonConstants.hamburgerIsFirst = true;
        try {
            Intent intent = getIntent();
            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);

            RelativeLayout Parent = new RelativeLayout(this);
            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            Parent.setLayoutParams(param);

            LinearLayout ParentIntermideate = new LinearLayout(this);
            ParentIntermideate.setOrientation(LinearLayout.VERTICAL);
            handleIntent(intent);
            bindView();
            setVisibilty();


            LinearLayout.LayoutParams gridViewParentParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            BottomButtonListSingleton mBottomButtonListSingleton;
            mBottomButtonListSingleton = BottomButtonListSingleton
                    .getListBottomButton();

            if (mBottomButtonListSingleton != null) {

                ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                        .listBottomButton;

                if (listBottomButton != null && listBottomButton.size() > 1) {
                    int px = (int) (50 * this.getResources().getDisplayMetrics().density + 0.5f);
                    gridViewParentParam.setMargins(0, 0, 0, px);
                }
            }

            parentView.setLayoutParams(gridViewParentParam);
            LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            ParentIntermideate.setLayoutParams(parentParam);
            addTitleBar(ParentIntermideate, Parent);
            ParentIntermideate.addView(parentView);
            Parent.addView(ParentIntermideate);

            createbottomButtontTab(Parent);
            setContentView(Parent);

            ViewTreeObserver observer = parentView.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {


                    mScreenHeight = parentView.getHeight() - mBannerHolder.getHeight();
                    mScreenHeight = mScreenHeight - scrollViewChild.getPaddingBottom() -
                            scrollViewChild.getPaddingBottom() + 1;
                    ViewTreeObserver obs = scrollView.getViewTreeObserver();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        //noinspection deprecation
                        obs.removeGlobalOnLayoutListener(this);
                    }
                    createMenuScreens(mainMenuUnsortedList);
                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void bindView() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);

        menuListTemplate = layoutInflater.inflate(R.layout.two_image_tile,
                null, false);
        // Initializing drawer layout
        drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);
        mBannerImageView = (ImageView) menuListTemplate
                .findViewById(R.id.top_image);
        scrollView = (ScrollView) menuListTemplate.findViewById(R.id.scrollView);
        progressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
        mBannerHolder = (RelativeLayout) menuListTemplate
                .findViewById(R.id.banner_holder);
        bannerParent = (LinearLayout) menuListTemplate.findViewById(R.id.bannerParent);
        parentView = (LinearLayout) menuListTemplate.findViewById(R.id.parentView);
        scrollViewChild = (LinearLayout) menuListTemplate.findViewById(R.id.scrollViewChild);
        if (tempBkgrdImg != null && !tempBkgrdImg.equals("")) {
            new ImageLoaderAsync(parentView).execute(tempBkgrdImg);
        } else {
            if ("1".equals(previousMenuLevel)) {
                if ((mBkgrdColor != null) && !(mBkgrdColor.equals("")) && !(mBkgrdColor.equals
                        ("N/A"))) {
                    parentView.setBackgroundColor(Color.parseColor(mBkgrdColor));
                } else {
                    new ImageLoaderAsync(parentView).execute(mBkgrdImage);
                }

            } else {
                if ((smBkgrdColor != null) && !(smBkgrdColor.equals("")) && !(smBkgrdColor
                        .equals("N/A"))) {
                    parentView.setBackgroundColor(Color.parseColor(smBkgrdColor));
                } else {
                    new ImageLoaderAsync(parentView).execute(smBkgrdImage);
                }
            }
        }
    }

    @Override
    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);

            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            smBkgrdImage = intent.getExtras().getString("smBkgrdImage");
            departFlag = intent.getExtras().getString("departFlag");
            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
            mFontColor = intent.getExtras().getString("mFontColor");
            smFontColor = intent.getExtras().getString("smFontColor");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");
            if (intent.hasExtra("isDisplayLabel")) {
                isDisplayLabel = intent.getExtras().getInt("isDisplayLabel");
            }
            if (intent.hasExtra("tempBkgrdImg")) {
                tempBkgrdImg = intent.getExtras().getString("tempBkgrdImg");
            }
            if (intent.hasExtra("btnLblColor")) {
                btnLblColor = intent.getExtras().getString("btnLblColor");
            }

        }

        super.handleIntent(intent);

    }

    class ImageLoaderAsync extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(TwoTileTemplate.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {


                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {
            try {
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                BitmapDrawable background = new BitmapDrawable(bmImg);
                int sdk = Build.VERSION.SDK_INT;
                if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
                    bmImage.setBackgroundDrawable(background);
                } else {
                    bmImage.setBackgroundDrawable(background);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    void createMenuScreens(ArrayList<MainMenuBO> mainMenuUnsortedList) {
        int layHeight = mScreenHeight / 2;
        int totalSize = mainMenuUnsortedList.size();
        int itemCount = 0;
        for (int i = 0; i < totalSize; i++) {
            MainMenuBO buttonMainMenu = mainMenuUnsortedList
                    .get(itemCount);
            FrameLayout rowLayout = createRowLayout(layHeight);
            FrameLayout imageView = createImageView(rowLayout, itemCount, buttonMainMenu);

            if (isDisplayLabel == 1) {
                FrameLayout textView = createTextView(imageView, buttonMainMenu, layHeight);
                scrollViewChild.addView(textView);
            } else {
                scrollViewChild.addView(imageView);
            }
            itemCount++;

        }

    }

    private FrameLayout createTextView(FrameLayout imageView, MainMenuBO buttonMainMenu, int
            layHeight) {
        int textViewHeight = layHeight - (2 * scrollViewChild.getPaddingBottom());
        TextView text = new TextView(this);
        FrameLayout.LayoutParams textParam = new FrameLayout.LayoutParams(ViewGroup.LayoutParams
                .MATCH_PARENT, (textViewHeight / 4));
        textParam.gravity = Gravity.BOTTOM;
        text.setLayoutParams(textParam);
        text.setTextColor(Color.parseColor(btnLblColor));
        text.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        text.setTypeface(Typeface.DEFAULT_BOLD);
        text.setText(buttonMainMenu.getmItemName());

        int textSize = (int) 25f;
        text.setTextSize(textSize);
        imageView.addView(text);
        return imageView;
    }

    private FrameLayout createImageView(FrameLayout rowLayout, int itemCount, MainMenuBO
            buttonMainMenu) {
        ImageView image = new ImageView(this);
        FrameLayout.LayoutParams imageParam = new FrameLayout.LayoutParams(ViewGroup.LayoutParams
                .MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        image.setLayoutParams(imageParam);
        image.setAdjustViewBounds(true);
        image.setTag(itemCount);
        image.setOnClickListener(this);
        String mItemImgUrl = buttonMainMenu.getmItemImgUrl();

        image.setScaleType(ImageView.ScaleType.FIT_XY);
        rowLayout.addView(image);

        final ProgressBar menuProgressBar = new ProgressBar(this, null, android.R.attr
                .progressBarStyleInverse);
        FrameLayout.LayoutParams progressParam = new FrameLayout.LayoutParams(ViewGroup
                .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        progressParam.gravity = Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL;
        menuProgressBar.setLayoutParams(progressParam);
        rowLayout.addView(menuProgressBar);


        Transformation transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(30)
                .oval(false)
                .build();

        mItemImgUrl = mItemImgUrl.replaceAll(" ", "%20");
        Picasso.with(TwoTileTemplate.this).load(mItemImgUrl).fit().transform(transformation).into
                (image, new Callback() {
                    @Override
                    public void onSuccess() {
                        menuProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        menuProgressBar.setVisibility(View.GONE);
                    }
                });
        return rowLayout;

    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);
        mMainMenuBO.setmBkgrdColor(mBkgrdColor);
        mMainMenuBO.setmBkgrdImage(mBkgrdImage);
        mMainMenuBO.setSmBkgrdColor(smBkgrdColor);
        linkClickListener(mMainMenuBO.getLinkTypeName(), mMainMenuBO);
    }

    private FrameLayout createRowLayout(int layHeight) {
        FrameLayout rowLay = new FrameLayout(this);
        FrameLayout.LayoutParams rowLayParam = new FrameLayout.LayoutParams(ViewGroup.LayoutParams
                .MATCH_PARENT, layHeight);
        rowLay.setLayoutParams(rowLayParam);
        rowLay.setPadding(0, scrollViewChild.getPaddingBottom(), 0, scrollViewChild
                .getPaddingBottom());
        return rowLay;
    }

    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activity = this;
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }

    private void setVisibilty() {
        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if (null != mBannerImg) {
                mBannerImageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(TwoTileTemplate.this).load(mBannerImg).into(mBannerImageView, new
                        Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        } else {
            customHorizontalTicker = (CustomHorizontalTicker) menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker) menuListTemplate.findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher) menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext, customHorizontalTicker, customVerticalTicker, bannerParent, customRotate, mBannerHolder);
        }

    }


}
