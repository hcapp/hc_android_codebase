package com.hubcity.android.screens;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

import com.scansee.hubregion.R;

/**
 * Created by supriya.m on 3/1/2016.
 */
public class TestTemplateActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_updown_four_image_template);
	}

}
