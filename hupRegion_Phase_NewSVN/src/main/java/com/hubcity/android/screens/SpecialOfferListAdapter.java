package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.CommonConstants;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SpecialOfferListAdapter extends BaseAdapter {
	private ArrayList<HashMap<String, String>> specialoffersList;
	private static LayoutInflater inflater = null;
	public CustomImageLoader customImageLoader;
	Activity activity;

	public SpecialOfferListAdapter(Activity activity,
			ArrayList<HashMap<String, String>> specialoffersList) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.specialoffersList = specialoffersList;
		this.activity = activity;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return specialoffersList.size();

	}

	@Override
	public Object getItem(int id) {
		return specialoffersList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		viewHolder = new ViewHolder();
		String itemName = specialoffersList.get(position).get("itemName");
		if ("currentSalesData".equalsIgnoreCase(itemName)) {

			view = inflater.inflate(R.layout.listitem_currentsales, parent,false);

			viewHolder.currentsalesPname = (TextView) view
					.findViewById(R.id.retailer_sales_name);
			viewHolder.currentsalesRegularprice = (TextView) view
					.findViewById(R.id.retailer_sales_price_1);
			viewHolder.currentsalesSaleprice = (TextView) view
					.findViewById(R.id.retailer_sales_price_2);
			viewHolder.currentsalesImage = (ImageView) view
					.findViewById(R.id.retailer_sales_image);

		} else if ("specialOffersData".equalsIgnoreCase(itemName)) {

			view = inflater.inflate(R.layout.listitem_specialoffer, parent,false);
			viewHolder.specialofferstext = (TextView) view
					.findViewById(R.id.retailer_specials_offer_name);
		}
		view.setTag(viewHolder);
		if ("currentSalesData".equalsIgnoreCase(itemName)) {
			viewHolder.currentsalesPname.setText(specialoffersList
					.get(position).get(
							CommonConstants.TAG_CURRENTSALES_PRODUCTNAME));
			viewHolder.currentsalesRegularprice.setText(specialoffersList.get(
					position)
					.get(CommonConstants.TAG_CURRENTSALES_REGULARPRICE));
			viewHolder.currentsalesSaleprice.setText(specialoffersList.get(
					position).get(CommonConstants.TAG_CURRENTSALES_SALEPRICE));

			viewHolder.currentsalesImage.setTag(specialoffersList.get(position)
					.get(CommonConstants.TAG_CURRENTSALES_IMAGEPATH));
			customImageLoader.displayImage(
					specialoffersList.get(position).get(
							CommonConstants.TAG_CURRENTSALES_IMAGEPATH),
					activity, viewHolder.currentsalesImage);

		} else if ("specialOffersData".equalsIgnoreCase(itemName)) {
			viewHolder.specialofferstext.setText(specialoffersList
					.get(position).get(
							CommonConstants.TAG_SPECIALOFFER_PAGETITLE));

		}
		return view;
	}

	public static class ViewHolder {
		public ImageView currentsalesImage;
		public TextView currentsalesSaleprice;
		public TextView currentsalesRegularprice;
		public TextView currentsalesPname;
		public TextView specialofferDummydata;
		public TextView specialofferstext;
		public TextView googleDummydata;
	}
}
