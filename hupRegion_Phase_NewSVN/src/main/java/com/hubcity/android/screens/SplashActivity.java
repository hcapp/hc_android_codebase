package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Window;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.PrefManager;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.UrlRequestParams;


import com.newrelic.agent.android.NewRelic;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


public class SplashActivity extends Activity {

    private Properties objProperties;
    private PrefManager objPrefManager;
    ArrayList<String> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash_screen);
        objPrefManager = new PrefManager(this);
        objProperties = new Properties();
        objProperties.setProperties(getResources().getString(R.string.hubcity_key),
                Boolean.parseBoolean(getResources().getString(R.string.is_region_app)),
                getResources().getString(R.string.twitter_consumer_key),
                getResources().getString(R.string.twitter_consumer_secret));


        //uncomment for production build
        if (objProperties.getIsNewRelicOn()) {
            NewRelic.withApplicationToken(getResources().getString(R.string.newrelic_key)).start(
                    this.getApplication());
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().get("body") != null) {
            String message = String.valueOf(getIntent().getExtras().get("body"));
            arrayList = Constants.saveNotificationMessage(message);
            LoginScreenViewAsyncTask mLoginScreenViewAsyncTask = new LoginScreenViewAsyncTask();
            mLoginScreenViewAsyncTask.execute(" ");
            handleNotification(message);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (objPrefManager.isFirstTimeLaunch()) {
                        launchSliderScreen();
                    } else {
                        new LoginScreenViewAsyncTask(SplashActivity.this).executeOnExecutor(AsyncTask
                                .THREAD_POOL_EXECUTOR, " ");
                    }

                }
            }, Constants.SPLASH_TIME_OUT);
        }

    }

    private void launchSliderScreen() {
        startActivity(new Intent(SplashActivity.this, SliderActivity.class));
        finish();
    }
    private void handleNotification(String message) {
        Intent intent = null;
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(message);

            boolean loginScreenStatus = Constants.bISLoginStatus;
            if (!UrlRequestParams.getUid().equals("-1")
                    && (loginScreenStatus || Constants.isCredentialsSaved(this))) {
                if (Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                    //This is to check whether the user is in Login screen or not, so that we can
                    // show proper alert according to that
                    ActivityManager am = (ActivityManager)
                            getSystemService(Activity.ACTIVITY_SERVICE);
                    String className = am.getRunningTasks(1).get(0).topActivity.getClassName();
                    Constants.showLoginMsg = className.contains("LoginScreen");
                    intent = new Intent(this, AlertDialogActivity.class);
                } else {
                    if (Properties.HUBCITI_KEY.equals("Tyler19")||Properties.HUBCITI_KEY.equals("Tyler Test75")||Properties.HUBCITI_KEY.equals("shrini2113")
                            || Properties.HUBCITI_KEY.equals("HEB Test2156") || Properties.HUBCITI_KEY.equals("HEB Test54")) {
                        if (arrayList.size() > 1) {
                            intent = new Intent(this, PushNotificationListScreen.class);
                        } else {
                            if (jsonObject.has("rssFeedList") && jsonObject.has("dealList")) {
                                intent = new Intent(this, PushNotificationListScreen.class);
                            } else if (jsonObject.has("rssFeedList")) {
                                String newsLink = jsonObject.getJSONArray("rssFeedList")
                                        .getJSONObject(0)

                                        .optString("link");
                                intent = new Intent(
                                        this,
                                        ScanseeBrowserActivity.class);
                                intent.putExtra(CommonConstants.URL, newsLink);
                            } else {
                                intent = Constants.getNotificationDealIntent(jsonObject, this);
                            }
                        }
                    } else {
//						If array size more than 1 it will navigate to list screen else it will
//						navigate to respective details screen
//						If the size is 0 it will go to list screen and show a
//                      pop up to user that deal has expired
                        if (arrayList.size() > 1) {
                            intent = new Intent(this, PushNotificationListScreen.class);
                        } else if (arrayList.size() > 0) {
                            intent = Constants.getNotificationDealIntent(jsonObject, this);
                        } else {
                            intent = new Intent(this, PushNotificationListScreen.class);
                        }
                    }
                }
            } else {
                intent = new Intent(this, SplashActivity.class);
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(Constants.PUSH_NOTIFY_MSG, message);
            startActivity(intent);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
