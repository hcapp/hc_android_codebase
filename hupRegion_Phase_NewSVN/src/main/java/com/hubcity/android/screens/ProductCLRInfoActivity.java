package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.CouponsDetailActivity;
import com.scansee.android.CouponsProductListAdapter2;
import com.scansee.android.ListItemGestures;
import com.scansee.android.ListItemGesturesCouponsInfo;
import com.scansee.android.SlideAnimation;
import com.scansee.hubregion.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ProductCLRInfoActivity extends CustomTitleBar {
	SharedPreferences settings = null;
	String userId, prodId, couponId, prodName, type;
	public ArrayList<HashMap<String, String>> clrList = null;
	public ListView couponsproductListView;
	public CouponsProductListAdapter2 couponsproductListAdapter2;
	public boolean isDelete = false;
	int lastVisited = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		title.setSingleLine(false);
		title.setMaxLines(1);
		title.setText("Coupon");

		leftTitleImage.setVisibility(View.GONE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setContentView(R.layout.discount_listview);
		SharedPreferences settings = getSharedPreferences(
				CommonConstants.PREFERANCE_FILE, 0);
		userId = settings.getString(CommonConstants.USER_ID, "0");
		couponsproductListView = (ListView) findViewById(R.id.coupons_list);
		final ListItemGesturesCouponsInfo listItemGestures = new ListItemGesturesCouponsInfo(
				this);
		prodId = getIntent().getExtras().getString(
				CommonConstants.TAG_CURRENTSALES_PRODUCTID);
		if (getIntent().hasExtra(CommonConstants.TAG_RETAILER_NAME)) {
			prodName = getIntent().getExtras().getString(
					CommonConstants.TAG_RETAILER_NAME);
		}
		couponsproductListView.setOnTouchListener(listItemGestures);
		couponsproductListView
				.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						HashMap<String, String> selectedData = clrList
								.get(position);
						if (isDelete) {
							isDelete = false;
							view.findViewById(R.id.shoppinglist_delete)
									.setAnimation(
											SlideAnimation
													.outToRightAnimation());
							view.findViewById(R.id.shoppinglist_delete)
									.setVisibility(View.GONE);
							Parcelable state = couponsproductListView
									.onSaveInstanceState();
							couponsproductListAdapter2 = new CouponsProductListAdapter2(
									ProductCLRInfoActivity.this, clrList);
							couponsproductListView
									.setAdapter(couponsproductListAdapter2);
							couponsproductListView
									.onRestoreInstanceState(state);

							return;
						} else {
							if (listItemGestures.swipeDetected()) {
								if (listItemGestures.getAction().equals(
										ListItemGestures.Action.REMOVE)) {
									return;
								}
								if ((listItemGestures.getAction().equals(
										ListItemGesturesCouponsInfo.Action.LR) || listItemGestures
										.getAction()
										.equals(ListItemGesturesCouponsInfo.Action.RL))
										&& "Green".equalsIgnoreCase(selectedData
												.get(CommonConstants.WISHLISTCOUPONUSAGE))) {
									isDelete = true;
									view.findViewById(R.id.shoppinglist_delete)
											.setAnimation(
													SlideAnimation
															.inFromRightAnimation());
									view.findViewById(R.id.shoppinglist_delete)
											.setVisibility(View.VISIBLE);

								}
							} else {
								Intent navIntent = new Intent(
										ProductCLRInfoActivity.this,
										CouponsDetailActivity.class);
								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSCOUPONNAME,
												selectedData
														.get(CommonConstants.ALLCOUPONSCOUPONNAME));

								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSCOUPONID,
												selectedData
														.get(CommonConstants.ALLCOUPONSCOUPONID));
								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSDESCRIPTION,
												selectedData
														.get(CommonConstants.ALLCOUPONSDESCRIPTION));
								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSIMAGEPATH,
												selectedData
														.get(CommonConstants.ALLCOUPONSIMAGEPATH));
								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSSTARTDATE,
												selectedData
														.get(CommonConstants.ALLCOUPONSSTARTDATE));
								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSEXPIRATIONDATE,
												selectedData
														.get(CommonConstants.ALLCOUPONSEXPIRATIONDATE));

								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSUSED,
												selectedData
														.get(CommonConstants.ALLCOUPONSUSED));
								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSVIEWABLE,
												selectedData
														.get(CommonConstants.ALLCOUPONSVIEWABLE));
								navIntent
										.putExtra(
												CommonConstants.ALLCOUPONSURL,
												selectedData
														.get(CommonConstants.ALLCOUPONSURL));
								if ("Green".equalsIgnoreCase(selectedData
										.get(CommonConstants.WISHLISTCOUPONUSAGE))) {
									navIntent.putExtra("type",
											CommonConstants.COUPON_TYPE_USED);
									navIntent.putExtra(
											CommonConstants.ALLCOUPONSFAVFLAG,
											"true");
								} else {
									navIntent.putExtra("type",
											CommonConstants.COUPON_TYPE_ALL);
									navIntent.putExtra(
											CommonConstants.ALLCOUPONSFAVFLAG,
											"false");
								}

								startActivityForResult(navIntent, 1250);

							}

						}
					}
				});
		new GetCLRInfo().execute();

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Constants.FINISHVALUE) {
			this.setResult(Constants.FINISHVALUE);
			finish();
		}
	}

	UrlRequestParams mUrlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	public class GetCLRInfo extends AsyncTask<String, Void, Boolean> {
		ProgressDialog dialog;
		JSONObject jsonObject = null, couponObj = null;
		JSONArray couponArray = null;
		HashMap<String, String> tempData = null;

		@Override
		protected void onPreExecute() {
			dialog = ProgressDialog.show(ProductCLRInfoActivity.this, "",
					"Fetching CouponsDetail ", true);
			dialog.setCancelable(false);
		}

		@Override
		protected Boolean doInBackground(String... arg0) {
			boolean result = false;
			try {
				String urlParameters = mUrlRequestParams.getCoupDiscounts(
						prodId, null, String.valueOf(lastVisited));
				String get_product_clrinfo = Properties.url_local_server
						+ Properties.hubciti_version
						+ "find/fetchproductclrinfo";
				jsonObject = mServerConnections.getUrlPostResponse(
						get_product_clrinfo, urlParameters, true);
				if (jsonObject != null) {
					clrList = new ArrayList<>();
					jsonObject = jsonObject.getJSONObject("CLRDetails");
					if ("true".equalsIgnoreCase(jsonObject
							.getString("isCouponthere"))) {
						couponArray = new JSONArray();
						try {
							couponObj = jsonObject.getJSONObject(
									"couponDetails").getJSONObject(
									"CouponDetails");

							couponArray.put(couponObj);
						} catch (Exception e) {
							e.printStackTrace();
							couponArray = jsonObject.getJSONObject(
									"couponDetails").getJSONArray(
									"CouponDetails");
						}

						for (int arrayCount = 0; arrayCount < couponArray
								.length(); arrayCount++) {
							tempData = new HashMap<>();
							couponObj = couponArray.getJSONObject(arrayCount);
							tempData.put(
									CommonConstants.ALLCOUPONSCOUPONID,
									couponObj
											.getString(CommonConstants.ALLCOUPONSCOUPONID));
							tempData.put(
									CommonConstants.ALLCOUPONSCOUPONNAME,
									couponObj
											.getString(CommonConstants.ALLCOUPONSCOUPONNAME));
							tempData.put(
									CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT,
									couponObj
											.getString(CommonConstants.ALLCOUPONSCOUPONDISCOUNTAMOUNT));
							tempData.put(
									CommonConstants.ALLCOUPONSEXPIRATIONDATE,
									couponObj
											.getString(CommonConstants.ALLCOUPONSEXPIRATIONDATE));
							tempData.put(
									CommonConstants.WISHLISTCOUPONUSAGE,
									couponObj
											.getString(CommonConstants.WISHLISTCOUPONUSAGE));
							tempData.put(CommonConstants.ALLCOUPONSIMAGEPATH,
									couponObj.getString("imagePath"));
							tempData.put(
									CommonConstants.ALLCOUPONSSHORTDESCRIPTION,
									couponObj
											.getString(CommonConstants.ALLCOUPONSSHORTDESCRIPTION));
							tempData.put(CommonConstants.ALLCOUPONSDESCRIPTION,
									couponObj
											.getString("couponLongDescription"));

							tempData.put(
									CommonConstants.ALLCOUPONSVIEWABLE,
									couponObj
											.getString(CommonConstants.ALLCOUPONSVIEWABLE));
							tempData.put(
									CommonConstants.ALLCOUPONSURL,
									couponObj
											.getString(CommonConstants.ALLCOUPONSURL));
							tempData.put(
									CommonConstants.ALLCOUPONSEXPIRATIONDATE,
									couponObj
											.getString(CommonConstants.ALLCOUPONSEXPIRATIONDATE));
							tempData.put(
									CommonConstants.ALLCOUPONSSTARTDATE,
									couponObj
											.getString(CommonConstants.ALLCOUPONSSTARTDATE));
							tempData.put(
									CommonConstants.WISHLISTHISTORYPRODUCTNAME,
									couponObj
											.getString(CommonConstants.WISHLISTHISTORYPRODUCTNAME));

							clrList.add(tempData);
						}
						result = true;
					}

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			dialog.dismiss();
			if (result) {
				couponsproductListAdapter2 = new CouponsProductListAdapter2(
						ProductCLRInfoActivity.this, clrList);
				couponsproductListView.setAdapter(couponsproductListAdapter2);
			} else {
				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						ProductCLRInfoActivity.this);
				notificationAlert.setMessage(getString(R.string.norecord))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				notificationAlert.create().show();
			}
		}

	}

	public void redeemCoupon(String couponId) {
		this.couponId = couponId;
		new CouponRedeem().execute();

	}

	public class CouponRedeem extends AsyncTask<String, Void, String> {
		JSONObject jsonObject = null;
		private ProgressDialog mDialog;
		HashMap<String, String> couponsdetails;

		UrlRequestParams objUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(ProductCLRInfoActivity.this, "",
					Constants.DIALOG_MESSAGE);
			mDialog.setCancelable(true);
		}

		@Override
		protected String doInBackground(String... params) {

			String result = "false";
			try {
				String urlParameters = objUrlRequestParams
						.getRedeemCoupon(couponId);
				String url_redeem_coupon = Properties.url_local_server
						+ Properties.hubciti_version
						+ "gallery/userredeemcoupon";
				jsonObject = mServerConnections.getUrlPostResponse(
						url_redeem_coupon, urlParameters, true);

				if (jsonObject != null) {
					jsonObject = jsonObject
							.getJSONObject(CommonConstants.TAG_COUPONREDEEM_RESULTSET);

					couponsdetails = new HashMap<>();
					couponsdetails
							.put(CommonConstants.TAG_COUPONREDEEM_RESPONSE_CODE,
									String.valueOf(jsonObject
											.getInt(CommonConstants.TAG_COUPONREDEEM_RESPONSE_CODE)));
					couponsdetails
							.put(CommonConstants.TAG_COUPONREDEEM_RESPONSETEXT,
									jsonObject
											.getString(CommonConstants.TAG_COUPONREDEEM_RESPONSETEXT));

					result = "true";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			mDialog.dismiss();

			if ("true".equalsIgnoreCase(result)
					&& "10000"
							.equalsIgnoreCase(couponsdetails
									.get(CommonConstants.TAG_COUPONREDEEM_RESPONSE_CODE))) {
				if (clrList != null) {
					couponsproductListAdapter2 = new CouponsProductListAdapter2(
							ProductCLRInfoActivity.this, clrList);
					couponsproductListView
							.setAdapter(couponsproductListAdapter2);
				}
				AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
						ProductCLRInfoActivity.this);
				notificationAlert
						.setMessage(
								couponsdetails
										.get(CommonConstants.TAG_COUPONREDEEM_RESPONSETEXT))
						.setPositiveButton(getString(R.string.specials_ok),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				notificationAlert.create().show();
			}
		}

	}

}
