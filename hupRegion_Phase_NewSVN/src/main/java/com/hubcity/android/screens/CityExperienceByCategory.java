package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.FindCategoryListAdapter;
import com.scansee.hubregion.R;

public class CityExperienceByCategory extends CustomTitleBar implements
		OnClickListener {

	private ArrayList<HashMap<String, String>> categoryList = null;
	private static final String CATEGORY_INFO = "CategoryInfo";
	public static final String TAG_FINDCATEGORY_NAME = "categoryName";
	public static final String TAG_CATEGORY_NAME = "CatDisName";
	public static final String TAG_FINDCATEGORY_NAME_ID = "categoryId";
	public static final String TAG_FINDCATEGORYIMAGE_PATH = "catImg";
	public static final String TAG_CATEGORYIMAGE_PATH = "CatImgPth";
	public static final String TAG_CATEGORY = "Category";
	public static final String CITYEXP_IMG_URL = "cityExpImgUlr";
	public static final String FILTERCOUNT_EXTRA = "retAffCount";
	public static final String FILTERID_EXTRA = "retAffId";
	public static final String TAG_RESPONE = "response";
	public static final String BY_CAT_EXP_FILTER_EXTRA = "filtertype";
	public static final String BY_FILTER = "filter";
	public static final String BY_EXPERIENCE = "experience";
	public static final String CITYEXP_ON_IMG_URL = "cityExpImgOnUlr";
	protected String mItemId;
	FindCategoryListAdapter catListAdapter;
	String citiExpId;
	ListView categorylistView;
	ImageView expImage;
	protected CustomImageLoader customImageLoader;
	String cityExpImgUlr;
	EditText retailerSearchText;
	Button cancel;
	protected String retAffCount, retAffId, filterName;
	public static final String TAG_FILTERID = "retAffId";
	LinearLayout filterBtmNavLayout, accBtmNavLayout;
	String byCatType, imagePathOn;
	InputMethodManager mgr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setText("Category");
			leftTitleImage.setVisibility(View.GONE);
			setContentView(R.layout.city_exp_bycategory);
			categorylistView = (ListView) findViewById(R.id.cityexp_listview_cat);
			citiExpId = getIntent().getExtras().getString(
                    Constants.CITI_EXPID_INTENT_EXTRA);
			cityExpImgUlr = getIntent().getExtras().getString(CITYEXP_IMG_URL);
			imagePathOn = getIntent().getExtras().getString(CITYEXP_ON_IMG_URL);
			if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getExtras().getString(
                            Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getExtras().getString(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);

            }
			expImage = (ImageView) findViewById(R.id.cat_citiexp_img);

			retailerSearchText = (EditText) findViewById(R.id.cityexp_category_edit_text);
			getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			cancel = (Button) findViewById(R.id.cityexp_cancel);
			cancel.setOnClickListener(this);
			findViewById(R.id.menu_nav_about).setOnClickListener(this);
			findViewById(R.id.cat_citiexp_img).setOnClickListener(this);
			findViewById(R.id.menu_nav_filter).setOnClickListener(this);
			findViewById(R.id.menu_nav_settings).setOnClickListener(this);
			findViewById(R.id.menu_nav_acc).setOnClickListener(this);
			accBtmNavLayout = (LinearLayout) findViewById(R.id.menu_nav_acc)
                    .getParent();
			filterBtmNavLayout = (LinearLayout) findViewById(R.id.menu_nav_filter)
                    .getParent();

			if (getIntent().hasExtra(FILTERCOUNT_EXTRA)) {
                retAffCount = getIntent().getExtras().getString(FILTERCOUNT_EXTRA);
                retAffId = getIntent().getExtras().getString(FILTERID_EXTRA);
                filterName = getIntent().getExtras().getString(
                        CommonConstants.TAG_FILTERNAME);
                if (null != retAffCount && "0".equals(retAffCount)) {
                    filterBtmNavLayout.setVisibility(View.GONE);
                    accBtmNavLayout.setVisibility(View.VISIBLE);
                }

            }

			if (getIntent().hasExtra(BY_CAT_EXP_FILTER_EXTRA)) {

                if (getIntent().getExtras().getString(BY_CAT_EXP_FILTER_EXTRA)
                        .equals(BY_FILTER)) {
                    byCatType = BY_FILTER;
                    findViewById(R.id.menu_nav_filter).setEnabled(false);
                    findViewById(R.id.menu_nav_filter).setPressed(true);
                    findViewById(R.id.menu_nav_filter).setSelected(true);
                    expImage.setTag(cityExpImgUlr);
                    customImageLoader = new CustomImageLoader(getApplicationContext(), false);
                    customImageLoader.displayImage(cityExpImgUlr, this, expImage);

                } else if (getIntent().getExtras()
                        .getString(BY_CAT_EXP_FILTER_EXTRA).equals(BY_EXPERIENCE)) {

                    byCatType = BY_EXPERIENCE;
                    expImage.setTag(imagePathOn);
                    customImageLoader = new CustomImageLoader(getApplicationContext(), false);
                    customImageLoader.displayImage(imagePathOn, this, expImage);

                }

            }

			new GetCategory().execute();

			categorylistView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long pst) {
                    if (categoryList.get(position).get(TAG_FINDCATEGORY_NAME_ID) != null) {
                        getRetailerByCategory(
                                categoryList.get(position).get(
                                        TAG_FINDCATEGORY_NAME_ID), null);
                    }

                }

            });

			retailerSearchText
                    .setOnEditorActionListener(new OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView searchtext,
													  int actionId, KeyEvent event) {
							if (actionId == EditorInfo.IME_ACTION_SEARCH) {

								String seatchTxt = retailerSearchText.getText()
										.toString().trim();
								getRetailerByCategory("0", seatchTxt);
							}

							return true;
						}
					});

			mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			retailerSearchText
                    .setOnFocusChangeListener(new OnFocusChangeListener() {

						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							mgr.showSoftInput(retailerSearchText,
									InputMethodManager.SHOW_IMPLICIT);

						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class GetCategory extends AsyncTask<String, Void, String> {

		JSONObject jsonObject = null;
		JSONArray jsonArray = null;
		JSONObject responseMenuObject = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(CityExperienceByCategory.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {

			categoryList = new ArrayList<>();
			String result = "false";
			try {
				UrlRequestParams mUrlRequestParams = new UrlRequestParams();
				ServerConnections mServerConnections = new ServerConnections();

				if (null != byCatType && byCatType.equals(BY_EXPERIENCE)) {
					String urlParameters = mUrlRequestParams
							.getCategoryByExp(citiExpId);
					String get_cityexpcategory_url = Properties.url_local_server
							+ Properties.hubciti_version
							+ "thislocation/getcatforgroup";
					jsonObject = mServerConnections.getUrlPostResponse(
							get_cityexpcategory_url, urlParameters, true);

				} else if (null != byCatType && byCatType.equals(BY_FILTER)) {

					String urlParameters = mUrlRequestParams
							.getCategoryByFilter(retAffId);
					String get_filtercategory_url = Properties.url_local_server
							+ Properties.hubciti_version
							+ "thislocation/getcatforpartner ";
					jsonObject = mServerConnections.getUrlPostResponse(
							get_filtercategory_url, urlParameters, true);
				}

				if (jsonObject != null) {

					if (jsonObject.has(TAG_RESPONE)) {

						int responeCode = Integer.valueOf(jsonObject
								.getJSONObject(TAG_RESPONE).getString(
										CommonConstants.TAG_RESPONE_CODE));
						switch (responeCode) {
						case 10005:
							result = jsonObject
									.getJSONObject(TAG_RESPONE)
									.getString(CommonConstants.TAG_RESPONE_TEXT);
							break;
						case 10002:
							result = jsonObject
									.getJSONObject(TAG_RESPONE)
									.getString(CommonConstants.TAG_RESPONE_TEXT);
							break;
						case 10001:
							result = jsonObject
									.getJSONObject(TAG_RESPONE)
									.getString(CommonConstants.TAG_RESPONE_TEXT);
							break;
						default:
							break;
						}

					} else {

						jsonArray = jsonObject.getJSONObject(TAG_CATEGORY)
								.optJSONArray(CATEGORY_INFO);

						HashMap<String, String> categoryDateData;
						if (null != jsonArray) {
							for (int arrayCount = 0; arrayCount < jsonArray
									.length(); arrayCount++) {
								categoryDateData = new HashMap<>();
								responseMenuObject = jsonArray
										.getJSONObject(arrayCount);
								categoryDateData
										.put(TAG_CATEGORY_NAME,
												responseMenuObject
														.getString(TAG_FINDCATEGORY_NAME));
								categoryDateData
										.put(TAG_FINDCATEGORY_NAME_ID,
												responseMenuObject
														.getString(TAG_FINDCATEGORY_NAME_ID));
								categoryDateData
										.put(TAG_CATEGORYIMAGE_PATH,
												responseMenuObject
														.getString(TAG_FINDCATEGORYIMAGE_PATH));
								categoryList.add(categoryDateData);
							}
						} else {

							categoryDateData = new HashMap<>();
							responseMenuObject = jsonObject.getJSONObject(
									TAG_CATEGORY).getJSONObject(CATEGORY_INFO);
							categoryDateData.put(TAG_CATEGORY_NAME,
									responseMenuObject
											.getString(TAG_FINDCATEGORY_NAME));
							categoryDateData
									.put(TAG_FINDCATEGORY_NAME_ID,
											responseMenuObject
													.getString(TAG_FINDCATEGORY_NAME_ID));
							categoryDateData
									.put(TAG_CATEGORYIMAGE_PATH,
											responseMenuObject
													.getString(TAG_FINDCATEGORYIMAGE_PATH));
							categoryList.add(categoryDateData);

						}

						result = "true";
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				mDialog.dismiss();

				if ("true".equals(result)) {

                    catListAdapter = new FindCategoryListAdapter(
                            CityExperienceByCategory.this, categoryList);
                    categorylistView.setAdapter(null);
                    categorylistView.setAdapter(catListAdapter);
                } else {
                    Toast.makeText(getBaseContext(), result, Toast.LENGTH_SHORT)
                            .show();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cityexp_cancel:
			retailerSearchText.getText().clear();
			mgr.hideSoftInputFromWindow(retailerSearchText.getWindowToken(), 0);
			break;
		case R.id.cat_citiexp_img:
			catCitiexpImg();
			break;
		case R.id.menu_nav_filter:
			menuNavFilter();
			break;

		case R.id.menu_nav_about:
			menuNavAbout();
			break;

		case R.id.menu_nav_acc:
			menuNavAcc();
			break;

		case R.id.menu_nav_settings:
			menuNavSettings();
			break;
		default:
			break;
		}
	}

	private void menuNavSettings() {
		Intent intent = new Intent(getApplicationContext(),
				PreferedCatagoriesScreen.class);
		intent.putExtra("regacc", false);
		startActivityForResult(intent, Constants.STARTVALUE);

	}

	private void menuNavAcc() {
		Intent settingsIntent = new Intent(getApplicationContext(),
				SetPreferrenceScreen.class);
		settingsIntent.putExtra("regacc", false);
		startActivityForResult(settingsIntent, Constants.STARTVALUE);

	}

	private void menuNavAbout() {
		Intent aboutUsActivity = new Intent(getApplicationContext(),
				AboutUsActivity.class);
		aboutUsActivity.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
		startActivityForResult(aboutUsActivity, Constants.STARTVALUE);

	}

	private void menuNavFilter() {
		if (null != retAffCount && "1".equals(retAffCount)) {
			Intent showFilter = new Intent(getApplicationContext(),
					CitiExperienceActivity.class);

			showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
					Constants.getMainMenuId());
			showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, citiExpId);
			showFilter.putExtra(Constants.CAT_IDS_INTENT_EXTRA, "0");
			showFilter.putExtra(CITYEXP_IMG_URL, cityExpImgUlr);
			showFilter.putExtra(CITYEXP_ON_IMG_URL, imagePathOn);
			showFilter.putExtra(TAG_FILTERID, retAffId);
			showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, citiExpId);
			showFilter.putExtra(FILTERCOUNT_EXTRA, retAffCount);
			startActivityForResult(showFilter, Constants.STARTVALUE);
			return;
		} else {
			Intent showFilter = new Intent(getApplicationContext(),
					CityExperienceFilterActivity.class);
			showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, citiExpId);
			showFilter.putExtra(CITYEXP_IMG_URL, cityExpImgUlr);
			startActivityForResult(showFilter, Constants.STARTVALUE);
			return;

		}

	}

	private void catCitiexpImg() {
		Intent showExperience = new Intent(getApplicationContext(),
				CitiExperienceActivity.class);
		showExperience.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
				Constants.getMainMenuId());
		showExperience.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, citiExpId);
		showExperience.putExtra(Constants.CAT_IDS_INTENT_EXTRA, "0");
		showExperience.putExtra(CITYEXP_IMG_URL, cityExpImgUlr);
		startActivityForResult(showExperience, Constants.STARTVALUE);

	}

	private void getRetailerByCategory(String catIds, String searchKey) {
		Intent intent = new Intent(CityExperienceByCategory.this,
				CitiExperienceActivity.class);
		intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
				Constants.getMainMenuId());
		intent.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, citiExpId);
		intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
		intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
		intent.putExtra(CITYEXP_IMG_URL, cityExpImgUlr);
		intent.putExtra(CITYEXP_ON_IMG_URL, imagePathOn);

		if (null != byCatType && byCatType.equals(BY_FILTER)) {
			intent.putExtra(FILTERCOUNT_EXTRA, retAffCount);
			intent.putExtra(CommonConstants.TAG_FILTERNAME, filterName);
			intent.putExtra(CommonConstants.FILTERID_EXTRA, retAffId);
		}

		startActivityForResult(intent, 3000);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 3000) {
			setResult(3000);
			finish();
		}

		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}
		if (requestCode == Constants.STARTVALUE) {
			setResult(Constants.FINISHVALUE);
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);

	}
}
