package com.hubcity.android.screens;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.MailTo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.provider.Settings;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
@SuppressLint("InlinedApi")
public class EventDescription extends CustomTitleBar implements OnClickListener {

    ImageView imageDescription;
    // TextView txtDescription;
    WebView longDescription;

    Button packageDetails;
    Button hotelDetails;
    Button eventLogistics;
    Button purchaseticket;
    Button moreInfo;
    public ArrayList<HashMap<String, String>> popUplistData;
    ProgressDialog progDialog;
    String responseText = "Connection time out";
    private static String returnValue = "sucessfull";

    JSONObject jsonObject;

    String mEventId;
    String mEventName;
    String mHubCitiID;

    String longDesc = "";
    String htmlcode = "";

    HashMap<String, String> eventDetails = new HashMap<>();

    TextView startDate;
    TextView startTime;
    TextView endDate;
    TextView endTime;
    TextView shortDesc;
    TextView eventsRecurrence;
    Button mLocation;
    ScrollView mScrollView;
    String mItemId, bottomBtnId;
    PopupWindow pwindo;
    PopupWindow eventPwindo;
    Button btnCloseLocPopup;
    LinearLayout mLinearLayout;

    LocationManager locationManager;
    String latitude, longitude;

    ArrayList<HashMap<String, String>> appSitetDetails = new ArrayList<>();

    String isAppSiteFlag = "";
    String evtLocTitle = "";
    String eventLgSSQRPath = "";

    AppSitesAsyncTask appsiteAsyncTask;
    calenderAsyncTask calenderAsyncTask;
    EventDetailsAsyncTask eventsAsyncTask;

    ShareInformation shareInfo;
    boolean isShareTaskCalled;

    Activity activity;
    Button addToCalendar;
    Button eventCreate;
    Button eventCopy;
    Button eventCancel;

    String occurenceType;
    String eventStartDate;
    String eventEDate;
    int eventTimeHrs = 00;
    int eventTimeMins = 00;
    // initializing if there is no end date then event is all day event
    int eventETimeHrs = 23;
    int eventETimeMins = 59;
    int recurrenceInterval;
    int recurrencePatternID;
    String isOnGoing;
    boolean isWeekDay;
    int endAfter;
    String days;
    int dayOfMonth;
    boolean byDayNumber;
    int dateOfMonth;
    String everyWeekDayMonth;
    int dayNumber = 0;
    boolean allDay = false;
    int sYear;
    int sMonth;
    int sDay;
    int eYear;
    int eMonth;
    int eDay;
    Calendar beginEvent;
    Calendar endEvent;
    String frequency;
    int count;
    String until;
    int interval;
    String byDay;
    ContentValues event = new ContentValues();
    boolean isCreatedBool = false;
    LinkedList<String> appsiteAdress;
    String copyAdress;
    String copyStartTime;
    String copyEndTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_description);

        try {
            addToCalendar = (Button) findViewById(R.id.add_to_calendar);
            activity = EventDescription.this;
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            isAppSiteFlag = "";

            if (EventsListDisplay.eventIDs != null) {
                mEventId = EventsListDisplay.eventIDs
                        .get(EventsListDisplay.eventIDs.size() - 1);
            }

            mEventName = getIntent().getStringExtra("eventName");
            mHubCitiID = getIntent().getStringExtra("hubCitiId");
            if (getIntent().hasExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getStringExtra(
                    Constants.MENU_ITEM_ID_INTENT_EXTRA) != null) {
                mItemId = getIntent().getStringExtra(
                        Constants.MENU_ITEM_ID_INTENT_EXTRA);
            }
            if (getIntent().hasExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA)
                    && getIntent().getStringExtra(
                    Constants.BOTTOM_ITEM_ID_INTENT_EXTRA) != null) {
                bottomBtnId = getIntent().getStringExtra(
                        Constants.BOTTOM_ITEM_ID_INTENT_EXTRA);
            }
            title.setText(mEventName);

            leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
            addToCalendar.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    initiateEventPopupWindow();

                }
            });
            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (EventsListDisplay.eventIDs != null
                            && !EventsListDisplay.eventIDs.isEmpty()) {
                        EventsListDisplay.eventIDs
                                .remove(EventsListDisplay.eventIDs.size() - 1);
                    }

                    cancelBackgroundProcess();
                    finish();
                }
            });
            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (GlobalConstants.isFromNewsTemplate) {
                        Intent intent = null;
                        switch (GlobalConstants.className) {
                            case Constants.COMBINATION:
                                intent = new Intent(EventDescription.this, CombinationTemplate.class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(EventDescription.this, ScrollingPageActivity.class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(EventDescription.this, TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }else
                    {
                        setResult(Constants.FINISHVALUE);
                        cancelBackgroundProcess();
                        startMenuActivity();
                    }
                }
            });

            mScrollView = (ScrollView) findViewById(R.id.scrollView1);

            mLinearLayout = (LinearLayout) findViewById(R.id.events_long_description_layout);

            imageDescription = (ImageView) findViewById(R.id.event_imageDescription_type_package);

            longDescription = (WebView) findViewById(R.id.event_textDescription_type_package);
            setWebViewSettings(longDescription);

            packageDetails = (Button) findViewById(R.id.event_Details_button1);
            hotelDetails = (Button) findViewById(R.id.event_Details_button2);
            eventLogistics = (Button) findViewById(R.id.event_logistics);
            purchaseticket = (Button) findViewById(R.id.event_Details_button3);
            moreInfo = (Button) findViewById(R.id.event_Details_button4);

            shortDesc = (TextView) findViewById(R.id.event_textDescription_short_description);
            startDate = (TextView) findViewById(R.id.events_description_start_date);
            startTime = (TextView) findViewById(R.id.events_description_start_time);
            endDate = (TextView) findViewById(R.id.events_description_end_date);
            endTime = (TextView) findViewById(R.id.events_description_end_time);
            eventsRecurrence = (TextView) findViewById(R.id.events_recurrence);
            mLocation = (Button) findViewById(R.id.event_Details_button_location);

            startDate.setVisibility(View.VISIBLE);
            startTime.setVisibility(View.VISIBLE);
            endDate.setVisibility(View.VISIBLE);
            endTime.setVisibility(View.VISIBLE);

            mLocation.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    if ("0".equals(isAppSiteFlag)) {

                        ArrayList<HashMap<String, String>> addressDetailList = new ArrayList<>();
                        HashMap<String, String> addressDetail = new HashMap<>();

                        addressDetail.put("appSiteName", "");
                        addressDetail.put("address", eventDetails.get("address"));

                        // New Changes
                        addressDetail.put("address1", eventDetails.get("address1"));
                        addressDetail.put("address2", eventDetails.get("address2"));
                        addressDetail.put("busEvent", eventDetails.get("busEvent"));
                        addressDetail.put("latitude", eventDetails.get("latitude"));
                        addressDetail.put("longitude",
                                eventDetails.get("longitude"));

                        addressDetailList.add(addressDetail);

                        initiateLocationWindow(addressDetailList);

                    } else if ("1".equals(isAppSiteFlag)) {
                        appsiteAsyncTask = new AppSitesAsyncTask();
                        appsiteAsyncTask.execute();

                    }

                }
            });

            packageDetails.setOnClickListener(this);
            hotelDetails.setOnClickListener(this);
            eventLogistics.setOnClickListener(this);
            purchaseticket.setOnClickListener(this);
            moreInfo.setOnClickListener(this);

            eventsAsyncTask = new EventDetailsAsyncTask();
            eventsAsyncTask.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void initiateEventPopupWindow() {
        if ("1".equals(isAppSiteFlag)) {
            calenderAsyncTask = new calenderAsyncTask();
            calenderAsyncTask.execute();
        }
        // We need to get the instance of the LayoutInflater
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.eventcalender_popup,
                (ViewGroup) this.findViewById(R.id.event_popup_element));
        DisplayMetrics metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int screenWidth = metrics.widthPixels;
        int screenHeight = metrics.heightPixels;
        eventPwindo = new PopupWindow(layout, screenWidth, screenHeight / 3,
                true);

        eventPwindo.setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        eventPwindo.setOutsideTouchable(true);

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    eventPwindo.dismiss();
                }
                return false;
            }
        });
        eventPwindo.showAtLocation(layout, Gravity.BOTTOM, 0, 0);


        eventCreate = (Button) layout.findViewById(R.id.create_event);

        eventCopy = (Button) layout.findViewById(R.id.copy);
        eventCancel = (Button) layout.findViewById(R.id.cancel);
        // format the StartDate of 11/27/2015 to sDay=27,sMonth=11,sYear=2015
        dateFormator(eventStartDate, true);
        eventCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                eventPwindo.dismiss();

            }
        });
        eventCopy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                CharSequence copyText = "";

                if (!"0".equals(isAppSiteFlag)) {

                    copyAdress = strJoin(appsiteAdress, ",");

                }
                // copy text to clipboard
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                if (isOnGoing.equalsIgnoreCase("no")) {

                    if (eventStartDate.equals(eventEDate)) {
                        if (occurenceType.equalsIgnoreCase("noEndDate")) {
                            if (copyEndTime.equals("N/A")) {
                                copyText = "<b>Title:</b> "
                                        + eventDetails.get("eventName")
                                        + "<br/><b>Short Description:</b> "
                                        + eventDetails.get("shortDes")
                                        + "<br/><b>Location:</b> " + copyAdress
                                        + "<br/><b>Start Date:</b> " + eventStartDate
                                        + ", " + copyStartTime;
                            } else {
                                copyText = "<b>Title:</b> "
                                        + eventDetails.get("eventName")
                                        + "<br/><b>Short Description:</b> "
                                        + eventDetails.get("shortDes")
                                        + "<br/><b>Location:</b> " + copyAdress
                                        + "<br/><b>Start Date:</b> " + eventStartDate
                                        + ", " + copyStartTime + "<br/><b>End Date:</b> "
                                        + eventEDate + ", " + copyEndTime;
                            }

                        }
                    } else {

                        if (copyEndTime.equals("N/A")) {
                            copyText = "<b>Title:</b> "
                                    + eventDetails.get("eventName")
                                    + "<br/><b>Short Description:</b> "
                                    + eventDetails.get("shortDes")
                                    + "<br/><b>Location:</b> " + copyAdress
                                    + "<br/><b>Start Date:</b> " + eventStartDate
                                    + ", " + copyStartTime;
                        } else {
                            copyText = "<b>Title:</b> "
                                    + eventDetails.get("eventName")
                                    + "<br/><b>Short Description:</b> "
                                    + eventDetails.get("shortDes")
                                    + "<br/><b>Location:</b> " + copyAdress
                                    + "<br/><b>Start Date:</b> " + eventStartDate
                                    + ", " + copyStartTime + "<br/><b>End Date:</b> "
                                    + eventEDate + ", " + copyEndTime;
                        }

                    }

                } else {
                    if (occurenceType.equalsIgnoreCase("noEndDate")) {
                        copyText = "<b>Title:</b> "
                                + eventDetails.get("eventName")
                                + "<br/><b>Short Description:</b> "
                                + eventDetails.get("shortDes")
                                + "<br/><b>Location:</b> " + copyAdress
                                + "<br/><b>Start Date:</b> " + eventStartDate
                                + ", " + copyStartTime;
                    } else {
                        copyText = "<b>Title:</b> "
                                + eventDetails.get("eventName")
                                + "<br/><b>Short Description:</b> "
                                + eventDetails.get("shortDes")
                                + "<br/><b>Location:</b> " + copyAdress
                                + "<br/><b>Start Date:</b> " + eventStartDate
                                + ", " + copyStartTime + "<br/><b>End Date:</b> "
                                + eventEDate + ", " + copyEndTime;
                    }
                }
                CharSequence styledText = Html.fromHtml((String) copyText);
                ClipData clip = ClipData.newPlainText("Text", styledText);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getApplicationContext(),
                        "Event is copied to clipboard", Toast.LENGTH_LONG)
                        .show();
                eventPwindo.dismiss();
            }
        });
        eventCreate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!"0".equals(isAppSiteFlag)) {

                    copyAdress = strJoin(appsiteAdress, ",");

                }

                isCreatedBool = checkEventCreated();
                if (isCreatedBool) {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(
                            EventDescription.this);
                    builder1.setMessage("Event has already been added to your calendar. Add again?");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                    eventPwindo.dismiss();
                                    isCreatedBool = false;
                                    createEvent();
                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                    eventPwindo.dismiss();
                                    isCreatedBool = true;
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }
                createEvent();

            }

        });

    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @SuppressLint("InlinedApi")
    private void createEvent() {
        if (!isCreatedBool) {

            // To add events to calender
            Uri eventsUri;
            if (android.os.Build.VERSION.SDK_INT >= 8) {

                eventsUri = Uri.parse("content://com.android.calendar/events");
            } else {

                eventsUri = Uri.parse("content://calendar/events");
            }

            event.put(CalendarContract.Events.CALENDAR_ID, 1);
            event.put(CalendarContract.Events.TITLE,
                    eventDetails.get("eventName"));
            event.put(CalendarContract.Events.DESCRIPTION,
                    eventDetails.get("shortDes"));
            if ("0".equals(isAppSiteFlag)) {
                event.put(CalendarContract.Events.EVENT_LOCATION,
                        eventDetails.get("address"));
            } else {

                event.put(CalendarContract.Events.EVENT_LOCATION, copyAdress);
            }

            Calendar mCalendar = Calendar.getInstance();
            TimeZone timeZone = mCalendar.getTimeZone();
            event.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());

            beginEvent();

            event.put(CalendarContract.Events.DTSTART,
                    beginEvent.getTimeInMillis());

            if (isOnGoing.equalsIgnoreCase("no")) {
                if (eventStartDate.equals(eventEDate)) {
                    if (occurenceType.equalsIgnoreCase("noEndDate")) {
                        if ((eventTimeHrs == 0) && (eventTimeMins == 0)) {
                            if ((eventETimeHrs == 0) && (eventETimeMins == 0)) {
                                allDay = true;
                                eventEDate = eventStartDate;
                                dateFormator(eventEDate, false);
                            } else if ((eventETimeHrs == 23)
                                    && (eventETimeMins == 59)) {
                                allDay = true;
                                eventEDate = eventStartDate;
                                dateFormator(eventEDate, false);
                            } else {
                                dateFormator(eventEDate, false);
                            }
                        } else {
                            dateFormator(eventEDate, false);
                        }

                    } else {
                        dateFormator(eventEDate, false);
                    }
                } else {

                    if ((eventTimeHrs == 0) && (eventTimeMins == 0)
                            && (eventETimeHrs == 0) && (eventETimeMins == 0)) {
                        eventETimeHrs = 23;
                        eventETimeMins = 59;
                        dateFormator(eventEDate, false);
                    } else {
                        dateFormator(eventEDate, false);
                    }

                }
            } else {
                getRecurenceId(recurrencePatternID);
                getOccurenceType(occurenceType);
                createRecurEvent(recurrencePatternID);

            }

            event.put(CalendarContract.Events.ALL_DAY, allDay);
            endEvent();
            event.put(CalendarContract.Events.DTEND, endEvent.getTimeInMillis());
            event.put(CalendarContract.Events.STATUS, 1);
            event.put(CalendarContract.Events.HAS_ALARM, 1); // 0 for
            // false,
            // 1 for
            // true
            @SuppressWarnings("unused")
            Uri url = getContentResolver().insert(eventsUri, event);

            eventPwindo.dismiss();
            Toast.makeText(getApplicationContext(),
                    "Event is added to the calendar", Toast.LENGTH_LONG).show();

        }

    }

    @SuppressLint("NewApi")
    private boolean checkEventCreated() {

        boolean isCreated = false;
        // format ensDate 01/26/2016 to eDay=26,eMonth=01,eYear=2016
        dateFormator(eventEDate, false);
        // to get calender events list
        String[] projection = new String[]{
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DESCRIPTION,
                CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND,
                CalendarContract.Events.ALL_DAY,
                CalendarContract.Events.EVENT_LOCATION};

        // to check from 1min back of startdate to endDate
        int eventStartTimeHrs = 0;
        int eventStartTimeMins = 0;
        int eventStartDay = sDay;
        int eventStartMonth = sMonth;
        int eventStartYear = sYear;
        if (eventTimeMins == 0) {
            eventStartTimeHrs = eventTimeHrs - 1;
            if (eventStartTimeHrs < 0) {
                eventStartTimeHrs = 0;
                eventStartTimeMins = 0;
                if (((eventStartTimeHrs == 0) && (eventStartTimeMins == 0))) {
                    eventStartDay = sDay - 1;
                    if (eventStartDay == 0) {
                        eventStartMonth = sMonth - 1;
                        if (eventStartMonth < 0) {
                            eventStartYear = sYear - 1;
                        }
                    }
                }
            } else {
                eventStartTimeMins = 59;
            }
        } else {
            eventStartTimeHrs = eventTimeHrs;
            eventStartTimeMins = eventTimeMins - 1;
            if (eventStartTimeMins < 0) {
                eventStartTimeMins = 0;
            }

        }
        Calendar startTime = Calendar.getInstance();
        startTime.set(eventStartYear, eventStartMonth, eventStartDay,
                eventStartTimeHrs, eventStartTimeMins);

        Calendar endTime = Calendar.getInstance();

        endTime.set(eYear, eMonth, eDay, eventETimeHrs, eventETimeMins);

        String selection = "(( " + CalendarContract.Events.DTSTART + " >= "
                + startTime.getTimeInMillis() + " ) AND ( "
                + CalendarContract.Events.DTSTART + " <= "
                + endTime.getTimeInMillis() + " ))";

        Cursor cursor = this
                .getBaseContext()
                .getContentResolver()
                .query(CalendarContract.Events.CONTENT_URI, projection,
                        selection, null, null);

        // output the events

        if (cursor.moveToFirst()) {
            do {
                String startEventTime = (new Date(cursor.getLong(3)))
                        .toString();
                String[] Array = startEventTime.split("\\s", 5);

                String monthString = Array[1];
                int monthInt = getMonthValue(monthString);

                String dateString = Array[2];
                int dateInt = Integer.parseInt(dateString);
                String hourString = Array[3];

                String yearString = Array[4];
                String yearSubString[] = yearString.split("\\s", 0);
                int yearInt = Integer.parseInt(yearSubString[1]);

                String[] subArray = hourString.split(":", 2);
                String sHourString = subArray[0];
                int sHourInt = Integer.parseInt(sHourString);
                String sMinSubString = subArray[1];
                String[] sMinSubStringArray = sMinSubString.split(":", 0);
                int sMinInt = Integer.parseInt(sMinSubStringArray[0]);

                String location = cursor.getString(6);
                String title = cursor.getString(1);
                if ("0".equals(isAppSiteFlag)) {
                    if ((yearInt == sYear)
                            && (monthInt == sMonth)
                            && (dateInt == sDay)
                            && (sHourInt == eventTimeHrs)
                            && (sMinInt == eventTimeMins)
                            && (location.equalsIgnoreCase(eventDetails
                            .get("address2")))
                            && (title.equalsIgnoreCase(eventDetails
                            .get("eventName")))) {

                        isCreated = true;
                        break;
                    }
                } else {

                    if ((yearInt == sYear)
                            && (monthInt == sMonth)
                            && (dateInt == sDay)
                            && (sHourInt == eventTimeHrs)
                            && (sMinInt == eventTimeMins)
                            && (location.equalsIgnoreCase(copyAdress))
                            && (title.equalsIgnoreCase(eventDetails
                            .get("eventName")))) {

                        isCreated = true;
                        break;
                    }
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return isCreated;
    }

    private int getMonthValue(String monthString) {
        int value = 0;
        switch (monthString) {
            case "Jan":
                value = 0;
                break;
            case "Feb":
                value = 1;
                break;
            case "Mar":
                value = 2;
                break;
            case "Apr":
                value = 3;
                break;
            case "May":
                value = 4;
                break;
            case "Jun":
                value = 5;
                break;
            case "Jul":
                value = 6;
                break;
            case "Aug":
                value = 7;
                break;
            case "Sep":
                value = 8;
                break;
            case "Oct":
                value = 9;
                break;
            case "Nov":
                value = 10;
                break;
            case "Dec":
                value = 11;
                break;
            default:
                break;
        }
        return value;
    }

    private void createRecurEvent(int recurrencePatternID) {
        if (recurrencePatternID == 1) {
            createDailyRecurEvent();
        } else if (recurrencePatternID == 2) {
            createWeeklyRecurEvent();
        } else {
            createMonthlyRecurEvent();
        }

    }

    private void createMonthlyRecurEvent() {
        interval = recurrenceInterval;

        if (byDayNumber) {

            if (occurenceType.equalsIgnoreCase("noEndDate")) {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "INTERVAL=" + interval);
            } else if (occurenceType.equalsIgnoreCase("endAfter")) {
                getUntil();

                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "INTERVAL=" + interval + ";" + "UNTIL=" + until);
            } else {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "UNTIL=" + until + ";" + "INTERVAL=" + interval);
            }
        } else {

            convertToByDay(everyWeekDayMonth, false);

            if (occurenceType.equalsIgnoreCase("noEndDate")) {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay + ";" + "INTERVAL=" + interval);
            } else if (occurenceType.equalsIgnoreCase("endAfter")) {
                getUntil();
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay + ";" + "UNTIL=" + until + ";"
                        + "INTERVAL=" + interval);
            } else {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay + ";" + "UNTIL=" + until + ";"
                        + "INTERVAL=" + interval);
            }

        }

        eventEDate = eventStartDate;
        dateFormator(eventEDate, false);

    }

    private void getUntil() {
        String[] stringDate = eventEDate.split("/", 3);
        String[] stringSubDate = new String[stringDate.length];

        for (int i = 0; i < stringDate.length; i++) {
            if (i == (stringDate.length - 2)) {
                stringSubDate[i] = stringDate[i];

            } else {
                stringSubDate[i] = stringDate[i];
            }

        }
        until = stringSubDate[2] + stringSubDate[0] + stringSubDate[1];

    }

    private void createWeeklyRecurEvent() {
        interval = recurrenceInterval;
        if (days != null) {
            convertToByDay(days, true);
            if (occurenceType.equalsIgnoreCase("noEndDate")) {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay + ";" + "INTERVAL=" + interval);
            } else if (occurenceType.equalsIgnoreCase("endAfter")) {
                getUntil();

                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay + ";" + "UNTIL=" + until + ";"
                        + "INTERVAL=" + interval);
            } else {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay + ";" + "UNTIL=" + until + ";"
                        + "INTERVAL=" + interval);
            }
        }
        eventEDate = eventStartDate;
        dateFormator(eventEDate, false);
    }

    private void convertToByDay(String days, boolean isWeekDay) {

        String array[] = days.split(",");
        int length = array.length;
        String dayFormat = null;
        List<String> dayConcat = new ArrayList<>();

        if (isWeekDay) {
            for (int i = length; i > 0; i--) {

                dayFormat = getDay(array[length - i]);
                dayConcat.add(dayFormat);

            }
        } else {
            String dayNumString;
            if (dayNumber != 5) {
                dayNumString = String.valueOf(dayNumber);
            } else {
                dayNumString = "-1";// for last day of the week in monthly
                // recurring
            }
            for (int i = length; i > 0; i--) {

                dayFormat = getDay(array[length - i]);
                dayFormat = dayNumString + dayFormat;
                dayConcat.add(dayFormat);

            }

        }

        byDay = strJoin(dayConcat, ",");
    }

    private String strJoin(List<String> dayConcat, String specialChar) {
        StringBuilder sbStr = new StringBuilder();
        for (int i = 0, il = dayConcat.size(); i < il; i++) {
            if (i > 0)
                sbStr.append(specialChar);
            sbStr.append(dayConcat.get(i));
        }
        return sbStr.toString();
    }

    private String getDay(String day) {
        String recurDay = null;
        switch (day) {
            case "Sunday":
                recurDay = "SU";
                break;
            case "Monday":
                recurDay = "MO";
                break;
            case "Tuesday":
                recurDay = "TU";
                break;
            case "Wednesday":
                recurDay = "WE";
                break;
            case "Thursday":
                recurDay = "TH";
                break;
            case "Friday":
                recurDay = "FR";
                break;
            case "Saturday":
                recurDay = "SA";
                break;

            default:
                break;
        }
        return recurDay;
    }

    private void createDailyRecurEvent() {

        if (!isWeekDay) {

            interval = recurrenceInterval;
            if (occurenceType.equalsIgnoreCase("noEndDate")) {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "INTERVAL=" + interval);
            } else if (occurenceType.equalsIgnoreCase("endAfter")) {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "INTERVAL=" + interval + ";" + "COUNT=" + count);
            } else {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "INTERVAL=" + interval + ";" + "UNTIL=" + until);
            }
        } else {
            byDay = "MO," + "TU," + "WE," + "TH," + "FR";
            if (occurenceType.equalsIgnoreCase("noEndDate")) {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay);
            } else if (occurenceType.equalsIgnoreCase("endAfter")) {
                getUntil();
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay + ";" + "UNTIL=" + until);
            } else {
                event.put(CalendarContract.Events.RRULE, "FREQ=" + frequency
                        + ";" + "BYDAY=" + byDay + ";" + "UNTIL=" + until);
            }
        }
        eventEDate = eventStartDate;
        dateFormator(eventEDate, false);
    }

    private void getOccurenceType(String occurenceType) {
        switch (occurenceType) {
            case "noEndDate":
                if ((eventTimeHrs == 0) && (eventTimeMins == 0)
                        && (eventETimeHrs == 0) && (eventETimeMins == 0)) {
                    eventETimeHrs = 23;
                    eventETimeMins = 59;
                }
                dateFormator(eventEDate, false);
                break;
            case "endAfter":
                if ((eventTimeHrs == 0) && (eventTimeMins == 0)
                        && (eventETimeHrs == 0) && (eventETimeMins == 0)) {
                    eventETimeHrs = 23;
                    eventETimeMins = 59;
                }
                count = endAfter;
                break;
            case "endBy":
                if ((eventTimeHrs == 0) && (eventTimeMins == 0)
                        && (eventETimeHrs == 0) && (eventETimeMins == 0)) {
                    eventETimeHrs = 23;
                    eventETimeMins = 59;
                }
                String[] stringDate = eventEDate.split("/", 3);
                String[] stringSubDate = new String[stringDate.length];

                for (int i = 0; i < stringDate.length; i++) {
                    if (i == (stringDate.length - 2)) {
                        stringSubDate[i] = stringDate[i];

                    } else {
                        stringSubDate[i] = stringDate[i];
                    }

                }
                until = stringSubDate[2] + stringSubDate[0] + stringSubDate[1];// in
                // month
                // ,date
                // and
                // year
                // format
                break;
            default:
                break;
        }

    }

    private void getRecurenceId(int recurrencePatternID) {
        switch (recurrencePatternID) {
            case 1:
                frequency = "DAILY";
                break;
            case 2:
                frequency = "WEEKLY";
                break;
            case 3:
                frequency = "MONTHLY";
                break;
            default:
                break;
        }

    }

    private void endEvent() {

        endEvent = Calendar.getInstance();
        endEvent.clear();
        endEvent.set(eYear, eMonth, eDay, eventETimeHrs, eventETimeMins);
    }

    private void beginEvent() {

        beginEvent = Calendar.getInstance();
        beginEvent.clear();
        beginEvent.set(sYear, sMonth, sDay, eventTimeHrs, eventTimeMins);
    }

    @SuppressLint("SimpleDateFormat")
    private void dateFormator(String Date, boolean isStartDate) {

        String[] stringDate = Date.split("/", 3);
        int[] intDate = new int[3];

        for (int i = 0; i < stringDate.length; i++) {
            intDate[i] = Integer.parseInt(stringDate[i]);

        }
        if (isStartDate) {
            sDay = intDate[1];
            sMonth = intDate[0] - 1;// month starts from 0=january
            sYear = intDate[2];
        } else {
            eDay = intDate[1];
            eMonth = intDate[0] - 1;
            eYear = intDate[2];
        }
    }

    @Override
    public void onClick(View v) {

        if (v == packageDetails) {

            Intent packageDetails = new Intent(this, EventPackageDetails.class);
            packageDetails.putExtra("pkgDes", eventDetails.get("pkgDes"));
            packageDetails.putExtra("eventName", eventDetails.get("eventName"));

            startActivityForResult(packageDetails, Constants.STARTVALUE);

        }

        if (v == hotelDetails) {

            Intent hotelDetails = new Intent(this,
                    EventsHotelListActivity.class);
            hotelDetails.putExtra("eventId", mEventId);
            startActivityForResult(hotelDetails, Constants.STARTVALUE);

        }

        if (v == eventLogistics) {

            Intent eventLogistics = new Intent(this,
                    ScanseeBrowserActivity.class);
            eventLogistics.putExtra(CommonConstants.URL,
                    eventDetails.get("eventLgSSQRPath"));
            eventLogistics.putExtra("isEventLogistics", true);
            startActivityForResult(eventLogistics, Constants.STARTVALUE);

        }

        if (v == purchaseticket) {

            Intent purchaseticket = new Intent(this,
                    ScanseeBrowserActivity.class);
            purchaseticket.putExtra(CommonConstants.URL,
                    eventDetails.get("pkgTicketURL"));
            startActivityForResult(purchaseticket, Constants.STARTVALUE);

        }
        if (v == moreInfo) {

            Intent purchaseticket = new Intent(this,
                    ScanseeBrowserActivity.class);
            purchaseticket.putExtra(CommonConstants.URL,
                    eventDetails.get("moreInfoURL"));
            startActivityForResult(purchaseticket, Constants.STARTVALUE);

        }
    }

    public class EventDetailsAsyncTask extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        protected void onPreExecute() {
            progDialog = new ProgressDialog(EventDescription.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if ("sucessfull".equals(returnValue)) {
                    mScrollView.setVisibility(View.VISIBLE);
                    if (eventDetails.get("longDes") != null) {
                        mLinearLayout.setVisibility(View.VISIBLE);
                        longDescription.setVisibility(View.VISIBLE);

                        longDesc = eventDetails.get("longDes");
                        htmlcode = "<html><body style=\"text-align:justify\"> %s </body></Html>";

                        if (null != longDescription) {

                            longDesc = longDesc.replaceAll("&#60;", "<");
                            longDesc = longDesc.replaceAll("&#62;", ">");

                            longDescription.loadData(
                                    String.format(htmlcode, longDesc), "text/html",
                                    "utf-8");
                        }
                    } else {
                        mLinearLayout.setVisibility(View.GONE);
                        longDescription.setVisibility(View.GONE);
                    }

                    shortDesc.setText(eventDetails.get("shortDes"));
                    if (eventDetails.get("startTime") != null
                            && !"N/A".equalsIgnoreCase(eventDetails
                            .get("startTime"))) {

                        startTime.setText("Start Time : "
                                + eventDetails.get("startTime"));
                    } else {
                        startTime.setVisibility(View.GONE);
                    }
                    if (eventDetails.get("startDate") != null
                            && !"N/A".equalsIgnoreCase(eventDetails
                            .get("startDate"))) {
                        startDate.setText("Start Date : "
                                + eventDetails.get("startDate"));
                    } else {
                        startDate.setVisibility(View.GONE);
                    }
                    if (eventDetails.get("endTime") != null
                            && !"N/A".equalsIgnoreCase(eventDetails.get("endTime"))) {
                        endTime.setText("End Time : " + eventDetails.get("endTime"));
                    } else {
                        endTime.setVisibility(View.GONE);
                    }
                    if (eventDetails.get("recurringDays") != null) {
                        eventsRecurrence.setText(eventDetails.get("recurringDays"));
                    }
                    if ((eventDetails.get("endDate") != null)
                            && !"N/A".equalsIgnoreCase(eventDetails.get("endDate"))) {
                        endDate.setText("End Date : " + eventDetails.get("endDate"));
                    } else {
                        endDate.setVisibility(View.GONE);
                    }

                    if (eventDetails.get("imgPath") != null
                            && !"".equals(eventDetails.get("imgPath"))
                            && !"N/A".equalsIgnoreCase(eventDetails.get("imgPath"))) {
                        String imagePath = eventDetails.get("imgPath");
                        new CustomImageLoader(imageDescription, EventDescription.this)
                                .setScaledImage(imagePath);
                    }

                    if ("0".equals(eventDetails.get("pkgEvent"))
                            || eventDetails.get("pkgEvent") == null) {

                        packageDetails.setVisibility(View.GONE);
                    } else {
                        packageDetails.setVisibility(View.VISIBLE);
                    }

                    if ("0".equals(eventDetails.get("hotelFlag"))
                            || eventDetails.get("hotelFlag") == null) {

                        hotelDetails.setVisibility(View.GONE);
                    } else {
                        hotelDetails.setVisibility(View.VISIBLE);
                    }

                    if (!"".equals(eventLgSSQRPath)
                            && !"N/A".equalsIgnoreCase(eventLgSSQRPath)) {
                        eventLogistics.setVisibility(View.VISIBLE);

                    } else {
                        eventLogistics.setVisibility(View.GONE);

                    }

                    if (eventDetails.get("moreInfoURL") == null) {

                        moreInfo.setVisibility(View.GONE);
                    } else {
                        moreInfo.setVisibility(View.VISIBLE);
                    }

                    if (eventDetails.get("pkgTicketURL") == null) {

                        purchaseticket.setVisibility(View.GONE);
                    } else {
                        purchaseticket.setVisibility(View.VISIBLE);
                    }

                    // Call for Share Information
                    shareInfo = new ShareInformation(EventDescription.this,
                            mEventId, "Events");
                    isShareTaskCalled = false;

                    leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
                    leftTitleImage.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (!Constants.GuestLoginId.equals(UrlRequestParams
                                    .getUid().trim())) {
                                // CityPreferencesScreen.bIsSendingTimeStamp =
                                // false;
                                shareInfo.shareTask(isShareTaskCalled);
                                isShareTaskCalled = true;
                            } else {
                                Constants.SignUpAlert(EventDescription.this);
                                isShareTaskCalled = false;
                            }
                        }
                    });

                } else {
                    progDialog.dismiss();
                    Toast.makeText(EventDescription.this, responseText,
                            Toast.LENGTH_SHORT).show();
                }

                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... arg0) {

            try {
                String url_event_description = Properties.url_local_server

                        + Properties.hubciti_version + "alertevent/fetcheventdetail?";

                String urlParameters;
                // to differentiate event and band event
                if(getIntent().getStringExtra("eventListId") != null) {
                    urlParameters  = url_event_description + "eventId="
                            + mEventId + "&eventsListId="
                            + getIntent().getStringExtra("eventListId")
                            + "&hubCitiId=" + UrlRequestParams.getHubCityId()
                            + "&platform=" + Constants.PLATFORM;
                }
                else {
                    urlParameters  = url_event_description + "eventId="
                            + mEventId + "&hubCitiId=" + UrlRequestParams.getHubCityId()
                            + "&platform=" + Constants.PLATFORM;
                }

                JSONObject jsonResponse = mServerConnections
                        .getUrlPostResponseWithHtmlContent(urlParameters, "",
                                false);

                return jsonParseEventDetailResponse(jsonResponse);

            } catch (Exception e) {
                e.printStackTrace();
                returnValue = "UNSUCCESS";
                return returnValue;
            }

        }

    }

    public String jsonParseEventDetailResponse(JSONObject jsonResponse) {
        Log.v("", "DATA FRM EVENT : " + jsonResponse);
        if (jsonResponse != null) {

            try {

                if (jsonResponse.has("EventDetails")) {

                    JSONObject jsonObject = jsonResponse
                            .getJSONObject("EventDetails");
                    eventDetails.put("eventName",
                            jsonObject.getString("eventName"));

                    if (jsonObject.has("startTime")) {
                        copyStartTime = jsonObject.getString("startTime");
                    }
                    if (jsonObject.has("endTime")) {
                        copyEndTime = jsonObject.getString("endTime");
                    }

                    if (jsonObject.has("eventEDate")) {
                        eventEDate = jsonObject.getString("eventEDate");
                    }
                    if (jsonObject.has("eventTimeHrs")) {
                        eventTimeHrs = jsonObject.getInt("eventTimeHrs");
                    }
                    if (jsonObject.has("eventSTimeHrs")) {
                        eventTimeHrs = jsonObject.getInt("eventSTimeHrs");
                    }

                    if (jsonObject.has("eventStartTimeHrs")) {
                        eventTimeHrs = jsonObject.getInt("eventStartTimeHrs");
                    }
                    if (jsonObject.has("eventTimeMins")) {
                        eventTimeMins = jsonObject.getInt("eventTimeMins");
                    }
                    if (jsonObject.has("eventSTimeMins")) {
                        eventTimeMins = jsonObject.getInt("eventSTimeMins");
                    }

                    if (jsonObject.has("eventStartTimeMins")) {
                        eventTimeMins = jsonObject.getInt("eventStartTimeMins");
                    }
                    if (jsonObject.has("eventETimeHrs")) {
                        eventETimeHrs = jsonObject.getInt("eventETimeHrs");
                    }

                    if (jsonObject.has("eventEndTimeHrs")) {
                        eventETimeHrs = jsonObject.getInt("eventEndTimeHrs");
                    }
                    if (jsonObject.has("eventETimeMins")) {
                        eventETimeMins = jsonObject.getInt("eventETimeMins");
                    }

                    if (jsonObject.has("eventEndTimeMins")) {
                        eventETimeMins = jsonObject.getInt("eventEndTimeMins");
                    }

                    if (jsonObject.has("recurrenceInterval")) {
                        recurrenceInterval = jsonObject
                                .getInt("recurrenceInterval");
                    }
                    if (jsonObject.has("recurrencePatternID")) {
                        recurrencePatternID = jsonObject
                                .getInt("recurrencePatternID");
                    }
                    if (jsonObject.has("isOngoing")) {
                        isOnGoing = jsonObject.getString("isOngoing");
                    }
                    if (jsonObject.has("isWeekDay")) {
                        isWeekDay = jsonObject.getBoolean("isWeekDay");
                    }
                    if (jsonObject.has("occurenceType")) {
                        occurenceType = jsonObject.getString("occurenceType");
                    }
                    if (jsonObject.has("endAfter")) {
                        endAfter = jsonObject.getInt("endAfter");
                    }
                    if (jsonObject.has("days")) {
                        days = jsonObject.getString("days");

                    }
                    if (jsonObject.has("dayOfMonth")) {
                        dayOfMonth = jsonObject.getInt("dayOfMonth");
                    }

                    if (jsonObject.has("byDayNumber")) {
                        byDayNumber = jsonObject.getBoolean("byDayNumber");
                    }
                    if (jsonObject.has("dateOfMonth")) {
                        dateOfMonth = jsonObject.getInt("dateOfMonth");
                    }

                    if (jsonObject.has("everyWeekDayMonth")) {
                        everyWeekDayMonth = jsonObject
                                .getString("everyWeekDayMonth");
                    }

                    if (jsonObject.has("dayNumber")) {
                        dayNumber = jsonObject.getInt("dayNumber");
                    }

                    if (jsonObject.has("shortDes")) {
                        eventDetails.put("shortDes",
                                jsonObject.getString("shortDes"));
                    }

                    if (jsonObject.has("longDes")) {
                        eventDetails.put("longDes",
                                jsonObject.getString("longDes"));
                    }
                    if (jsonObject.has("hubCitiId")) {
                        eventDetails.put("hubCitiId",
                                jsonObject.getString("hubCitiId"));
                    }
                    if (jsonObject.has("imgPath")) {
                        eventDetails.put("imgPath",
                                jsonObject.getString("imgPath"));
                    }
                    if (jsonObject.has("busEvent")) {
                        eventDetails.put("busEvent",
                                jsonObject.getString("busEvent"));
                    }
                    if (jsonObject.has("pkgEvent")) {
                        eventDetails.put("pkgEvent",
                                jsonObject.getString("pkgEvent"));
                    }
                    if (jsonObject.has("startDate")) {
                        eventDetails.put("startDate",
                                jsonObject.getString("startDate"));
                        eventStartDate = jsonObject.getString("startDate");
                    }
                    if (jsonObject.has("startTime")) {
                        eventDetails.put("startTime",
                                jsonObject.getString("startTime"));
                    }
                    if (jsonObject.has("endDate")) {
                        eventDetails.put("endDate",
                                jsonObject.getString("endDate"));
                    }
                    if (jsonObject.has("endTime")) {
                        eventDetails.put("endTime",
                                jsonObject.getString("endTime"));
                    }
                    if (jsonObject.has("mItemExist")) {
                        eventDetails.put("mItemExist",
                                jsonObject.getString("mItemExist"));
                    }
                    if (jsonObject.has("eventCatIds")) {
                        eventDetails.put("eventCatIds",
                                jsonObject.getString("eventCatIds"));
                    }

                    if (jsonObject.has("address")) {
                        eventDetails.put("address",
                                jsonObject.getString("address"));
                        copyAdress = jsonObject.getString("address");
                    }

                    // New Changes
                    if (jsonObject.has("address1")) {
                        eventDetails.put("address1",
                                jsonObject.getString("address1"));
                    }

                    if (jsonObject.has("address2")) {
                        eventDetails.put("address2",
                                jsonObject.getString("address2"));

                    }

                    if (jsonObject.has("hotelFlag")) {
                        eventDetails.put("hotelFlag",
                                jsonObject.getString("hotelFlag"));
                    }

                    if (jsonObject.has("moreInfoURL")) {
                        eventDetails.put("moreInfoURL",
                                jsonObject.getString("moreInfoURL"));
                    }

                    if (jsonObject.has("pkgTicketURL")) {
                        eventDetails.put("pkgTicketURL",
                                jsonObject.getString("pkgTicketURL"));
                    }
                    if (jsonObject.has("pkgDes")) {
                        eventDetails.put("pkgDes",
                                jsonObject.getString("pkgDes"));
                    }

                    if (jsonObject.has("address")) {
                        eventDetails.put("address",
                                jsonObject.getString("address"));
                    }

                    if (jsonObject.has("latitude")) {
                        eventDetails.put("latitude",
                                jsonObject.getString("latitude"));
                    }

                    if (jsonObject.has("longitude")) {
                        eventDetails.put("longitude",
                                jsonObject.getString("longitude"));
                    }

                    if (jsonObject.has("isAppSiteFlag")) {
                        isAppSiteFlag = jsonObject.getString("isAppSiteFlag");
                        eventDetails.put("isAppSiteFlag", isAppSiteFlag);
                    }

                    if (jsonObject.has("evtLocTitle")) {
                        evtLocTitle = jsonObject.getString("evtLocTitle");
                        eventDetails.put("evtLocTitle", evtLocTitle);
                    }

                    if (jsonObject.has("eventLgSSQRPath")) {
                        eventLgSSQRPath = jsonObject
                                .getString("eventLgSSQRPath");
                        eventDetails.put("eventLgSSQRPath", eventLgSSQRPath);
                    }

                    if (jsonObject.has("recurringDays")) {
                        eventDetails.put("recurringDays",
                                jsonObject.getString("recurringDays"));
                    }

                } else {

                    JSONObject jsonRecordNotFound = jsonResponse
                            .getJSONObject("response");
                    responseText = jsonRecordNotFound.getString("responseText");
                    returnValue = "UNSUCCESS";
                    return returnValue;

                }
            } catch (Exception ex) {

                ex.printStackTrace();
                cancelBackgroundProcess();
                finish();
            }

        }

        return null;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Constants.FINISHVALUE) {
            setResult(Constants.FINISHVALUE);
            cancelBackgroundProcess();
            finish();
        }
    }

    class calenderAsyncTask extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        protected void onPreExecute() {
            if (progDialog != null) {
                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }

                progDialog = null;

            }

            progDialog = new ProgressDialog(EventDescription.this);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage(Constants.DIALOG_MESSAGE);
            progDialog.setCancelable(false);
            progDialog.show();

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if (result.equalsIgnoreCase("success")) {

                    if (progDialog.isShowing()) {
                        progDialog.dismiss();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... arg0) {
            appsiteAdress = new LinkedList<>();

            try {

                // Change from GET to POST
                String url_event_description_appsites = Properties.url_local_server
                        + Properties.hubciti_version
                        + "alertevent/eventappsiteloc";
                String urlParameters = mUrlRequestParams
                        .getEventAppSiteLocParams(mEventId, latitude, longitude);
                JSONObject jsonResponse = mServerConnections
                        .getUrlPostResponse(url_event_description_appsites,
                                urlParameters, true);

                if (jsonResponse != null) {

                    if (jsonResponse.has("RetailersDetails")) {

                        JSONObject elem = jsonResponse
                                .getJSONObject("RetailersDetails");

                        if (elem.has("retList"))

                        {
                            JSONObject elem1 = elem.getJSONObject("retList");

                            if (elem1.has("Retailer")) {

                                boolean isArray = false;
                                isArray = isJSONArray(elem1, "Retailer");

                                if (isArray) {

                                    JSONArray arr = elem1
                                            .getJSONArray("Retailer");

                                    for (int index = 0; index < arr.length(); index++) {

                                        JSONObject obj = arr
                                                .getJSONObject(index);

                                        if (obj.has("appSiteName")) {

                                            appsiteAdress.add(obj
                                                    .getString("appSiteName"));

                                        }

                                    }

                                } else {

                                    JSONObject obj = elem1
                                            .getJSONObject("Retailer");

                                    if (obj.has("appSiteName")) {
                                        appsiteAdress.add(obj
                                                .getString("appSiteName"));

                                    }

                                }
                            }
                        }
                    } else if (jsonResponse.has("response")) {

                        JSONObject elem1 = jsonResponse
                                .getJSONObject("response");
                        responseText = elem1.getString("responseText");
                    }
                }

                return "success";

            } catch (Exception e) {
                e.printStackTrace();
                returnValue = "UNSUCCESS";
                return returnValue;
            }

        }

    }

    class AppSitesAsyncTask extends AsyncTask<String, Void, String> {

        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        protected void onPreExecute() {
            try {
                if (progDialog != null) {
                    if (progDialog.isShowing()) {
                        progDialog.dismiss();
                    }

                    progDialog = null;

                }

                progDialog = new ProgressDialog(EventDescription.this);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setMessage(Constants.DIALOG_MESSAGE);
                progDialog.setCancelable(false);
                progDialog.show();

                getGPSValues();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if ("sucessfull".equals(returnValue)) {

                    initiateLocationWindow(appSitetDetails);
                } else {
                    progDialog.dismiss();
                    Toast.makeText(EventDescription.this, responseText,
                            Toast.LENGTH_SHORT).show();
                }

                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... arg0) {
            appsiteAdress = new LinkedList<>();
            appSitetDetails = new ArrayList<>();
            try {

                // Change from GET to POST
                String url_event_description_appsites = Properties.url_local_server
                        + Properties.hubciti_version
                        + "alertevent/eventappsiteloc";
                String urlParameters = mUrlRequestParams
                        .getEventAppSiteLocParams(mEventId, latitude, longitude);
                JSONObject jsonResponse = mServerConnections
                        .getUrlPostResponse(url_event_description_appsites,
                                urlParameters, true);

                if (jsonResponse != null) {

                    if (jsonResponse.has("RetailersDetails")) {

                        JSONObject elem = jsonResponse
                                .getJSONObject("RetailersDetails");

                        if (elem.has("retList"))

                        {
                            JSONObject elem1 = elem.getJSONObject("retList");

                            if (elem1.has("Retailer")) {

                                boolean isArray = false;
                                isArray = isJSONArray(elem1, "Retailer");

                                if (isArray) {

                                    JSONArray arr = elem1
                                            .getJSONArray("Retailer");

                                    for (int index = 0; index < arr.length(); index++) {

                                        HashMap<String, String> appSite = new HashMap<>();

                                        JSONObject obj = arr
                                                .getJSONObject(index);

                                        if (obj.has("latitude")) {
                                            appSite.put("latitude",
                                                    obj.getString("latitude"));

                                        }

                                        if (obj.has("longitude")) {
                                            appSite.put("longitude",
                                                    obj.getString("longitude"));

                                        }

                                        if (obj.has("distance")) {
                                            appSite.put("distance",
                                                    obj.getString("distance"));

                                        }

                                        if (obj.has("logoImagePath")) {
                                            appSite.put("logoImagePath", obj
                                                    .getString("logoImagePath"));

                                        }

                                        if (obj.has("appSiteId")) {
                                            appSite.put("appSiteId",
                                                    obj.getString("appSiteId"));

                                        }

                                        if (obj.has("appSiteName")) {
                                            appSite.put("appSiteName", obj
                                                    .getString("appSiteName"));
                                            appsiteAdress.add(obj
                                                    .getString("appSiteName"));

                                        }

                                        if (obj.has("address")) {
                                            appSite.put("address",
                                                    obj.getString("address"));

                                        }

                                        // @Rekha: Appsite data
                                        if (obj.has(CommonConstants.TAG_RETAIL_ID)) {
                                            appSite.put(
                                                    CommonConstants.TAG_RETAIL_ID,
                                                    obj.getString(CommonConstants.TAG_RETAIL_ID));
                                        }

                                        if (obj.has("retailLocationId")) {
                                            appSite.put(
                                                    "retailLocationId",
                                                    obj.getString("retailLocationId"));
                                        }
                                        if (obj.has(CommonConstants.TAG_RETAIL_LIST_ID)) {
                                            appSite.put(
                                                    CommonConstants.TAG_RETAIL_LIST_ID,
                                                    obj.getString(CommonConstants.TAG_RETAIL_LIST_ID));
                                        }
                                        appSitetDetails.add(appSite);

                                    }

                                } else {

                                    HashMap<String, String> appSite = new HashMap<>();
                                    JSONObject obj = elem1
                                            .getJSONObject("Retailer");

                                    if (obj.has("latitude")) {
                                        appSite.put("latitude",
                                                obj.getString("latitude"));

                                    }

                                    if (obj.has("longitude")) {
                                        appSite.put("longitude",
                                                obj.getString("longitude"));

                                    }

                                    if (obj.has("distance")) {
                                        appSite.put("distance",
                                                obj.getString("distance"));

                                    }

                                    if (obj.has("logoImagePath")) {
                                        appSite.put("logoImagePath",
                                                obj.getString("logoImagePath"));

                                    }

                                    if (obj.has("appSiteId")) {
                                        appSite.put("appSiteId",
                                                obj.getString("appSiteId"));

                                    }

                                    if (obj.has("appSiteName")) {
                                        appSite.put("appSiteName",
                                                obj.getString("appSiteName"));

                                    }

                                    if (obj.has("address")) {
                                        appSite.put("address",
                                                obj.getString("address"));

                                    }
                                    // @Rekha: Appsite data
                                    if (obj.has(CommonConstants.TAG_RETAIL_ID)) {
                                        appSite.put(
                                                CommonConstants.TAG_RETAIL_ID,
                                                obj.getString(CommonConstants.TAG_RETAIL_ID));
                                    }

                                    if (obj.has("retailLocationId")) {
                                        appSite.put("retailLocationId", obj
                                                .getString("retailLocationId"));
                                    }

                                    if (obj.has(CommonConstants.TAG_RETAIL_LIST_ID)) {
                                        appSite.put(
                                                CommonConstants.TAG_RETAIL_LIST_ID,
                                                obj.getString(CommonConstants.TAG_RETAIL_LIST_ID));
                                    }

                                    appSitetDetails.add(appSite);
                                }
                            }
                        }
                    } else if (jsonResponse.has("response")) {

                        JSONObject elem1 = jsonResponse
                                .getJSONObject("response");
                        responseText = elem1.getString("responseText");
                    }
                }

                return null;

            } catch (Exception e) {
                e.printStackTrace();
                returnValue = "UNSUCCESS";
                return returnValue;
            }

        }

    }

    public boolean isJSONArray(JSONObject jsonObject, String value) {
        boolean isArray = false;

        JSONObject isJSONObject = jsonObject.optJSONObject(value);
        if (isJSONObject == null) {
            JSONArray isJSONArray = jsonObject.optJSONArray(value);
            if (isJSONArray != null) {
                isArray = true;
            }
        }
        return isArray;
    }

    private void initiateLocationWindow(
            final ArrayList<HashMap<String, String>> listData) {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) EventDescription.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater
                    .inflate(
                            R.layout.events_location_popup,
                            (ViewGroup) findViewById(R.id.events_location_popup_element));

            DisplayMetrics metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int screenWidth = metrics.widthPixels;
            layout.setBackgroundColor(Color.WHITE);

            pwindo = new PopupWindow(layout, screenWidth - 50,
                    ViewGroup.LayoutParams.WRAP_CONTENT, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            if (!"N/A".equalsIgnoreCase(evtLocTitle) && evtLocTitle != null
                    && !"".equals(evtLocTitle)) {
                TextView eventName = (TextView) layout
                        .findViewById(R.id.events_location_popup_title);

                eventName.setText(evtLocTitle);
            }

            ListView mLocationList = (ListView) layout
                    .findViewById(R.id.events_location_listview);

            EventsAppSiteArrayAdapter adapter = null;
            if (popUplistData == null) {
                popUplistData = listData;
            }

            adapter = new EventsAppSiteArrayAdapter(EventDescription.this,
                    popUplistData, isAppSiteFlag, false);

            mLocationList.setAdapter(adapter);
            btnCloseLocPopup = (Button) layout
                    .findViewById(R.id.btn_location_close_popup);
            btnCloseLocPopup.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });

            mLocationList.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int pos, long arg3) {

                    String lat = null;
                    String lon = null;

                    if ("0".equals(isAppSiteFlag)) {
                        if (listData.get(pos).get("latitude") != null) {
                            lat = listData.get(pos).get("latitude");
                        }

                        if (listData.get(pos).get("longitude") != null) {
                            lon = listData.get(pos).get("longitude");
                        }

                        String addr1 = "", addr2 = "";
                        StringBuffer addr = new StringBuffer();

                        if (listData.get(pos).get("address1") != null) {
                            addr1 = listData.get(pos).get("address1");
                        }

                        if (listData.get(pos).get("address2") != null) {
                            addr2 = listData.get(pos).get("address2");
                        }

                        if (addr1 != null && !"".equals(addr1)) {
                            addr.append(addr1);
                            addr.append(" ,");
                        }

                        if (addr2 != null && !"".equals(addr2)) {
                            addr.append(addr2);
                        }

                        MapDirections mapDirections = new MapDirections(
                                locationManager, activity, addr.toString(),
                                lat, lon, "");
                        mapDirections.getMapDirections();

                        pwindo.dismiss();

                    } else if ("1".equals(isAppSiteFlag)) {
                        // @Rekha: If isAppSiteFlag is "1" pull up Appsite
                        Intent intent = new Intent(EventDescription.this,
                                RetailerActivity.class);

                        if (null != listData.get(pos).get(
                                CommonConstants.TAG_RETAIL_ID)) {
                            intent.putExtra(
                                    CommonConstants.TAG_RETAIL_ID,
                                    listData.get(pos).get(
                                            CommonConstants.TAG_RETAIL_ID));
                        }

                        if (null != listData.get(pos).get(
                                CommonConstants.TAG_RETAIL_LIST_ID)) {
                            intent.putExtra(
                                    CommonConstants.TAG_RETAIL_LIST_ID,
                                    listData.get(pos).get(
                                            CommonConstants.TAG_RETAIL_LIST_ID));
                        }
                        if (null != listData.get(pos).get("retailLocationId")) {
                            intent.putExtra(Constants.TAG_RETAILE_LOCATIONID,
                                    listData.get(pos).get("retailLocationId"));
                        }
                        if (null != listData.get(pos).get("appSiteId")) {
                            intent.putExtra("appSiteId",
                                    listData.get(pos).get("appSiteId"));
                        }
                        if (null != listData.get(pos).get("appSiteName")) {
                            intent.putExtra(Constants.TAG_APPSITE_NAME,
                                    listData.get(pos).get("appSiteName"));
                        }

                        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                                mItemId);
                        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                                bottomBtnId);

                        startActivity(intent);
                        pwindo.dismiss();

                    }

                    pwindo.dismiss();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    public void getGPSValues() {
        String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                latitude = String.valueOf(locNew.getLatitude());
                longitude = String.valueOf(locNew.getLongitude());

            } else {
                // N/W Tower Info Start
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());
                } else {
                    latitude = CommonConstants.LATITUDE;
                    longitude = CommonConstants.LONGITUDE;
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelBackgroundProcess();
    }

    private void cancelBackgroundProcess() {
        if (eventsAsyncTask != null && !eventsAsyncTask.isCancelled()) {
            eventsAsyncTask.cancel(true);
        }

        eventsAsyncTask = null;

        if (appsiteAsyncTask != null && !appsiteAsyncTask.isCancelled()) {
            appsiteAsyncTask.cancel(true);
        }

        appsiteAsyncTask = null;

    }

    /**
     * Customized WebClient to handle Email links, HTTP links and Anchor Tags
     *
     * @author rekha_p
     */
    public class MyWebViewClient extends WebViewClient {
        private final WeakReference<Activity> mActivityRef;
        boolean flag = false;

        public MyWebViewClient(Activity activity) {
            mActivityRef = new WeakReference<>(activity);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // Handles Email Link
            if (url.startsWith("mailto:")) {
                final Activity activity = mActivityRef.get();
                if (activity != null) {
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(activity, mt.getTo(),
                            mt.getSubject(), mt.getBody(), mt.getCc());
                    activity.startActivity(i);
                    view.reload();
                    return true;
                }
            } else if (url != null
                    && (url.startsWith("http://") || url.startsWith("https://"))) {
                // Handles HTTP links (Opens link in device browser)
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // Handles Anchor Tags
            if (url.contains("#") && !flag) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        longDescription.loadData(
                                String.format(htmlcode, longDesc), "text/html",
                                "utf-8");
                    }
                }, 400);

                flag = true;
            } else {
                flag = false;
            }

        }

        /**
         * Intent to open Email Composer screen
         *
         * @param context
         * @param address
         * @param subject
         * @param body
         * @param cc
         * @return
         */
        private Intent newEmailIntent(Context context, String address,
                                      String subject, String body, String cc) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
            intent.putExtra(Intent.EXTRA_TEXT, body);
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_CC, cc);
            intent.setType("message/rfc822");
            return intent;
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebViewSettings(WebView webView) {
        webView.setVerticalScrollBarEnabled(true);
        webView.setLayerType(View.LAYER_TYPE_NONE, null);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.setWebViewClient(new MyWebViewClient(EventDescription.this));

        webView.setBackgroundColor(Color.parseColor("#FFFFFF"));

        // Optimize loading times.
        webSettings.setAppCacheEnabled(false);
        webSettings.setBlockNetworkImage(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setGeolocationEnabled(false);
        webSettings.setNeedInitialFocus(false);
        webSettings.setSaveFormData(false);

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        if (EventsListDisplay.eventIDs != null
                && !EventsListDisplay.eventIDs.isEmpty()) {
            EventsListDisplay.eventIDs
                    .remove(EventsListDisplay.eventIDs.size() - 1);
            cancelBackgroundProcess();
            finish();
        }

    }

}