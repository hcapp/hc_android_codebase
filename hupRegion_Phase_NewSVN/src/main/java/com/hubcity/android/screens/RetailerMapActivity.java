package com.hubcity.android.screens;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;

public class RetailerMapActivity extends CustomTitleBar implements
		LocationSource, LocationSource.OnLocationChangedListener {
	public static final String LOG_TAG = RetailerMapActivity.class.getName();
	private GoogleMap mMap;
	UiSettings mUiSettings;
	private Builder latLngBounds, gpsBounds;
	String retailerName, retailerAddress;
	double latitude, longitude;
	Double mapLng, mapLat;
	ArrayList<LatLng> directionSteps = new ArrayList<>();
	private OnLocationChangedListener mListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.retailer_map);
			mapLat = (getIntent().getExtras().getDouble("mapLat"));
			mapLng = (getIntent().getExtras().getDouble("mapLng"));
			retailerAddress = (getIntent().getExtras()
                    .getString(CommonConstants.TAG_ADDRESS1));
			retailerName = (getIntent().getExtras()
                    .getString(CommonConstants.TAG_RETAILE_NAME));

			title.setSingleLine(false);
			title.setMaxLines(2);
			title.setEllipsize(TextUtils.TruncateAt.END);
			title.setText(retailerName);
			leftTitleImage.setVisibility(View.GONE);

			initilizeMap();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * function to load map. If map is not created it will create it for you
	 */
	private void initilizeMap() {
		if (mMap == null) {

			mMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();

			if (mMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();

			} else {

				// create marker
				MarkerOptions marker = new MarkerOptions().position(
						new LatLng(mapLat, mapLng)).title(
						retailerName + "," + retailerAddress);

				// adding marker
				mMap.addMarker(marker);

				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
						mapLat, mapLng), 14));

			}

		}
	}

	/**
	 * This is where we can add markers or lines, add listeners or move the
	 * camera. In this case, we just add a marker near Africa.
	 * <p>
	 * This should only be called once and when we are sure that {@link #mMap}
	 * is not null.
	 */
	@SuppressWarnings("deprecation")
	void setUpMap() {
		mMap.addMarker(new MarkerOptions().position(new LatLng(mapLat, mapLng))
				.title(retailerName).snippet(retailerAddress)
				.icon(BitmapDescriptorFactory.defaultMarker(0)));
		gpsBounds = LatLngBounds.builder();
		gpsBounds.include(new LatLng(mapLat, mapLng));

		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		final String provider = Settings.Secure.getString(getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if (provider.contains("gps")) {
			mMap.setMyLocationEnabled(true);
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
					CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
					new ScanSeeLocListener());
			Location locNew = locationManager
					.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (locNew != null) {

				latitude = locNew.getLatitude();
				longitude = locNew.getLongitude();

			} else {

				latitude = Double.valueOf(CommonConstants.LATITUDE);
				longitude = Double.valueOf(CommonConstants.LONGITUDE);

			}
			mMap.addMarker(new MarkerOptions()
					.position(new LatLng(latitude, longitude))
					.title("Start Location")
					.icon(BitmapDescriptorFactory.defaultMarker(120)));
			gpsBounds.include(new LatLng(latitude, longitude));
			new GetDirections().execute();
		}
		mMap.setOnCameraChangeListener(new OnCameraChangeListener() {
			@Override
			public void onCameraChange(CameraPosition arg0) {

				if (provider.contains("gps")) {
					mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
							gpsBounds.build(), 75));
				} else {
					mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
							new LatLng(mapLat, mapLng), 15f));
				}

				mMap.setOnCameraChangeListener(null);
			}
		});
	}

	private class GetDirections extends AsyncTask<String, Void, Boolean> {
		JSONObject jsonObject = null;
		JSONArray jsonArray = null;
		private ProgressDialog mDialog;

		@Override
		protected void onPostExecute(Boolean result) {
			try {
				mDialog.dismiss();
				if (result && !directionSteps.isEmpty()) {
                    // Instantiates a new Polyline object and adds points to define
                    // a rectangle
                    PolylineOptions rectOptions = new PolylineOptions()
                            .geodesic(true);
                    // add points
                    rectOptions.addAll(directionSteps);
                    // Set the rectangle's color to red
                    rectOptions.color(Color.RED);

                    // Get back the mutable Polyline
                    latLngBounds = LatLngBounds.builder();
                    for (int i = 0; i < directionSteps.size(); i++) {
                        if (directionSteps.get(i) != null) {
                            latLngBounds.include(directionSteps.get(i));
                        }
                    }
                    mMap.setOnCameraChangeListener(new OnCameraChangeListener() {

                        @Override
                        public void onCameraChange(CameraPosition arg0) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
                                    latLngBounds.build(), 50));
                            // Remove listener to prevent position reset on camera
                            // move.

                            mMap.setOnCameraChangeListener(null);

                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), "No Directions Found",
                            Toast.LENGTH_SHORT).show();
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(RetailerMapActivity.this, "",
					"Fetching Directions.. ", true);
			mDialog.setCancelable(false);
		}

		@Override
		protected Boolean doInBackground(String... arg0) {
			boolean result = false;
			try {
				directionSteps = new ArrayList<>();
				if (jsonObject != null
						&& "OK".equalsIgnoreCase(jsonObject.getString("status"))) {
					if (jsonObject.getJSONArray("routes").length() > 0) {
						jsonArray = jsonObject.getJSONArray("routes")
								.getJSONObject(0).getJSONArray("legs")
								.getJSONObject(0).getJSONArray("steps");
						if (jsonArray.length() > 0) {
							directionSteps.add(new LatLng(latitude, longitude));

							directionSteps.add(new LatLng(mapLat, mapLng));
							result = true;
						} else {
							result = false;
						}

					}
				} else {
					result = false;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		mListener.onLocationChanged(location);

	}

	@Override
	public void activate(OnLocationChangedListener listener) {
		mListener = listener;
	}

	@Override
	public void deactivate() {
		mListener = null;
	}

}
