package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class TemplateListMenuScreen extends MenuPropertiesActivity {
    LinearLayout parentIntermideate;
    CustomImageLoader customImageLoader;
    boolean isResumed;
    ImageView mBannerImageView;
    private ProgressBar progressBar;
    RelativeLayout bannerHolder;
    private Context mContext;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = TemplateListMenuScreen.this;
        CommonConstants.hamburgerIsFirst = true;
        customImageLoader = new CustomImageLoader(getApplicationContext(), false);
        Intent intent = getIntent();
        previousMenuLevel = intent.getExtras().getString(
                Constants.MENU_LEVEL_INTENT_EXTRA);


        parent = new RelativeLayout(this);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        parent.setLayoutParams(param);

        parentIntermideate = new LinearLayout(this);
        parentIntermideate.setOrientation(LinearLayout.VERTICAL);

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        menuListTemplate = layoutInflater.inflate(R.layout.menu_list_template,
                null, false);
        // Initializing drawer layout
        drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);
        LinearLayout.LayoutParams menuListTemplateParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        BottomButtonListSingleton mBottomButtonListSingleton;
        mBottomButtonListSingleton = BottomButtonListSingleton
                .getListBottomButton();
        if (mBottomButtonListSingleton != null) {

            ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                    .listBottomButton;

            if (listBottomButton != null && listBottomButton.size() > 1) {
                int px = (int) (50 * this.getResources().getDisplayMetrics().density + 0.5f);
                menuListTemplateParam.setMargins(0, 0, 0, px);
            }
        }
        menuListTemplate.setLayoutParams(menuListTemplateParam);

        LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        parentIntermideate.setLayoutParams(parentParam);
        addTitleBar(parentIntermideate, parent);
        // adding banner image

        mBannerImageView = (ImageView) menuListTemplate
                .findViewById(R.id.top_image);
        progressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
        bannerHolder = (RelativeLayout) menuListTemplate.findViewById(R.id.banner_holder);
        bannerParent = (LinearLayout)menuListTemplate. findViewById(R.id.bannerParent);

        parentIntermideate.addView(menuListTemplate);

        parent.addView(parentIntermideate);

        createbottomButtontTab(parent);

        setContentView(parent);

        handleIntent(intent);
        backgroundSetting();

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            HubCityContext.level = 0;
        } else {
            HubCityContext.level = HubCityContext.level + 1;
        }
        setBannerOrTicker();
    }


    private void backgroundSetting() {

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            if (mBkgrdColor != null && !"N/A".equals(mBkgrdColor)) {
                menuListTemplate.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(mBkgrdColor));

            } else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
                new ImageLoaderAsync1(menuListTemplate).execute(mBkgrdImage);

            }

        } else {
            if (smBkgrdColor != null && !"N/A".equals(smBkgrdColor)) {
                menuListTemplate.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(smBkgrdColor));


            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {
                new ImageLoaderAsync1(menuListTemplate).execute(smBkgrdImage);

            }

        }

    }

    private void setBannerOrTicker() {
        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if (mBannerImg != null) {
                mBannerImageView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(TemplateListMenuScreen.this).load(mBannerImg).into(mBannerImageView, new
                        Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        } else {
            customHorizontalTicker = (CustomHorizontalTicker)menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker)menuListTemplate. findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher)menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext, customHorizontalTicker, customVerticalTicker, bannerParent, customRotate, bannerHolder);
        }

    }


    @Override
    protected void onPause() {

        menuListTemplate.setBackgroundColor(Color.parseColor("#ffffff"));
        parent.setBackgroundColor(Color.parseColor("#ffffff"));
        super.onPause();
    }

    class ImageLoaderAsync1 extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync1(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {

            mDialog = ProgressDialog.show(TemplateListMenuScreen.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {

                e.printStackTrace();

            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            BitmapDrawable background = new BitmapDrawable(bmImg);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                bmImage.setBackgroundDrawable(background);
            } else {
                bmImage.setBackgroundDrawable(background);
            }

            try {

                if ((this.mDialog != null) && this.mDialog.isShowing()) {
                    this.mDialog.dismiss();
                }
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                this.mDialog = null;
            }

        }
    }

    protected void createListView(ArrayList<MainMenuBO> list,
                                  BaseAdapter adapter) {

        if (list != null) {

            menuList = (ListView) menuListTemplate.findViewById(R.id.menu_list);
            menuList.setAdapter(adapter);

        } else {

        }
    }

    protected ListView menuList;
    protected ArrayList<MainMenuBO> mlList;
    private TemplateListMenuRowAdapter adapter;
    private View menuListTemplate;

    protected static final String DEBUG_TAG = "TemplateListMenuScreen";
    private ArrayList<MainMenuBO> mainMenuUnsortedList;
    private String mBkgrdColor;

    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);

            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");

            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");

            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");

            mBkgrdImage = intent.getExtras().getString("smBkgrdImage");

            departFlag = intent.getExtras().getString("departFlag");

            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");

            setDataAdapter(mainMenuUnsortedList);
            adapter.notifyDataSetChanged();
        }
        if (mainMenuUnsortedList != null) {

        }

        super.handleIntent(intent);

    }

    @Override
    protected void onNewIntent(Intent intent) {

        if (mainMenuUnsortedList != null) {

        }

        handleIntent(intent);
        super.onNewIntent(intent);
    }

    protected void setDataAdapter(ArrayList<MainMenuBO> mainMenuUnsortedList) {

        adapter = new TemplateListMenuRowAdapter(this, mainMenuUnsortedList,
                previousMenuLevel);
        createListView(mainMenuUnsortedList, adapter);
        menuList = (ListView) menuListTemplate.findViewById(R.id.menu_list);
        menuList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                try {
                    onTemplateListMenuRowItemClick(arg0, arg1, arg2, arg3);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });
    }

    private void onTemplateListMenuRowItemClick(final AdapterView<?> arg0,
                                                final View view, final int position, final long id) {
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);
        mMainMenuBO.setmBkgrdColor(mBkgrdColor);
        mMainMenuBO.setmBkgrdImage(mBkgrdImage);
        mMainMenuBO.setSmBkgrdColor(smBkgrdColor);
        linkClickListener(mMainMenuBO.getLinkTypeName(), mMainMenuBO);
    }

    @Override
    protected void onResume() {
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }

        // handleIntent(getIntent());
        adapter.notifyDataSetChanged();
//        setupMainMenuContents(parent);

        backgroundSetting();


        activity = this;
        super.onResume();
    }

}

class TemplateListMenuRowAdapter extends BaseAdapter {
    Activity mContext;
    ArrayList<MainMenuBO> mlist;
    String level;
    String btnFontColor, btnColor;

    private CustomImageLoader customImageLoader;

    TemplateListMenuRowAdapter(Activity context, ArrayList<MainMenuBO> list,
                               String menuLevel) {
        mContext = context;
        mlist = list;
        level = menuLevel;
        customImageLoader = new CustomImageLoader(context, false);
    }

    static class ViewHolder {
        ImageView icon;
        ImageView disclosureIcon;
        TextView text;
        LinearLayout menuListTemplateParent;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater
                    .inflate(R.layout.template_list_menu_row, parent, false);
            holder = new ViewHolder();
            holder.icon = (ImageView) view
                    .findViewById(R.id.imageView_row_icon);
            holder.disclosureIcon = (ImageView) view
                    .findViewById(R.id.imageView_disclosure);
            holder.text = (TextView) view
                    .findViewById(R.id.tv_row_header);

            holder.menuListTemplateParent = (LinearLayout) view
                    .findViewById(R.id.menu_list_template_parent);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        MainMenuBO item = mlist.get(position);

        String itmItemImgUrl = item.getmItemImgUrl();
        String itemName = item.getmItemName();

        holder.icon.setTag(itmItemImgUrl);
        customImageLoader.displayImage(itmItemImgUrl, mContext, holder.icon);

        holder.text.setText(itemName);

        if (level != null && "1".equals(level)) {
            btnFontColor = item.getmBtnFontColor();
            btnColor = item.getmBtnColor();

        } else {
            btnFontColor = item.getSmBtnFontColor();
            btnColor = item.getSmBtnColor();
        }
        if (btnFontColor != null && !"N/A".equals(btnFontColor)) {
            holder.text.setTextColor(Color.parseColor(btnFontColor));

        } else {
            holder.text.setTextColor(Color.WHITE);
        }
        if (btnColor != null && !"N/A".equals(btnColor)) {
            holder.menuListTemplateParent.setBackgroundColor(Color
                    .parseColor(btnColor));
        } else {
            holder.menuListTemplateParent.setBackgroundColor(Color.BLACK);
        }

        return view;

    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}