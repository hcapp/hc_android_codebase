package com.hubcity.android.screens;

/**
 * Created by subramanya.v on 8/23/2016.
 */
public class RetDetailList
{
	private String claimTxtMsg;
	private String claimCategory;
	private String claimCategoryURL;

	public String getClaimCategory()
	{
		return claimCategory;
	}

	public void setClaimCategory(String claimCategory)
	{
		this.claimCategory = claimCategory;
	}

	public String getClaimCategoryURL()
	{
		return claimCategoryURL;
	}

	public void setClaimCategoryURL(String claimCategoryURL)
	{
		this.claimCategoryURL = claimCategoryURL;
	}

	public String getClaimTxtMsg()
	{
		return claimTxtMsg;
	}

	public void setClaimTxtMsg(String claimTxtMsg)
	{
		this.claimTxtMsg = claimTxtMsg;
	}


}
