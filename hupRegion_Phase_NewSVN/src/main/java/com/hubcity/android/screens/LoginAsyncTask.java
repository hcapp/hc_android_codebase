package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.WindowManager;

import com.artifex.mupdflib.AsyncTask;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CityDataHelper;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by supriya.m on 7/6/2016.
 */
public class LoginAsyncTask extends AsyncTask<String, Void, Boolean> {
    UrlRequestParams mUrlRequestParams = new UrlRequestParams();
    ServerConnections mServerConnections = new ServerConnections();
    Context mContext;
    ProgressDialog progDialog;
    boolean isShowProgress;
    private Hashtable<String, String> logInResponseData;
    SharedPreferences settings;
    Boolean mStateChecked, guestLogin, isFirst;
    String strPushMsg, userName, password, templateName;
    int iPushFlag = 0, iGuestUserPush = 0;

    public LoginAsyncTask(Context context, boolean isShowProgress, boolean guestLogin, String
            strPushMsg, int iPushFlag, int iGuestUserPush, boolean isFirst, String templateName) {
        this.mContext = context;
        this.isShowProgress = isShowProgress;
        settings = mContext.getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);
        this.guestLogin = guestLogin;
        this.strPushMsg = strPushMsg;
        this.iPushFlag = iPushFlag;
        this.iGuestUserPush = iGuestUserPush;
        this.isFirst = isFirst;
        this.templateName = templateName;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isShowProgress) {
            ((Activity) mContext).getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            progDialog = new ProgressDialog(mContext);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setMessage("Please Wait..");
            progDialog.setCancelable(false);
            progDialog.show();
        }
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try {
            String url_log_in = Properties.url_local_server
                    + Properties.hubciti_version + "firstuse/v2/getuserlogin";

            userName = params[0];
            password = params[1];
            String urlParameters = mUrlRequestParams
                    .createLogInUrlParameter(userName, password);

            JSONObject xmlResponde = mServerConnections.getUrlPostResponse(
                    url_log_in, urlParameters, true);
            return jsonParseLoginResponse(xmlResponde);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        if (aBoolean) {
            Constants.bISLoginStatus = true;
            if (logInResponseData != null) {
                if (!logInResponseData.get("userId").equals(
                        settings.getString(
                                Constants.PREFERENCE_KEY_TEMP_U_ID, ""))) {
                    mContext.deleteDatabase("HupCity.db");
                    MenuAsyncTask.dateModified = "";
                }
                mStateChecked = settings.getBoolean(Constants.REMEMBER,
                        false);
                if (mStateChecked) {
                    saveLoginData(mStateChecked);
                }
                hideKeyboard();

                if (strPushMsg != null && iPushFlag == 1
                        && iGuestUserPush != 1) {
                    Constants.startPushList(strPushMsg, mContext);
                } else {
                    //Added by sharanamma
                    checkTemplateType();
                }

            }
        } else {
            Constants.bISLoginStatus = false;
            if (logInResponseData != null) {

                logInResponseData.get("responseCode");
                String responseText = logInResponseData.get("responseText");
                if (isShowProgress) {
                    progDialog.dismiss();
                    ((LoginScreen) mContext).createAlert(responseText);
                } else {
                    saveLoginData(false);
                    startLoginScreen();
                }
            }
        }
    }

    private void startLoginScreen() {
        Intent intent = new Intent(mContext, LoginScreen.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (((Activity) mContext).getIntent().getStringExtra(Constants.PUSH_NOTIFY_MSG) != null) {
            intent.putExtra("pushtitle",
                    ((Activity) mContext).getIntent().getStringExtra("pushtitle"));
            intent.putExtra(Constants.PUSH_NOTIFY_MSG, ((Activity) mContext).getIntent()
                    .getStringExtra(Constants.PUSH_NOTIFY_MSG));
        }

        intent.putExtra("chkloc", true);
        ((Activity) mContext).finish();
        (mContext).startActivity(intent);
    }

    //Added by sharanamma
    private void checkTemplateType() {
        if (templateName != null && !templateName.isEmpty()) {
            SubMenuStack.clearSubMenuStack();
            MainMenuBO mainMenuBO = new MainMenuBO(null, null, null, null, 0,
                    null, null, null, null, null, null, "1", null,
                    null, null,
                    null, null, null, null);
            SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
            switch (templateName) {
                case Constants.COMBINATION:
                    mContext.startActivity(new Intent(mContext, CombinationTemplate.class));
                    ((Activity) mContext).finish();
                    break;
                case Constants.SCROLLING:
                    mContext.startActivity(new Intent(mContext, ScrollingPageActivity.class));
                    ((Activity) mContext).finish();
                    break;
                case Constants.NEWS_TILE:
                    mContext.startActivity(new Intent(mContext, TwoTileNewsTemplateActivity
                            .class));
                    ((Activity) mContext).finish();
                    break;
            }
        } else {
            if (GlobalConstants.isFromNewsTemplate) {
                SubMenuStack.clearSubMenuStack();
                MainMenuBO mainMenuBO = new MainMenuBO(null, null, null, null, 0,
                        null, null, null, null, null, null, "1", null,
                        null, null,
                        null, null, null, null);
                SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
                if (GlobalConstants.className != null && GlobalConstants.className.equalsIgnoreCase
                        (Constants.SCROLLING)) {
                    mContext.startActivity(new Intent(mContext, ScrollingPageActivity.class));
                    ((Activity) mContext).finish();

                } else if (GlobalConstants.className != null && GlobalConstants.className
                        .equalsIgnoreCase(Constants.NEWS_TILE)) {
                    mContext.startActivity(new Intent(mContext, TwoTileNewsTemplateActivity
                            .class));
                    ((Activity) mContext).finish();

                } else {
                    mContext.startActivity(new Intent(mContext, CombinationTemplate.class));
                    ((Activity) mContext).finish();

                }
            } else {
                startMenuActivity();
            }
        }
    }

    public void startMenuActivity() {
        String level = "1";
        String mItemId = "0";
        String mLinkId = "0";
        SubMenuStack.clearSubMenuStack();
        MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null, null, 0,
                null, mLinkId, null, null, null, null, level, null, null, null,
                null, null, null, null);
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

        MenuAsyncTask menu = new MenuAsyncTask(isFirst, (Activity) mContext, "mainMenu");
        menu.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, level, mItemId,
                "0", "None", "0", "0");
    }

    public void hideKeyboard() {
        ((Activity) mContext).getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void saveLoginData(boolean state) {
        SharedPreferences.Editor prefEditor = settings.edit();
        prefEditor.putString(Constants.REMEMBER_NAME, null);
        prefEditor.putString(Constants.REMEMBER_PASSWORD, null);
        if (state) {
            prefEditor.putString(Constants.REMEMBER_NAME, userName.trim());
            prefEditor.putString(Constants.REMEMBER_PASSWORD, password.trim());
        } else {
            prefEditor.putString(Constants.REMEMBER_NAME, null);
            prefEditor.putString(Constants.REMEMBER_PASSWORD, null);
        }
        prefEditor.putBoolean(Constants.REMEMBER, state);
        prefEditor.apply();
    }

    public Boolean jsonParseLoginResponse(JSONObject xmlResponde) {
        JSONObject localJSONObject;
        logInResponseData = new Hashtable<>();

        if (xmlResponde == null) {
            return false;
        }
        try {

            if (xmlResponde.has(Constants.SUCCESSFULL_REGISTRATION_RESPONSE)) {

                localJSONObject = xmlResponde
                        .getJSONObject(Constants.SUCCESSFULL_REGISTRATION_RESPONSE);
                if (localJSONObject.has("postalCode")) {
                    String constantZipcode = localJSONObject.getString("postalCode");
                    Constants.setZipCode(constantZipcode);
                }

                String userId = localJSONObject.getString("userId");
                logInResponseData.put("userId", userId);
                String isHCActive = localJSONObject.getString("isHCActive"); // Indicates
                // Hub
                // Citi
                // active
                // or
                // not
                logInResponseData.put("isHCActive", isHCActive);
                String isPrefSet = localJSONObject.getString("isPrefSet"); // To
                // ask
                // user
                // to
                // user
                // preferences
                logInResponseData.put("isPrefSet", isPrefSet);
                String addDevToUser = localJSONObject.getString("addDevToUser"); // To
                // add
                // device,
                // for
                // push
                // notification
                // when
                // logged
                // through
                // new
                // device
                logInResponseData.put("addDevToUser", addDevToUser);

                try {
                    Constants.setNavigationBarValues(Constants.IS_TEMP_USER, localJSONObject
                            .getString
                                    (Constants.IS_TEMP_USER));
                    Constants.setNavigationBarValues(Constants.NO_EMAIL_COUNT, localJSONObject
                            .getString
                                    (Constants.NO_EMAIL_COUNT));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SharedPreferences preferences = mContext.getSharedPreferences(
                        Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
                SharedPreferences.Editor e = preferences.edit().putString(
                        Constants.PREFERENCE_KEY_U_ID, userId);
                e.apply();
                if (guestLogin) {
                    Constants.GuestLoginId = Constants.GuestLoginIdforSigup = userId;
                    preferences.edit().putBoolean(
                            Constants.PREFERENCE_IS_LOGIN, false).apply();
                } else {
                    Constants.GuestLoginId = Constants.GuestLoginIdforSigup = "";
                    guestLogin = false;
                    preferences.edit().putBoolean(
                            Constants.PREFERENCE_IS_LOGIN, true).apply();
                }

                //Added by sharanamma
                if (localJSONObject.has("cityList")) {
                    JSONArray cityArray = new JSONArray();
                    JSONObject cityObject = localJSONObject.optJSONObject("cityList");
                    if (cityObject != null && cityObject.has("City")) {
                        JSONObject isJSONObject = cityObject.optJSONObject("City");
                        if (isJSONObject == null) {
                            cityArray = cityObject.optJSONArray("City");
                            if (cityArray != null) {
                                new CityDataHelper().storePreferedCities((Activity) mContext,
                                        cityArray);
                            }
                        } else if (cityObject.optJSONObject("City") != null) {
                            cityArray.put(cityObject.optJSONObject("City"));
                            new CityDataHelper().storePreferedCities((Activity) mContext,
                                    cityArray);
                        }

                    }
                }

                return true;

            } else if (xmlResponde.has(Constants.ERROR_REGISTRATION_RESPONSE)) {

                localJSONObject = xmlResponde
                        .getJSONObject(Constants.ERROR_REGISTRATION_RESPONSE);

                String responseCode = localJSONObject.getString("responseCode");
                String responseText = localJSONObject.getString("responseText");

                logInResponseData.put("responseCode", responseCode);
                logInResponseData.put("responseText", responseText);

                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
