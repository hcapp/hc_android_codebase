package com.hubcity.android.screens;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.NewsDetailsActivity;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 10/1/2016.
 */
public class CustomVerticalTicker extends ScrollView implements View.OnClickListener {
    private final Context mContext;
    private int marqueeSpeed = 10;
    private int mTextHeight;
    private int fromYValue;
    private int toYValue;
    private int duration;
    private int sideMargin = 20;
    private int topMargin = 30;

    public LinearLayout tickrParent;
    @SuppressWarnings("CanBeFinal")
    private final Interpolator mInterpolator = new LinearInterpolator();
    private View tickerView;


    public CustomVerticalTicker(Context context) {
        super(context);
        this.mContext = context;
    }

    public CustomVerticalTicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        addView();
    }


    public void addView() {
        try {
            if (MenuAsyncTask.tickerMode != null) {
                if (MenuAsyncTask.tickerMode.equalsIgnoreCase(mContext.getString(R.string.scrolling))) {
                    if (!(MenuAsyncTask.tickerDirection.equalsIgnoreCase(mContext.getString(R.string.right))
                            || MenuAsyncTask.tickerDirection.equalsIgnoreCase(mContext.getString(R.string.left)))) {
                        LayoutInflater tickerInflater = (LayoutInflater) mContext
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                        tickerView = tickerInflater.inflate(R.layout.vertical_scroll,
                                null, false);
                        tickrParent = (LinearLayout) tickerView.findViewById(R.id.ticker_parent);
                        addTextView(tickrParent);
                        this.addView(tickerView);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addTextView(LinearLayout tickrParent) {
        LinearLayout.LayoutParams textParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParam.setMargins(sideMargin, topMargin, sideMargin, 0);
        if (MenuAsyncTask.tickerItem != null) {
            for (int position = 0; position < MenuAsyncTask.tickerItem.size(); position++) {
                TextView newsView = new TextView(mContext);
                newsView.setGravity(Gravity.CENTER_HORIZONTAL);
                newsView.setText(MenuAsyncTask.tickerItem.get(position).getTitle());
                newsView.setTextSize(16);
                newsView.setMaxLines(2);
                newsView.setEllipsize(TextUtils.TruncateAt.END);
                if (MenuAsyncTask.tickerTxtColor != null) {
                    newsView.setTextColor(Color.parseColor(MenuAsyncTask.tickerTxtColor));
                }
                newsView.setTypeface(Typeface.DEFAULT_BOLD);
                newsView.setOnClickListener(this);
                newsView.setTag(position);
                newsView.setLayoutParams(textParam);
                tickrParent.addView(newsView);

            }
        }
    }

    public void startTextAnimation(int screenHeight, boolean topToBottom) {
        try {
            tickrParent = (LinearLayout) tickerView.findViewById(R.id.ticker_parent);
            mTextHeight = tickrParent.getMeasuredHeight();

            final int childCount = tickrParent.getChildCount();
            if (mTextHeight < screenHeight) {
                LinearLayout.LayoutParams newsTextParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (childCount > 1) {
                    int viewSize = tickrParent.getChildAt(childCount - 1).getHeight();
                    int finalHeight = (screenHeight - mTextHeight) + viewSize  + topMargin;
                    newsTextParam.setMargins(sideMargin, topMargin, sideMargin, (finalHeight - topMargin - viewSize));
                    tickrParent.getChildAt(childCount - 1).setLayoutParams(newsTextParam);
                }
                if (childCount == 1) {
                    newsTextParam.setMargins(sideMargin, topMargin,sideMargin, (screenHeight - topMargin -tickrParent.getChildAt(0).getHeight()));
                    tickrParent.getChildAt(0).setLayoutParams(newsTextParam);
                }
            }

            duration = (screenHeight + mTextHeight) * marqueeSpeed;

            if (topToBottom) {
                fromYValue = -mTextHeight;
                toYValue = screenHeight;
            } else {
                fromYValue = screenHeight;
                toYValue = -mTextHeight;
            }
            ValueAnimator animator = ValueAnimator.ofFloat( fromYValue, toYValue);
            animator.setRepeatMode(ValueAnimator.RESTART);
            animator.setRepeatCount(ValueAnimator.INFINITE);
            animator.setInterpolator(mInterpolator);
            animator.setDuration(duration);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    for (int position = 0; position < childCount; position++) {
                        tickrParent.getChildAt(position).setTranslationY((float) animation.getAnimatedValue());
                    }
                }
            });
            animator.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        Intent ticker = new Intent(mContext,NewsDetailsActivity.class);
        ticker.putExtra("itemId",Integer.parseInt(MenuAsyncTask.tickerItem.get(position).getRowCount()));
        mContext.startActivity(ticker);
    }
}
