package com.hubcity.android.screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.android.CouponsDetailActivity;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShowMapMultipleLocations extends CustomTitleBar implements
        OnInfoWindowClickListener, LocationSource,
        LocationSource.OnLocationChangedListener {
    private GoogleMap mMap;
    private Builder latLngBounds, gpsBounds;
    String retailerName, retailerAddress;
    double latitude, longitude;
    Double mapLng, mapLat;
    ArrayList<LatLng> directionSteps = new ArrayList<>();
    private OnLocationChangedListener mListener;
    private ArrayList<RetailerBo> mRetailerList;
    boolean isEvent;
    EventDetailObj eventSummary;
    private ProgressDialog mapDialog;
    private boolean isCoupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.retailer_map);
            mRetailerList = (getIntent().getExtras()
                    .getParcelableArrayList("mRetailerList"));
            title.setSingleLine(false);
            title.setMaxLines(2);
            getIntentData();
            setClickListener();

            rightImage.setVisibility(View.VISIBLE);
            divider.setVisibility(View.GONE);
            leftTitleImage.setVisibility(View.GONE);

            if (eventSummary != null && !eventSummary.equals("")) {
                title.setText(eventSummary
                        .getBandName());
                isEvent = true;
                initilizeEventMap();

            } else {
                title.setText("Choose Your" + "\n" + "Location");
                initilizeMap();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickListener() {
        this.backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getIntentData() {
        eventSummary = (EventDetailObj) getIntent().getExtras().getSerializable("EventDetails");
        Intent intent = getIntent();
        if (intent.hasExtra("Events")) {
            if (intent.getExtras().getBoolean("Events")) {
                isEvent = true;
            }
        }
        if (intent.hasExtra("isCoupon")) {
            isCoupon = intent.getExtras().getBoolean("isCoupon");
        }

    }

    private void initilizeEventMap() {

        if (mMap == null) {

            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
                    R.id.map)).getMap();

            if (isEvent) {
                mapLat = Double.valueOf(eventSummary
                        .getLatitude());
                mapLng = Double.valueOf(eventSummary
                        .getLongitude());

                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(mapLat, mapLng)).title(eventSummary.
                        getEventName()).snippet(eventSummary.getAddress());

                mMap.addMarker(marker);

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        mapLat, mapLng), 14));

                if (mMap == null) {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                            .show();
                }

            } else {
                mapDialog = ProgressDialog.show(ShowMapMultipleLocations.this, "",
                        Constants.DIALOG_MESSAGE, true);
                mapDialog.setCancelable(false);

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                final LatLngBounds bounds;
                if (null != mRetailerList && !mRetailerList.isEmpty()) {

                    for (int i = 0; i < mRetailerList.size(); i++) {

                        if (mRetailerList.get(0).getLatitude() != null) {
                            mapLat = Double.valueOf(mRetailerList.get(0)
                                    .getLatitude());
                        }

                        if (mRetailerList.get(0).getLongitude() != null) {
                            mapLng = Double.valueOf(mRetailerList.get(0)
                                    .getLongitude());
                        }

                        String retAddress = "";
                        if (mRetailerList.get(i).getRetailAddress() != null
                                && !"".equals(mRetailerList.get(i)
                                .getRetailAddress())) {
                            retAddress += mRetailerList.get(i)
                                    .getRetailAddress();
                        } else if (mRetailerList.get(i).getCompleteAddress() != null
                                && !"".equals(mRetailerList.get(i)
                                .getCompleteAddress())) {
                            retAddress += mRetailerList.get(i)
                                    .getCompleteAddress();
                        } else {
                            retAddress = "";
                        }

                        if (null != mRetailerList.get(i)) {

                            // create marker

                            if (mRetailerList.get(i).getLatitude() != null
                                    && mRetailerList.get(i).getLongitude() != null) {
                                MarkerOptions marker = new MarkerOptions()
                                        .position(
                                                new LatLng(
                                                        Double.valueOf(mRetailerList
                                                                .get(i)
                                                                .getLatitude()),
                                                        Double.valueOf(mRetailerList
                                                                .get(i)
                                                                .getLongitude())))
                                        .title(mRetailerList.get(i)
                                                .getRetailerName()).snippet(retAddress);

                                mRetailerList.get(i).setMapMarkerId(
                                        mMap.addMarker(marker).getId());
                                builder.include(marker.getPosition());
                            }

                        }

                    }
                    bounds = builder.build();
                    final int padding = 100; // offset from edges of the map in pixels

                    mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                            mMap.moveCamera(cu);
                            if (mapDialog != null && mapDialog.isShowing()) {
                                mapDialog.dismiss();
                            }

                        }
                    });


                    // check if map is created successfully or not
                    if (mMap == null) {
                        Toast.makeText(getApplicationContext(),
                                "Sorry! unable to create maps",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }

        }

    }

    /**
     * function to load map. If map is not created it will create it for you
     */
    private void initilizeMap() {

        if (mMap == null) {

            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
                    R.id.map)).getMap();
            mMap.setOnInfoWindowClickListener(this);


            if (isEvent) {
                mapLat = Double.valueOf(getIntent().getExtras().getString(
                        "latitude"));
                mapLng = Double.valueOf(getIntent().getExtras().getString(
                        "longitude"));

                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(mapLat, mapLng)).title(
                        getIntent().getExtras().getString("address"));

                mMap.addMarker(marker);

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        mapLat, mapLng), 14));

                if (mMap == null) {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                            .show();
                }

            } else {
                mapDialog = ProgressDialog.show(ShowMapMultipleLocations.this, "",
                        Constants.DIALOG_MESSAGE, true);
                mapDialog.setCancelable(false);

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                final LatLngBounds bounds;
                if (null != mRetailerList && !mRetailerList.isEmpty()) {

                    for (int i = 0; i < mRetailerList.size(); i++) {

                        if (mRetailerList.get(0).getLatitude() != null) {
                            mapLat = Double.valueOf(mRetailerList.get(0)
                                    .getLatitude());
                        }

                        if (mRetailerList.get(0).getLongitude() != null) {
                            mapLng = Double.valueOf(mRetailerList.get(0)
                                    .getLongitude());
                        }

                        String retAddress = "";
                        if (mRetailerList.get(i).getRetailAddress() != null
                                && !"".equals(mRetailerList.get(i)
                                .getRetailAddress())) {
                            retAddress += mRetailerList.get(i)
                                    .getRetailAddress();
                        } else if (mRetailerList.get(i).getCompleteAddress() != null
                                && !"".equals(mRetailerList.get(i)
                                .getCompleteAddress())) {
                            retAddress += mRetailerList.get(i)
                                    .getCompleteAddress();
                        } else {
                            retAddress = "";
                        }

                        if (null != mRetailerList.get(i)) {

                            // create marker

                            if (mRetailerList.get(i).getLatitude() != null
                                    && mRetailerList.get(i).getLongitude() != null) {
                                MarkerOptions marker = new MarkerOptions()
                                        .position(
                                                new LatLng(
                                                        Double.valueOf(mRetailerList
                                                                .get(i)
                                                                .getLatitude()),
                                                        Double.valueOf(mRetailerList
                                                                .get(i)
                                                                .getLongitude())))
                                        .title(mRetailerList.get(i)
                                                .getRetailerName()).snippet(retAddress);

                                mRetailerList.get(i).setMapMarkerId(
                                        mMap.addMarker(marker).getId());
                                builder.include(marker.getPosition());
                            }

                        }

                    }
                    bounds = builder.build();
                    final int padding = 100; // offset from edges of the map in pixels

                    mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                            mMap.moveCamera(cu);
                            if (mapDialog != null && mapDialog.isShowing()) {
                                mapDialog.dismiss();
                            }
                        }
                    });

                }
                // check if map is created successfully or not
                if (mMap == null) {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! unable to create maps",
                            Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        if (!isEvent) {
            for (int i = 0; i < mRetailerList.size(); i++) {

                if (mRetailerList.get(i) != null
                        && marker.getId().equals(
                        mRetailerList.get(i).getMapMarkerId())) {
                    if (isCoupon) {
                        Intent mapDetailsIntent = new Intent(ShowMapMultipleLocations.this,
                                CouponsDetailActivity.class);
                        mapDetailsIntent.putExtra("couponId",
                                mRetailerList.get(i).getRetailerId());
                        startActivity(mapDetailsIntent);
                    } else {
                        Intent findlocationInfo = new Intent(
                                ShowMapMultipleLocations.this,
                                RetailerActivity.class);
                        findlocationInfo.putExtra(CommonConstants.TAG_RETAIL_ID,
                                mRetailerList.get(i).getRetailerId());
                        findlocationInfo.putExtra(
                                CommonConstants.TAG_RETAILE_LOCATIONID,
                                mRetailerList.get(i).getRetailLocationId());
                        findlocationInfo.putExtra(
                                CommonConstants.TAG_RETAIL_LIST_ID, mRetailerList
                                        .get(i).getRetListId());
                        findlocationInfo.putExtra(
                                CommonConstants.TAG_RETAILER_NAME, mRetailerList
                                        .get(i).getRetailerName());
                        findlocationInfo.putExtra(
                                CommonConstants.TAG_BANNER_IMAGE_PATH,
                                mRetailerList.get(i).getBannerAdImagePath());
                        findlocationInfo.putExtra(
                                CommonConstants.TAG_RIBBON_ADIMAGE_PATH,
                                mRetailerList.get(i).getRibbonAdImagePath());
                        findlocationInfo.putExtra(
                                CommonConstants.TAG_RIBBON_AD_URL, mRetailerList
                                        .get(i).getRibbonAdURL());
                        findlocationInfo.putExtra(CommonConstants.TAG_DISTANCE,
                                mRetailerList.get(i).getDistance());
                        startActivityForResult(findlocationInfo,
                                Constants.STARTVALUE);
                    }
                }

            }
        }

    }

    /**
     * This is where we can add markers or lines, add listeners or move the
     * camera. In this case, we just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap}
     * is not null.
     */
    @SuppressWarnings("deprecation")
    void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(mapLat, mapLng))
                .title(retailerName).snippet(retailerAddress)
                .icon(BitmapDescriptorFactory.defaultMarker(0)));
        gpsBounds = LatLngBounds.builder();
        gpsBounds.include(new LatLng(mapLat, mapLng));

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final String provider = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) {
            mMap.setMyLocationEnabled(true);
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locNew != null) {

                latitude = locNew.getLatitude();
                longitude = locNew.getLongitude();

            } else {

                latitude = Double.valueOf(CommonConstants.LATITUDE);
                longitude = Double.valueOf(CommonConstants.LONGITUDE);

            }
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .title("Start Location")
                    .icon(BitmapDescriptorFactory.defaultMarker(120)));
            gpsBounds.include(new LatLng(latitude, longitude));
            new GetDirections().execute();
        }
        mMap.setOnCameraChangeListener(new OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition arg0) {

                if (provider.contains("gps")) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
                            gpsBounds.build(), 75));
                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(mapLat, mapLng), 15f));
                }

                mMap.setOnCameraChangeListener(null);
            }
        });
    }


    private class GetDirections extends AsyncTask<String, Void, Boolean> {
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;
        private ProgressDialog mDialog;

        @Override
        protected void onPostExecute(Boolean result) {
            try {
                mDialog.dismiss();
                if (result && !directionSteps.isEmpty()) {
                    // Instantiates a new Polyline object and adds points to define
                    // a rectangle
                    PolylineOptions rectOptions = new PolylineOptions()
                            .geodesic(true);
                    // add points
                    rectOptions.addAll(directionSteps);
                    // Set the rectangle's color to red
                    rectOptions.color(Color.RED);

                    // Get back the mutable Polyline
                    latLngBounds = LatLngBounds.builder();
                    for (int i = 0; i < directionSteps.size(); i++) {
                        if (directionSteps.get(i) != null) {
                            latLngBounds.include(directionSteps.get(i));
                        }
                    }
                    mMap.setOnCameraChangeListener(new OnCameraChangeListener() {

                        @Override
                        public void onCameraChange(CameraPosition arg0) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
                                    latLngBounds.build(), 50));
                            // Remove listener to prevent position reset on camera
                            // move.

                            mMap.setOnCameraChangeListener(null);

                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), "No Directions Found",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(ShowMapMultipleLocations.this, "",
                    "Fetching Directions.. ", true);
            mDialog.setCancelable(false);
        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            boolean result = false;
            try {
                directionSteps = new ArrayList<>();
                if (jsonObject != null
                        && "OK".equalsIgnoreCase(jsonObject.getString("status"))) {
                    if (jsonObject.getJSONArray("routes").length() > 0) {
                        jsonArray = jsonObject.getJSONArray("routes")
                                .getJSONObject(0).getJSONArray("legs")
                                .getJSONObject(0).getJSONArray("steps");
                        if (jsonArray.length() > 0) {
                            directionSteps.add(new LatLng(latitude, longitude));
                            directionSteps.add(new LatLng(mapLat, mapLng));
                            result = true;
                        } else {
                            result = false;
                        }

                    }
                } else {
                    result = false;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mListener.onLocationChanged(location);

    }

    @Override
    public void activate(OnLocationChangedListener listener) {
        mListener = listener;
    }

    @Override
    public void deactivate() {
        mListener = null;
    }


}
