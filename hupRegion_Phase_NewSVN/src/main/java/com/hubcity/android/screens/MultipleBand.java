package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MultipleBandModel;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

import java.util.LinkedList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by subramanya.v on 9/8/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class MultipleBand extends CustomTitleBar
{
	private Activity activity;
	private String bEvtId;
	private int lowerLimit = 0;
	private RestClient mRestClient;
	private MultipleBandAdapter mulBandAdapter;
	private ListView multipleBand;
	private LinkedList<MultipleBandObj> multileBand;
	private final LinkedList<RetailerDetail> retailerDetail = new LinkedList<>();
	private String eventName;
	public boolean isLoaded = false;
	private boolean isFirst = true;
	private View moreResultsView;
	private boolean isPaginationEnd = true;
	private LinearLayout bottomLayout;
	private BottomButtons bb;
	private boolean hasBottomButtons;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multiple_band);
		activity = MultipleBand.this;
		getBindValue();
		getIntentValue();
		title.setText(eventName);
		setClickListener();
		mRestClient = RestClient.getInstance();
		initializeBottomButton();
		callMultipleBandRequest();

	}

	private void initializeBottomButton()
	{
		try {
			//noinspection ConstantConditions
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			bb.setActivityInfo(this, "multipleBand");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		if (null != bb) {
			bb.setActivityInfo(this, "multipleBand");
		}
	}

	private void setClickListener()
	{
		multipleBand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
				Intent bandScreen = new Intent(activity, BandDetailScreen
						.class);
				bandScreen.putExtra("bandID", retailerDetail.get
						(position).getBandID());
				startActivity(bandScreen);

			}
		});
	}

	private void getBindValue()
	{
		moreResultsView = getLayoutInflater().inflate(
				R.layout.pagination, multipleBand, true);
		multipleBand = (ListView) findViewById(R.id.multiple_band);
		bottomLayout = (LinearLayout) findViewById(R.id.bottom_bar);
		bottomLayout.setVisibility(View.INVISIBLE);
	}

	private void getIntentValue()
	{
		Intent intent = getIntent();

		if (getIntent().hasExtra("eventId")) {
			bEvtId = intent.getExtras().getString("eventId");
		}
		if (getIntent().hasExtra("eventName")) {
			eventName = intent.getExtras().getString("eventName");
		}
	}

	private void callMultipleBandRequest()
	{
		ProgressDialog progDialog = null;
		if (isFirst) {
			progDialog = new ProgressDialog(this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage("Please Wait..");
			progDialog.setCancelable(false);
			progDialog.show();
		}
		MultipleBandModel multipleReq = UrlRequestParams.sendMultipleBands(bEvtId, lowerLimit);
		final ProgressDialog finalProgDialog = progDialog;
		mRestClient.sendMulBandReq(multipleReq, new Callback<MultipleBandObj>()
		{
			@Override
			public void success(MultipleBandObj multipleBandObj, Response response)
			{
				multileBand = new LinkedList<>();
				multileBand.add(multipleBandObj);
				if (!multipleBandObj.getResponseText().equalsIgnoreCase("Success")) {
					CommonConstants.displayToast(activity, multipleBandObj.getResponseText());
				}
				if(multileBand.get(0).getRetailerDetail() != null) {
					retailerDetail.addAll(multileBand.get(0).getRetailerDetail());
					displayListView();
				}

				dismissProgress(finalProgDialog);
				isFirst = false;
				isLoaded = false;
			}

			@Override
			public void failure(RetrofitError error)
			{
				if (error.getResponse() != null) {
					CommonConstants.displayToast(activity, error
							.getResponse().getReason());
				}

				dismissProgress(finalProgDialog);
			}
		});
	}

	private void displayListView()
	{
		if (multileBand.get(0).getNextPage() == 0) {
			loadListView();

			//removing footer view
			multipleBand.removeFooterView(moreResultsView);
			moreResultsView.setVisibility(View.GONE);

		} else {
			loadListView();

			//adding footer view
			multipleBand.removeFooterView(moreResultsView);
			multipleBand.addFooterView(moreResultsView);
			moreResultsView.setVisibility(View.VISIBLE);
		}
	}

	private void loadListView()
	{

		if(multileBand.get(0).getBottomBtnList() != null && multileBand.get(0).getBottomBtnList()
				.size() != 0) {
			BottomButtonListSingleton
					.clearBottomButtonListSingleton();
			BottomButtonListSingleton
					.getListBottomButton(multileBand.get(0).getBottomBtnList());
			hasBottomButtons = true;
		}

		if (hasBottomButtons && isFirst) {
			bb.createbottomButtontTab(bottomLayout, false);
			bottomLayout.setVisibility(View.VISIBLE);
		}
		if (isPaginationEnd) {
			mulBandAdapter = new MultipleBandAdapter(activity, retailerDetail,multileBand);
			multipleBand.setAdapter(mulBandAdapter);
			isPaginationEnd = false;
		} else {
			mulBandAdapter.updateList(retailerDetail,multileBand);
			mulBandAdapter.notifyDataSetChanged();
		}
	}

	private void dismissProgress(ProgressDialog progDialog)
	{
		if (progDialog != null) {
			if (progDialog.isShowing()) {
				progDialog.dismiss();
			}
		}
	}

	public void startPagination(int maxRowNum)
	{
		isLoaded = true;
		lowerLimit = maxRowNum;
		callMultipleBandRequest();
	}
}
