package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class SquareGridTemplate extends MenuPropertiesActivity implements
        OnItemClickListener {

    protected CustomImageLoader customImageLoader;
    private GridView gridview;
    private ArrayList<MainMenuBO> mainMenuUnsortedList;
    private String mBannerImg;
    private ImageView mBannerImageView;
    protected int latitude;
    protected int longitude;
    protected static int reqWidth;
    protected static int reqHeight;
    private LinearLayout gridViewParent;

    private LinearLayout parentIntermideate;
    private ProgressBar progressBar;
    private RelativeLayout bannerHolder;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;
    private View menuListTemplate;
    private Context mContext;

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        super.onNewIntent(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = SquareGridTemplate.this;
        CommonConstants.hamburgerIsFirst = true;

        Intent intent = getIntent();
        previousMenuLevel = intent.getExtras().getString(
                Constants.MENU_LEVEL_INTENT_EXTRA);


        parent = new RelativeLayout(this);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        parent.setLayoutParams(param);

        parentIntermideate = new LinearLayout(this);
        parentIntermideate.setOrientation(LinearLayout.VERTICAL);

        LayoutInflater layoutInflater = LayoutInflater.from(this);
         menuListTemplate = layoutInflater.inflate(R.layout.square_menu_layout,
                null, false);
        // Initializing drawer layout
        drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);
        mBannerImageView = (ImageView) menuListTemplate
                .findViewById(R.id.top_image);
        mBannerImageView.setScaleType(ScaleType.FIT_XY);
        progressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
        bannerHolder = (RelativeLayout) menuListTemplate.findViewById(R.id.banner_holder);
        bannerParent = (LinearLayout)menuListTemplate. findViewById(R.id.bannerParent);
        gridViewParent = (LinearLayout) menuListTemplate
                .findViewById(R.id.gridView_parent);

        gridview = (GridView) gridViewParent.findViewById(R.id.gridView);
        gridview.setOnItemClickListener(this);
        LinearLayout.LayoutParams gridViewParentParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        BottomButtonListSingleton mBottomButtonListSingleton;
        mBottomButtonListSingleton = BottomButtonListSingleton
                .getListBottomButton();

        if (mBottomButtonListSingleton != null) {

            ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                    .listBottomButton;

            if (listBottomButton != null && listBottomButton.size() > 1) {
                int px = (int) (40 * this.getResources().getDisplayMetrics().density + 0.5f);
                gridViewParentParam.setMargins(0, 0, 0, px);
            }
        }

        gridViewParent.setLayoutParams(gridViewParentParam);
        LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        parentIntermideate.setLayoutParams(parentParam);
        addTitleBar(parentIntermideate, parent);
        parentIntermideate.addView(gridViewParent);
        parent.addView(parentIntermideate);

        createbottomButtontTab(parent);
        setContentView(parent);

        handleIntent(intent);

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            HubCityContext.level = 0;
        } else {
            HubCityContext.level = HubCityContext.level + 1;
        }
        setDataAdapter();

        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if ((mBannerImg != null) && !("N/A".equalsIgnoreCase(mBannerImg))) {
                progressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(SquareGridTemplate.this).load(mBannerImg).into(mBannerImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        }else {
            customHorizontalTicker = (CustomHorizontalTicker)menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker)menuListTemplate. findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher)menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext, customHorizontalTicker, customVerticalTicker, bannerParent, customRotate, bannerHolder);
        }

    }

    private void backgroundColorSetting() {

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {

            if ((mBkgrdColor != null) && !("N/A".equalsIgnoreCase(mBkgrdColor))) {
                gridViewParent
                        .setBackgroundColor(Color.parseColor(mBkgrdColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(mBkgrdColor));
            } else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
                new ImageLoaderAsync1(gridViewParent).execute(mBkgrdImage);

            }
        } else {
            if ((smBkgrdColor != null)
                    && !("N/A".equalsIgnoreCase(smBkgrdColor))) {
                gridViewParent.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(smBkgrdColor));
            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {
                new ImageLoaderAsync1(gridViewParent).execute(smBkgrdImage);

            }
        }

    }

    @Override
    protected void onPause() {
        gridViewParent.setBackgroundColor(Color.parseColor("#ffffff"));
        parentIntermideate.setBackgroundColor(Color.parseColor("#ffffff"));
        parent.setBackgroundColor(Color.parseColor("#ffffff"));
        super.onPause();
    }

    @Override
    protected void onResume() {

        handleIntent(getIntent());
        backgroundColorSetting();
        activity = this;
        super.onResume();
        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }

    class ImageLoaderAsync1 extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync1(View bmImage) {
            this.bmImage = bmImage;
        }

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {

            mDialog = ProgressDialog.show(SquareGridTemplate.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            BitmapDrawable background = new BitmapDrawable(bmImg);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                bmImage.setBackgroundDrawable(background);
            } else {
                bmImage.setBackgroundDrawable(background);
            }

            try {
                if ((this.mDialog != null) && this.mDialog.isShowing()) {
                    this.mDialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                e.printStackTrace();
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                this.mDialog = null;
            }
        }
    }

    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);


            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("smBkgrdImage");
            departFlag = intent.getExtras().getString("departFlag");
            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
            mFontColor = intent.getExtras().getString("mFontColor");
            smFontColor = intent.getExtras().getString("smFontColor");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");

        }

        super.handleIntent(intent);

    }

    // Set the Data Adapter
    protected void setDataAdapter() {
        SquareViewAdapter squareViewAdapter = new SquareViewAdapter(SquareGridTemplate.this,
                R.layout.square_row_grid, mainMenuUnsortedList,
                previousMenuLevel, mFontColor, smFontColor);
        gridview.setAdapter(squareViewAdapter);

    }

    private void onTemplateListMenuRowItemClick(final AdapterView<?> arg0,
                                                final View view, final int position, final long id) {
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);
        super.mItemName = mMainMenuBO.getmItemName();
        super.mItemId = mMainMenuBO.getmItemId();
        super.linkTypeId = mMainMenuBO.getLinkTypeId();
        super.linkTypeName = mMainMenuBO.getLinkTypeName();
        super.mMainMenuBOPosition = mMainMenuBO.getPosition();
        super.mItemImgUrl = mMainMenuBO.getmItemImgUrl();
        super.mLinkId = mMainMenuBO.getLinkId();
        super.mBtnColor = mMainMenuBO.getmBtnColor();
        super.mBtnFontColor = mMainMenuBO.getmBtnFontColor();
        super.smBtnColor = mMainMenuBO.getSmBtnColor();
        super.smBtnFontColor = mMainMenuBO.getSmBtnFontColor();
        linkClickListener(linkTypeName);
    }

    @Override
    public void onItemClick(final AdapterView<?> arg0, final View view,
                            final int position, final long id) {
        onTemplateListMenuRowItemClick(arg0, view, position, id);
    }

}
