package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;

public class EventHotelListAdapter extends
		ArrayAdapter<HashMap<String, String>> {

	private ArrayList<HashMap<String, String>> items;
	private LayoutInflater vi;


	public EventHotelListAdapter(Context context,
			ArrayList<HashMap<String, String>> items) {
		super(context, 0, items);

		this.items = items;
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		final HashMap<String, String> item = items.get(position);
		if (item != null) {
			v = vi.inflate(R.layout.events_hotelist_row_item, parent,false);
			final TextView title = (TextView) v
					.findViewById(R.id.events_hotellist_item_entry_title);
			final TextView rating = (TextView) v
					.findViewById(R.id.events_hotellist_item_entry_rating);
			final TextView price = (TextView) v
					.findViewById(R.id.events_hotellist_item_entry_price);
			final TextView distance = (TextView) v
					.findViewById(R.id.events_hotellist_item_entry_distance);
			final ImageView image = (ImageView) v
					.findViewById(R.id.events_hotellist_main_image);

			if (title != null) {
				title.setText(item.get("retailerName"));
			}
			if (rating != null) {
				rating.setText("Rating : " + item.get("rating"));
			}
			if (price != null) {
				price.setText("Price:" + item.get("hotelPrice"));
			}
			if (distance != null) {
				distance.setText("Distance:" + item.get("distance"));
			}

			if (image != null) {
				if (item.get("logoImagePath") != null) {
					new DownloadImageTask(image).execute(item.get(
							"logoImagePath"));
				}
			}

		}

		return v;

	}

	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		private RotateAnimation anim;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
			anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF,
					0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setRepeatCount(Animation.INFINITE);
			anim.setDuration(700);
			this.bmImage.setAnimation(null);
			this.bmImage.setImageResource(R.drawable.loading_button);
			this.bmImage.startAnimation(anim);
			this.bmImage.setAnimation(anim);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0].replaceAll(" ", "%20");
			Bitmap mIcon11 = null;
			try {

				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null) {
				bmImage.setImageBitmap(result);
			}
			bmImage.setAnimation(null);
		}
	}
}
