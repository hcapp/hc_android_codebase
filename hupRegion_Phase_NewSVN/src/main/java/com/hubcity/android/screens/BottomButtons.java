package com.hubcity.android.screens;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.ImageLoaderAsync;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.govqa.GovQaLogin;
import com.hubcity.twitter.PrepareRequestTokenActivity;
import com.hubcity.twitter.TwitterUtils;
import com.scansee.android.CouponsActivty;
import com.scansee.android.FilterAndSortScreen;
import com.scansee.android.FindActivity;
import com.scansee.android.MyCouponActivity;
import com.scansee.android.ScanNowSplashActivity;
import com.scansee.android.ThisLocationGetLocation;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class creates bottom buttons and sets action for each bottom button
 *
 * @author rekha_p
 */
public class BottomButtons {

    protected static String GROUPED_TAB = "Grouped Tab";
    protected static String GROUPED_TAB_WITH_IMAGE = "Grouped Tab With Image";
    protected String mBannerImg;
    protected String smBtnFontColor;
    protected String smBtnColor;
    protected String mBtnFontColor;
    protected String mBtnColor;
    protected String mLinkId;
    protected String mItemImgUrl;
    protected String linkTypeName;
    protected String linkTypeId;
    protected String mItemId;
    protected String mItemName;
    protected int mMainMenuBOPosition;
    protected String previousMenuLevel;
    protected String catIds;
    public String searchKey;
    protected String mBkgrdColor;
    protected String mBkgrdImage;
    protected String smBkgrdColor;
    protected String smBkgrdImage;
    protected String departFlag;
    protected String typeFlag;
    protected AsyncTask<String, String, ArrayList<MainMenuBO>> mMenuAsyncTask;
    protected LinearLayout includeButtomControllTab;
    protected ImageView b1, b2, b3, b4;
    protected View buttomControllTabBar;
    protected String cityExpImgUlr;
    protected ArrayList<MainMenuBO> mainMenuUnsortedList;
    protected String bottomBtnId;
    protected TextView title;

    protected Button leftTitleImage;
    protected RelativeLayout parent;
    protected TextView divider;
    protected String templateName;
    protected String bottomBtnLinkId, bottomBtnLinkTypeID,
            bottomBtnLinkTypeName, bottomBtnPosition;

    private final Handler mTwitterHandler = new Handler();

    private SharedPreferences prefs;

    private String appPlaystoreLInk, appleStoreLink, appIconImg;
    String menuName;
    private PopupWindow pwindo;

    protected static Button btnClosePopup;
    protected String mGrpBkgrdColor;
    protected String mGrpFntColor;
    protected String sGrpBkgrdColor;
    protected String sGrpFntColor;
    protected String mFontColor;
    protected String smFontColor;

    String retAffCount = null;
    String retAffId = null;
    String retAffName = null;
    String cityExpId = null;

    String currentScreen = "";
    Activity activity;

    Button btnTwitterShare, btnSMSShare, btnEmailShare;
    Button btnFacebookShare;

    int screenWidth;
    int screenHeight;

    HashMap<String, String> values;
    ArrayList<RetailerBo> mRetailerList;

    public Activity getActivity() {
        return activity;
    }

    public void setActivityInfo(Activity activity, String currentscreen) {
        this.activity = activity;
        this.currentScreen = currentscreen;
    }

    public BottomButtons(String smBtnFontColor, String smBtnColor,
                         String mBtnFontColor, String mBtnColor, String mLinkId,
                         String mItemId, String mBkgrdColor, String mBkgrdImage,
                         String smBkgrdColor, String smBkgrdImage, String cityExpImgUlr,
                         String appPlaystoreLInk, String appleStoreLink, String appIconImg,
                         String retAffCount, String retAffId, String retAffName,
                         String cityExpId, String previousMenuLevel, String menuName,
                         String mFontColor, String smFontColor) {
        super();
        this.smBtnFontColor = smBtnFontColor;
        this.smBtnColor = smBtnColor;
        this.mBtnFontColor = mBtnFontColor;
        this.mBtnColor = mBtnColor;
        this.mLinkId = mLinkId;
        this.mItemId = mItemId;
        this.mBkgrdColor = mBkgrdColor;
        this.mBkgrdImage = mBkgrdImage;
        this.smBkgrdColor = smBkgrdColor;
        this.smBkgrdImage = smBkgrdImage;
        this.cityExpImgUlr = cityExpImgUlr;
        this.appPlaystoreLInk = appPlaystoreLInk;
        this.appleStoreLink = appleStoreLink;
        this.appIconImg = appIconImg;
        this.retAffCount = retAffCount;
        this.retAffId = retAffId;
        this.retAffName = retAffName;
        this.cityExpId = cityExpId;
        this.previousMenuLevel = previousMenuLevel;
        this.menuName = menuName;
        this.mFontColor = mFontColor;
        this.smFontColor = smFontColor;

    }

    /**
     * Parses the server response and saves data in the object
     *
     * @param xmlResponde2
     * @return
     * @throws JSONException
     */
    public ArrayList<BottomButtonBO> parseForBottomButton(
            JSONObject xmlResponde2) throws JSONException {

        ArrayList<BottomButtonBO> listBottomButton = new ArrayList<>();
        JSONArray arrayBottomButton = null;

        if (xmlResponde2.has("BottomButton")) {
            arrayBottomButton = xmlResponde2.optJSONArray("BottomButton");
        } else if (xmlResponde2.has("arBottomBtnList")) {
            arrayBottomButton = xmlResponde2.optJSONArray("arBottomBtnList");
        } else if (xmlResponde2.has("bottomBtnList")) {
            arrayBottomButton = xmlResponde2.optJSONArray("bottomBtnList");
        }
        String btnLinkID = null;
        if (arrayBottomButton != null) {
            for (int i = 0; i < arrayBottomButton.length(); i++) {

                JSONObject bottomButton = arrayBottomButton.getJSONObject(i);
                if (bottomButton != null) {
                    String bottomBtnID = bottomButton.getString("bottomBtnID");
                    String bottomBtnImg = bottomButton
                            .getString("bottomBtnImg");
                    String bottomBtnImgOff = bottomButton
                            .getString("bottomBtnImgOff");
                    String btnLinkTypeID = bottomButton
                            .getString("btnLinkTypeID");
                    String btnLinkTypeName = "";
                    if (bottomButton.has("btnLinkTypeName")) {
                        btnLinkTypeName = bottomButton
                                .getString("btnLinkTypeName");
                    }
                    String position = "";

                    if (bottomButton.has("position")) {
                        position = bottomButton.getString("position");
                    }

                    if (bottomButton.has("btnLinkID")) {
                        btnLinkID = bottomButton.getString("btnLinkID");
                    }

                    BottomButtonBO bottomButtonBO = new BottomButtonBO(
                            bottomBtnID, bottomBtnImg, bottomBtnImgOff,
                            btnLinkTypeID, btnLinkTypeName, position, btnLinkID);
                    listBottomButton.add(bottomButtonBO);
                }
            }

        } else {
            JSONObject bottomButton = null;

            if (xmlResponde2.has("BottomButton")) {
                bottomButton = xmlResponde2.optJSONObject("BottomButton");
            } else if (xmlResponde2.has("arBottomBtnList")) {
                bottomButton = xmlResponde2.optJSONObject("arBottomBtnList");
            } else if (xmlResponde2.has("bottomBtnList")) {
                bottomButton = xmlResponde2.optJSONObject("bottomBtnList");
            }

            if (bottomButton != null) {

                String bottomBtnID = bottomButton.getString("bottomBtnID");
                String bottomBtnImg = bottomButton.getString("bottomBtnImg");
                String bottomBtnImgOff = bottomButton
                        .getString("bottomBtnImgOff");
                String btnLinkTypeID = bottomButton.getString("btnLinkTypeID");

                String btnLinkTypeName = "";
                if (bottomButton.has("btnLinkTypeName")) {
                    btnLinkTypeName = bottomButton.getString("btnLinkTypeName");
                }

                String position = bottomButton.getString("position");
                if (bottomButton.has("btnLinkID")) {
                    btnLinkID = bottomButton.getString("btnLinkID");

                }

                BottomButtonBO bottomButtonBO = new BottomButtonBO(bottomBtnID,
                        bottomBtnImg, bottomBtnImgOff, btnLinkTypeID,
                        btnLinkTypeName, position, btnLinkID);
                listBottomButton.add(bottomButtonBO);
            }
        }
        if (!listBottomButton.isEmpty()) {
            return listBottomButton;
        } else {
            return null;
        }
    }

    /**
     * Gets the screen resolution i.e., width and height
     */
    void screenResolution() {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
    }

    /**
     * Creates bottom button tab and adds to the parent layout
     *
     * @param linearLayout
     */
    @SuppressLint("InflateParams")
    public void createbottomButtontTab(LinearLayout linearLayout,
                                       boolean isReverse) {
        ProgressBar p1, p2, p3, p4;
        screenResolution();

        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        buttomControllTabBar = layoutInflater.inflate(
                R.layout.buttom_controll_tab, null, false);
        buttomControllTabBar.setVisibility(View.VISIBLE);

        includeButtomControllTab = (LinearLayout) buttomControllTabBar
                .findViewById(R.id.include_buttom_controll_tab);
        //Setting backgroud color to bottom bar same as topbar background color
        includeButtomControllTab.setBackgroundColor(Color.parseColor(Constants.getNavigationBarValues("titleBkGrdColor")));
        includeButtomControllTab.setVisibility(View.VISIBLE);

        b1 = (ImageView) buttomControllTabBar.findViewById(R.id.b1);
        b2 = (ImageView) buttomControllTabBar.findViewById(R.id.b2);
        b3 = (ImageView) buttomControllTabBar.findViewById(R.id.b3);
        b4 = (ImageView) buttomControllTabBar.findViewById(R.id.b4);
        p1 = (ProgressBar) buttomControllTabBar.findViewById(R.id.progress_b1);
        p2 = (ProgressBar) buttomControllTabBar.findViewById(R.id.progress_b2);
        p3 = (ProgressBar) buttomControllTabBar.findViewById(R.id.progress_b3);
        p4 = (ProgressBar) buttomControllTabBar.findViewById(R.id.progress_b4);

        ArrayList<ImageView> bottonButtonViewList = new ArrayList<>();
        ArrayList<ProgressBar> progressBarList = new ArrayList<>();
        if (isReverse) {
            bottonButtonViewList.add(b4);
            bottonButtonViewList.add(b3);
            bottonButtonViewList.add(b2);
            bottonButtonViewList.add(b1);
            progressBarList.add(p4);
            progressBarList.add(p3);
            progressBarList.add(p2);
            progressBarList.add(p1);
        } else {
            bottonButtonViewList.add(b1);
            bottonButtonViewList.add(b2);
            bottonButtonViewList.add(b3);
            bottonButtonViewList.add(b4);
            progressBarList.add(p1);
            progressBarList.add(p2);
            progressBarList.add(p3);
            progressBarList.add(p4);
        }

        BottomButtonListSingleton mBottomButtonListSingleton;
        mBottomButtonListSingleton = BottomButtonListSingleton
                .getListBottomButton();

        if (mBottomButtonListSingleton != null) {
            ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                    .listBottomButton;

            if (listBottomButton == null) {
                includeButtomControllTab.setVisibility(View.GONE);
                return;
            }
            for (int i = 0; i < listBottomButton.size(); i++) {

                try {
                    BottomButtonBO bottomButtonBO = listBottomButton.get(i);

                    String bottomBtnImg = bottomButtonBO.getBottomBtnImg();

                    ImageView bottonButtonImageView = bottonButtonViewList
                            .get(i);
                    final ProgressBar progressBar = progressBarList.get(i);

                    if (bottomBtnImg != null) {
                        bottomBtnImg = bottomBtnImg.replaceAll(" ", "%20");
                        Picasso.with(activity).load(bottomBtnImg).into(bottonButtonImageView, new
                                Callback() {
                                    @Override
                                    public void onSuccess() {
                                        progressBar.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
//                        customImageLoader.displayImage(listBottomButton.get(i)
//                                        .getBottomBtnImg(), activity,
//                                bottonButtonImageView);

                    }

                    setButtomButtonclickListener(bottonButtonImageView,
                            bottomButtonBO);
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            android.widget.RelativeLayout.LayoutParams buttomControllTabBarLayoutParam = new
                    RelativeLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, screenHeight / 11);
            buttomControllTabBarLayoutParam
                    .addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            buttomControllTabBar
                    .setLayoutParams(buttomControllTabBarLayoutParam);

            linearLayout.addView(buttomControllTabBar);

        } else {
            includeButtomControllTab.setVisibility(View.GONE);
        }
    }

    public void setOptionsValues(HashMap<String, String> values,
                                 ArrayList<RetailerBo> mRetailerList) {
        this.values = values;
        this.mRetailerList = mRetailerList;
    }

    /**
     * Sets Listener to the bottom buttons and performs appropriate actions on
     * click of it
     *
     * @param button    selected button
     * @param bottomBtn Object holding the bottom button related data
     */
    @SuppressLint("ClickableViewAccessibility")
    void setButtomButtonclickListener(ImageView button,
                                      final BottomButtonBO bottomBtn) {

        if (button == null) {
            return;
        }

        button.setOnTouchListener(new OnTouchListener() {

            String typeName = "";

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        actionDown(v, bottomBtn, typeName);
                        break;

                    case MotionEvent.ACTION_UP:
                        actionUp(v, bottomBtn, typeName);
                        break;

                    case MotionEvent.ACTION_CANCEL:
                        actionCancel(v, bottomBtn, typeName);
                        break;

                    default:
                        break;

                }
                return true;
            }
        });

    }

    private void actionCancel(View v, BottomButtonBO bottomBtn, String typeName) {
        ImageView viewCancel = (ImageView) v;
        String bottomBtnimg = bottomBtn.getBottomBtnImg();
        if (bottomBtnimg != null) {
            if (!currentScreen.equals(typeName)) {
                new ImageLoaderAsync(viewCancel).execute(bottomBtnimg);
                viewCancel.invalidate();
            }
        }

    }

    private void actionUp(View v, BottomButtonBO bottomBtn, String typeName) {
        ImageView viewDown = (ImageView) v;
        String bottomBtnImg = bottomBtn.getBottomBtnImg();
        if (bottomBtnImg != null) {
            if (!currentScreen.equals(typeName)) {
                new ImageLoaderAsync(viewDown).execute(bottomBtnImg);
                viewDown.invalidate();
            }
        }

    }

    private void actionDown(View v, BottomButtonBO bottomBtn, String typeName) {
        ImageView viewUp = (ImageView) v;
        String bottomBtnImgOff = bottomBtn.getBottomBtnImgOff();
        if (bottomBtnImgOff != null) {

            if (!currentScreen.equals(typeName)) {
                new ImageLoaderAsync(viewUp).execute(bottomBtnImgOff);
                viewUp.invalidate();
            }

            bottomButtonClickAction(bottomBtn);
        }

    }

    private void bottomButtonClickAction(BottomButtonBO bottomBtn) {

        bottomBtnId = bottomBtn.getBottomBtnID();
        bottomBtnLinkId = bottomBtn.getBtnLinkID();
        bottomBtnLinkTypeID = bottomBtn.getBtnLinkTypeID();

        bottomBtnLinkTypeName = bottomBtn.getBtnLinkTypeName();
        bottomBtnPosition = bottomBtn.getPosition();

        if (currentScreen.equals(bottomBtnLinkTypeName)) {
            if (!bottomBtnLinkTypeName.equals("Find")) {
                if (!bottomBtnLinkTypeName.startsWith("EventSingleCategory-")
                        || !bottomBtnLinkTypeName
                        .startsWith("FindSingleCategory-")) {
                    return;
                }
            }
        }

        // this is checking is for - if we press find from bottom btn when are
        // at the same screen
        if ((currentScreen.equals("Find-BottomBtn") || currentScreen.equals("Find"))
                && bottomBtnLinkTypeName.equals("Find")) {
            return;
        }

        if ("GovQA".equalsIgnoreCase(linkTypeName)) {
            startGovQaScreen();

        } else if ("Fundraisers".equals(bottomBtnLinkTypeName)) {
            startButtomBtnFundraiserScreen(bottomBtnId, bottomBtnLinkId, "0",
                    null);
        } else if ("Find".equals(bottomBtnLinkTypeName)) {
            startButtomBtnFindScreen();
        } else if ("Filters".equals(bottomBtnLinkTypeName)) {
            cityExpId = bottomBtnLinkId;
            startFilterScreen();
        } else if ("SubMenu".equals(bottomBtnLinkTypeName)) {
            startBottomBtnSubMenu();
        } else if ("City Experience".equals(bottomBtnLinkTypeName)) {
            startCitiExpeienceScreen(bottomBtnId, bottomBtnLinkId, "0", null);
        } else if ("About".equals(bottomBtnLinkTypeName)) {
            startAboutScreen(true);
        } else if ("Privacy Policy".equals(bottomBtnLinkTypeName)) {
            startPrivatePolicyScreen(true);
        } else if ("Events".equals(bottomBtnLinkTypeName)) {
            startEventsScreen(true);
        } else if (bottomBtnLinkTypeName.startsWith("FindSingleCategory-")) {
            String[] catNames = bottomBtnLinkTypeName.split("-");
            startFindSingleCategoryScreen(catNames[1], true);
        } else if ("Alerts".equals(bottomBtnLinkTypeName)) {
            startAlertScreen(true);
        } else if ("AnythingPage".equals(bottomBtnLinkTypeName)) {
            startAnythingPage(true);
        } else if ("AppSite".equals(bottomBtnLinkTypeName)) {
            starAppsitePage(true);
        } else if ("Whats NearBy".equals(bottomBtnLinkTypeName)) {
            startWhatsNearbyScreen(true);
        } else if (bottomBtnLinkTypeName.startsWith("EventSingleCategory-")) {
            startEventsScreen(true);
        } else if ("FAQ".equals(bottomBtnLinkTypeName)) {
            startFaqScreen(true);
        } else if ("Scan Now".equals(bottomBtnLinkTypeName)) {
            startScannowScreen(true);
        }
        else if ("My Accounts".equals(bottomBtnLinkTypeName)||"My Deals".equals(bottomBtnLinkTypeName)) {
            startMyAccount();
        }
        else if ("Deals".equals(bottomBtnLinkTypeName)) {
            startHotdealsScreen(true);
        } // Settings
        else if ("User Information".equals(bottomBtnLinkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startUserInformation();
            } else {
                Constants.SignUpAlert(activity);
            }

        } else if ("Location Preferences".equals(bottomBtnLinkTypeName)) {

            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startLocationPreference();
            } else {
                Constants.SignUpAlert(activity);
            }

        } else if ("Category Favorites".equals(bottomBtnLinkTypeName)) {

            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startCategoryFavourites();
            } else {
                Constants.SignUpAlert(activity);
            }

        } else if ("City Favorites".equals(bottomBtnLinkTypeName)) {

            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startCityFavourites();
            } else {
                Constants.SignUpAlert(activity);
            }

        } else if ("Map".equalsIgnoreCase(bottomBtnLinkTypeName)) {
            startMapScreen();

        } else if ("SortFilter".equalsIgnoreCase(bottomBtnLinkTypeName) || "Filter".equalsIgnoreCase(bottomBtnLinkTypeName)) {
            startSortScreen();

        } else {
            linkClickListener(bottomBtnLinkTypeName);
        }
    }

    private void startMyAccount() {
        Intent intent = new Intent(activity, MyCouponActivity.class);
        activity.startActivity(intent);
    }

    private void startMapScreen() {
        if (mRetailerList != null && mRetailerList.size() > 0) {

            Intent showMap = new Intent(activity,
                    ShowMapMultipleLocations.class);

            if (values.containsKey("filterFlag")
                    && Boolean.valueOf(values.get("filterFlag"))
                    && values.containsKey("retAffName")) {

                showMap.putExtra(CommonConstants.TAG_FILTERNAME,
                        values.get("retAffName"));
            }

            showMap.putParcelableArrayListExtra("mRetailerList", mRetailerList);
            activity.startActivity(showMap);

        } else {
            Toast.makeText(
                    activity,
                    activity.getResources().getString(
                            R.string.alert_no_retailer), Toast.LENGTH_SHORT)
                    .show();
        }

    }

    private void startSortScreen() {
        if (values != null) {
            HashMap<String, String> intent_values = new HashMap<>();

            Intent sortbylist = new Intent();
            String className = "";

            if (values.containsKey("Class")) {
                className = values.get("Class");
            }

            if ("Find".equals(className)) {

                intent_values.put("catName", values.get("selectedCat"));

                intent_values.put("subCatId", values.get("subCatId"));

                intent_values.put("mItemId", values.get("mItemId"));
                intent_values.put("mLinkId", values.get("mLinkId"));
                intent_values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        values.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));

                intent_values.put("sortChoice", values.get("sortChoice"));
                intent_values.put("catId", values.get("selectedCatId"));

                intent_values.put("typeName", values.get("typeName"));

                intent_values.put("latitude", values.get("latitude"));

                intent_values.put("longitude", values.get("longitude"));

                if (intent_values.containsKey("srchKey")) {
                    intent_values.put("srchKey", values.get("srchKey"));
                }

            } else if ("BandLanding".equals(className)) {

                intent_values.put("mItemId", values.get("mItemId"));

                intent_values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        values.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));

                intent_values.put("latitude", values.get("latitude"));

                intent_values.put("longitude", values.get("longitude"));

                intent_values.put("moduleName", values.get("moduleName"));

                if (values.containsKey("srchKey")) {
                    intent_values.put("srchKey", values.get("srchKey"));
                }
                if (values.containsKey("radius")) {
                    intent_values.put("radius", values.get("radius"));
                }
            } else if ("FindSingleCat".equals(className)) {

                intent_values.put("sortChoice", values.get("sortChoice"));

                intent_values.put("mLinkId", values.get("mLinkId"));

                intent_values.put("catName", values.get("catName"));

                intent_values.put("catId", values.get("catId"));

                intent_values.put("mItemId", values.get("mItemId"));

                intent_values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        values.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));

                intent_values.put("latitude", values.get("latitude"));

                intent_values.put("longitude", values.get("longitude"));

                intent_values.put("srchKey", values.get("srchKey"));

                if (values.containsKey("subCatId")) {
                    intent_values.put("subCatId", values.get("subCatId"));
                }

            } else if ("Find All".equals(className)) {

                if (values.containsKey("srchKey")) {
                    intent_values.put("srchKey", values.get("srchKey"));
                }

                intent_values.put("mItemId", values.get("mItemId"));

                intent_values.put(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        values.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));

                if (values.containsKey("catName")) {
                    intent_values.put("catName", values.get("catName"));
                }

                if (values.containsKey("selectedCatId")) {
                    intent_values.put("selectedCatId", values.get("selectedCatId"));
                }
                intent_values.put("catId", values.get("selectedCatId"));
                intent_values.put("mLinkId", values.get("mLinkId"));
                intent_values.put("latitude", values.get("latitude"));

                intent_values.put("longitude", values.get("longitude"));

                intent_values.put("subCatId", values.get("subCatId"));

            } else if ("Citi Exp".equals(className)) {
                if (values.containsKey(Constants.CITI_EXPID_INTENT_EXTRA)) {
                    intent_values.put(Constants.CITI_EXPID_INTENT_EXTRA,
                            values.get(Constants.CITI_EXPID_INTENT_EXTRA));
                }

                intent_values.put("latitude", values.get("latitude"));
                try {
                    intent_values.put("mLinkId", values.get("mLinkId"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent_values.put("longitude", values.get("longitude"));

                if (values.containsKey("srchKey")) {
                    intent_values.put("srchKey", values.get("srchKey"));
                }

            } else if ("filter".equals(className)) {

                intent_values.put("latitude", values.get("latitude"));

                intent_values.put("longitude", values.get("longitude"));
                try {
                    intent_values.put("mLinkId", values.get("mLinkId"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent_values.put(CommonConstants.TAG_FILTERID,
                        values.get(CommonConstants.TAG_FILTERID));

                if (values.containsKey("srchKey")) {
                    intent_values.put("srchKey", values.get("srchKey"));
                }

            } else if ("Fundraiser".equals(className)) {

                Intent sortIntent = new Intent(activity,
                        FundraisersSortByActivity.class);
                sortIntent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        values.get("mItemId"));
                sortIntent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        values.get(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA));
                sortIntent.putExtra("isDeptFlag",
                        Integer.valueOf(values.get("isDeptFlag")));
                sortIntent.putExtra("retailerId", values.get("retailerId"));
                sortIntent.putExtra("retListId", values.get("retListId"));
                sortIntent.putExtra("retailLocationId",
                        values.get("retailLocationId"));
                sortIntent.putExtra("retailerEvents", values.get("retailerEvents"));
                sortIntent.putExtra("mLinkId", values.get("mLinkId"));
                sortIntent.putExtra("catName", values.get("catName"));
                sortIntent.putExtra("catId", values.get("catId"));
                sortIntent.putExtra("deptId", values.get("deptId"));
                activity.startActivity(sortIntent);
                activity.finish();
                return;
            }

            if ("Whats NearBy".equalsIgnoreCase(className)) {
                sortbylist.putExtra("Class", className);
                sortbylist.setClass(activity, OptionSortByList.class);
            } else {
                intent_values.put("Class", className);
                sortbylist.putExtra("values", values);
                sortbylist.putExtra("title", bottomBtnLinkTypeName);
                sortbylist.setClass(activity, FilterAndSortScreen.class);
            }

            activity.startActivityForResult(sortbylist, 01);
        }
    }

    protected void startButtomBtnFundraiserScreen(String bottomBtnId,
                                                  String linkTypeId, String catIds, String searchKey) {
        Intent intent = new Intent(activity, FundraiserActivity.class);
        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
        intent.putExtra(Constants.FUNDRAISER_ID_INTENT_EXTRA, linkTypeId);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        activity.startActivity(intent);

    }

    private void startScannowScreen(boolean isBottomBtn) {

        Intent intent = new Intent(activity, ScanNowSplashActivity.class);
        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);

    }

    private void startGovQaScreen() {
        Intent intent = new Intent(activity, GovQaLogin.class);
        intent.putExtra("BottomBtnJsonObj", MenuAsyncTask.bbJsonObj.toString());
        activity.startActivity(intent);

    }

    private void startHotdealsScreen(boolean isBottomBtn) {

        Intent intent = new Intent(activity, CouponsActivty.class);
        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    protected void startBottomBtnSubMenu() {
        Constants.SHOWGPSALERT = false;
        Integer nextLevel;
        int i = 0;
        if (previousMenuLevel != null) {
            nextLevel = Integer.parseInt(previousMenuLevel) + 1;
            i = nextLevel.intValue();
        } else {
            nextLevel = i + 1;
            i = nextLevel;
        }

        int position = 0;
        if (bottomBtnPosition != null && !"".equals(bottomBtnPosition)) {
            position = Integer.parseInt(bottomBtnPosition);
        }

        MainMenuBO mainMenuBO = new MainMenuBO(bottomBtnId, mItemName,
                bottomBtnLinkTypeID, bottomBtnLinkTypeName, position,
                mItemImgUrl, bottomBtnLinkId, mBtnColor, mBtnFontColor,
                smBtnColor, smBtnFontColor, nextLevel.toString(), mGrpFntColor,
                sGrpFntColor, mGrpBkgrdColor, sGrpBkgrdColor, "", null, mBkgrdColor);
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

        mMenuAsyncTask = new MenuAsyncTask(activity).execute("0", bottomBtnId,
                bottomBtnLinkId, "None", "0", "0");
    }

    private void startFaqScreen(boolean isBottomBtn) {
        Intent intent = new Intent(activity, FaqScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        activity.startActivity(intent);
    }

    private void startWhatsNearbyScreen(boolean isBottomBtn) {

        Intent intent = new Intent(activity, ThisLocationGetLocation.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            intent.putExtra("mBkgrdColor", mBkgrdColor);
            intent.putExtra("mBkgrdImage", mBkgrdImage);
            intent.putExtra("mBtnColor", mBtnColor);
            intent.putExtra("mBtnFontColor", mBtnFontColor);

        } else {

            intent.putExtra("mBkgrdColor", smBkgrdColor);
            intent.putExtra("mBkgrdImage", smBkgrdImage);
            intent.putExtra("mBtnColor", smBtnColor);
            intent.putExtra("mBtnFontColor", smBtnFontColor);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, Constants.STARTVALUE);
    }

    private void starAppsitePage(boolean isBottomBtn) {

        Intent intent = new Intent(activity, AppsiteActivity.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

        } else {
            intent.putExtra(CommonConstants.LINK_ID, this.mLinkId);
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, Constants.STARTVALUE);
    }

    private void startAnythingPage(boolean isBottomBtn) {

        Intent intent = new Intent(activity, AnythingPageDisplayActivity.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(CommonConstants.LINK_ID, this.mLinkId);
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        intent.putExtra("isShare", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, Constants.STARTVALUE);
    }

    private void startAlertScreen(boolean isBottomBtn) {

        Intent intent = new Intent(activity, AlertListDisplay.class);
        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, Constants.STARTVALUE);
    }

    private void startFindSingleCategoryScreen(String catName,
                                               boolean isBottomBtn) {

        Intent intent = new Intent(activity, FindSingleCategory.class);
        intent.putExtra("catName", catName);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            intent.putExtra("mLinkId", mLinkId);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private void startEventsScreen(boolean isBottomBtn) {

        Intent intent = new Intent(activity, EventsListDisplay.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            intent.putExtra("mLinkId", mLinkId);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, Constants.STARTVALUE);
    }
    private void startPrivatePolicyScreen(boolean isBottomBtn) {

        Intent intent = new Intent(activity, PrivacyPolicyActivity.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            intent.putExtra("mLinkId", mLinkId);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivityForResult(intent, Constants.STARTVALUE);
    }

    private void startAboutScreen(boolean isBottomBtn) {
        Intent intent = new Intent(activity, AboutUsActivity.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    /**
     * Intent call to CitiExperienceActivity class
     *
     * @param mItemId
     * @param linkTypeId
     * @param catIds
     * @param searchKey
     */
    private void startCitiExpeienceScreen(String mItemId, String linkTypeId,
                                          String catIds, String searchKey) {

        Intent intent = new Intent(activity, CitiExperienceActivity.class);
        intent.putExtra("cityExpImgUlr", cityExpImgUlr);
        intent.putExtra(Constants.APP_STORE_LINK_INTENT_EXTRA, appPlaystoreLInk);
        intent.putExtra("downLoadLink", appleStoreLink);
        intent.putExtra(Constants.APP_ICON_INTENT_EXTRA, appIconImg);
        intent.putExtra("mBkgrdColor", mBkgrdColor);
        intent.putExtra("mBkgrdImage", mBkgrdImage);
        intent.putExtra("mBtnColor", mBtnColor);
        intent.putExtra("mBtnFontColor", mBtnFontColor);
        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, linkTypeId);
        intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    /**
     * Intent call to PreferedCatagoriesScreen class
     */
    private void startPreferenceScreen() {
        Intent intent = new Intent(activity, PreferedCatagoriesScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);
        activity.startActivity(intent);
    }

    /**
     * Intent call to SettingsPage class
     */
    private void startSettingsPageScreen() {
        Intent intent = new Intent(activity, SettingsPage.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);
        intent.putExtra("mItemId", mItemId);
        intent.putExtra("mLinkId", mLinkId);
        activity.startActivityForResult(intent, Constants.STARTVALUE);

    }

    /**
     * Intent call to FindActivity class
     */
    private void startButtomBtnFindScreen() {

        Intent intent = new Intent(activity, FindActivity.class);
        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        intent.putExtra("mLinkId", mLinkId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);

    }

    /**
     * Intent call to CitiExperienceActivity or CityExperienceFilterActivity
     * class depending on the condition check
     */
    private void startFilterScreen() {

        if (null != retAffCount && "1".equals(retAffCount)) {
            Intent showFilter = new Intent(activity.getApplicationContext(),
                    CitiExperienceActivity.class);

            showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, cityExpId);
            showFilter.putExtra(Constants.CAT_IDS_INTENT_EXTRA, "0");
            showFilter.putExtra(CommonConstants.CITYEXP_IMG_URL, cityExpImgUlr);
            showFilter.putExtra(CommonConstants.TAG_FILTERID, retAffId);
            showFilter.putExtra(CommonConstants.FILTERCOUNT_EXTRA, retAffCount);
            showFilter.putExtra(CommonConstants.TAG_FILTERNAME, retAffName);
            showFilter.putExtra("mBtnColor", mBtnColor);
            showFilter.putExtra("mBtnFontColor", mBtnFontColor);
            showFilter.putExtra("mBkgrdColor", mBkgrdColor);
            showFilter.putExtra("mBkgrdImage", mBkgrdImage);
            showFilter.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                    bottomBtnId);
            activity.startActivityForResult(showFilter, Constants.STARTVALUE);

        } else {
            Intent showFilter = new Intent(activity.getApplicationContext(),
                    CityExperienceFilterActivity.class);
            showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, cityExpId);
            showFilter.putExtra(CommonConstants.CITYEXP_IMG_URL, cityExpImgUlr);
            showFilter.putExtra("mBtnColor", mBtnColor);
            showFilter.putExtra("mBtnFontColor", mBtnFontColor);
            showFilter.putExtra("mBkgrdColor", mBkgrdColor);
            showFilter.putExtra("mBkgrdImage", mBkgrdImage);
            showFilter.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                    bottomBtnId);
            activity.startActivityForResult(showFilter, Constants.STARTVALUE);

        }

    }

    /**
     * On click of Share bottom button a popup window is shown
     */
    protected void initiatePopupWindow() {
        try {
            try {
                // We need to get the instance of the LayoutInflater
                LayoutInflater inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View layout = inflater.inflate(
                        R.layout.share_screen_popup,
                        (ViewGroup) activity.findViewById(R.id.popup_element));
                DisplayMetrics metrics = new DisplayMetrics();
                activity.getWindowManager().getDefaultDisplay()
                        .getMetrics(metrics);
                int screenWidth = metrics.widthPixels;
                int screenHeight = metrics.heightPixels;
                pwindo = new PopupWindow(layout, screenWidth, screenHeight / 2,
                        true);

                pwindo.showAtLocation(layout, Gravity.BOTTOM, 0, 0);

                btnClosePopup = (Button) layout
                        .findViewById(R.id.btn_close_popup);

                btnFacebookShare = (Button) layout
                        .findViewById(R.id.btn_facebook_popup);
                btnTwitterShare = (Button) layout
                        .findViewById(R.id.btn_twitter_popup);
                btnSMSShare = (Button) layout.findViewById(R.id.btn_text_popup);
                btnEmailShare = (Button) layout
                        .findViewById(R.id.btn_email_popup);

                btnFacebookShare.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaFacebook();

                    }
                });
                btnTwitterShare.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        shareViaTwitter();
                    }

                });
                btnSMSShare.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaSMS();

                    }

                });
                btnEmailShare.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaEMail();
                    }

                });

                btnClosePopup.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Intent call to EmailShareActivity class
     */
    private void shareViaEMail() {

        EmailAsyncTask emailAsyncTask = new EmailAsyncTask(activity, null);
        emailAsyncTask.execute();
    }

    /**
     * Sharing link via Email
     */
    private void shareViaSMS() {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer
                .append("Please download the HubCiti App from the below link:");
        stringBuffer.append("\n");
        stringBuffer.append(appPlaystoreLInk);
        stringBuffer.append("\n");
        stringBuffer.append(appleStoreLink);
        stringBuffer.append("\n");

        if (stringBuffer.length() > 0) {
            Intent smsShare = new Intent(Intent.ACTION_VIEW);
            smsShare.setData(Uri.parse("sms:"));
            try {
                smsShare.putExtra("sms_body",
                        URLDecoder.decode(stringBuffer.toString(), "UTF-8"));
                activity.startActivity(smsShare);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                Toast.makeText(activity.getApplicationContext(),
                        "Unable to SMS this Content.", Toast.LENGTH_SHORT)
                        .show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(activity.getApplicationContext(),
                        "SMS Option Not Available", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /**
     * Share link via Facebook
     */
    private void shareViaFacebook() {

        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.fb_share_popup);
        dialog.setTitle("Share via FaceBook");

        final EditText eText = (EditText) dialog.findViewById(R.id.text);

        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        new ImageLoaderAsync(image).execute(appIconImg);

        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog
                .findViewById(R.id.dialogButtonCancel);

        dialogButtonOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent facebookIntent = new Intent(activity,
                        FaceBookActivity.class);
                facebookIntent.putExtra("userText", eText.getText().toString());
                facebookIntent.putExtra("appPlayStoreLink", appPlaystoreLInk);
                dialog.dismiss();
                activity.startActivity(facebookIntent);
            }
        });
        dialogButtonCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    /**
     * Calls AsyncTask to share link via twitter
     */
    private void shareViaTwitter() {
        this.prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.fb_share_popup);
        dialog.setTitle("Share via Twitter");

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setSingleLine(false);
        text.setText(activity.getResources().getString(R.string.tweet_msg)
                + "\n" + appPlaystoreLInk);

        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        new ImageLoaderAsync(image).execute(appIconImg);

        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog
                .findViewById(R.id.dialogButtonCancel);

        dialogButtonOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new TweetMsg().execute();
            }
        });
        dialogButtonCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    /**
     * AsyncTask to post link on twitter
     *
     * @author rekha_p
     */
    public class TweetMsg extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (TwitterUtils.isAuthenticated(prefs)) {
                sendTweet();
            } else {
                Intent i = new Intent(activity.getApplicationContext(),
                        PrepareRequestTokenActivity.class);
                i.putExtra("tweet_msg",
                        activity.getResources().getString(R.string.tweet_msg)
                                + "\n" + appPlaystoreLInk);
                i.putExtra("image",
                       appIconImg);
                activity.startActivityForResult(i, 2505);
            }
            return null;
        }

    }

    /**
     * Posts link on twitter
     */
    public void sendTweet() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    TwitterUtils.sendTweet(prefs, activity.getResources()
                            .getString(R.string.tweet_msg)
                            + "\n"
                            + appPlaystoreLInk, appIconImg);
                    mTwitterHandler.post(mUpdateTwitterNotification);
                } catch (Exception e) {
                    if (e.getMessage().contains(
                            "Status is over 140 characters")) {
                        Toast.makeText(
                                activity.getApplicationContext(),
                                "Tweet not sent - Status is over 140 characters!",
                                Toast.LENGTH_LONG).show();
                    } else if (e.getMessage().contains("duplicate")) {
                        Toast.makeText(activity.getApplicationContext(),
                                "Tweet not sent - Status Duplicate!",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(activity.getApplicationContext(),
                                "Tweet not sent - Error!",
                                Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }

    /**
     * Updates status of tweet
     */
    final Runnable mUpdateTwitterNotification = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(activity.getBaseContext(), "Tweet sent !",
                    Toast.LENGTH_LONG).show();
            btnClosePopup.performClick();

        }
    };

    /**
     * Opens submenu
     */
    protected void startSubMenu() {
        Constants.SHOWGPSALERT = false;
        Integer nextLevel;
        int currentMenuLevel = 0;
        if (previousMenuLevel != null) {
            nextLevel = Integer.parseInt(previousMenuLevel) + 1;
            currentMenuLevel = nextLevel.intValue();
        } else {
            nextLevel = currentMenuLevel + 1;
            currentMenuLevel = nextLevel;
        }

        MainMenuBO mainMenuBO = new MainMenuBO(mItemId, mItemName, linkTypeId,
                linkTypeName, mMainMenuBOPosition, mItemImgUrl, mLinkId,
                mBtnColor, mBtnFontColor, smBtnColor, smBtnFontColor,
                nextLevel.toString(), mGrpFntColor, sGrpFntColor,
                mGrpBkgrdColor, sGrpBkgrdColor, "", null, mBkgrdColor);
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

        int level = SubMenuStack.getSubMenuStack().size();

        SubMenuDetails subMenuDetails = new SubMenuDetails("0", "0", "Name",
                "", mLinkId, mItemId, currentMenuLevel);
        SortDepttNTypeScreen.subMenuDetailsList.add(subMenuDetails);

        mMenuAsyncTask = new MenuAsyncTask(activity).execute(
                String.valueOf(currentMenuLevel), mItemId, mLinkId, "None", "0", "0");

    }

    /**
     * Sets action on click of bottom button
     *
     * @param linkTypeName
     */
    public void linkClickListener(String linkTypeName) {

        try {
            if (currentScreen.equals(linkTypeName)) {
                return;
            }

            if (Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                Constants.SignUpAlert(activity);

            } else {

                if ("Preferences".equals(linkTypeName)
                        || "Preference".equals(linkTypeName)) {

                    startPreferenceScreen();

                } else if ("Settings".equals(linkTypeName)) {
                    startSettingsPageScreen();

                } else if ("Share".equals(linkTypeName)) {
                    initiatePopupWindow();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Handles the intent data
     *
     * @param intent
     */
    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            if (null != mainMenuUnsortedList && !mainMenuUnsortedList.isEmpty()) {
                if (null != templateName
                        && (templateName.equals(GROUPED_TAB) || templateName
                        .equals(GROUPED_TAB_WITH_IMAGE))
                        && mainMenuUnsortedList.size() >= 2) {
                    if (previousMenuLevel != null
                            && "1".equals(previousMenuLevel)) {
                        mBtnColor = mainMenuUnsortedList.get(1).getmBtnColor();
                        mBtnFontColor = mainMenuUnsortedList.get(1)
                                .getmBtnFontColor();

                    } else {

                        smBtnColor = mainMenuUnsortedList.get(1)
                                .getSmBtnColor();
                        smBtnFontColor = mainMenuUnsortedList.get(1)
                                .getSmBtnFontColor();

                    }
                } else {
                    if (previousMenuLevel != null
                            && "1".equals(previousMenuLevel)) {
                        mBtnColor = mainMenuUnsortedList.get(0).getmBtnColor();
                        mBtnFontColor = mainMenuUnsortedList.get(0)
                                .getmBtnFontColor();

                    } else {

                        smBtnColor = mainMenuUnsortedList.get(0)
                                .getSmBtnColor();
                        smBtnFontColor = mainMenuUnsortedList.get(0)
                                .getSmBtnFontColor();

                    }

                }

            }

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);
            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            smBkgrdImage = intent.getExtras().getString("smBkgrdImage");

            departFlag = intent.getExtras().getString("departFlag");
            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
            String APP_ICON_LINK = "appIconImg";
            appIconImg = intent.getExtras().getString(APP_ICON_LINK);
            String APP_PLAYSTORE_LINK = "androidLink";
            appPlaystoreLInk = intent.getExtras().getString(APP_PLAYSTORE_LINK);
            appleStoreLink = intent.getExtras().getString("downLoadLink");

            if (intent.hasExtra("cityExpImgUlr")) {
                this.cityExpImgUlr = intent.getExtras().getString(
                        "cityExpImgUlr");

            }

            retAffCount = intent.getExtras().getString("retAffCount");

            if (null != retAffCount && "1".equals(retAffCount)) {

                retAffId = intent.getExtras().getString("retAffId");
                retAffName = intent.getExtras().getString("retAffName");

            }

        }
    }

    private void startUserInformation() {

        Intent intent = new Intent(activity, SetPreferrenceScreen.class);
        intent.putExtra("regacc", false);
        activity.startActivity(intent);

    }

    private void startLocationPreference() {
        Intent intent = new Intent(activity, SettingsLocationServices.class);
        intent.putExtra("regacc", false);

        activity.startActivity(intent);
    }

    private void startCategoryFavourites() {
        Intent intent = new Intent(activity, PreferedCatagoriesScreen.class);
        intent.putExtra("regacc", false);
        activity.startActivity(intent);

    }

    private void startCityFavourites() {
        Intent intent = new Intent(activity, CityPreferencesScreen.class);
        intent.putExtra("regacc", false);
        activity.startActivity(intent);

    }

}
