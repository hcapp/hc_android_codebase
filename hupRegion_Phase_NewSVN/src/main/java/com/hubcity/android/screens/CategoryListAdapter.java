package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class CategoryListAdapter extends BaseAdapter implements OnClickListener {
	private PreferedCatagoriesScreen activity;
	private ArrayList<HashMap<String, Object>> categoryList, originalList;
	private static LayoutInflater inflater = null;

	String itemName;

	boolean isItemExpanded;
	Integer itemLength;
	float itemDisp;
	public static final String TAG_MAIN_PREFERENCES_MENU = "MainCategory";
	public static final String TAG_SUB_PREFERENCES_MENU = "SubCategory";

	public CategoryListAdapter(Activity activity,
			ArrayList<HashMap<String, Object>> categoryList,
			ArrayList<HashMap<String, Object>> originalList) {
		this.activity = (PreferedCatagoriesScreen) activity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.categoryList = categoryList;
		this.originalList = originalList;
	}

	@Override
	public int getCount() {
		return categoryList.size();
	}

	@Override
	public Object getItem(int id) {
		return categoryList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder;
		itemName = (String) categoryList.get(position).get("itemName");

		if (itemName != null) {
			if (itemName.equalsIgnoreCase(TAG_MAIN_PREFERENCES_MENU)) {
				view = inflater.inflate(R.layout.listitem_preferences, parent,false);
				viewHolder = new ViewHolder();
				viewHolder.categoryPreferenceText = (TextView) view
						.findViewById(R.id.category_preference_text);

				viewHolder.categoryPreferenceDownbtn = (ImageView) view
						.findViewById(R.id.category_preference_downbtn);
				view.findViewById(R.id.category_preference_checkbox)
						.setFocusable(false);
				view.findViewById(R.id.category_preference_checkbox)
						.setOnClickListener(this);
				view.findViewById(R.id.category_preference_checkbox).setTag(
						position);

				view.setTag(viewHolder);
				itemLength = (Integer) categoryList.get(position).get(
						"subCatLength");
				isItemExpanded = (Boolean) categoryList.get(position).get(
						"expanded");
				viewHolder.categoryPreferenceText
						.setText((CharSequence) categoryList
								.get(position)
								.get(PreferedCatagoriesScreen.TAG_MAIN_PREFERENCES_NAME));
				viewHolder.categoryPreferenceDownbtn
						.setVisibility(View.INVISIBLE);
				if (itemLength > 1) {
					viewHolder.categoryPreferenceDownbtn
							.setVisibility(View.VISIBLE);
					if (isItemExpanded) {
						viewHolder.categoryPreferenceDownbtn
								.setImageResource(R.drawable.acc_up);
					} else {
						viewHolder.categoryPreferenceDownbtn
								.setImageResource(R.drawable.acc_down);
					}
				}
				itemDisp = (Float) categoryList.get(position).get("isAdded");
				if (itemDisp == 1f) {
					((ImageView) view
							.findViewById(R.id.category_preference_checkbox))
							.setImageResource(R.drawable.checkbox_marked);
				} else if (itemDisp == 0f) {
					((ImageView) view
							.findViewById(R.id.category_preference_checkbox))
							.setImageResource(R.drawable.checkbox_unmarked);
				} else {
					((ImageView) view
							.findViewById(R.id.category_preference_checkbox))
							.setImageResource(R.drawable.checkmarkpart);

				}
			} else if (itemName.equalsIgnoreCase(TAG_SUB_PREFERENCES_MENU)) {
				view = inflater.inflate(R.layout.listitem_preferences_subcat,
						parent,false);
				viewHolder = new ViewHolder();
				viewHolder.categoryPreferenceText = (TextView) view
						.findViewById(R.id.category_preference_cat_text);
				viewHolder.categoryPreferenceSubcatText = (TextView) view
						.findViewById(R.id.category_preference_subcat_text);

				viewHolder.categoryPreferenceDownbtn = (ImageView) view
						.findViewById(R.id.category_preference_subcat_checkbox);
				view.setTag(viewHolder);
				viewHolder.categoryPreferenceSubcatText
						.setText((CharSequence) categoryList.get(position).get(
								"subCatName"));
				viewHolder.categoryPreferenceText
						.setText((CharSequence) categoryList.get(position).get(
								"categoryName"));
				itemDisp = Integer.valueOf((String) categoryList.get(position)
						.get("isAdded"));
				if (itemDisp == 1) {
					viewHolder.categoryPreferenceDownbtn
							.setImageResource(R.drawable.checkbox_marked);
					viewHolder.categoryPreferenceText.setTextColor(activity
							.getResources().getColor(R.color.steel_blue));
				} else {
					viewHolder.categoryPreferenceDownbtn
							.setImageResource(R.drawable.checkbox_unmarked);
				}
			}
		}

		return view;
	}

	public static class ViewHolder {
		protected TextView categoryPreferenceText;
		protected TextView categoryPreferenceSubcatText;
		protected ImageView categoryPreferenceDownbtn;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.category_preference_checkbox:
			categoryPreferenceCheckbox(view);
			break;
		default:
			break;
		}

	}

	private void categoryPreferenceCheckbox(View view) {
		activity.savePrefbtn.setEnabled(true);
		int originalIndex = originalList.indexOf(categoryList
				.get((Integer) view.getTag()));

		itemLength = (Integer) originalList.get(originalIndex).get(
				"subCatLength");
		itemDisp = (Float) originalList.get(originalIndex).get("isAdded");
		if (originalIndex != 0) {
			for (int catCount = originalIndex + 1; catCount <= originalIndex
					+ itemLength; catCount++) {
				if (itemDisp == 1f || itemDisp == 0.5f) {
					originalList.get(catCount).put("isAdded", "0");
				} else {
					originalList.get(catCount).put("isAdded", "1");
				}
			}
		} else {
			for (int catCount = 0; catCount < originalList.size(); catCount++) {
				if (((String) originalList.get(catCount).get("itemName"))
						.equalsIgnoreCase(TAG_SUB_PREFERENCES_MENU)) {
					if (itemDisp == 1f || itemDisp == 0.5f) {
						originalList.get(catCount).put("isAdded", "0");
					} else {
						originalList.get(catCount).put("isAdded", "1");
					}
				}
			}
		}
		if (itemDisp == 1f || itemDisp == 0.5f) {
			originalList.get(originalIndex).put("isAdded", 0f);
		} else {
			originalList.get(originalIndex).put("isAdded", 1f);
		}
		filltrueList();
		activity.categoryListAdapter = new CategoryListAdapter(activity,
				categoryList, originalList);
		Parcelable stateMain = activity.categoryListView.onSaveInstanceState();
		activity.categoryListView.setAdapter(activity.categoryListAdapter);
		activity.categoryListView.onRestoreInstanceState(stateMain);

	}

	private void filltrueList() {
		categoryList = new ArrayList<>();
		if (!originalList.isEmpty()) {
			for (int catCount = 0; catCount < originalList.size(); catCount++) {
				if (((String) originalList.get(catCount).get("itemName"))
						.equalsIgnoreCase(TAG_MAIN_PREFERENCES_MENU)) {
					itemLength = (Integer) originalList.get(catCount).get(
							"subCatLength");
					int localSum = 0;
					for (int subcount = catCount + 1; subcount <= catCount
							+ itemLength; subcount++) {
						localSum += Integer.valueOf((String) originalList.get(
								subcount).get("isAdded"));

					}

					if (itemLength - localSum == 0) {
						originalList.get(catCount).put("isAdded", 1f);
					} else if (itemLength - localSum == itemLength) {
						originalList.get(catCount).put("isAdded", 0f);
					} else {
						originalList.get(catCount).put("isAdded", 0.5f);
					}
				}
				if ((Boolean) originalList.get(catCount).get("visibility")) {
					categoryList.add(originalList.get(catCount));
				}
			}
			float allSelected = 0f;
			float maincatCount = 0f;
			for (int catCount = 1; catCount < originalList.size(); catCount++) {
				if (((String) originalList.get(catCount).get("itemName"))
						.equalsIgnoreCase(TAG_MAIN_PREFERENCES_MENU)) {
					maincatCount += 1f;
					allSelected += (Float) originalList.get(catCount).get(
							"isAdded");
				}
			}

			if (maincatCount - allSelected == 0) {
				originalList.get(0).put("isAdded", 1f);
			} else if (maincatCount - allSelected == maincatCount) {
				originalList.get(0).put("isAdded", 0f);
			} else {
				originalList.get(0).put("isAdded", 0.5f);
			}

		}
	}
}
