package com.hubcity.android.screens;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by supriya.m on 10/4/2016.
 */
public class ThreeCrossThreeTemplate extends MenuPropertiesActivity implements
        AdapterView.OnItemClickListener {

    private static String TAG = "MainMenuGridTemplate";
    protected CustomImageLoader customImageLoader;
    private GridView gridview;
    private GridViewAdapter gridviewAdapter;
    private ArrayList<MainMenuBO> mainMenuUnsortedList;
    private String mBannerImg;
    private ImageView mBannerImageView;
    protected int latitude;
    protected int longitude;
    private LinearLayout gridViewParent;
    ProgressBar progressBar;
    private String templateName;
    private LinearLayout parentIntermideate;
    private RelativeLayout bannerHolder;
    private Context mContext;
    private CustomHorizontalTicker customHorizontalTicker;
    private CustomVerticalTicker customVerticalTicker;
    private LinearLayout bannerParent;
    private TextSwitcher customRotate;
    private View menuListTemplate;

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
        calculateDeviceScreenwidth();
        super.onNewIntent(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @SuppressLint("InflateParams")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        mContext = ThreeCrossThreeTemplate.this;
        CommonConstants.hamburgerIsFirst = true;
        previousMenuLevel = intent.getExtras().getString(
                Constants.MENU_LEVEL_INTENT_EXTRA);
        parent = new RelativeLayout(this);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        parent.setLayoutParams(param);

        parentIntermideate = new LinearLayout(this);
        parentIntermideate.setOrientation(LinearLayout.VERTICAL);

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        menuListTemplate = layoutInflater.inflate(R.layout.three_cross_three,
                null, false);

        mBannerImageView = (ImageView) menuListTemplate
                .findViewById(R.id.top_image);
        mBannerImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        progressBar = (ProgressBar) menuListTemplate.findViewById(R.id.progress);
        bannerHolder = (RelativeLayout) menuListTemplate.findViewById(R.id.banner_holder);
        bannerParent = (LinearLayout) menuListTemplate.findViewById(R.id.bannerParent);

        gridViewParent = (LinearLayout) menuListTemplate
                .findViewById(R.id.gridView_parent);

        gridview = (GridView) gridViewParent.findViewById(R.id.gridView);

        gridview.setOnItemClickListener(this);
        LinearLayout.LayoutParams gridViewParentParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        BottomButtonListSingleton mBottomButtonListSingleton;
        mBottomButtonListSingleton = BottomButtonListSingleton
                .getListBottomButton();

        if (mBottomButtonListSingleton != null) {

            ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                    .listBottomButton;

            if (listBottomButton != null && listBottomButton.size() > 1) {
                int px = (int) (40 * this.getResources().getDisplayMetrics().density + 0.5f);
                gridViewParentParam.setMargins(0, 0, 0, px);
            }
        }

        gridViewParent.setLayoutParams(gridViewParentParam);
        LinearLayout.LayoutParams parentParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        parentIntermideate.setLayoutParams(parentParam);
        // Initializing drawer layout
        drawer = (DrawerLayout) menuListTemplate.findViewById(R.id.drawer_layout);
        addTitleBar(parentIntermideate, parent);
        parentIntermideate.addView(gridViewParent);
        parent.addView(parentIntermideate);

        createbottomButtontTab(parent);
        setContentView(parent);

        handleIntent(intent);

        calculateDeviceScreenwidth();
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            HubCityContext.level = 0;
        } else {
            HubCityContext.level = HubCityContext.level + 1;
        }

        if (!MenuAsyncTask.isNewTicker.equalsIgnoreCase("1")) {
            if ((mBannerImg != null) && !("N/A".equalsIgnoreCase(mBannerImg))) {
                progressBar.setVisibility(View.VISIBLE);
                mBannerImg = mBannerImg.replaceAll(" ", "%20");
                Picasso.with(ThreeCrossThreeTemplate.this).load(mBannerImg).into(mBannerImageView, new
                        Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
            } else {
                bannerParent.setVisibility(View.GONE);
            }
        } else {
            customHorizontalTicker = (CustomHorizontalTicker) menuListTemplate.findViewById(R.id.custom_horizontal_ticker);
            customVerticalTicker = (CustomVerticalTicker) menuListTemplate.findViewById(R.id.custom_vertical_ticker);
            customRotate = (TextSwitcher) menuListTemplate.findViewById(R.id.rotate);
            startTickerMode(mContext, customHorizontalTicker, customVerticalTicker, bannerParent, customRotate, bannerHolder);
        }
    }

    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            gridview.setBackground(new BitmapDrawable(getResources(), bitmap));
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    private void backgroundColorSetting() {

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {

            if ((mBkgrdColor != null) && !("N/A".equalsIgnoreCase(mBkgrdColor))) {
                gridViewParent
                        .setBackgroundColor(Color.parseColor(mBkgrdColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(mBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(mBkgrdColor));
            } else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
                mBkgrdImage = mBkgrdImage.replace(" ", "%20");

                WeakReference<Target> ref = new WeakReference<Target>(target);
                Picasso.with(this).load(mBkgrdImage).into(ref.get());

            }
        } else {
            if ((smBkgrdColor != null)
                    && !("N/A".equalsIgnoreCase(smBkgrdColor))) {
                gridViewParent.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parentIntermideate.setBackgroundColor(Color
                        .parseColor(smBkgrdColor));
                parent.setBackgroundColor(Color.parseColor(smBkgrdColor));
            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {
                new ImageLoaderAsync1(gridview).execute(smBkgrdImage);
            }
        }


    }

    @Override
    protected void onPause() {
        //gridViewParent.setBackgroundColor(Color.parseColor("#ffffff"));
        //parentIntermideate.setBackgroundColor(Color.parseColor("#ffffff"));
        //parent.setBackgroundColor(Color.parseColor("#ffffff"));
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        backgroundColorSetting();
        activity = this;

        if (CommonConstants.hamburgerIsFirst) {
            callSideMenuApi((CustomNavigation) menuListTemplate.findViewById(R.id.custom_navigation),false);
            CommonConstants.hamburgerIsFirst = false;
        }
    }

    class ImageLoaderAsync1 extends AsyncTask<String, Void, Bitmap> {
        View bmImage;

        public ImageLoaderAsync1(View bmImage) {
            this.bmImage = bmImage;
        }

        //private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {

            //mDialog = ProgressDialog.show(MainMenuGridTemplate.this, "",
            //Constants.DIALOG_MESSAGE, true);
            //mDialog.setCancelable(false);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay = urldisplay.replaceAll(" ", "%20");

            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(Bitmap bmImg) {

            BitmapDrawable background = new BitmapDrawable(bmImg);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                bmImage.setBackgroundDrawable(background);
            } else {
                bmImage.setBackgroundDrawable(background);
            }

//			try {
////				if ((this.mDialog != null) && this.mDialog.isShowing()) {
////					this.mDialog.dismiss();
////				}
//			} catch (final IllegalArgumentException e) {
//				e.printStackTrace();
//			} catch (final Exception e) {
//				e.printStackTrace();
//			} finally {
//				this.mDialog = null;
//			}
        }
    }

    private void calculateDeviceScreenwidth() {

        ViewTreeObserver vto = gridViewParent.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {

                ViewGroup.MarginLayoutParams vlpBaner = (ViewGroup.MarginLayoutParams) bannerParent
                        .getLayoutParams();
                ViewGroup.MarginLayoutParams vlpGrid = (ViewGroup.MarginLayoutParams) gridview
                        .getLayoutParams();
//                int height = gridViewParent.getMeasuredHeight()
//                        - (bannerParent.getMeasuredHeight()
//                        + vlpBaner.topMargin + vlpBaner.bottomMargin);
                int height = gridViewParent.getMeasuredHeight();
                int width = gridViewParent.getMeasuredWidth()
                        - vlpGrid.rightMargin - vlpGrid.leftMargin;

                ViewTreeObserver obs = gridViewParent.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    obs.removeGlobalOnLayoutListener(this);
                }
                if (mainMenuUnsortedList != null) {
                    if (templateName.contains("Square")) {
                        gridviewAdapter = new GridViewAdapter(
                                ThreeCrossThreeTemplate.this, R.layout.row_grid,
                                mainMenuUnsortedList, previousMenuLevel, width, height,
                                mFontColor, smFontColor, bannerParent, true);
                        gridview.setAdapter(gridviewAdapter);
                    } else {
                        gridviewAdapter = new GridViewAdapter(
                                ThreeCrossThreeTemplate.this, R.layout.row_grid,
                                mainMenuUnsortedList, previousMenuLevel, width, height,
                                mFontColor, smFontColor, bannerParent, false);
                        gridview.setAdapter(gridviewAdapter);
                    }
                }
            }
        });

    }

    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            previousMenuLevel = intent.getExtras().getString(
                    Constants.MENU_LEVEL_INTENT_EXTRA);
            mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
            smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
            mBkgrdImage = intent.getExtras().getString("smBkgrdImage");
            departFlag = intent.getExtras().getString("departFlag");
            typeFlag = intent.getExtras().getString("typeFlag");
            mBannerImg = intent.getExtras().getString("mBannerImg");
            mFontColor = intent.getExtras().getString("mFontColor");
            smFontColor = intent.getExtras().getString("smFontColor");

            mLinkId = intent.getExtras().getString("mLinkId");
            mItemId = intent.getExtras().getString("mItemId");
            templateName = intent.getExtras().getString("rectangularTemp");
            calculateDeviceScreenwidth();
        }

        super.handleIntent(intent);

    }

    private void onTemplateListMenuRowItemClick(final AdapterView<?> arg0,
                                                final View view, final int position, final long id) {
        MainMenuBO mMainMenuBO = mainMenuUnsortedList
                .get(position);

        super.mItemName = mMainMenuBO.getmItemName();
        super.mItemId = mMainMenuBO.getmItemId();
        super.linkTypeId = mMainMenuBO.getLinkTypeId();
        super.linkTypeName = mMainMenuBO.getLinkTypeName();
        super.mMainMenuBOPosition = mMainMenuBO.getPosition();
        super.mItemImgUrl = mMainMenuBO.getmItemImgUrl();
        super.mLinkId = mMainMenuBO.getLinkId();
        super.mBtnColor = mMainMenuBO.getmBtnColor();
        super.mBtnFontColor = mMainMenuBO.getmBtnFontColor();
        super.smBtnColor = mMainMenuBO.getSmBtnColor();
        super.smBtnFontColor = mMainMenuBO.getSmBtnFontColor();
        linkClickListener(linkTypeName);
    }

    @Override
    public void onItemClick(final AdapterView<?> arg0, final View view,
                            final int position, final long id) {
        onTemplateListMenuRowItemClick(arg0, view, position, id);
    }

}
