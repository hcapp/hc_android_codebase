package com.hubcity.android.screens;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("rawtypes")
public class FindSingleCategoryListAdapter extends ArrayAdapter
{

	private ArrayList<Item> items;
	private LayoutInflater vi;

	ArrayList<HashMap<String, String>> mArrayList;
	protected CustomImageLoader customImageLoader;
	Activity activity;

	@SuppressWarnings("unchecked")
	public FindSingleCategoryListAdapter(Context context,
			ArrayList<Item> items, String sortby,
			ArrayList<HashMap<String, String>> findlocationList,
			Activity activity)
	{
		super(context, 0, items);

		this.items = items;

		this.mArrayList = findlocationList;
		this.activity = activity;

		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		customImageLoader = new CustomImageLoader(context.getApplicationContext(), false);

	}

	@Override
	public int getCount()
	{
		return items.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		ViewHolder viewHolder;

		/***
		 * If the list item reaches to its last position then it will be called
		 * for next pagination
		 ***/
		if (position == items.size() - 1) {
			if (!((FindSingleCategory) activity).isAlreadyLoading) {
				if (((FindSingleCategory) activity).nextPage) {
					((FindSingleCategory) activity).isAlreadyLoading = true;
					((FindSingleCategory) activity).isViewMore = true;
					((FindSingleCategory) activity).enableBottomButton = false;
					// Added to disable toggle click
					CommonConstants.disableClick = true;
					((FindSingleCategory) activity).callFindAsyncTask();
				}
			}
		}

		final Item i = items.get(position);
		if (i != null) {
				viewHolder = new ViewHolder();

				FindSingleCategoryEntryItem ei = (FindSingleCategoryEntryItem) i;

				v = vi.inflate(R.layout.find_location_main_menu, parent, false);

				viewHolder.image = (ImageView) v
						.findViewById(R.id.retailer_image);

				viewHolder.retailerName = (TextView) v
						.findViewById(R.id.find_category_name);
				viewHolder.address = (TextView) v
						.findViewById(R.id.find_category_address);
				viewHolder.address2 = (TextView) v
						.findViewById(R.id.find_category_address2);
				viewHolder.distance = (TextView) v
						.findViewById(R.id.distance);
				viewHolder.retailerStatus = (TextView) v
						.findViewById(R.id.find_retailer_status);
				viewHolder.retilerSpecial = (ImageView) v
						.findViewById(R.id.retailer_specials);

				if (viewHolder.retailerName != null) {
					viewHolder.retailerName.setText(ei.retailerName);
				}
				StringBuffer address = new StringBuffer();
				if (ei.address2 != null && !ei.address2.equals("") && !ei.address2.equals("N/A")) {
					address.append(ei.address1 + ", " + ei
							.address2);
				} else {
					address.append(ei.address1);
				}
			if (ei.locationOpen != null && !ei.locationOpen.isEmpty() && !ei.locationOpen.equalsIgnoreCase("N/A")) {
				viewHolder.retailerStatus.setText(ei.locationOpen);
			}
				if (viewHolder.address != null) {
					viewHolder.address.setText(address);
				}
				viewHolder.address2.setText(ei.city + ", " + ei.state + ", " + ei
						.postalCode);
				if (viewHolder.distance != null) {
					if (ei.distance.length() > 0) {
						viewHolder.distance.setText(ei.distance);
					} else {
						viewHolder.distance.setVisibility(View.GONE);
					}
				}
				if ("false".equalsIgnoreCase(ei.saleFlag)) {
					viewHolder.retilerSpecial.setVisibility(View.INVISIBLE);
				}

				if (viewHolder.image != null) {

					if (ei.url != null) {
						viewHolder.image.setTag(ei.url);
						customImageLoader.displayImage(ei.url, activity,
								viewHolder.image);
						// new
						// DownloadImageTask(viewHolder.image).execute(ei.url);
					} else {
						viewHolder.image.setImageBitmap(null);
					}
				}


		}

		return v;

	}

	public static class ViewHolder
	{
		protected ImageView image;
		protected ImageView retilerSpecial;
		protected TextView retailerName;
		protected TextView retailerStatus;
		protected TextView address;
		protected TextView address2;
		protected TextView distance;

	}


}
