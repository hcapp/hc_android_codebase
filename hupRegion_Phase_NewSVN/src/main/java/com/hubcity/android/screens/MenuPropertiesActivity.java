    package com.hubcity.android.screens;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.text.method.DigitsKeyListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.ImageLoaderAsync;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UpdateZipCode;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.govqa.GovQaLogin;
import com.hubcity.twitter.PrepareRequestTokenActivity;
import com.hubcity.twitter.TwitterUtils;
import com.scansee.android.BandsLandingPageActivity;
import com.scansee.android.CouponsActivty;
import com.scansee.android.FilterAndSortScreen;
import com.scansee.android.FindActivity;
import com.scansee.android.MyCouponActivity;
import com.scansee.android.ScanNowSplashActivity;
import com.scansee.android.ThisLocationGetLocation;
import com.scansee.android.ThisLocationGetRetailers;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.CommonMethods;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.NavigationItem;
import com.scansee.newsfirst.NewsDetailsActivity;
import com.scansee.newsfirst.NewsSideMenu;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.SideMenuObject;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.RetrofitError;
import retrofit.client.Response;


public class MenuPropertiesActivity extends FragmentActivity implements OnClickListener {

    protected static int RESULTCODE = 11;
    protected String mBannerImg;
    protected String smBtnFontColor;
    protected String smBtnColor;
    protected String mBtnFontColor;
    protected String mBtnColor;
    protected String mLinkId;
    protected String mItemImgUrl;
    protected String linkTypeName;
    protected String linkTypeId;
    protected String mItemId;
    protected String mItemName;
    protected String mShapeName;
    protected int mMainMenuBOPosition;
    protected String previousMenuLevel;
    protected String catIds;
    protected String searchKey;
    protected String mBkgrdColor;
    protected String templateBgColor;
    protected String mBkgrdImage;
    protected String smBkgrdColor;
    protected String smBkgrdImage;
    protected String mFontColor;
    protected String smFontColor;
    protected String departFlag;
    protected String typeFlag;
    public DrawerLayout drawer;
    private SharedPreferences prefs;
    private final Handler mTwitterHandler = new Handler();
    protected AsyncTask<String, String, ArrayList<MainMenuBO>> mMenuAsyncTask;
    protected LinearLayout includeBottomControllTab;
    protected ImageView b1, b2, b3, b4;
    protected View bottomControllTabBar;
    protected String cityExpImgUlr;
    protected ArrayList<MainMenuBO> mainMenuUnsortedList = new ArrayList<>();
    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
    protected String bottomBtnId, bottomBtnLinkId, bottomBtnLinkTypeID,
            bottomBtnLinkTypeName, bottomBtnPosition;
    private String appPlaystoreLInk, appleStoreLink, appIconImg;
    private static String APP_PLAYSTORE_LINK = "androidLink";
    public static String menuName = "";
    protected String mGrpBkgrdColor;
    protected String mGrpFntColor;
    protected String sGrpBkgrdColor;
    protected String sGrpFntColor;
    protected TextView title;
    protected Button leftTitleImage;
    protected ImageView rightImage, backImage, drawerIcon;
    protected RelativeLayout parent;
    protected TextView divider;
    protected String templateName;
    String retAffCount = null;
    String retAffId = null;
    String retAffName = null;
    String cityExpId = null;
    protected static String GROUPED_TAB = "Grouped Tab";
    protected static String GROUPED_TAB_WITH_IMAGE = "Grouped Tab With Image";
    static MenuAsyncTask csMenuTask;
    public static Activity activity;
    public boolean isRefresh;

    //newsTicker Param
    private boolean leftToRight = false;
    private boolean topToBottom = false;
    private boolean rotateLeft = false;
    private int width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Relaunch of app when app crashes
        if (savedInstanceState != null) {
            Intent intent = new Intent(this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }
        activity = this;


        new VersionCheckAsyncTask(activity).execute();
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {

//			If launching the app for first time and not a guest user then showing this pop up to
//					allow or disallow push notification
            SharedPreferences setprefs = getSharedPreferences(
                    Constants.PREFERENCE_HUB_CITY, 0);
            boolean isFirstLaunch = setprefs.getBoolean("isFirstLaunch", true);
            if (isFirstLaunch) {
                setprefs.edit().putBoolean("isFirstLaunch", false).apply();
                Constants.IS_PREFERENCE_CHANGED = false;
                Constants.IS_SUB_MENU_REFRESHED = false;
                showPushNotificationAlert(activity);

            } else {
                if (Constants.SHOWGPSALERT) {
                    showGPsAlert();
                }
            }
        }
        setContentView(R.layout.buttom_controll_tab);
        screenResolution();
    }


    // Added by Sharanamma
    // Side menu API call : includes hubciti and news functionalities

    List<NavigationItem> sideMenuDataList = new LinkedList<>();
    String userStatusText;

    public void callSideMenuApi(final CustomNavigation customNavigation, final boolean isNewsTemplate) {
        NewsSideMenu sideMenuRequest = UrlRequestParams.getSideMenu("0", "1", "true");
        RestClient.getInstance().getSideNavMenu(sideMenuRequest, new retrofit.Callback<SideMenuObject>() {

            @Override
            public void success(SideMenuObject sideMenuObject, Response response) {
                String responseText = sideMenuObject.getResponseText();
                int hubCitiPos = 0;
                if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                    userStatusText = "Logout";
                } else {
                    userStatusText = "Login/SignUp";
                }

                //Adding login/logout button in list
               /* NavigationItem navItem = new NavigationItem();
                navItem.setCatName(userStatusText);
                navItem.setLinkTypeName(userStatusText);
                navItem.setFlag(0);
*/
                if (responseText.equalsIgnoreCase("Success")) {
                    if (sideMenuObject.getListCatDetails().size() != 0) {
                        if (sideMenuObject.getListCatDetails().get(0)
                                .getListMainCat() != null) {
                            hubCitiPos = 1;
                        }
                        sideMenuDataList = CommonMethods.arrangeInSortedOrder(sideMenuObject, hubCitiPos);
                     /*   NavigationItem navItem1 = new NavigationItem();
                        navItem1.setCatName("News Settings");
                        navItem1.setFlag(2);
                        sideMenuDataList.add(navItem1);
*/
                    }

                }
                else
                {
                    NavigationItem navItem = new NavigationItem();
                    navItem.setCatName(userStatusText);
                    navItem.setLinkTypeName(userStatusText);
                    navItem.setFlag(1);
                    sideMenuDataList.add(navItem);
                }
//                sideMenuDataList.add(navItem);
                customNavigation.populateSideMenu(sideMenuObject, sideMenuDataList, drawer, hubCitiPos);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    System.out.println("error :" + error.getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    public void startTickerMode(final Context mContext, CustomHorizontalTicker customHorizontalTicker, CustomVerticalTicker customVerticalTicker, LinearLayout bannerParent, TextSwitcher customRotate, RelativeLayout bannerHolder) {
        try {
            Display display = getWindowManager().getDefaultDisplay();
            width = display.getWidth();

            if (MenuAsyncTask.tickerMode.equalsIgnoreCase(activity.getString(R.string.scrolling))) {
                if (MenuAsyncTask.tickerDirection.equalsIgnoreCase(activity.getString(R.string.right))) {
                    leftToRight = true;
                }


                if (MenuAsyncTask.tickerDirection.equalsIgnoreCase(activity.getString(R.string.down))) {
                    topToBottom = true;
                }


                if ((MenuAsyncTask.tickerDirection.equalsIgnoreCase(activity.getString(R.string.right))
                        || MenuAsyncTask.tickerDirection.equalsIgnoreCase(activity.getString(R.string.left)))) {
                    customHorizontalTicker.setVisibility(View.VISIBLE);
                } else {
                    customVerticalTicker.setVisibility(View.VISIBLE);
                }

            } else {
                if (MenuAsyncTask.tickerDirection.equalsIgnoreCase(activity.getString(R.string.left))) {
                    rotateLeft = true;
                }

                customRotate.setVisibility(View.VISIBLE);
                if (customRotate.getChildCount() < 1) {
                    if (MenuAsyncTask.tickerItem != null) {
                        startRorateTicker(mContext, customRotate);
                    }
                }

            }
            bannerHolder.setVisibility(View.GONE);
            if (MenuAsyncTask.tickerBkgrdColor != null) {
                bannerParent.setBackgroundColor(Color.parseColor(MenuAsyncTask.tickerBkgrdColor));
            }
            treeObserver(bannerParent, customHorizontalTicker, customVerticalTicker);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startRorateTicker(final Context mContext, final TextSwitcher customRotate) {
        final int[] currentIndex = {-1};
        customRotate.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                FrameLayout.LayoutParams textParam = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                textParam.setMargins(20, 0, 20, 0);
                TextView tickerText = new TextView(mContext);
                tickerText.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                tickerText.setTextSize(16);
                tickerText.setMaxLines(2);
                tickerText.setEllipsize(TextUtils.TruncateAt.END);
                tickerText.setLayoutParams(textParam);
                tickerText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent ticker = new Intent(activity, NewsDetailsActivity.class);
                        ticker.putExtra("itemId", Integer.parseInt(MenuAsyncTask.tickerItem.get(currentIndex[0]).getRowCount()));
                        activity.startActivity(ticker);
                    }
                });
                if (MenuAsyncTask.tickerTxtColor != null) {
                    tickerText.setTextColor(Color.parseColor(MenuAsyncTask.tickerTxtColor));
                }
                return tickerText;
            }
        });

        Animation in;
        Animation out;
        if (rotateLeft) {
            in = AnimationUtils.loadAnimation(mContext, R.anim.right_fade_in);
            out = AnimationUtils.loadAnimation(mContext, R.anim.right_fade_out);
        } else {
            in = AnimationUtils.loadAnimation(mContext, R.anim.left_fade_in);
            out = AnimationUtils.loadAnimation(mContext, R.anim.left_fade_out);
        }

        customRotate.setInAnimation(in);
        customRotate.setOutAnimation(out);

        final Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (MenuAsyncTask.tickerItem.size() != 0) {
                                currentIndex[0]++;
                                if (currentIndex[0] >= MenuAsyncTask.tickerItem.size()) {
                                    currentIndex[0] = 0;
                                }
                                if (MenuAsyncTask.tickerItem != null) {
                                    if (MenuAsyncTask.tickerItem.size() != 0) {
                                        if (MenuAsyncTask.tickerItem.get(currentIndex[0]).getTitle() != null && !MenuAsyncTask.tickerItem.get(currentIndex[0]).getTitle().isEmpty())

                                        {
                                            customRotate.setText(MenuAsyncTask.tickerItem.get(currentIndex[0]).getTitle());
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, 0, 2500);
    }

    private void treeObserver(final LinearLayout bannerParent, final CustomHorizontalTicker customHorizontalTicker, final CustomVerticalTicker customVerticalTicker) {
        ViewTreeObserver parentView = bannerParent.getViewTreeObserver();
        parentView.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                screenHeight = bannerParent.getMeasuredHeight();
                if (customHorizontalTicker.getVisibility() != customHorizontalTicker.GONE) {
                    customHorizontalTicker.startTextAnimation(width, leftToRight);
                }
                if (customVerticalTicker.getVisibility() != customVerticalTicker.GONE) {
                    customVerticalTicker.startTextAnimation(screenHeight, topToBottom);
                }

                ViewTreeObserver obs = bannerParent.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

            }
        });
    }


    String radius;

    /**
     * @Rekha Shows Alert for the user to ask if he wants to turn on the Push
     * Notification Service Only during the first launch of the app
     */
    private void showPushNotificationAlert(final Context context) {
        final SharedPreferences setprefs = getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, 0);

        try {
            AlertDialog.Builder notificationAlert = new AlertDialog.Builder(context);
            notificationAlert
                    .setMessage(
                            "Do you want to Enable Push Notification Service for "
                                    + activity.getResources().getString(
                                    R.string.app_name) + " App?")
                    .setNegativeButton(context.getString(R.string.alert_loc_dont),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    setprefs.edit()
                                            .putBoolean("push_status", false)
                                            .apply();
                                   /* new SaveUserSettingsAsync(MenuPropertiesActivity.this, String
                                            .valueOf
                                                    (false), false).execute(radius);*/
                                    new GetUserSettingsAsync(MenuPropertiesActivity.this, false).execute();
                                    dialog.dismiss();
                                    if (Constants.SHOWGPSALERT) {
                                        showGPsAlert();
                                    }
                                }

                            })
                    .setPositiveButton(context.getString(R.string.alert_loc_allow),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    setprefs.edit().putBoolean("push_status", true)
                                            .apply();
                                   /* new SaveUserSettingsAsync(MenuPropertiesActivity.this, String
                                            .valueOf
                                                    (true), false).execute(radius);*/
                                    new GetUserSettingsAsync(MenuPropertiesActivity.this, true).execute();
                                    dialog.dismiss();
                                    if (Constants.SHOWGPSALERT) {
                                        showGPsAlert();
                                    }
                                }
                            });
            notificationAlert.create().show();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    // @Ramachandran
    @Override
    protected void onResume() {
        super.onResume();
        Log.v("", "MENU RESUME : " + LoginScreenViewAsyncTask.bIsLoginFlag);
        CommonConstants.previousMenuLevel = previousMenuLevel;
        CommonConstants.commonLinkId = mLinkId;
        CommonConstants.mBkgrndColor = mBkgrdColor;
        CommonConstants.mBkgrndImage = mBkgrdImage;
        CommonConstants.smBkgrndColor = smBkgrdColor;
        CommonConstants.smBkgrndImage = smBkgrdImage;


        if (Constants.IS_PREFERENCE_CHANGED) {

            if (previousMenuLevel != null
                    && ("1").equals(previousMenuLevel)) {
                Constants.IS_PREFERENCE_CHANGED = false;

                StartMenuActivity();

            } else if (Constants.IS_SUB_MENU_REFRESHED) {
                Constants.IS_SUB_MENU_REFRESHED = false;
                startSubMenuActivity(mItemId, mLinkId, true);
            }
        } else {
            if (HubCityContext.isRefreshSubMenu && !("1").equals(previousMenuLevel)) {
                HubCityContext.isRefreshSubMenu = false;
                if (mItemId != null || mLinkId != null) {
                    startSubMenuActivity(mItemId, mLinkId, true);
                }
            }
        }
        //Added this code to refresh main menu on press of back from any screen
        //isRefresh boolean is added to refresh the main menu after creation
        if (isRefresh) {
            if (previousMenuLevel != null
                    && ("1").equals(previousMenuLevel)) {
                Constants.IS_PREFERENCE_CHANGED = false;
                StartMenuActivity();
            }
        }
        isRefresh = true;

        try {
            if (Constants.getNavigationBarValues(Constants.IS_TEMP_USER).equals("1")) {
                CommonConstants.resetPassword(this, false);
            }

            if (Constants.getNavigationBarValues(Constants.NO_EMAIL_COUNT).equals("1")) {
                CommonConstants.emailConfigurationDialog(this);
                Constants.setNavigationBarValues(Constants.NO_EMAIL_COUNT, "0");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 6252) {
            if (btnClosePopup != null) {
                btnClosePopup.performClick();
            }
        }
        if (resultCode == RESULTCODE) {

            finish();

        }
        if (resultCode == 2) {
            if (MenuAsyncTask.menuNames != null
                    && !MenuAsyncTask.menuNames.isEmpty()) {
                MenuAsyncTask.menuNames.remove(MenuAsyncTask.menuNames
                        .size() - 1);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showGPsAlert() {
        // *****GPS ALERT*****
        Constants.SHOWGPSALERT = false;
        settings = getSharedPreferences(CommonConstants.PREFERANCE_FILE, 0);
        scanSeeLocationListener = new ScanSeeLocationListener();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        triggerLocationUpdates();
        // End
        try {
            isFirst = getIntent().getExtras().getBoolean("chkloc");
        } catch (Exception e) {
            isFirst = false;
            e.printStackTrace();
        }

        checkLocService();

        // ***End**GPS ALERT*****
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    protected void setupMainMenuContents(View parent) {

        if (previousMenuLevel != null && ("1").equals(previousMenuLevel)) {
            // i.e. this is main menu
            leftTitleImage.setVisibility(View.GONE);
//			title.setText(getResources().getString(R.string.app_name));
            title.setText("Main Menu");
            backImage.setVisibility(View.GONE);
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                rightImage.setVisibility(View.GONE);
                rightImage.setBackgroundResource(R.drawable.ic_action_logout);
                rightImage.setTag("Logout");
            } else {
                rightImage.setVisibility(View.GONE);
                rightImage.setBackgroundResource(R.drawable.signup);
                rightImage.setTag("Signup");
            }
            this.title.setEllipsize(TruncateAt.END);
            this.divider.setVisibility(View.INVISIBLE);
        } else {
            this.leftTitleImage.setVisibility(View.GONE);
            this.leftTitleImage.setTag("SubMenu");
            if (SubMenuStack.getTopElement() != null) {
                if (MenuAsyncTask.menuNames != null
                        && !MenuAsyncTask.menuNames.isEmpty()) {
                    String title = MenuAsyncTask.menuNames
                            .get(MenuAsyncTask.menuNames.size() - 1);
                    this.title.setText(title);
                    this.title.setMaxLines(2);

                }
            }

            this.rightImage.setVisibility(View.VISIBLE);
            //loading home image url to imageview
            if (Constants.getNavigationBarValues("homeImgPath") != null) {
                Picasso.with(activity).load(Constants.getNavigationBarValues("homeImgPath")
                        .replace(" ", "%20")).placeholder(R.drawable.loading_button).into
                        (rightImage);
            } else {
                rightImage.setBackgroundResource(R.drawable.mainmenu_white);
            }

            this.divider.setVisibility(View.INVISIBLE);
        }

        this.backImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Handle back button
                if (SubMenuStack.getTopElement() != null) {
                    SubMenuStack.pop();
                }

                if (MenuAsyncTask.mainIDs != null
                        && !MenuAsyncTask.mainIDs.isEmpty()) {
                    MenuAsyncTask.mainIDs.remove(MenuAsyncTask.mainIDs.size() - 1);
                }

                if (MenuAsyncTask.menuNames != null
                        && !MenuAsyncTask.menuNames.isEmpty()) {
                    MenuAsyncTask.menuNames.remove(MenuAsyncTask.menuNames
                            .size() - 1);
                }

                // }
                LoginScreenViewAsyncTask.bIsLoginFlag = false;
                HubCityContext.isResumed = false;
                // startMenu();
                finish();
            }
        });
        this.leftTitleImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (("SubMenu").equals(v.getTag())) {
                    if (MenuAsyncTask.mainIDs != null
                            && !MenuAsyncTask.mainIDs.isEmpty()) {
                        MenuAsyncTask.mainIDs.remove(MenuAsyncTask.mainIDs
                                .size() - 1);
                    }

                    if (MenuAsyncTask.menuNames != null
                            && !MenuAsyncTask.menuNames.isEmpty()) {
                        MenuAsyncTask.menuNames.remove(MenuAsyncTask.menuNames
                                .size() - 1);
                    }
                    LoginScreenViewAsyncTask.bIsLoginFlag = false;
                    HubCityContext.isResumed = false;
                    finish();
                }

            }
        });
        this.rightImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("Logout".equals(v.getTag())) {
                    logout();
                } else if ("Signup".equals(v.getTag())) {
                    signUp();
                } else {
                    if (GlobalConstants.isFromNewsTemplate) {
                        Intent intent = null;
                        switch (GlobalConstants.className) {
                            case Constants.COMBINATION:
                                intent = new Intent(MenuPropertiesActivity.this,
                                        CombinationTemplate.class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(MenuPropertiesActivity.this,
                                        ScrollingPageActivity.class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(MenuPropertiesActivity.this,
                                        TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    } else {
                        startMenuActivity();
                    }

                }

            }
        });

        try {
            if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
                if (mBkgrdColor != null && !"N/A".equals(mBkgrdColor)) {

                    ((LinearLayout) parent.findViewById(R.id.menu_list_parent))
                            .setBackgroundColor(Color.parseColor(mBkgrdColor));

                }

            } else {
                if (smBkgrdColor != null && !"N/A".equals(smBkgrdColor)) {

                    (parent.findViewById(R.id.menu_list_parent))
                            .setBackgroundColor(Color.parseColor(smBkgrdColor));

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void signUp() {
        finish();
        Intent intent = new Intent(MenuPropertiesActivity.this,
                RegistrationActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        MenuPropertiesActivity.this.startActivity(intent);
        SharedPreferences preferences = getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = preferences.edit().putString("radious", "");
        e.apply();
        new HubCityContext().clearArrayAndAllValues(true);
        //Added by sharanamma
        GlobalConstants.isFromNewsTemplate = false;
    }

    public void logout() {
//		logoutMenuPropertiesActivity.this.finish();
        Constants.setZipCode("");
        Constants.bISLoginStatus = false;
//		Utils.startLoginScreen(MenuPropertiesActivity.this);
        Constants.DONT_ALLOW_LOGIN = true;
        Constants.SHOWGPSALERT = true;
////		Constants.removeHubCitiId();
        HubCityContext.savedSubMenuCityIds = new HashMap<>();
        HubCityContext.arrSubMenuDetails.clear();
        SharedPreferences preferences = getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = preferences.edit().putString(

                Constants.PREFERENCE_KEY_TEMP_U_ID,
                preferences.getString(
                        Constants.PREFERENCE_KEY_U_ID, ""));
        e.apply();
        CommonConstants.clearLoginCredentials(activity);

        e = preferences.edit().putString("radious", "");
        e.apply();
        new HubCityContext().clearArrayAndAllValues(true);

        SharedPreferences.Editor prefEditor = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).edit();
        prefEditor.putBoolean(Constants.REMEMBER, false);
        prefEditor.apply();

        new LoginAsyncTask(activity, false, true, null, 0, 0,
                true, null)
                .execute(Constants.USERNAME, Constants.PASSWORD);
    }

    private HashMap<String, String> getListValues() {
        HashMap<String, String> values = new HashMap<String, String>();

        values.put("departFlag", departFlag);
        values.put("typeFlag", typeFlag);
        values.put("mLinkId", mLinkId);
        values.put("mItemId", getIntent().getExtras().getString("mItemId"));
        values.put("Class", "SubMenu");

        return values;

    }


    int screenWidth;
    int screenHeight;

    void screenResolution() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
    }

    @Override
    public void onBackPressed() {
// Handle back button
        if (SubMenuStack.getTopElement() != null) {
            SubMenuStack.pop();
        }
        if (MenuAsyncTask.mainIDs != null && !MenuAsyncTask.mainIDs.isEmpty()) {
            MenuAsyncTask.mainIDs.remove(MenuAsyncTask.mainIDs.size() - 1);
        }

        if (MenuAsyncTask.menuNames != null
                && !MenuAsyncTask.menuNames.isEmpty()) {
            MenuAsyncTask.menuNames.remove(MenuAsyncTask.menuNames.size() - 1);
        }
        LoginScreenViewAsyncTask.bIsLoginFlag = false;
        HubCityContext.isResumed = false;
        super.onBackPressed();

    }

    public void StartMenuActivity() {
        String level = "1";
        String mItemId = "0";
        String mLinkId = "0";
        MenuAsyncTask menu = new MenuAsyncTask(true, this);
        menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, level, mItemId,
                "0", "None", "0", "0");
    }

    @SuppressLint("InflateParams")
    void createbottomButtontTab(RelativeLayout parent) {
        ProgressBar p1, p2, p3, p4;
        // CREATE FOR BOTTON BUTTON TAB
        screenResolution();
        CustomImageLoader customImageLoader = new CustomImageLoader(this, false);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        bottomControllTabBar = layoutInflater.inflate(
                R.layout.buttom_controll_tab, null, false);
        bottomControllTabBar.setVisibility(View.VISIBLE);

        includeBottomControllTab = (LinearLayout) bottomControllTabBar
                .findViewById(R.id.include_buttom_controll_tab);
        //Setting backgroud color to bottom bar same as topbar background color
        if (Constants.getNavigationBarValues("titleBkGrdColor") != null) {
            includeBottomControllTab.setBackgroundColor(Color.parseColor(Constants
                    .getNavigationBarValues("titleBkGrdColor")));
        }
        includeBottomControllTab.setVisibility(View.VISIBLE);

        b1 = (ImageView) bottomControllTabBar.findViewById(R.id.b1);
        b2 = (ImageView) bottomControllTabBar.findViewById(R.id.b2);
        b3 = (ImageView) bottomControllTabBar.findViewById(R.id.b3);
        b4 = (ImageView) bottomControllTabBar.findViewById(R.id.b4);
        p1 = (ProgressBar) bottomControllTabBar.findViewById(R.id.progress_b1);
        p2 = (ProgressBar) bottomControllTabBar.findViewById(R.id.progress_b2);
        p3 = (ProgressBar) bottomControllTabBar.findViewById(R.id.progress_b3);
        p4 = (ProgressBar) bottomControllTabBar.findViewById(R.id.progress_b4);

        ArrayList<ImageView> bottonButtonViewList = new ArrayList<>();
        ArrayList<ProgressBar> progressBarList = new ArrayList<>();

        bottonButtonViewList.add(b1);
        bottonButtonViewList.add(b2);
        bottonButtonViewList.add(b3);
        bottonButtonViewList.add(b4);
        progressBarList.add(p1);
        progressBarList.add(p2);
        progressBarList.add(p3);
        progressBarList.add(p4);

        BottomButtonListSingleton mBottomButtonListSingleton;
        mBottomButtonListSingleton = BottomButtonListSingleton
                .getListBottomButton();

        if (mBottomButtonListSingleton != null) {
            ArrayList<BottomButtonBO> listBottomButton = BottomButtonListSingleton
                    .listBottomButton;

            if (listBottomButton == null) {
                includeBottomControllTab.setVisibility(View.GONE);
                return;
            }
            for (int i = 0; i < listBottomButton.size(); i++) {
                BottomButtonBO bottomButtonBO = listBottomButton.get(i);

                String bottomBtnImg = bottomButtonBO.getBottomBtnImg();

                ImageView bottonButtonImageView = bottonButtonViewList.get(i);
                final ProgressBar progressBar = progressBarList.get(i);
                if (bottomBtnImg != null) {
                    bottomBtnImg = bottomBtnImg.replaceAll(" ", "%20");
//                    customImageLoader.displayImage(bottomBtnImg, this,
//                            bottonButtonImageView);
                    Picasso.with(activity).load(bottomBtnImg).into(bottonButtonImageView, new
                            Callback() {
                                @Override
                                public void onSuccess() {
                                    progressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    progressBar.setVisibility(View.GONE);
                                }
                            });

                }
                setButtomButtonclickListener(bottonButtonImageView,
                        bottomButtonBO);
            }

            android.widget.RelativeLayout.LayoutParams buttomControllTabBarLayoutParam = new
                    RelativeLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, screenHeight / 11);
            buttomControllTabBarLayoutParam
                    .addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            bottomControllTabBar
                    .setLayoutParams(buttomControllTabBarLayoutParam);

            parent.addView(bottomControllTabBar);

        } else {
            includeBottomControllTab.setVisibility(View.GONE);
        }
    }

    void setButtomButtonclickListener(ImageView button,
                                      final BottomButtonBO bottomBtn) {

        if (button == null) {
            return;
        }

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                bottomBtnId = bottomBtn.getBottomBtnID();
                bottomBtnLinkId = bottomBtn.getBtnLinkID();
                bottomBtnLinkTypeID = bottomBtn.getBtnLinkTypeID();
                bottomBtnLinkTypeName = bottomBtn.getBtnLinkTypeName();
                bottomBtnPosition = bottomBtn.getPosition();


                if ("GovQA".equalsIgnoreCase(linkTypeName)) {
                    startGovQaScreen();
                } else if ("My Accounts".equalsIgnoreCase(linkTypeName) || "My Deals".equalsIgnoreCase(linkTypeName)) {
                    startMyAccount();
                } else if ("Fundraisers".equals(bottomBtnLinkTypeName)) {
                    startButtomBtnFundraiserScreen(bottomBtnId,
                            bottomBtnLinkId, "0", null);
                } else if ("Find".equals(bottomBtnLinkTypeName)) {
                    startButtomBtnFindScreen(bottomBtnId, mLinkId);
                } else if ("Filters".equals(bottomBtnLinkTypeName)) {
                    cityExpId = bottomBtn.getBtnLinkID();
                    startFilterScreen(true, bottomBtnId, mItemId);
                } else if ("SubMenu".equals(bottomBtnLinkTypeName)) {
                    startBottomBtnSubMenu();
                } else if ("City Experience".equals(bottomBtnLinkTypeName)) {
                    startCitiExpeienceScreen(bottomBtnId, bottomBtnLinkId, "0",
                            null, true);
                } else if ("About".equals(bottomBtnLinkTypeName)) {
                    startAboutScreen(true, bottomBtnId, mItemId);
                } else if ("Events".equals(bottomBtnLinkTypeName)) {
                    startEventsScreen(true, bottomBtnId, mItemId,
                            bottomBtnLinkId, mLinkId);
                } else if ("Band Events".equals(bottomBtnLinkTypeName)) {
                    startBandEventsScreen(true, bottomBtnId, mItemId,
                            bottomBtnLinkId, mLinkId);
                } else if ("playing today".equalsIgnoreCase(linkTypeName)) {
                    startEventMapScreen();
                } else if (bottomBtnLinkTypeName
                        .startsWith("FindSingleCategory-")) {
                    String[] catNames = bottomBtnLinkTypeName.split("-");
                    startFindSingleCategoryScreen(catNames[1], true,
                            bottomBtnId, mItemId, bottomBtnLinkId, mLinkId);
                } else if ("Alerts".equals(bottomBtnLinkTypeName)) {
                    startAlertScreen(true, bottomBtnId, mItemId);
                } else if ("AnythingPage".equals(bottomBtnLinkTypeName)) {
                    startAnythingPage(true, bottomBtnId, mItemId,
                            bottomBtnLinkId, mLinkId);
                } else if ("AppSite".equals(bottomBtnLinkTypeName)) {
                    starAppsitePage(true, bottomBtnId, mItemId,
                            bottomBtnLinkId, mLinkId);
                } else if ("Whats NearBy".equals(bottomBtnLinkTypeName)) {

                    if (!Constants.GuestLoginId.equals(UrlRequestParams
                            .getUid().trim())) {
                        startWhatsNearbyScreen(false, bottomBtnId, mItemId);
                    } else {
                        startFindActivityForGuestLogin(bottomBtnId, mItemId);
                    }

                } else if (bottomBtnLinkTypeName
                        .startsWith("EventSingleCategory-")) {
                    startEventsScreen(true, bottomBtnId, mItemId,
                            bottomBtnLinkId, mLinkId);
                } else if ("FAQ".equals(bottomBtnLinkTypeName)) {
                    startFaqScreen(true, bottomBtnId, mItemId);
                } else if ("Scan Now".equals(bottomBtnLinkTypeName)) {
                    startScannowScreen(true, bottomBtnId, mItemId);
                } else if ("Deals".equals(bottomBtnLinkTypeName)) {
                    startHotdealsScreen(true, bottomBtnId, mItemId);
                } // Settings
                else if ("User Information".equals(linkTypeName)) {
                    if (!Constants.GuestLoginId.equals(UrlRequestParams
                            .getUid().trim())) {
                        startUserInformation();
                    } else {
                        Constants.SignUpAlert(MenuPropertiesActivity.this);
                    }

                } else if ("Location Preferences".equals(linkTypeName)) {

                    if (!Constants.GuestLoginId.equals(UrlRequestParams
                            .getUid().trim())) {
                        startLocationPreference();
                    } else {
                        Constants.SignUpAlert(MenuPropertiesActivity.this);
                    }

                } else if ("Category Favorites".equals(linkTypeName)) {

                    if (!Constants.GuestLoginId.equals(UrlRequestParams
                            .getUid().trim())) {
                        startCategoryFavourites();
                    } else {
                        Constants.SignUpAlert(MenuPropertiesActivity.this);
                    }

                } else if ("City Favorites".equals(linkTypeName)) {

                    if (!Constants.GuestLoginId.equals(UrlRequestParams
                            .getUid().trim())) {
                        startCityFavourites();
                    } else {
                        Constants.SignUpAlert(MenuPropertiesActivity.this);
                    }

                } else if ("SortFilter".equalsIgnoreCase(bottomBtnLinkTypeName) || "Filter"
                        .equalsIgnoreCase(bottomBtnLinkTypeName)) {
                    startSortScreen();

                } else {
                    linkClickListener(bottomBtnLinkTypeName);
                }

            }
        });
    }

    private void startMyAccount() {
        //clearing entered zipcode of popup in coupon screen only from hamburger,menu screen and
        // bottom button of menu screen and not clearing from modules
        CouponsActivty.postalCode = null;
        Intent intent = new Intent(this, MyCouponActivity.class);
        this.startActivity(intent);
    }

    private void startSortScreen() {
        Intent intent = new Intent(MenuPropertiesActivity.this,
                FilterAndSortScreen.class);
        intent.putExtra("values", getListValues());
        intent.putExtra("rectangularTemp", menutemplateName);
        intent.putExtra("title", bottomBtnLinkTypeName);
        startActivityForResult(intent, 02);

    }

    @SuppressLint("InflateParams")
    protected void startFindActivityForGuestLogin(String bottomBtnId,
                                                  String mItemId) {

        Intent getLocationIntent = new Intent(getApplicationContext(),
                ThisLocationGetRetailers.class);
        getLocationIntent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                bottomBtnId);
        getLocationIntent
                .putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        getLocationIntent.putExtra(CommonConstants.ZIP_CODE,
                Constants.GuestLoginZipcode);
        this.startActivity(getLocationIntent);
    }

    @SuppressLint("InflateParams")
    protected View addTitleBar(View parentIntermediate, View parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View titleBar = layoutInflater.inflate(R.layout.title_bar, null, false);
        ((ViewGroup) parentIntermediate).addView(titleBar);

        //setting navigation bar background color
        if (Constants.getNavigationBarValues("titleBkGrdColor") != null) {
            (titleBar.findViewById(R.id.title_layout)).setBackgroundColor(Color
                    .parseColor(Constants.getNavigationBarValues("titleBkGrdColor")));
        } else {
            (titleBar.findViewById(R.id.title_layout)).setBackgroundColor
                    (getResources().getColor(R.color.black));
        }
        title = (TextView) titleBar.findViewById(R.id.title);
        //Setting title color
        if (Constants.getNavigationBarValues("titleTxtColor") != null) {
            title.setTextColor(Color.parseColor(Constants.getNavigationBarValues
                    ("titleTxtColor")));
        } else {
            title.setTextColor(getResources().getColor(R.color.white));
        }

        drawerIcon = (ImageView) titleBar.findViewById(R.id.drawer_icon);
        drawerIcon.setVisibility(View.VISIBLE);
        if (Constants.getNavigationBarValues("hamburgerImg") != null) {
            Picasso.with(activity).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (drawerIcon);
        }
        drawerIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (drawer.isDrawerOpen(Gravity.LEFT)) {
                        drawer.closeDrawer(Gravity.LEFT);
                    } else {
                        drawer.openDrawer(Gravity.LEFT);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        rightImage = (ImageView) titleBar.findViewById(R.id.right_button);
        leftTitleImage = (Button) titleBar.findViewById(R.id.left_title_image);
        backImage = (ImageView) titleBar.findViewById(R.id.back_btn);
        //Loading back image
        if (Constants.getNavigationBarValues("bkImgPath") != null) {
            Picasso.with(activity).load(Constants.getNavigationBarValues("bkImgPath").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (backImage);
        } else {
            backImage.setBackgroundResource(R.drawable.ic_action_back);
        }
        divider = (TextView) titleBar.findViewById(R.id.view_div);
        setupMainMenuContents(parent);
        return titleBar;
    }

    private void startGovQaScreen() {
        Intent intent = new Intent(this, GovQaLogin.class);
        intent.putExtra("BottomBtnJsonObj", MenuAsyncTask.bbJsonObj.toString());
        startActivity(intent);
    }

    private void startCitiExpeienceScreen(String itemId, String linkTypeId,
                                          String catIds, String searchKey, boolean isBottonBtn) {

        Intent intent = new Intent(this, CitiExperienceActivity.class);
        intent.putExtra("cityExpImgUlr", cityExpImgUlr);

        intent.putExtra(Constants.APP_STORE_LINK_INTENT_EXTRA, appPlaystoreLInk);
        intent.putExtra("downLoadLink", appleStoreLink);
        intent.putExtra(Constants.APP_ICON_INTENT_EXTRA, appIconImg);

        intent.putExtra("mBkgrdColor", mBkgrdColor);
        intent.putExtra("mBkgrdImage", mBkgrdImage);
        intent.putExtra("mBtnColor", mBtnColor);
        intent.putExtra("mBtnFontColor", mBtnFontColor);

        if (isBottonBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, itemId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, itemId);
        }

        intent.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, /* citiExpId */
                linkTypeId);
        intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
        intent.putExtra(Constants.APP_STORE_LINK_INTENT_EXTRA, appPlaystoreLInk);
        intent.putExtra("downLoadLink", appleStoreLink);
        intent.putExtra(Constants.APP_ICON_INTENT_EXTRA, appIconImg);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startCitiExpeienceScreen(String itemId, String linkTypeId,
                                          String catIds, String searchKey, boolean isBottonBtn,
                                          MainMenuBO mMainMenuBO) {

        Intent intent = new Intent(this, CitiExperienceActivity.class);
        intent.putExtra("cityExpImgUlr", cityExpImgUlr);

        intent.putExtra(Constants.APP_STORE_LINK_INTENT_EXTRA, appPlaystoreLInk);
        intent.putExtra("downLoadLink", appleStoreLink);
        intent.putExtra(Constants.APP_ICON_INTENT_EXTRA, appIconImg);
        intent.putExtra("mBkgrdColor", mMainMenuBO.getmBkgrdColor());
        intent.putExtra("mBkgrdImage", mMainMenuBO.getmBkgrdImage());
        intent.putExtra("mBtnColor", mMainMenuBO.getmBtnColor());
        intent.putExtra("mBtnFontColor", mMainMenuBO.getmBtnFontColor());

        if (isBottonBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, itemId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, itemId);
        }

        intent.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, /* citiExpId */
                linkTypeId);
        intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
        intent.putExtra(Constants.APP_STORE_LINK_INTENT_EXTRA, appPlaystoreLInk);
        intent.putExtra("downLoadLink", appleStoreLink);
        intent.putExtra(Constants.APP_ICON_INTENT_EXTRA, appIconImg);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startAlertScreen(boolean isBottomBtn, String bottomBtnId,
                                  String mItemId) {

        Intent intent = new Intent(this, AlertListDisplay.class);
        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startFindScreen(boolean isBottomBtn, String bottomBtnId,
                                 String mItemId, String bottomBtnLinkId, String mLinkId,
                                 String itemName, String bgColor) {

        Intent intent = new Intent(this, FindActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
            intent.putExtra("mLinkId", mLinkId);
        } else {
            intent.putExtra("BgColor", bgColor);
            intent.putExtra("Title", itemName);
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            intent.putExtra("mLinkId", mLinkId);
        }

        boolean isSubMenu;
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {

            isSubMenu = false;
        } else {

            isSubMenu = true;
        }

        intent.putExtra("isSubMenu", isSubMenu);

        this.startActivity(intent);
    }

    private void startFindSingleCategoryScreen(String catName,
                                               boolean isBottomBtn, String bottomBtnId, String mItemId,
                                               String bottomBtnLinkTypeID, String mLinkId) {

        Intent intent = new Intent(this, FindSingleCategory.class);
        intent.putExtra("catName", catName);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkTypeID);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            intent.putExtra("mLinkId", mLinkId);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        this.startActivity(intent);
    }

    private void startScannowScreen(boolean isBottomBtn, String bottomBtnId,
                                    String mItemId) {

        Intent intent = new Intent(this, ScanNowSplashActivity.class);
        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);


    }

    private void startHotdealsScreen(boolean isBottomBtn, String bottomBtnId,
                                     String mItemId) {

        Intent intent = new Intent(this, CouponsActivty.class);
        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }


        boolean isSubMenu;
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {

            isSubMenu = false;
        } else {
            isSubMenu = true;
        }

        intent.putExtra("isSubMenu", isSubMenu);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startWhatsNearbyScreen(boolean isBottomBtn,
                                        String bottomBtnId, String mItemId) {

        Intent intent = new Intent(this, ThisLocationGetLocation.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            intent.putExtra("mBkgrdColor", mBkgrdColor);
            intent.putExtra("mBkgrdImage", mBkgrdImage);
            intent.putExtra("mBtnColor", mBtnColor);
            intent.putExtra("mBtnFontColor", mBtnFontColor);

        } else {

            intent.putExtra("mBkgrdColor", smBkgrdColor);
            intent.putExtra("mBkgrdImage", smBkgrdImage);
            intent.putExtra("mBtnColor", smBtnColor);
            intent.putExtra("mBtnFontColor", smBtnFontColor);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startWhatsNearbyScreen(boolean isBottomBtn,
                                        String bottomBtnId, String mItemId, MainMenuBO mMainMenuBO) {

        Intent intent = new Intent(this, ThisLocationGetLocation.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            intent.putExtra("mBkgrdColor", mMainMenuBO.getmBkgrdColor());
            intent.putExtra("mBkgrdImage", mMainMenuBO.getmBkgrdImage());
            intent.putExtra("mBtnColor", mMainMenuBO.getmBtnColor());
            intent.putExtra("mBtnFontColor", mMainMenuBO.getmBtnFontColor());

        } else {

            intent.putExtra("mBkgrdColor", mMainMenuBO.getSmBkgrdColor());
            intent.putExtra("mBkgrdImage", smBkgrdImage);
            intent.putExtra("mBtnColor", mMainMenuBO.getSmBtnColor());
            intent.putExtra("mBtnFontColor", mMainMenuBO.getSmBtnFontColor());
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startBandEventsScreen(boolean isBottomBtn, String bottomBtnId,
                                       String mItemId, String bottomBtnLinkId, String mLinkId) {

        Intent intent = new Intent(this, EventsListDisplay.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            intent.putExtra("mLinkId", mLinkId);
        }
        intent.putExtra("listType", "Band");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startPrivacyPlicyScreen(boolean isBottomBtn, String bottomBtnId,
                                         String mItemId, String bottomBtnLinkId, String mLinkId) {

        Intent intent = new Intent(this, PrivacyPolicyActivity.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            intent.putExtra("mLinkId", mLinkId);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startEventsScreen(boolean isBottomBtn, String bottomBtnId,
                                   String mItemId, String bottomBtnLinkId, String mLinkId) {

        Intent intent = new Intent(this, EventsListDisplay.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
            intent.putExtra("mLinkId", mLinkId);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startAboutScreen(boolean isBottomBtn, String bottomBtnId,
                                  String mItemId) {
        Intent intent = new Intent(this, AboutUsActivity.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startPreferenceScreen() {

        Intent intent = new Intent(this, PreferedCatagoriesScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);
        this.startActivity(intent);

    }

    /**
     * Starts FAQ screen after checking if the call is from bottom button or
     * from the main menu
     */
    private void startFaqScreen(boolean isBottomBtn, String bottomBtnId,
                                String mItemId) {
        Intent intent = new Intent(this, FaqScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);

        }

        this.startActivity(intent);
    }

    private void startSettingsPageScreen(String mItemId, String mLinkId) {
        Intent intent = new Intent(this, SettingsPage.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);
        intent.putExtra("mItemId", mItemId);
        intent.putExtra("mLinkId", mLinkId);
        this.startActivity(intent);
    }

    private void startButtomBtnFindScreen(String bottomBtnId, String mLinkId) {

        Intent intent = new Intent(this, FindActivity.class);
        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        intent.putExtra("mLinkId", mLinkId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        this.startActivity(intent);
    }

    private void startFilterScreen(boolean isBottomBtn, String bottomBtnId,
                                   String mItemId) {

        if (null != retAffCount && "1".equals(retAffCount)) {
            Intent showFilter = new Intent(getApplicationContext(),
                    CitiExperienceActivity.class);

            if (isBottomBtn) {
                showFilter.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        bottomBtnId);
            } else {
                showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        mItemId);
            }

            showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, cityExpId);
            showFilter.putExtra(Constants.CAT_IDS_INTENT_EXTRA, "0");
            showFilter.putExtra(CommonConstants.CITYEXP_IMG_URL, cityExpImgUlr);
            showFilter.putExtra(CommonConstants.TAG_FILTERID, retAffId);
            showFilter.putExtra(CommonConstants.FILTERCOUNT_EXTRA, retAffCount);
            showFilter.putExtra(CommonConstants.TAG_FILTERNAME, retAffName);
            showFilter.putExtra("mBtnColor", mBtnColor);
            showFilter.putExtra("mBtnFontColor", mBtnFontColor);
            showFilter.putExtra("mBkgrdColor", mBkgrdColor);
            showFilter.putExtra("mBkgrdImage", mBkgrdImage);
            startActivity(showFilter);

        } else {
            Intent showFilter = new Intent(getApplicationContext(),
                    CityExperienceFilterActivity.class);

            if (isBottomBtn) {
                showFilter.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        bottomBtnId);
            } else {
                showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        mItemId);
            }

            showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, cityExpId);
            showFilter.putExtra(CommonConstants.CITYEXP_IMG_URL, cityExpImgUlr);
            showFilter.putExtra("mBtnColor", mBtnColor);
            showFilter.putExtra("mBtnFontColor", mBtnFontColor);
            showFilter.putExtra("mBkgrdColor", mBkgrdColor);
            showFilter.putExtra("mBkgrdImage", mBkgrdImage);

            if (isBottomBtn) {
                showFilter.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        bottomBtnId);
            } else {
                showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        mItemId);
            }

            startActivity(showFilter);

        }

    }

    private void startFilterScreen(boolean isBottomBtn, String bottomBtnId,
                                   String mItemId, MainMenuBO mMainMenuBO) {

        if (null != retAffCount && "1".equals(retAffCount)) {
            Intent showFilter = new Intent(getApplicationContext(),
                    CitiExperienceActivity.class);

            if (isBottomBtn) {
                showFilter.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        bottomBtnId);
            } else {
                showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        mItemId);
            }

            showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, cityExpId);
            showFilter.putExtra(Constants.CAT_IDS_INTENT_EXTRA, "0");
            showFilter.putExtra(CommonConstants.CITYEXP_IMG_URL, cityExpImgUlr);
            showFilter.putExtra(CommonConstants.TAG_FILTERID, retAffId);
            showFilter.putExtra(CommonConstants.FILTERCOUNT_EXTRA, retAffCount);
            showFilter.putExtra(CommonConstants.TAG_FILTERNAME, retAffName);
            showFilter.putExtra("mBtnColor", mMainMenuBO.getmBtnColor());
            showFilter
                    .putExtra("mBtnFontColor", mMainMenuBO.getmBtnFontColor());
            showFilter.putExtra("mBkgrdColor", mMainMenuBO.getmBkgrdColor());
            showFilter.putExtra("mBkgrdImage", mMainMenuBO.getmBkgrdImage());
            startActivity(showFilter);

        } else {
            Intent showFilter = new Intent(getApplicationContext(),
                    CityExperienceFilterActivity.class);

            if (isBottomBtn) {
                showFilter.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        bottomBtnId);
            } else {
                showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        mItemId);
            }

            showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, cityExpId);
            showFilter.putExtra(CommonConstants.CITYEXP_IMG_URL, cityExpImgUlr);
            showFilter.putExtra("mBtnColor", mMainMenuBO.getmBtnColor());
            showFilter
                    .putExtra("mBtnFontColor", mMainMenuBO.getmBtnFontColor());
            showFilter.putExtra("mBkgrdColor", mMainMenuBO.getmBkgrdColor());
            showFilter.putExtra("mBkgrdImage", mMainMenuBO.getmBkgrdImage());

            if (isBottomBtn) {
                showFilter.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA,
                        bottomBtnId);
            } else {
                showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                        mItemId);
            }

            startActivity(showFilter);

        }

    }

    private PopupWindow pwindo;
    public static Button btnClosePopup;
    protected static Button btnRedeem;
    Button btnTwitterShare, btnSMSShare, btnEmailShare;

    Button btnFacebookShare;

    protected void initiatePopupWindow() {
        try {
            try {
                // We need to get the instance of the LayoutInflater
                LayoutInflater inflater = (LayoutInflater) this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.share_screen_popup,
                        (ViewGroup) this.findViewById(R.id.popup_element));
                DisplayMetrics metrics = new DisplayMetrics();
                this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int screenWidth = metrics.widthPixels;
                int screenHeight = metrics.heightPixels;
                pwindo = new PopupWindow(layout, screenWidth, screenHeight / 2,
                        true);

                pwindo.showAtLocation(layout, Gravity.BOTTOM, 0, 0);

                btnClosePopup = (Button) layout
                        .findViewById(R.id.btn_close_popup);

                btnFacebookShare = (Button) layout
                        .findViewById(R.id.btn_facebook_popup);
                btnTwitterShare = (Button) layout
                        .findViewById(R.id.btn_twitter_popup);
                btnSMSShare = (Button) layout.findViewById(R.id.btn_text_popup);
                btnEmailShare = (Button) layout
                        .findViewById(R.id.btn_email_popup);

                btnFacebookShare.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaFacebook();

                    }
                });
                btnTwitterShare.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        shareViaTwitter();
                    }

                });
                btnSMSShare.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaSMS();

                    }

                });
                btnEmailShare.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaEMail();
                    }

                });

                btnClosePopup.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void shareViaEMail() {

        EmailAsyncTask emailAsyncTask = new EmailAsyncTask(activity, null);
        emailAsyncTask.execute();

    }

    public void startMenuActivity() {
        // @Ramachandran
        if (csMenuTask != null
                && csMenuTask.getStatus() == AsyncTask.Status.RUNNING) {
            csMenuTask.cancel(true);
        }
        if (EventsListDisplay.eventIDs != null) {
            for (int i = 0; i < EventsListDisplay.eventIDs.size(); i++) {
                EventsListDisplay.eventIDs.remove(i);
            }
        }

        String level = "1";
        String mItemId = "0";
        Constants.SHOWGPSALERT = false;
        if (SortDepttNTypeScreen.subMenuDetailsList.size() > 0) {
            SortDepttNTypeScreen.subMenuDetailsList.remove(0);
        }
        SubMenuStack.clearSubMenuStack();
        MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null, null, 0,
                null, mLinkId, null, null, null, null, level, null, null, null,
                null, null, null, null);
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

        MenuAsyncTask menu = new MenuAsyncTask(false, this, "mainMenu");
        menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, level, mItemId,
                "0", "None", "0", "0");
    }

    public void startSubMenuActivity(String mItemId, String mLinkId,
                                     boolean chkloc) {
        String level = null;

        level = String.valueOf(SubMenuStack.getSubMenuStack().size());

        String itemId = mItemId;
        String linkId = mLinkId;
        String typeId = "0";
        String deptId = "0";
        int index1 = SortDepttNTypeScreen.subMenuDetailsList.size();
        Log.v("", "LEVEL BEST : " + index1 + " , " + linkId);
        // if (SortDepttNTypeScreen.subMenuDetailsList != null
        // && !SortDepttNTypeScreen.subMenuDetailsList.isEmpty()
        // && SortDepttNTypeScreen.subMenuDetailsList.size() > 1) {
        //
        // int index = SortDepttNTypeScreen.subMenuDetailsList.size() - 1;
        //
        // itemId = SortDepttNTypeScreen.subMenuDetailsList.get(index - 1)
        // .getmItemId();
        // linkId = SortDepttNTypeScreen.subMenuDetailsList.get(index - 1)
        // .getmLinkId();
        // typeId = SortDepttNTypeScreen.subMenuDetailsList.get(index - 1)
        // .getType();
        // deptId = SortDepttNTypeScreen.subMenuDetailsList.get(index - 1)
        // .getDept();
        // }
        // SortDepttNTypeScreen.subMenuDetailsList.remove(index1 - 1);
        MenuAsyncTask submenu = new MenuAsyncTask(chkloc, this);
        submenu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, level,
                itemId, linkId, "None", typeId, deptId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HubCityContext hubCityContext = (HubCityContext) getApplicationContext();
        if (previousMenuLevel != null && "1".equals(previousMenuLevel)) {
            HubCityContext.level = 0;
        } else {
            if (HubCityContext.level > 0) {
                HubCityContext.level = HubCityContext.level - 1;
            }
        }
    }

    private void shareViaSMS() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer
                .append("Please download the HubCiti App from the below link:");
        stringBuffer.append("\n");
        stringBuffer.append(appPlaystoreLInk);
        stringBuffer.append("\n");
        stringBuffer.append(appleStoreLink);
        stringBuffer.append("\n");

        if (stringBuffer.length() > 0) {
            Intent smsShare = new Intent(Intent.ACTION_VIEW);
            smsShare.setData(Uri.parse("sms:"));
            try {
                smsShare.putExtra("sms_body",
                        URLDecoder.decode(stringBuffer.toString(), "UTF-8"));
                startActivity(smsShare);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    // @Ramachandran
    public static void threadRunning(MenuAsyncTask menu) {
        csMenuTask = menu;

    }

    private void shareViaFacebook() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.fb_share_popup);
        dialog.setTitle("Share via FaceBook");

        final EditText eText = (EditText) dialog.findViewById(R.id.text);

        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        new ImageLoaderAsync(image).execute(appIconImg);

        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog
                .findViewById(R.id.dialogButtonCancel);

        dialogButtonOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent facebookIntent = new Intent(MenuPropertiesActivity.this,
                        FaceBookActivity.class);
                facebookIntent.putExtra("userText", eText.getText().toString());
                facebookIntent.putExtra("appPlayStoreLink", appPlaystoreLInk);
                dialog.dismiss();
                startActivity(facebookIntent);
            }
        });
        dialogButtonCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void shareViaTwitter() {
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.fb_share_popup);
        dialog.setTitle("Share via Twitter");

        EditText text = (EditText) dialog.findViewById(R.id.text);
        text.setEnabled(false);
        text.setSingleLine(false);
        text.setText(getResources().getString(R.string.tweet_msg) + "\n"
                + appPlaystoreLInk);

        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        new ImageLoaderAsync(image).execute(appIconImg);

        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog
                .findViewById(R.id.dialogButtonCancel);

        dialogButtonOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new TweetMsg().execute();
            }
        });
        dialogButtonCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }


    public class TweetMsg extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (TwitterUtils.isAuthenticated(prefs)) {
                sendTweet();
            } else {
                Intent i = new Intent(getApplicationContext(),
                        PrepareRequestTokenActivity.class);
                i.putExtra("tweet_msg",
                        getResources().getString(R.string.tweet_msg) + "\n"
                                + appPlaystoreLInk);
                i.putExtra("image",
                        appIconImg);
                startActivityForResult(i, 2505);
            }
            return null;
        }

    }

    public void sendTweet() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    TwitterUtils.sendTweet(prefs,
                            getResources().getString(R.string.tweet_msg) + "\n"
                                    + appPlaystoreLInk, appIconImg);
                    mTwitterHandler.post(mUpdateTwitterNotification);
                } catch (Exception e) {
                    if (e != null && "" + e != null) {
                        if (e.getMessage().contains(
                                "Status is over 140 characters")) {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Tweet not sent - Status is over 140 characters!",
                                    Toast.LENGTH_LONG).show();
                        } else if (e.getMessage().contains("duplicate")) {
                            Toast.makeText(getApplicationContext(),
                                    "Tweet not sent - Status Duplicate!",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Tweet not sent - Error!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }

            }
        });
    }

    final Runnable mUpdateTwitterNotification = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getBaseContext(), "Tweet sent !", Toast.LENGTH_LONG)
                    .show();
            btnClosePopup.performClick();

        }
    };
    private String menutemplateName;


    private void startFundraiserScreen(String mItemId, String mLinkId,
                                       String catIds, String searchKey) {
        Intent intent = new Intent(this, FundraiserActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
        intent.putExtra("mLinkId", mLinkId);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);

    }

    protected void startButtomBtnFundraiserScreen(String bottomBtnId,
                                                  String mLinkId, String catIds, String searchKey) {
        Intent intent = new Intent(this, FundraiserActivity.class);
        intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
        intent.putExtra("mLinkId", mLinkId);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        this.startActivity(intent);

    }

    protected void startSubMenu() {

        Constants.SHOWGPSALERT = false;
        Integer nextLevel = null;

        int currentMenuLevel = 0;
        if (previousMenuLevel != null) {
            nextLevel = Integer.parseInt(previousMenuLevel) + 1;
            currentMenuLevel = nextLevel.intValue();

        } else {
            nextLevel = currentMenuLevel + 1;
            currentMenuLevel = nextLevel;
        }

        MainMenuBO mainMenuBO = new MainMenuBO(mItemId, mItemName, linkTypeId,
                linkTypeName, mMainMenuBOPosition, mItemImgUrl, mLinkId,
                mBtnColor, mBtnFontColor, smBtnColor, smBtnFontColor,
                nextLevel.toString(), mGrpFntColor, sGrpFntColor,
                mGrpBkgrdColor, sGrpBkgrdColor, mShapeName, mBkgrdImage, mBkgrdColor);
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

        int level = SubMenuStack.getSubMenuStack().size();

        SubMenuDetails subMenuDetails = new SubMenuDetails("0", "0", "Name",
                "", mLinkId, mItemId, /*level*/currentMenuLevel);
        SortDepttNTypeScreen.subMenuDetailsList.add(subMenuDetails);

        mMenuAsyncTask = new MenuAsyncTask(this);
        mMenuAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                String.valueOf(currentMenuLevel), mItemId, mLinkId, "None", "0", "0");

    }

    private void startNewsTemplateSubMenu(Class act, String mItemId, String mLinkId) {
        Intent intent = new Intent(activity, act);
        intent.putExtra(Constants.LEVEL, "2");
        intent.putExtra(Constants.ITEMID, mItemId);
        intent.putExtra(Constants.LINKID, mLinkId);
        intent.putExtra(Constants.IS_SUB_MENU, true);
        startActivity(intent);
    }

    protected void startSubMenu(MainMenuBO mMainMenuBO) {

        Constants.SHOWGPSALERT = false;
        Integer nextLevel;

        int currentMenuLevel = 0;
        if (previousMenuLevel != null) {
            nextLevel = Integer.parseInt(previousMenuLevel) + 1;
            currentMenuLevel = nextLevel.intValue();

        } else {
            nextLevel = currentMenuLevel + 1;
            currentMenuLevel = nextLevel;
        }

        MainMenuBO mainMenuBO = new MainMenuBO(mMainMenuBO.getmItemId(),
                mMainMenuBO.getmItemName(), mMainMenuBO.getLinkTypeId(),
                mMainMenuBO.getLinkTypeName(), mMainMenuBO.getPosition(),
                mMainMenuBO.getmItemImgUrl(), mMainMenuBO.getLinkId(),
                mMainMenuBO.getmBtnColor(), mMainMenuBO.getmBtnFontColor(),
                mMainMenuBO.getSmBtnColor(), mMainMenuBO.getSmBtnFontColor(),
                nextLevel.toString(), mGrpFntColor, sGrpFntColor,
                mGrpBkgrdColor, sGrpBkgrdColor, mShapeName, mMainMenuBO.getmBkgrdImage(),
                mMainMenuBO.getmBkgrdColor());
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

        int level = SubMenuStack.getSubMenuStack().size();

        SubMenuDetails subMenuDetails = new SubMenuDetails("0", "0", "Name",

                "", mMainMenuBO.getLinkId(), mMainMenuBO.getmItemId(), /*level*/ currentMenuLevel);

        SortDepttNTypeScreen.subMenuDetailsList.add(subMenuDetails);

        mMenuAsyncTask = new MenuAsyncTask(MenuPropertiesActivity.this);
        mMenuAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                String.valueOf(currentMenuLevel), mMainMenuBO.getmItemId(),
                mMainMenuBO.getLinkId(), "None", "0", "0");

    }

    // private void StartAsyncTaskInParallel(MenuAsyncTask task) {
    // if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
    // task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    // else
    // task.execute();
    // }
    protected void startBottomBtnSubMenu() {
        Constants.SHOWGPSALERT = false;
        Integer nextLevel = null;
        int currentMenuLevel = 0;
        if (previousMenuLevel != null) {
            nextLevel = Integer.parseInt(previousMenuLevel) + 1;
            currentMenuLevel = nextLevel.intValue();
        } else {
            nextLevel = currentMenuLevel + 1;
            currentMenuLevel = nextLevel;
        }

        MainMenuBO mainMenuBO = new MainMenuBO(bottomBtnId, mItemName,
                bottomBtnLinkTypeID, bottomBtnLinkTypeName,
                Integer.parseInt(bottomBtnPosition), mItemImgUrl,
                bottomBtnLinkId, mBtnColor, mBtnFontColor, smBtnColor,
                smBtnFontColor, nextLevel.toString(), mGrpFntColor,
                sGrpFntColor, mGrpBkgrdColor, sGrpBkgrdColor, "", null, null);
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

        int level = SubMenuStack.getSubMenuStack().size();
        SubMenuDetails subMenuDetails = new SubMenuDetails("0", "0", "Name",
                "", bottomBtnLinkId, bottomBtnId, currentMenuLevel);
        SortDepttNTypeScreen.subMenuDetailsList.add(subMenuDetails);

        mMenuAsyncTask = new MenuAsyncTask(activity);
        mMenuAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                String.valueOf(currentMenuLevel), bottomBtnId, bottomBtnLinkId, "None",
                "0", "0");
    }

    public void linkClickListener(String linkTypeName) {


        if ("GovQA".equalsIgnoreCase(linkTypeName)) {
            startGovQaScreen();
        } else if ("My Accounts".equalsIgnoreCase(linkTypeName) || "My Deals".equalsIgnoreCase(linkTypeName)) {
            startMyAccount();
        } else if ("City Experience".equals(linkTypeName)) {
            startCitiExpeienceScreen(mItemId, mLinkId, "0", null, false);
        } else if ("Preferences".equals(linkTypeName)
                || "Preference".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startPreferenceScreen();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }
        } else if ("FAQ".equals(linkTypeName)) {
            startFaqScreen(false, bottomBtnId, mItemId);
        } else if ("Settings".equals(linkTypeName)) {
            Log.v("", "USER NAME : " + UrlRequestParams.getUid() + " , "
                    + Constants.GuestLoginId);
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                CityPreferencesScreen.bIsSendingTimeStamp = false;
                startSettingsPageScreen(mItemId, mLinkId);
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }
        } else if ("About".equals(linkTypeName)) {
            startAboutScreen(false, bottomBtnId, mItemId);
        } else if ("Share".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                initiatePopupWindow();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("Scan Now".equals(linkTypeName)) {
            startScannowScreen(false, bottomBtnId, mItemId);
        } else if ("Deals".equals(linkTypeName)) {
            startHotdealsScreen(false, bottomBtnId, mItemId);
        } else if ("Alerts".equals(linkTypeName)) {
            startAlertScreen(false, bottomBtnId, mItemId);
        } else if ("Privacy Policy".equals(linkTypeName)) {
            startPrivacyPlicyScreen(false, bottomBtnId, mItemId,
                    bottomBtnLinkId, mLinkId);
        } else if ("AnythingPage".equals(linkTypeName)) {
            startAnythingPage(false, bottomBtnId, mItemId, bottomBtnLinkId,
                    mLinkId);
        } else if ("AppSite".equals(linkTypeName)) {
            starAppsitePage(false, bottomBtnId, mItemId, bottomBtnLinkId,
                    mLinkId);
        } else if ("Whats NearBy".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startWhatsNearbyScreen(false, bottomBtnId, mItemId);

            } else {
                startFindActivityForGuestLogin(bottomBtnId, mItemId);
            }

        } else if ("Find".equals(linkTypeName)) {
            String backgrnd = "";
            if (mBkgrdColor != null && !"N/A".equals(mBkgrdColor)) {
                backgrnd = mBkgrdColor;
            } else if (mBkgrdImage != null && !"N/A".equals(mBkgrdImage)) {
                backgrnd = mBkgrdImage;
            } else if (smBkgrdImage != null && !"N/A".equals(smBkgrdImage)) {
                backgrnd = smBkgrdImage;
            } else if (smBkgrdColor != null && !"N/A".equals(smBkgrdColor)) {
                backgrnd = smBkgrdColor;
            }

            startFindScreen(false, bottomBtnId, mItemId, bottomBtnLinkId,
                    mLinkId, mItemName, backgrnd);

        } else if (linkTypeName.startsWith("FindSingleCategory-")) {
            String[] catNames = linkTypeName.split("-");
            startFindSingleCategoryScreen(catNames[1], false, bottomBtnId,
                    mItemId, bottomBtnLinkId, mLinkId);

        } else if ("Events".equals(linkTypeName)) {
            startEventsScreen(false, bottomBtnId, mItemId, bottomBtnLinkId,
                    mLinkId);
        } else if ("Band Events".equals(linkTypeName)) {
            startBandEventsScreen(false, bottomBtnId, mItemId, bottomBtnLinkId,
                    mLinkId);
        } else if ("playing today".equalsIgnoreCase(linkTypeName)) {
            startEventMapScreen();
        } else if (linkTypeName.startsWith("EventSingleCategory-")) {
            startEventsScreen(false, bottomBtnId, mItemId, bottomBtnLinkId,
                    mLinkId);
        } else if ("SubMenu".equals(linkTypeName)) {
            //Added by Sharanamma
            if (mItemName.equalsIgnoreCase(Constants.SCROLLING)) {
                startNewsTemplateSubMenu(ScrollingPageActivity.class, mItemId, mLinkId);
            } else if (mItemName.equalsIgnoreCase(Constants.COMBINATION)) {
                startNewsTemplateSubMenu(CombinationTemplate.class, mItemId, mLinkId);
            } else if (mItemName.equalsIgnoreCase(Constants.NEWS_TILE)) {
                startNewsTemplateSubMenu(TwoTileNewsTemplateActivity.class, mItemId, mLinkId);
            } else {
                startSubMenu();
            }
        } else if ("Filters".equals(linkTypeName)) {
            if (null != mLinkId) {
                cityExpId = mLinkId;
            }
            startFilterScreen(false, bottomBtnId, mItemId);
        } // for fundraisers
        else if ("Fundraisers".equals(linkTypeName)) {
            startFundraiserScreen(mItemId, mLinkId, "0", null);
        } // Settings
        else if ("User Information".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startUserInformation();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("Location Preferences".equals(linkTypeName)) {

            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startLocationPreference();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("Category Favorites".equals(linkTypeName)) {

            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startCategoryFavourites();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("City Favorites".equals(linkTypeName)) {

            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startCityFavourites();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("Band".equals(linkTypeName)) {
            startBandLandingScreen(mItemId);
        }

    }

    public void linkClickListener(String linkTypeName, MainMenuBO mMainMenuBO) {


        if ("GovQA".equalsIgnoreCase(linkTypeName)) {
            startGovQaScreen();
        } else if ("My Accounts".equalsIgnoreCase(linkTypeName) || "My Deals".equalsIgnoreCase(linkTypeName)) {
            startMyAccount();
        } else if ("City Experience".equals(linkTypeName)) {
            startCitiExpeienceScreen(mMainMenuBO.getmItemId(),
                    mMainMenuBO.getLinkId(), "0", null, false, mMainMenuBO);
        } else if ("Preferences".equals(linkTypeName)
                || "Preference".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startPreferenceScreen();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }
        } else if ("FAQ".equals(linkTypeName)) {
            startFaqScreen(false, bottomBtnId, mMainMenuBO.getmItemId());
        } else if ("Settings".equals(linkTypeName)) {
            Log.v("", "USER NAME : " + UrlRequestParams.getUid() + " , "
                    + Constants.GuestLoginId);
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                CityPreferencesScreen.bIsSendingTimeStamp = false;
                startSettingsPageScreen(mMainMenuBO.getmItemId(),
                        mMainMenuBO.getLinkId());
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }
        } else if ("About".equals(linkTypeName)) {
            startAboutScreen(false, bottomBtnId, mMainMenuBO.getmItemId());
        } else if ("Share".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                initiatePopupWindow();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("Scan Now".equals(linkTypeName)) {
            startScannowScreen(false, bottomBtnId, mMainMenuBO.getmItemId());
        } else if ("Deals".equals(linkTypeName)) {
            startHotdealsScreen(false, bottomBtnId, mMainMenuBO.getmItemId());
        } else if ("Alerts".equals(linkTypeName)) {
            startAlertScreen(false, bottomBtnId, mMainMenuBO.getmItemId());
        } else if ("Privacy Policy".equals(linkTypeName)) {
            startPrivacyPlicyScreen(false, bottomBtnId, mMainMenuBO.getmItemId(),
                    bottomBtnLinkId, mMainMenuBO.getLinkId());
        } else if ("AnythingPage".equals(linkTypeName)) {
            startAnythingPage(false, bottomBtnId, mMainMenuBO.getmItemId(),
                    bottomBtnLinkId, mMainMenuBO.getLinkId());
        } else if ("AppSite".equals(linkTypeName)) {
            starAppsitePage(false, bottomBtnId, mMainMenuBO.getmItemId(),
                    bottomBtnLinkId, mMainMenuBO.getLinkId());
        } else if ("Whats NearBy".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startWhatsNearbyScreen(false, bottomBtnId,
                        mMainMenuBO.getmItemId(), mMainMenuBO);

            } else {
                startFindActivityForGuestLogin(bottomBtnId,
                        mMainMenuBO.getmItemId());
            }

        } else if ("Find".equals(linkTypeName)) {
            String backgrnd = "";
            if (mMainMenuBO.getSmBkgrdColor() != null
                    && !mMainMenuBO.getSmBkgrdColor().equals("N/A")) {
                backgrnd = mMainMenuBO.getSmBkgrdColor();
            }
            startFindScreen(false, bottomBtnId, mMainMenuBO.getmItemId(),
                    bottomBtnLinkId, mMainMenuBO.getLinkId(),
                    mMainMenuBO.getmItemName(), backgrnd);

        } else if (linkTypeName.startsWith("FindSingleCategory-")) {
            String[] catNames = linkTypeName.split("-");
            startFindSingleCategoryScreen(catNames[1], false, bottomBtnId,
                    mMainMenuBO.getmItemId(), bottomBtnLinkId,
                    mMainMenuBO.getLinkId());

        } else if ("Events".equals(linkTypeName)) {
            startEventsScreen(false, bottomBtnId, mMainMenuBO.getmItemId(),
                    bottomBtnLinkId, mMainMenuBO.getLinkId());
        } else if ("Band Events".equals(linkTypeName)) {
            startBandEventsScreen(false, bottomBtnId, mMainMenuBO.getmItemId(),
                    bottomBtnLinkId, mMainMenuBO.getLinkId());
        } else if ("playing today".equalsIgnoreCase(linkTypeName)) {
            if (!mMainMenuBO.getmItemId().isEmpty() && mMainMenuBO.getmItemId() != null)
                mItemId = mMainMenuBO.getmItemId();
            startEventMapScreen();
        } else if (linkTypeName.startsWith("EventSingleCategory-")) {
            startEventsScreen(false, bottomBtnId, mMainMenuBO.getmItemId(),
                    bottomBtnLinkId, mMainMenuBO.getLinkId());
        } else if ("SubMenu".equals(linkTypeName)) {
            //startSubMenu(mMainMenuBO);
            //Added by Sharanamma
            if (mMainMenuBO.getmItemName().equalsIgnoreCase(Constants.SCROLLING)) {
                startNewsTemplateSubMenu(ScrollingPageActivity.class, mMainMenuBO.getmItemId(),
                        mMainMenuBO.getLinkId());
            } else if (mMainMenuBO.getmItemName().equalsIgnoreCase(Constants.COMBINATION)) {
                startNewsTemplateSubMenu(CombinationTemplate.class, mMainMenuBO.getmItemId(),
                        mMainMenuBO.getLinkId());
            } else if (mMainMenuBO.getmItemName().equalsIgnoreCase(Constants.NEWS_TILE)) {
                startNewsTemplateSubMenu(TwoTileNewsTemplateActivity.class, mMainMenuBO
                                .getmItemId(),
                        mMainMenuBO.getLinkId());
            } else {
                startSubMenu(mMainMenuBO);
            }

        } else if ("Filters".equals(linkTypeName)) {
            if (null != mMainMenuBO.getLinkId()) {
                cityExpId = mMainMenuBO.getLinkId();
            }
            startFilterScreen(false, bottomBtnId, mMainMenuBO.getmItemId(),
                    mMainMenuBO);
        } // for fundraisers

        else if ("Fundraisers".equals(linkTypeName)) {
            startFundraiserScreen(mMainMenuBO.getmItemId(),
                    mMainMenuBO.getLinkId(), "0", null);
        } // Settings

        else if ("User Information".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startUserInformation();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("Location Preferences".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startLocationPreference();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("Category Favorites".equals(linkTypeName)) {
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startCategoryFavourites();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("City Favorites".equals(linkTypeName)) {

            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {
                startCityFavourites();
            } else {
                Constants.SignUpAlert(MenuPropertiesActivity.this);
            }

        } else if ("Band".equals(linkTypeName)) {
            mItemName = mMainMenuBO.getmItemName();
            startBandLandingScreen(mMainMenuBO.getmItemId());
        } else if (linkTypeName.contains("Logout")) {
            logout();
        } else if (linkTypeName.contains("Login/SignUp")) {
            signUp();
        }

    }

    private void startBandLandingScreen(String mItemId) {
        Intent intent = new Intent(this, BandsLandingPageActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra(Constants.MENU_ITEM_NAME_INTENT_EXTRA, mItemName);
        this.startActivity(intent);
    }

    private void startEventMapScreen() {
        Intent intent = new Intent(this, EventMapScreen.class);
        intent.putExtra("isPlayingToday", true);
        intent.putExtra("mItemId", mItemId);
        this.startActivity(intent);

    }

    protected void handleIntent(Intent intent) {
        if (intent != null) {
            mainMenuUnsortedList = intent
                    .getParcelableArrayListExtra(Constants.MENU_PARCELABLE_INTENT_EXTRA);

            if (null != mainMenuUnsortedList && !mainMenuUnsortedList.isEmpty()) {
                if (null != templateName
                        && (templateName.equals(GROUPED_TAB) || templateName
                        .equals(GROUPED_TAB_WITH_IMAGE))
                        && mainMenuUnsortedList.size() >= 2) {
                    if (previousMenuLevel != null
                            && "1".equals(previousMenuLevel)) {
                        mBtnColor = mainMenuUnsortedList.get(1).getmBtnColor();
                        mBtnFontColor = mainMenuUnsortedList.get(1)
                                .getmBtnFontColor();

                    } else {

                        smBtnColor = mainMenuUnsortedList.get(1)
                                .getSmBtnColor();
                        smBtnFontColor = mainMenuUnsortedList.get(1)
                                .getSmBtnFontColor();

                    }
                } else {
                    if (previousMenuLevel != null
                            && "1".equals(previousMenuLevel)) {
                        mBtnColor = mainMenuUnsortedList.get(0).getmBtnColor();
                        mBtnFontColor = mainMenuUnsortedList.get(0)
                                .getmBtnFontColor();

                    } else {

                        smBtnColor = mainMenuUnsortedList.get(0)
                                .getSmBtnColor();
                        smBtnFontColor = mainMenuUnsortedList.get(0)
                                .getSmBtnFontColor();

                    }
                    previousMenuLevel = intent.getExtras().getString(
                            Constants.MENU_LEVEL_INTENT_EXTRA);

                    mBkgrdColor = intent.getExtras().getString("mBkgrdColor");
                    mBkgrdImage = intent.getExtras().getString("mBkgrdImage");
                    smBkgrdColor = intent.getExtras().getString("smBkgrdColor");
                    smBkgrdImage = intent.getExtras().getString("smBkgrdImage");
                    mFontColor = intent.getExtras().getString("mFontColor");
                    smFontColor = intent.getExtras().getString("smFontColor");
                    menutemplateName = intent.getExtras().getString("rectangularTemp");
                    departFlag = intent.getExtras().getString("departFlag");
                    typeFlag = intent.getExtras().getString("typeFlag");
                    mBannerImg = intent.getExtras().getString("mBannerImg");
                    String APP_ICON_LINK = "appIconImg";
                    appIconImg = intent.getExtras().getString(APP_ICON_LINK);
                    appPlaystoreLInk = intent.getExtras().getString(APP_PLAYSTORE_LINK);
                    appleStoreLink = intent.getExtras().getString("downLoadLink");

                }

            }


            if (intent.hasExtra("cityExpImgUlr")) {
                this.cityExpImgUlr = intent.getExtras().getString(
                        "cityExpImgUlr");
            }

            retAffCount = intent.getExtras().getString("retAffCount");

            if (null != retAffCount && "1".equals(retAffCount)) {

                retAffId = intent.getExtras().getString("retAffId");
                retAffName = intent.getExtras().getString("retAffName");

            }


        }
        // Creates bottom button singleton
        BottomButtons bottomButtons = new BottomButtons(smBtnFontColor,
                smBtnColor, mBtnFontColor, mBtnColor, mLinkId, mItemId,
                mBkgrdColor, mBkgrdImage, smBkgrdColor, smBkgrdImage,
                cityExpImgUlr, appPlaystoreLInk, appleStoreLink,
                appIconImg, retAffCount, retAffId, retAffName, cityExpId,
                previousMenuLevel, menuName, mFontColor, smFontColor);

        BottomButtonInfoSingleton.clearBottomButtonInfoSingleton();
        BottomButtonInfoSingleton
                .createBottomButtonsSingleton(bottomButtons);


        //added for News bottom button single ton
        CommonConstants.smNewsBtnFontColor = smBtnFontColor;
        CommonConstants.smNewsBtnColor = smBtnColor;
        CommonConstants.mBtnNewsFontColor = mBtnFontColor;
        CommonConstants.mBtnNewsColor = mBtnColor;
        CommonConstants.mNewsBkgrdColor = mBkgrdColor;
        CommonConstants.mNewsBkgrdImage = mBkgrdImage;
        CommonConstants.smNewsBkgrdColor = smBkgrdColor;
        CommonConstants.smNewsBkgrdImage = smBkgrdImage;
        CommonConstants.newsCityExpImgUlr = cityExpImgUlr;
        CommonConstants.newsCityExpId = cityExpId;
        CommonConstants.mNewsFontColor = mFontColor;
        CommonConstants.smNewsFontColor = smFontColor;

    }

    @Override
    public void onClick(View v) {
        MainMenuBO expMainMenuBO = null;
        Iterator<MainMenuBO> tlistIterator = mainMenuUnsortedList.iterator();
        while (tlistIterator.hasNext()) {
            MainMenuBO menuBO = tlistIterator.next();
            if (menuBO.getmItemName().equalsIgnoreCase(v.getTag().toString())) {

                expMainMenuBO = menuBO;
                MainMenuBO mMainMenuBO = expMainMenuBO;

                mItemName = mMainMenuBO.getmItemName();
                mItemId = mMainMenuBO.getmItemId();
                linkTypeId = mMainMenuBO.getLinkTypeId();
                linkTypeName = mMainMenuBO.getLinkTypeName();
                mMainMenuBOPosition = mMainMenuBO.getPosition();
                mItemImgUrl = mMainMenuBO.getmItemImgUrl();
                mLinkId = mMainMenuBO.getLinkId();

                mBtnColor = mMainMenuBO.getmBtnColor();
                mBtnFontColor = mMainMenuBO.getmBtnFontColor();
                smBtnColor = mMainMenuBO.getSmBtnColor();
                smBtnFontColor = mMainMenuBO.getSmBtnFontColor();

                mGrpBkgrdColor = mMainMenuBO.getmGrpBkgrdColor();
                mGrpFntColor = mMainMenuBO.getmGrpFntColor();

                //smBkgrdColor = mMainMenuBO.getSmBkgrdColor();
                mShapeName = mMainMenuBO.getmShapeName();
                if (mMainMenuBO.getmBkgrdImage() != null) {
                    mBkgrdImage = mMainMenuBO.getmBkgrdImage();
                }

                linkClickListener(linkTypeName);

                break;
            }

        }
    }

    boolean flag;
    protected int latitude;
    protected int longitude;
    private SharedPreferences settings;
    private ScanSeeLocationListener scanSeeLocationListener;
    private LocationManager locationManager;
    boolean isFirst;

    private Dialog createDialogToUpdateZip() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Update Zip code");
        builder.setMessage("Please enter Zip code:");

        // Use an EditText view to get user input.
        final EditText input = new EditText(this);
        input.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});

        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                new UpdateZipCode(getApplicationContext(), UrlRequestParams
                        .getUid(), value).execute();
                return;
            }
        });

        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });

        return builder.create();
    }

    protected void checkLocService() {
        // Share Location
        // Restore preferences added new line
        Editor prefEditor = settings.edit();

        boolean gpsEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (locationManager.getAllProviders().contains("gps") && !gpsEnabled) {

            try {
                AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
                        MenuPropertiesActivity.this);
                notificationAlert
                        .setMessage(getString(R.string.alert_loc))
                        .setNegativeButton(getString(R.string.alert_loc_dont),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        flag = false;
                                        new GetZipcode().execute();

                                        final SharedPreferences setprefs = getSharedPreferences(
                                                Constants.PREFERENCE_HUB_CITY, 0);
                                        new GetUserSettingsAsync(true, MenuPropertiesActivity.this, setprefs.getBoolean("push_status", false)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                        dialog.cancel();
                                    }

                                })
                        .setPositiveButton(getString(R.string.alert_loc_allow),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        flag = true;
                                        AlertDialog.Builder notificationAlert = new AlertDialog
                                                .Builder(
                                                MenuPropertiesActivity.this);
                                        notificationAlert
                                                .setMessage(
                                                        getString(R.string.alert_turn_on))
                                                .setPositiveButton(
                                                        getString(R.string.specials_ok),
                                                        new DialogInterface.OnClickListener() {

                                                            @Override
                                                            public void onClick(
                                                                    DialogInterface dialog,
                                                                    int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                        notificationAlert.create().show();
                                    }
                                });
                notificationAlert.create().show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (gpsEnabled) {
            flag = false;
            // HubCityLogger.d(DEBUG_TAG, gpsEnabled + " Home2  GPSenabled");
            triggerLocationUpdates();
        } else {
            new AlertDialog.Builder(this).setTitle("Alert")
                    .setMessage("GPS in not available in your Device")
                    .setPositiveButton("OK", null).show();
        }
        prefEditor.apply();

    }

    String accZipcode = null;

    private class GetZipcode extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;

        // u try to do this 2screens
        @Override
        protected String doInBackground(String... params) {
            String result = "false";
            try {
                UrlRequestParams mUrlRequestParams = new UrlRequestParams();
                ServerConnections mServerConnections = new ServerConnections();
                String fetchuserlocationpoints = Properties.url_local_server
                        + Properties.hubciti_version
                        + "thislocation/fetchuserlocationpoints?userId=";
                String urlParameters = mUrlRequestParams.getUserZipcode();
                jsonObject = mServerConnections.getJSONFromUrl(false,
                        fetchuserlocationpoints + urlParameters, null);
                if (jsonObject != null) {

                    if (jsonObject.has("UserLocationPoints")) {

                        if ("10005".equalsIgnoreCase(jsonObject.getJSONObject(
                                "UserLocationPoints").getString("responseCode"))) {
                            result = jsonObject.getJSONObject("UserLocationPoints")
                                    .getString("responseText");

                        } else {

                            if (jsonObject.getJSONObject("UserLocationPoints").getString
                                    ("postalCode") != null) {
                                accZipcode = jsonObject.getJSONObject("UserLocationPoints")
                                        .getString("postalCode");
                            } else {
                                accZipcode = null;
                            }
                            result = "true";
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                result = "false";
            }

            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            if ("true".equals(result)) {
                Constants.setZipCode(accZipcode);
            } else {
                try {
                    new AlertDialog.Builder(MenuPropertiesActivity.this)
                            .setTitle(null)
                            .setMessage(getString(R.string.alert_zipcode))
                            .setPositiveButton(
                                    getString(R.string.alert_zipcode_yes),
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            createDialogToUpdateZip().show();
                                        }
                                    })
                            .setNegativeButton(
                                    getString(R.string.alert_zipcode_no),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            new AlertDialog.Builder(
                                                    MenuPropertiesActivity.this)
                                                    .setTitle(null)
                                                    .setMessage(
                                                            getString(R.string.alert_zipcode_onClick))
                                                    .setPositiveButton("OK", null)
                                                    .show();
                                        }
                                    }).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            //mDialog.dismiss();
        }
    }

    class ScanSeeLocationListener implements LocationListener {

        private Editor prefEditor;

        @Override
        public void onLocationChanged(Location location) {
            if (settings == null) {
                settings = getSharedPreferences(
                        CommonConstants.PREFERANCE_FILE, 0);
            }
            prefEditor = settings.edit();
            latitude = (int) (location.getLatitude() * 1E6);
            longitude = (int) (location.getLongitude() * 1E6);

            prefEditor.putInt("latitude", latitude);
            prefEditor.putInt("longitude", longitude);
            prefEditor.apply();

        }

        @Override
        public void onProviderDisabled(String arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
            // TODO Auto-generated method stub

        }

    }

    public void triggerLocationUpdates() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                scanSeeLocationListener);

    }

    String gpsAlertMessage = "Event Logistics uses your Phone's Location Services to locate you " +
            "on" +
            " the map. "
            + "Please turn on Location Services in Settings";

    private void startAnythingPage(final boolean isBottomBtn, final String bottomBtnId,
                                   final String mItemId, final String bottomBtnLinkId, final String mLinkId) {
        Intent intent = new Intent(this, AnythingPageDisplayActivity.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);
        } else {
            intent.putExtra(CommonConstants.LINK_ID, mLinkId);
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        intent.putExtra("isShare", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);


    }


    private void starAppsitePage(boolean isBottomBtn, String bottomBtnId,
                                 String mItemId, String bottomBtnLinkId, String mLinkId) {

        Intent intent = new Intent(MenuPropertiesActivity.this, AppsiteActivity.class);

        if (isBottomBtn) {
            intent.putExtra(Constants.BOTTOM_LINK_ID_INTENT_EXTRA,
                    bottomBtnLinkId);
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, bottomBtnId);

        } else {
            intent.putExtra(CommonConstants.LINK_ID, mLinkId);
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }


    private void startUserInformation() {
        Intent intent = new Intent(this, SetPreferrenceScreen.class);
        intent.putExtra("regacc", false);
        startActivity(intent);

    }

    private void startLocationPreference() {
        Intent intent = new Intent(this, SettingsLocationServices.class);
        intent.putExtra("regacc", false);
        startActivity(intent);
    }

    private void startCategoryFavourites() {
        Intent intent = new Intent(this, PreferedCatagoriesScreen.class);
        intent.putExtra("regacc", false);
        startActivity(intent);
    }

    private void startCityFavourites() {
        Intent intent = new Intent(this, CityPreferencesScreen.class);
        intent.putExtra("regacc", false);
        startActivity(intent);

    }


}
