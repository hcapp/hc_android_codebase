package com.hubcity.android.screens;

import java.lang.ref.WeakReference;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.hubregion.R;

public class DealsMoreInformation extends CustomTitleBar {

	WebView long_desc = null;
	static String htmlcode;
	String long_desc_txt = "";

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deals_more_info);

		if (getIntent().hasExtra("dealName")) {
			title.setText(getIntent().getExtras().getString("dealName"));
		}

		if (getIntent().hasExtra("long_desc")) {
			long_desc_txt = getIntent().getExtras().getString("long_desc");
		}

		long_desc = (WebView) findViewById(R.id.long_desc);
		long_desc.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

		WebSettings webSettings = long_desc.getSettings();
		webSettings.setJavaScriptEnabled(true);

		htmlcode = "<html><body style=\"text-align:justify\"> %s </body></Html>";

		if (null != long_desc_txt) {

			long_desc_txt = long_desc_txt.replaceAll("&#60;", "<");
			long_desc_txt = long_desc_txt.replaceAll("&#62;", ">");

			long_desc.setWebViewClient(new MyWebViewClient(
					DealsMoreInformation.this));

			long_desc.loadData(String.format(htmlcode, long_desc_txt),
					"text/html", "utf-8");
			long_desc.setBackgroundColor(Color.parseColor("#ffffff"));
		}

	}

	/**
	 * Customized WebClient to handle Email links, HTTP links and Anchor Tags
	 * 
	 * @author rekha_p
	 * 
	 */
	public class MyWebViewClient extends WebViewClient {
		private final WeakReference<Activity> mActivityRef;
		boolean flag = false;

		public MyWebViewClient(Activity activity) {
			mActivityRef = new WeakReference<>(activity);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// Handles Email Link
			if (url.startsWith("mailto:")) {
				final Activity activity = mActivityRef.get();
				if (activity != null) {
					MailTo mt = MailTo.parse(url);
					Intent i = newEmailIntent(activity, mt.getTo(),
							mt.getSubject(), mt.getBody(), mt.getCc());
					activity.startActivity(i);
					view.reload();
					return true;
				}
			} else if (url != null && url.startsWith("http://")) { // Handles
																	// HTTP
																	// links
																	// (Opens
																	// link in
																	// device
																	// browser)
				view.getContext().startActivity(
						new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				return true;
			} else {
				view.loadUrl(url);
			}
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// Handles Anchor Tags
			if (url.contains("#") && flag == false) {
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {
					public void run() {
						long_desc.loadData(
								String.format(htmlcode, long_desc_txt),
								"text/html", "utf-8");
					}
				}, 400);

				flag = true;
			} else {
				flag = false;
			}

		}

		/**
		 * Intent to open Email Composer screen
		 * 
		 * @param context
		 * @param address
		 * @param subject
		 * @param body
		 * @param cc
		 * @return
		 */
		private Intent newEmailIntent(Context context, String address,
				String subject, String body, String cc) {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
			intent.putExtra(Intent.EXTRA_TEXT, body);
			intent.putExtra(Intent.EXTRA_SUBJECT, subject);
			intent.putExtra(Intent.EXTRA_CC, cc);
			intent.setType("message/rfc822");
			return intent;
		}
	}
}
