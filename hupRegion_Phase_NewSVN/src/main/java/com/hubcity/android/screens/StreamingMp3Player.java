package com.hubcity.android.screens;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.SeekBar;

import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

public class StreamingMp3Player extends CustomTitleBar implements
		OnClickListener, OnTouchListener, OnCompletionListener,
		OnBufferingUpdateListener {

	private Button buttonPlayPause;
	private SeekBar seekBarProgress;
	protected String audioURL;

	private MediaPlayer mediaPlayer;
	private int mediaFileLengthInMilliseconds;

	private final Handler handler = new Handler();

	ShareInformation shareInfo;
	boolean isShareTaskCalled;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.play_audio_layout);
		audioURL = getIntent().getExtras().getString(CommonConstants.URL);
		title.setSingleLine(true);
		title.setMaxLines(1);
		title.setWidth(290);
		title.setEllipsize(TextUtils.TruncateAt.END);
		title.setText("Details");
		initView();

		try {
			mediaPlayer.setDataSource(audioURL);
			mediaPlayer.prepare();
			mediaPlayer.start();
            if (UrlRequestParams.getUid().equals("-1")) {
                leftTitleImage.setVisibility(View.GONE);
                rightImage.setVisibility(View.GONE);
            }

		} catch (Exception e) {
			e.printStackTrace();
		}

		mediaFileLengthInMilliseconds = mediaPlayer.getDuration();

		backImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mediaPlayer != null) {
					mediaPlayer.stop();
				}

				finish();
			}
		});

		// Call for Share Information
		if (getIntent().hasExtra("isShare")
				&& getIntent().getExtras().getBoolean("isShare")) {
			shareInfo = new ShareInformation(StreamingMp3Player.this, "", "",
					getIntent().getExtras().getString("pageId"), "Anything");
			isShareTaskCalled = false;

			leftTitleImage.setBackgroundResource(R.drawable.share_btn_down);
			leftTitleImage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (!Constants.GuestLoginId.equals(UrlRequestParams
							.getUid().trim())) {
						shareInfo.shareTask(isShareTaskCalled);
						isShareTaskCalled = true;
					} else {
						Constants.SignUpAlert(StreamingMp3Player.this);
						isShareTaskCalled = false;
					}
				}
			});
		}
	}

	/** This method initialise all the views in project */
	private void initView() {
		buttonPlayPause = (Button) findViewById(R.id.audioPlayPause);
		buttonPlayPause.setOnClickListener(this);

		seekBarProgress = (SeekBar) findViewById(R.id.seekbar);
		seekBarProgress.setMax(99); // It means 100% .0-99
		seekBarProgress.setOnTouchListener(this);

		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnBufferingUpdateListener(this);
		mediaPlayer.setOnCompletionListener(this);
	}

	/**
	 * Method which updates the SeekBar primary progress by current song playing
	 * position
	 */
	private void primarySeekBarProgressUpdater() {
		seekBarProgress.setProgress((int) (((float) mediaPlayer
				.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This
																				// math
																				// construction
																				// give
																				// a
																				// percentage
																				// of
																				// "was playing"/"song length"
		if (mediaPlayer.isPlaying()) {
			Runnable notification = new Runnable() {
				public void run() {
					primarySeekBarProgressUpdater();
				}
			};
			handler.postDelayed(notification, 1000);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.audioPlayPause) {
			/**
			 * ImageButton onClick event handler. Method which start/pause
			 * mediaplayer playing
			 */
			try {
				mediaPlayer.setDataSource(audioURL);
				mediaPlayer.prepare();
			} catch (Exception e) {
				e.printStackTrace();
			}

			mediaFileLengthInMilliseconds = mediaPlayer.getDuration();
			if (!mediaPlayer.isPlaying()) {
				mediaPlayer.start();
				buttonPlayPause.setText("Pause");
			} else {
				mediaPlayer.pause();
				buttonPlayPause.setText("Play");
			}

			primarySeekBarProgressUpdater();
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v.getId() == R.id.seekbar) {
			/**
			 * Seekbar onTouch event handler. Method which seeks MediaPlayer to
			 * seekBar primary progress position
			 */
			if (mediaPlayer.isPlaying()) {
				SeekBar sb = (SeekBar) v;
				int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100)
						* sb.getProgress();
				mediaPlayer.seekTo(playPositionInMillisecconds);
			}
		}
		return false;
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		/**
		 * MediaPlayer onCompletion event handler. Method which calls then song
		 * playing is complete
		 */
		buttonPlayPause.setText("Play");
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		/**
		 * Method which updates the SeekBar secondary progress by current song
		 * loading from URL position
		 */
		seekBarProgress.setSecondaryProgress(percent);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			mediaPlayer.stop();
		}
		return super.onKeyDown(keyCode, event);
	}
}
