package com.hubcity.android.screens;

import java.io.InputStream;
import java.util.ArrayList;

import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;
import com.hubcity.android.businessObjects.SpecialRetailerEntryItem;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SpecialsRetailerEntryAdapter extends ArrayAdapter<Item> {

	Context context;
	private ArrayList<Item> items;
	private LayoutInflater vi;

	public SpecialsRetailerEntryAdapter(Context context, ArrayList<Item> items) {
		super(context, 0, items);
		this.context = context;
		this.items = items;
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		final Item i = items.get(position);
		if (i != null) {
			if (i.isSection()) {
				SectionItem si = (SectionItem) i;
				v = vi.inflate(R.layout.list_item_section, parent,false);

				v.setOnClickListener(null);
				v.setOnLongClickListener(null);
				v.setLongClickable(false);

				final TextView sectionView = (TextView) v
						.findViewById(R.id.list_item_section_text);
				sectionView.setText(si.getTitle());

			} else {
				SpecialRetailerEntryItem ei = (SpecialRetailerEntryItem) i;
				v = vi.inflate(R.layout.special_list_retailer_item_enty, parent,false);
				final TextView title = (TextView) v
						.findViewById(R.id.list_item_entry_title);
				final TextView description = (TextView) v
						.findViewById(R.id.list_item_Description);
				if (title != null) {
					title.setText(ei.pageTitle);
				}
				if (description != null) {
					description.setMaxLines(2);
					description.setText(ei.sDescription);
				}
			}
		}
		return v;

	}

	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		private RotateAnimation anim;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
			anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF,
					0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
			anim.setInterpolator(new LinearInterpolator());
			anim.setRepeatCount(Animation.INFINITE);
			anim.setDuration(700);
			this.bmImage.setAnimation(null);
			this.bmImage.setImageResource(R.drawable.loading_button);
			this.bmImage.startAnimation(anim);
			this.bmImage.setAnimation(anim);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0].replaceAll(" ", "%20");
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			bmImage.setAnimation(null);
		}
	}
}