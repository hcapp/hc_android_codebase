package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.hubcity.android.businessObjects.CitiExperienceEntryItem;
import com.hubcity.android.businessObjects.Item;
import com.hubcity.android.businessObjects.SectionItem;

public class AustinExpRetailersListAdapter extends ArrayAdapter<Item> {

    private ArrayList<Item> items;
    private LayoutInflater vi;
    CustomImageLoader customImageLoader;
    private Activity activity;

    public AustinExpRetailersListAdapter(Activity activity, Context context,
                                         ArrayList<Item> items) {
        super(context, 0, items);
        this.activity = activity;
        this.items = items;
        vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        customImageLoader = new CustomImageLoader(context.getApplicationContext(), false);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;

        /***
         * If the list item reaches to its last position then it will be called
         * for next pagination
         ***/
        if (position == items.size() - 1) {
            if (!((CitiExperienceActivity) activity).isAlreadyLoading) {
                if (((CitiExperienceActivity) activity).nextPage) {
                    ((CitiExperienceActivity) activity).isAlreadyLoading = true;
                    ((CitiExperienceActivity) activity).enableBottomButton = false;
                    ((CitiExperienceActivity) activity).viewMoreBtn();
                }
            }
        }

        final Item i = items.get(position);
        if (i != null) {
            if (i.isSection()) {
                SectionItem si = (SectionItem) i;

                v = vi.inflate(R.layout.citi_experience_type_header, parent, false);

                v.setOnClickListener(null);
                v.setOnLongClickListener(null);
                v.setLongClickable(false);

                final TextView sectionView = (TextView) v
                        .findViewById(R.id.type_header);
                sectionView.setTextColor(Color.BLACK);
                sectionView.setGravity(Gravity.LEFT);
                sectionView.setBackgroundColor(Color.parseColor("#C0C0C0"));
                sectionView.setPadding(5, 5, 5, 5);

                if (!("N/A").equalsIgnoreCase(si.getTitle())
                        && !"".equals(si.getTitle())) {
                    sectionView.setText(si.getTitle());
                    sectionView.setVisibility(View.VISIBLE);

                } else {
                    sectionView.setVisibility(View.GONE);
                }
            } else {

                viewHolder = new ViewHolder();

                CitiExperienceEntryItem ei = (CitiExperienceEntryItem) i;
                v = vi.inflate(R.layout.listitem_austin_exp_get_retailers, parent, false);

                viewHolder.retailerImage = (ImageView) v
                        .findViewById(R.id.retailer_image);

                viewHolder.retailerName = (TextView) v
                        .findViewById(R.id.retailer_name);

                viewHolder.retailerSpecials = (ImageView) v
                        .findViewById(R.id.retailer_specials);

                viewHolder.retailerMileAddress = (TextView) v
                        .findViewById(R.id.retailer_address);
                viewHolder.retailerDistance = (TextView) v
                        .findViewById(R.id.distance);
                viewHolder.retailerMileAddress2 = (TextView) v
                        .findViewById(R.id.retailer_address2);
                viewHolder.retailerStatus = (TextView) v.findViewById(R.id.retailer_status);

                v.setTag(viewHolder);

                if (!"N/A".equalsIgnoreCase(ei.logoImagePath)) {
                    viewHolder.retailerImage.setTag(ei.logoImagePath);
                    customImageLoader.displayImage(ei.logoImagePath, activity,
                            viewHolder.retailerImage);
                    customImageLoader.displayImage(ei.bannerAdImagePath,
                            activity, new ImageView(activity));
                } else {
                    viewHolder.retailerImage.setImageBitmap(null);
                }
                String locationStatus = ei.locationOpen;
                if (locationStatus != null && !locationStatus.isEmpty()&& !locationStatus.equalsIgnoreCase("N/A")) {
                    viewHolder.retailerStatus.setText(locationStatus);
                }
                viewHolder.retailerName.setText(ei.retailerName);
                String retailerAddress="";
                if (ei.retailAddress1 != null && !ei.retailAddress1.equals("N/A")){
                    retailerAddress = ei.retailAddress1;
                }
                if (ei.retailAddress2 != null && !ei.retailAddress2.equals("N/A")){
                    retailerAddress = retailerAddress + ", " +ei.retailAddress2;
                }
                viewHolder.retailerMileAddress.setText(retailerAddress);

                if (!"N/A".equals(ei.distance) && ei.distance != null) {
                    viewHolder.retailerDistance.setVisibility(View.VISIBLE);
                    viewHolder.retailerDistance.setText(ei.distance);
                } else {
                    viewHolder.retailerDistance.setVisibility(View.GONE);
                }
                String address = "";
                if (!"N/A".equals(ei.city) && ei.city != null) {
                    address = ei.city;
                }
                if (!"N/A".equals(ei.state) && ei.state != null) {
                    if (address.isEmpty())
                        address = ei.state;
                    else
                        address = address + ", " + ei.state;
                }
                if (!"N/A".equals(ei.postalCode) && ei.postalCode != null) {
                    if (address.isEmpty())
                        address = ei.postalCode;
                    else
                        address = address + ", " + ei.postalCode;
                }
                viewHolder.retailerMileAddress2.setText(address);
                if (viewHolder.retailerSpecials != null) {
                    if ("true".equalsIgnoreCase(ei.saleFlag)) {
                        viewHolder.retailerSpecials.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.retailerSpecials.setVisibility(View.GONE);
                    }
                }
            }
        }
        return v;
    }

    public static class ViewHolder {
        ImageView retailerImage;
        ImageView retailerSpecials;
        TextView retailerName;
        TextView retailerMileAddress;
        TextView retailerMileAddress2;
        TextView retailerDistance;
        TextView retailerStatus;
    }

}
