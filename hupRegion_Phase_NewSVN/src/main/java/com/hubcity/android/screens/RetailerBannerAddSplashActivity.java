package com.hubcity.android.screens;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class RetailerBannerAddSplashActivity extends Activity
{

	ImageView bannerAd;

	String bannerAdUrl;
	CustomImageLoader customImageLoader;
	ProgressDialog progressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.banner_ad_splash);
		bannerAd = (ImageView) findViewById(R.id.banner_ad);

		bannerAdUrl = getIntent().getExtras().getString("bannerAdUrl");
		loadProgressDialog();
//		Loading image with picasso library to make faster download
		Picasso.with(this).load(bannerAdUrl.replace(" ", "%20")).into(bannerAd, new Callback()
		{
			@Override
			public void onSuccess()
			{
				progressDialog.dismiss();
				int SPLASH_TIME_OUT = 3000;
				new Handler().postDelayed(new Runnable()
				{

					@Override
					public void run()
					{
						onResultSet();

					}
				}, SPLASH_TIME_OUT);
			}

			@Override
			public void onError()
			{

			}
		});
	}

	private void loadProgressDialog()
	{
		progressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(Constants.DIALOG_MESSAGE);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}
	@Override
	public void onBackPressed() {
		onResultSet();
	}
	private void onResultSet(){
		Intent returnIntent = new Intent();
		setResult(RESULT_OK, returnIntent);
		RetailerBannerAddSplashActivity.this.finish();
	}
}
