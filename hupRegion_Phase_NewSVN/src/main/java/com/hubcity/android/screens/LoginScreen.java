package com.hubcity.android.screens;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.businessObjects.LoginScreenSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomImageLoader;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.PrefManager;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

public class LoginScreen extends Activity
{

	private String userNameVal;
	private String passwordVal;
	private EditText etUserName;
	private EditText etPassword;
	private String bkgndColor;
	private String btnColor;
	private String logoImg;
	private String poweredBy;
	private String btnFontColor;
	private String fontColor;
	private ImageView logo;
	private SharedPreferences settings;
	private static String TAG = "LoginScreen";
	String description;
	String title;
	CustomImageLoader customImageLoader;
	Bitmap mIcon11 = null;
	ProgressBar progressBar;
	TextView tvPwd;
	boolean guestLogin = false;
	int iPushFlag = 0, iGuestUserPush = 0;
	static Activity activity;
	CheckBox cbRememberLogin;
	private PrefManager objPrefManager;

	private void startSignUpActivity()
	{
		Intent intent = new Intent(this, RegistrationActivity.class);
		if (strPushMsg != null) {
			intent.putExtra(
					Constants.PUSH_NOTIFY_MSG, strPushMsg);
		}
		startActivity(intent);
	}

	private boolean mStateChecked;
	private boolean isFirst;
	private String strPushMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.scansee_login);
		objPrefManager = new PrefManager(this);
		activity = this;
		strPushMsg = this.getIntent().getStringExtra(Constants.PUSH_NOTIFY_MSG);
		if (strPushMsg != null) {
			SubMenuStack.clearSubMenuStack();

			LoginScreenViewAsyncTask mLoginScreenViewAsyncTask = new LoginScreenViewAsyncTask();
			mLoginScreenViewAsyncTask.execute(" ");
		}
			new VersionCheckAsyncTask(activity).execute();


		settings = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);

		try {
			isFirst = getIntent().getExtras().getBoolean("chkloc");
		} catch (Exception e) {
			e.printStackTrace();
			isFirst = false;
		}
		if (strPushMsg != null) {
			settingPushMsg(strPushMsg);
		}

		LoginScreenSingleton mLoginScreenSingleton = LoginScreenSingleton
				.getLoginScreenSingleton();

		progressBar = (ProgressBar) findViewById(R.id.logo_progress);
		if (mLoginScreenSingleton != null) {

			bkgndColor = mLoginScreenSingleton.getBkgndColor();

			btnColor = mLoginScreenSingleton.getBtnColor();
			btnFontColor = mLoginScreenSingleton.getBtnFontColor();

			description = mLoginScreenSingleton.getDescription();
			fontColor = mLoginScreenSingleton.getFontColor();

			logoImg = mLoginScreenSingleton.getLogoImgUrl();

			title = mLoginScreenSingleton.getTitle();
			poweredBy = mLoginScreenSingleton.getPoweredBy();

		}

		LinearLayout parentLayout = (LinearLayout) findViewById(R.id.login_screen_parent_ll);
		if (bkgndColor != null) {
			parentLayout.setBackgroundColor(Color.parseColor(bkgndColor));
		}

		logo = (ImageView) findViewById(R.id.logo);
		ImageView logoSmall ;


		if (logoImg != null && !"N/A".equals(logoImg)) {
			logoImg = logoImg.replace(" ", "%20");
			Picasso.with(activity).load(logoImg).into(logo, new Callback()
			{
				@Override
				public void onSuccess()
				{
					progressBar.setVisibility(View.GONE);
				}

				@Override
				public void onError()
				{
					progressBar.setVisibility(View.GONE);
				}
			});
		}

		logoSmall = (ImageView) findViewById(R.id.logo_small);
		if (poweredBy != null && !poweredBy.equalsIgnoreCase("N/A")) {
			poweredBy = poweredBy.replace(" ", "%20");
			Picasso.with(activity).load(poweredBy).placeholder(R.drawable.loading_button).into
					(logoSmall);
		}

		TextView tvPoweredByy = (TextView) findViewById(R.id.tv_powered_by);

		TextView tvUserName = (TextView) findViewById(R.id.tv_username);
		tvUserName.setText(R.string.username);

		tvPwd = (TextView) findViewById(R.id.tv_pwd);
		tvPwd.setText(R.string.password);

		TextView tvForgotPassword = (TextView) findViewById(R.id.forgot_password_textview_login);
		tvForgotPassword.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				forgetPassDialog();
			}
		});

		etUserName = (EditText) findViewById(R.id.et_username);
		etPassword = (EditText) findViewById(R.id.et_pwd);

		hideKeyboard();

		cbRememberLogin = (CheckBox) findViewById(R.id.rememberme_textview_login);

		mStateChecked = settings.getBoolean(Constants.REMEMBER, false);
		cbRememberLogin.setChecked(mStateChecked);

		cbRememberLogin
				.setOnCheckedChangeListener(new OnCheckedChangeListener()
				{

					@Override
					public void onCheckedChanged(CompoundButton button,
							boolean state)
					{
						saveLoginData(state);
					}
				});

		Button buttonSignUp = (Button) findViewById(R.id.button_sign_up);
		buttonSignUp.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Constants.GuestLoginId = "";
				startSignUpActivity();
			}
		});
		Button buttonGuestLogIn = (Button) findViewById(R.id.button_guest_Login);
		buttonGuestLogIn.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{

				LoginScreen.this.deleteDatabase("HupCity.db");
				MenuAsyncTask.dateModified = "";

				guestLogin = true;
				Constants.GuestLoginId = Constants.GuestLoginIdforSigup;

				Log.v(TAG, strPushMsg + " , " + iPushFlag);
				if (strPushMsg != null && iPushFlag == 1) {
					iGuestUserPush = 1;
					settingGuestUserPushMsg();
				} else {
					iGuestUserPush = 0;
					startGuestLogInAsynkTask();
				}

			}
		});

		Button buttonLogIn = (Button) findViewById(R.id.button_log_in);
		buttonLogIn.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				guestLogin = false;
				startLogInAsynkTask();

			}
		});
		TextView version = (TextView) findViewById(R.id.version_name);
		if (fontColor != null) {
			tvPwd.setTextColor(Color.parseColor(fontColor));
			tvForgotPassword.setTextColor(Color.parseColor(fontColor));
			tvUserName.setTextColor(Color.parseColor(fontColor));
			tvPoweredByy.setTextColor(Color.parseColor(fontColor));

			cbRememberLogin.setTextColor(Color.parseColor(fontColor));
			version.setTextColor(Color.parseColor(fontColor));
			/* adding version name */
			try {
				String versionName = getPackageManager().getPackageInfo(
						getPackageName(), 0).versionName;
				version.setText(versionName);
			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}
		}

		if (btnColor != null) {

			GradientDrawable buttonLogInShape = (GradientDrawable) buttonLogIn
					.getBackground();
			GradientDrawable buttonSignUpShape = (GradientDrawable) buttonSignUp
					.getBackground();
			GradientDrawable buttonGuestLogInShape = (GradientDrawable) buttonGuestLogIn
					.getBackground();
			buttonLogInShape.setColor(Color.parseColor(btnColor));
			buttonSignUpShape.setColor(Color.parseColor(btnColor));
			buttonGuestLogInShape.setColor(Color.parseColor(btnColor));
		}
		if (btnFontColor != null) {
			buttonSignUp.setTextColor(Color.parseColor(btnFontColor));
			buttonLogIn.setTextColor(Color.parseColor(btnFontColor));
			buttonGuestLogIn.setTextColor(Color.parseColor(btnFontColor));
		}

	}

	private void saveLoginData(boolean state)
	{
		Editor prefEditor = settings.edit();
		prefEditor.putString(Constants.REMEMBER_NAME, null);
		prefEditor.putString(Constants.REMEMBER_PASSWORD, null);
		if (state) {
			prefEditor.putString(Constants.REMEMBER_NAME, etUserName.getText()
					.toString().trim());
			prefEditor.putString(Constants.REMEMBER_PASSWORD, etPassword
					.getText().toString().trim());
		} else {
			prefEditor.putString(Constants.REMEMBER_NAME, null);
			prefEditor.putString(Constants.REMEMBER_PASSWORD, null);
		}
		prefEditor.putBoolean(Constants.REMEMBER, state);
		prefEditor.apply();
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		moveTaskToBack(true);
	}


	public void settingPushMsg(String strmsg)
	{
		AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
				activity);
		notificationAlert.setCancelable(false);
		notificationAlert.setTitle("Push Notification");
		notificationAlert
				.setMessage("Please login to view the Push Notification")
				.setPositiveButton("Ok",
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int id)
							{
								iPushFlag = 1;
								dialog.dismiss();
							}
						});
		notificationAlert.create().show();
	}

	public void settingGuestUserPushMsg()
	{
		AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
				activity);
		notificationAlert.setCancelable(false);
		notificationAlert.setTitle("Push Notification");
		notificationAlert.setMessage(
				"Please SignUp to view the Push Notification")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int id)
					{
						startGuestLogInAsynkTask();
						dialog.dismiss();
					}
				});

		notificationAlert.create().show();
	}


	private void startLogInAsynkTask()
	{
		String userName = etUserName.getText().toString();
		if (userName.trim() != null) {
			userNameVal = userName;
		}

		String password = etPassword.getText().toString();
		if (password.trim() != null) {
			passwordVal = password;
		}

		if (passwordVal != null && userNameVal != null) {
			new LoginAsyncTask(this, true, guestLogin, strPushMsg, iPushFlag, iGuestUserPush,
					isFirst, null)
					.execute(userNameVal, passwordVal);

		}
	}

	private void startGuestLogInAsynkTask()
	{
		String userName = "WelcomeScanSeeGuest";
		if (userName.trim() != null) {
			userNameVal = userName;
		}

		String password = ":::We@Love!!ScanSee?{People}";
		if (password.trim() != null) {
			passwordVal = password;
		}

		SharedPreferences.Editor prefEditor = settings.edit();
		prefEditor.putBoolean(Constants.REMEMBER, false);
		prefEditor.apply();

		if (passwordVal != null && userNameVal != null) {
			new LoginAsyncTask(this, true, guestLogin, strPushMsg, iPushFlag, iGuestUserPush,
					isFirst, null)
					.execute(userNameVal, passwordVal);

		}
	}

	@Override
	protected void onResume()
	{

		mStateChecked = settings.getBoolean(Constants.REMEMBER, false);

		if (mStateChecked && !Constants.DONT_ALLOW_LOGIN
				&& objPrefManager.isFirstTimeLaunch()) {

			etUserName.setText(settings
					.getString(Constants.REMEMBER_NAME, null));

			etUserName.setSelection(etUserName.getText().length());
			etPassword.setText(settings.getString(Constants.REMEMBER_PASSWORD,
					null));
			etPassword.setSelection(etPassword.getText().length());

		} else if (mStateChecked) {

			etUserName.setText(settings
					.getString(Constants.REMEMBER_NAME, null));
			etUserName.setSelection(etUserName.getText().length());
			etPassword.setText(settings.getString(Constants.REMEMBER_PASSWORD,
					null));
			etPassword.setSelection(etPassword.getText().length());
		} else {
			etUserName.setText("");
			etPassword.setText("");
		}
		super.onResume();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Constants.DONT_ALLOW_LOGIN = false;
	}

	@SuppressWarnings("deprecation")
	public void createAlert(String text)
	{
		if (text == null) {
			return;
		}

		final AlertDialog alertDialog = new AlertDialog.Builder(
				LoginScreen.this).create();
		alertDialog.setMessage(text);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				alertDialog.dismiss();
			}
		});
		alertDialog.show();
	}

	public class ForgotPasswordAsyncTask extends
			AsyncTask<String, Void, String>
	{

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();
		private ProgressDialog mDialog;
		String responseCode = "";

		@Override
		protected void onPreExecute()
		{
			mDialog = ProgressDialog
					.show(LoginScreen.this, "", "Please Wait..");
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params)
		{
			String response = "";
			try {
				String urlParameters = mUrlRequestParams
						.createForgotPasswordParameter(params[0]);
				String url_forgot_pwd = Properties.url_local_server
						+ Properties.hubciti_version + "firstuse/v2/forgotpwd";

				JSONObject jsonObject = mServerConnections.getUrlPostResponse(
						url_forgot_pwd, urlParameters, true);
				if (jsonObject != null) {
					jsonObject = jsonObject.getJSONObject("response");
					response = jsonObject.getString("responseText");
					responseCode = jsonObject.getString("responseCode");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return response;
		}

		@Override
		protected void onPostExecute(String response)
		{
			try {
				mDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				mDialog = null;
			}
			if (!response.equals("")) {
				successfulDialog(response);
				if (responseCode.equals("10000")) {
					etPassword.setText("");
					cbRememberLogin.setChecked(false);
					SharedPreferences.Editor prefEditor = settings.edit();
					prefEditor.putBoolean(Constants.REMEMBER, false);
					prefEditor.putString(Constants.REMEMBER_PASSWORD, null);
					prefEditor.apply();
				}
			} else {
				successfulDialog("Please Try after some time");
			}
		}
	}



	public static void hideKeyboard()
	{
		activity.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private void forgetPassDialog()
	{
		// custom dialog for forget password
		final Dialog dialog = new Dialog(this, android.R.style.Theme_DeviceDefault_Light_Dialog);
		dialog.setContentView(R.layout.forgot_password_dialog);
		dialog.setTitle("Forgot Password");
		dialog.setCancelable(false);

		final EditText etEmail = (EditText) dialog.findViewById(R.id.forgot_password_et);

		Button submitButton = (Button) dialog.findViewById(R.id.forgot_password_submit_btn);
		submitButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (etEmail.getText().toString().trim().equals("")) {
					Toast.makeText(LoginScreen.this, "Please enter your registered email id", Toast
							.LENGTH_SHORT).show();
				} else {
					dialog.dismiss();
					new ForgotPasswordAsyncTask().execute(etEmail.getText().toString());
				}
			}
		});

		Button cancelButton = (Button) dialog.findViewById(R.id.forgot_password_cancel_btn);
		cancelButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	private void successfulDialog(String msg)
	{
		new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Light_Dialog)
				.setMessage(msg)
				.setCancelable(false)
				.setPositiveButton("ok", new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				}).create().show();
	}

}