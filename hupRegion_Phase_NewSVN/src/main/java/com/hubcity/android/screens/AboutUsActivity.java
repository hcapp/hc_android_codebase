

package com.hubcity.android.screens;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.LoginScreenSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;
import com.squareup.picasso.Picasso;

public class AboutUsActivity extends CustomTitleBar {

    private String hubCitiImgUrl;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.webview);

        try {
            LoginScreenSingleton loginScreenSingleton = LoginScreenSingleton
                    .getLoginScreenSingleton();
            if (loginScreenSingleton != null) {
                hubCitiImgUrl = loginScreenSingleton.getSmallLogoUrl();

            }

            super.title.setVisibility(View.GONE);

            super.imageLogo.setVisibility(View.VISIBLE);
            Picasso.with(this).load(hubCitiImgUrl).placeholder(R.drawable
                    .loading_button).into(super.imageLogo);

            ((TextView) findViewById(R.id.right_text)).setText("About");
            ((TextView) findViewById(R.id.right_text)).setTextSize(16);
            ((TextView) findViewById(R.id.right_text)).setTypeface(null, Typeface.BOLD);
            ((TextView) findViewById(R.id.right_text)).setTextColor(Color.parseColor(Constants.getNavigationBarValues("titleTxtColor")));
            rightImage.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            super.leftTitleImage.setVisibility(View.GONE);

            WebView webviewprivacypolicy = (WebView) findViewById(R.id.webView1);
            webviewprivacypolicy.getSettings().setJavaScriptEnabled(true);
            String url_about_us = Properties.url_local_server_aboutandprivacy
                    + "/Images/hubciti/html/";
            webviewprivacypolicy.loadUrl(url_about_us
                    + UrlRequestParams.getHubCityId() + "/About_Us.html");
            webviewprivacypolicy.getSettings().setBuiltInZoomControls(true);



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}