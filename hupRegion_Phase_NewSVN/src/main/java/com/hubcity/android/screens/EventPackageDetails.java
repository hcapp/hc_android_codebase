package com.hubcity.android.screens;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class EventPackageDetails extends CustomTitleBar {
	String packagedetails;
	TextView packageDescription;
	String mEventName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_package_details);

		packagedetails = getIntent().getStringExtra("pkgDes");
		mEventName = getIntent().getStringExtra("eventName");
		packageDescription = (TextView) findViewById(R.id.event_package_text);

		title.setText(mEventName);
		leftTitleImage.setVisibility(View.INVISIBLE);

		rightImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (GlobalConstants.isFromNewsTemplate) {
					Intent intent = null;
					switch (GlobalConstants.className) {
						case Constants.COMBINATION:
							intent = new Intent(EventPackageDetails.this, CombinationTemplate.class);
							break;
						case Constants.SCROLLING:
							intent = new Intent(EventPackageDetails.this, ScrollingPageActivity.class);
							break;
						case Constants.NEWS_TILE:
							intent = new Intent(EventPackageDetails.this, TwoTileNewsTemplateActivity.class);
							break;
					}
					if (intent != null) {
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
					}
				} else {
					setResult(Constants.FINISHVALUE);
					startMenuActivity();
				}
			}
		});

		if (packagedetails != null) {
			packageDescription.setText(packagedetails);
		}

	}

}
