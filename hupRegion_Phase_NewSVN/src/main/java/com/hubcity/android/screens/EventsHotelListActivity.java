package com.hubcity.android.screens;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.scansee.hubregion.R;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

public class EventsHotelListActivity extends CustomTitleBar implements
		OnItemClickListener, OnClickListener {

	ProgressDialog progDialog;
	private static String returnValue = "sucessfull";
	Button viewMoreButton;
	View viewMore;

	String lastVisitedRecord = "0";
	String responseText;
	String maxRowNum = "0";

	Button moreInfo;
	View moreResultsView;
	HashMap<String, String> hotelDetails;
	ArrayList<HashMap<String, String>> hotelList = new ArrayList<>();
	ListView listOfHotels = null;
	LinearLayout mainLayout;
	String mEventId = "";
	String mainMenuId = "";


	HotelListAsyncTask hotelAsyncTask;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.events_hotellist);

		mEventId = getIntent().getStringExtra("eventId");

		mainLayout = (LinearLayout) findViewById(R.id.main_layout);
		mainLayout.setVisibility(View.INVISIBLE);

		listOfHotels = (ListView) findViewById(R.id.events_hotelList);

		moreResultsView = getLayoutInflater().inflate(
				R.layout.events_pagination, listOfHotels, false);
		moreInfo = (Button) moreResultsView
				.findViewById(R.id.events_view_more_button);
		moreInfo.setOnClickListener(this);

		viewMore = getLayoutInflater().inflate(R.layout.events_view_more,
				listOfHotels, false);
		viewMoreButton = (Button) viewMore
				.findViewById(R.id.events_viewMore_button);
		viewMoreButton.setOnClickListener(this);

		title.setText("Hotels");
		leftTitleImage.setVisibility(View.INVISIBLE);
		backImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				cancelAsyncTask();
				finish();
			}
		});

		rightImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (GlobalConstants.isFromNewsTemplate){
					Intent intent = null;
					switch (GlobalConstants.className) {
						case Constants.COMBINATION:
							intent = new Intent(EventsHotelListActivity.this, CombinationTemplate.class);
							break;
						case Constants.SCROLLING:
							intent = new Intent(EventsHotelListActivity.this, ScrollingPageActivity.class);
							break;
						case Constants.NEWS_TILE:
							intent = new Intent(EventsHotelListActivity.this, TwoTileNewsTemplateActivity.class);
							break;
					}
					if (intent != null) {
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
					}
				}else {
					setResult(Constants.FINISHVALUE);
					cancelAsyncTask();
					startMenuActivity();
				}
			}
		});

		callHotelAsyncTask();

	}

	public class HotelListAsyncTask extends AsyncTask<String, Void, String> {

		UrlRequestParams mUrlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		Boolean nextPage = false;

		protected void onPreExecute() {
			progDialog = new ProgressDialog(EventsHotelListActivity.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage(Constants.DIALOG_MESSAGE);
			progDialog.setCancelable(false);
			progDialog.show();

		}

		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if ("sucessfull".equals(returnValue)) {

				if (nextPage) {

					try {
						listOfHotels.removeFooterView(moreResultsView);
					} catch (Exception ex) {
						ex.printStackTrace();
					}

					moreInfo.setVisibility(View.VISIBLE);

					listOfHotels.addFooterView(moreResultsView);

				}

				else {
					listOfHotels.removeFooterView(moreResultsView);
				}

				EventHotelListAdapter adapter = new EventHotelListAdapter(
						EventsHotelListActivity.this, hotelList);
				listOfHotels.setAdapter(adapter);
				listOfHotels
						.setOnItemClickListener(EventsHotelListActivity.this);

			}

			else {
				progDialog.dismiss();
				Toast.makeText(EventsHotelListActivity.this, responseText,
						Toast.LENGTH_SHORT).show();

			}

			mainLayout.setVisibility(View.VISIBLE);

			if (progDialog.isShowing()) {
				progDialog.dismiss();
			}

		}

		@Override
		protected String doInBackground(String... arg0) {

			try {
				String lowerLimit = lastVisitedRecord;
				String url_events_hotel_list = Properties.url_local_server
						+ Properties.hubciti_version
						+ "alertevent/geteventhoteldisplay";
				String urlParameters = mUrlRequestParams
						.createHotelListParameter(mEventId, "android",
								lowerLimit, mainMenuId);
				JSONObject jsonResponse = mServerConnections
						.getUrlPostResponse(url_events_hotel_list,
								urlParameters, true);

				try {

					if (jsonResponse.has("RetailersDetails")) {

						JSONObject retailersListJSONObject = jsonResponse
								.getJSONObject("RetailersDetails");

						if (retailersListJSONObject != null) {

							JSONObject elem = retailersListJSONObject
									.getJSONObject("retailers");

							if (elem != null) {

								if (elem.has("Retailer")) {

									boolean isRetailsArray = isJSONArray(elem,
											"Retailer");

									if (isRetailsArray) {

										JSONArray retailersList = elem
												.getJSONArray("Retailer");

										for (int index = 0; index < retailersList
												.length(); index++) {

											hotelDetails = new HashMap<>();

											JSONObject elem1 = retailersList
													.getJSONObject(index);

											if (elem1.has("rowNumber")) {

												hotelDetails
														.put("rowNumber",
																elem1.getString("rowNumber"));
											}

											if (elem1.has("retailerId")) {
												hotelDetails
														.put("retailerId",
																elem1.getString("retailerId"));
											}

											if (elem1.has("retailerName")) {
												hotelDetails
														.put("retailerName",
																elem1.getString("retailerName"));
											}

											if (elem1.has("retailLocationId")) {
												hotelDetails
														.put("retailLocationId",
																elem1.getString("retailLocationId"));
											}
											if (elem1.has("distance")) {
												hotelDetails
														.put("distance",
																elem1.getString("distance"));
											}
											if (elem1.has("logoImagePath")) {
												hotelDetails
														.put("logoImagePath",
																elem1.getString("logoImagePath"));
											}
											if (elem1.has("retListId")) {
												hotelDetails
														.put("retListId",
																elem1.getString("retListId"));
											}

											if (elem1.has("hotelPrice")) {
												hotelDetails
														.put("hotelPrice",
																elem1.getString("hotelPrice"));
											}

											if (elem1.has("rating")) {
												hotelDetails
														.put("rating",
																elem1.getString("rating"));
											}

											hotelList.add(hotelDetails);

										}

									} else {

										hotelDetails = new HashMap<>();

										JSONObject elem1 = elem
												.getJSONObject("Retailer");
										if (elem1.has("rowNumber")) {

											hotelDetails.put("rowNumber", elem1
													.getString("rowNumber"));
										}

										if (elem1.has("retailerId")) {
											hotelDetails
													.put("retailerId",
															elem1.getString("retailerId"));
										}

										if (elem1.has("retailerName")) {
											hotelDetails
													.put("retailerName",
															elem1.getString("retailerName"));
										}

										if (elem1.has("retailLocationId")) {
											hotelDetails
													.put("retailLocationId",
															elem1.getString("retailLocationId"));
										}
										if (elem1.has("distance")) {
											hotelDetails
													.put("distance",
															elem1.getString("distance"));
										}
										if (elem1.has("logoImagePath")) {
											hotelDetails
													.put("logoImagePath",
															elem1.getString("logoImagePath"));
										}
										if (elem1.has("retListId")) {
											hotelDetails.put("retListId", elem1
													.getString("retListId"));
										}

										if (elem1.has("hotelPrice")) {
											hotelDetails
													.put("hotelPrice",
															elem1.getString("hotelPrice"));
										}

										if (elem1.has("rating")) {
											hotelDetails.put("rating",
													elem1.getString("rating"));
										}
										hotelList.add(hotelDetails);

									}

								}

							}

						}

						try {

							if (Integer.valueOf(retailersListJSONObject
									.getString("maxRowNum")) > Integer
									.valueOf(lastVisitedRecord)) {
								lastVisitedRecord = retailersListJSONObject
										.getString("maxRowNum");

							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						try {

							if ("1".equals(retailersListJSONObject
									.getString("nextPage"))) {

								nextPage = true;
							}

						} catch (Exception e) {
							e.printStackTrace();
						}

					} else {

						JSONObject jsonRecordNotFound = jsonResponse
								.getJSONObject("response");
						responseText = jsonRecordNotFound
								.getString("responseText");
						returnValue = "UNSUCCESS";
						return returnValue;

					}

				} catch (Exception ex) {
					ex.printStackTrace();
					returnValue = "UNSUCCESS";
					return returnValue;

				}

				return returnValue;

			} catch (Exception e) {
				e.printStackTrace();
				returnValue = "UNSUCCESS";
				return returnValue;
			}

		}

	}

	private boolean isJSONArray(JSONObject jsonObject, String value) {
		boolean isArray = false;

		JSONObject isJSONObject = jsonObject.optJSONObject(value);
		if (isJSONObject == null) {
			JSONArray isJSONArray = jsonObject.optJSONArray(value);
			if (isJSONArray != null) {
				isArray = true;
			}
		}
		return isArray;

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {

		HashMap<String, String> hotelDetails = hotelList.get(position);

		Intent hotelDetailsIntent = new Intent(EventsHotelListActivity.this,
				EventHotelDetailActivity.class);
		hotelDetailsIntent.putExtra("logoImagePath",
				hotelDetails.get("logoImagePath"));
		hotelDetailsIntent.putExtra("retailerName",
				hotelDetails.get("retailerName"));
		hotelDetailsIntent.putExtra("eventId", mEventId);
		hotelDetailsIntent.putExtra("retListId", hotelDetails.get("retListId"));
		hotelDetailsIntent.putExtra("retailLocationId",
				hotelDetails.get("retailLocationId"));

		startActivityForResult(hotelDetailsIntent, Constants.STARTVALUE);

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.events_view_more_button:
			callHotelAsyncTask();
			break;
		default:
			break;
		}

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Constants.FINISHVALUE) {
			setResult(Constants.FINISHVALUE);
			cancelAsyncTask();
			finish();
		}
	}

	private void callHotelAsyncTask() {

		if (hotelAsyncTask != null) {
			if (!hotelAsyncTask.isCancelled()) {
				hotelAsyncTask.cancel(true);
			}

			hotelAsyncTask = null;
		}

		hotelAsyncTask = new HotelListAsyncTask();
		hotelAsyncTask.execute();

	}

	private void cancelAsyncTask() {
		if (hotelAsyncTask != null && !hotelAsyncTask.isCancelled()) {
			hotelAsyncTask.cancel(true);
		}

		hotelAsyncTask = null;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		cancelAsyncTask();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cancelAsyncTask();
	}

}
