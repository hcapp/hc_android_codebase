package com.hubcity.android.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.commonUtil.CustomImageLoader;
import com.scansee.hubregion.R;

public class SpecialNamesListAdapter extends BaseAdapter {
	private ArrayList<String> specialoffersList;
	private static LayoutInflater inflater = null;
	public CustomImageLoader customImageLoader;
	Activity activity;
	String fontColor = "";
	String mColor = "";

	public SpecialNamesListAdapter(Activity activity,
			ArrayList<String> specialoffersList, String fontColor, String mColor) {
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.specialoffersList = specialoffersList;
		this.activity = activity;
		this.fontColor = fontColor;
		this.mColor = mColor;
		customImageLoader = new CustomImageLoader(activity.getApplicationContext(), false);
	}

	@Override
	public int getCount() {
		return specialoffersList.size();

	}

	@Override
	public Object getItem(int id) {
		return specialoffersList.get(id);
	}

	@Override
	public long getItemId(int id) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (convertView == null){
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.listitem_special, parent,false);
			holder.menuListLayout = (LinearLayout) view.findViewById(R.id.ll);
			holder.menuListLayout.setBackgroundColor(Color.parseColor(mColor));
			holder.currentsalesPname = (TextView) view
					.findViewById(R.id.retailer_sales_name);
			view.setTag(holder);
		}else{
			holder = (ViewHolder)view.getTag();
		}
		String itemName = specialoffersList.get(position);
		holder.currentsalesPname.setText(itemName);
		holder.currentsalesPname.setTextColor(Color.parseColor(fontColor));

		return view;
	}

	public static class ViewHolder {

		LinearLayout menuListLayout;
		public TextView currentsalesPname;

	}
}
