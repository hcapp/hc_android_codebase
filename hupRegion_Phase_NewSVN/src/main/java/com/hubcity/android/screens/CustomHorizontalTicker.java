package com.hubcity.android.screens;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.NewsDetailsActivity;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 10/1/2016.
 */
@SuppressWarnings("FieldCanBeLocal")
public class CustomHorizontalTicker extends HorizontalScrollView implements View.OnClickListener {
    private final Context mContext;
    private int mTextWidth;
    private int fromXValue;
    private int toXValue;
    private int duration;
    private int sideMargin = 60;
    @SuppressWarnings("CanBeFinal")
    private final Interpolator mInterpolator = new LinearInterpolator();
    private LinearLayout tickrParent;
    private int marqueeSpeed = 6;
    private View tickerView;


    public CustomHorizontalTicker(Context context) {
        super(context);
        this.mContext = context;
        addView();
    }

    public CustomHorizontalTicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        addView();
    }


    public void addView() {
        try {
            if(MenuAsyncTask.tickerMode != null) {
                if (MenuAsyncTask.tickerMode.equalsIgnoreCase(mContext.getString(R.string.scrolling))) {
                    if ((MenuAsyncTask.tickerDirection.equalsIgnoreCase(mContext.getString(R.string.right))
                            || MenuAsyncTask.tickerDirection.equalsIgnoreCase(mContext.getString(R.string.left)))) {
                        LayoutInflater tickerInflater = (LayoutInflater) mContext
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                         tickerView = tickerInflater.inflate(R.layout.horizontal_scroll,
                                null, false);
                        tickrParent = (LinearLayout) tickerView.findViewById(R.id.ticker_parent);
                        addTextView(tickrParent);
                        this.addView(tickerView);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addTextView(LinearLayout tickrParent) {
        LinearLayout.LayoutParams textParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParam.setMargins(sideMargin, 0, sideMargin, 0);
        if (MenuAsyncTask.tickerItem != null) {
            for (int position = 0; position < MenuAsyncTask.tickerItem.size(); position++) {
                if (MenuAsyncTask.tickerItem.get(position).getTitle() != null && !MenuAsyncTask.tickerItem.get(position).getTitle().isEmpty()) {
                    TextView newsView = new TextView(mContext);
                    newsView.setGravity(Gravity.CENTER_VERTICAL);
                    newsView.setText(MenuAsyncTask.tickerItem.get(position).getTitle());
                    newsView.setTextSize(16);
                    if (MenuAsyncTask.tickerTxtColor != null) {
                        newsView.setTextColor(Color.parseColor(MenuAsyncTask.tickerTxtColor));
                    }
                    newsView.setTypeface(Typeface.DEFAULT_BOLD);
                    newsView.setOnClickListener(this);
                    newsView.setTag(position);
                    newsView.setLayoutParams(textParam);
                    tickrParent.addView(newsView);
                }

            }
        }
    }


    public void startTextAnimation(int width, boolean leftToRight) {
        try {
            tickrParent = (LinearLayout) tickerView.findViewById(R.id.ticker_parent);
            mTextWidth = tickrParent.getMeasuredWidth();
            final int childCount = tickrParent.getChildCount();
            if(mTextWidth < width) {
                LinearLayout.LayoutParams newsTextParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (childCount > 1) {
                    int viewSize = tickrParent.getChildAt(childCount - 1).getWidth();
                    int finalWidth = (width - mTextWidth) + viewSize + (sideMargin*2);
                    newsTextParam.setMargins(sideMargin, 0, (finalWidth - sideMargin -viewSize), 0);
                    tickrParent.getChildAt(childCount - 1).setLayoutParams(newsTextParam);
                }
                if (childCount == 1) {
                    newsTextParam.setMargins(sideMargin, 0,(width - sideMargin -tickrParent.getChildAt(0).getWidth()), 0);
                    tickrParent.getChildAt(0).setLayoutParams(newsTextParam);
                }
            }


            duration = (width + mTextWidth) * marqueeSpeed;

            if (leftToRight) {
                fromXValue = -mTextWidth;
                toXValue = width;
            } else {
                fromXValue = width;
                toXValue = -mTextWidth;
            }

            ValueAnimator animator = ValueAnimator.ofFloat( fromXValue, toXValue);
            animator.setRepeatMode(ValueAnimator.RESTART);
            animator.setRepeatCount(ValueAnimator.INFINITE);
            animator.setInterpolator(mInterpolator);
            animator.setDuration(duration);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    for (int position = 0; position < childCount; position++) {
                        tickrParent.getChildAt(position).setTranslationX((float) animation.getAnimatedValue());
                    }
                }
            });
                animator.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }


    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        Intent ticker = new Intent(mContext,NewsDetailsActivity.class);
        ticker.putExtra("itemId",Integer.parseInt(MenuAsyncTask.tickerItem.get(position).getRowCount()));
        mContext.startActivity(ticker);
    }
}
