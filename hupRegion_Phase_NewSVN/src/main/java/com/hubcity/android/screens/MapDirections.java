package com.hubcity.android.screens;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;

import com.hubcity.android.commonUtil.CommonConstants;
import com.scansee.android.ScanseeBrowserActivity;

public class MapDirections {

	String alertMessage = "Get Directions uses Location Services to locate you on the map. Please turn on Location Services in Settings";

	LocationManager locationManager;
	Activity activity;
	String address, retailerName;
	String latitude, longitude;

	public MapDirections(LocationManager locationManager, Activity activity,
			String address, String latitude, String longitude,
			String retailerName) {
		this.locationManager = locationManager;
		this.activity = activity;
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
		this.retailerName = retailerName;
	}

	public void getMapDirections() {

		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (locationManager.getAllProviders().contains("gps") && gpsEnabled) {
			openWebview();

		} else {
			new AlertDialog.Builder(activity)
					.setTitle("")
					.setMessage(alertMessage)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									openWebview();
								}
							}).show();
		}
	}

	private void openWebview() {

		// For Testing only
		// String url = "http://www.google.com/maps/dir/" + currentLat + ","
		// + currentLon + "/" + "12.9702" + ","
		// + "77.5603";

		String url = "http://www.google.com/maps/place/" + address;

		Intent retailerLink = new Intent(activity, ScanseeBrowserActivity.class);
		retailerLink.putExtra(CommonConstants.URL, url);
		retailerLink.putExtra("isMap", true);
		retailerLink.putExtra(CommonConstants.TAG_PAGE_TITLE, retailerName);
		activity.startActivity(retailerLink);
	}

}
