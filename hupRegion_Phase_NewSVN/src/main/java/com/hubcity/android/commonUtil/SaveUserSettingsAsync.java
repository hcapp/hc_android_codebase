package com.hubcity.android.commonUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.artifex.mupdflib.AsyncTask;
import com.scansee.hubregion.R;

import org.json.JSONObject;

/**
 * Created by supriya.m on 12/15/2015.
 * This class is notifying the server whether to send a push notification on the basis of user interaction
 */
public class SaveUserSettingsAsync extends AsyncTask<String, Void, String> {

    Context mContext;
    ProgressDialog progressDialog;
    String pushNotify;
    SharedPreferences preferences;
    boolean isDisplay, bLocService;

    public SaveUserSettingsAsync(Context context, String pushNotify, boolean bLocService, boolean isDisplay) {
        this.mContext = context;
        this.pushNotify = pushNotify;
        this.isDisplay = isDisplay;
        this.bLocService = bLocService;
        preferences = context.getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);
    }

    @Override
    protected String doInBackground(String... params) {
        UrlRequestParams mUrlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();
        String returnValue = null;
        try {
            String url_usersettings = Properties.url_local_server
                    + Properties.hubciti_version
                    + "firstuse/setusersettings";
            String urlParameters = mUrlRequestParams
                    .createUserSettingsParameters(params[0],
                            pushNotify, bLocService);
            JSONObject jsonRespone = mServerConnections.getUrlPostResponse(
                    url_usersettings, urlParameters, true);

            if (jsonRespone != null) {

                if (jsonRespone.has("response")) {

                    JSONObject elem = jsonRespone.getJSONObject("response");

                    if ("User Setting Added/Updated.".equalsIgnoreCase(elem
                            .getString("responseText"))) {
                        preferences.edit().putString("radious", params[0]).apply();
                        returnValue = "true";

                    } else if ("Insufficient request".equalsIgnoreCase(elem
                            .getString("responseText"))) {

                        returnValue = elem.getString("responseText");
                        Toast.makeText(mContext,
                                elem.getString("responseText"),
                                Toast.LENGTH_SHORT).show();

                    } else {
                        returnValue = elem.getString("responseText");
                        Toast.makeText(mContext,
                                elem.getString("responseText"),
                                Toast.LENGTH_SHORT).show();

                    }

                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return returnValue;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isDisplay) {
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(Constants.DIALOG_MESSAGE);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if ("true".equals(result)) {
            preferences.edit().putBoolean("push_status", Boolean.valueOf(pushNotify))
                    .apply();
            if (isDisplay) {
                new AlertDialog.Builder(mContext)
                        .setMessage(R.string.settings_saved)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        ((Activity) mContext).finish();
                                    }
                                }).show();
            }
        } else {

            Toast.makeText(mContext, result,
                    Toast.LENGTH_SHORT).show();
        }
    }

}
