package com.hubcity.android.commonUtil;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 10/4/2016.
 */
public class GetClaimObject {
    private String responseCode;
    private  String responseText;
    private ArrayList<CustomerInfoList>customerInfoList;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public ArrayList<CustomerInfoList> getCustomerInfoList() {
        return customerInfoList;
    }

    public void setCustomerInfoList(ArrayList<CustomerInfoList> customerInfoList) {
        this.customerInfoList = customerInfoList;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }



}
