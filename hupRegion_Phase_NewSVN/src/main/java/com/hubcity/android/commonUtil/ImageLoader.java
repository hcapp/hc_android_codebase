package com.hubcity.android.commonUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Stack;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;


import com.scansee.hubregion.R;

public class ImageLoader {
	// the simplest in-memory cache implementation. This should be replaced with
	// something like SoftReference or BitmapOptions.inPurgeable(since 1.6)
	private HashMap<String, Bitmap> cache = new HashMap<>();

	private File cacheDir;
	private RotateAnimation anim;
	private ImageView mBannerImage;
	private Context context;
	private static String TAG = "ImageLoader";
	boolean isSquare;

	public ImageLoader(Context context, String imageDirPath) {

		anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);

		// Make the background thead low priority. This way it will not affect
		// the UI performance
		photoLoaderThread.setPriority(Thread.NORM_PRIORITY - 1);

		// Find the dir to save cached images
		cacheDir = getDiskCacheDir(context, imageDirPath);
		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
		}
	}

	public ImageLoader(Context context, boolean isSquare) {

		this.isSquare = isSquare;

		anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);

		// Make the background thead low priority. This way it will not affect
		// the UI performance
		photoLoaderThread.setPriority(Thread.NORM_PRIORITY - 1);

		// Find the dir to save cached images
		cacheDir = context.getCacheDir();
		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
		}
	}

	public ImageLoader(ImageView imageView, Context contxt) {
		mBannerImage = imageView;
		context = contxt;
	}

	/**
	 * Get a usable cache directory (external if available, internal otherwise).
	 * 
	 * @param context
	 *            The context to use
	 * @param uniqueName
	 *            A unique directory name to append to the cache dir
	 * @return The cache dir
	 */
	public static File getDiskCacheDir(Context context, String uniqueName) {
		// Check if media is mounted or storage is built-in, if so, try and use
		// external cache dir
		// otherwise use internal cache dir
		final String cachePath = Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState()) || !isExternalStorageRemovable() ? getExternalCacheDir(
				context).getPath()
				: context.getCacheDir().getPath();

		return new File(cachePath + File.separator + uniqueName);
	}

	/**
	 * Get the external app cache directory.
	 * 
	 * @param context
	 *            The context to use
	 * @return The external cache dir
	 */
	@TargetApi(VERSION_CODES.FROYO)
	public static File getExternalCacheDir(Context context) {
		if (Utils.hasFroyo()) {
			return context.getExternalCacheDir();
		}

		// Before Froyo we need to construct the external cache dir ourselves
		final String cacheDir = "/Android/data/" + context.getPackageName()
				+ "/cache/";
		return new File(Environment.getExternalStorageDirectory().getPath()
				+ cacheDir);
	}

	/**
	 * Check if external storage is built-in or removable.
	 * 
	 * @return True if external storage is removable (like an SD card), false
	 *         otherwise.
	 */
	@TargetApi(VERSION_CODES.GINGERBREAD)
	public static boolean isExternalStorageRemovable() {
		if (Utils.hasGingerbread()) {
			return Environment.isExternalStorageRemovable();
		}
		return true;
	}

	final static int STUBID = R.drawable.loading_button;

	public void displayImage(String url, Activity activity, ImageView imageView) {

		try {
			url = url.replaceAll(" ", "%20");
			if (cache.containsKey(url)) {
				imageView.setImageBitmap(cache.get(url));

				imageView.setAnimation(null);
			} else {

				queuePhoto(url, activity, imageView);
				imageView.setImageResource(STUBID);
				imageView.startAnimation(anim);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void displayImageWithutSpinner(String url, Activity activity,
			ImageView imageView) {

		url = url.replaceAll(" ", "%20");
		if (cache.containsKey(url)) {

			imageView.setImageBitmap(cache.get(url));

			imageView.setAnimation(null);
		} else {
			imageView.setAnimation(null);
			queuePhoto(url, activity, imageView);

		}
	}

	private void queuePhoto(String url, Activity activity, ImageView imageView) {
		// This ImageView may be used for other images before. So there may be
		// some old tasks in the queue. We need to discard them.
		photosQueue.clean(imageView);
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		synchronized (photosQueue.photosToLoad) {
			photosQueue.photosToLoad.push(p);
			photosQueue.photosToLoad.notifyAll();
		}

		// start thread if it's not started yet
		if (photoLoaderThread.getState() == Thread.State.NEW) {
			photoLoaderThread.start();
		}
	}

	public Bitmap getBitmap(String url, int imgHeight, int imgWidth) {
		// I identify images by hashcode. Not a perfect solution, good for the
		// demo.
		if (url != null) {
			String filename = String.valueOf(url.hashCode());
			File f = new File(cacheDir, filename);
			Bitmap b = null;
			if(f.exists()){
				// from SD cache
				b = decodeFile(f, imgHeight, imgWidth);
			}

			if (b != null) {
				return b;
			}

			// from web
			try {
				Bitmap bitmap = null;
				if ("N/A".equals(url)) {
					return bitmap;
				}
				URL imgURL = new URL(url);
				URLConnection connection = imgURL.openConnection();
				connection.setConnectTimeout(10000);
				InputStream is = connection.getInputStream();
				OutputStream os = new FileOutputStream(f);
				Utils.copyStream(is, os);
				os.close();
				bitmap = decodeFile(f, imgHeight, imgWidth);

				return bitmap;

			} catch (Exception ex) {

				ex.printStackTrace();

				return null;
			}
		}

		return null;
	}

	public static Bitmap roundCorner(Bitmap src) {
		// image size

		int width = src.getWidth();
		int height = src.getHeight();

		float round = height / 2.0f;
		// create bitmap output
		Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);

		// set canvas for painting
		Canvas canvas = new Canvas(result);
		canvas.drawARGB(0, 0, 0, 0);

		// config paint
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);

		// config rectangle for embedding
		final Rect rect = new Rect(0, 0, width, height);
		final RectF rectF = new RectF(rect);

		// draw rect to canvas
		canvas.drawRoundRect(rectF, round, round, paint);

		// create Xfer mode
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// draw source image to canvas
		canvas.drawBitmap(src, rect, rect, paint);

		// return final image
		return result;
	}

	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 50;
		int targetHeight = 50;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				Math.min(((float) targetWidth), ((float) targetHeight)) / 2,
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);

		return targetBitmap;
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f, int imgHeight, int imgWidth) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			Log.d(TAG, "Loading image from cache " + f.getName());

			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			final int requiredSizeWidth = 100;
			final int requiredSizeHeight = 100;

			int widthTmp = o.outWidth, heightTmp = o.outHeight;
			int scale = 1;

			if (heightTmp > requiredSizeHeight || heightTmp > requiredSizeWidth) {

				final int halfHeight = heightTmp / 2;
				final int halfWidth = widthTmp / 2;

				// Calculate the largest inSampleSize value that is a power of 2
				// and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / scale) > requiredSizeHeight
						|| (halfWidth / scale) > requiredSizeWidth) {
					scale++;
				}
			}

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (Exception e) {

			e.printStackTrace();

		}
		return null;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	// Task for the queue
	private class PhotoToLoad {
		protected String url;
		protected ImageView imageView;

		public PhotoToLoad(String u, ImageView i) {
			url = u;
			imageView = i;
		}
	}

	PhotosQueue photosQueue = new PhotosQueue();

	public void stopThread() {
		photoLoaderThread.interrupt();
	}

	// stores list of photos to download
	class PhotosQueue {
		private Stack<PhotoToLoad> photosToLoad = new Stack<>();

		// removes all instances of this ImageView
		public void clean(ImageView image) {
			try {
				for (int j = 0; j < photosToLoad.size();) {
					if (photosToLoad.get(j).imageView == image) {
						photosToLoad.remove(j);
					} else {
						++j;
					}
				}
			} catch (Exception e) {

				e.printStackTrace();

			}
		}
	}

	class PhotosLoader extends Thread {
		@Override
		public void run() {

			try {
				while (true) {
					// thread waits until there are any images to load in the
					// queue
					if (photosQueue.photosToLoad.isEmpty()) {
						synchronized (photosQueue.photosToLoad) {
							photosQueue.photosToLoad.wait();
						}
					}

					if (!photosQueue.photosToLoad.isEmpty()) {
						PhotoToLoad photoToLoad;

						synchronized (photosQueue.photosToLoad) {
							photoToLoad = photosQueue.photosToLoad.pop();
						}

						int imgHeight = photoToLoad.imageView.getHeight();
						int imgWidth = photoToLoad.imageView.getWidth();

						Bitmap bmp = getBitmap(photoToLoad.url, imgHeight,
								imgWidth);

						if (!isSquare
								&& photoToLoad.imageView.getId() == R.id.imgItem) {
							try {
								if (bmp != null) {
									bmp = roundCorner(bmp);
								}
							} catch (Exception e) {

								e.printStackTrace();

							}
						}

						cache.put(photoToLoad.url, bmp);
						if (bmp == null) {
							photoToLoad.imageView.setAnimation(null);
						}

						try {
							BitmapDisplayer bd = new BitmapDisplayer(bmp,
									photoToLoad.imageView);
							Activity a = (Activity) photoToLoad.imageView
									.getContext();
							a.runOnUiThread(bd);
						} catch (ClassCastException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (Thread.interrupted()) {
						break;
					}
				}
			} catch (InterruptedException e) {
				// allow thread to exit
			}

		}
	}

	PhotosLoader photoLoaderThread = new PhotosLoader();

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		ImageView imageView;

		public BitmapDisplayer(Bitmap b, ImageView i) {

			bitmap = b;
			imageView = i;
		}

		@Override
		public void run() {
			if (bitmap != null) {

				// if (isSquare) {
				// bitmap = getRoundedShape(bitmap);
				// }

				imageView.setImageBitmap(bitmap);
				imageView.setAnimation(null);
				if (null == bitmap) {
					imageView.setBackgroundColor(Color.GRAY);
				}
			} else {

				imageView.setImageBitmap(null);
			}
		}
	}

	public void clearCache() {
		// clear memory cache
		cache.clear();

		// clear SD cache
		File[] files = cacheDir.listFiles();
		for (File f : files) {
			f.delete();
		}
	}

	/**
	 * Handler used for setting image to imagevew
	 */
	Handler imageHandler = new Handler() {
		@SuppressWarnings("deprecation")
		public void handleMessage(Message message) {
			if (message.obj != null && message.obj instanceof Bitmap) {

				final BitmapDrawable background = new BitmapDrawable(
						context.getResources(), (Bitmap) message.obj);
				mBannerImage.setBackgroundDrawable(background);
			}
		}
	};

	/**
	 * This method sets image to image view after scaling the image to avoid
	 * Blurring of the Image
	 * 
	 * @param imageURL
	 */
	public void setScaledImage(final String imageURL) {
		new Thread() {

			public void run() {
				try {
					Bitmap bitmap = BitmapFactory
							.decodeStream((InputStream) new URL(imageURL
									.replaceAll(" ", "%20")).getContent());

					int width = mBannerImage.getWidth();
					int bitmapWidth = bitmap.getWidth();
					int bitmapHeight = bitmap.getHeight();
					int changedImageWidth = width;
					int changedImageHeight = (int) Math
							.floor((double) bitmapHeight
									* ((double) changedImageWidth / (double) bitmapWidth));
					Bitmap scaledBitmap = null;
					if (changedImageWidth > 0 && changedImageHeight > 0) {
						scaledBitmap = Bitmap.createScaledBitmap(bitmap,
								changedImageWidth, changedImageHeight, true);
					} else {
						scaledBitmap = Bitmap.createScaledBitmap(bitmap,
								bitmap.getWidth(), bitmap.getHeight(), true);
					}

					Message message = new Message();
					message.obj = scaledBitmap;
					imageHandler.sendMessage(message);

				} catch (MalformedURLException e) {

					e.printStackTrace();

				}

				catch (IOException e) {

					e.printStackTrace();

				}

				catch (Exception e) {

					e.printStackTrace();

				}
			}
		}.start();
	}

}
