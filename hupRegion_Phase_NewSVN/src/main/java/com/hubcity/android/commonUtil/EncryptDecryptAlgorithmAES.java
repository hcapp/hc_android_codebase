package com.hubcity.android.commonUtil;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import android.app.Activity;
import android.util.Log;

public class EncryptDecryptAlgorithmAES {
	static final String TAG = "SymmetricAlgorithmAES";

	// Set up secret key spec for 128-bit AES encryption and decryption
	public void generateKey() {

		try {
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			sr.setSeed("any data used as random seed".getBytes());
			KeyGenerator kg = KeyGenerator.getInstance("AES");
			kg.init(128, sr);
			SecretKeySpec sks = new SecretKeySpec(
					(kg.generateKey()).getEncoded(), "AES");

			Constants.govQaKey = sks;

		} catch (Exception e) {
			Log.e(TAG, "AES secret key spec error");
		}

	}

	// Encode the original data with AES
	public byte[] encodeData(String data) {

		byte[] encodedBytes = null;

		try {
			Cipher c = Cipher.getInstance("AES");
			c.init(Cipher.ENCRYPT_MODE, Constants.govQaKey);
			encodedBytes = c.doFinal(data.getBytes());

		} catch (Exception e) {
			Log.e(TAG, "AES encryption error");
		}

		return encodedBytes;

	}

	// Decode the encoded data with AES
	public String decodeData(byte[] encodedBytes) {

		byte[] decodedBytes = null;

		try {
			Cipher c = Cipher.getInstance("AES");
			c.init(Cipher.DECRYPT_MODE, Constants.govQaKey);
			decodedBytes = c.doFinal(encodedBytes);

		} catch (Exception e) {
			Log.e(TAG, "AES decryption error");
		}

		return (new String(decodedBytes));

	}

}
