package com.hubcity.android.commonUtil;

/**
 * Created by subramanya.v on 6/1/2016.
 */
public class ScrollingPageModel
{
	private final String isSideBar;
	private String userId;
	private String hubCitiId;
	private String catName;
	private int lowerLimit;
	private String dateCreated;
	private String level;
	private String linkId;


	public ScrollingPageModel(String userId, String hubCitiId, String catName, int lowerLimit,
			String isSidebar, String dateCreated, String level, String linkID)
	{
		this.userId = userId;
		this.hubCitiId = hubCitiId;
		this.catName = catName;
		this.lowerLimit = lowerLimit;
		this.isSideBar = isSidebar;
		this.dateCreated = dateCreated;
		this.level = level;
		this.linkId = linkID;
	}
}
