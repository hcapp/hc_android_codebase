package com.hubcity.android.commonUtil;

/**
 * Created by subramanya.v on 5/31/2016.
 */
public class BookMarkModel {
    private final String hubCitiId;
    private final String userId;
    private final String isBkMark;
    private final String level;
    private final String linkId;

    public BookMarkModel(String hubCitiId, String userId, String isBkMark, String linkID, String level) {
        this.hubCitiId = hubCitiId;
        this.userId = userId;
        this.isBkMark = isBkMark;
        this.linkId = linkID;
        this.level = level;
    }
}
