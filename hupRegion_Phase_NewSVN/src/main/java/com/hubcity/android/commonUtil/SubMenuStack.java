package com.hubcity.android.commonUtil;

import java.util.Stack;

import com.hubcity.android.businessObjects.MainMenuBO;

public class SubMenuStack {

	@SuppressWarnings("rawtypes")
	private static Stack subMenuStack;
	protected static boolean SHOWGPSALERT = true;

	static public void clearSubMenuStack() {
		if (subMenuStack != null) {
			subMenuStack.clear();
		}
	}

	@SuppressWarnings("rawtypes")
	static public Stack createSubMenuStack() {
		if (subMenuStack == null) {
			subMenuStack = new Stack();

		}
		return subMenuStack;
	}

	@SuppressWarnings("rawtypes")
	static public Stack getSubMenuStack() {
		if (subMenuStack != null) {
			return subMenuStack;
		} else {

			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private static void push(MainMenuBO mainMenuBO) {
		if (subMenuStack != null) {
			subMenuStack.push(mainMenuBO);
		} else {
			subMenuStack = SubMenuStack.createSubMenuStack();
			subMenuStack.push(mainMenuBO);
		}


	}
	
	static public int returnStackSize() {
		if (subMenuStack != null && !subMenuStack.isEmpty())
		{
			return subMenuStack.size();
		} 
		return 0;
	}

	static public void pop() {
		if (subMenuStack != null && !subMenuStack.isEmpty()) {
			subMenuStack.pop();
		} 
	}

	public static MainMenuBO getTopElement() {

		if (subMenuStack != null && !subMenuStack.isEmpty()) {

			MainMenuBO mMainMenuBO = (MainMenuBO) subMenuStack.lastElement();


			return mMainMenuBO;
		} else {

			return null;
		}
	}

	public static MainMenuBO onDisplayPreviousSubMenu()
			throws SubMenuStackException {
		pop();
		MainMenuBO mainMenuBO = getTopElement();

		if (mainMenuBO == null) {
			throw new SubMenuStackException(
					"There is no element for previous subMenu");
		} else {
			return mainMenuBO;
		}
	}

	public static void onDisplayNextSubMenu(MainMenuBO mainMenuBO) {
		push(mainMenuBO);
	}

	@SuppressWarnings("serial")
	public static class SubMenuStackException extends Exception {

		SubMenuStackException(String text) {
			super(text);
		}

		@Override
		public String getMessage() {
			return super.getMessage();
		}
	}
}