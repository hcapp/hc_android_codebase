package com.hubcity.android.commonUtil;


import com.hubcity.twitter.Constants;
import com.scansee.hubregion.BuildConfig;
import com.scansee.hubregion.R;


public class Properties
{

	public static String hubciti_version = BuildConfig.SERVER_PATH;

	// Server URLs
	public static String url_local_server = "";
	public static String url_local_server_aboutandprivacy = "";
	public static String url_ssqr_server = "";

	// Check of new relic should be on
	public static boolean isRegionApp = true;
	public static String HUBCITI_KEY;

	// Sets all the properties of the app
	public void setProperties(String hub_city_key, boolean isRegionApplication,
							  String consumerKey ,String consumerSecret)
	{

		HUBCITI_KEY = hub_city_key;
		isRegionApp = isRegionApplication;
		Constants.CONSUMER_KEY = consumerKey;
		Constants.CONSUMER_SECRET = consumerSecret;

		if ("QA".equalsIgnoreCase(BuildConfig.ENVIRONMENT)) {
			// *****Etapmus*****

			url_local_server = "http://" + "66.228.143.28" + ":8080";
			//url_ssqr_server = "66.228.143.28 ";
			//url_local_server_aboutandprivacy = "http://" + "66.228.143.28"
				//	+ ":8080";
		} else if ("Production".equalsIgnoreCase(BuildConfig.ENVIRONMENT)) {
			// *****Production*****
			url_local_server = "https://app.scansee.net";
			//url_ssqr_server = "www.scansee.net";
			//url_local_server_aboutandprivacy = "https://www.scansee.net";
			isNewRelicOn = true;

		} else {
			// ****Public*****
//			url_local_server = "http://" + "10.10.221.225" + ":8080";
			url_local_server = "http://10.10.221.215:8080";

			/*url_local_server = "http://10.10.221.27:9990";*/
			url_ssqr_server = "10.10.221.215:8080";
			url_local_server_aboutandprivacy = "http://" + "10.10.221.215:8080";

		}

	}

	// Check of new relic should be on
	public static boolean isNewRelicOn;

	// Set icon for Push notification
	public static final int push_icon = R.drawable.ic_launcher;



	public static String getBaseUrl()
	{
//		return "http://10.10.221.215:8080/HubCiti2.8.2/";

		return url_local_server + hubciti_version;
//		return "http://10.10.220.231:9990/HubCiti2.9";


	}

	public boolean getIsNewRelicOn()
	{
		return isNewRelicOn;
	}

}
