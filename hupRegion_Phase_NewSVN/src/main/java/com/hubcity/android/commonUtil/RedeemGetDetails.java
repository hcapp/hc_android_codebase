package com.hubcity.android.commonUtil;

/**
 * Created by shruthi.n on 12/5/2016.
 */
public class RedeemGetDetails {

    private final String couponId;
    private final String userId;

    public RedeemGetDetails(String couponId, String userId)
    {
        this.couponId = couponId;
        this.userId = userId;
    }

}
