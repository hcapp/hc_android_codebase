package com.hubcity.android.commonUtil;

/**
 * Created by shruthi.n on 12/6/2016.
 */
public class CouponDetails {
    private final String userId;
    private final String couponId;
    private final String couponListId;
    private final String hubCitiId;

    public CouponDetails(String userId, String couponId, String couponListId, String hubCitiId)
    {
        this.userId = userId;
        this.couponId = couponId;
        this.couponListId = couponListId;
        this.hubCitiId = hubCitiId;

    }

}
