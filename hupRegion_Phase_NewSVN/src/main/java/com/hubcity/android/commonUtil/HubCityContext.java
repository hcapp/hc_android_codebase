package com.hubcity.android.commonUtil;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.hubcity.android.businessObjects.FilterArdSortScreenBO;
import com.scansee.android.SubMenuFilterAndSortObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HubCityContext extends MultiDexApplication {

    private String groupChoice = "date";
    private String sortChoice = "date";

    private String fundGroupChoice = "date";
    private String fundSortChoice = "date";

    private Boolean mGPSEnabled = false;

    private String findSingleGroupChoice = "";
    private String findGroupChoice = "atoz";
    private String findSortChoice = "distance";

    private String findSingleCatName = null;
    private String findSingleCatId;

    private String radius = "5";

    private static Context context;
    public static int level = 0;

    public static HashMap<String, String> govqa_customer_info = new HashMap<>();
    public static HashMap<String, String> savedValues = new HashMap<>();
    public static HashMap<String, Integer> savedSubMenuSortByPosition = new HashMap<>();
    public static HashMap<String, String> savedSubMenuCityIds = new HashMap<>();

    public static ArrayList<FilterArdSortScreenBO> arrSortAndFilter = new ArrayList<>();
    public static ArrayList<SubMenuFilterAndSortObject> arrSubMenuDetails = new ArrayList<>();
    public static ArrayList<String> arrSubCatIds = new ArrayList<>();
    public static ArrayList<String> arrInterests = new ArrayList<>();
    public static ArrayList<String> arrCityIds = new ArrayList<>();
    public static ArrayList<String> arrOptionFilterIds = new ArrayList<>();
    public static ArrayList<String> arrFValueIds = new ArrayList<>();
    public static ArrayList<String> bandArrSubCatIds = new ArrayList<>();
    public static ArrayList<String> searchedArrSubCatIds = new ArrayList<>();
    public static boolean isDoneClicked;
    public static boolean isResumed;
    public static boolean isRefreshSubMenu;

    public static int sortSelectedPostion = -1;
    static boolean isAllFilterValuesChecked = true;
    public static ArrayList<String> pushMsgArray = new ArrayList<>();

    private boolean isCancelled;

    public void onCreate() {
        super.onCreate();
        HubCityContext.context = getApplicationContext();

    }

    public static Context getHubCityContext() {
        return HubCityContext.context;
    }

    public String getFindSingleGroupSelection() {
        return findSingleGroupChoice;
    }

    public void setFindSingleGroupSelection(String findSingleGroupChoice) {
        this.findSingleGroupChoice = findSingleGroupChoice;
    }

    public void setFundGroupSelection(String group) {
        this.fundGroupChoice = group;
    }

    public String getFundGroupSelection() {
        return this.fundGroupChoice;
    }

    public void setFundSortSelection(String sort) {
        this.fundSortChoice = sort;
    }

    public String getFundSortSelection() {
        return this.fundSortChoice;
    }

    public void setGroupSelection(String group) {
        this.groupChoice = group;
    }

    public String getGroupSelection() {
        return this.groupChoice;
    }

    public void setSortSelection(String sort) {
        this.sortChoice = sort;
    }

    public String getSortSelection() {
        return this.sortChoice;
    }

    public void setFindGroupSelection(String group) {
        this.findGroupChoice = group;
    }

    public String getFindGroupSelection() {
        return this.findGroupChoice;
    }

    public void setFindSortSelection(String sort) {
        this.findSortChoice = sort;
    }

    public String getFindSortSelection() {
        return this.findSortChoice;
    }

    public void setGpsState(Boolean mGPSEnabled) {
        this.mGPSEnabled = mGPSEnabled;
    }

    public Boolean getGpsState() {
        return this.mGPSEnabled;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getRadius() {
        return this.radius;
    }

    public void setFindSingleCatName(String catName) {
        this.findSingleCatName = catName;
    }

    public String getFindSingleCatName() {
        return this.findSingleCatName;
    }

    public void setFindSingleCatId(String catId) {
        this.findSingleCatId = catId;
    }

    public String getFindSingleCatId() {
        return this.findSingleCatId;
    }

    @SuppressWarnings("static-access")
    public void setFilterValues(HashMap<String, String> savedValues) {
        this.savedValues = savedValues;
    }

    @SuppressWarnings("static-access")
    public HashMap<String, String> getFilterValues() {
        return this.savedValues;
    }

    public void clearSavedValues() {
        savedValues.clear();
        setCancelled(false);
    }

    public void clearArrayAndAllValues(boolean clearSubmenu) {
        savedValues.clear();
        setCancelled(false);
        arrSortAndFilter = new ArrayList<>();
        if (clearSubmenu) {
            
            arrSubMenuDetails = new ArrayList<>();
            savedSubMenuCityIds = new HashMap<>();
        }
        arrSubCatIds = new ArrayList<>();
        arrInterests = new ArrayList<>();
        arrCityIds = new ArrayList<>();
        arrOptionFilterIds = new ArrayList<>();
        arrFValueIds = new ArrayList<>();
        sortSelectedPostion = -1;
        bandArrSubCatIds = new ArrayList<>();
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public static ArrayList<FilterArdSortScreenBO> getFilterSavedValues(
            ArrayList<FilterArdSortScreenBO> arrFilter, boolean isBand) {
        for (int i = 0; i < arrFilter.size(); i++) {
            if (arrFilter.get(i).getHeaderName().contains("Sort")) {
                if (sortSelectedPostion != -1) {
                    if (sortSelectedPostion == i) {
                        arrFilter.get(i).setChecked(true);
                    } else {
                        arrFilter.get(i).setChecked(false);
                    }
                }
            } else {
                if (arrFilter.get(i).getFilterName().contains("Specials")) {

                    if (savedValues.get("locSpecials") != null) {
                        if (savedValues.get("locSpecials").equals("1")) {
                            arrFilter.get(i).setChecked(true);
                        } else {
                            arrFilter.get(i).setChecked(false);
                        }
                    }
                } else if (arrFilter.get(i).getFilterName().contains("Date")) {
                    if (savedValues.get("EventDate") != null
                            && savedValues.get("EventDate").equals("")) {
                        arrFilter.get(i).setChecked(false);
                    } else {
                        if (savedValues.get("EventDate") != null) {
                            arrFilter.get(i).setChecked(true);
                        } else {
                            arrFilter.get(i).setChecked(false);
                        }
                    }
                }

            }

            for (int j = 0; j < arrFilter.get(i).getArrFilterOptionBOs().size(); j++) {
                if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Category")) {
                    if (isBand) {
                        if (bandArrSubCatIds.size() > 0) {
                            if (bandArrSubCatIds.contains(arrFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterId())) {
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            } else {
                                if (arrFilter.get(i).getArrFilterOptionBOs().get(j).isChecked()){
                                    arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                            .setChecked(true);
                                }else{
                                    arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                            .setChecked(false);
                                }

                            }
                        }
                    } else {
                        if (arrSubCatIds.size() > 0) {
                            if (arrSubCatIds.contains(arrFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterId())) {
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            } else {
                                if(arrFilter.get(i).getArrFilterOptionBOs().get(j).isChecked()){
                                    arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                            .setChecked(true);
                                }else {
                                    arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                            .setChecked(false);
                                }
                            }
                        }
                    }
                } else if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Options")) {
                    if (arrOptionFilterIds.size() > 0) {
                        if (arrOptionFilterIds.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            if (  arrFilter.get(i).getArrFilterOptionBOs().get(j).isChecked()){
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            }else {
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }
                        }
                    }
                } else if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Interests")) {
                    if (arrInterests.size() > 0) {
                        if (arrInterests.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            if(arrFilter.get(i).getArrFilterOptionBOs().get(j).isChecked()){
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            }else{
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }

                        }
                    }
                } else if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("City")) {
                    if (arrCityIds.size() > 0) {
                        if (arrCityIds.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            if (arrFilter.get(i).getArrFilterOptionBOs().get(j).isChecked()){
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }else{
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }
                        }
                    }
                } else {
                    if (arrFValueIds.size() > 0) {
                        if (arrFValueIds.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterGroupName())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(0)
                                    .setChecked(true);
                        }
                        if (arrFValueIds.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(false);
                        }
                    }
                }
            }
        }
        return arrFilter;
    }

    public static ArrayList<FilterArdSortScreenBO> getSearchedFilterSavedValues(
            ArrayList<FilterArdSortScreenBO> arrFilter) {
        for (int i = 0; i < arrFilter.size(); i++) {
            if (arrFilter.get(i).getHeaderName().contains("Sort")) {
                if (sortSelectedPostion == i) {
                    arrFilter.get(i).setChecked(true);
                } else {
                    arrFilter.get(i).setChecked(false);
                }
            } else {
                if (arrFilter.get(i).getFilterName().contains("Specials")) {

                    if (savedValues.get("locSpecials") != null) {
                        if (savedValues.get("locSpecials").equals("1")) {
                            arrFilter.get(i).setChecked(true);
                        } else {
                            arrFilter.get(i).setChecked(false);
                        }
                    }
                } else if (arrFilter.get(i).getFilterName().contains("Event")) {
                    if (savedValues.get("EventDate") != null
                            && savedValues.get("EventDate").equals("")) {
                        arrFilter.get(i).setChecked(false);
                    } else {
                        if (savedValues.get("EventDate") != null) {
                            arrFilter.get(i).setChecked(true);
                        } else {
                            arrFilter.get(i).setChecked(false);
                        }
                    }
                }

            }

            for (int j = 0; j < arrFilter.get(i).getArrFilterOptionBOs().size(); j++) {
                if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Category")) {
                    if (!arrFilter.get(i)
                            .getArrFilterOptionBOs().get(j).getFilterName().equalsIgnoreCase
                                    ("All")) {
                        if (searchedArrSubCatIds.size() > 0) {
                            if (searchedArrSubCatIds.contains(arrFilter.get(i)
                                    .getArrFilterOptionBOs().get(j).getFilterId())) {
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            } else {
                                if (arrFilter.get(i).getArrFilterOptionBOs().get(j).isChecked()){
                                    arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                            .setChecked(true);
                                }else{
                                    arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                            .setChecked(false);
                                }
                                isAllFilterValuesChecked = false;
                            }
                        }
                    }
                } else if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Options")) {
                    if (arrOptionFilterIds.size() > 0) {
                        if (arrOptionFilterIds.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            if(arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .isChecked()){
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            }else{
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }
                        }
                    }
                } else if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Interests")) {
                    if (arrInterests.size() > 0) {
                        if (arrInterests.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .isChecked()){
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            }else {
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }
                        }
                    }
                } else if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("City")) {
                    if (arrCityIds.size() > 0) {
                        if (arrCityIds.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            if(arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .isChecked()){
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            }else {
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }
                        }
                    }
                } else {
                    if (arrFValueIds.size() > 0) {
                        if (arrFValueIds.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterGroupName())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(0)
                                    .setChecked(true);
                        }
                        if (arrFValueIds.contains(arrFilter.get(i)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            if(arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .isChecked()){
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            }else {
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }
                        }
                    }
                }
            }
        }
        return arrFilter;
    }

    public static ArrayList<FilterArdSortScreenBO> getFilterSavedValuesForSubMenu(
            ArrayList<FilterArdSortScreenBO> arrFilter,
            SubMenuFilterAndSortObject subMenuFilterAndSortObject) {
        for (int i = 0; i < arrFilter.size(); i++) {
            if (arrFilter.get(i).getHeaderName().contains("Sort")) {
                if (subMenuFilterAndSortObject.getSortSelectedPostion() == i) {
                    arrFilter.get(i).setChecked(true);
                } else {
                    arrFilter.get(i).setChecked(false);
                }
            }

            for (int j = 0; j < arrFilter.get(i).getArrFilterOptionBOs().size(); j++) {
                if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Department")) {
                    if (subMenuFilterAndSortObject.getSavedDepartmentId() != null) {
                        if (subMenuFilterAndSortObject.getSavedDepartmentId()
                                .equals(arrFilter.get(i)
                                        .getArrFilterOptionBOs().get(j)
                                        .getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(false);
                        }
                    }
                } else if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("Type")) {
                    if (subMenuFilterAndSortObject.getSavedTypeId() != null) {
                        if (subMenuFilterAndSortObject.getSavedTypeId().equals(
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .getFilterId())) {
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        } else {
                            if(arrFilter.get(i).getArrFilterOptionBOs().get(j).isChecked()){
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(true);
                            }else{
                                arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                        .setChecked(false);
                            }
                        }
                    }
                } else if (arrFilter.get(i).getArrFilterOptionBOs().get(j)
                        .getFilterGroupName().equalsIgnoreCase("City")) {
                    if (subMenuFilterAndSortObject.getArrCityIds().contains(
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .getFilterId())) {
                        arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                .setChecked(true);
                    } else {
                        if(arrFilter.get(i).getArrFilterOptionBOs().get(j).isChecked()){
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(true);
                        }else{
                            arrFilter.get(i).getArrFilterOptionBOs().get(j)
                                    .setChecked(false);
                        }
                    }
                }
            }
        }
        return arrFilter;
    }

    public static ArrayList<FilterArdSortScreenBO> getFilterSavedValuesforCitySubMenu(
            ArrayList<FilterArdSortScreenBO> arrFilter, int groupPosition,
            String mItemId) {

        if (savedSubMenuCityIds.get(mItemId) != null
                && !savedSubMenuCityIds.get(mItemId).equals("")) {
            if (arrFilter.get(groupPosition).getFilterName().equals("City")) {
                for (int j = 0; j < arrFilter.get(groupPosition)
                        .getArrFilterOptionBOs().size(); j++) {
                    boolean isChecked = false;
                    String[] arrCity = savedSubMenuCityIds.get(mItemId).split(
                            ",");
                    for (String anArrCity : arrCity) {
                        if (anArrCity.equals(arrFilter.get(groupPosition)
                                .getArrFilterOptionBOs().get(j).getFilterId())) {
                            isChecked = true;
                        }
                    }
                    if (isChecked) {
                        arrFilter.get(groupPosition).getArrFilterOptionBOs()
                                .get(j).setChecked(true);
                    } else {
                        arrFilter.get(groupPosition).getArrFilterOptionBOs()
                                .get(j).setChecked(false);
                    }
                }
            }
        }
        return arrFilter;

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}