package com.hubcity.android.commonUtil;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.hubcity.android.model.CityModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/**
 * Created by sharanamma on 1/27/2016.
 */
public class CityDataHelper {
    private static final String TAG = CityDataHelper.class.getSimpleName();

    public void storePreferedCities(Activity activity, JSONArray cityArray)
    {
        clearCityPreference(activity);
        SharedPreferences preference = getSharedPreference(activity);
        SharedPreferences.Editor editor = preference.edit();
        try {
            for (int i = 0; i < cityArray.length(); i++) {
                String key = cityArray.getJSONObject(i).getString("cityId");
                String value = cityArray.getJSONObject(i).getString("cityName");
                editor.putString(key, value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editor.apply();
    }

    public void storePreferedCities(Activity activity, ArrayList<CityModel> cityList) {
        clearCityPreference(activity);
        SharedPreferences preference = getSharedPreference(activity);
        SharedPreferences.Editor editor = preference.edit();

        for (int i = 0; i < cityList.size(); i++) {
            editor.putString(cityList.get(i).getCityId(), cityList.get(i).getCityName());
        }

        editor.apply();
    }
    public String getCitiesList(Activity activity)
    {

        String cityListString = "";
        Map<String, ?> allPrefs = getSharedPreference(activity).getAll(); //sharedPreference
        Set<String> set = allPrefs.keySet();
        for (String key : set)
        {
            Log.d(TAG, "<" + key + "> =  "
                    + allPrefs.get(key).toString());
            cityListString = cityListString + "," + key;
        }

        if (cityListString.startsWith(",")){
            cityListString = cityListString.substring(1,cityListString.length());
        }
         return cityListString;
    }

    public SharedPreferences getSharedPreference(Activity activity) {
        return activity.getSharedPreferences(Constants.CITY_PREFERENCE, Context.MODE_PRIVATE);
    }

    public void clearCityPreference(Activity activity) {
        getSharedPreference(activity).edit().clear().apply();
    }
}
