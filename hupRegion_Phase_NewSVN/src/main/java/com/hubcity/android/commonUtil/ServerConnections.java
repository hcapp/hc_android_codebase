package com.hubcity.android.commonUtil;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.jsoup.Jsoup;

import android.util.Log;

public class ServerConnections {

    private BufferedReader in;
    private static JSONObject jObj;

    public static JSONObject requestWebService(String serviceUrl)
            throws JSONException {

        HttpURLConnection urlConnection = null;
        try {
            // create connection
            URL urlToRequest = new URL(serviceUrl);
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(30000);
            urlConnection.setReadTimeout(30000);

            // handle issues
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                // handle unauthorized (if service requires user login)
            } else if (statusCode != HttpURLConnection.HTTP_OK) {
                // handle any other errors, like 404, 500,..
            }

            // create JSON object from content
            InputStream in = new BufferedInputStream(
                    urlConnection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            Log.e("hhhh", in + "");
            StringBuffer sb = new StringBuffer("");
            String line = "";
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            while ((line = br.readLine()) != null) {
                if ("".equals(line)) {
                    sb.append("\n\n");
                }

                sb.append(line);
            }
            br.close();
            Log.e("res xml ", sb.toString());
            String tempString = null;
            try {
                StringBuffer newSB = new StringBuffer();
                for (int count = 0; count < sb.length(); count++) {
                    if (count + 9 < sb.length()
                            && "<![CDATA[".equalsIgnoreCase(sb.subSequence(
                            count, count + 9).toString())) {
                        newSB.append("<![CDATA[");
                        count += 9;
                        for (int inner = count; inner < sb.length(); inner++) {
                            if (inner + 3 < sb.length()
                                    && "]]>".equalsIgnoreCase(sb.subSequence(
                                    inner, inner + 3).toString())) {
                                newSB.append(Jsoup
                                        .parse(sb.subSequence(count, inner)
                                                .toString()).text()
                                        .replace("<", "").replace("</", "")
                                        .replace(">", "").replace("/>", ""));
                                count = inner + 2;
                                newSB.append("]]");
                                break;
                            }
                        }

                    }
                    newSB.append(sb.charAt(count));
                }
                tempString = newSB.toString();
                newSB = new StringBuffer();
                sb = new StringBuffer(tempString);
                for (int count = 0; count < sb.length(); count++) {
                    if (count + 3 < sb.length()
                            && "<p ".equalsIgnoreCase(sb.subSequence(count,
                            count + 3).toString())) {

                        count += 3;
                        for (int inner = count; inner < sb.length(); inner++) {
                            if (inner + 4 < sb.length()
                                    && "</p>".equalsIgnoreCase(sb.subSequence(
                                    inner, inner + 4).toString())) {

                                newSB.append(Jsoup
                                        .parse("<p "
                                                + sb.subSequence(count, inner)
                                                .toString() + "</p>")
                                        .text().replace("<", "")
                                        .replace("</", "").replace(">", "")
                                        .replace("/>", ""));
                                count = inner + 3;
                                break;
                            }
                        }

                    }
                    newSB.append(sb.charAt(count));
                }
                tempString = newSB.toString();
            } catch (Exception e) {
                e.printStackTrace();
                tempString = sb.toString().replace("<p>", "")
                        .replace("<P>", "").replace("</p>", "")
                        .replace("</P>", "").replace("<b>", "")
                        .replace("</b>", "").replace("<B>", "")
                        .replace("</B>", "").replace("</br>", "")
                        .replace("<br/>", "").replace("<br>", "");
            }

            sb.setLength(0);
            sb.append(tempString.replace("<![CDATA[", "").replace("]]>", "")
                    .replaceAll("&", "&amp;"));

            try {
                jObj = XML.toJSONObject(sb.toString());
                jObj = new JSONObject(jObj.toString().replaceAll("&amp;", "&"));
            } catch (Exception e) {
                e.printStackTrace();
                jObj = XML.toJSONObject("");
            }
            // Log.v("","SERVER RESPONSE : "+jObj.toString());
            return jObj;

        } catch (MalformedURLException e) {
            // URL is invalid
        } catch (SocketTimeoutException e) {
            // data retrieval or connection timed out
        } catch (IOException e) {
            // could not read response body
            // (could not create input stream)
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    public JSONObject getJSONFromUrl(boolean isPost, String strURL,
                                     String postData) throws Exception {

        HttpGet requestGet = null;
        HttpPost requestPost = null;
        HttpResponse response = null;
        HttpClient client = new DefaultHttpClient();

        if (isPost) {
            requestPost = new HttpPost(new URI(strURL));
            StringEntity entity = new StringEntity(postData, HTTP.UTF_8);
            requestPost.setEntity(entity);
            requestPost.addHeader("Content-Type", "text/xml");
            response = client.execute(requestPost);

        } else {
            Log.i("Request: ", strURL);
            requestGet = new HttpGet(new URI(strURL));
            response = client.execute(requestGet);
        }

        in = new BufferedReader(new InputStreamReader(response.getEntity()
                .getContent()));
        StringBuffer sb = new StringBuffer("");
        String line = "";
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();

        Log.i("res xml ", sb.toString());

        String tempString = null;
        try {
            StringBuffer newSB = new StringBuffer();
            for (int count = 0; count < sb.length(); count++) {
                if (count + 9 < sb.length()
                        && "<![CDATA[".equalsIgnoreCase(sb.subSequence(count,
                        count + 9).toString())) {
                    newSB.append("<![CDATA[");
                    count += 9;
                    for (int inner = count; inner < sb.length(); inner++) {
                        if (inner + 3 < sb.length()
                                && "]]>".equalsIgnoreCase(sb.subSequence(inner,
                                inner + 3).toString())) {
                            newSB.append(Jsoup
                                    .parse(sb.subSequence(count, inner)
                                            .toString()).text()
                                    .replace("<", "").replace("</", "")
                                    .replace(">", "").replace("/>", ""));
                            count = inner + 2;
                            newSB.append("]]");
                            break;
                        }
                    }

                }
                newSB.append(sb.charAt(count));
            }
            tempString = newSB.toString();
            newSB = new StringBuffer();
            sb = new StringBuffer(tempString);
            for (int count = 0; count < sb.length(); count++) {
                if (count + 3 < sb.length()
                        && "<p ".equalsIgnoreCase(sb.subSequence(count,
                        count + 3).toString())) {

                    count += 3;
                    for (int inner = count; inner < sb.length(); inner++) {
                        if (inner + 4 < sb.length()
                                && "</p>".equalsIgnoreCase(sb.subSequence(
                                inner, inner + 4).toString())) {

                            newSB.append(Jsoup
                                    .parse("<p "
                                            + sb.subSequence(count, inner)
                                            .toString() + "</p>")
                                    .text().replace("<", "").replace("</", "")
                                    .replace(">", "").replace("/>", ""));
                            count = inner + 3;
                            break;
                        }
                    }

                }
                newSB.append(sb.charAt(count));
            }
            tempString = newSB.toString();
        } catch (Exception e) {
            e.printStackTrace();
            tempString = sb.toString().replace("<p>", "").replace("<P>", "")
                    .replace("</p>", "").replace("</P>", "").replace("<b>", "")
                    .replace("</b>", "").replace("<B>", "").replace("</B>", "")
                    .replace("</br>", "").replace("<br/>", "")
                    .replace("<br>", "");
        }

        sb.setLength(0);
        sb.append(tempString.replace("<![CDATA[", "").replace("]]>", "")
                .replaceAll("&", "&amp;"));
        // sb.append(tempString);
        try {
            jObj = XML.toJSONObject(sb.toString());
            jObj = new JSONObject(jObj.toString().replaceAll("&amp;", "&"));
        } catch (Exception e) {
            e.printStackTrace();
            jObj = XML.toJSONObject("");
        }
        Log.i("res", jObj.toString());
        return jObj;
    }

    public JSONObject getJSONFromGetRequest(String strURL
    ) throws Exception {
        try {
            HttpGet requestGet = null;
            HttpResponse response = null;
            HttpClient client = new DefaultHttpClient();

            Log.i("Request: ", strURL);
            requestGet = new HttpGet(new URI(strURL));
            response = client.execute(requestGet);

            in = new BufferedReader(new InputStreamReader(response.getEntity()
                    .getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            try {
                while ((line = in.readLine()) != null) {
                    if ("".equals(line)) {
                        sb.append("\n\n");
                    }

                    sb.append(line);
                }

                in.close();
                Log.i("res xml ", sb.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            jObj = new JSONObject(sb.toString());

            return jObj;
        } catch (java.net.SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public class NullHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            Log.i("RestUtilImpl", "Approving certificate for " + hostname);
            return true;
        }
    }

    // Handling JSON Request and Response
    public JSONObject getUrlJsonPostResponse(String url,
                                             JSONObject urlParameters) {
        try {

            URL obj;
            HttpURLConnection con;

            obj = new URL(url);

            con = (HttpURLConnection) obj.openConnection();
            con.setConnectTimeout(60000); // Set connection timeout to 1
            // minute
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            Log.i("url : ", url);
            Log.i("urlParameter : ", urlParameters.toString());
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            // wr.writeBytes(urlParameters.toString());
            wr.write(urlParameters.toString().getBytes("UTF-8"));
            wr.flush();
            wr.close();

            in = new BufferedReader(new InputStreamReader(con.getInputStream(),
                    "UTF-8"));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            try {
                while ((line = in.readLine()) != null) {
                    if ("".equals(line)) {
                        sb.append("\n\n");
                    }

                    sb.append(line);
                }

                in.close();
                Log.i("res xml ", sb.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            jObj = new JSONObject(sb.toString());

            return jObj;

        } catch (java.net.SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject getUrlPostResponse(String url, String urlParameters,
                                         boolean isPost) {
        try {
            URL obj;
            HttpURLConnection con;

            if (isPost) {
                Log.d("ServerConnection", "Url is :" + url);
                obj = new URL(url);

                con = (HttpURLConnection) obj.openConnection();
                con.setConnectTimeout(60000); // Set connection timeout to 1
                // minute
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.addRequestProperty("Content-Type", "text/xml");
                Log.i("url : ", url);
                Log.i("urlParameters Code : ", urlParameters);
                DataOutputStream wr = new DataOutputStream(
                        con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
            } else {
                obj = new URL(url + urlParameters);
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setDoOutput(true);
                con.connect();

                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                in = new BufferedReader(isr);
            }

            StringBuffer sb = new StringBuffer("");
            String line = "";
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            while ((line = in.readLine()) != null) {
                if ("".equals(line)) {
                    sb.append("\n\n");
                }

                sb.append(line);
            }
            in.close();
            Log.i("res xml ", sb.toString());

            String tempString = null;
            try {
                StringBuffer newSB = new StringBuffer();
                for (int count = 0; count < sb.length(); count++) {
                    if (count + 9 < sb.length()
                            && "<![CDATA[".equalsIgnoreCase(sb.subSequence(
                            count, count + 9).toString())) {
                        newSB.append("<![CDATA[");
                        count += 9;
                        for (int inner = count; inner < sb.length(); inner++) {
                            if (inner + 3 < sb.length()
                                    && "]]>".equalsIgnoreCase(sb.subSequence(
                                    inner, inner + 3).toString())) {
                                if (CommonConstants.BR_TAG) {
                                    newSB.append(sb.subSequence(count, inner)
                                            .toString());
                                    CommonConstants.BR_TAG = false;
                                } else {
                                    newSB.append(Jsoup
                                            .parse(sb.subSequence(count, inner)
                                                    .toString()).text()
                                            .replace("<", "").replace("</", "")
                                            .replace(">", "").replace("/>", ""));
                                }
                                count = inner + 2;
                                newSB.append("]]");
                                break;
                            }
                        }

                    }
                    newSB.append(sb.charAt(count));
                }
                tempString = newSB.toString();
                newSB = new StringBuffer();
                sb = new StringBuffer(tempString);
                for (int count = 0; count < sb.length(); count++) {
                    if (count + 3 < sb.length()
                            && "<p ".equalsIgnoreCase(sb.subSequence(count,
                            count + 3).toString())) {

                        count += 3;
                        for (int inner = count; inner < sb.length(); inner++) {
                            if (inner + 4 < sb.length()
                                    && "</p>".equalsIgnoreCase(sb.subSequence(
                                    inner, inner + 4).toString())) {

                                newSB.append(Jsoup
                                        .parse("<p "
                                                + sb.subSequence(count, inner)
                                                .toString() + "</p>")
                                        .text().replace("<", "")
                                        .replace("</", "").replace(">", "")
                                        .replace("/>", ""));
                                count = inner + 3;
                                break;
                            }
                        }

                    }
                    newSB.append(sb.charAt(count));
                }
                tempString = newSB.toString();
            } catch (Exception e) {
                e.printStackTrace();
                tempString = sb.toString().replace("<p>", "")
                        .replace("<P>", "").replace("</p>", "")
                        .replace("</P>", "").replace("<b>", "")
                        .replace("</b>", "").replace("<B>", "")
                        .replace("</B>", "").replace("</br>", "")
                        .replace("<br/>", "").replace("<br>", "");
            }

            sb.setLength(0);
            sb.append(tempString.replace("<![CDATA[", "").replace("]]>", "")
                    .replaceAll("&", "&amp;"));

            try {
                jObj = XML.toJSONObject(sb.toString());
                jObj = new JSONObject(jObj.toString().replaceAll("&amp;", "&"));
            } catch (Exception e) {
                e.printStackTrace();
                jObj = XML.toJSONObject("");
            }
            Log.v("", "SERVER RESPONSE : " + jObj.toString());
            return jObj;

        } catch (java.net.SocketTimeoutException e) {
            e.printStackTrace();
            return null;
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public class CategoryDetails {

        int catId;
        String catName;

    }

    public JSONObject getUrlPostResponseWithHtmlContent(String url,
                                                        String urlParameters, boolean isPost) {
        try {
            URL obj;
            HttpURLConnection con;

            HttpGet requestGet = null;
            HttpResponse response = null;
            HttpClient client = new DefaultHttpClient();

            if (isPost) {
                obj = new URL(url);
                con = (HttpURLConnection) obj.openConnection();
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.addRequestProperty("Content-Type", "text/xml");
                Log.i("urlParameters Code : ", urlParameters);
                DataOutputStream wr = new DataOutputStream(
                        con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
            } else {
                // obj = new URL(url + urlParameters);
                // con = (HttpURLConnection) obj.openConnection();
                // con.setRequestMethod("GET");
                // con.setDoOutput(true);
                // con.connect();
                //
                // isr = new InputStreamReader(con.getInputStream());
                // in = new BufferedReader(isr);

                requestGet = new HttpGet(new URI(url));
                response = client.execute(requestGet);

                in = new BufferedReader(new InputStreamReader(response
                        .getEntity().getContent()));
            }

            StringBuffer sb = new StringBuffer("");
            String line = "";
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            in.close();
            String tempString = null;
            try {
                StringBuffer newSB = new StringBuffer();
                for (int count = 0; count < sb.length(); count++) {
                    if (count + 9 < sb.length()
                            && "<![CDATA[".equalsIgnoreCase(sb.subSequence(
                            count, count + 9).toString())) {
                        newSB.append("<![CDATA[");
                        count += 9;
                        for (int inner = count; inner < sb.length(); inner++) {
                            if (inner + 3 < sb.length()
                                    && "]]>".equalsIgnoreCase(sb.subSequence(
                                    inner, inner + 3).toString())) {
                                newSB.append(sb.subSequence(count, inner)
                                        .toString());
                                count = inner + 2;
                                newSB.append("]]");
                                break;
                            }
                        }

                    }
                    newSB.append(sb.charAt(count));
                }
                tempString = newSB.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }

            sb.setLength(0);
            sb.append(tempString.replace("<![CDATA[", "").replace("]]>", "")
                    .replaceAll("&", "&amp;"));


            try {
                jObj = XML.toJSONObject(sb.toString());
                jObj = new JSONObject(jObj.toString().replaceAll("&amp;", "&"));
            } catch (Exception e) {
                e.printStackTrace();
                jObj = XML.toJSONObject("");
            }
            return jObj;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
