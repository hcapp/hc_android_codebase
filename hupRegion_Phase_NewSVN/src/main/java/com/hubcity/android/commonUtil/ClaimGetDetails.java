package com.hubcity.android.commonUtil;

/**
 * Created by subramanya.v on 10/4/2016.
 */
public class ClaimGetDetails {

    private final String retLocationId;
    private final String userId;
    private final String hubCitiId;

    public ClaimGetDetails(String retailLocationID, String userId, String hubCitiId) {
        this.retLocationId = retailLocationID;
        this.userId = userId;
        this.hubCitiId = hubCitiId;

    }
}
