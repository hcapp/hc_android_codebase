package com.hubcity.android.commonUtil;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.screens.MenuPropertiesActivity;

public class MenuDataHelper {
    Context csContext;
    public String strDateCreated = "";

    public MenuDataHelper(Context context) {
        csContext = context;
    }

    public void creatingTable(SQLiteDatabase sql, String strTableName) {
        sql.execSQL("Create Table if not exists " + strTableName + "("
                + tableFields() + ")");
    }

    public void creatingBottomTable(SQLiteDatabase sql, String strTableName) {
        sql.execSQL("Create Table if not exists " + strTableName + "("
                + tableBottomFields() + ")");
    }

    public String tableFields() {
        return "linkId TEXT,dateModified TEXT,isTempChanged INTEGER,mBannerImg TEXT,mBkgrdColor TEXT,mBkgrdImage TEXT,smBtnColor TEXT,smBkgrdImage TEXT," +
                "smBkgrdColor TEXT,smBtnFontColor TEXT,mFontColor TEXT,retGroupImg TEXT,androidLink TEXT,retAffCount TEXT,retAffId INTEGER,retAffName TEXT," +
                "templateBgColor TEXT,menuName TEXT," +
                "appIconImg TEXT,typeFlag INTEGER,menuId INTEGER,templateName TEXT,noOfColumns TEXT,responseText TEXT,departFlag INTEGER,level INTEGER,smFontColor TEXT," +
                "MenuItem BLOB,MenuItem1 BLOB,isDisplayLabel INTEGER,TempBkgrdImg TEXT,btnBkgrdColor TEXT,btnLblColor TEXT,isNewTicker TEXT,tickerTxtColor TEXT," +
                "tickerBkgrdColor TEXT,tickerDirection TEXT,tickerMode TEXT";
    }

    public String tableBottomFields() {
        return "linkId TEXT,dateModified TEXT,BottomButton BLOB,BottomButton1 BLOB";
    }

    private String tableTickerFields() {
        return "linkId TEXT,TickerData BLOB";
    }


    public void insertingTickerValues(SQLiteDatabase csSql, JSONArray tickerItem, String mLinkId) {
        ContentValues csContent = new ContentValues();
        csContent.put("linkId", mLinkId);
        csContent.put("TickerData",
                Utils.convertingJsonArrayToByteArray(tickerItem));
        long rowInserted = csSql.insert("ticker", null, csContent);
        Log.v("tickerInserted", String.valueOf(rowInserted));

    }

    public void insertingBottomValues(SQLiteDatabase sql, String menuid,
                                      String date, JSONArray jsonarr, JSONObject jsonarr1) {
        ContentValues csContent = new ContentValues();
        csContent.put("linkId", menuid);
        csContent.put("dateModified", date);
        csContent.put("BottomButton",
                Utils.convertingJsonArrayToByteArray(jsonarr));
        csContent.put("BottomButton1",
                Utils.convertingJsonObjectToByteArray(jsonarr1));
        sql.insert("bottomtable", null, csContent);
    }

    public void updatingBottomValues(SQLiteDatabase sql, String linkid,
                                     String whereArgs, String date, JSONArray jsonarr,
                                     JSONObject jsonarr1) {
        ContentValues csContent = new ContentValues();
        csContent.put("linkId", whereArgs);
        csContent.put("dateModified", date);
        csContent.put("BottomButton",
                Utils.convertingJsonArrayToByteArray(jsonarr));
        csContent.put("BottomButton1",
                Utils.convertingJsonObjectToByteArray(jsonarr1));
        sql.update("bottomtable", csContent, linkid + "=" + whereArgs, null);
    }

    public void handlingGeneric(ContentValues csContent, String linkid,
                                MenuAsyncTask obj, JSONArray jsonarr, JSONObject jsonarr1) {
        csContent.put("linkId", linkid);
        csContent.put("dateModified", MenuAsyncTask.dateModified);
        csContent.put("isTempChanged", obj.isTempChanged);
        csContent.put("mBannerImg", obj.mBannerImg);
        csContent.put("mBkgrdColor", obj.mBkgrdColor);
        csContent.put("mBkgrdImage", obj.mBkgrdImage);
        csContent.put("smBtnColor", obj.smBkgrdColor);
        csContent.put("smBkgrdImage", obj.smBkgrdImage);
        csContent.put("smBkgrdColor", obj.smBkgrdColor);
        csContent.put("smBtnFontColor", obj.smBtnFontColor);
        csContent.put("mFontColor", obj.mFontColor);
        csContent.put("retGroupImg", obj.cityExpImgUlr);
        csContent.put("androidLink", obj.appPlaystoreLInk);
        csContent.put("retAffCount", obj.retAffCount);
        csContent.put("retAffId", obj.retAffId);
        csContent.put("retAffName", obj.retAffName);
        csContent.put("templateBgColor", obj.templateBgColor);
        csContent.put("menuName", MenuPropertiesActivity.menuName);
        csContent.put("appIconImg", obj.appIconImg);
        csContent.put("smFontColor", obj.smFontColor);
        csContent.put("departFlag", obj.departFlag);
        csContent.put("typeFlag", obj.typeFlag);
        csContent.put("menuId", obj.mainMenuID);
        csContent.put("templateName", obj.templateName);
        csContent.put("noOfColumns", obj.noOfColumns);
        csContent.put("level", obj.mLevel);
        csContent
                .put("MenuItem", Utils.convertingJsonArrayToByteArray(jsonarr));
        csContent.put("MenuItem1",
                Utils.convertingJsonObjectToByteArray(jsonarr1));

        csContent.put("TempBkgrdImg",
                obj.TempBkgrdImg);
        csContent.put("isDisplayLabel",
                obj.isDisplayLabel);
        csContent.put("btnBkgrdColor",
                obj.btnBkgrdColor);
        csContent.put("btnLblColor",
                obj.btnLblColor);

        csContent.put("isNewTicker",
                obj.isNewTicker);
        csContent.put("tickerTxtColor",
                obj.tickerTxtColor);
        csContent.put("tickerBkgrdColor",
                obj.tickerBkgrdColor);
        csContent.put("tickerDirection",
                obj.tickerDirection);
        csContent.put("tickerMode",
                obj.tickerMode);
    }

    public void insertingValues(SQLiteDatabase sql, String strtable,
                                String strlinkid, MenuAsyncTask obj, JSONArray jsonarr,
                                JSONObject jsonarr1) {
        Log.v("", "Data Inserted SUccessfully..............");
        ContentValues csInsertContent = new ContentValues();
        handlingGeneric(csInsertContent, strlinkid, obj, jsonarr, jsonarr1);

        long menuInserted = sql.insert(strtable, null, csInsertContent);
        Log.v("rowInserted", String.valueOf(menuInserted));
    }

    public void updatemanimenu(SQLiteDatabase sql, String strtable,
                               String strlinkid, String whereArgs, MenuAsyncTask obj,
                               JSONArray jsonarr, JSONObject jsonarr1) {
        Log.v("", "Data Updated SUccessfully.............. " + whereArgs
                + " , " + jsonarr);
        ContentValues csupdateContent = new ContentValues();
        handlingGeneric(csupdateContent, whereArgs, obj, jsonarr, jsonarr1);
        sql.update(strtable, csupdateContent, strlinkid + "=" + whereArgs, null);

    }

    public boolean checkingLevels(SQLiteDatabase sql, String table, String level) {
        if (sql != null) {
            Cursor csCursor = sql.rawQuery("select * from " + table
                    + " where linkId=" + level, null);
            if (csCursor != null && csCursor.getCount() > 0) {
                csCursor.moveToFirst();
                strDateCreated = csCursor.getString(csCursor
                        .getColumnIndex("dateModified"));

                csCursor.close();
                return true;
            }
        }
        return false;
    }

    public int checkingCounting(SQLiteDatabase sql) {
        int icount = 0;
        if (sql != null) {
            Cursor csCursor = sql.rawQuery("select * from menu", null);
            if (csCursor != null && csCursor.getCount() > 0) {
                csCursor.moveToFirst();
                for (int index = 0; index < csCursor.getCount(); index++) {
                    Log.v("",
                            "LINK ID .............. "
                                    + csCursor.getString(csCursor
                                    .getColumnIndex("linkId")));
                    csCursor.moveToNext();
                }

                csCursor.close();
                return icount;
            }
        }
        return icount;
    }

    public boolean checkingSubLevels(SQLiteDatabase sql, String table,
                                     String level) {
        if (sql != null) {
            Cursor csCursor = sql.rawQuery("select * from " + table
                    + " where linkId=" + level, null);
            if (csCursor != null && csCursor.getCount() > 0) {
                csCursor.moveToFirst();
                csCursor.close();
                return true;
            }
        }
        return false;
    }


    public boolean isCacheAvailable(SQLiteDatabase sql,
                                    String strtable, String ilevel, MenuAsyncTask objMenu) {
        boolean bAvailable = false;
        if (sql != null) {
            Cursor csCursor = sql.rawQuery("select * from " + strtable
                    + " where linkId=" + ilevel, null);
            if (csCursor != null) {
                if (csCursor.getCount() > 0) {
                    bAvailable = true;
                }
                csCursor.close();
            }
        }
        return bAvailable;

    }

    public ArrayList<MainMenuBO> retrievingValues(SQLiteDatabase sql,
                                                  String strtable, String ilevel, MenuAsyncTask objMenu)
            throws JSONException {
        JSONArray menuItemJsonObjArray = null;
        if (sql != null) {
            Cursor csCursor = sql.rawQuery("select * from " + strtable
                    + " where linkId=" + ilevel, null);
            if (csCursor != null) {
                if (csCursor.getCount() > 0) {
                    csCursor.moveToFirst();
                    for (int index = 0; index < csCursor.getCount(); index++) {
                        MenuAsyncTask.dateModified = csCursor
                                .getString(csCursor
                                        .getColumnIndex("dateModified"));

                        objMenu.isTempChanged = csCursor.getInt(csCursor
                                .getColumnIndex("isTempChanged"));

                        objMenu.mBannerImg = csCursor.getString(csCursor
                                .getColumnIndex("mBannerImg"));

                        objMenu.mBkgrdColor = csCursor.getString(csCursor
                                .getColumnIndex("mBkgrdColor"));

                        objMenu.mBkgrdImage = csCursor.getString(csCursor
                                .getColumnIndex("mBkgrdImage"));

                        objMenu.smBkgrdColor = csCursor.getString(csCursor
                                .getColumnIndex("smBtnColor"));

                        objMenu.smBkgrdImage = csCursor.getString(csCursor
                                .getColumnIndex("smBkgrdImage"));

                        objMenu.smBkgrdColor = csCursor.getString(csCursor
                                .getColumnIndex("smBkgrdColor"));

                        objMenu.templateBgColor = csCursor.getString(csCursor
                                .getColumnIndex("templateBgColor"));


                        objMenu.smBtnFontColor = csCursor.getString(csCursor
                                .getColumnIndex("smBtnFontColor"));

                        objMenu.mFontColor = csCursor.getString(csCursor
                                .getColumnIndex("mFontColor"));

                        objMenu.smFontColor = csCursor.getString(csCursor
                                .getColumnIndex("smFontColor"));

                        objMenu.departFlag = csCursor.getString(csCursor
                                .getColumnIndex("departFlag"));

                        objMenu.cityExpImgUlr = csCursor.getString(csCursor
                                .getColumnIndex("retGroupImg"));

                        objMenu.appIconImg = csCursor.getString(csCursor
                                .getColumnIndex("appIconImg"));

                        objMenu.appPlaystoreLInk = csCursor.getString(csCursor
                                .getColumnIndex("androidLink"));

                        objMenu.retAffCount = csCursor.getString(csCursor
                                .getColumnIndex("retAffCount"));

                        if (null != objMenu.retAffCount
                                && "1".equals(objMenu.retAffCount)) {

                            objMenu.retAffId = csCursor.getString(csCursor
                                    .getColumnIndex("retAffId"));

                            objMenu.retAffName = csCursor.getString(csCursor
                                    .getColumnIndex("retAffName"));
                        }

                        MenuPropertiesActivity.menuName = csCursor
                                .getString(csCursor.getColumnIndex("menuName"));
                        MenuAsyncTask.menuNames.add(csCursor.getString(csCursor
                                .getColumnIndex("menuName")));
                        objMenu.typeFlag = csCursor.getString(csCursor
                                .getColumnIndex("typeFlag"));

                        String menuId = csCursor.getString(csCursor
                                .getColumnIndex("menuId"));
                        objMenu.templateName = csCursor.getString(csCursor
                                .getColumnIndex("templateName"));

                        objMenu.noOfColumns = csCursor.getString(csCursor
                                .getColumnIndex("noOfColumns"));

                        objMenu.isDisplayLabel = csCursor.getInt(csCursor
                                .getColumnIndex("isDisplayLabel"));

                        objMenu.TempBkgrdImg = csCursor.getString(csCursor
                                .getColumnIndex("TempBkgrdImg"));

                        objMenu.btnBkgrdColor = csCursor.getString(csCursor
                                .getColumnIndex("btnBkgrdColor"));

                        objMenu.btnLblColor = csCursor.getString(csCursor
                                .getColumnIndex("btnLblColor"));


                        objMenu.tickerTxtColor = csCursor.getString(csCursor
                                .getColumnIndex("tickerTxtColor"));
                        objMenu.tickerBkgrdColor = csCursor.getString(csCursor
                                .getColumnIndex("tickerBkgrdColor"));
                        objMenu.tickerDirection = csCursor.getString(csCursor
                                .getColumnIndex("tickerDirection"));
                        objMenu.tickerMode = csCursor.getString(csCursor
                                .getColumnIndex("tickerMode"));
                        objMenu.isNewTicker = csCursor.getString(csCursor
                                .getColumnIndex("isNewTicker"));


                        if (menuId != null) {
                            objMenu.mainMenuID = menuId;
                        }
                        Log.v("", "Template Name : " + objMenu.templateName);
                        int levelInt = csCursor.getInt(csCursor
                                .getColumnIndex("level"));
                        objMenu.level = Integer.toString(levelInt);
                        byte[] csmenuByteArray1 = csCursor.getBlob(csCursor
                                .getColumnIndex("MenuItem1"));
                        String strNewmenu1 = new String(csmenuByteArray1);
                        JSONObject menuItem = null;

                        if (!strNewmenu1.equals("")) {
                            menuItem = new JSONObject(strNewmenu1);
                        }

                        if (menuItem == null) {
                            byte[] csmenuByteArray = csCursor.getBlob(csCursor
                                    .getColumnIndex("MenuItem"));
                            String strNewmenu = new String(csmenuByteArray);
                            menuItemJsonObjArray = new JSONArray(strNewmenu);
                            Log.v("", "JSON ARRAY GETTING "
                                    + menuItemJsonObjArray.length());
                        } else {
                            String mItemName = menuItem
                                    .getString(MenuAsyncTask.MENU_ITEM_NAME);
                            String mItemId = menuItem
                                    .getString(MenuAsyncTask.MENU_ID);
                            String linkTypeId = menuItem
                                    .getString(MenuAsyncTask.MENU_LINKTYPE_ID);
                            String linkTypeName = menuItem
                                    .getString(MenuAsyncTask.MENU_LINK_TYPE_NAME);

                            String position = menuItem
                                    .getString(MenuAsyncTask.MENU_ITEM_POSITION);
                            String mItemImgUrl = menuItem
                                    .getString(MenuAsyncTask.MENU_ITEM_ITEM_IMG_URL);

                            String linkId = null;

                            String mgrpFntColor = null, sgrpFntColor = null, mgrpBgColor = null,
                                    mBkgrdColor = null, sgrpBgColor = null, mShapeName = null,
                                    mBkgrdImage = null;

                            if (menuItem.has(MenuAsyncTask.MENU_ITEM_LINKID)) {
                                int linkIdInt = menuItem
                                        .getInt(MenuAsyncTask.MENU_ITEM_LINKID);
                                linkId = Integer.toString(linkIdInt);
                            }

                            String mBtnColor = menuItem
                                    .getString(MenuAsyncTask.MENU_ITEM_MBTNFONTCOLOR);
                            String mBtnFontColor = menuItem
                                    .getString(MenuAsyncTask.MENU_ITEM_MBTNFONTCOLOR);
                            String smBtnColor = menuItem
                                    .getString(MenuAsyncTask.MENU_ITEM_SMBTNCOLOR);
                            String smBtnFontColor = menuItem
                                    .getString(MenuAsyncTask.MENU_ITEM_SMBTNFONTCOLOR);

                            if (menuItem.has(MenuAsyncTask.MM_GROUP_FONT_COLOR)) {
                                mgrpFntColor = menuItem
                                        .getString(MenuAsyncTask.MM_GROUP_FONT_COLOR);

                            }

                            if (menuItem.has(MenuAsyncTask.SM_GROUP_FONT_COLOR)) {
                                sgrpFntColor = menuItem
                                        .getString(MenuAsyncTask.SM_GROUP_FONT_COLOR);

                            }

                            if (menuItem.has(MenuAsyncTask.MM_GROUP_BG_COLOR)) {
                                mgrpBgColor = menuItem
                                        .getString(MenuAsyncTask.MM_GROUP_BG_COLOR);

                            }

                            if (menuItem.has(MenuAsyncTask.SM_GROUP_BG_COLOR)) {
                                sgrpBgColor = menuItem
                                        .getString(MenuAsyncTask.SM_GROUP_BG_COLOR);

                            }
                            if (menuItem.has(MenuAsyncTask.m_ShapeName)) {
                                mShapeName = menuItem
                                        .getString(MenuAsyncTask.m_ShapeName);

                            }

                            if (menuItem.has("mBkgrdImage")) {
                                mBkgrdImage = menuItem.getString("mBkgrdImage");

                            }

                            if (menuItem.has("mBkgrdColor")) {
                                mBkgrdColor = menuItem.getString("mBkgrdColor");

                            }

                            MainMenuBO mMainMenuBO = new MainMenuBO(mItemId,
                                    mItemName, linkTypeId, linkTypeName,
                                    Integer.parseInt(position), mItemImgUrl,
                                    linkId, mBtnColor, mBtnFontColor,
                                    smBtnColor, smBtnFontColor, objMenu.level,
                                    mgrpFntColor, sgrpFntColor, mgrpBgColor,
                                    sgrpBgColor, mShapeName, mBkgrdImage, mBkgrdColor);
                            objMenu.mainMenuUnsortedList.add(mMainMenuBO);
                        }

                        // Log.i(DEBUG_TAG, " level :" + level);
                        if (menuItemJsonObjArray != null) {
                            for (int i = 0; i < menuItemJsonObjArray.length(); i++) {

                                // tab's group's header
                                JSONObject menuItemJsonObj = menuItemJsonObjArray
                                        .getJSONObject(i);

                                String mItemName = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_ITEM_NAME);
                                String mItemId = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_ID);
                                String linkTypeId = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_LINKTYPE_ID);
                                String linkTypeName = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_LINK_TYPE_NAME);
                                String position = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_ITEM_POSITION);
                                String mItemImgUrl = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_ITEM_ITEM_IMG_URL);
                                String linkId = null;
                                String mgrpFntColor = null, sgrpFntColor = null, mgrpBgColor =
                                        null, mBkgrdColor = null, sgrpBgColor = null, mShapeName = null;

                                if (menuItemJsonObj
                                        .has(MenuAsyncTask.MENU_ITEM_LINKID)) {
                                    int linkIdInt = menuItemJsonObj
                                            .getInt(MenuAsyncTask.MENU_ITEM_LINKID);
                                    linkId = Integer.toString(linkIdInt);
                                }

                                String mBtnColor = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_ITEM_MBTNCOLOR);
                                String mBtnFontColor = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_ITEM_MBTNFONTCOLOR);
                                String smBtnColor = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_ITEM_SMBTNCOLOR);
                                String smBtnFontColor = menuItemJsonObj
                                        .getString(MenuAsyncTask.MENU_ITEM_SMBTNFONTCOLOR);
                                if (menuItemJsonObj
                                        .has(MenuAsyncTask.MM_GROUP_FONT_COLOR)) {
                                    mgrpFntColor = menuItemJsonObj
                                            .getString(MenuAsyncTask.MM_GROUP_FONT_COLOR);

                                }

                                if (menuItemJsonObj
                                        .has(MenuAsyncTask.SM_GROUP_FONT_COLOR)) {
                                    sgrpFntColor = menuItemJsonObj
                                            .getString(MenuAsyncTask.SM_GROUP_FONT_COLOR);

                                }

                                if (menuItemJsonObj
                                        .has(MenuAsyncTask.MM_GROUP_BG_COLOR)) {
                                    mgrpBgColor = menuItemJsonObj
                                            .getString(MenuAsyncTask.MM_GROUP_BG_COLOR);

                                }

                                if (menuItemJsonObj
                                        .has(MenuAsyncTask.SM_GROUP_BG_COLOR)) {
                                    sgrpBgColor = menuItemJsonObj
                                            .getString(MenuAsyncTask.SM_GROUP_BG_COLOR);

                                }
                                if (menuItemJsonObj
                                        .has(MenuAsyncTask.m_ShapeName)) {
                                    mShapeName = menuItemJsonObj
                                            .getString(MenuAsyncTask.m_ShapeName);

                                }
                                if (menuItemJsonObj.has("mBkgrdColor")) {
                                    mBkgrdColor = menuItemJsonObj.getString("mBkgrdColor");

                                }
                                MainMenuBO mMainMenuBO = new MainMenuBO(
                                        mItemId, mItemName, linkTypeId,
                                        linkTypeName,
                                        Integer.parseInt(position),
                                        mItemImgUrl, linkId, mBtnColor,
                                        mBtnFontColor, smBtnColor,
                                        smBtnFontColor, objMenu.level,
                                        mgrpFntColor, sgrpFntColor,
                                        mgrpBgColor, sgrpBgColor, mShapeName,
                                        null, mBkgrdColor);
                                objMenu.mainMenuUnsortedList.add(mMainMenuBO);

                            }

                        }
                        try {
                            ArrayList<BottomButtonBO> bottomButtonList = parseForBottomButtonDB(
                                    sql, objMenu.mLinkId);
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(bottomButtonList);

                            MenuAsyncTask.bottomButtonList = bottomButtonList;

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        //to fetch newsTicker
                        try {
                            MenuAsyncTask.tickerItem = parseForTickerDB(
                                    sql, objMenu.mLinkId);

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                        csCursor.moveToNext();
                    }
                }
                csCursor.close();
            }
        }
        return objMenu.mainMenuUnsortedList;
    }

    private ArrayList<TickerObj> parseForTickerDB(SQLiteDatabase sql, String mLinkId) throws JSONException {
        ArrayList<TickerObj> listTicker = null;
        Cursor csCur1 = sql.rawQuery("select * from ticker where linkId="
                + mLinkId, null);
        if (csCur1 != null && csCur1.getCount() > 0) {
            csCur1.moveToFirst();
            for (int iloop = 0; iloop < csCur1.getCount(); iloop++) {
                listTicker = new ArrayList<>();
                byte[] csBottomByteArray = csCur1.getBlob(csCur1
                        .getColumnIndex("TickerData"));
                String strNewBottom = new String(csBottomByteArray);
                JSONArray arrayTicker = null;
                if (!strNewBottom.equals("")) {
                    arrayTicker = new JSONArray(strNewBottom);
                }

                String btnLinkID = null;
                if (arrayTicker != null && arrayTicker.length() > 0) {

                    for (int i = 0; i < arrayTicker.length(); i++) {

                        JSONObject tickerButton = arrayTicker
                                .getJSONObject(i);
                        if (tickerButton != null) {
                            String title = tickerButton
                                    .optString("title");
                            String catId = tickerButton
                                    .optString("catId");
                            String catName = tickerButton
                                    .optString("catName");
                            String rowCount = tickerButton
                                    .optString("rowCount");
                            TickerObj tickerDet = new TickerObj();
                            tickerDet.setCatName(catName);
                            tickerDet.setTitle(title);
                            tickerDet.setCatId(catId);
                            tickerDet.setRowCount(rowCount);

                            listTicker.add(tickerDet);
                        }
                    }

                }
                csCur1.moveToNext();
            }
            csCur1.close();
        }
        if (listTicker != null && !listTicker.isEmpty()) {
            return listTicker;
        } else {
            return null;
        }

    }

    private ArrayList<BottomButtonBO> parseForBottomButtonDB(
            SQLiteDatabase sql, String mainMenuID2) throws JSONException {
        ArrayList<BottomButtonBO> listBottomButton = null;
        Cursor csCur1 = sql.rawQuery("select * from bottomtable where linkId="
                + mainMenuID2, null);
        Log.v("", "Total Bottom Button : " + csCur1.getCount());
        if (csCur1 != null && csCur1.getCount() > 0) {
            csCur1.moveToFirst();
            for (int iloop = 0; iloop < csCur1.getCount(); iloop++) {
                listBottomButton = new ArrayList<>();
                byte[] csBottomByteArray = csCur1.getBlob(csCur1
                        .getColumnIndex("BottomButton"));
                String strNewBottom = new String(csBottomByteArray);
                JSONArray arrayBottomButton = null;
                if (!strNewBottom.equals("")) {
                    arrayBottomButton = new JSONArray(strNewBottom);
                }

                String btnLinkID = null;
                if (arrayBottomButton != null && arrayBottomButton.length() > 0) {
                    // bottom button caching for Govqa
                    // JSONObject jsonObject = new JSONObject(
                    // arrayBottomButton.toString());
                    // MenuAsyncTask.bbJsonObj = jsonObject;

                    for (int i = 0; i < arrayBottomButton.length(); i++) {

                        JSONObject bottomButton = arrayBottomButton
                                .getJSONObject(i);
                        if (bottomButton != null) {
                            String bottomBtnID = bottomButton
                                    .getString("bottomBtnID");
                            String bottomBtnImg = bottomButton
                                    .getString("bottomBtnImg");
                            String bottomBtnImgOff = bottomButton
                                    .getString("bottomBtnImgOff");
                            String btnLinkTypeID = bottomButton
                                    .getString("btnLinkTypeID");

                            String btnLinkTypeName = "";
                            if (bottomButton.has("btnLinkTypeName")) {
                                btnLinkTypeName = bottomButton
                                        .getString("btnLinkTypeName");
                            }
                            String position = bottomButton
                                    .getString("position");
                            if (bottomButton.has("btnLinkID")) {
                                btnLinkID = bottomButton.getString("btnLinkID");

                            }

                            BottomButtonBO bottomButtonBO = new BottomButtonBO(
                                    bottomBtnID, bottomBtnImg, bottomBtnImgOff,
                                    btnLinkTypeID, btnLinkTypeName, position,
                                    btnLinkID);
                            listBottomButton.add(bottomButtonBO);
                        }
                    }

                } else {
                    byte[] csBottomByteArray1 = csCur1.getBlob(csCur1
                            .getColumnIndex("BottomButton1"));
                    String strNewBottom1 = new String(csBottomByteArray1);
                    JSONObject bottomButtonObj = null;
                    if (!strNewBottom1.equals("")) {
                        bottomButtonObj = new JSONObject(strNewBottom1);
                    }

                    if (bottomButtonObj != null) {
                        String bottomBtnID = bottomButtonObj
                                .getString("bottomBtnID");
                        String bottomBtnImg = bottomButtonObj
                                .getString("bottomBtnImg");
                        String bottomBtnImgOff = bottomButtonObj
                                .getString("bottomBtnImgOff");
                        String btnLinkTypeID = bottomButtonObj
                                .getString("btnLinkTypeID");
                        String btnLinkTypeName = bottomButtonObj
                                .getString("btnLinkTypeName");
                        String position = bottomButtonObj.getString("position");
                        if (bottomButtonObj.has("btnLinkID")) {
                            btnLinkID = bottomButtonObj.getString("btnLinkID");
                        }

                        BottomButtonBO bottomButtonBO = new BottomButtonBO(
                                bottomBtnID, bottomBtnImg, bottomBtnImgOff,
                                btnLinkTypeID, btnLinkTypeName, position,
                                btnLinkID);
                        listBottomButton.add(bottomButtonBO);
                    }
                }

                csCur1.moveToNext();
            }
            csCur1.close();
        }
        if (listBottomButton != null && !listBottomButton.isEmpty()) {
            return listBottomButton;
        } else {
            return null;
        }
    }

    public void creatingTickerTable(SQLiteDatabase csSql, String tickerTableName) {
        csSql.execSQL("Create Table if not exists " + tickerTableName + "("
                + tableTickerFields() + ")");
    }


}
