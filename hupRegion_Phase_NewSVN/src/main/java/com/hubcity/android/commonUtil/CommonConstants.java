package com.hubcity.android.commonUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.InputFilter;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.hubcity.android.businessObjects.RetailerBo;
import com.hubcity.android.govqa.GovQaFindInformation;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.SetPreferrenceScreen;
import com.hubcity.android.screens.ShowMapMultipleLocations;
import com.scansee.android.ResetPasswordAsync;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.hubregion.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonConstants {
    public static final String PREFERANCE_FILE = "ScanseePrefFile";
    public static final String AMPERSAND = "&";
    public static final String URL = "URL";
    public static final String TAB_SELECTED = "TabSelected";
    public static final String TAB_SELECT_BASE = "CouponSelectBase";
    public static final String TAB_PRODUCT = "product";
    public static final String TAB_LOCATION = "location";
    public static String smBkgrndColor;
    public static String mBkgrndColor;
    public static String mBkgrndImage;
    public static String smBkgrndImage;
    public static String previousMenuLevel;
    public static String commonLinkId;


    public static String smNewsBtnFontColor;
    public static String smNewsBtnColor;
    public static String mBtnNewsFontColor;
    public static String mBtnNewsColor;
    public static String mNewsBkgrdColor;
    public static String mNewsBkgrdImage;
    public static String smNewsBkgrdColor;
    public static String smNewsBkgrdImage;
    public static String newsCityExpImgUlr;
    public static String newsCityExpId;
    public static String mNewsFontColor;
    public static String smNewsFontColor;



    public static final String TAG_RESULTSET = "AuthenticateUser";
    public static final String TAG_UPDATEVERSIONTEXT = "updateVersionText";
    public static final String TAG_LATESTAPPVERURL = "latAndriodVerUrl";
    public static final String TAG_UPDATEFLAG = "forcefulUpdateVersionFlag";
    public static boolean isFromSliderScreen = false;

    // user tracking to get mainmenuid

    public static final String TAG_RESULTSET_MAINMENUID = "UserTrackingData";
    public static final String TAG_MAINMENUID = "mainMenuId";

    // facebook 1.5.1

    public static final String URL_ABOUT = "https://app.scansee.net/html/andrd/1.5" +
            ".3/AboutUs_1_5_3" +
            ".html";
    // GPS Constants
    public static final long MINIMUM_TIME_BETWEEN_UPDATES = 3000L;
    public static final float MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 10f;

    // allcoupons webservices
    public static final String ALLCOUPONSRETAILERDETAIL = "RetailerDetail";
    public static final String ALLCOUPONSCATID = "cateId";
    public static final String ALLCOUPONSCOUPONCATNAME = "cateName";
    public static final String ALLCOUPONSCOUPONID = "couponId";
    public static final String ALLCOUPONSCOUPONNAME = "couponName";
    public static final String ALLCOUPONSCOUPONDISCOUNTAMOUNT = "couponDiscountAmount";
    public static final String ALLCOUPONSEXPIRATIONDATE = "couponExpireDate";
    public static final String ALLCOUPONSDESCRIPTION = "coupDesc";
    public static final String ALLCOUPONSIMAGEPATH = "couponImagePath";
    public static final String ALLCOUPONSSHORTDESCRIPTION = "couponShortDescription";
    public static final String ALLCOUPONSSTARTDATE = "couponStartDate";

    public static final String ALLCOUPONSNEWFLAG = "newFlag";
    public static final String ALLCOUPONSUSED = "used";
    public static final String ALLCOUPONSURL = "couponURL";
    public static final String ALLCOUPONSVIEWABLE = "viewableOnWeb";
    public static final String ALLCOUPONSLOCATION = "location";
    public static final String ALLCOUPONSUSEDCOUPONID = "userCouponGalleryID";
    public static final String ALLCOUPONSFAVFLAG = "favFlag";

    // SAME AS PRODUCTSEARCH
    public static final String COUPONPRODUCTDESCRIPTION = "productShortDescription";
    public static final String COUPONPRODUCTIMAGEPATH = "imagePath";

    // find smartsearch webservices
    public static final String FINDSMARTSEARCHRESULTSET = "ProductDetails";
    public static final String FINDSMARTSEARCHMENU = "ProductDetail";
    public static final String FINDSMARTSEARCHCATEGORYNAME = "parentCategoryName";
    public static final String FINDSMARTSEARCHCATEGORIES = "categs";
    public static final String FINDSMARTSEARCHCATEGORYID = "parCatId";

    // find product search web services
    public static final String FINDPRODUCTSEARCHRESULTSET = "ProductDetails";
    public static final String FINDPRODUCTSEARCHMENU = "ProductDetail";
    public static final String FINDPRODUCTSEARCHIMAGEPATH = "productImagePath";
    public static final String FINDPRODUCTSEARCHPRODUCTID = "productId";
    public static final String FINDPRODUCTSEARCHPRODUCTLISTID = "prodListId";
    public static final String FINDPRODUCTSEARCHPRODUCTNAME = "productName";

    public static final String FINDPRODUCTSEARCHPRODUCTDESCRIPTION = "productDescription";
    public static final String FINDPRODUCTSEARCHPRODUCTPRODUCTCOUNT = "maxCount";
    public static final String FINDPRODUCTSEARCHPRODUCTROWNUMBER = "rowNumber";
    public static final String FINDPRODUCTSEARCHPRODUCTNEXTPAGE = "nextPage";
    public static final String FINDPRODUCTSEARCHPRODUCTCOUNT = "prdCount";

    // Webservice Params
    public static final String USER_ID = "userId";
    public static final String TAG_RETAILE_LOCATIONID = "retailLocationID";
    public static final String TAG_RETAIL_ID = "retailerId";
    public static final String TAG_RETAIL_LIST_ID = "retListId";

    public static final String TAG_RETAILE_ID = "retailID";
    public static final String TAG_RETAILE_NAME = "retailerName";
    public static final String ZIP_CODE = "zipcode";
    public static String LATITUDE = null;
    public static String LONGITUDE = null;


    public static final String TAG_RETAILER_RESULT = "retailers";
    public static final String TAG_RETAILER_NEXTPAGE = "nextPage";
    public static final String TAG_RETAILER_NAME = "retailerName";
    public static final String TAG_RETAILER_ID = "retailerId";
    public static final String TAG_RETAILER_LOCATION_ID = "retailLocationId";

    public static final String TAG_DISTANCE = "distance";
    public static final String TAG_DETAILS_ID = "retDetId";
    public static final String TAG_LOGOIMAGE_PATH = "logoImagePath";
    public static final String TAG_ROW_NUMBER = "rowNumber";
    public static final String TAG_BANNER_IMAGE_PATH = "bannerAdImagePath";
    public static final String TAG_RET_LATITIUDE = "latitude";
    public static final String TAG_RET_LONGITUDE = "longitude";

    public static final String TAG_RIBBON_ADIMAGE_PATH = "ribbonAdImagePath";
    public static final String TAG_RIBBON_AD_URL = "ribbonAdURL";
    public static final String TAG_SALE_FLAG = "saleFlag";
    public static final String TAG_FINDLOCATION_NAME = "name";
    public static final String TAG_FINDLOCATION_ADDRESS = "address";
    public static final String TAG_FINDLOCATION_LAT = "lat";
    public static final String TAG_FINDLOCATION_LON = "lng";
    public static final String TAG_FINDLOCATION_REFERENCE = "reference";
    public static final String TAG_FINDLOCATION_SCANSEERETAILERID = "retailerId";
    public static final String TAG_FINDLOCATION_RETAILERNAME = "retailerName";
    public static final String TAG_FINDLOCATION_SCANSEERETAILERLOCATIONID = "retailLocationId";
    public static final String TAG_FINDLOCATION_SCANSEERETAILERLISTID = "retListId";
    public static final String TAG_RETAILER_OPEN_CLOSE = "locationOpen";
    public static final String TAG_FINDLOCATION_SCANSEEADDRESS1 = "retaileraddress1";
    public static final String TAG_FINDLOCATION_SCANSEEADDRESS2 = "retaileraddress2";
    public static final String TAG_FINDSINGLE_CATEGORY_SCANSEEADDRESS = "completeAddress";
    public static final String TAG_FINDLOCATION_SCANSEECITY = "city";
    public static final String TAG_FINDLOCATION_SCANSEESTATE = "state";
    public static final String TAG_FINDLOCATION_SCANSEEPOSTALCODE = "postalCode";
    public static final String TAG_FINDLOCATION_SCANSEEDISTANCE = "distance";
    public static final String TAG_FINDLOCATION_SCANSEELOGOIMAGEPATH = "logoImagePath";
    public static final String TAG_FINDLOCATION_SCANSEEBANNERADIMAGEPATH = "bannerAdImagePath";
    public static final String TAG_FINDSINGLECATEGORY_LATITUDE = "retLatitude";
    public static final String TAG_FINDSINGLECATEGORY_LONGITUDE = "retLongitude";
    public static final String TAG_FINDSINGLECATEGORY_GROUP_CONTENT = "groupContent";
    public static final String TAG_FINDLOCATION_SCANSEERIBBONADIMAGEPATH = "ribbonAdImagePath";
    public static final String TAG_FINDLOCATION_SCANSEERIBBONADURL = "ribbonAdURL";
    public static final String TAG_FINDLOCATION_SCANSEESALEFLAG = "saleFlag";
    public static final String TAG_FINDLOCATION_SCANSEELAT = "retLatitude";
    public static final String TAG_FINDLOCATION_SCANSEELON = "retLongitude";
    public static final String TAG_GOOGLE_LATLNG = "latlng";
    public static final String TAG_GOOGLE_SENSOR = "sensor";

    // webservices for retailer module
    public static final String TAG_PAGE_RETAILERDETAIL = "RetailerDetail";
    public static final String TAG_PAGE_LINK = "pageLink";
    public static final String TAG_PAGE_TITLE = "pageTitle";
    public static final String TAG_PAGE_IMAGE = "pageImage";
    public static final String TAG_ADDRESS1 = "retaileraddress1";
    public static final String TAG_PAGE_INFO = "pageInfo";
    public static final String TAG_PHONE = "contactPhone";
    public static final String TAG_RETAILER_URL = "retailerURL";

    public static final String TAG_PRODUCTNAME = "productName";
    public static final String TAG_PRODUCTSHORTDESCRIPTION = "productShortDescription";
    public static final String TAG_PRODUCTLONGDESCRIPTION = "productLongDescription";
    public static final String TAG_IMAGEPATH = "imagePath";

    // currentsales_nearby
    public static final String TAG_FINDNEARBYDETAILSPRODUCTPRICE = "productPrice";

    // RATING REVIEW


    // special offers webservices
    public static final String TAG_CURRENTSALES_REGULARPRICE = "regularPrice";
    public static final String TAG_CURRENTSALES_SALEPRICE = "salePrice";
    public static final String TAG_CURRENTSALES_PRODUCTNAME = "productName";
    public static final String TAG_CURRENTSALES_PRODUCTMODELNUMBER = "modelNumber";
    public static final String TAG_CURRENTSALES_IMAGEPATH = "imagePath";
    public static final String TAG_CURRENTSALES_PRODUCTID = "productId";
    public static final String TAG_CURRENTSALES_RODUCTLISTID = "prodListId";

    public static final String TAG_SPECIALOFFER_PAGETITLE = "pageTitle";

    // Login
    public static final String TAG_LOGIN_RESULTSET = "response";
    public static final String TAG_LOGIN_RESPONSE_CODE = "responseCode";
    public static final String TAG_LOGIN_RESPONSETEXT = "responseText";

    // coupons redeem
    public static final String COUPON_TYPE_MY = "Mycoups";

    public static final String COUPON_TYPE_ALL = "Claimed";
    public static final String COUPON_TYPE_USED = "Used";
    public static final String COUPON_TYPE_EXPIRED = "Expired";
    public static final String TAG_COUPONREDEEM_RESULTSET = "response";
    public static final String TAG_COUPONREDEEM_RESPONSE_CODE = "responseCode";
    public static final String TAG_COUPONREDEEM_RESPONSETEXT = "responseText";

    // shopping List
    public static final String SHOPPINGLISTRESULTSET = "AddRemoveSBProducts";
    public static final String SHOPPINGLISTRESULTSET_CARTMENU = "cartProducts";
    public static final String SHOPPINGLIST_CATEGORY = "Category";
    public static final String SHOPPINGLIST_PRODUCTDETAIL = "ProductDetail";
    public static final String SHOPPINGLIST_PRODUCTIMAGEPATH = "productImagePath";
    public static final String SHOPPINGLIST_PARENTCATEGORYNAME = "parentCategoryName";
    public static final String SHOPPINGLIST_USERPRODUCTID = "userProductId";
    public static final String SHOPPINGLIST_PRODUCTID = "productId";
    public static final String SHOPPINGLIST_PRODUCTNAME = "productName";
    public static final String SHOPPINGLIST_PRODUCTSHORTDESC = "productShortDescription";
    public static final String SHOPPINGLIST_COUPONSTATUS = "coupon__Status";
    public static final String SHOPPINGLIST_REBATESTATUS = "rebate__Status";
    public static final String SHOPPINGLIST_LOYALTYSTATUS = "loyalty__Status";
    public static final String SHOPPINGLISTRESULTSET_BASKETMENU = "basketProducts";

    public static final String ADDTOSHOPPINGLISTRESULTSET = "response";
    public static final String ADDTOSHOPPINGLISTRESPONSECODE = "responseCode";
    public static final String ADDTOSHOPPINGLIST = "responseText";
    public static final String ADDTOSHOPPINGLISTUSERPRODUCTIDS = "UserProductIds";

    public static final String ADDTOSLRESULTSET = "response";
    public static final String ADDTOSL = "responseText";

    public static final String WISHLISTHISTORYPRODUCTNAME = "productName";

    // get coupon wish list
    public static final String WISHLISTCOUPONID = "couponId";
    public static final String WISHLISTCOUPONNAME = "couponName";
    public static final String WISHLISTCOUPONDISCOUNTAMOUNT = "couponDiscountAmount";
    public static final String WISHLISTCOUPONEXPIREDATE = "couponExpireDate";
    public static final String WISHLISTCOUPONUSAGE = "usage";
    public static final String WISHLISTCOUPONIMAGEPATH = "couponImagePath";

    // Added by Dileep
    public static final String TAG_SALE_FLG = "saleFlg";
    public static final String TAG_PAGE_TEMP_LINK = "pageTempLink";
    public static final String TAG_ANYTHING_PAGE_LIST_ID = "anythingPageListId";
    public static final String TAG_ANYTHING_PAGE_CLICK = "anythingPageClick";
    public static final String TAG_RET_DETAILS_ID = "retDetailsId";
    public static final String LINK_ID = "LinkId";


    public static final String CITYEXP_IMG_URL = "cityExpImgUlr";
    public static final String FILTERID_EXTRA = "retAffId";
    public static final String TAG_FILTERID = "retAffId";
    public static final String FILTERCOUNT_EXTRA = "retAffCount";
    public static final String TAG_FILTERNAME = "retAffName";
    public static final String TAG_RESPONE_TEXT = "responseText";
    public static final String TAG_RESPONE_CODE = "responseCode";
    //checking flag for </br> tag
    public static boolean BR_TAG = false;

    public static boolean hamburgerIsFirst = false;


    // Added to enable or disble toggle(Distance,Name,Map) click in retailer listing screen
    public static boolean disableClick;


    public static void resetPassword(final Context context, boolean isCanceleble) {
        // custom dialog for reset password
        final Dialog dialog = new Dialog(context, android.R.style
                .Theme_DeviceDefault_Light_Dialog);
        dialog.setContentView(R.layout.reset_password);
        dialog.setTitle("Reset Password");
        if (!isCanceleble) {
            dialog.setCancelable(false);
        }

        final EditText etNewPass = (EditText) dialog.findViewById(R.id.reset_password_new_pass_et);
        final EditText etConfirmPass = (EditText) dialog.findViewById(R.id
                .reset_password_confirm_pass_et);
        Button submitButton = (Button) dialog.findViewById(R.id.reset_password_submit_btn);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNewPass.getText().toString().equals("")) {
                    showToast(context, "Please enter new password");
                } else if (etConfirmPass.getText().toString().equals("")) {
                    showToast(context, "Please enter confirm password");
                } else if (etNewPass.getText().toString().length() < 6) {
                    showToast(context, "Password length should be minimum 6 Characters");
                } else if (!etNewPass.getText().toString().equals(etConfirmPass.getText().toString
                        ())) {
                    showToast(context, "Password Mismatch!");
                } else {
                    dialog.dismiss();
                    new ResetPasswordAsync(context).execute(etNewPass.getText().toString());
                }

            }
        });

        dialog.show();
    }

    private static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void emailConfigurationDialog(final Context context) {
        try {
            new AlertDialog.Builder(context, android.R.style.Theme_DeviceDefault_Light_Dialog)
                    .setTitle("Warning!")
                    .setMessage(context.getResources().getString(R.string.email_configuration_txt))
                    .setCancelable(false)
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            context.startActivity(new Intent(context, SetPreferrenceScreen
                                    .class).putExtra(
                                    "regacc", false));
                        }
                    }).create().show();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void startMapScreen(Context context, ArrayList<RetailerBo> mRetailerList, boolean isCoupon) {
        if (mRetailerList != null && mRetailerList.size() > 0) {

            Intent showMap = new Intent(context,
                    ShowMapMultipleLocations.class);
            showMap.putParcelableArrayListExtra("mRetailerList", mRetailerList);
            showMap.putExtra("isCoupon", isCoupon);
            context.startActivity(showMap);

        } else {
            Toast.makeText(
                    context,
                    context.getResources().getString(
                            R.string.alert_no_retailer), Toast.LENGTH_SHORT)
                    .show();
        }

    }

    public static String convertDeviceTimeToUTC() {
        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateAsString = outputFmt.format(new Date());
        return dateAsString;
    }

    public final static boolean isValidPhoneNumber(String phoneNumber) {
        final String PhonePattern = "^[+]?[0-9]{10,13}$";
        Pattern pattern;
        Matcher matcher;
        if (phoneNumber == null) {
            return false;
        } else {
            if (phoneNumber.length() < 10) {
                return false;
            } else {
                pattern = Pattern.compile(PhonePattern);
                matcher = pattern.matcher(phoneNumber);
                boolean vaildPhone = matcher.matches();
                return vaildPhone;
            }
        }
    }

    public final static boolean isValidUrl(String url) {
        final String websitePattern = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?";
        Pattern pattern;
        Matcher matcher;
        if (websitePattern == null) {
            return false;
        } else {
            pattern = Pattern.compile(websitePattern);
            matcher = pattern.matcher(url);
            boolean vaildPhone = matcher.matches();
            return vaildPhone;

        }
    }


    // Sorts the City Ids
    public static String getSortedCityIds(String cities_ids) {

        String[] cities_ids_array = cities_ids.split(",");

        Arrays.sort(cities_ids_array, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
            }
        });

        String sorted_cities_ids = Arrays.toString(cities_ids_array);
        //sorted_cities_ids = sorted_cities_ids.replace("[","").replace("]","");
        sorted_cities_ids = sorted_cities_ids.replace("[", "");
        sorted_cities_ids = sorted_cities_ids.replace("]", "");
        return sorted_cities_ids;
    }

    public static void displayToast(Context mContext, String message) {
        Toast.makeText(mContext, message,
                Toast.LENGTH_SHORT).show();
    }


    public static void clearLoginCredentials(Context mContext) {
        SharedPreferences pref = mContext.getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putString(Constants.GOV_QA_REMEMBER_NAME, null);
        prefEditor.putString(Constants.GOV_QA_REMEMBER_PASSWORD, null);
        prefEditor.putBoolean(Constants.GOV_QA_REMEMBER, false);
        prefEditor.apply();

    }

    public static void traverseSearchView(SearchView search, Activity mActivity) {
        Resources resource = mActivity.getResources();

        Drawable searchIcon = ResourcesCompat.getDrawable(resource, R.drawable.search_icon, null);
        Drawable cancelIcon = ResourcesCompat.getDrawable(resource, R.drawable.cancel_icon, null);

        //to change edit text colour
        int searcheditID = resource.getIdentifier("android:id/search_edit_frame", null, null);
        LinearLayout searchEditView = (LinearLayout) search.findViewById(searcheditID);
        LinearLayout.LayoutParams editFrameParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        editFrameParam.gravity = Gravity.CENTER_VERTICAL;
        searchEditView.setLayoutParams(editFrameParam);
        searchEditView.setBackgroundColor(Color.WHITE);


        //to set search icon inside edit text
        int searchMagImgId = resource.getIdentifier("android:id/search_mag_icon", null, null);
        ImageView searchMagImage = (ImageView) search.findViewById(searchMagImgId);
        LinearLayout.LayoutParams searchMagImageParam = new LinearLayout.LayoutParams(50, 50);
        searchMagImageParam.setMargins(10, 0, 0, 0);
        searchMagImageParam.gravity = Gravity.CENTER_VERTICAL;
        searchMagImage.setLayoutParams(searchMagImageParam);
        searchMagImage.setImageDrawable(searchIcon);

        //to hide default search icon inside edit text
        searchMagImage.setVisibility(View.GONE);

        //to change text colour and text hint and background colour
        int searchSrcTextId = resource.getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = (EditText) search.findViewById(searchSrcTextId);
        searchEditText.setTextColor(Color.BLACK);
        searchEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});


        //to add close icon
        int closeButtonId = resource.getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButtonImage = (ImageView) search.findViewById(closeButtonId);
        LinearLayout.LayoutParams closeButtonmageParam = new LinearLayout.LayoutParams(100, 100);
        closeButtonmageParam.gravity = Gravity.CENTER_VERTICAL;
        closeButtonmageParam.setMargins(0, 0, 0, 0);
        closeButtonImage.setLayoutParams(closeButtonmageParam);
        closeButtonImage.setImageDrawable(cancelIcon);


        //to add under line for outer edit text
        int searcheditPlateID = resource.getIdentifier("android:id/search_plate", null, null);
        LinearLayout searchUnderLine = (LinearLayout) search.findViewById(searcheditPlateID);
        searchUnderLine.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.white));

        //to remove default search icon and to expand view
        search.setIconifiedByDefault(false);

    }

    public static HashMap<String, String> getGpsValue(Activity mActivity) {
        HashMap<String, String> gpsValue = new HashMap<>();
        LocationManager locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        String provider = Settings.Secure.getString(mActivity.getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        String latitude = null;
        String longitude = null;
        if (provider.contains("gps")) {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                latitude = String.valueOf(locNew.getLatitude());
                longitude = String.valueOf(locNew.getLongitude());

            } else {
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    latitude = String.valueOf(locNew.getLatitude());
                    longitude = String.valueOf(locNew.getLongitude());
                }
            }
        }
        gpsValue.put("latitude", latitude);
        gpsValue.put("longitude", longitude);
        return gpsValue;
    }

    public static void setPaddingToSideMenu(CustomNavigation customNavigation ,int screenHeight)
    {
        customNavigation.setPadding(0, 0, 0, screenHeight / 12);
    }


    public static boolean isExists(String URLName) {

        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(URLName)
                    .openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public static File bitmapToFile(Bitmap bmp, int index) {
        String temp = "attachment" + index;
        OutputStream outStream;
        String path = Environment.getExternalStorageDirectory().toString();
        new File(path + "/shareTemp").mkdirs();
        File file = new File(path + "/shareTemp", temp + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(path + "/shareTemp", temp + ".png");
        }

        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file;
    }
}