package com.hubcity.android.commonUtil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.com.hubcity.rest.RestClient;
import com.hubcity.android.businessObjects.LoginScreenSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.screens.CustomNavigation;
import com.hubcity.android.screens.EventsListDisplay;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.hubcity.android.screens.SplashActivity;
import com.hubcity.android.screens.SubMenuDetails;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.CommonMethods;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.NavigationItem;
import com.scansee.newsfirst.NewsSideMenu;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.SideMenuObject;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class CustomTitleBar extends FragmentActivity {

    protected TextView title;
    protected Button leftTitleImage, refreshBtn;
    protected TextView divider;
    protected ImageView imageLogo, rightImage, backImage, drawerIcon;
    public static boolean isSubMenuStart,isNonFeed = false;
    protected DrawerLayout drawer;
    protected RelativeLayout parentLayout;

    public void closeKeypad(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context
                .INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Relaunch of app when app crashes
        if (savedInstanceState != null) {
            Intent intent = new Intent(this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.title_bar);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
                R.layout.title_bar);
        parentLayout = (RelativeLayout)findViewById(R.id.title_layout);
        //setting navigation bar background color
        if (Constants.getNavigationBarValues("titleBkGrdColor") != null) {
            parentLayout.setBackgroundColor(Color.parseColor
                    (Constants.getNavigationBarValues("titleBkGrdColor")));
        } else {
            parentLayout.setBackgroundColor(getResources()
                    .getColor(R.color.black));
        }

        title = (TextView) findViewById(R.id.title);
        //Setting title color
        if (Constants.getNavigationBarValues("titleTxtColor") != null) {
            title.setTextColor(Color.parseColor(Constants.getNavigationBarValues
                    ("titleTxtColor")));
        } else {
            title.setTextColor(getResources().getColor(R.color.white));
        }

        imageLogo = (ImageView) findViewById(R.id.image_logo);
        divider = (TextView) findViewById(R.id.view_div);
        divider.setVisibility(View.GONE);
        DisplayMetrics metrics = new DisplayMetrics();
        CustomTitleBar.this.getWindowManager().getDefaultDisplay()
                .getMetrics(metrics);
        int screenWidth = metrics.widthPixels;

        title.setWidth(screenWidth / 2);
        rightImage = (ImageView) findViewById(R.id.right_button);
        //loading home image url to imageview
        if (Constants.getNavigationBarValues("homeImgPath") != null) {
            Picasso.with(this).load(Constants.getNavigationBarValues("homeImgPath").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (rightImage);
        } else {
            rightImage.setBackgroundResource(R.drawable.mainmenu_white);
        }

        leftTitleImage = (Button) findViewById(R.id.left_title_image);
        backImage = (ImageView) findViewById(R.id.back_btn);
        findViewById(R.id.login_icon).setVisibility(View.GONE);
        //Loading back image
        if (Constants.getNavigationBarValues("bkImgPath") != null && !isNonFeed) {
            Picasso.with(this).load(Constants.getNavigationBarValues("bkImgPath").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (backImage);
        } else {
            backImage.setBackgroundResource(R.drawable.ic_action_back);
        }

        refreshBtn = (Button) findViewById(R.id.refresh_btn);
        refreshBtn.setVisibility(View.GONE);

        LoginScreenSingleton loginScreenSingleton = LoginScreenSingleton
                .getLoginScreenSingleton();
        if (loginScreenSingleton != null) {
            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // is there any way to write here so that i can avoid
                    // putting in each subclass having edittext ?
                    // u got it?
                    // @Ramachandran

                    if (GlobalConstants.isFromNewsTemplate) {
                        Intent intent = null;
                        switch (GlobalConstants.className) {
                            case Constants.COMBINATION:
                                intent = new Intent(CustomTitleBar.this, CombinationTemplate
                                        .class);
                                break;
                            case Constants.SCROLLING:
                                intent = new Intent(CustomTitleBar.this, ScrollingPageActivity
                                        .class);
                                break;
                            case Constants.NEWS_TILE:
                                intent = new Intent(CustomTitleBar.this,
                                        TwoTileNewsTemplateActivity.class);
                                break;
                        }
                        if (intent != null) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    } else {
                        if (SortDepttNTypeScreen.subMenuDetailsList != null) {
                            SortDepttNTypeScreen.subMenuDetailsList.clear();
                            SubMenuStack.clearSubMenuStack();
                            SortDepttNTypeScreen.subMenuDetailsList = new
                                    ArrayList<SubMenuDetails>();
                        }
                        LoginScreenViewAsyncTask.bIsLoginFlag = true;
                        callingMainMenu();
                    }


                }
            });

            backImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

		drawerIcon = (ImageView)findViewById(R.id.drawer_icon);
        if (Constants.getNavigationBarValues("hamburgerImg") != null) {
            Picasso.with(this).load(Constants.getNavigationBarValues("hamburgerImg").replace(" ",
                    "%20")).placeholder(R.drawable.loading_button).into
                    (drawerIcon);
        }
		drawerIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					if (drawer.isDrawerOpen(Gravity.LEFT)) {
						drawer.closeDrawer(Gravity.LEFT);
					} else {
						drawer.openDrawer(Gravity.LEFT);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
    //below Class called only in modules hamburger
    // Added by Sharanamma
    // Side menu API call : includes hubciti and news functionalities
    List<NavigationItem> sideMenuDataList = new LinkedList<>();
    String userStatusText;


    public void callSideMenuApi(final CustomNavigation customNavigation) {
        NewsSideMenu sideMenuRequest = UrlRequestParams.getSideMenu("0", "1","true");
        RestClient.getInstance().getSideNavMenu(sideMenuRequest, new retrofit.Callback<SideMenuObject>() {

            @Override
            public void success(SideMenuObject sideMenuObject, Response response) {
                String responseText = sideMenuObject.getResponseText();
                int hubCitiPos = 0;
                if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                    userStatusText = "Logout";
                } else {
                    userStatusText = "Login/SignUp";
                }

                //Adding login/logout button in list
               /* NavigationItem navItem = new NavigationItem();
                navItem.setCatName(userStatusText);
                navItem.setLinkTypeName(userStatusText);
                navItem.setFlag(0);*/

                if (responseText.equalsIgnoreCase("Success")) {
                    if (sideMenuObject.getListCatDetails().size() != 0) {
                        if (sideMenuObject.getListCatDetails().get(0)
                                .getListMainCat() != null) {
                            hubCitiPos = 1;
                        }
                        sideMenuDataList = CommonMethods.arrangeInSortedOrder(sideMenuObject, hubCitiPos);
                       /* NavigationItem navItem1 = new NavigationItem();
                        navItem1.setCatName("News Settings");
                        navItem1.setFlag(2);
                        sideMenuDataList.add(navItem1);*/
                    }
                }
                else
                {
                    NavigationItem navItem = new NavigationItem();
                    navItem.setCatName(userStatusText);
                    navItem.setLinkTypeName(userStatusText);
                    navItem.setFlag(1);
                    sideMenuDataList.add(navItem);
                }
//                sideMenuDataList.add(navItem);
                customNavigation.populateSideMenu(sideMenuObject, sideMenuDataList, drawer, hubCitiPos);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    System.out.println("error :" + error.getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    public void callingMainMenu() {
        if (EventsListDisplay.eventIDs != null) {
            for (int i = 0; i < EventsListDisplay.eventIDs.size(); i++) {
                EventsListDisplay.eventIDs.remove(i);
            }
        }

        String level = "1";
        String mItemId = "0";
        String mLinkId = "0";
        Constants.SHOWGPSALERT = false;
        // @Ramachandran
        SubMenuStack.clearSubMenuStack();
        MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null, null, 0,
                null, mLinkId, null, null, null, null, level, null, null, null,
                null, null, null, null);
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
        MenuAsyncTask menu = new MenuAsyncTask(false, this, "mainMenu");

        menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, level, mItemId,
                "0", "None", "0", "0");
    }

    public void startMenuActivity() {

        if (EventsListDisplay.eventIDs != null) {
            for (int i = 0; i < EventsListDisplay.eventIDs.size(); i++) {
                EventsListDisplay.eventIDs.remove(i);
            }
        }

        String level = "1";
        String mItemId = "0";
        Constants.SHOWGPSALERT = false;

        MenuAsyncTask menu = new MenuAsyncTask(false, this, "mainMenu");

        menu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, level, mItemId,
                "0", "None", "0", "0");

    }



}
