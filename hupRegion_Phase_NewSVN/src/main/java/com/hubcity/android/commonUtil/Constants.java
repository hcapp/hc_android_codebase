package com.hubcity.android.commonUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.provider.Settings;

import com.hubcity.android.screens.PushNotificationListScreen;
import com.hubcity.android.screens.RegistrationActivity;
import com.hubcity.android.screens.SpecialOffersScreen;
import com.scansee.android.CouponsDetailActivity;
import com.scansee.android.HotDealsDetailsActivity;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

public class Constants
{
	public static final int SPLASH_TIME_OUT = 3000;
	public static String GuestLoginId = "1615";
	public static String GuestLoginIdforSigup = "1615";
	public static String GuestLoginZipcode = "";

	public static boolean bISLoginStatus = false;
	public static boolean showLoginMsg = false;

	public static final int STARTVALUE = 2001;
	public static final int FINISHVALUE = 2000;
	public static final String PUSH_NOTIFY_MSG = "pushmsgobj";

	public static final String APP_ICON_INTENT_EXTRA = "appIcon";
	public static final String APP_STORE_LINK_INTENT_EXTRA = "appStoreLink";

	public static final String DIALOG_MESSAGE = "Refreshing Data..";

	// public static boolean NEED_REMEMBER_LOGIN;
	public static boolean DONT_ALLOW_LOGIN;
	public static boolean SHOWGPSALERT = true;
	public static final int RANGE = 30;

	public static final String MENU_LEVEL_INTENT_EXTRA = "level";

	// Gov QA Auth key and preference key
	public static final String GOV_QA_AUTH_KEY = "ifF34jauK;";
	public static String GOV_QA_SESSION_ID = "gov_s_id";
	public static boolean GOV_QA_DONT_ALLOW_LOGIN;

	public static final String GOV_QA_REMEMBER = "gov_remember";
	public static final String GOV_QA_REMEMBER_NAME = "gov_email_id";
	public static final String GOV_QA_REMEMBER_PASSWORD = "gov_password";

	public static String GOV_QA_PASSWORD = "gov_pwd";

	public static SecretKeySpec govQaKey;

	public static String FIND_SINGLE_CAT_LINK_ID;
	public static String FIND_CAT_LINK_ID;

	public static final String MENU_PARCELABLE_INTENT_EXTRA = "Menu_Parcelable_intent_extra";

	public static final String CAT_IDS_INTENT_EXTRA = "catIds";
	public static final String CITI_EXPID_INTENT_EXTRA = "citiExpId";
	public static final String FUNDRAISER_ID_INTENT_EXTRA = "fundraiserId";
	public static final String SEARCH_KEY_INTENT_EXTRA = "searchKey";
	public static final String MENU_ITEM_ID_INTENT_EXTRA = "mItemId";
	public static final String MENU_ITEM_NAME_INTENT_EXTRA = "mItemName";
	public static String HUB_CITY_ID;

	public static final String TAG_MAIN_PREFERENCES_MENU = "MainCategory";

	public static final String UN_SUCCESSFULL = "un_sucessfull";
	public static final String SUCCESSFULL_REGISTRATION_RESPONSE = "UserDetails";
	public static final String ERROR_REGISTRATION_RESPONSE = "response";
	public static final String LOGIN_FLOW_DETAILS = "LoginFlowDetails";
	public static final String PREFERENCE_KEY_U_ID = "u_id";
	public static final String PREFERENCE_KEY_TEMP_U_ID = "temp_u_id";
	public static final String PREFERENCE_KEY_MAIN_MENU_ID = "main_menu_id";
	public static final String PREFERENCE_IS_LOGIN = "IS_LOGGED_IN";

	public static final String PREFERENCE_HUB_CITY = "preference";
	public static final String PREFERENCE_HUB_CITY_ID = "hubCitiId";
	public static final String TAG_LASTVISITEDRECORD = "lastVisitedRecord";
	public static final String PAGE_TYPE_LOGIN = "Login Page";

	public static final String REMEMBER = "remember";
	public static final String REMEMBER_NAME = "username";
	public static final String REMEMBER_PASSWORD = "password";
	private static final String ZIP_CODE = "zip_code";

	public static final String USER_ID = "userId";
	public static final String PLATFORM = "Android";
	public static final String HUBCITI_ID = "hubCitiId";

	public static final String TAG_RETAILE_LOCATIONID = "retailLocationID";
	public static final String TAG_RETAIL_ID = "retailerId";
	public static final String TAG_SCAN_TYPE_ID = "scanTypeId";
	public static final String TAG_RET_LIST_ID = "retListId";
	public static final String MAIN_MENU_ID = "mainMenuId";

	public static final String TAG_PAGE_SPECIALOFFERFLAGRESULTSET = "SpecialOffersList";
	public static final String TAG_PAGE_SPECIALOFFERFLAGMENU = "SpecialOffer";
	public static final String TAG_HOTDEALFLAG = "hotDealFlag";

	public static final String TAG_RETAILER_NAME = "retailerName";
	public static final String TAG_APPSITE_NAME = "appSiteName";

	public static final String TAG_RIBBON_ADIMAGE_PATH = "ribbonAdImagePath";

	public static final String TAG_RIBBON_AD_URL = "retListId";
	public static final String TAG_RETAILE_ID = "retailID";
	private static SharedPreferences settings;

	public static final String BOTTOM_ITEM_ID_INTENT_EXTRA = "bottomBtnId";
	public static final String BOTTOM_LINK_ID_INTENT_EXTRA = "bottomBtnLinkId";

	public static final String APP_INFO_PREFERENCE = "appInfoPreference";
	public static final String APP_VERSION = "app version";
	public static final String CITY_PREFERENCE = "city preference";


	public static final String LIST_POSITION = "listPosition";
	public static final String EMAIL = "email";
	public static final String FILTER_NAME = "filterName";
	public static boolean IS_PREFERENCE_CHANGED = false;
	public static boolean IS_SUB_MENU_REFRESHED = false;

	public static final String COMBINATION = "Combination News Template";
	public static final String SCROLLING = "Scrolling News Template";
	public static final String NEWS_TILE = "News Tile Template";

	public static final String LEVEL = "level";
	public static final String ITEMID = "itemID";
	public static final String LINKID = "linkID";
	public static final String IS_SUB_MENU = "isSubMenu";
	public static final String IS_TEMP_USER = "isTempUser";
	public static final String NO_EMAIL_COUNT = "noEmailCount";
	public static final String Weblink = "WebLink";
	public static final int TYPE_WIFI = 1;
	public static final int TYPE_MOBILE = 2;
	public static final int TYPE_NOT_CONNECTED = 0;

	public static final String USERNAME = "WelcomeScanSeeGuest";
	public static final String PASSWORD = ":::We@Love!!ScanSee?{People}";
	public static void setZipCode(String zip)
	{
			settings = HubCityContext.getHubCityContext().getSharedPreferences(
					Constants.PREFERENCE_HUB_CITY, 0);
		if (settings != null) {
			Editor prefEditor = settings.edit();
			prefEditor.putString(Constants.ZIP_CODE, zip).apply();
		}

	}

	public static String getZipCode()
	{
			settings = HubCityContext.getHubCityContext().getSharedPreferences(
					Constants.PREFERENCE_HUB_CITY, 0);
		if (settings != null) {
			return settings.getString(Constants.ZIP_CODE, "");
		} else {
			return "";
		}
	}

	@SuppressWarnings("static-access")
	public static String getMainMenuId()
	{
		SharedPreferences preferences = HubCityContext.getHubCityContext()
				.getSharedPreferences(Constants.PREFERENCE_HUB_CITY,
						HubCityContext.getHubCityContext().MODE_PRIVATE);

		return preferences.getString(
				Constants.PREFERENCE_KEY_MAIN_MENU_ID, null);
	}

	@SuppressWarnings("static-access")
	public static boolean saveMainMenuId(String value)
	{
		Context context = HubCityContext.getHubCityContext();
		SharedPreferences preferences = context.getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, context.MODE_PRIVATE);
		SharedPreferences.Editor e = preferences.edit().putString(
				Constants.PREFERENCE_KEY_MAIN_MENU_ID, value);

		return e.commit();
	}

	/**
	 * Retaining the push messages if it is not a daily push
	 *
	 * @param array
	 * @param arrayName
	 * @return
	 */
	public static boolean savePushArray(ArrayList<String> array, String arrayName)
	{
		Context context = HubCityContext.getHubCityContext();
		SharedPreferences prefs = context.getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(arrayName + "_size", array.size());
		for (int i = 0; i < array.size(); i++) {
			editor.putString(arrayName + "_" + i, array.get(i));
		}
		return editor.commit();
	}

	/**
	 * Getting all the retained push messages
	 *
	 * @param arrayName
	 * @return ArrayList
	 */
	public static ArrayList<String> getPushArray(String arrayName)
	{
		Context context = HubCityContext.getHubCityContext();
		SharedPreferences prefs = context.getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
		int size = prefs.getInt(arrayName + "_size", 0);
		ArrayList<String> array = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			array.add(prefs.getString(arrayName + "_" + i, null));
		}
		return array;
	}

	/**
	 * Removing the messages if daily push comes
	 *
	 * @param arrayName
	 * @return void
	 */
	public static void removePushArray(String arrayName)
	{
		Context context = HubCityContext.getHubCityContext();
		SharedPreferences prefs = context.getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		int size = prefs.getInt(arrayName + "_size", 0);
		for (int i = 0; i < size; i++) {
			editor.remove(arrayName + "_" + i);
		}
		editor.apply();
	}

	/**
	 * Removing a particular messages
	 *
	 * @param arrayName
	 * @return void
	 */
	public static void removePushArrayByPosition(String arrayName, int position)
	{
		Context context = HubCityContext.getHubCityContext();
		SharedPreferences prefs = context.getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.remove(arrayName + "_" + position);
		editor.apply();
	}

	public static String getAppVersion() throws NameNotFoundException
	{
		Context mContext = HubCityContext.getHubCityContext();
		PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(
				mContext.getPackageName(), 0);
		return pInfo.versionName;
	}

	public static String getDeviceId()
	{
		return Settings.Secure.getString(HubCityContext.getHubCityContext()
				.getContentResolver(), Settings.Secure.ANDROID_ID);
	}

	public static void SignUpAlert(final Activity activity)
	{
		new AlertDialog.Builder(activity)
				.setTitle("Alert")
				.setMessage(R.string.guestUserAlert)
				.setPositiveButton("Sign Up",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								Intent intent = new Intent(activity,
										RegistrationActivity.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
										| Intent.FLAG_ACTIVITY_NEW_TASK);
								activity.startActivity(intent);
							}
						}).setNegativeButton("Continue", null).show();

	}

	public static void SignUpAlert(final Activity activity, final String className, final String
			level)
	{
		new AlertDialog.Builder(activity)
				.setTitle("Alert")
				.setMessage(R.string.guestUserAlert)
				.setPositiveButton("Sign Up",
						new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
//								GlobalConstants.isFromNewsTemplate = level.equals("1");
								Intent intent = new Intent(activity,
										RegistrationActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								activity.startActivity(intent);
//								GlobalConstants.className = className;
							}
						}).setNegativeButton("Continue", null).show();

	}

	/**
	 * This Method will help you to launch the particular app if the provided package is already
	 * installed else it will open the market link
	 *
	 * @param packageName
	 * @param context
	 */
	public static void openCustomUrl(String packageName, Context context)
	{
		boolean installed = appInstalledOrNot(packageName, context);
		if (installed) {
//			Intent LaunchIntent = context.getPackageManager()
//					.getLaunchIntentForPackage(packageName);
			Intent intent = new Intent(Intent.ACTION_MAIN);
////			intent.setComponent(new ComponentName(packageName, packageName + "
// .SplashActivity"));
			intent.setComponent(new ComponentName(packageName, "com.hubcity.android.screens" +
					".SplashActivity"));
			intent.setPackage(packageName);
////
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			//startActivity(intent);
			intent.addCategory(Intent.CATEGORY_LAUNCHER);
			context.startActivity(intent);
		} else {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=" + packageName));
			context.startActivity(intent);
		}
	}


	/**
	 * This is to check whether the provided app uri is installed in the particular device or not
	 *
	 * @param uri
	 * @param context
	 * @return
	 */
	private static boolean appInstalledOrNot(String uri, Context context)
	{
		PackageManager pm = context.getPackageManager();
		boolean app_installed;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	// Sorts the City Ids
	public static String getSortedCityIds(String cities_ids)
	{

		String[] cities_ids_array = cities_ids.split(",");

		Arrays.sort(cities_ids_array, new Comparator<String>()
		{
			@Override
			public int compare(String o1, String o2)
			{
				return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
			}
		});

		String sorted_cities_ids = Arrays.toString(cities_ids_array);
		//sorted_cities_ids = sorted_cities_ids.replace("[","").replace("]","");
		sorted_cities_ids = sorted_cities_ids.replace("[", "");
		sorted_cities_ids = sorted_cities_ids.replace("]", "");
		return sorted_cities_ids;
	}

	public static boolean getDealExpirationState(String timeToCompare)
	{
		boolean isExpired = false;
		try {
			Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(timeToCompare);
			if (Calendar.getInstance().getTime().after(date)) {
				isExpired = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isExpired;
	}

	/**
	 * getting notification intent according to deal type
	 *
	 * @param jsonObject
	 * @return Intent
	 */
	public static Intent getNotificationDealIntent(JSONObject jsonObject, Context context)
	{
		Intent intent = null;
		try {
			if (jsonObject.has("dealList")) {
				JSONArray dealsArray = jsonObject.getJSONArray("dealList");
				String dealType = dealsArray.getJSONObject(0).optString("type");
				String dealSplUrl = dealsArray.getJSONObject(0).optString("splUrl");
				String dealId = dealsArray.getJSONObject(0).optString("dealId");
				String dealName = dealsArray.getJSONObject(0).optString("dealName");
				String dealRetLocId = dealsArray.getJSONObject(0).optString
						("retailLocationId");
				String dealRetId = dealsArray.getJSONObject(0).optString("retailerId");
				if (dealType.equals("Coupons")) {
					intent = new Intent(context,
							CouponsDetailActivity.class);
					intent.putExtra("couponId", dealId);
					intent.putExtra("couponName", dealName);
					intent.putExtra("push", "pushnotify");
				} else if (dealType.equals("Hotdeals")) {
					intent = new Intent(context,
							HotDealsDetailsActivity.class);
					intent.putExtra("hotDealId", dealId);
					intent.putExtra("hotDealName", dealName);
					intent.putExtra("push", "pushnotify");
				} else if (dealType.equals("SpecialOffers") && dealSplUrl != null) {
					if (dealSplUrl.equals("")) {
						intent = new Intent(context,
								SpecialOffersScreen.class);
						intent.putExtra("hotDealId", dealId);
						if (dealRetId != null && !"".equals(dealRetId)) {
							intent.putExtra("retailerId", dealRetId);
						}

						if (dealRetLocId != null && !"".equals(dealRetLocId)) {
							intent.putExtra("retailLocationId", dealRetLocId);
						}
						intent.putExtra("hotDealName", dealName);
						intent.putExtra("push", "pushnotify");
					} else {
						intent = new Intent(context, ScanseeBrowserActivity.class);
						intent.putExtra(CommonConstants.URL, dealSplUrl);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return intent;
	}

	//Added by Supriya

	/**
	 * This is a common method to store navigation
	 * bar values(background color, title text color, back image and home image)
	 *
	 * @param key   to store different values
	 * @param value is the navigation bar values
	 */
	public static void setNavigationBarValues(String key, String value)
	{
		SharedPreferences.Editor e = HubCityContext.getHubCityContext().getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, HubCityContext.MODE_PRIVATE).edit();
		e.putString(key, value);
		e.apply();
	}

	//Added by Supriya

	/**
	 * This is a common method to get navigation
	 * bar values(background color, title text color, back image and home image)
	 * so that it can be accessed from any screen
	 *
	 * @param key to fetch values accordingly
	 */
	public static String getNavigationBarValues(String key)
	{
		SharedPreferences preferences = HubCityContext.getHubCityContext().getSharedPreferences(
				Constants.PREFERENCE_HUB_CITY, HubCityContext.MODE_PRIVATE);
		return preferences.getString(key, null);
	}

	public static void startPushList(String strpush, Context context) {
		Intent intent = null;
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(strpush);

			if (Properties.HUBCITI_KEY.equals("Tyler19")||Properties.HUBCITI_KEY.equals("Tyler Test75")||Properties.HUBCITI_KEY.equals("shrini2113") || Properties.HUBCITI_KEY.equals("HEB Test2156") || Properties.HUBCITI_KEY.equals("HEB Test54")) {
				if (HubCityContext.pushMsgArray.size() > 1) {
					intent = new Intent(context, PushNotificationListScreen.class);
				} else {
					if (jsonObject.has("rssFeedList") && jsonObject.has("dealList")) {
						intent = new Intent(context, PushNotificationListScreen.class);
					} else if (jsonObject.has("rssFeedList")) {
						String newsLink = jsonObject.getJSONArray("rssFeedList")
								.getJSONObject(0)

								.optString("link");
						intent = new Intent(
								context,
								ScanseeBrowserActivity.class);
						intent.putExtra(CommonConstants.URL, newsLink);
					} else {
						intent = Constants.getNotificationDealIntent(jsonObject, context);
					}
				}
			} else {
				//	If array size more than 1 it will navigate to list screen else it will
				//	navigate to respective details screen
				//	If the size is 0 it will go to list screen and show a
				//  pop up to user that deal has expired
				if (HubCityContext.pushMsgArray.size() > 1) {
					intent = new Intent(context, PushNotificationListScreen.class);
				} else if (HubCityContext.pushMsgArray.size() > 0) {
					intent = Constants.getNotificationDealIntent(jsonObject, context);
				} else {
					intent = new Intent(context, PushNotificationListScreen.class);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(Constants.PUSH_NOTIFY_MSG, strpush);
		context.startActivity(intent);
		((Activity) context).finish();

	}

	/**
	 * Checking the user credential is saved in preference or not
	 * to get the confirmation of user's login status
	 *
	 * @param context
	 * @return true if the credential is saved else false
	 */
	public static boolean isCredentialsSaved(Context context) {
		boolean isCredentailSaved = false;
		SharedPreferences settings = context.getSharedPreferences(Constants.PREFERENCE_HUB_CITY,
				0);
		boolean mStateChecked = settings.getBoolean(Constants.REMEMBER, false);
		String usrName = settings.getString(Constants.REMEMBER_NAME, null);
		String passwd = settings.getString(Constants.REMEMBER_PASSWORD, null);

		if (mStateChecked && usrName != null && passwd != null) {
			isCredentailSaved = true;
		}

		return isCredentailSaved;

	}

	/**
	 * @param message is the notification message object
	 * @return saved message array
	 */
	public static ArrayList<String> saveNotificationMessage(String message) {
		ArrayList<String> arrayList = new ArrayList<>();
		try {
			JSONObject jsonObject = new JSONObject(message);
			System.out.println("message : " + message);
//			cheching if tyler instance if not then retaining all deals that has not expired else
//			retaining push if it is not daily deal
			if (Properties.HUBCITI_KEY.equals("Tyler19") ||Properties.HUBCITI_KEY.equals("Tyler Test75")||Properties.HUBCITI_KEY.equals("shrini2113")|| Properties.HUBCITI_KEY.equals("HEB Test2156") || Properties.HUBCITI_KEY.equals("HEB Test54")) {

//            Checking if daily deal then clearing all the push
//            else retaining the deal
				if (jsonObject.getString("notiMgs").contains(".") || jsonObject.getString
						("notiMgs")

						.toLowerCase().contains("top")) {
					Constants.removePushArray("gcm_msg");
					arrayList = new ArrayList<>();
					arrayList.add(message);
					Constants.savePushArray(arrayList, "gcm_msg");
				} else {
					//if user change deal details then clearing old deal and adding new deal based on ID if it is same
					arrayList = Constants.getPushArray("gcm_msg");
					for (int i = 0; i < arrayList.size(); i++) {
						try {
							String dealId = new JSONObject(arrayList.get(i)).getJSONArray
									("dealList")
									.getJSONObject(0).optString("dealId");
							if (dealId.equals(jsonObject.getJSONArray
									("dealList")
									.getJSONObject(0).optString("dealId"))) {
								arrayList.remove(i);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					for (int i = 0; i < arrayList.size(); i++) {
						try {
							String title = new JSONObject(arrayList.get(i)).getJSONArray
									("rssFeedList")
									.getJSONObject(0).optString("title");
							if (title.equals(jsonObject.getJSONArray
									("rssFeedList")
									.getJSONObject(0).optString("title"))) {
								arrayList.remove(i);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					arrayList.add(message);
					Constants.savePushArray(arrayList, "gcm_msg");
				}
			} else {
				//for other instance have only deals as notification
//				getting the previous pushed deals if any
				arrayList = Constants.getPushArray("gcm_msg");
//				Checking if the deal has expired or not
				if (!Constants.getDealExpirationState(jsonObject.getJSONArray
						("dealList")
						.getJSONObject(0).optString("endDate") + " " + jsonObject
						.getJSONArray("dealList")
						.getJSONObject(0).optString("endTime"))) {
//					if same deal has came then clearing the previously stored one and retaining
// new one
					for (int i = 0; i < arrayList.size(); i++) {
						String dealId = new JSONObject(arrayList.get(i)).getJSONArray
								("dealList")
								.getJSONObject(0).optString("dealId");
						if (dealId.equals(jsonObject.getJSONArray
								("dealList")
								.getJSONObject(0).optString("dealId"))) {
							arrayList.remove(i);
						}
					}
					arrayList.add(message);
					Constants.savePushArray(arrayList, "gcm_msg");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		HubCityContext.pushMsgArray = arrayList;
		return arrayList;
	}



}
