package com.hubcity.android.commonUtil;

/**
 * Created by subramanya.v on 10/4/2016.
 */
public class CustomerInfoList {
    private String name;
    private String retAddress;
    private String city;
    private String state;
    private String postalCode;
    private String type;
    private String website;
    private String country;
    private String phone;
    private String retAddress2;
    private String mailAddress;
    private String mailAddress2;
    private String mailCity;
    private String mailState;
    private String mailPhone;


    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getMailAddress2() {
        return mailAddress2;
    }

    public void setMailAddress2(String mailAddress2) {
        this.mailAddress2 = mailAddress2;
    }

    public String getMailCity() {
        return mailCity;
    }

    public void setMailCity(String mailCity) {
        this.mailCity = mailCity;
    }

    public String getMailState() {
        return mailState;
    }

    public void setMailState(String mailState) {
        this.mailState = mailState;
    }

    public String getMailPhone() {
        return mailPhone;
    }

    public void setMailPhone(String mailPhone) {
        this.mailPhone = mailPhone;
    }

    public String getMailCountry() {
        return mailCountry;
    }

    public void setMailCountry(String mailCountry) {
        this.mailCountry = mailCountry;
    }

    public String getMailPostalCode() {
        return mailPostalCode;
    }

    public void setMailPostalCode(String mailPostalCode) {
        this.mailPostalCode = mailPostalCode;
    }

    public String getIsMailAddress() {
        return isMailAddress;
    }

    public void setIsMailAddress(String isMailAddress) {
        this.isMailAddress = isMailAddress;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    private String mailCountry;
    private String mailPostalCode;
    private String isMailAddress;
    private String keywords;


    public String getClaimImg() {
        return claimImg;
    }

    public void setClaimImg(String claimImg) {
        this.claimImg = claimImg;
    }

    public String getClaimTxt() {
        return claimTxt;
    }

    public void setClaimTxt(String claimTxt) {
        this.claimTxt = claimTxt;
    }

    private String claimImg;
    private String claimTxt;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRetAddress2() {
        return retAddress2;
    }

    public void setRetAddress2(String retAddress2) {
        this.retAddress2 = retAddress2;
    }




    public String getRetAddress() {
        return retAddress;
    }

    public void setRetAddress(String retAddress) {
        this.retAddress = retAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }



}
