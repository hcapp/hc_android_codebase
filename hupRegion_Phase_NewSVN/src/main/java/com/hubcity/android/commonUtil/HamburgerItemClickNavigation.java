package com.hubcity.android.commonUtil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.govqa.GovQaLogin;
import com.hubcity.android.screens.AboutUsActivity;
import com.hubcity.android.screens.AlertListDisplay;
import com.hubcity.android.screens.AnythingPageDisplayActivity;
import com.hubcity.android.screens.AppsiteActivity;
import com.hubcity.android.screens.CitiExperienceActivity;
import com.hubcity.android.screens.CityExperienceFilterActivity;
import com.hubcity.android.screens.CityPreferencesScreen;
import com.hubcity.android.screens.EmailAsyncTask;
import com.hubcity.android.screens.EventMapScreen;
import com.hubcity.android.screens.EventsListDisplay;
import com.hubcity.android.screens.FaceBookActivity;
import com.hubcity.android.screens.FaqScreen;
import com.hubcity.android.screens.FindSingleCategory;
import com.hubcity.android.screens.FundraiserActivity;
import com.hubcity.android.screens.LoginAsyncTask;
import com.hubcity.android.screens.PreferedCatagoriesScreen;
import com.hubcity.android.screens.PrivacyPolicyActivity;
import com.hubcity.android.screens.RegistrationActivity;
import com.hubcity.android.screens.SetPreferrenceScreen;
import com.hubcity.android.screens.SettingsLocationServices;
import com.hubcity.android.screens.SettingsPage;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.hubcity.android.screens.SubMenuDetails;
import com.hubcity.twitter.PrepareRequestTokenActivity;
import com.hubcity.twitter.TwitterUtils;
import com.scansee.android.BandsLandingPageActivity;
import com.scansee.android.CouponsActivty;
import com.scansee.android.FindActivity;
import com.scansee.android.MyCouponActivity;
import com.scansee.android.ScanNowSplashActivity;
import com.scansee.android.ThisLocationGetLocation;
import com.scansee.android.ThisLocationGetRetailers;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.BackgroundTask.GuestUserLoginAsyncTask;
import com.scansee.newsfirst.BookMarkActivity;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ListCatDetails;
import com.scansee.newsfirst.MenuList;
import com.scansee.newsfirst.NavigationItem;
import com.scansee.newsfirst.NewsTemplateDataHolder;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.SideMenuObject;
import com.scansee.newsfirst.SubpageActivty;
import com.scansee.newsfirst.TabFragmentView;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by supriya.m on 10/26/2016.
 */
public class HamburgerItemClickNavigation {

    Context mContext;
    String cityExpId, mItemName;
    private SharedPreferences prefs;

    private ArrayList<MenuList> menuList = new ArrayList();
    MenuList menuItem;

    public HamburgerItemClickNavigation(Context context, SideMenuObject sideMenuObject, int hubCitiPos) {
        this.mContext = context;
        if (sideMenuObject.getListCatDetails() != null) {
            ArrayList<ListCatDetails> listDetails = sideMenuObject.getListCatDetails();
            if (listDetails.size() > 1) {
                menuList = listDetails.get(hubCitiPos).getMenuList();
                if (menuList != null) {

                    menuItem = menuList.get(0);
                }
            }
        }
    }

    public void linkClickListener(NavigationItem listItem) {

        if (listItem.getFlag() == 1) {
            String linkTypeName = listItem.getLinkTypeName();
            if ("GovQA".equalsIgnoreCase(linkTypeName)) {
                startGovQaScreen();
            } else if ("My Accounts".equalsIgnoreCase(linkTypeName) || "My Deals".equalsIgnoreCase(linkTypeName)) {
                startMyAccount();
            } else if ("City Experience".equals(linkTypeName)) {
                startCitiExpeienceScreen(listItem.getmItemId(),
                        listItem.getLinkId(), "0", null, false);
            } else if ("Preferences".equals(linkTypeName)
                    || "Preference".equals(linkTypeName)) {
                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {
                    startPreferenceScreen();
                } else {
                    Constants.SignUpAlert((Activity) mContext);
                }
            } else if ("FAQ".equals(linkTypeName)) {
                startFaqScreen(listItem.getmItemId());
            } else if ("Settings".equals(linkTypeName)) {
                Log.v("", "USER NAME : " + UrlRequestParams.getUid() + " , "
                        + Constants.GuestLoginId);
                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {
                    CityPreferencesScreen.bIsSendingTimeStamp = false;
                    startSettingsPageScreen(listItem.getmItemId(),
                            listItem.getLinkId());
                } else {
                    Constants.SignUpAlert((Activity) mContext);
                }
            } else if ("About".equals(linkTypeName)) {
                startAboutScreen(listItem.getmItemId());
            } else if ("Share".equals(linkTypeName)) {
                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {
                    initiatePopupWindow();
                } else {
                    Constants.SignUpAlert((Activity) mContext);
                }

            } else if ("Scan Now".equals(linkTypeName)) {
                startScannowScreen(listItem.getmItemId());
            } else if ("Deals".equals(linkTypeName)) {
                startHotdealsScreen(listItem.getmItemId());
            } else if ("Alerts".equals(linkTypeName)) {
                startAlertScreen(listItem.getmItemId());
            } else if ("AnythingPage".equals(linkTypeName)) {
                startAnythingPage(listItem.getmItemId(), listItem.getLinkId());
            } else if ("AppSite".equals(linkTypeName)) {
                starAppsitePage(listItem.getmItemId(), listItem.getLinkId());
            } else if ("Whats NearBy".equals(linkTypeName)) {
                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {
                    startWhatsNearbyScreen(listItem.getmItemId(), listItem);

                } else {
                    startFindActivityForGuestLogin(listItem.getmItemId());
                }

            } else if ("Find".equals(linkTypeName)) {
                String backgrnd = "";
                if (CommonConstants.smBkgrndColor != null
                        && !CommonConstants.smBkgrndColor.equals("N/A")) {
                    backgrnd = CommonConstants.smBkgrndColor;
                }
                startFindScreen(listItem.getmItemId(), listItem.getLinkId(),
                        listItem.getmItemName(), backgrnd);

            } else if (linkTypeName.startsWith("FindSingleCategory-")) {
                String[] catNames = linkTypeName.split("-");
                startFindSingleCategoryScreen(catNames[1],
                        listItem.getmItemId(),
                        listItem.getLinkId());

            } else if ("Events".equals(linkTypeName)) {
                startEventsScreen(listItem.getmItemId(), listItem.getLinkId());
            } else if ("Band Events".equals(linkTypeName)) {
                startBandEventsScreen(listItem.getmItemId(), listItem.getLinkId());
            } else if ("playing today".equalsIgnoreCase(linkTypeName)) {
                startEventMapScreen(listItem.getmItemId());
            } else if ("Privacy Policy".equals(linkTypeName)) {
                startPrivacyPlicyScreen(listItem.getmItemId(), listItem.getLinkId());
            } else if (linkTypeName.startsWith("EventSingleCategory-")) {
                startEventsScreen(listItem.getmItemId(), listItem.getLinkId());
            } else if ("SubMenu".equals(linkTypeName)) {
                //startSubMenu(mMainMenuBO);
                //Added by Sharanamma
                if (listItem.getmItemName().equalsIgnoreCase(Constants.SCROLLING)) {
                    startNewsTemplateSubMenu(ScrollingPageActivity.class, listItem.getmItemId(),
                            listItem.getLinkId());
                } else if (listItem.getmItemName().equalsIgnoreCase(Constants.COMBINATION)) {
                    startNewsTemplateSubMenu(CombinationTemplate.class, listItem.getmItemId(),
                            listItem.getLinkId());
                } else if (listItem.getmItemName().equalsIgnoreCase(Constants.NEWS_TILE)) {
                    startNewsTemplateSubMenu(TwoTileNewsTemplateActivity.class, listItem
                                    .getmItemId(),
                            listItem.getLinkId());
                } else {
                    startSubMenu(listItem);
                }

            } else if ("Filters".equals(linkTypeName)) {
                if (null != listItem.getLinkId()) {
                    cityExpId = listItem.getLinkId();
                }
                startFilterScreen(listItem.getmItemId(),
                        listItem);
            } // for fundraisers

            else if ("Fundraisers".equals(linkTypeName)) {
                startFundraiserScreen(listItem.getmItemId(),
                        listItem.getLinkId(), "0", null);
            } // Settings

            else if ("User Information".equals(linkTypeName)) {
                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {
                    startUserInformation();
                } else {
                    Constants.SignUpAlert((Activity) mContext);
                }

            } else if ("Location Preferences".equals(linkTypeName)) {
                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {
                    startLocationPreference();
                } else {
                    Constants.SignUpAlert((Activity) mContext);
                }

            } else if ("Category Favorites".equals(linkTypeName)) {
                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {
                    startCategoryFavourites();
                } else {
                    Constants.SignUpAlert((Activity) mContext);
                }

            } else if ("City Favorites".equals(linkTypeName)) {

                if (!Constants.GuestLoginId
                        .equals(UrlRequestParams.getUid().trim())) {
                    startCityFavourites();
                } else {
                    Constants.SignUpAlert((Activity) mContext);
                }


            } else if ("Band".equals(linkTypeName)) {
                mItemName = listItem.getmItemName();
                startBandLandingScreen(listItem.getmItemId());
            } else if (listItem.getLinkTypeName().contains("Logout")) {
                if (GlobalConstants.isFromNewsTemplate) {
                    new GuestUserLoginAsyncTask((Activity) mContext, GlobalConstants.className).execute();
                } else {
                    logout();
                }

            } else if (listItem.getLinkTypeName().contains("Login/SignUp")) {
                signUp();
            } else if (listItem.getLinkTypeName().contains("News Settings")) {
                newsSideMenuItemClick(listItem);
            }

        }
        else if (listItem.getFlag() == 2) {
            newsSideMenuItemClick(listItem);

        }



    }

    private void newsSideMenuItemClick(NavigationItem navItem) {

        int tabPos = 0;
        boolean isPresent = false;
//        if (!CommonConstants.previousMenuLevel.equals("1")) {
//            GlobalConstants.isFromNewsTemplate = false;
//        } else {
//            GlobalConstants.isFromNewsTemplate = true;
//        }
        // if its news settings
        Activity activityRef = null;
        try {
            activityRef = (Activity) mContext;
        } catch (Exception e) {
            e.printStackTrace();
        }
        String activityName = activityRef.getLocalClassName().toString();
        if (navItem.getCatName().equalsIgnoreCase("News Settings")) {
            if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                Intent bookMark = new Intent(mContext, BookMarkActivity.class);
                if (activityName.contains("ScrollingPageActivity") || activityName.contains("CombinationTemplate") || activityName.contains("TwoTileNewsTemplateActivity")) {
                    bookMark.putExtra("ClassName", NewsTemplateDataHolder.className);
                }
                bookMark.putExtra("LinkId", CommonConstants.commonLinkId);
                bookMark.putExtra("Level", CommonConstants.previousMenuLevel);
                mContext.startActivity(bookMark);
            } else {
                Constants.SignUpAlert((Activity) mContext, NewsTemplateDataHolder.className, CommonConstants.previousMenuLevel);

            }
        } else if (activityRef != null && activityRef.getLocalClassName().toString().contains("ScrollingPageActivity")) {
            try {
                String sideMenuCat = navItem.getCatName();
                ScrollingPageActivity activity = (ScrollingPageActivity) mContext;
                if (activity.bookMarkData.size() != 0) {
                    for (ListCatDetails bookMarkCat : (activity)
                            .bookMarkData
                            .get(0)
                            .getBookMarkList()
                            .get(0)
                            .getListCatDetails()) {
                        tabPos++;
                        if (bookMarkCat.getParCatName().equalsIgnoreCase
                                (sideMenuCat)) {
                            isPresent = true;
                            break;
                        }
                    }
                }
                if (isPresent) {
                    activity.bannerImage.setVisibility(View
                            .VISIBLE);
                    activity.newsTitle.setVisibility(View
                            .GONE);
                    activity.template = "Scrolling News" +
                            " " +
                            "Template";
                    activity.getTabView().setVisibility(View.VISIBLE);
                    activity.HomeView.setVisibility(View
                            .GONE);
                    activity.tabLayout.setVisibility
                            (View.VISIBLE);
                    activity.contentSearch.setVisibility
                            (View.VISIBLE);
                    activity.getViewPager().setVisibility(View.VISIBLE);

                    if (Constants.getNavigationBarValues("titleBkGrdColor") != null) {
                        activity.customParent
                                .setBackgroundColor(Color.parseColor(Constants
                                        .getNavigationBarValues
                                                ("titleBkGrdColor")));
                    }
                    int selectedTab = tabPos - 1;
                    //when pull to request performing in scrolling page at same time
                    // if user click on side menu which has subcategory(Sports) then
                    // removing views when coming back to scrolling screeen
                    removeView(selectedTab, sideMenuCat);
                    activity.getViewPager().setCurrentItem(selectedTab);
                } else {
                    gotoSubPage(navItem);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            gotoSubPage(navItem);

        }
    }

    private void gotoSubPage(NavigationItem navItem) {
        String sideMenuCat = navItem.getCatName();
        Intent scroling = new Intent(mContext, SubpageActivty
                .class);
        scroling.putExtra("templateType", "Subpage");
        scroling.putExtra("sideMenuCat", sideMenuCat);
        scroling.putExtra("isSideBar", "1");
        scroling.putExtra("isSubCategory", navItem.getIsSubCategory());
        scroling.putExtra(Constants.LEVEL, CommonConstants.previousMenuLevel);
        scroling.putExtra(Constants.LINKID, CommonConstants.commonLinkId);
        scroling.putExtra("catTxtColor", navItem.getCatTxtColor());
        mContext.startActivity(scroling);
    }

    private void removeView(int selectedTab, String sideMenuCat) {
        ScrollingPageActivity activity = (ScrollingPageActivity) mContext;
        if (!(sideMenuCat.equalsIgnoreCase("photos") || sideMenuCat.equalsIgnoreCase("Videos"))) {
            TabFragmentView frag = (TabFragmentView) activity.getViewPager().getAdapter()
                    .instantiateItem(activity.getViewPager(), selectedTab);
            if (frag != null) {
                if (frag.swipeRefreshLayout != null) {
                    frag.swipeRefreshLayout.setRefreshing(false);
                }
            }
        }
    }

    private void startEventMapScreen(String mItemId) {

        Intent intent = new Intent(mContext, EventMapScreen.class);
        intent.putExtra("isPlayingToday", true);
        intent.putExtra("mItemId", mItemId);
        mContext.startActivity(intent);

    }

    private void startMyAccount() {
        //clearing entered zipcode of popup in coupon screen only from hamburger,menu screen and
        // bottom button of menu screen and not clearing from modules
        CouponsActivty.postalCode = null;
        Intent intent = new Intent(mContext, MyCouponActivity.class);
        mContext.startActivity(intent);
    }

    private void startGovQaScreen() {
        Intent intent = new Intent(mContext, GovQaLogin.class);
        intent.putExtra("BottomBtnJsonObj", MenuAsyncTask.bbJsonObj.toString());
        mContext.startActivity(intent);
    }

    private void startCitiExpeienceScreen(String itemId, String linkTypeId,
                                          String catIds, String searchKey, boolean isBottonBtn) {

        Intent intent = new Intent(mContext, CitiExperienceActivity.class);

        if (isBottonBtn) {
            intent.putExtra(Constants.BOTTOM_ITEM_ID_INTENT_EXTRA, itemId);
        } else {
            intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, itemId);
        }

        intent.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, /* citiExpId */
                linkTypeId);
        intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startPreferenceScreen() {

        Intent intent = new Intent(mContext, PreferedCatagoriesScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);
        mContext.startActivity(intent);

    }

    /**
     * Starts FAQ screen after checking if the call is from bottom button or
     * from the main menu
     */
    private void startFaqScreen(String mItemId) {
        Intent intent = new Intent(mContext, FaqScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        mContext.startActivity(intent);
    }

    private void startSettingsPageScreen(String mItemId, String mLinkId) {
        Intent intent = new Intent(mContext, SettingsPage.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("regacc", false);
        intent.putExtra("mItemId", mItemId);
        intent.putExtra("mLinkId", mLinkId);
        mContext.startActivity(intent);
    }

    private void startAboutScreen(String mItemId) {
        Intent intent = new Intent(mContext, AboutUsActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private PopupWindow pwindo;
    public static Button btnClosePopup;
    protected static Button btnRedeem;
    Button btnTwitterShare, btnSMSShare, btnEmailShare;

    Button btnFacebookShare;

    protected void initiatePopupWindow() {
        try {
            try {
                // We need to get the instance of the LayoutInflater
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.share_screen_popup,
                        (ViewGroup) ((Activity) mContext).findViewById(R.id.popup_element));
                DisplayMetrics metrics = new DisplayMetrics();
                ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int screenWidth = metrics.widthPixels;
                int screenHeight = metrics.heightPixels;
                pwindo = new PopupWindow(layout, screenWidth, screenHeight / 2,
                        true);

                pwindo.showAtLocation(layout, Gravity.BOTTOM, 0, 0);

                btnClosePopup = (Button) layout
                        .findViewById(R.id.btn_close_popup);

                btnFacebookShare = (Button) layout
                        .findViewById(R.id.btn_facebook_popup);
                btnTwitterShare = (Button) layout
                        .findViewById(R.id.btn_twitter_popup);
                btnSMSShare = (Button) layout.findViewById(R.id.btn_text_popup);
                btnEmailShare = (Button) layout
                        .findViewById(R.id.btn_email_popup);

                btnFacebookShare.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaFacebook();

                    }
                });
                btnTwitterShare.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        shareViaTwitter();
                    }

                });
                btnSMSShare.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaSMS();

                    }

                });
                btnEmailShare.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                        shareViaEMail();
                    }

                });

                btnClosePopup.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        pwindo.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void shareViaEMail() {

        EmailAsyncTask emailAsyncTask = new EmailAsyncTask((Activity) mContext, null);
        emailAsyncTask.execute();

    }

    private void shareViaFacebook() {

        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.fb_share_popup);
        dialog.setTitle("Share via FaceBook");

        final EditText eText = (EditText) dialog.findViewById(R.id.text);

        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        new ImageLoaderAsync(image).execute(menuItem.getAppIconImg());

        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog
                .findViewById(R.id.dialogButtonCancel);

        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent facebookIntent = new Intent(mContext,
                        FaceBookActivity.class);
                facebookIntent.putExtra("userText", eText.getText().toString());
                facebookIntent.putExtra("appPlayStoreLink", menuItem.getAndroidLink());
                dialog.dismiss();
                mContext.startActivity(facebookIntent);
            }
        });
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void shareViaTwitter() {
        this.prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.fb_share_popup);
        dialog.setTitle("Share via Twitter");

        EditText text = (EditText) dialog.findViewById(R.id.text);
        text.setEnabled(false);
        text.setSingleLine(false);
        text.setText(mContext.getResources().getString(R.string.tweet_msg) + "\n"
                + menuItem.getAndroidLink());

        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        new ImageLoaderAsync(image).execute(menuItem.getAppIconImg());

        Button dialogButtonOk = (Button) dialog
                .findViewById(R.id.dialogButtonOK);
        Button dialogButtonCancel = (Button) dialog
                .findViewById(R.id.dialogButtonCancel);

        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new TweetMsg().execute();
            }
        });
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public class TweetMsg extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (TwitterUtils.isAuthenticated(prefs)) {
                sendTweet();
            } else {
                Intent i = new Intent(mContext.getApplicationContext(),
                        PrepareRequestTokenActivity.class);
                i.putExtra("tweet_msg",
                        mContext.getResources().getString(R.string.tweet_msg) + "\n"
                                + menuItem.getAndroidLink());
                i.putExtra("image",
                        menuItem.getAppIconImg());
                ((Activity) mContext).startActivityForResult(i, 2505);
            }
            return null;
        }

    }

    public void sendTweet() {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    TwitterUtils.sendTweet(prefs,
                            mContext.getResources().getString(R.string.tweet_msg) + "\n"
                                    + menuItem.getAndroidLink(), menuItem.getAppIconImg());
                    new Handler().post(mUpdateTwitterNotification);
                } catch (Exception e) {
                    if (e != null && "" + e != null) {
                        if (e.getMessage().contains(
                                "Status is over 140 characters")) {
                            Toast.makeText(
                                    mContext.getApplicationContext(),
                                    "Tweet not sent - Status is over 140 characters!",
                                    Toast.LENGTH_LONG).show();
                        } else if (e.getMessage().contains("duplicate")) {
                            Toast.makeText(mContext.getApplicationContext(),
                                    "Tweet not sent - Status Duplicate!",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(mContext.getApplicationContext(),
                                    "Tweet not sent - Error!",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }

            }
        });
    }

    final Runnable mUpdateTwitterNotification = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(mContext, "Tweet sent !", Toast.LENGTH_LONG)
                    .show();
            btnClosePopup.performClick();

        }
    };

    private void shareViaSMS() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer
                .append("Please download the HubCiti App from the below link:");
        stringBuffer.append("\n");
        stringBuffer.append(menuItem.getAndroidLink());
        stringBuffer.append("\n");
        stringBuffer.append(menuItem.getDownLoadLink());
        stringBuffer.append("\n");

        if (stringBuffer.length() > 0) {
            Intent smsShare = new Intent(Intent.ACTION_VIEW);
            smsShare.setData(Uri.parse("sms:"));
            try {
                smsShare.putExtra("sms_body",
                        URLDecoder.decode(stringBuffer.toString(), "UTF-8"));
                mContext.startActivity(smsShare);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startScannowScreen(String mItemId) {
        Intent intent = new Intent(mContext, ScanNowSplashActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startHotdealsScreen(String mItemId) {

        Intent intent = new Intent(mContext, CouponsActivty.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        boolean isSubMenu;
        if (CommonConstants.previousMenuLevel != null && "1".equals(CommonConstants.previousMenuLevel)) {
            isSubMenu = false;
        } else {
            isSubMenu = true;
        }
        intent.putExtra("isSubMenu", isSubMenu);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startAlertScreen(String mItemId) {
        Intent intent = new Intent(mContext, AlertListDisplay.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startAnythingPage(String mItemId, String mLinkId) {
        Intent intent = new Intent(mContext, AnythingPageDisplayActivity.class);
        intent.putExtra(CommonConstants.LINK_ID, mLinkId);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra("isFromSideNav", true);
        intent.putExtra("isShare", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void starAppsitePage(String mItemId, String mLinkId) {

        Intent intent = new Intent(mContext, AppsiteActivity.class);
        intent.putExtra(CommonConstants.LINK_ID, mLinkId);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startWhatsNearbyScreen(String mItemId, NavigationItem listItem) {

        Intent intent = new Intent(mContext, ThisLocationGetLocation.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);

        if (CommonConstants.previousMenuLevel != null && "1".equals(CommonConstants.previousMenuLevel)) {
            intent.putExtra("mBkgrdColor", CommonConstants.mBkgrndColor);
            intent.putExtra("mBkgrdImage", CommonConstants.mBkgrndImage);
            intent.putExtra("mBtnColor", listItem.getmBtnColor());
            intent.putExtra("mBtnFontColor", listItem.getmBtnFontColor());

        } else {

            intent.putExtra("mBkgrdColor", CommonConstants.smBkgrndColor);
            intent.putExtra("mBkgrdImage", CommonConstants.smBkgrndImage);
            intent.putExtra("mBtnColor", listItem.getSmBtnColor());
            intent.putExtra("mBtnFontColor", listItem.getSmBtnFontColor());
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    protected void startFindActivityForGuestLogin(String mItemId) {
        Intent getLocationIntent = new Intent(mContext.getApplicationContext(),
                ThisLocationGetRetailers.class);
        getLocationIntent
                .putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        getLocationIntent.putExtra(CommonConstants.ZIP_CODE,
                Constants.GuestLoginZipcode);
        mContext.startActivity(getLocationIntent);
    }

    private void startFindScreen(String mItemId, String mLinkId,
                                 String itemName, String bgColor) {

        Intent intent = new Intent(mContext, FindActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("BgColor", bgColor);
        intent.putExtra("Title", itemName);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra("mLinkId", mLinkId);

        boolean isSubMenu;
        if (CommonConstants.previousMenuLevel != null && "1".equals(CommonConstants.previousMenuLevel)) {
            isSubMenu = false;
        } else {
            isSubMenu = true;
        }

        intent.putExtra("isSubMenu", isSubMenu);

        mContext.startActivity(intent);
    }

    private void startFindSingleCategoryScreen(String catName, String mItemId, String mLinkId) {

        Intent intent = new Intent(mContext, FindSingleCategory.class);
        intent.putExtra("catName", catName);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra("mLinkId", mLinkId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startEventsScreen(String mItemId, String mLinkId) {
        Intent intent = new Intent(mContext, EventsListDisplay.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra("mLinkId", mLinkId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startPrivacyPlicyScreen(String mItemId, String mLinkId) {
        Intent intent = new Intent(mContext, PrivacyPolicyActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra("mLinkId", mLinkId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startBandEventsScreen(String mItemId, String mLinkId) {
        Intent intent = new Intent(mContext, EventsListDisplay.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra("mLinkId", mLinkId);
        intent.putExtra("listType", "Band");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void startNewsTemplateSubMenu(Class act, String mItemId, String mLinkId) {
        Intent intent = new Intent(mContext, act);
        intent.putExtra(Constants.LEVEL, "2");
        intent.putExtra(Constants.ITEMID, mItemId);
        intent.putExtra(Constants.LINKID, mLinkId);
        intent.putExtra(Constants.IS_SUB_MENU, true);
        mContext.startActivity(intent);
    }

    protected void startSubMenu(NavigationItem mMainMenuBO) {

        Constants.SHOWGPSALERT = false;
        Integer nextLevel;

        int currentMenuLevel = 0;
        if (CommonConstants.previousMenuLevel != null) {
            nextLevel = Integer.parseInt(CommonConstants.previousMenuLevel) + 1;
            currentMenuLevel = nextLevel.intValue();

        } else {
            nextLevel = currentMenuLevel + 1;
            currentMenuLevel = nextLevel;
        }

        MainMenuBO mainMenuBO = new MainMenuBO(mMainMenuBO.getmItemId(),
                mMainMenuBO.getmItemName(), mMainMenuBO.getLinkTypeId(),
                mMainMenuBO.getLinkTypeName(), Integer.parseInt(mMainMenuBO.getSortOrder()),
                mMainMenuBO.getmItemImg(), mMainMenuBO.getLinkId(),
                mMainMenuBO.getmBtnColor(), mMainMenuBO.getmBtnFontColor(),
                mMainMenuBO.getSmBtnColor(), mMainMenuBO.getSmBtnFontColor(),
                nextLevel.toString(), mMainMenuBO.getmGrpFntColor(), mMainMenuBO.getSmGrpFntColor(),
                mMainMenuBO.getmGrpBkgrdColor(), mMainMenuBO.getSmGrpBkgrdColor(), null, null, null);
        SubMenuStack.onDisplayNextSubMenu(mainMenuBO);

        int level = SubMenuStack.getSubMenuStack().size();

        SubMenuDetails subMenuDetails = new SubMenuDetails("0", "0", "Name",

                "", mMainMenuBO.getLinkId(), mMainMenuBO.getmItemId(), /*level*/ currentMenuLevel);

        SortDepttNTypeScreen.subMenuDetailsList.add(subMenuDetails);

        MenuAsyncTask mMenuAsyncTask = new MenuAsyncTask((Activity) mContext);
        mMenuAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                String.valueOf(currentMenuLevel), mMainMenuBO.getmItemId(),
                mMainMenuBO.getLinkId(), "None", "0", "0");

    }

    private void startFilterScreen(String mItemId, NavigationItem mMainMenuBO) {

        if (null != menuItem.getRetAffCount() && "1".equals(menuItem.getRetAffCount())) {
            Intent showFilter = new Intent(mContext,
                    CitiExperienceActivity.class);
            showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                    mItemId);
            showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, cityExpId);
            showFilter.putExtra(Constants.CAT_IDS_INTENT_EXTRA, "0");
            showFilter.putExtra(CommonConstants.CITYEXP_IMG_URL, menuItem.getRetGroupImg());
            showFilter.putExtra(CommonConstants.TAG_FILTERID, menuItem.getRetAffId());
            showFilter.putExtra(CommonConstants.FILTERCOUNT_EXTRA, menuItem.getRetAffCount());
            showFilter.putExtra(CommonConstants.TAG_FILTERNAME, menuItem.getRetAffName());
            showFilter.putExtra("mBtnColor", mMainMenuBO.getmBtnColor());
            showFilter.putExtra("mBtnFontColor", mMainMenuBO.getmBtnFontColor());
            showFilter.putExtra("mBkgrdColor", CommonConstants.mBkgrndColor);
            showFilter.putExtra("mBkgrdImage", CommonConstants.mBkgrndImage);
            mContext.startActivity(showFilter);

        } else {
            Intent showFilter = new Intent(mContext,
                    CityExperienceFilterActivity.class);
            showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                    mItemId);
            showFilter.putExtra(Constants.CITI_EXPID_INTENT_EXTRA, cityExpId);
            showFilter.putExtra(CommonConstants.CITYEXP_IMG_URL, menuItem.getRetGroupImg());
            showFilter.putExtra("mBtnColor", mMainMenuBO.getmBtnColor());
            showFilter.putExtra("mBtnFontColor", mMainMenuBO.getmBtnFontColor());
            showFilter.putExtra("mBkgrdColor", CommonConstants.mBkgrndColor);
            showFilter.putExtra("mBkgrdImage", CommonConstants.mBkgrndImage);
            showFilter.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA,
                    mItemId);
            mContext.startActivity(showFilter);

        }

    }

    private void startFundraiserScreen(String mItemId, String mLinkId,
                                       String catIds, String searchKey) {
        Intent intent = new Intent(mContext, FundraiserActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra(Constants.SEARCH_KEY_INTENT_EXTRA, searchKey);
        intent.putExtra("mLinkId", mLinkId);
        intent.putExtra(Constants.CAT_IDS_INTENT_EXTRA, catIds);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);

    }

    private void startUserInformation() {
        Intent intent = new Intent(mContext, SetPreferrenceScreen.class);
        intent.putExtra("regacc", false);
        mContext.startActivity(intent);
    }

    private void startLocationPreference() {
        Intent intent = new Intent(mContext, SettingsLocationServices.class);
        intent.putExtra("regacc", false);
        mContext.startActivity(intent);
    }

    private void startCategoryFavourites() {
        Intent intent = new Intent(mContext, PreferedCatagoriesScreen.class);
        intent.putExtra("regacc", false);
        mContext.startActivity(intent);
    }

    private void startCityFavourites() {
        Intent intent = new Intent(mContext, CityPreferencesScreen.class);
        intent.putExtra("regacc", false);
        mContext.startActivity(intent);
    }

    private void startBandLandingScreen(String mItemId) {
        Intent intent = new Intent(mContext, BandsLandingPageActivity.class);
        intent.putExtra(Constants.MENU_ITEM_ID_INTENT_EXTRA, mItemId);
        intent.putExtra(Constants.MENU_ITEM_NAME_INTENT_EXTRA, mItemName);
        mContext.startActivity(intent);
    }

    public void logout() {
//		logoutMenuPropertiesActivity.this.finish();
        Constants.setZipCode("");
        Constants.bISLoginStatus = false;
//		Utils.startLoginScreen(MenuPropertiesActivity.this);
        Constants.DONT_ALLOW_LOGIN = true;
        Constants.SHOWGPSALERT = true;
////		Constants.removeHubCitiId();
        HubCityContext.savedSubMenuCityIds = new HashMap<>();
        HubCityContext.arrSubMenuDetails.clear();
        SharedPreferences preferences = mContext.getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = preferences.edit().putString(

                Constants.PREFERENCE_KEY_TEMP_U_ID,
                preferences.getString(
                        Constants.PREFERENCE_KEY_U_ID, ""));
        e.apply();
        CommonConstants.clearLoginCredentials(mContext);

        e = preferences.edit().putString("radious", "");
        e.apply();
        new HubCityContext().clearArrayAndAllValues(true);

        SharedPreferences.Editor prefEditor = mContext.getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0).edit();
        prefEditor.putBoolean(Constants.REMEMBER, false);
        prefEditor.apply();

        new LoginAsyncTask(mContext, false, true, null, 0, 0,
                true, null)
                .execute(Constants.USERNAME, Constants.PASSWORD);
    }

    private void signUp() {
        ((Activity) mContext).finish();
        Intent intent = new Intent(mContext,
                RegistrationActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(intent);
        SharedPreferences preferences = mContext.getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = preferences.edit().putString("radious", "");
        e.apply();
        new HubCityContext().clearArrayAndAllValues(true);
    }

}
