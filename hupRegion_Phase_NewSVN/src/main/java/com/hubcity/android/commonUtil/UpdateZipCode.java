package com.hubcity.android.commonUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class UpdateZipCode extends AsyncTask<Void, Void, Boolean> {
	JSONObject jsonObject = null;
	String userName;
	String zip;
	Context mContext;

	public UpdateZipCode(Context context, String userName, String zip) {
		this.userName = userName;
		this.zip = zip;
		mContext = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

	}

	public String createUpdateZipCodeUrlParameter(String userName, String zip)
			throws Exception {

		StringBuilder urlBuilder = new StringBuilder();
		if (userName != null) {
			urlBuilder.append("userId=").append(userName).append("&zipcode=").append(zip);
		}
		Log.i("urlBuilder", urlBuilder.toString());
		return urlBuilder.toString();
	}

	public JSONObject getJSONFromUrl(String strURL, String postData)
			throws Exception {

		String urlStr = strURL + postData;
		URL url = new URL(urlStr);
		HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
		ucon.connect();

		BufferedReader in1;
		HttpGet requestGet;
		HttpResponse response;
		HttpClient client = new DefaultHttpClient();

		requestGet = new HttpGet(new URI(strURL + postData));
		response = client.execute(requestGet);
		in1 = new BufferedReader(new InputStreamReader(response.getEntity()
				.getContent()));
		String inputLine;
		StringBuffer response1 = new StringBuffer();

		while ((inputLine = in1.readLine()) != null) {
			response1.append(inputLine);
		}
		in1.close();
		String xmlResponde = response1.toString();
		Log.d(" #### xmlResponde :", xmlResponde);

		return XML.toJSONObject(xmlResponde);

	}

	@Override
	protected Boolean doInBackground(Void... voids) {
		Boolean result = false;

		try {

			String urlParameters = createUpdateZipCodeUrlParameter(userName,
					zip);
			String url_updateusrzipcode = Properties.url_local_server
					+ Properties.hubciti_version
					+ "thislocation/updateusrzipcode?";
			jsonObject = getJSONFromUrl(url_updateusrzipcode, urlParameters);

			if (jsonObject != null) {
				if ("10000".equals(jsonObject.getJSONObject("response")
						.getString("responseCode"))) {
					result = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if (result) {
			if (zip.length() > 0) {
				Constants.setZipCode(zip);
			}
		}
		// successfull

		try {

			if (jsonObject != null && jsonObject.has("responseText")) {
				Toast.makeText(
						mContext,
						jsonObject.getJSONObject("response").getString(
								"responseText"), Toast.LENGTH_SHORT).show();
			}

		} catch (NotFoundException e) {
			e.printStackTrace();
		}

		catch (JSONException e) {
			e.printStackTrace();
		}
	}

}