package com.hubcity.android.commonUtil;

import java.io.InputStream;
import java.io.OutputStream;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import com.hubcity.android.businessObjects.LoginScreenSingleton;
import com.hubcity.android.screens.LoginScreen;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;

public class Utils {

	public static void copyStream(InputStream is, OutputStream os) {
		final int bufferSize = 1024;
		try {
			byte[] bytes = new byte[bufferSize];
			for (;;) {
				int count = is.read(bytes, 0, bufferSize);
				if (count == -1) {
					break;
				}
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public static void createAlert(Context activityContext, String text) {
		if (text == null) {
			return;
		}
		final AlertDialog alertDialog = new AlertDialog.Builder(activityContext)
				.create();
		alertDialog.setMessage(text);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();
			}
		});
		alertDialog.show();
	}

	public static byte[] convertingJsonObjectToByteArray(JSONObject csJsonobj) {
		byte[] bytearr = new byte[1024];
		if (csJsonobj == null) {
			byte[] csbyte = new byte[0];
			return csbyte;
		}
		bytearr = csJsonobj.toString().getBytes();
		return bytearr;
	}

	public static byte[] convertingJsonArrayToByteArray(JSONArray csJsonArr) {
		byte[] bytearr = new byte[1024];
		if (csJsonArr == null) {
			byte[] csbyte = new byte[0];
			return csbyte;
		}
		bytearr = csJsonArr.toString().getBytes();
		return bytearr;
	}

	public static SQLiteDatabase creatingDB(Context context) {
		SQLiteDatabase csSql = null;
		try {
			csSql = context.openOrCreateDatabase("HupCity.db",
					SQLiteDatabase.CREATE_IF_NECESSARY, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return csSql;
	}

	public static boolean hasFroyo() {
		// Can use static final constants like FROYO, declared in later versions
		// of the OS since they are inlined at compile time. This is guaranteed
		// behavior.
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	public static boolean hasGingerbread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

}