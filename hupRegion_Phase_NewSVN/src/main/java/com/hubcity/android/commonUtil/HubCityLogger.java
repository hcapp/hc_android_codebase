package com.hubcity.android.commonUtil;

import android.util.Log;

public class HubCityLogger {

	static boolean release = false;
	
	public static int d(String tag, String msg) {
		if (release) {
			return 0;
		} else {
			return Log.d(tag, msg);
		}
	}

	public static int e(String tag, String msg) {
		if (release) {
			return 0;
		} else {
			return Log.e(tag, msg);
		}
	}

	public static int i(String tag, String msg) {

		if (release) {
			return 0;
		} else {
			return Log.i(tag, msg);
		}
	}

	public static int v(String tag, String msg) {
		if (release) {
			return 0;
		} else {
			return Log.v(tag, msg);
		}
	}

	public static int w(String tag, String msg) {
		if (release) {
			return 0;
		} else {
			return Log.w(tag, msg);
		}
	}

	public static int e(String tag, String msg, Exception e) {
		if (release) {
			return 0;
		} else {
			return Log.e(tag, msg, e);
		}
	}

	public static int w(String tag, String msg, Exception e) {
		if (release) {
			return 0;
		} else {
			return Log.w(tag, msg, e);
		}
	}

	public static int w(String tag, Exception e) {
		if (release) {
			return 0;
		} else {
			return Log.w(tag, e);
		}
	}

}
