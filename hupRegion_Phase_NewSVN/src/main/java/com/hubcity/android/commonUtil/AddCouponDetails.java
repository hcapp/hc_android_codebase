package com.hubcity.android.commonUtil;

/**
 * Created by shruthi.n on 12/5/2016.
 */
public class AddCouponDetails {

    private final String couponId;
    private final String userId;

    public AddCouponDetails(String couponId, String userId)
    {
        this.couponId = couponId;
        this.userId = userId;
    }
}
