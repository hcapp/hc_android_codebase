package com.hubcity.android.commonUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.businessObjects.MainMenuBO;
import com.hubcity.android.screens.CityPreferencesScreen;
import com.hubcity.android.screens.ComboTemplateScreen;
import com.hubcity.android.screens.FourTileTemplate;
import com.hubcity.android.screens.GroupedListViewTabMenuScreen;
import com.hubcity.android.screens.GroupedTabMenuScreen;
import com.hubcity.android.screens.MainMenuGridTemplate;
import com.hubcity.android.screens.MainMenuTwoImageTemplate;
import com.hubcity.android.screens.MenuPropertiesActivity;
import com.hubcity.android.screens.RectangleGridTemplate;
import com.hubcity.android.screens.SingleColumnMenuTemplate;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.hubcity.android.screens.SquareGridTemplate;
import com.hubcity.android.screens.SquareImageTemplate;
import com.hubcity.android.screens.TemplateListMenuScreen;
import com.hubcity.android.screens.ThreeCrossThreeTemplate;
import com.hubcity.android.screens.TwoColumnMenuTemplate;
import com.hubcity.android.screens.TwoColumnMenuTemplateWithBanner;
import com.hubcity.android.screens.TwoTileTemplate;
import com.scansee.hubregion.R;
import com.scansee.newsfirst.CombinationTemplate;
import com.scansee.newsfirst.GlobalConstants;
import com.scansee.newsfirst.ScrollingPageActivity;
import com.scansee.newsfirst.TwoTileNewsTemplateActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MenuAsyncTask extends
        AsyncTask<String, String, ArrayList<MainMenuBO>> {
    private JSONObject xmlResponde;

    private static final String ICONIC_GRID = "Iconic Grid";
    private static final String SQUARE_GRID = "Square Grid";
    private static final String ICONIC_3x3 = "Iconic 3X3";
    private static final String SQUARE_3x3 = "Square Grid  3x3";
    private static final String TWO_IMAGE = "Two Image Template";
    private static final String GRID_4x4 = "4X4 Grid";
    private static final String RECTANGULAR_GRID = "Rectangular Grid";
    private static final String LIST_VIEW_TEMPLATE = "List View";
    private static final String LIST_VIEW_WITH_BANNER_TEMPLATE = "List View with Banner Ad";
    private static final String TWO_COLUMN_TAB_WITH_BANNER_AD = "Two column Tab with Banner Ad";
    private static final String GROUPED_TAB = "Grouped Tab";
    private static final String TWO_COLUMN_MENU_TEMPLATE = "Single/Two Column Tab";
    private static final String SQUARE_IMAGE_TEMPLATE = "Square Image Template";
    private static final String FOUR_TILE_TEMPLATE = "Four Tile Template";
    private static final String TWO_TILE_TEMPLATE = "Two Tile Template";
    private static final String COMBO_TEMPLETE = "Combo Template";
    private static final String GROUPED_TAB_WITH_IMAGE = "Grouped Tab With Image";

    public static final String MENU_ID = "mItemId";
    public static final String MENU_ITEM_NAME = "mItemName";
    public static final String MENU_LINKTYPE_ID = "linkTypeId";
    public static final String MENU_LINK_TYPE_NAME = "linkTypeName";
    public static final String MENU_ITEM_POSITION = "position";
    public static final String MENU_ITEM_ITEM_IMG_URL = "mItemImg";
    public static final String MENU_ITEM_LINKID = "linkId";
    public static final String MENU_ITEM_MBTNCOLOR = "mBtnColor";
    public static final String MENU_ITEM_MBTNFONTCOLOR = "mBtnFontColor";
    public static final String MENU_ITEM_SMBTNCOLOR = "smBtnColor";
    public static final String MENU_ITEM_SMBTNFONTCOLOR = "smBtnFontColor";
    private static final String CITY_EXP_IMG_URL = "retGroupImg";
    private static final String APP_ICON_LINK = "appIconImg";
    private static final String APP_PLAYSTORE_LINK = "androidLink";
    private static final String MENU_NAME = "menuName";

    public static final String MM_GROUP_FONT_COLOR = "mGrpFntColor";
    public static final String MM_GROUP_BG_COLOR = "mGrpBkgrdColor";

    public static final String SM_GROUP_FONT_COLOR = "smGrpFntColor";
    public static final String m_ShapeName = "mShapeName";


    public static final String SM_GROUP_BG_COLOR = "smGrpBkgrdColor";
    public static final String FILETR_COUNT = "retAffCount";
    public static final String FILETR_ID = "retAffId";
    public static final String FILETR_NAME = "retAffName";
    private static final String TAG = "MenuAsyncTask";

    public static String TempBkgrdImg;
    public int isDisplayLabel = 0;
    public static String btnBkgrdColor;
    public static String btnLblColor;

    public static JSONObject bbJsonObj;
    public static ArrayList<BottomButtonBO> bottomButtonList;
    public String templateName, noOfColumns = "";
    public String level;
    private ProgressDialog progDialog;
    private String responseText;
    public String mBkgrdColor;
    public String templateBgColor;
    public String mBkgrdImage;
    public String smBkgrdColor;
    public String smBtnFontColor;
    public String smBkgrdImage;
    public String mFontColor;
    public String smFontColor;
    public String departFlag;
    public String cityExpImgUlr;
    public String typeFlag;
    private String smBtnColor;
    private final Activity mActivity;
    public int mLevel;
    public String mBannerImg;
    public static String dateModified;
    public int isTempChanged;
    private boolean chkloc;
    private String mainMenu = null;
    public String appPlaystoreLInk;
    private String appleStoreLink;
    public String appIconImg;
    public String retAffCount = null;
    public String retAffId = null;
    public String retAffName = null;

    String mLinkId = null;
    private String mItemId = null;
    private String timestamp = "";

    public ArrayList<MainMenuBO> mainMenuUnsortedList = new ArrayList<>();
    public static final ArrayList<String> mainIDs = new ArrayList<>();
    public static final ArrayList<String> menuNames = new ArrayList<>();
    String mainMenuID;
    private MenuDataHelper csObjMenuAsync;
    private SQLiteDatabase csSql;
    private final UrlRequestParams mUrlRequestParams = new UrlRequestParams();
    private final ServerConnections mServerConnections = new ServerConnections();
    private String responseCode;

    //newsTicker
    public static String tickerTxtColor;
    public static String tickerBkgrdColor;
    public static String tickerDirection;
    public static String tickerMode;
    public static String isNewTicker = "0";
    public static ArrayList<TickerObj> tickerItem = new ArrayList<>();


    public MenuAsyncTask(Activity activity) {
        super();
        this.mActivity = activity;
    }

    public MenuAsyncTask(Boolean chkloc, Activity activity) {
        super();
        this.mActivity = activity;
        this.chkloc = chkloc;
        Log.v(TAG, "FLAG SET : " + chkloc);
    }

    public MenuAsyncTask(Boolean chkloc, Activity activity, String mainMenu) {
        super();
        this.mActivity = activity;
        this.chkloc = chkloc;
        this.mainMenu = mainMenu;
    }

    @Override
    protected void onPreExecute() {
        try {
            if (!mActivity.isFinishing()) {

                csObjMenuAsync = new MenuDataHelper(mActivity);
                csSql = Utils.creatingDB(mActivity);

                csObjMenuAsync.creatingTable(csSql, "menu");

                progDialog = new ProgressDialog(mActivity);
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setMessage("Please wait...");
                progDialog.setCancelable(false);
                progDialog.show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPreExecute();
    }

    @Override
    protected ArrayList<MainMenuBO> doInBackground(String... params) {

        ArrayList<MainMenuBO> csObjofAccess = null;
        String cityIds = "", sortColumn = "None";
        String level = params[0];
        mItemId = params[1];
        mLinkId = params[2];
        String departmentId = params[4];
        String typeId = params[5];
        String templateType = null;
        try {
            templateType = params[6];
        } catch (Exception e) {
            e.printStackTrace();
        }

        String sorted_cities;
        //Getting the cached values if any is there for the particular submenu
        try {
            if (HubCityContext.arrSubMenuDetails != null && HubCityContext.arrSubMenuDetails.size
                    () >
                    0) {
                for (int i = 0; i < HubCityContext.arrSubMenuDetails.size(); i++) {
                    if (HubCityContext.arrSubMenuDetails.get(i).getmItemId().equals(mItemId)) {
                        departmentId = HubCityContext.arrSubMenuDetails.get(i)
                                .getSavedDepartmentId();
                        typeId = HubCityContext.arrSubMenuDetails.get(i).getSavedTypeId();
                        sortColumn = HubCityContext.arrSubMenuDetails.get(i).getSortBy();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (level != null) {
            mLevel = Integer.parseInt(level);
        }

        sorted_cities = CommonConstants.getSortedCityIds(new CityDataHelper().getCitiesList(mActivity));

        if (mLevel == 1) {
            sortColumn = "None";

        } else {

            try {
                if (HubCityContext.savedSubMenuCityIds.containsKey(mItemId)) {
                    cityIds = HubCityContext.savedSubMenuCityIds.get(mItemId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            cityIds = cityIds.replace("All,", "");
            Log.d(TAG, "cityIds" + cityIds);
            //get from filters of submenu
            if (cityIds != null && !cityIds.isEmpty()) {
                sorted_cities = CommonConstants.getSortedCityIds(cityIds);
            }
        }

        SharedPreferences settings = mActivity.getSharedPreferences(
                Constants.PREFERENCE_HUB_CITY, 0);
        Log.v(TAG, "REG ID : " + settings.getString("gcm_registrationid", null));
        Log.v(TAG, "LINK ID IN MENU LOADING :: " + mLinkId);
        if (csObjMenuAsync != null
                && csObjMenuAsync.checkingLevels(csSql, "menu", mLinkId)
                && !SortDepttNTypeScreen.isSubMenuSort
                && !CityPreferencesScreen.bIsSendingTimeStamp) {
            timestamp = csObjMenuAsync.strDateCreated;
        }
        if (csObjMenuAsync != null
                && csObjMenuAsync.checkingLevels(csSql, "menu", mLinkId)
                ) {
            dateModified = csObjMenuAsync.strDateCreated;
        }
        Log.v(TAG, "timestamp: " + timestamp);
        Log.v(TAG, "dateModified : " + dateModified);

        if (departmentId == null) {
            departmentId = "0";
        }
        if (typeId == null) {
            typeId = "0";
        }
        if (mItemId == null) {
            mItemId = "0";
        }
        if (mLinkId == null) {
            mLinkId = "0";
        }


        JSONObject urlJsonParameters;
        String url_menu_display;

        try {

            if (Properties.isRegionApp) {
               url_menu_display = Properties.url_local_server
                        + Properties.hubciti_version + "firstuse/hubregionmenudisplay";
            } else {
               url_menu_display = Properties.url_local_server
                        + Properties.hubciti_version + "firstuse/hubmenudisplay";
                sorted_cities = "";
            }
            if (!csObjMenuAsync.isCacheAvailable(csSql,
                    "menu", mLinkId, MenuAsyncTask.this)) {
                dateModified = null;
            }
            if (GlobalConstants.isFromNewsTemplate) {
                dateModified = null;
            }
            if (templateType != null && templateType.equals("NewsTemplate")) {
                dateModified = null;
            }


            urlJsonParameters = mUrlRequestParams
                    .createMainMenuJsonParameter(Integer.parseInt(mLinkId),
                            Integer.parseInt(level),
                            Integer.parseInt(UrlRequestParams.getHubCityId()),
                            sortColumn, Integer.parseInt(departmentId),
                            Integer.parseInt(typeId), sorted_cities, dateModified);


            Log.v(TAG, "REQUEST URL : " + urlJsonParameters);

            xmlResponde = mServerConnections.getUrlJsonPostResponse(
                    url_menu_display, urlJsonParameters);
            if (!(xmlResponde.optString("templateName").equalsIgnoreCase(Constants.COMBINATION) ||
                    xmlResponde.optString("templateName").equalsIgnoreCase(Constants.SCROLLING)
                    || xmlResponde.optString("templateName").equalsIgnoreCase(Constants.NEWS_TILE))) {
                tickerItem = new ArrayList<>();
            }
            if (validatingExistingData(xmlResponde) == 0) {

                try {
                    csObjofAccess = csObjMenuAsync.retrievingValues(csSql,
                            "menu", mLinkId, MenuAsyncTask.this);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                bbJsonObj = xmlResponde;

            } else {
                csObjofAccess = jsonParseMenuResponse(xmlResponde);
                jsonParseTickerResponse(xmlResponde);


            }

        } catch (Exception e) {

            e.printStackTrace();

        }
        return csObjofAccess;
    }


    @Override
    protected void onPostExecute(ArrayList<MainMenuBO> mainMenuUnsortedList) {
        if (responseCode != null && responseCode.equals("10000")) {
            //checking for parsed response
            loadTemplates(mainMenuUnsortedList);
        } else if (responseCode == null) {
            //checking for database response
            loadTemplates(mainMenuUnsortedList);
        } else {
            if (mActivity != null) {
                Utils.createAlert(mActivity, responseText.replace("\\n", "\n"));
                if (mActivity instanceof RectangleGridTemplate) {
                    ((RectangleGridTemplate) mActivity).gridview.setVisibility(View.INVISIBLE);
                }
            }
        }

        try

        {
            if (progDialog != null && progDialog.isShowing()) {
                progDialog.dismiss();
            }
        } catch (
                final IllegalArgumentException e
                )

        {

            e.printStackTrace();

        } catch (
                Exception e
                )

        {
            e.printStackTrace();

        } finally

        {
            if (csSql != null) {
                csSql.close();
            }
            progDialog = null;
        }

        super.onPostExecute(mainMenuUnsortedList);
    }

    private void loadTemplates(ArrayList<MainMenuBO> mainMenuUnsortedList) {
        if (templateName != null) {
            //Code added by Supriya
            //Check for template change
            if (templateName.equalsIgnoreCase(Constants.SCROLLING)) {
                startNewsTemplate(ScrollingPageActivity.class);
            } else if (templateName.equalsIgnoreCase(Constants.COMBINATION)) {
                startNewsTemplate(CombinationTemplate.class);
            } else if (templateName.equalsIgnoreCase(Constants.NEWS_TILE)) {
                startNewsTemplate(TwoTileNewsTemplateActivity.class);
            } else {
                if (mActivity instanceof RectangleGridTemplate) {
                    ((RectangleGridTemplate) mActivity).gridview.setVisibility(View.VISIBLE);
                }
                startMenuActivity(templateName, mainMenuUnsortedList);
            }
        } else {
            if (mActivity != null) {
                Utils.createAlert(mActivity, responseText);
                if (mActivity instanceof RectangleGridTemplate) {
                    ((RectangleGridTemplate) mActivity).gridview.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    //Code added by Supriya
    private void startNewsTemplate(Class activity) {
        if (level.equals("1")) {
            SubMenuStack.clearSubMenuStack();
            MainMenuBO mainMenuBO = new MainMenuBO(mItemId, null, null, null, 0,
                    null, mLinkId, null, null, null, null, "1", null,
                    null, null,
                    null, null, null, null);
            SubMenuStack.onDisplayNextSubMenu(mainMenuBO);
            Intent intent = new Intent(mActivity, activity);
            mActivity.startActivity(intent);
            mActivity.finish();
        } else {
            Intent intent = new Intent(mActivity, activity);
            intent.putExtra(Constants.LEVEL, level);
            intent.putExtra(Constants.ITEMID, mItemId);
            intent.putExtra(Constants.LINKID, mLinkId);
            intent.putExtra(Constants.IS_SUB_MENU, true);
            mActivity.startActivity(intent);
        }
    }


    private int validatingExistingData(JSONObject xmlResponde2) {
        int iTempchange = 1;
        if (xmlResponde2 != null) {
            try {

                if (xmlResponde2.has("isTempChanged")) {
                    iTempchange = xmlResponde2.getInt("isTempChanged");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return iTempchange;

    }

    @SuppressLint("UseValueOf")
    private ArrayList<MainMenuBO> jsonParseMenuResponse(JSONObject xmlResponde2) {
        Log.d(TAG, "Inside jsonParseMenuResponse");

        ArrayList<MainMenuBO> mainMenuUnsortedList = null;


        if (xmlResponde2 != null) {

            /** for error response **/
            if (xmlResponde2.has("response")) {

                JSONObject responseJsonObj;
                try {
                    responseJsonObj = xmlResponde.getJSONObject("response");
                    responseText = responseJsonObj.getString("responseText");
                    responseCode = responseJsonObj.getString("responseCode");
                    if (responseJsonObj != null && responseJsonObj.has("hamburgerImg")) {
                        Constants.setNavigationBarValues("hamburgerImg", responseJsonObj.getString
                                ("hamburgerImg"));
                    }
                } catch (JSONException e) {

                    e.printStackTrace();

                }
            } else {
                try {
                    responseText = xmlResponde2.getString("responseText");
                    responseCode = xmlResponde2.getString("responseCode");


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            /** for error response --END **/

            mainMenuUnsortedList = new ArrayList<>();


            try {

                JSONObject menuJsonObj = xmlResponde2;

                bbJsonObj = menuJsonObj;
                //used for news Ticker
                if (menuJsonObj.has("isNewTicker")) {
                    isNewTicker = menuJsonObj.getString("isNewTicker");
                }
                if (menuJsonObj.has("tickerTxtColor")) {
                    tickerTxtColor = menuJsonObj.getString("tickerTxtColor");
                }
                if (menuJsonObj.has("tickerBkgrdColor")) {
                    tickerBkgrdColor = menuJsonObj.getString("tickerBkgrdColor");
                }
                if (menuJsonObj.has("tickerDirection")) {
                    tickerDirection = menuJsonObj.getString("tickerDirection");
                }
                if (menuJsonObj.has("tickerMode")) {
                    tickerMode = menuJsonObj.getString("tickerMode");
                }


                //TempBkgrdImg and isDisplayLabel only in two image template
                if (menuJsonObj.has("TempBkgrdImg")) {
                    TempBkgrdImg = menuJsonObj.getString("TempBkgrdImg");
                } else {
                    TempBkgrdImg = null;
                }
                if (menuJsonObj.has("isDisplayLabel")) {
                    isDisplayLabel = menuJsonObj.getInt("isDisplayLabel");
                }
                if (menuJsonObj.has("btnBkgrdColor")) {
                    btnBkgrdColor = menuJsonObj.getString("btnBkgrdColor");
                }
                if (menuJsonObj.has("btnLblColor")) {
                    btnLblColor = menuJsonObj.getString("btnLblColor");
                }

                if (menuJsonObj.has("dateModified")) {
                    dateModified = menuJsonObj.getString("dateModified");
                }

                if (menuJsonObj.has("isTempChanged")) {
                    isTempChanged = menuJsonObj.getInt("isTempChanged");
                }

                if (menuJsonObj.has("mBannerImg")) {
                    mBannerImg = menuJsonObj.getString("mBannerImg")
                            .replaceAll(" ", "%20");
                }

                if (menuJsonObj.has("mBkgrdColor")) {
                    mBkgrdColor = menuJsonObj.getString("mBkgrdColor");
                }

                if (menuJsonObj.has("templateBgColor")) {
                    templateBgColor = menuJsonObj.getString("templateBgColor");
                }

                if (menuJsonObj.has("mBkgrdImage")) {
                    mBkgrdImage = menuJsonObj.getString("mBkgrdImage");
                } else {
                    mBkgrdImage = null;
                }

                if (menuJsonObj.has("smBtnColor")) {
                    smBkgrdColor = menuJsonObj.getString("smBtnColor");
                }

                if (menuJsonObj.has("smBkgrdImage")) {
                    smBkgrdImage = menuJsonObj.getString("smBkgrdImage");
                } else {
                    smBkgrdImage = null;
                }

                if (menuJsonObj.has("smBkgrdColor")) {
                    smBkgrdColor = menuJsonObj.getString("smBkgrdColor");
                }

                if (menuJsonObj.has("smBtnFontColor")) {
                    smBtnFontColor = menuJsonObj.getString("smBtnFontColor");
                }

                if (menuJsonObj.has("mFontColor")) {
                    mFontColor = menuJsonObj.getString("mFontColor");
                }

                if (menuJsonObj.has("smFontColor")) {
                    smFontColor = menuJsonObj.getString("smFontColor");
                }

                if (menuJsonObj.has("departFlag")) {
                    departFlag = menuJsonObj.getString("departFlag");
                }

                if (menuJsonObj.has("retGroupImg")) {
                    cityExpImgUlr = menuJsonObj.getString(CITY_EXP_IMG_URL);
                }

                if (menuJsonObj.has(APP_ICON_LINK)) {
                    appIconImg = menuJsonObj.getString(APP_ICON_LINK);
                }

                if (menuJsonObj.has(APP_PLAYSTORE_LINK)) {
                    appPlaystoreLInk = menuJsonObj
                            .getString(APP_PLAYSTORE_LINK);
                }

                if (menuJsonObj.has("downLoadLink")) {
                    appleStoreLink = menuJsonObj.getString("downLoadLink");
                }

                if (menuJsonObj.has(FILETR_COUNT)) {
                    retAffCount = menuJsonObj.getString(FILETR_COUNT);
                }

                if (null != retAffCount && "1".equals(retAffCount)) {
                    if (menuJsonObj.has(FILETR_ID)) {
                        retAffId = menuJsonObj.getString(FILETR_ID);
                    }
                    if (menuJsonObj.has(FILETR_NAME)) {
                        retAffName = menuJsonObj.getString(FILETR_NAME);
                    }
                }

                if (menuJsonObj.has(MENU_NAME)) {
                    MenuPropertiesActivity.menuName = menuJsonObj
                            .getString(MENU_NAME);

                    menuNames.add(menuJsonObj.getString(MENU_NAME));
                }

                if (menuJsonObj.has("typeFlag")) {
                    typeFlag = menuJsonObj.getString("typeFlag");
                }

                //Getting Navigation bar values and storing it to shared preference to access from
                // all the screens
                if (menuJsonObj.has("bkImgPath")) {
                    Constants.setNavigationBarValues("bkImgPath", menuJsonObj.getString
                            ("bkImgPath"));
                }
                if (menuJsonObj.has("homeImgPath")) {
                    Constants.setNavigationBarValues("homeImgPath", menuJsonObj.getString
                            ("homeImgPath"));
                }
                if (menuJsonObj.has("hamburgerImg")) {
                    Constants.setNavigationBarValues("hamburgerImg", menuJsonObj.getString
                            ("hamburgerImg"));
                }
                if (menuJsonObj.has("titleTxtColor")) {
                    Constants.setNavigationBarValues("titleTxtColor", menuJsonObj.getString
                            ("titleTxtColor"));
                }
                if (menuJsonObj.has("titleBkGrdColor")) {
                    Constants.setNavigationBarValues("titleBkGrdColor", menuJsonObj.getString
                            ("titleBkGrdColor"));
                }

                // Check if its region app
                if (menuJsonObj.has("isRegApp")) {
                    if ("1".equals(menuJsonObj.getString("isRegApp"))) {
                        Properties.isRegionApp = true;
                    } else if ("0".equals(menuJsonObj.getString("isRegApp"))) {
                        Properties.isRegionApp = false;
                    }
                }
                if (menuJsonObj.has("templateName")) {
                    templateName = menuJsonObj.getString("templateName");
                }


                if (menuJsonObj.has("noOfColumns")) {
                    noOfColumns = menuJsonObj.getString("noOfColumns");
                }

                if (menuJsonObj.has("menuId")) {
                    mainMenuID = menuJsonObj.getString("menuId");
                }

                if (menuJsonObj.has("level")) {
                    int levelInt = menuJsonObj.getInt("level");
                    level = Integer.toString(levelInt);

                } else {
                    level = String.valueOf(SubMenuStack.returnStackSize());

                }

                JSONObject menuItem = menuJsonObj.optJSONObject("arMItemList");
                // which will return a JSONObject or null if the MenuItem object
                // is not a json object. Then you do the following:

                JSONArray menuItemJsonObjArray = null;
                if (menuItem == null) {

                    menuItemJsonObjArray = menuJsonObj
                            .optJSONArray("arMItemList");
                } else {

                    String mItemName = menuItem.getString(MENU_ITEM_NAME);
                    String mItemId = menuItem.getString(MENU_ID);
                    String linkTypeId = menuItem.getString(MENU_LINKTYPE_ID);
                    String linkTypeName = menuItem
                            .getString(MENU_LINK_TYPE_NAME);

                    String position = menuItem.getString(MENU_ITEM_POSITION);
                    String mItemImgUrl = menuItem
                            .getString(MENU_ITEM_ITEM_IMG_URL);

                    String linkId = null;

                    String mgrpFntColor = null, sgrpFntColor = null, mgrpBgColor = null,
                            sgrpBgColor = null, mShapeName = null, mBkgrdImage = null,
                            mBkgrdColor = null;

                    if (menuItem.has(MENU_ITEM_LINKID)) {
                        int linkIdInt = menuItem.getInt(MENU_ITEM_LINKID);
                        linkId = Integer.toString(linkIdInt);
                    }

                    String mBtnColor = menuItem
                            .getString(MENU_ITEM_MBTNFONTCOLOR);
                    String mBtnFontColor = menuItem
                            .getString(MENU_ITEM_MBTNFONTCOLOR);
                    smBtnColor = menuItem
                            .getString(MENU_ITEM_SMBTNCOLOR);
                    String smBtnFontColor = menuItem
                            .getString(MENU_ITEM_SMBTNFONTCOLOR);

                    if (menuItem.has(MM_GROUP_FONT_COLOR)) {
                        mgrpFntColor = menuItem.getString(MM_GROUP_FONT_COLOR);

                    }

                    if (menuItem.has(SM_GROUP_FONT_COLOR)) {
                        sgrpFntColor = menuItem.getString(SM_GROUP_FONT_COLOR);

                    }

                    if (menuItem.has(MM_GROUP_BG_COLOR)) {
                        mgrpBgColor = menuItem.getString(MM_GROUP_BG_COLOR);

                    }

                    if (menuItem.has(SM_GROUP_BG_COLOR)) {
                        sgrpBgColor = menuItem.getString(SM_GROUP_BG_COLOR);

                    }
                    if (menuItem.has(m_ShapeName)) {
                        mShapeName = menuItem.getString(m_ShapeName);

                    }
                    if (menuItem.has("mBkgrdImage")) {
                        mBkgrdImage = menuItem.getString("mBkgrdImage");

                    }

                    if (menuItem.has("mBkgrdColor")) {
                        mBkgrdColor = menuItem
                                .getString("mBkgrdColor");

                    }

                    MainMenuBO mMainMenuBO = new MainMenuBO(mItemId, mItemName,
                            linkTypeId, linkTypeName,
                            Integer.parseInt(position), mItemImgUrl, linkId,
                            mBtnColor, mBtnFontColor, smBtnColor,
                            smBtnFontColor, level, mgrpFntColor, sgrpFntColor,
                            mgrpBgColor, sgrpBgColor, mShapeName, mBkgrdImage, mBkgrdColor);
                    mainMenuUnsortedList.add(mMainMenuBO);
                }

                if (isTempChanged != 0) {
                    if (csObjMenuAsync != null
                            && !csObjMenuAsync.checkingLevels(csSql, "menu",
                            mLinkId)) {
                        csObjMenuAsync.insertingValues(csSql, "menu", mLinkId,
                                this, menuItemJsonObjArray, menuItem);
                    } else {
                        if (csObjMenuAsync != null
                                && !dateModified
                                .equals(csObjMenuAsync.strDateCreated)
                                || SortDepttNTypeScreen.isSubMenuSort) {
                            csObjMenuAsync.updatemanimenu(csSql, "menu",
                                    "linkId", mLinkId, this,
                                    menuItemJsonObjArray, menuItem);
                        }
                    }
                }


                if (menuItemJsonObjArray != null) {
                    for (int i = 0; i < menuItemJsonObjArray.length(); i++) {

                        // tab's group's header
                        JSONObject menuItemJsonObj = menuItemJsonObjArray
                                .getJSONObject(i);

                        String mItemName = menuItemJsonObj
                                .getString(MENU_ITEM_NAME);
                        String mItemId = menuItemJsonObj.getString(MENU_ID);
                        String linkTypeId = menuItemJsonObj
                                .getString(MENU_LINKTYPE_ID);
                        String linkTypeName = menuItemJsonObj
                                .getString(MENU_LINK_TYPE_NAME);
                        String position = menuItemJsonObj
                                .getString(MENU_ITEM_POSITION);
                        String mItemImgUrl = menuItemJsonObj
                                .getString(MENU_ITEM_ITEM_IMG_URL);
                        String linkId = null;
                        String mgrpFntColor = null, sgrpFntColor = null, mgrpBgColor = null,
                                sgrpBgColor = null, mShapeName = null, mBkgrdImage = null,
                                mBkgrdColor = null;

                        if (menuItemJsonObj.has(MENU_ITEM_LINKID)) {
                            int linkIdInt = menuItemJsonObj
                                    .getInt(MENU_ITEM_LINKID);
                            linkId = Integer.toString(linkIdInt);

                        }

                        String mBtnColor = menuItemJsonObj
                                .getString(MENU_ITEM_MBTNCOLOR);
                        String mBtnFontColor = menuItemJsonObj
                                .getString(MENU_ITEM_MBTNFONTCOLOR);
                        String smBtnColor = menuItemJsonObj
                                .getString(MENU_ITEM_SMBTNCOLOR);
                        String smBtnFontColor = menuItemJsonObj
                                .getString(MENU_ITEM_SMBTNFONTCOLOR);
                        if (menuItemJsonObj.has(MM_GROUP_FONT_COLOR)) {
                            mgrpFntColor = menuItemJsonObj
                                    .getString(MM_GROUP_FONT_COLOR);

                        }

                        if (menuItemJsonObj.has(SM_GROUP_FONT_COLOR)) {
                            sgrpFntColor = menuItemJsonObj
                                    .getString(SM_GROUP_FONT_COLOR);

                        }

                        if (menuItemJsonObj.has(MM_GROUP_BG_COLOR)) {
                            mgrpBgColor = menuItemJsonObj
                                    .getString(MM_GROUP_BG_COLOR);

                        }

                        if (menuItemJsonObj.has("departmentName")) {
                            if (menuItemJsonObj.has("smBkgrdColor")) {
                                sgrpBgColor = menuItemJsonObj
                                        .getString("smBkgrdColor");

                            }
                        } else {
                            if (menuItemJsonObj.has("smGrpBkgrdColor")) {
                                sgrpBgColor = menuItemJsonObj
                                        .getString("smGrpBkgrdColor");

                            }
                        }

                        if (menuItemJsonObj.has(m_ShapeName)) {
                            mShapeName = menuItemJsonObj.getString(m_ShapeName);

                        }

                        if (menuItemJsonObj.has("mBkgrdImage")) {
                            mBkgrdImage = menuItemJsonObj
                                    .getString("mBkgrdImage");

                        }
                        if (menuItemJsonObj.has("mBkgrdColor")) {
                            mBkgrdColor = menuItemJsonObj
                                    .getString("mBkgrdColor");

                        }
                        MainMenuBO mMainMenuBO = new MainMenuBO(mItemId,
                                mItemName, linkTypeId, linkTypeName,
                                Integer.parseInt(position), mItemImgUrl,
                                linkId, mBtnColor, mBtnFontColor, smBtnColor,
                                smBtnFontColor, level, mgrpFntColor,
                                sgrpFntColor, mgrpBgColor, sgrpBgColor,
                                mShapeName, mBkgrdImage, mBkgrdColor);
                        mainMenuUnsortedList.add(mMainMenuBO);

                    }

                }

                try {
                    ArrayList<BottomButtonBO> bottomButtonList = parseForBottomButton(
                            menuJsonObj, mLinkId, dateModified);
                    BottomButtonListSingleton.clearBottomButtonListSingleton();
                    BottomButtonListSingleton
                            .getListBottomButton(bottomButtonList);

                } catch (Exception e1) {

                    e1.printStackTrace();

                }

                Log.d(TAG, "Exiting jsonParseMenuResponse");
                return mainMenuUnsortedList;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        Log.d(TAG, "Exiting jsonParseMenuResponse");
        return mainMenuUnsortedList;
    }

    private void jsonParseTickerResponse(JSONObject xmlResponde) {
        csObjMenuAsync.creatingTickerTable(csSql, "ticker");
        JSONArray arrayTicker = xmlResponde
                .optJSONArray("arItemList");

        if (arrayTicker != null) {
            for (int count = 0; count < arrayTicker.length(); count++) {
                try {
                    JSONObject tickerObj = arrayTicker.getJSONObject(count);
                    if (tickerObj.optString("title") != null && !tickerObj.optString("title").isEmpty()) {
                        TickerObj tickerDetObj = new TickerObj();
                        tickerDetObj.setCatId(tickerObj.optString("catId"));
                        tickerDetObj.setCatName(tickerObj.optString("catName"));
                        tickerDetObj.setRowCount(tickerObj.optString("rowCount"));
                        tickerDetObj.setTitle(tickerObj.optString("title"));
                        tickerItem.add(tickerDetObj);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (isTempChanged != 0) {
            if (csObjMenuAsync != null
                    && !csObjMenuAsync.checkingLevels(csSql, "ticker",
                    mLinkId)) {
                csObjMenuAsync.insertingTickerValues(csSql, arrayTicker, mLinkId);

            }
        }

    }


    private ArrayList<BottomButtonBO> parseForBottomButton(
            JSONObject xmlResponde2, String mainMenuID2, String dateModified2)
            throws JSONException {
        Log.v("", "Check for bottom button ");
        csObjMenuAsync.creatingBottomTable(csSql, "bottomtable");
        ArrayList<BottomButtonBO> listBottomButton = new ArrayList<>();

        JSONArray arrayBottomButton = xmlResponde2
                .optJSONArray("arBottomBtnList");
        JSONObject bottomButtonObj = xmlResponde2
                .optJSONObject("arBottomBtnList");

        if (arrayBottomButton != null) {
            for (int i = 0; i < arrayBottomButton.length(); i++) {

                JSONObject bottomButton = arrayBottomButton.getJSONObject(i);
                if (bottomButton != null) {
                    String bottomBtnID = bottomButton.getString("bottomBtnID");
                    String bottomBtnImg = bottomButton
                            .getString("bottomBtnImg");
                    String bottomBtnImgOff = bottomButton
                            .getString("bottomBtnImgOff");
                    String btnLinkTypeID = bottomButton
                            .getString("btnLinkTypeID");

                    String btnLinkTypeName = "";
                    if (bottomButton.has("btnLinkTypeName")) {
                        btnLinkTypeName = bottomButton
                                .getString("btnLinkTypeName");
                    }

                    String position = bottomButton.getString("position");
                    String btnLinkID = "";
                    if (bottomButton.has("btnLinkID")) {
                        btnLinkID = bottomButton.getString("btnLinkID");

                    }

                    BottomButtonBO bottomButtonBO = new BottomButtonBO(
                            bottomBtnID, bottomBtnImg, bottomBtnImgOff,
                            btnLinkTypeID, btnLinkTypeName, position, btnLinkID);
                    listBottomButton.add(bottomButtonBO);
                }
            }

        } else {

            if (bottomButtonObj != null) {

                String bottomBtnID = bottomButtonObj.getString("bottomBtnID");
                String bottomBtnImg = bottomButtonObj.getString("bottomBtnImg");
                String bottomBtnImgOff = bottomButtonObj
                        .getString("bottomBtnImgOff");
                String btnLinkTypeID = bottomButtonObj
                        .getString("btnLinkTypeID");
                String btnLinkTypeName = "";
                if (bottomButtonObj.has("btnLinkTypeName")) {
                    bottomButtonObj.getString("btnLinkTypeName");
                }
                String position = bottomButtonObj.getString("position");
                String btnLinkID = "";
                if (bottomButtonObj.has("btnLinkID")) {
                    btnLinkID = bottomButtonObj.getString("btnLinkID");

                }

                BottomButtonBO bottomButtonBO = new BottomButtonBO(bottomBtnID,
                        bottomBtnImg, bottomBtnImgOff, btnLinkTypeID,
                        btnLinkTypeName, position, btnLinkID);
                listBottomButton.add(bottomButtonBO);
            }
        }

        if (isTempChanged != 0) {
            if (csObjMenuAsync != null
                    && !csObjMenuAsync.checkingLevels(csSql, "bottomtable",
                    mLinkId)) {
                csObjMenuAsync.insertingBottomValues(csSql, mainMenuID2,
                        dateModified2, arrayBottomButton, bottomButtonObj);

            } else {
                if (csObjMenuAsync != null
                        && !dateModified2.equals(csObjMenuAsync.strDateCreated)
                        || SortDepttNTypeScreen.isSubMenuSort) {
                    if (SortDepttNTypeScreen.isSubMenuSort) {
                        SortDepttNTypeScreen.isSubMenuSort = false;
                    }

                    csObjMenuAsync.updatingBottomValues(csSql, "linkId",
                            mLinkId, dateModified2, arrayBottomButton,
                            bottomButtonObj);
                }

            }

        }

        if (SortDepttNTypeScreen.isSubMenuSort) {
            SortDepttNTypeScreen.isSubMenuSort = false;
        }

        if (CityPreferencesScreen.bIsSendingTimeStamp) {
            CityPreferencesScreen.bIsSendingTimeStamp = false;
        }

        if (!listBottomButton.isEmpty()) {
            return listBottomButton;
        }
        return null;
    }

    private void startMenuActivity(String templateName,
                                   ArrayList<MainMenuBO> mainMenuUnsortedList) {

        Intent intent = null;

        if (templateName.equalsIgnoreCase(LIST_VIEW_TEMPLATE)
                || templateName
                .equalsIgnoreCase(LIST_VIEW_WITH_BANNER_TEMPLATE)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    TemplateListMenuScreen.class);

        } else if (templateName.equalsIgnoreCase(TWO_IMAGE)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    MainMenuTwoImageTemplate.class);

        } else if (templateName.equalsIgnoreCase(TWO_TILE_TEMPLATE)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    TwoTileTemplate.class);

        } else if (templateName.equalsIgnoreCase(ICONIC_GRID)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    MainMenuGridTemplate.class);
        } else if (templateName.equalsIgnoreCase(SQUARE_GRID)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    SquareGridTemplate.class);

        } else if (templateName.equalsIgnoreCase(ICONIC_3x3) || templateName.equalsIgnoreCase(SQUARE_3x3)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    ThreeCrossThreeTemplate.class);

        } else if (templateName.equalsIgnoreCase(RECTANGULAR_GRID) ||
                templateName.equalsIgnoreCase(GRID_4x4)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    RectangleGridTemplate.class);
            intent.putExtra("rectangularTemp", templateName);

        } else if (templateName.equalsIgnoreCase(FOUR_TILE_TEMPLATE)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    FourTileTemplate.class);
        } else if (templateName.equalsIgnoreCase(SQUARE_IMAGE_TEMPLATE)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    SquareImageTemplate.class);

        } else if (templateName.equalsIgnoreCase(TWO_COLUMN_MENU_TEMPLATE)) {
            if (noOfColumns != null) {
                if ("2".equals(noOfColumns)) {
                    intent = new Intent(mActivity.getApplicationContext(),
                            TwoColumnMenuTemplate.class);
                } else {
                    intent = new Intent(mActivity.getApplicationContext(),
                            SingleColumnMenuTemplate.class);
                }
            }

        } else if (templateName.equalsIgnoreCase(TWO_COLUMN_TAB_WITH_BANNER_AD)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    TwoColumnMenuTemplateWithBanner.class);
        } else if (templateName.equalsIgnoreCase(GROUPED_TAB)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    GroupedTabMenuScreen.class);
        } else if (templateName.equalsIgnoreCase(COMBO_TEMPLETE)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    ComboTemplateScreen.class);
        } else if (templateName.equalsIgnoreCase(GROUPED_TAB_WITH_IMAGE)) {
            intent = new Intent(mActivity.getApplicationContext(),
                    GroupedListViewTabMenuScreen.class);
        }

        if (CustomTitleBar.isSubMenuStart && intent != null) {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            CustomTitleBar.isSubMenuStart = false;
        }

        mainIDs.add(mainMenuID);

        if (intent != null) {
            if (chkloc) {
                intent.putExtra("chkloc", true);
            } else {
                intent.putExtra("chkloc", false);

            }
            intent.putParcelableArrayListExtra(
                    Constants.MENU_PARCELABLE_INTENT_EXTRA,
                    mainMenuUnsortedList);
            intent.putExtra(Constants.MENU_LEVEL_INTENT_EXTRA, level);
            intent.putExtra("mLinkId", mLinkId);
            intent.putExtra("mItemId", mItemId);
            intent.putExtra("mBkgrdColor", mBkgrdColor);
            intent.putExtra("mBkgrdImage", mBkgrdImage);
            intent.putExtra("smBkgrdColor", smBkgrdColor);
            intent.putExtra("smBkgrdImage", smBkgrdImage);
            intent.putExtra("smBtnColor", smBtnColor);
            intent.putExtra("smBtnFontColor", smBtnFontColor);
            intent.putExtra("templateBgColor", templateBgColor);

            // For Iconic buttons
            intent.putExtra("mFontColor", mFontColor);
            intent.putExtra("smFontColor", smFontColor);

            intent.putExtra("departFlag", departFlag);
            intent.putExtra("typeFlag", typeFlag);
            intent.putExtra("mBannerImg", mBannerImg);
            intent.putExtra("cityExpImgUlr", cityExpImgUlr);
            intent.putExtra(APP_ICON_LINK, appIconImg);
            intent.putExtra(APP_PLAYSTORE_LINK, appPlaystoreLInk);
            intent.putExtra("downLoadLink", appleStoreLink);

            // For Filters
            intent.putExtra("retAffCount", retAffCount);
            if (null != TempBkgrdImg) {
                intent.putExtra("TempBkgrdImg", TempBkgrdImg);
            }
            if (isDisplayLabel == 1) {
                intent.putExtra("isDisplayLabel", isDisplayLabel);
            }
            if (btnLblColor != null) {
                intent.putExtra("btnLblColor", btnLblColor);
            }
            if (btnBkgrdColor != null) {
                intent.putExtra("btnBkgrdColor", btnBkgrdColor);
            }
            intent.putExtra("rectangularTemp", templateName);

            if (null != retAffCount && "1".equals(retAffCount)) {
                intent.putExtra("retAffId", retAffId);
                intent.putExtra("retAffName", retAffName);

            }

            if (null != mainMenu && "mainMenu".equals(mainMenu)) {
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }

            int REQUESTCODE = 00;
            mActivity.startActivityForResult(intent, REQUESTCODE);

            if (SortDepttNTypeScreen.isSortDone) {
                MenuPropertiesActivity.activity.finish(); // Kill previous
                // subMenu
                SortDepttNTypeScreen.isSortDone = false;

                mainIDs.remove(mainIDs.size() - 1);
                menuNames.remove(menuNames.size() - 1);

                mActivity.finish();
            }

            if (chkloc) {
                if (mActivity != null && !mActivity.isFinishing()) {

                    mActivity.finish();
                }
            }

        }
    }

}
