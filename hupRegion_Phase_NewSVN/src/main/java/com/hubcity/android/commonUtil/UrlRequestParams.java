package com.hubcity.android.commonUtil;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;
import android.util.SparseArray;

import com.com.hubcity.model.CombinationalTempRequest;
import com.com.hubcity.model.CouponModel;
import com.com.hubcity.model.MapModel;
import com.com.hubcity.model.MyCouponModel;
import com.com.hubcity.model.UserInfoRequestModel;
import com.hubcity.android.businessObjects.UserSettingsField;
import com.hubcity.android.screens.CitySort;
import com.scansee.android.CouponsActivty;
import com.scansee.android.ScanSeeLocListener;
import com.scansee.newsfirst.NewsSideMenu;
import com.scansee.newsfirst.SideMenuModel;
import com.scansee.newsfirst.UpdateModel;
import com.scansee.newsfirst.model.SubPageModel;
import com.scansee.newsfirst.model.TileTemplateRequestModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class UrlRequestParams {

    private StringBuilder urlBuilder;


    public String getRetailerCupons(String retailLocationID, String retListId,
                                    String retailerId, String mainMenuId, String lastVisitedNo) {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<RetailerDetail> <userId>").append(getUid()).append("</userId>");

        if (retailLocationID != null) {
            postDataBuilder.append("<retailLocationId>").append(retailLocationID).append
                    ("</retailLocationId>");
        }

        if (retailerId != null) {
            postDataBuilder.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        postDataBuilder.append("<hubCitiId><![CDATA[").append(getHubCityId()).append
                ("]]></hubCitiId>");

        if (lastVisitedNo != null) {
            postDataBuilder.append("<lastVisitedNo>").append(lastVisitedNo).append
                    ("</lastVisitedNo>");
        }

        if (retListId != null) {
            postDataBuilder.append("<retListId>").append(retListId).append("</retListId>");
        }

        if (mainMenuId != null && !"".equals(mainMenuId)) {
            postDataBuilder.append("<mainMenuId>").append(mainMenuId).append("</mainMenuId>");
        }

        postDataBuilder.append("</RetailerDetail>");

        HubCityLogger.v("GET_THISLOCATION_SPECIALOFFERSPAGE Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getHotDealsByLocationAndProducts(String searchKey,
                                                   String type, String mItemId, String lowerLimit, String bottomBtnId) {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<ProductDetailsRequest> <userId>").append(getUid()).append
                ("</userId>");
        postDataBuilder.append("<type>").append(type).append("</type>");

        if (searchKey != null) {
            postDataBuilder.append("<searchKey>").append(searchKey).append("</searchKey>");
        }
        if (lowerLimit != null) {
            postDataBuilder.append("<lowerLimit>").append(lowerLimit).append("</lowerLimit>");
        }
      /*  if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }*/
        if (mItemId != null && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
        }
        if (bottomBtnId != null && !"".equals(bottomBtnId)) {
            postDataBuilder.append("<bottomBtnId>").append(bottomBtnId).append("</bottomBtnId>");
        }


        if (CommonConstants.LATITUDE != null
                && CommonConstants.LONGITUDE != null && !CommonConstants.LATITUDE.isEmpty()
                && !CommonConstants.LONGITUDE.isEmpty()) {
            postDataBuilder.append("<latitude>").append(CommonConstants.LATITUDE).append
                    ("</latitude>");
            postDataBuilder.append("<longitude>").append(CommonConstants.LONGITUDE).append
                    ("</longitude>");
        }


        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (!"".equals(Constants.getZipCode())) {
                postDataBuilder.append("<postalcode>").append(Constants.getZipCode()).append
                        ("</postalcode>");
            }
        } else {
            postDataBuilder.append("<postalcode>").append(Constants.getZipCode()).append
                    ("</postalcode>");
        }

        if (getHubCityId() != null) {
            postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }

        postDataBuilder.append("<platform>Android</platform>");
        postDataBuilder.append("</ProductDetailsRequest>");

        HubCityLogger.v("getProductDetail  Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getCouponCategories() {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<HotDealsDetails><userId>").append(getUid()).append("</userId>");

        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append
                ("</hubCitiId></HotDealsDetails>");

        HubCityLogger.v("getGetDeal details ", postDataBuilder.toString());
        return postDataBuilder.toString();

    }

    public JSONObject getNewsSearchParam(String searchKey, String
            isSideBar, String lowerLimit) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId", getUid());
            jsonObject.put("hubCitiId", getHubCityId());
            if (searchKey != null && !searchKey.equals("")) {
                jsonObject.put("searchKey", searchKey);
            }
            if (isSideBar != null && !isSideBar.equals("")) {
                jsonObject.put("isSideBar", isSideBar);
            }
            jsonObject.put("lowerLimit", lowerLimit);
            Log.d("GETNEWSSEARCHPARAMS", jsonObject.toString());
            return jsonObject;

        } catch (Exception e) {
            return null;
        }

    }

    public String hotDealClaimRedeem(String hotDealId) {
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<HotDealsListRequest><userId>").append(getUid()).append
                ("</userId>");
        postDataBuilder.append("<hotDealId>").append(hotDealId).append("</hotDealId>");
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        postDataBuilder.append("</HotDealsListRequest >");
        HubCityLogger.v("getGetDeal details ", postDataBuilder.toString());
        return postDataBuilder.toString();
    }

    public String getCoupnClaimRedeemExpired(String type, String lastvisitId,
                                             String searchKey) {
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ProductDetailsRequest><userId>").append(getUid()).append
                ("</userId>");

        postDataBuilder.append("<lowerLimit>").append(lastvisitId).append("</lowerLimit>");
        if (searchKey != null) {
            postDataBuilder.append("<searchKey>").append(searchKey).append("</searchKey>");
        }
        if (type != null) {
            postDataBuilder.append("<type>").append(type).append("</type>");
        }

        if (CommonConstants.LATITUDE != null
                && CommonConstants.LONGITUDE != null && !CommonConstants.LATITUDE.isEmpty()
                && !CommonConstants.LONGITUDE.isEmpty()) {
            postDataBuilder.append("<latitude>").append(CommonConstants.LATITUDE).append
                    ("</latitude>");
            postDataBuilder.append("<longitude>").append(CommonConstants.LONGITUDE).append
                    ("</longitude>");
        }

        if (getHubCityId() != null) {
            postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (!"".equals(Constants.getZipCode())) {
                postDataBuilder.append("<postalcode>").append(Constants.getZipCode()).append
                        ("</postalcode>");
            }
        } else {
            if (!Constants.getZipCode().isEmpty())
                postDataBuilder.append("<postalcode>").append(Constants.getZipCode()).append
                        ("</postalcode>");
        }
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }

        postDataBuilder.append("</ProductDetailsRequest>");

        HubCityLogger.v("getCoupnClaimRedeemExpired details ",
                postDataBuilder.toString());
        return postDataBuilder.toString();
    }

    public String getGetDeal(String hotDealId, String hotDealListId) {
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<HotDealsDetails><userId>").append(getUid()).append("</userId>");
        if (hotDealId != null) {
            postDataBuilder.append("<hotDealId>").append(hotDealId).append("</hotDealId>");
        }
        if (hotDealListId != null) {
            postDataBuilder.append("<hotDealListId>").append(hotDealListId).append
                    ("</hotDealListId>");
        }
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append
                ("</hubCitiId></HotDealsDetails>");

        HubCityLogger.v("getGetDeal details ", postDataBuilder.toString());
        return postDataBuilder.toString();

    }

    public String getEvent(String eventId, String eventListId) {
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<EventDetails><userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<eventId>").append(eventId).append("</hotDealId>");
        postDataBuilder.append("<eventsListId>").append(eventListId).append("</eventsListId>");
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append
                ("</hubCitiId></EventDetails>");

        HubCityLogger.v("getEvent details ", postDataBuilder.toString());
        return postDataBuilder.toString();

    }

    public JSONObject getFindCategoryPartners(String mItemId, String postalcode,
                                              String latitude, String longitude, String bottomBtnId,
                                              boolean isSubMenu, String radious, String cityIds) throws Exception {
        JSONObject jsonObject = new JSONObject();
//		jsonObject.put("userId", getUid());
        jsonObject.put("platform", "Android");
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            jsonObject.put("mainMenuId", Constants.getMainMenuId());
        }
        if (mItemId != null) {
            jsonObject.put("mItemId", mItemId);
        } else if (null != bottomBtnId && !"".equals(bottomBtnId)) {
            jsonObject.put("bottomBtnId", bottomBtnId);
        }
        if (radious != null && !radious.equals("")) {
            jsonObject.put("radius", radious);
        } else {
            jsonObject.put("radius", "50");
        }
        if (Properties.isRegionApp) {
            if (cityIds != null && !cityIds.equals("")) {
                jsonObject.put("cityIds", cityIds);
            }
        }
        jsonObject.put("hubCitiId", getHubCityId());
        jsonObject.put("levelFlag", isSubMenu);
        if (latitude != null && !"".equals(latitude)
                && !"null".equals(latitude)) {
            jsonObject.put("latitude", latitude);
        }
        if (longitude != null && !"".equals(longitude)
                && !"null".equals(longitude)) {
            jsonObject.put("longitude", longitude);
            if (!postalcode.isEmpty() && postalcode != null) {
                jsonObject.put("userPostalCode ", postalcode);
            }

        }
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {

            if (longitude == null || "".equals(longitude)) {
                if (postalcode != null && !postalcode.isEmpty()) {
                    jsonObject.put("postalCode", postalcode);
                }
            }
        } else {
            if (!Constants.getZipCode().isEmpty() && Constants.getZipCode() != null) {
                jsonObject.put("postalCode", Constants.getZipCode());
            }
        }

        return jsonObject;
    }

    public String createPriceFilterParam(String catIds, String radius,
                                         String cityIds, String latitude, String longitude, String mItemId,
                                         String mBottomId, String fName, String searchKey) throws Exception {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<MenuItem><userId>").append(getUid()).append("</userId>");

        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");


        if (latitude != null && !"".equals(latitude)) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (longitude != null && !"".equals(longitude)) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }


        if (catIds != null && !"".equals(catIds)) {
            postDataBuilder.append("<catId>").append(catIds).append("</catId>");
        }

        if (cityIds != null && !"".equals(cityIds)) {
            postDataBuilder.append("<cityIds>").append(cityIds).append("</cityIds>");
        }

        if (mItemId != null && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
        }
        if (mBottomId != null && !"".equals(mBottomId)) {
            postDataBuilder.append("<bottomBtnId>").append(mBottomId).append("</bottomBtnId>");
        }

        // Change tag name
        if (fName != null && !"".equals(fName)) {
            postDataBuilder.append("<fName>").append(fName).append("</fName>");
        }

        if (searchKey != null && !"".equals(searchKey)) {
            postDataBuilder.append("<searchKey>").append(searchKey).append("</searchKey>");
        }

        postDataBuilder.append("</MenuItem>");

        HubCityLogger.v("createAddisonFilterParam", postDataBuilder.toString());
        return postDataBuilder.toString();

    }


    public JSONObject getRetailerSearch(String mItemId, String lastVisitedNo,
                                        String latitude, String longitude, String radius, String searchKey,
                                        String parCatId, String postaCode, String bottomBtnId,
                                        String sortColumn, String subCatId, String filterId,
                                        String filterValueId, String locSpecials, String interests,
                                        String cityIds, String requestedTime) throws Exception

    {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId", getUid());
            if (parCatId != null) {
                jsonObject.put("catName", parCatId);
            }
            if (lastVisitedNo != null) {
                jsonObject.put("lastVisitedNo", lastVisitedNo);
            }
            if (latitude != null && !"".equals(latitude)) {
                jsonObject.put("latitude", latitude);
            }
            if (longitude != null && !"".equals(longitude)) {
                jsonObject.put("longitude", longitude);
            }
            if (!Constants.GuestLoginId
                    .equals(UrlRequestParams.getUid().trim())) {

                if (latitude == null) {
                    if (Constants.getZipCode() != null && !"".equals(Constants.getZipCode())) {
                        jsonObject.put("postalCode", Constants.getZipCode());
                    }
                }
            } else {
                if (Constants.getZipCode() != null && !"".equals(Constants.getZipCode())) {
                    jsonObject.put("postalCode", Constants.getZipCode());
                }
            }
            if (radius != null && !radius.equals("")) {
                jsonObject.put("radius", radius);
            } else {
                jsonObject.put("radius", "50");
            }
            if (searchKey != null) {
                jsonObject.put("searchKey", searchKey);
            }
            if (subCatId != null && !"".equals(subCatId)) {
                jsonObject.put("subCatIds", subCatId);
            }
            jsonObject.put("sortOrder", "ASC");

            if (Constants.getMainMenuId() != null
                    && !"".equals(Constants.getMainMenuId())) {
                jsonObject.put("mainMenuId", Constants.getMainMenuId());
            }
            if (mItemId != null && !"".equals(mItemId)) {
                jsonObject.put("mItemId", mItemId);
            }
            if (bottomBtnId != null && !"".equals(bottomBtnId)) {
                jsonObject.put("bottomBtnId", bottomBtnId);
            }
            if (sortColumn != null && !"".equals(sortColumn)) {
                jsonObject.put("sortColumn", sortColumn);
            }
            if (Properties.isRegionApp) {
                if (cityIds != null && !"".equals(cityIds)) {
                    jsonObject.put("cityIds", cityIds);
                }
            }
            // For Addison Filters
            if (filterId != null && !"".equals(filterId)) {
                jsonObject.put("filterId", filterId);
            }

            if (filterValueId != null && !"".equals(filterValueId)) {
                jsonObject.put("fValueId", filterValueId);
            }

            if (locSpecials != null && !"".equals(locSpecials)) {
                jsonObject.put("locSpecials", locSpecials);
            }

            if (interests != null && !"".equals(interests)) {
                jsonObject.put("interests", interests);
            }

            if (requestedTime != null && !"".equals(requestedTime)) {
                jsonObject.put("requestedTime", requestedTime);
            }

            jsonObject.put("hubCitiId", getHubCityId());

            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getRedeemCoupon(String couponId) {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        //noinspection StringConcatenationInsideStringBufferAppend
        postDataBuilder.append("<CouponDetails> <userId>").append(getUid()).append("</userId>");

        if (couponId != null) {
            postDataBuilder.append("<couponId>").append(couponId).append("</couponId>");
        }

        postDataBuilder.append("</CouponDetails>");

        HubCityLogger.v("getRedeemCoupon  Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getClaimCoupon(String couponId) {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<CLRDetails> <userId>").append(getUid()).append("</userId>");

        if (couponId != null) {
            postDataBuilder.append("<couponId>").append(couponId).append("</couponId>");
        }

        postDataBuilder.append("</CLRDetails>");

        HubCityLogger
                .v("getClaimCoupon  Post data", postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getCoupDiscounts(String productId, String retailerId,
                                   String lowerLimit) {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<CLRDetails> <userId>").append(getUid()).append("</userId>");

        if (productId != null) {
            postDataBuilder.append("<productId>").append(productId).append("</productId>");
        }

        if (retailerId != null) {
            postDataBuilder.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        if (lowerLimit != null) {
            postDataBuilder.append("<lowerLimit>").append(lowerLimit).append("</lowerLimit>");
        }

        if (getHubCityId() != null) {
            postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }

        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        postDataBuilder.append("</CLRDetails>");

        HubCityLogger.v("Coupon Discounts  Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getQualifyingCoupDetails(String couponId, String type,
                                           String couponListId) {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<CLRDetails> <userId>").append(getUid()).append("</userId>");

        if (couponId != null) {
            postDataBuilder.append("<couponId>").append(couponId).append("</couponId>");
        }

        if (couponListId != null) {
            postDataBuilder.append("<couponListId>").append(couponListId).append
                    ("</couponListId>");
        }

        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        if (getHubCityId() != null) {
            postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }

        if (type != null) {
            postDataBuilder.append("<type>").append(type).append("</type>");
        }

        postDataBuilder.append("</CLRDetails>");

        HubCityLogger.v("URL_GET_RET_HOT_DEALS  Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getCouponDetails(String couponId, String couponListId) {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<RetailerDetail> <userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");

        if (couponId != null) {
            postDataBuilder.append("<couponId>").append(couponId).append("</couponId>");
        }

        if (couponListId != null) {
            postDataBuilder.append("<couponListId>").append(couponListId).append
                    ("</couponListId>");
        }
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        postDataBuilder.append("</RetailerDetail>");

        HubCityLogger.v("URL_GET_RET_HOT_DEALS  Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getAllCoupons(String retailID, String lastvisitProdId,
                                String searchKey, String postalCode) throws Exception

    {
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ProductDetailsRequest><userId>").append(getUid()).append
                ("</userId>");
        postDataBuilder.append("<lowerLimit>").append(lastvisitProdId).append("</lowerLimit>");
        if (searchKey != null) {
            postDataBuilder.append("<searchKey>").append(searchKey).append("</searchKey>");
        }
        if (CommonConstants.LATITUDE != null
                && CommonConstants.LONGITUDE != null && !CommonConstants.LATITUDE.isEmpty()
                && !CommonConstants.LONGITUDE.isEmpty()) {
            postDataBuilder.append("<latitude>").append(CommonConstants.LATITUDE).append
                    ("</latitude>");
            postDataBuilder.append("<longitude>").append(CommonConstants.LONGITUDE).append
                    ("</longitude>");
        }
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (postalCode != null && !"".equals(postalCode)) {
                postDataBuilder.append("<postalcode>").append(postalCode).append("</postalcode>");
            }
        } else {
            if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                postDataBuilder.append("<postalcode>").append(Constants.getZipCode()).append
                        ("</postalcode>");
        }

        if (retailID != null) {
            postDataBuilder.append("<retailId>").append(retailID).append("</retailId>");
        }
        if (getHubCityId() != null) {
            postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }

        postDataBuilder.append("<busCatIds>" + 0 + "</busCatIds>");

        postDataBuilder.append("<platform>" + Constants.PLATFORM
                + "</platform></ProductDetailsRequest>");
        HubCityLogger.v("getAllcoupons URL Post data",
                postDataBuilder.toString());
        return postDataBuilder.toString();

    }

    public String getSpecialOffersLocations(String retailerId, String pageId)
            throws Exception {

        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<RetailerDetail> <userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId><![CDATA[").append(getHubCityId()).append
                ("]]></hubCitiId>");
        postDataBuilder.append("<retailerId>").append(retailerId).append("</retailerId>");
        postDataBuilder.append("<pageId>").append(pageId).append("</pageId>");
        postDataBuilder.append("<platform>" + Constants.PLATFORM
                + "</platform>");
        postDataBuilder.append("</RetailerDetail>");

        return postDataBuilder.toString();
    }

    public String getSpecialOffersDetail(String retailerId,
                                         String retailLocationId, String pageId) throws Exception {

        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<RetailerDetail> <userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<retailerId>").append(retailerId).append("</retailerId>");
        postDataBuilder.append("<retailLocationId>").append(retailLocationId).append
                ("</retailLocationId>");
        postDataBuilder.append("<pageId>").append(pageId).append("</pageId>");
        postDataBuilder.append("<hubCitiId><![CDATA[").append(getHubCityId()).append
                ("]]></hubCitiId>");
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }

        postDataBuilder.append("<platform>" + Constants.PLATFORM
                + "</platform>");

        postDataBuilder.append("</RetailerDetail>");

        return postDataBuilder.toString();
    }

    public String getProductDetail(String retailLocationId, String retListId,
                                   String retailerId, String lastVisitedNo, String mainMenuId)
            throws Exception {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<ProductDetailsRequest> <userId>").append(getUid()).append
                ("</userId>");
        if (retailLocationId != null) {
            postDataBuilder.append("<retailLocationId>").append(retailLocationId).append
                    ("</retailLocationId>");
        }

        if (lastVisitedNo != null) {
            postDataBuilder.append("<lastVisitedProductNo>").append(lastVisitedNo).append
                    ("</lastVisitedProductNo>");
        }
        if (retailerId != null) {
            postDataBuilder.append("<retailId>").append(retailerId).append("</retailId>");
        }
        postDataBuilder.append("<hubCitiId><![CDATA[").append(getHubCityId()).append
                ("]]></hubCitiId>");

        if (retListId != null) {
            postDataBuilder.append("<retListId>").append(retListId).append("</retListId>");
        }

        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        postDataBuilder.append("</ProductDetailsRequest>");

        HubCityLogger.v("getProductDetail  Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getRetHotdeals(String retailLocationId, String retListId,
                                 String retailerId, String lastVisitedNo) throws Exception {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<RetailerDetail> <userId>").append(getUid()).append("</userId>");

        if (retailerId != null) {
            postDataBuilder.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        if (retailLocationId != null) {
            postDataBuilder.append("<retailLocationId>").append(retailLocationId).append
                    ("</retailLocationId>");
        }

        if (lastVisitedNo != null) {
            postDataBuilder.append("<lastVisitedNo>").append(lastVisitedNo).append
                    ("</lastVisitedNo>");
        }

        postDataBuilder.append("<hubCitiId><![CDATA[").append(getHubCityId()).append
                ("]]></hubCitiId>");

        postDataBuilder.append("</RetailerDetail>");

        HubCityLogger.v("URL_GET_RET_HOT_DEALS  Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getSpecialOffersInfo(String retailLocationID,
                                       String retListId, String retailerId, String mainMenuId,
                                       String lastVisitedNo) throws Exception {
        urlBuilder = new StringBuilder();
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<RetailerDetail> <userId>").append(getUid()).append("</userId>");

        if (retailLocationID != null) {
            postDataBuilder.append("<retailLocationId>").append(retailLocationID).append
                    ("</retailLocationId>");
        }

        if (retailerId != null) {
            postDataBuilder.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        postDataBuilder.append("<hubCitiId><![CDATA[").append(getHubCityId()).append
                ("]]></hubCitiId>");

        if (lastVisitedNo != null) {
            postDataBuilder.append("<lastVisitedNo>").append(lastVisitedNo).append
                    ("</lastVisitedNo>");
        }

        if (retListId != null) {
            postDataBuilder.append("<retListId>").append(retListId).append("</retListId>");
        }

        if (mainMenuId != null && !"".equals(mainMenuId)) {
            postDataBuilder.append("<mainMenuId>").append(mainMenuId).append("</mainMenuId>");
        }

        postDataBuilder.append("</RetailerDetail>");

        HubCityLogger.v("GET_THISLOCATION_SPECIALOFFERSPAGE Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getRetSpecialsSalesInfo(String retailLocationID,
                                          String retailID) throws Exception {
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<ProductDetailsRequest> <userId>").append(getUid()).append
                ("</userId>");
        if (retailID != null) {
            postDataBuilder.append("<retailId>").append(retailID).append("</retailId>");
        }
        if (retailLocationID != null) {
            postDataBuilder.append("<retailLocationId>").append(retailLocationID).append
                    ("</retailLocationId>");
        }
        postDataBuilder.append("<hubCitiId><![CDATA[").append(getHubCityId()).append
                ("]]></hubCitiId>");
        postDataBuilder.append("</ProductDetailsRequest>");

        HubCityLogger.v("getRetSpecialsSalesInfo Post data",
                postDataBuilder.toString());
        return postDataBuilder.toString();
    }

    private StringBuilder postDataBuilder = null;

    public String getDepttAndType(String fName) throws Exception {
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<MenuItem><userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        if (MenuAsyncTask.mainIDs != null && !MenuAsyncTask.mainIDs.isEmpty()) {
            postDataBuilder.append("<menuId>").append(MenuAsyncTask.mainIDs.get(
                    MenuAsyncTask.mainIDs.size() - 1)).append("</menuId>");
        }

        postDataBuilder.append("<fName>").append(fName).append("</fName>");

        postDataBuilder.append("</MenuItem>");

        HubCityLogger
                .v("getDepttAndType Post data", postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getDept(String mItemId, String mBottomId, String retailerId,
                          String retailerLocationId) throws Exception {
        postDataBuilder = new StringBuilder();

        postDataBuilder.append("<MenuItem><userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");

        if (mItemId != null && !mItemId.isEmpty()) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");

        } else if (mBottomId != null && !mBottomId.isEmpty()) {
            postDataBuilder.append("<bottomBtnId>").append(mBottomId).append("</bottomBtnId>");
        }

        if (retailerId != null && !"".equals(retailerId)) {
            postDataBuilder.append("<retailId>").append(retailerId).append("</retailId>");
        }

        if (retailerLocationId != null && !"".equals(retailerLocationId)) {
            postDataBuilder.append("<retailLocationId>").append(retailerLocationId).append
                    ("</retailLocationId>");
        }

        postDataBuilder.append("</MenuItem>");

        HubCityLogger
                .v("getDepttAndType Post data", postDataBuilder.toString());

        return postDataBuilder.toString();
    }

    public String getRetailersSummary(String productId, String productListId,
                                      String scanTypeId, String postalcode, String latitude,
                                      String longitude, String retailID, String retailLocationID)
            throws Exception {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ProductDetailsRequest><userId>").append(getUid()).append
                ("</userId>");
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {

            if (postalcode != null && !postalcode.isEmpty()) {
                postDataBuilder.append("<postalcode>").append(postalcode).append("</postalcode>");
            }
        } else {
            if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                postDataBuilder.append("<postalcode>").append(Constants.getZipCode()).append
                        ("</postalcode>");
        }

        if (latitude != null && !latitude.isEmpty()) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (longitude != null && !longitude.isEmpty()) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }

        if (retailID != null) {

            postDataBuilder.append("<retailID>").append(retailID).append("</retailID>");
        }
        if (retailLocationID != null) {
            postDataBuilder.append("<retailLocationId>").append(retailLocationID).append
                    ("</retailLocationId>");
        }
        if (getHubCityId() != null) {
            postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }

        Log.v("", "MAIN IN RETAILER PRODUCT : " + Constants.getMainMenuId());
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        if (scanTypeId != null) {
            postDataBuilder.append("<scanTypeId>").append(scanTypeId).append("</scanTypeId>");
        }
        if (productListId != null) {
            postDataBuilder.append("<prodListId>").append(productListId).append("</prodListId>");
        }
        postDataBuilder.append("<productId>").append(productId).append
                ("</productId></ProductDetailsRequest>");
        HubCityLogger.v("getRetailersSummary Post data",
                postDataBuilder.toString());
        return postDataBuilder.toString();
    }

    @SuppressWarnings("static-access")
    public static String getUid() {
        SharedPreferences preferences = HubCityContext.getHubCityContext()
                .getSharedPreferences(Constants.PREFERENCE_HUB_CITY,
                        HubCityContext.getHubCityContext().MODE_PRIVATE);
        String s = preferences.getString(Constants.PREFERENCE_KEY_U_ID, "-1");

        return s;
    }

    public String getFindSmartSearch(String searchkey) throws Exception {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ProductDetail><userId>").append(getUid()).append("</userId>");
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");

        postDataBuilder.append("<searchkey><![CDATA[").append(searchkey).append
                ("]]></searchkey></ProductDetail>");

        HubCityLogger.v("getfindsmartsearch URL Post data",
                postDataBuilder.toString());
        return postDataBuilder.toString();

    }

    public String getFindProductSearchList(String searchkey, String parCatId,
                                           String lastVistedProductNo, String mainMenuid) throws Exception {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ProductDetail><searchkey><![CDATA[").append(searchkey).append
                ("]]></searchkey>");
        if (mainMenuid != null && !"".equals(mainMenuid)) {
            postDataBuilder.append("<mainMenuId>").append(mainMenuid).append("</mainMenuId>");
        }

        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<parCatId>").append(parCatId).append("</parCatId>");
        postDataBuilder.append("<lastVistedProductNo>").append(lastVistedProductNo).append
                ("</lastVistedProductNo></ProductDetail>");
        HubCityLogger.v("getfindproductsearch URL Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();

    }

    public String getFindProductSearch(String searchkey, String parCatId,
                                       String lastVistedProductNo) throws Exception {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ProductDetail><searchkey><![CDATA[").append(searchkey).append
                ("]]></searchkey>");
        postDataBuilder.append("<parCatId>").append(parCatId).append("</parCatId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<lastVistedProductNo>").append(lastVistedProductNo).append
                ("</lastVistedProductNo></ProductDetail>");

        HubCityLogger.v("getfindproductsearch URL Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();

    }

    public String getFindRetailerSearch(String searchkey, String parCatId,
                                        String lastVistedProductNo) throws Exception {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ProductDetail><searchkey><![CDATA[").append(searchkey).append
                ("]]></searchkey>");
        postDataBuilder.append("<parCatId>").append(parCatId).append("</parCatId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<lastVistedProductNo>").append(lastVistedProductNo).append
                ("</lastVistedProductNo></ProductDetail>");

        HubCityLogger.v("getfindproductsearch URL Post data",
                postDataBuilder.toString());

        return postDataBuilder.toString();

    }

    public String getScanNowHistory(String lastVisitedRecord) throws Exception {
        StringBuilder urlBuilder = new StringBuilder();
        String fetch_history_scannow = Properties.url_local_server
                + Properties.hubciti_version + "scannow/getscanhistory?";
        urlBuilder.append(fetch_history_scannow);
        urlBuilder.append(Constants.USER_ID + "=").append(getUid());
        urlBuilder.append("&" + Constants.TAG_LASTVISITEDRECORD + "=").append(lastVisitedRecord);
        urlBuilder.append("&" + Constants.HUBCITI_ID + "=").append(getHubCityId());
        HubCityLogger.v("getscannowproduct ", urlBuilder.toString());
        return urlBuilder.toString();
    }

    @SuppressWarnings("static-access")
    public static String getHubCityId() {
        SharedPreferences preferences = HubCityContext.getHubCityContext()
                .getSharedPreferences(Constants.PREFERENCE_HUB_CITY,
                        HubCityContext.getHubCityContext().MODE_PRIVATE);
        String s = preferences
                .getString(Constants.PREFERENCE_HUB_CITY_ID, "-1");
        return s;
    }

    public String fetchBarcodeProd(String barcode, String scanType,
                                   String scanLatitude, String scanLongitude) throws Exception {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ProductDetail>");
        if (Constants.getDeviceId() != null) {
            postDataBuilder.append("<deviceId>").append(Constants.getDeviceId()).append
                    ("</deviceId>");
        }
        if (barcode != null) {
            postDataBuilder.append("<barCode>" + "<![CDATA[").append(barcode).append("]]>  ")
                    .append("</barCode>");
        }
        if (scanType != null) {
            postDataBuilder.append("<scanType>").append(scanType).append("</scanType>");
        }
        if (getUid() != null) {
            postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        }
        if (getHubCityId() != null) {
            postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }

        if (scanLatitude != null && !scanLatitude.isEmpty()) {
            postDataBuilder.append("<scanLatitude>").append(scanLatitude).append
                    ("</scanLatitude>");
        }

        if (scanLongitude != null && !scanLongitude.isEmpty()) {
            postDataBuilder.append("<scanLongitude>").append(scanLongitude).append
                    ("</scanLongitude>");
        }

        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }

        postDataBuilder.append("</ProductDetail>");

        HubCityLogger.v("fetchBarcodeProd URL Post data",
                postDataBuilder.toString());
        return postDataBuilder.toString();
    }

    private static final String GOOGLE_GET_GEOCODE = "http://maps.google" +
            ".com/maps/api/geocode/xml?";

    public String getHotDealsAroundYou(String category, String latitude,
                                       String longitude, String lastVisitedProductNo, String cityId,
                                       String searchItem, String mItemId, String mBottomId, String zipcode)
            throws Exception {
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<HotDealsListRequest><userId>").append(getUid()).append
                ("</userId>");

        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<category>").append(category).append("</category>");
        if (latitude != null && !latitude.isEmpty()) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (longitude != null) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {


            if (zipcode != null && !"".equals(zipcode)) {
                postDataBuilder.append("<zipcode>").append(zipcode).append("</zipcode>");
            }

        }

        if (searchItem != null) {
            postDataBuilder.append("<searchItem>").append(searchItem).append("</searchItem>");
        }
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            postDataBuilder.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }

        if (mItemId != null && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");

        } else if (mBottomId != null && !"".equals(mBottomId)) {
            postDataBuilder.append("<bottomBtnId>").append(mBottomId).append("</bottomBtnId>");
        }

        postDataBuilder.append("<lastVisitedProductNo>").append(lastVisitedProductNo).append
                ("</lastVisitedProductNo></HotDealsListRequest>");

        return postDataBuilder.toString();
    }

    public String getHotdealToDelete(String hotDealId, String hDInterested) {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<HotDealsListRequest><userId>").append(getUid()).append
                ("</userId>");
        postDataBuilder.append("<hotDealId>").append(hotDealId).append("</hotDealId>");
        postDataBuilder.append("<hDInterested>" + 0 + "</hDInterested>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append
                ("</hubCitiId></HotDealsListRequest>");

        HubCityLogger.v("getHotdealToDelete details ",
                postDataBuilder.toString());
        return postDataBuilder.toString();

    }

    public String getGeoCode(String latitude, String longitude)
            throws Exception {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(GOOGLE_GET_GEOCODE);
        urlBuilder.append(CommonConstants.TAG_GOOGLE_LATLNG + "=").append(latitude).append(",")
                .append(longitude);
        urlBuilder.append(CommonConstants.AMPERSAND
                + CommonConstants.TAG_GOOGLE_SENSOR + "=false");
        HubCityLogger.v("getGooglePinCode", urlBuilder.toString());
        return urlBuilder.toString();
    }

    public String updateAccount(String userId,

                                SparseArray<UserSettingsField> userInfo, HashMap<String, String> fieldValues)
            throws Exception {
        postDataBuilder = new StringBuilder();
        postDataBuilder.append("<UserDetails>");
        if (!"-1".equals(getUid())) {
            postDataBuilder.append(getTag(Constants.USER_ID, getUid()));
        }
        if (Constants.getDeviceId() != null) {
            postDataBuilder.append("<deviceId>").append(Constants.getDeviceId()).append
                    ("</deviceId>");
        }
        UserSettingsField field;
        String value;
        for (int i = 0; i < userInfo.size(); i++) {
            field = userInfo.valueAt(i);
            value = fieldValues.get(field.getLabel());
            if (field.getTag() != null) {
                if (value != null && value.length() > 0) {
                    if ("dob".equalsIgnoreCase(field.getTag())) {
                        if (value.contains("-")) {
                            String dob[] = value.split("-");
                            postDataBuilder.append(getTag(field.getTag(),
                                    dob[0] + (" ") + dob[1] + (", ") + (dob[2])
                                            + ("")));
                        } else {
                            postDataBuilder
                                    .append(getTag(field.getTag(), value));
                        }
                    } else {
                        postDataBuilder.append(getTag(field.getTag(), value));
                    }
                }
            }
        }

        postDataBuilder.append("</UserDetails>");

        return postDataBuilder.toString();
    }

    public JSONObject getRetailersInfoJson(String preferredRadius, String zipcode,
                                           String latitude, String longitude, String lastVisitedRecord,
                                           String gpsEnabled, String mainMenuId, String locOnMap,
                                           String sortColumn, String sortOrder, String bottomBtnId,
                                           String mItemId, String requestedTime) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", getUid());
            jsonObject.put("hubCitiId", getHubCityId());

            if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                if (preferredRadius != null) {
                    jsonObject.put("preferredRadius", preferredRadius);
                }
            }

            if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                if (zipcode != null && !zipcode.equalsIgnoreCase("")) {
                    jsonObject.put("zipcode", zipcode);
                }
            } else {
                if (zipcode != null && !zipcode.equalsIgnoreCase("")) {
                    jsonObject.put("zipcode", Constants.getZipCode());
                }
            }

            if (latitude != null && !latitude.isEmpty()) {
                jsonObject.put("latitude", latitude);
            }
            if (longitude != null) {
                jsonObject.put("longitude", longitude);
            }


            if (lastVisitedRecord != null) {
                jsonObject.put("lastVisitedRecord", lastVisitedRecord);
            }
            if (gpsEnabled != null) {
                jsonObject.put("gpsEnabled", gpsEnabled);
            }
            if (mainMenuId != null && !"".equals(mainMenuId)) {
                jsonObject.put("mainMenuId", mainMenuId);
            }
            if (locOnMap != null) {
                jsonObject.put("locOnMap", locOnMap);
            }
            if (mItemId != null) {
                jsonObject.put("mItemId", mItemId);
            }

            if (sortColumn != null) {
                jsonObject.put("sortColumn", sortColumn);
            }

            if (sortOrder != null) {
                jsonObject.put("sortOrder", sortOrder);
            }

            if (bottomBtnId != null) {
                jsonObject.put("bottomBtnId", bottomBtnId);
            }
            jsonObject.put("platform", "Android");
            jsonObject.put("requestedTime", requestedTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String createCitiExpeienceMenuUrlParameter(String citiExpId,
                                                      String catIds, String lowerLimit, String mItemId, String platform) {

        return "<ThisLocationRequest>" + "<userId><![CDATA[" + getUid() + "]]></userId>" +
                "<hubCitiId><![CDATA[" + Constants.HUB_CITY_ID + "]]></hubCitiId>" +
                "<citiExpId><![CDATA[" + citiExpId + "]]></citiExpId>" + "<catIds><![CDATA[" +
                catIds + "]]></catIds>" + "<lowerLimit><![CDATA[" + lowerLimit +
                "]]></lowerLimit>" + "<mItemId><![CDATA[" + mItemId + "]]></mItemId>" +
                "<platform><![CDATA[" + platform + "]]></platform>" + "</ThisLocationRequest>";

    }

    public String createAlertListParameter(String mItemId, String platform,
                                           String lowerLimit) throws NameNotFoundException {

        return "<MenuItem>" + "<userId>" + getUid() + "</userId>" + "<hubCitiId>" + getHubCityId()
                + "</hubCitiId>" + "<mItemId>" + mItemId + "</mItemId>" + "<platform>" + platform
                + "</platform>" + "<lowerLimit>" + lowerLimit + "</lowerLimit></MenuItem>";
    }

    public String hubcitispedeals() throws NameNotFoundException {

        return "<ProductDetailsRequest>" + "<userId>" + getUid() + "</userId>" + "<hubCitiId>" +
                getHubCityId() + "</hubCitiId></ProductDetailsRequest>";
    }

    // To get FAQ flag
    public String createFAQsFlagParameter() {

        return "<UserDetails>" + "<userId>" + getUid() + "</userId>" + "<hubCitiId>" +
                getHubCityId() + "</hubCitiId>" + "</UserDetails>";
    }

    public String createFundraiserLocations(String fundId, String latitude,
                                            String longitude) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<Fundraiser>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");

        urlParameters.append("<fundId>").append(fundId).append("</fundId>");

        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (!Constants.getZipCode().isEmpty() && latitude == null
                    && longitude == null) {
                urlParameters.append("<postalCode>").append(Constants.getZipCode()).append
                        ("</postalCode>");
            }
        } else {
            if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                urlParameters.append("<postalCode>").append(Constants.getZipCode()).append
                        ("</postalCode>");
        }


        if (latitude != null && !latitude.isEmpty()) {
            urlParameters.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (longitude != null) {
            urlParameters.append("<longitude>").append(longitude).append("</longitude>");
        }


        urlParameters.append("</Fundraiser>");

        return urlParameters.toString();
    }

    public JSONObject createEventsListParameter(String mItemId, String lowerLimit,
                                                String sortBy, String bottomBtnId, String postalCode,
                                                String latitude, String longitude, String catId, String retailerId,
                                                String retListId, String retailLocationId, String fundId,
                                                String menuId, String evtDate, String cityIds, String listType)
            throws NameNotFoundException, JSONException {
        JSONObject mainJsonObject = new JSONObject();
        mainJsonObject.put("userId", getUid());
        mainJsonObject.put("hubCitiId", getHubCityId());

        if (listType != null && !"".equals(listType)) {
            mainJsonObject.put("listType", listType);
        }
        if (mItemId != null && !"".equals(mItemId)) {
            mainJsonObject.put("mItemId", mItemId);
        }
        if (bottomBtnId != null && !"".equals(bottomBtnId)) {
            mainJsonObject.put("bottomBtnId", bottomBtnId);
        }

        if (retailerId != null && !"".equals(retailerId)) {
            mainJsonObject.put("retailId", retailerId);
        }

        if (retailLocationId != null && !"".equals(retailLocationId)) {
            mainJsonObject.put("retailLocationId", retailLocationId);
        }

        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            mainJsonObject.put("mainMenuId", Constants.getMainMenuId());
        }
        mainJsonObject.put("platform", Constants.PLATFORM);

        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (Constants.getZipCode().isEmpty() && latitude == null
                    && longitude == null) {
                mainJsonObject.put("postalCode", Constants.getZipCode());
            }
        } else {
            if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                mainJsonObject.put("postalCode", Constants.getZipCode());

        }

        mainJsonObject.put("sortBy", sortBy);
        if (fundId != null && !"".equals(fundId)) {
            mainJsonObject.put("fundId", fundId);
        }

        if (cityIds != null && !"".equals(cityIds)) {
            mainJsonObject.put("cityIds", cityIds);
        }

        String isRegion;
        if (Properties.isRegionApp) {
            isRegion = "1";
        } else {
            isRegion = "0";
        }

        mainJsonObject.put("isRegApp", isRegion);

        if (latitude != null && !latitude.isEmpty()) {
            mainJsonObject.put("latitude", latitude);
        }
        if (longitude != null) {
            mainJsonObject.put("longitude", longitude);
        }

        if (catId != null && !"".equals(catId) && !"0".equals(catId)) {
            mainJsonObject.put("catIds", catId);
        }

        if (evtDate != null && !"".equals(evtDate)) {
            mainJsonObject.put("evtDate", evtDate);
        }

        mainJsonObject.put("lowerLimit", lowerLimit);
        return mainJsonObject;
    }

    public JSONObject createbandSummary(String bandId, double latitude, double longitude)
            throws NameNotFoundException, JSONException {
        String mainMenuId = Constants.getMainMenuId();
        JSONObject mainJsonObject = new JSONObject();
        mainJsonObject.put("userId", getUid());
        mainJsonObject.put("hubCitiId", getHubCityId());
        if (bandId != null && !"".equals(bandId)) {
            mainJsonObject.put("bandID", bandId);
        }

        if (latitude != 0) {
            mainJsonObject.put("latitude", latitude);
        }
        if (longitude != 0) {
            mainJsonObject.put("longitude", longitude);
        }
        if (mainMenuId != null && !"".equals(mainMenuId)) {
            mainJsonObject.put("mainMenuId", mainMenuId);
        }

        return mainJsonObject;
    }

    // for bandEvent List
    public JSONObject createShowMapParameter(int lowerLimit, String evtTypeID, double latitude,
                                             double longitude, String postalCode, int bandId,
                                             String radius, String sortColumn, String sortOrder, String catIds, String eventDate, String cityIds, String mItemId)
            throws NameNotFoundException, JSONException {
        JSONObject mainJsonObject = new JSONObject();

        mainJsonObject.put("userId", getUid());
        mainJsonObject.put("hubCitiId", getHubCityId());
        if (eventDate != null && !"".equals(eventDate)) {
            mainJsonObject.put("evtDate", eventDate);
        }
        if (mItemId != null && !"".equals(mItemId)) {
            mainJsonObject.put("mItemId", mItemId);
        }
        if (evtTypeID != null && !"".equals(evtTypeID)) {
            mainJsonObject.put("evtTypeID", evtTypeID);
        }
        //sortBy only in event map screen of austin
        if (sortColumn != null && !"".equals(sortColumn)) {
            mainJsonObject.put("sortBy", sortColumn);
        }
        if (sortOrder != null && !"".equals(sortOrder)) {
            mainJsonObject.put("sortOrder", sortOrder);
        }
        if (catIds != null && !"".equals(catIds)) {
            mainJsonObject.put("catIds", catIds);
        }

        if (postalCode != null && !postalCode.isEmpty()) {
            mainJsonObject.put("postalCode", postalCode);
        }
        if (latitude != 0) {
            mainJsonObject.put("latitude", latitude);
        }
        if (longitude != 0) {
            mainJsonObject.put("longitude", longitude);
        }

        mainJsonObject.put("lowerLimit", lowerLimit);
        if (bandId != 0) {
            mainJsonObject.put("bandId", bandId);
        }
        if (cityIds != null && !cityIds.equals("")) {
            mainJsonObject.put("cityIds", cityIds);
        }
        if (radius != null && !radius.equals("")) {
            mainJsonObject.put("radius", radius);
        } else {
            mainJsonObject.put("radius", "50");
        }
        return mainJsonObject;

    }

    public JSONObject createDealsFundraiserJsonParameter(String lowerLimit,
                                                         String groupBy, String sortBy, String searchKey, String deptId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", getUid());
            jsonObject.put("hubCitiId", getHubCityId());
            String mainMenuId = Constants.getMainMenuId();
            if (mainMenuId != null && !"".equals(mainMenuId)) {
                jsonObject.put("mainMenuId", mainMenuId);
            }
            jsonObject.put("lowerLimit", lowerLimit);
            jsonObject.put("sortOrder", "ASC");
            jsonObject.put("groupBy", groupBy);
            jsonObject.put("sortBy", sortBy);
            if (searchKey != null && !"".equals(searchKey)) {
                jsonObject.put("searchKey", searchKey);
            }

            if ("city".equals(sortBy) && CitySort.getSelectedCities() != null
                    && !"".equals(CitySort.getSelectedCities())) {
                jsonObject.put("cityIds", CitySort.getSelectedCities());
            }

            if (deptId != null && !"".equals(deptId)) {
                jsonObject.put("departmentId", deptId);
            }

            jsonObject.put("platform", Constants.PLATFORM);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public JSONObject createFundraiserEventsListJsonParameter(String mItemId,
                                                              String platform, String lowerLimit, String groupBy, String sortBy,
                                                              String bottomBtnId, String postalCode, String latitude,
                                                              String longitude, String catId, String retailerId,
                                                              String retListId, String retailLocationId, String deptId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", getUid());
            jsonObject.put("hubCitiId", getHubCityId());
            if (mItemId != null && !"".equals(mItemId)) {
                jsonObject.put("mItemId", mItemId);
            }

            if (bottomBtnId != null && !"".equals(bottomBtnId)) {
                jsonObject.put("bottomBtnId", bottomBtnId);
            }

            // @Rekha: Event changes
            if (retailerId != null && !"".equals(retailerId)) {
                jsonObject.put("retailId", retailerId);
            }

            if (retailLocationId != null && !"".equals(retailLocationId)) {
                jsonObject.put("retailLocationId", retailLocationId);
            }

            if (deptId != null && !"".equals(deptId)) {
                jsonObject.put("departmentId", deptId);
            }

            jsonObject.put("platform", platform);
            if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                if (!Constants.getZipCode().isEmpty() && latitude == null
                        && longitude == null) {
                    jsonObject.put("postalCode", Constants.getZipCode());
                }
            } else {
                if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                    jsonObject.put("postalCode", Constants.getZipCode());
            }

            jsonObject.put("sortBy", sortBy);

            if (groupBy != null && !"".equals(groupBy)) {
                jsonObject.put("groupBy", groupBy);
            }

            if ("city".equals(sortBy) && !"".equals(CitySort.getSelectedCities())) {
                jsonObject.put("cityIds", CitySort.getSelectedCities());
            }

            String isRegion;
            if (Properties.isRegionApp) {
                isRegion = "1";
            } else {
                isRegion = "0";
            }

            jsonObject.put("isRegApp", isRegion);

            if (latitude != null && !latitude.isEmpty()) {
                jsonObject.put("latitude", latitude);
            }
            if (longitude != null && !longitude.isEmpty()) {
                jsonObject.put("longitude", longitude);
            }
            if (catId != null) {
                jsonObject.put("catId", catId);
            }
            jsonObject.put("lowerLimit", lowerLimit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String createEventsCatListParameter(String mItemId,
                                               String bottomBtnId) {

        StringBuilder urlParams = new StringBuilder();
        urlParams.append("<MenuItem>");
        urlParams.append("<userId>").append(getUid()).append("</userId>");
        urlParams.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");

        if (mItemId != null && !"".equals(mItemId)) {
            urlParams.append("<mItemId>").append(mItemId).append("</mItemId>");
        }

        if (bottomBtnId != null && !"".equals(bottomBtnId)) {
            urlParams.append("<bottomBtnId>").append(bottomBtnId).append("</bottomBtnId>");
        }

        urlParams.append("</MenuItem>");

        return urlParams.toString();
    }

    public String createHotelListParameter(String eventId, String platform,
                                           String lastVisitedRecord, String mainMenuId)

            throws NameNotFoundException {

        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ThisLocationRequest>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        urlParameters.append("<eventId>").append(eventId).append("</eventId>");
        urlParameters.append("<lastVisitedRecord>").append(lastVisitedRecord).append
                ("</lastVisitedRecord>");
        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            urlParameters.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }

        urlParameters.append("</ThisLocationRequest>");
        return urlParameters.toString();
    }

    public String createHotelDetailParameter(String eventId, String retListId,
                                             String retailerLocid) throws NameNotFoundException {

        return "<Retailer>" + "<userId>" + getUid() + "</userId>" + "<hubCitiId>" + getHubCityId()
                + "</hubCitiId>" + "<eventId>" + eventId + "</eventId>" + "<retListId>" +
                retListId + "</retListId>" + "<retailLocationId>" + retailerLocid +
                "</retailLocationId></Retailer>";
    }

    public String getSpecialOfferDisp(String lowerLimit, String latitude,
                                      String longitude) throws NameNotFoundException {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ThisLocationRequest>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<lowerLimit>").append(lowerLimit).append("</lowerLimit>");

        if (latitude != null && !latitude.isEmpty()) {
            urlParameters.append("<latitude>").append(latitude).append("</latitude>");
        }

        if (longitude != null) {
            urlParameters.append("<longitude>").append(longitude).append("</longitude>");
        }


        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (!Constants.getZipCode().isEmpty()) {
                urlParameters.append("<postalCode>").append(Constants.getZipCode()).append
                        ("</postalCode>");
            }

        } else {
            if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                urlParameters.append("<postalCode>").append(Constants.getZipCode()).append
                        ("</postalCode>");
        }

        urlParameters.append("<hubCitiId>").append(getHubCityId()).append
                ("</hubCitiId></ThisLocationRequest>");

        return urlParameters.toString();
    }

    public String getSpecialOfferList(String lastVisitedNo,
                                      String retailLocationId, String retailerId, String mainMenuId)
            throws NameNotFoundException {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<RetailerDetail>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<lastVisitedNo>").append(lastVisitedNo).append("</lastVisitedNo>");

        if (retailLocationId != null) {
            urlParameters.append("<retailLocationId>").append(retailLocationId).append
                    ("</retailLocationId>");
        }

        if (retailerId != null) {
            urlParameters.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            urlParameters.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }
        urlParameters.append("<hubCitiId>").append(getHubCityId()).append
                ("</hubCitiId></RetailerDetail>");

        return urlParameters.toString();
    }

    public String createLogInUrlParameter(String userNameVal, String passwordVal)
            throws NameNotFoundException {

        return "<UserDetails>" + "<userName><![CDATA[" + userNameVal + "]]></userName>" +
                "<password><![CDATA[" + passwordVal + "]]></password>" + "<appVersion><![CDATA["
                + Constants.getAppVersion() + "]]></appVersion>" + "<deviceId><![CDATA[" +
                Constants.getDeviceId() + "]]></deviceId>" + "<platform><![CDATA[" + Constants
                .PLATFORM
                + "]]></platform>" + "<hubCitiKey><![CDATA[" + Properties.HUBCITI_KEY +
                "]]></hubCitiKey></UserDetails>";
    }

    public String setregIdInServer(String strregid) {

        return "<UserRegistrationInfo>" + "<userTokenID><![CDATA[" + strregid +
                "]]></userTokenID>" + "<platform>" + "Android" + "</platform>" +
                "<deviceID><![CDATA[" + Constants.getDeviceId() + "]]></deviceID>" +
                "<hcKey><![CDATA[" + Properties.HUBCITI_KEY + "]]></hcKey></UserRegistrationInfo>";
    }

    public String getUserZipcode() throws Exception {
        urlBuilder = new StringBuilder();
        if (UrlRequestParams.getUid() != null) {
            urlBuilder.append(UrlRequestParams.getUid());
        }
        HubCityLogger.v("getUserZipcode URL", urlBuilder.toString());
        return urlBuilder.toString();
    }

    public String getMainMenuId(String mItemId) {
        StringBuilder urlParameters = new StringBuilder();
        if (getUid() != null) {
            urlParameters.append("<MenuItem><userId>").append(getUid()).append("</userId>");
        }
        if (getHubCityId() != null) {
            urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }
        if (mItemId != null) {
            urlParameters.append("<mItemId>").append(mItemId).append("</mItemId>");
        }
        urlParameters.append("<platform>" + Constants.PLATFORM
                + "</platform></MenuItem>");
        return urlParameters.toString();
    }

    public String createGetRetailerParameter(String retailerId,
                                             String retailLocationId, String retListId, String mainMenuId,
                                             String scanTypeId, String accZipcode, String latitude,
                                             String longitude, String appSiteId) throws NameNotFoundException {

        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<RetailerDetail>");

        if (getUid() != null) {
            urlParameters.append("<userId>").append(getUid()).append("</userId>");
        }

        if (retailerId != null) {
            urlParameters.append("<retailerId>").append(retailerId).append("</retailerId>");
        }
        if (retailLocationId != null) {
            urlParameters.append("<retailLocationId>").append(retailLocationId).append
                    ("</retailLocationId>");

        }
        if (retListId != null) {
            urlParameters.append("<retListId>").append(retListId).append("</retListId>");
        }

        if (Constants.getMainMenuId() != null
                && !"".equals(Constants.getMainMenuId())) {
            urlParameters.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }

        if (latitude != null && !latitude.isEmpty()) {

            urlParameters.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (longitude != null && !longitude.isEmpty()) {
            urlParameters.append("<longitude>").append(longitude).append("</longitude>");

        }

        urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {

            if (accZipcode != null && !accZipcode.isEmpty()) {

                urlParameters.append("<postalCode>").append(accZipcode).append("</postalCode>");
            }
        } else {
            if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                urlParameters.append("<postalCode>").append(Constants.getZipCode()).append
                        ("</postalCode>");
        }

        if (scanTypeId != null) {
            urlParameters.append("<scanTypeId>").append(scanTypeId).append("</scanTypeId>");
        }

        if (appSiteId != null) {
            urlParameters.append("<appSiteId>").append(appSiteId).append("</appSiteId>");
        }
        urlParameters.append("</RetailerDetail>");

        return urlParameters.toString();
    }

    // @Beena : For Anything Page user Tracking
    public String createUserTrackngAnythngPageParamater(String retDetailsId,
                                                        String retListId, String anythingPageListId,
                                                        String anythingPageClick) {
        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<UserTrackingData>");
        if (retDetailsId != null) {
            urlParameters.append("<retDetailsId>").append(retDetailsId).append("</retDetailsId>");
        }
        if (retListId != null) {
            urlParameters.append("<retListId>").append(retListId).append("</retListId>");
        }
        if (anythingPageListId != null) {
            urlParameters.append("<anythingPageListId>").append(anythingPageListId).append
                    ("</anythingPageListId>");
        }
        if (anythingPageClick != null && anythingPageClick.equals("directions")) {
            urlParameters.append("<getDirClick>" + true + "</getDirClick>");
        } else if (anythingPageClick != null
                && anythingPageClick.equals("call")) {
            urlParameters.append("<callStoreClick>" + true
                    + "</callStoreClick>");
        } else if (anythingPageClick != null
                && anythingPageClick.equals("browser")) {
            urlParameters.append("<browseWebClick>" + true
                    + "</browseWebClick>");
        } else if (anythingPageClick != null
                && anythingPageClick.equals("anypage")) {
            urlParameters.append("<anythingPageClick>" + true
                    + "</anythingPageClick>");
        }

        urlParameters.append("</UserTrackingData>");
        return urlParameters.toString();

    }

    public String createUserCatUrlParameter() throws NameNotFoundException {

        return "<UserDetails>" + "<userId>" + getUid() + "</userId>" + "<hubCitiId>" +
                getHubCityId() + "</hubCitiId>" + "</UserDetails>";
    }

    public String createUpdatePasswordUrlParameter(String newPwd)
            throws NameNotFoundException {
        return "<UserRegistrationInfo>" + "<userId><![CDATA[" + getUid() + "]]></userId>"
                + "<hubCitiKey><![CDATA[" + Properties.HUBCITI_KEY + "]]></hubCitiKey>" +
                "<password><![CDATA[" + newPwd + "]]></password>" + "</UserRegistrationInfo>";
    }

    // @Beena:For city preferences

    public String createCityCatUrlParameter() throws NameNotFoundException {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<UserDetails>");
        if (!getUid().equalsIgnoreCase("-1")) {
            urlParameters.append("<userId>").append(getUid()).append("</userId>");
        }
        urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        urlParameters.append("</UserDetails>");

        return urlParameters.toString();
    }

    public String createSaveCityPrefParameter(ArrayList<String> cityIds,
                                              boolean isCitiesSelected) throws Exception {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<UserDetails>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        if (isCitiesSelected) {
            urlParameters.append("<cityIds>");

            int count = 0;
            for (String city : cityIds) {
                count++;
                urlParameters.append(city);
                if (count < cityIds.size()) {
                    urlParameters.append(",");
                }
            }
            urlParameters.append("</cityIds>");
        }
        urlParameters.append("</UserDetails>");

        return urlParameters.toString();
    }

    // End of save for city preferences

    public String createSavePrefParameter(ArrayList<String> catIds)
            throws Exception {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<CategoryDetails>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<catIds>");

        int count = 0;
        for (String cat : catIds) {
            count++;
            urlParameters.append(cat);
            if (count < catIds.size()) {
                urlParameters.append(",");
            }
        }

        urlParameters.append("</catIds>");
        urlParameters.append("</CategoryDetails>");

        return urlParameters.toString();
    }

    public String createSavePrefParameterforLocation(ArrayList<String> catIds,
                                                     ArrayList<String> subCatIds) throws Exception {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<CategoryDetails>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");

        int count = 0;

        if (catIds != null && !catIds.isEmpty()) {
            urlParameters.append("<catIds>");

            for (String cat : catIds) {
                count++;
                urlParameters.append(cat);
                if (count < catIds.size()) {
                    urlParameters.append(",");
                }
            }

            urlParameters.append("</catIds>");
        }

        if (subCatIds != null && !subCatIds.isEmpty()) {
            urlParameters.append("<subCatIds>");

            for (int i = 0; i < subCatIds.size(); i++) {

                urlParameters.append(subCatIds.get(i));

                if ((i != subCatIds.size() - 1)
                        && !subCatIds.get(i).equals("|")
                        && !subCatIds.get(i + 1).equals("|")) {
                    urlParameters.append(",");
                }
            }

            urlParameters.append("</subCatIds>");
        }
        urlParameters.append("</CategoryDetails>");

        return urlParameters.toString();
    }

    public JSONObject createMainMenuJsonParameter(int linkId, int level, int hubCitiId, String
            sortOrder, int departmentId,
                                                  int typeId, String cityid, String dateCreated) {
        JSONObject mainJsonObj = new JSONObject();

        try {
            mainJsonObj.put("hubCitiId", hubCitiId);
            mainJsonObj.put("level", level);
            mainJsonObj.put("sortOrder", sortOrder);
            mainJsonObj.put("departmentId", departmentId);
            mainJsonObj.put("typeId", typeId);
            mainJsonObj.put("linkId", linkId);
            if (dateCreated != null && !dateCreated.isEmpty() && level == 1) {
                mainJsonObj.put("dateCreated", dateCreated);
            }

            if (Properties.isRegionApp) {
                //mainJsonObj.put("userId", getUid());
                // mainJsonObj.put("deviceId", deviceId);
                //mainJsonObj.put("platform", Constants.PLATFORM);
                if (cityid != null && !cityid.isEmpty()) {
                    mainJsonObj.put("cityIds", cityid);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mainJsonObj;
    }


    public String createForgotPasswordParameter(String userName)
            throws Exception {
        return "<UserRegistrationInfo>" + "<userName><![CDATA[" + userName + "]]></userName>" +
                "<hubCitiId><![CDATA[" + getHubCityId() + "]]></hubCitiId>" +
                "</UserRegistrationInfo>";
    }

    public String createLogInViewUrlParameter(String pageType, String hubCitiKey)
            throws NameNotFoundException {

        return "<LoginFlowDetails>" + "<pageType><![CDATA[" + pageType + "]]></pageType>" +
                "<hubCitiKey><![CDATA[" + hubCitiKey + "]]></hubCitiKey>" + "</LoginFlowDetails>";
    }

    public String createSignUpUrlParameter(String userNameVal,
                                           String passwordVal, String email) throws NameNotFoundException {
        if (email.equals("")) {
            return "<UserDetails>" + "<userName><![CDATA[" + userNameVal + "]]></userName>" +
                    "<password><![CDATA[" + passwordVal + "]]></password>" +
                    "<appVersion><![CDATA["
                    + Constants.getAppVersion() + "]]></appVersion>" + "<deviceId><![CDATA[" +
                    Constants.getDeviceId() + "]]></deviceId>" + "<hubCitiKey><![CDATA[" +
                    Properties
                            .HUBCITI_KEY + "]]></hubCitiKey>"
                    + "<platform><![CDATA[Android]]></platform></UserDetails>";
        } else {
            return "<UserDetails>" + "<userName><![CDATA[" + userNameVal + "]]></userName>" +
                    "<password><![CDATA[" + passwordVal + "]]></password>" +
                    "<appVersion><![CDATA["
                    + Constants.getAppVersion() + "]]></appVersion>" + "<deviceId><![CDATA[" +
                    Constants.getDeviceId() + "]]></deviceId>" + "<hubCitiKey><![CDATA[" +
                    Properties
                            .HUBCITI_KEY + "]]></hubCitiKey>" + "<email><![CDATA[" + email +
                    "]]></email>" +
                    "<platform><![CDATA[Android]]></platform></UserDetails>";
        }
    }


    private String getTag(String tagName, String value) {
        return "<" + tagName + ">" + value + "</" + tagName + ">";
    }

    public JSONObject createCityExperienceJsonParam(String citiExpId,
                                                    String catIds, String searchKey, String lowerLimit, String mItemId,
                                                    String mBottomId, String zipCode, String latitude,
                                                    String longitude, String sortColumn, String sortOrder,
                                                    String locSpecials, String interests, String cityIds) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", getUid());
            jsonObject.put("hubCitiId", getHubCityId());
            jsonObject.put("citiExpId", citiExpId);
            if (catIds != null && !"".equals(catIds) && !"0".equals(catIds)) {
                jsonObject.put("catIds", catIds);
            }
            if (sortColumn != null && !"".equals(sortColumn)) {
                jsonObject.put("sortColumn", sortColumn);
            }
            jsonObject.put("sortOrder", sortOrder);
            if (null != searchKey && !"".equals(searchKey)) {
                jsonObject.put("searchKey", searchKey);
            }
            jsonObject.put("lowerLimit", lowerLimit);

            if (mItemId != null && !"".equals(mItemId)) {
                jsonObject.put("mItemId", mItemId);
            }

            if (mBottomId != null && !"".equals(mBottomId)) {
                jsonObject.put("bottomBtnId", mBottomId);
            }
            if ((null != latitude && !"".equals(latitude))
                    && (null != longitude && !"".equals(longitude))) {

                jsonObject.put("latitude", latitude);
                jsonObject.put("longitude", longitude);
            }
            if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                if (null != zipCode && !zipCode.isEmpty()) {

                    jsonObject.put("postalCode", zipCode);
                }

            } else {
                if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                    jsonObject.put("postalCode",
                            Constants.getZipCode());
            }
            jsonObject.put("platform", Constants.PLATFORM);

            if (locSpecials != null && !"".equals(locSpecials)) {
                jsonObject.put("locSpecials", locSpecials);
            }

            if (interests != null && !"".equals(interests)) {
                jsonObject.put("interests", interests);
            }

            if (!"".equals(cityIds) && cityIds != null) {
                jsonObject.put("cityIds", cityIds);
            }
            jsonObject.put("requestedTime", CommonConstants.convertDeviceTimeToUTC());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }


    public JSONObject createFilterJsonParam(String catIds,
                                            String retAffId, String lowerLimit, String zipCode,
                                            String latitude, String longitude, String sortColumn,
                                            String sortOrder, String locSpecials, String searchKey,
                                            String bottomBtnId, String mItemId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", getUid());
            jsonObject.put("hubCitiId", getHubCityId());

            if (Constants.getMainMenuId() != null
                    && !"".equals(Constants.getMainMenuId())) {

                jsonObject.put("mainMenuId", Constants.getMainMenuId());
            }

            jsonObject.put("lowerLimit", lowerLimit);

            jsonObject.put("retAffId", retAffId);

            if (catIds != null && !"".equals(catIds) && !"0".equals(catIds)) {
                jsonObject.put("catIds", catIds);
            }
            if (bottomBtnId != null && !"".equals(bottomBtnId)
                    && !"0".equals(bottomBtnId)) {
                jsonObject.put("bottomBtnId", bottomBtnId);
            }
            if (mItemId != null && !mItemId.isEmpty()) {
                jsonObject.put("mItemId", mItemId);
            }
            if ((null != latitude && !"".equals(latitude))
                    && (null != longitude && !"".equals(longitude))) {

                jsonObject.put("latitude", latitude);
                jsonObject.put("longitude", longitude);
            }
            jsonObject.put("platform", Constants.PLATFORM);

            if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
                if (null != zipCode && !zipCode.isEmpty()) {

                    jsonObject.put("postalCode", zipCode);
                }

            } else {
                if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                    jsonObject.put("postalCode",
                            Constants.getZipCode());

            }

            jsonObject.put("sortColumn", sortColumn);

            jsonObject.put("sortOrder", sortOrder);

            jsonObject.put("locSpecials", locSpecials);

            if (searchKey != null && !"".equals(searchKey)) {
                jsonObject.put("searchKey", searchKey);
            }
            jsonObject.put("requestedTime", CommonConstants.convertDeviceTimeToUTC());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String getCategoryByExp(String expId) {
        return "<ThisLocationRequest>" + getTag(Constants.HUBCITI_ID, getHubCityId()) + getTag
                ("retGroupId", expId) + "</ThisLocationRequest>";
    }

    public String getCategoryByFilter(String retAffId) {
        return "<ThisLocationRequest>" + getTag(Constants.HUBCITI_ID, getHubCityId()) + getTag
                ("retAffId", retAffId) + "</ThisLocationRequest>";
    }

    public JSONObject getFindLocationInfo(String mItemId, String catName,
                                          String lastVisitedNo, String latitude, String longitude,
                                          String radius, String searchKey, String subCatId,
                                          String bottmBtnId, String sortColumn, String filterId,
                                          String filterValueId, String locSpecials, String interests,
                                          String cityIds, String postalCode, String requestedTime) throws
            Exception {

        try {
            JSONObject jsonObject = new JSONObject();
//			jsonObject.put("userId", getUid());
            if (catName != null) {
                jsonObject.put("catName", catName);
            }
            if (lastVisitedNo != null) {
                jsonObject.put("lastVisitedNo", lastVisitedNo);
            }
            if (latitude != null && !"".equals(latitude)
                    && !"null".equals(latitude)) {
                jsonObject.put("latitude", latitude);
            }
            if (longitude != null && !"".equals(longitude)
                    && !"null".equals(longitude)) {
                jsonObject.put("longitude", longitude);
            }
            if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {

                if (postalCode != null && !postalCode.isEmpty()) {
                    jsonObject.put("postalCode", postalCode);

                }
            } else {
                if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty())
                    jsonObject.put("postalCode", Constants.getZipCode());
            }
            if (radius != null && !radius.equals("")) {
                jsonObject.put("radius", radius);
            } else {
                jsonObject.put("radius", "50");
            }
            if (searchKey != null) {
                jsonObject.put("searchKey", searchKey);
            }
            if (subCatId != null && !"".equals(subCatId)) {
                jsonObject.put("subCatIds", subCatId);
            }
            jsonObject.put("sortOrder", "ASC");

            if (Constants.getMainMenuId() != null
                    && !"".equals(Constants.getMainMenuId())) {
                jsonObject.put("mainMenuId", Constants.getMainMenuId());
            }
            if (mItemId != null && !"".equals(mItemId)) {
                jsonObject.put("mItemId", mItemId);
            }
            if (bottmBtnId != null && !"".equals(bottmBtnId)) {
                jsonObject.put("bottomBtnId", bottmBtnId);
            }
            if (sortColumn != null && !"".equals(sortColumn)) {
                jsonObject.put("sortColumn", sortColumn);
            }
            if (Properties.isRegionApp) {
                if (cityIds != null && !"".equals(cityIds)) {
                    jsonObject.put("cityIds", cityIds);
                }
            }
            // For Addison Filters
            if (filterId != null && !"".equals(filterId)) {
                jsonObject.put("filterId", filterId);
            }

            if (filterValueId != null && !"".equals(filterValueId)) {
                jsonObject.put("fValueId", filterValueId);
            }

            if (locSpecials != null && !"".equals(locSpecials)) {
                jsonObject.put("locSpecials", locSpecials);
            }

            if (interests != null && !"".equals(interests)) {
                jsonObject.put("interests", interests);
            }
            if (requestedTime != null && !"".equals(requestedTime)) {
                jsonObject.put("requestedTime", requestedTime);
            }

            jsonObject.put("hubCitiId", getHubCityId());

            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public JSONObject getEventTypeRequestParam(String radius, String postalCode, String mItemId,
                                               String sortColumn, String sortOrder, String groupBy, String lastVisitedNo, String
                                                       catIds, String cityIds) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("hubCitiId", getHubCityId());
            if (radius != null && !radius.equals("")) {
                jsonObject.put("radius", radius);
            } else {
                jsonObject.put("radius", "50");
            }
            if (Constants.getMainMenuId() != null
                    && !"".equals(Constants.getMainMenuId())) {
                jsonObject.put("mainMenuId", Constants.getMainMenuId());
            }
            if (postalCode != null && !postalCode.equals("")) {
                jsonObject.put("postalCode", postalCode);
            }

            if (mItemId != null && !"".equals(mItemId)) {
                jsonObject.put("mItemId", mItemId);
            }
            if (sortColumn != null && !"".equals(sortColumn)) {
                jsonObject.put("sortColumn", sortColumn);
            }
            if (sortOrder != null && !"".equals(sortOrder)) {
                jsonObject.put("sortOrder", sortOrder);
            }
            if (groupBy != null && !"".equals(groupBy)) {
                jsonObject.put("groupBy", groupBy);
            }
            if (lastVisitedNo != null && !"".equals(lastVisitedNo)) {
                jsonObject.put("lastVisitedNo", lastVisitedNo);
            }
            if (catIds != null && !catIds.equals("")) {
                jsonObject.put("catIds", catIds);
            }
            if (cityIds != null && !cityIds.equals("")) {
                jsonObject.put("cityIds", cityIds);
            }
            jsonObject.put("userId", getUid());
            return jsonObject;
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getEventSearchRequestParam(String latitude, String longitude,
                                                 String radius, String postalCode, String searchKey) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("hubCitiId", getHubCityId());
            if (radius != null && !radius.equals("")) {
                jsonObject.put("radius", radius);
            } else {
                jsonObject.put("radius", "50");
            }
            if (Constants.getMainMenuId() != null
                    && !"".equals(Constants.getMainMenuId())) {
                jsonObject.put("mainMenuId", Constants.getMainMenuId());
            }

            if (latitude != null && !"".equals(latitude) && !"null".equals(latitude)) {
                jsonObject.put("latitude", latitude);
            }
            if (longitude != null && !"".equals(longitude) && !"null".equals(longitude)) {
                jsonObject.put("longitude", longitude);
            } else {
                if (postalCode != null && !postalCode.equals("")) {
                    jsonObject.put("postalCode", postalCode);
                }
            }
            if (searchKey != null && !"".equals(searchKey)) {
                jsonObject.put("searchKey", searchKey);
            }
            jsonObject.put("lowerLimit", "0");
            jsonObject.put("userId", getUid());
            return jsonObject;
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getBandRequestParam(String radius, String postalCode, String mItemId,
                                          String sortColumn, String sortOrder, String groupBy,
                                          String
                                                  searchKey,
                                          String lastVisitedNo, String catIds, String cityIds) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("hubCitiId", getHubCityId());
            if (radius != null && !radius.equals("")) {
                jsonObject.put("radius", radius);
            } else {
                jsonObject.put("radius", "50");
            }
            if (Constants.getMainMenuId() != null
                    && !"".equals(Constants.getMainMenuId())) {
                jsonObject.put("mainMenuId", Constants.getMainMenuId());
            }
            if (postalCode != null && !postalCode.equals("")) {
                jsonObject.put("postalCode", postalCode);
            }
            if (catIds != null && !catIds.equals("")) {
                jsonObject.put("catIds", catIds);
            }
            if (mItemId != null && !"".equals(mItemId)) {
                jsonObject.put("mItemId", mItemId);
            }
            if (sortColumn != null && !"".equals(sortColumn)) {
                jsonObject.put("sortColumn", sortColumn);
            }
            if (sortOrder != null && !"".equals(sortOrder)) {
                jsonObject.put("sortOrder", sortOrder);
            }
            if (groupBy != null && !"".equals(groupBy)) {
                jsonObject.put("groupBy", groupBy);
            }
            if (searchKey != null && !"".equals(searchKey)) {
                jsonObject.put("searchKey", searchKey);
            }
            if (lastVisitedNo != null && !"".equals(lastVisitedNo)) {
                jsonObject.put("lastVisitedNo", lastVisitedNo);
            }
            if (cityIds != null && !cityIds.equals("")) {
                jsonObject.put("cityIds", cityIds);
            }
            jsonObject.put("userId", getUid());
            return jsonObject;
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getRetailerImpressionRequest(String catName, String retailerIds, String
            retailerLocIds) {
        try {
            JSONObject jsonObject = new JSONObject();
            if (catName != null && !catName.equals("")) {
                jsonObject.put("catName", catName);
            }
            if (retailerIds != null && !retailerIds.equals("")) {
                jsonObject.put("retailerId", retailerIds);
            }
            if (retailerLocIds != null && !retailerLocIds.equals("")) {
                jsonObject.put("retailLocationId", retailerLocIds);
            }
            if (Constants.getMainMenuId() != null
                    && !"".equals(Constants.getMainMenuId())) {
                jsonObject.put("mainMenuId", Constants.getMainMenuId());
//				jsonObject.put("mainMenuId", "138");
            }
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject getFindSingleCategoryRetailersRequest(String mItemId,
                                                            String mBottomId, String catName, String lastVisitedNo,
                                                            String latitude, String longitude, String radius, String searchKey,
                                                            String subCatId, String sortBy, String filterId,
                                                            String filterValueId, String locSpecials, String interests,
                                                            String cityIds, String requestedTime) throws
            Exception {
        try {
            JSONObject jsonObject = new JSONObject();
//			jsonObject.put("userId", getUid());

            if (catName != null) {
                jsonObject.put("catName", catName);
            }
            if (lastVisitedNo != null) {
                jsonObject.put("lastVisitedNo", lastVisitedNo);
            }
            if (latitude != null && !"".equals(latitude)
                    && !"null".equals(latitude)) {
                jsonObject.put("latitude", latitude);
            }
            if (longitude != null && !"".equals(longitude)
                    && !"null".equals(longitude)) {
                jsonObject.put("longitude", longitude);
                jsonObject.put("userPostalCode ", Constants.getZipCode());
            }

            if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {

                if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty()) {
                    jsonObject.put("postalCode", Constants.getZipCode());

                }
            } else {
                if (Constants.getZipCode() != null && !Constants.getZipCode().isEmpty()) {
                    jsonObject.put("postalCode", Constants.getZipCode());
                }
            }
            if (radius != null && !radius.equals("")) {
                jsonObject.put("radius", radius);
            } else {
                jsonObject.put("radius", "50");
            }
            if (searchKey != null) {
                jsonObject.put("searchKey", searchKey);
            }
            if (subCatId != null && !"".equals(subCatId)) {
                jsonObject.put("subCatIds", subCatId);
            }
            jsonObject.put("sortOrder", "ASC");

            if (Constants.getMainMenuId() != null
                    && !"".equals(Constants.getMainMenuId())) {
                jsonObject.put("mainMenuId", Constants.getMainMenuId());
            }
            if (mItemId != null && !"".equals(mItemId)) {
                jsonObject.put("mItemId", mItemId);
            }
            if (mBottomId != null && !"".equals(mBottomId)) {
                jsonObject.put("bottomBtnId", mBottomId);
            }
            if (sortBy != null && !"".equals(sortBy)) {
                jsonObject.put("sortColumn", sortBy);
            }
            if (Properties.isRegionApp) {
                if (cityIds != null && !"".equals(cityIds)) {
                    jsonObject.put("cityIds", cityIds);
                }
            }
            // For Addison Filters
            if (filterId != null && !"".equals(filterId)) {
                jsonObject.put("filterId", filterId);
            }

            if (filterValueId != null && !"".equals(filterValueId)) {
                jsonObject.put("fValueId", filterValueId);
            }

            if (locSpecials != null && !"".equals(locSpecials)) {
                jsonObject.put("locSpecials", locSpecials);
            }

            if (interests != null && !"".equals(interests)) {
                jsonObject.put("interests", interests);
            }
            if (requestedTime != null && !"".equals(requestedTime)) {
                jsonObject.put("requestedTime", requestedTime);
            }

            jsonObject.put("hubCitiId", getHubCityId());

            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getFindSingleCategoryRetailersType(String catId)
            throws Exception {

        postDataBuilder = new StringBuilder();
        postDataBuilder.append("userId=").append(getUid());
        postDataBuilder.append("&hubCitiId=").append(getHubCityId());
        if (catId != null) {
            postDataBuilder.append("&catId=").append(catId);
        }

        return postDataBuilder.toString();
    }

    public JSONObject getAnythingPageJsonReqParam(String pageId, String mItemId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageId", pageId);
            jsonObject.put("mItemId", mItemId);
            jsonObject.put(Constants.HUBCITI_ID, getHubCityId());
            jsonObject.put("platform", "Android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String getAppSiteReqParam(String mItemId, String linkId,
                                     String postalCode, String latitude, String longitude,
                                     String bottomBtnId) {
        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<RetailerDetail>");
        urlParameters.append(getTag("userId", getUid()));
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append(getTag("linkId", linkId));
        if (null != mItemId && !"".equals(mItemId)) {
            urlParameters.append(getTag("mItemId", mItemId));
        }
        if (null != bottomBtnId && !"".equals(bottomBtnId)) {
            urlParameters.append(getTag("bottomBtnId", bottomBtnId));
        }
        if ((null != latitude && !"".equals(latitude))
                && (null != longitude && !"".equals(longitude))) {

            urlParameters.append(getTag("latitude", latitude));
            urlParameters.append(getTag("longitude", longitude));
        }
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (null != Constants.getZipCode() && !"".equals(Constants.getZipCode())) {

                urlParameters.append(getTag("postalCode", Constants.getZipCode()));

            }

        } else {
            if (null != Constants.getZipCode() && !Constants.getZipCode().isEmpty()) {
                urlParameters.append(getTag("postalCode",
                        Constants.getZipCode()));
            }
        }
        urlParameters.append(getTag("platform", "Android"));
        urlParameters.append("</RetailerDetail>");
        return urlParameters.toString();
    }

    public String createUserSettingsParameters(String radius,
                                               String pushNotify, boolean locationFlag) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<UserSettings>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");

        urlParameters.append("<localeRadius>").append(radius).append("</localeRadius>");

        if (pushNotify != null)
        {
            urlParameters.append("<pushNotify>").append(pushNotify).append("</pushNotify>");
        }
        if (Constants.getDeviceId() != null) {
            urlParameters.append("<deviceId>").append(Constants.getDeviceId()).append
                    ("</deviceId>");
        }
        if (getHubCityId() != null) {
            urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }
        urlParameters.append("<bLocService>").append(locationFlag).append("</bLocService>");
        urlParameters.append("</UserSettings>");
        return urlParameters.toString();

    }

    public String getUserSettingsParameters() {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<AuthenticateUser>");
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        if (Constants.getDeviceId() != null) {
            urlParameters.append("<deviceID>").append(Constants.getDeviceId()).append
                    ("</deviceID>");
        }
        if (getHubCityId() != null) {
            urlParameters.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        }
        urlParameters.append("</AuthenticateUser>");

        HubCityLogger.d("SettingsPage", "request :" + urlParameters.toString());
        return urlParameters.toString();

    }

    public String createProductEmailShare(String productId) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<ShareProductInfo>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<userId>").append(getUid()).append("</userId>");

        if (productId != null && !"".equals(productId)) {
            urlParameters.append("<productId>").append(productId).append("</productId>");
        }

        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");
        urlParameters.append("</ShareProductInfo>");
        return urlParameters.toString();

    }

    public String createAppsiteEmailShare(String retailerId,
                                          String retailLocationId) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<ShareProductInfo>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");
        if (retailerId != null && !"".equals(retailerId)) {
            urlParameters.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        if (retailLocationId != null && !"".equals(retailLocationId)) {
            urlParameters.append("<retailerLocationId>").append(retailLocationId).append
                    ("</retailerLocationId>");
        }

        urlParameters.append("</ShareProductInfo>");
        return urlParameters.toString();

    }

    public String createSplOfferEmailShare(String retailerId, String pageId) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<ShareProductInfo>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");

        if (retailerId != null && !"".equals(retailerId)) {
            urlParameters.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        if (pageId != null && !"".equals(pageId)) {
            urlParameters.append("<pageId>").append(pageId).append("</pageId>");
        }

        urlParameters.append("</ShareProductInfo>");
        return urlParameters.toString();

    }

    public String createEventsEmailShare(String eventId) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<ShareProductInfo>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");

        if (eventId != null && !"".equals(eventId)) {
            urlParameters.append("<eventId>").append(eventId).append("</eventId>");
        }

        urlParameters.append("</ShareProductInfo>");
        return urlParameters.toString();

    }

    public String createFundraiserEmailShare(String fundraiserId) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<ShareProductInfo>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");

        if (fundraiserId != null && !"".equals(fundraiserId)) {
            urlParameters.append("<fundraiserId>").append(fundraiserId).append("</fundraiserId>");
        }

        urlParameters.append("</ShareProductInfo>");
        return urlParameters.toString();

    }

    public String createCouponsEmailShare(String couponId) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<ShareProductInfo>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");

        if (couponId != null && !"".equals(couponId)) {
            urlParameters.append("<couponId>").append(couponId).append("</couponId>");
        }

        urlParameters.append("</ShareProductInfo>");
        return urlParameters.toString();

    }

    public String createNewsEmailShare(String newsId) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<ShareProductInfo>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");

        if (newsId != null && !"".equals(newsId)) {
            urlParameters.append("<newsId>").append(newsId).append("</newsId>");
        }

        urlParameters.append("</ShareProductInfo>");
        return urlParameters.toString();

    }

    public String createHotDealsEmailShare(String hotdealId) {

        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<ShareProductInfo>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<userId>").append(getUid()).append("</userId>");
        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");

        if (hotdealId != null && !"".equals(hotdealId)) {
            urlParameters.append("<hotdealId>").append(hotdealId).append("</hotdealId>");
        }

        urlParameters.append("</ShareProductInfo>");
        return urlParameters.toString();

    }

    public String createEmailShareParam() {

        return "<ShareProductInfo>" + getTag(Constants.HUBCITI_ID, getHubCityId()) + "<userId>" +
                getUid() + "</userId>" + "<shareType>email</shareType>" + "<platform>" +
                Constants.PLATFORM + "</platform>" + "</ShareProductInfo>";

    }

    public String createFaqDisplayParam(String catId, /* String lowerLimit, */
                                        String searchKey, String mainMenuId) {
        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<FAQDetails>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<categoryId>").append(catId).append("</categoryId>");
        urlParameters.append("<searchKey><![CDATA[").append(searchKey).append("]]></searchKey>");
        urlParameters.append("<platform>" + "Android" + "</platform>");
        if (null != mainMenuId && !"".equals(mainMenuId)) {
            urlParameters.append("<mainMenuId>").append(mainMenuId).append("</mainMenuId>");
        }

        urlParameters.append("</FAQDetails>");
        return urlParameters.toString();
    }

    public String createProductShareParam(String productId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ShareProductInfo>");

        if (productId != null && !"".equals(productId)) {
            urlParameters.append("<productId>").append(productId).append("</productId>");
        }

        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                ("</hubCitiId>");
        urlParameters.append("</ShareProductInfo>");

        return urlParameters.toString();
    }

    public String createAppsiteShareParam(String retailerId,
                                          String retailLocationId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ShareProductInfo>");

        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                ("</hubCitiId>");

        if (retailerId != null && !"".equals(retailerId)) {
            urlParameters.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        if (retailLocationId != null && !"".equals(retailLocationId)) {
            urlParameters.append("<retailerLocationId>").append(retailLocationId).append
                    ("</retailerLocationId>");
        }

        urlParameters.append("</ShareProductInfo>");

        return urlParameters.toString();
    }

    public String createSplOfferShareParam(String retailerId, String pageId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ShareProductInfo>");

        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                ("</hubCitiId>");

        if (retailerId != null && !"".equals(retailerId)) {
            urlParameters.append("<retailerId>").append(retailerId).append("</retailerId>");
        }

        if (pageId != null && !"".equals(pageId)) {
            urlParameters.append("<pageId>").append(pageId).append("</pageId>");
        }

        urlParameters.append("</ShareProductInfo>");

        return urlParameters.toString();
    }

    public String createEventsShareParam(String eventId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ShareProductInfo>");

        if (eventId != null && !"".equals(eventId)) {
            urlParameters.append("<eventId>").append(eventId).append("</eventId>");
        }

        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                ("</hubCitiId>");
        urlParameters.append("</ShareProductInfo>");

        return urlParameters.toString();
    }

    public String createFundraiserShareParam(String fundraiserId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ShareProductInfo>");

        if (fundraiserId != null && !"".equals(fundraiserId)) {
            urlParameters.append("<fundraiserId>").append(fundraiserId).append("</fundraiserId>");
        }

        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                ("</hubCitiId>");
        urlParameters.append("</ShareProductInfo>");

        return urlParameters.toString();
    }

    public String createCouponsShareParam(String couponId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ShareProductInfo>");

        if (couponId != null && !"".equals(couponId)) {
            urlParameters.append("<couponId>").append(couponId).append("</couponId>");
        }

        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                ("</hubCitiId>");
        urlParameters.append("</ShareProductInfo>");

        return urlParameters.toString();
    }
    public String createNewsShareParam(String newsId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ShareProductInfo>");

        if (newsId != null && !"".equals(newsId)) {
            urlParameters.append("<newsId>").append(newsId).append("</newsId>");
        }

        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                ("</hubCitiId>");
        urlParameters.append("</ShareProductInfo>");

        return urlParameters.toString();
    }

    public String createHotDealsShareParam(String hotdealId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<ShareProductInfo>");

        if (hotdealId != null && !"".equals(hotdealId)) {
            urlParameters.append("<hotdealId>").append(hotdealId).append("</hotdealId>");
        }

        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");
        urlParameters.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                ("</hubCitiId>");
        urlParameters.append("</ShareProductInfo>");

        return urlParameters.toString();
    }

    public String getShareUserTracking(String shrTypNam, String tarAddr,
                                       String module, String moduleId, String retId, String retLocId,
                                       String pageId) {
        StringBuilder urlParameters = new StringBuilder();

        urlParameters.append("<UserTrackingData>");
        urlParameters.append("<shrTypNam>").append(shrTypNam).append("</shrTypNam>");

        if ("Product Details".equals(module)) {
            if (moduleId != null && !"".equals(moduleId)) {
                urlParameters.append("<prodId>").append(moduleId).append("</prodId>");
            }

        } else if ("Appsite".equals(module)) {
            if (retId != null && !"".equals(retId)) {
                urlParameters.append("<retId>").append(retId).append("</retId>");
            }

            if (retLocId != null && !"".equals(retLocId)) {
                urlParameters.append("<retLocId>").append(retLocId).append("</retLocId>");
            }

        } else if ("Anything".equals(module)) {
            if (pageId != null && !"".equals(pageId)) {
                urlParameters.append("<aPageId>").append(pageId).append("</aPageId>");
            }

            if (retId != null && !"".equals(retId)) {
                urlParameters.append("<retId>").append(retId).append("</retId>");
            }

            if (retLocId != null && !"".equals(retLocId)) {
                urlParameters.append("<retLocId>").append(retLocId).append("</retLocId>");
            }

        } else if ("Specials".equals(module)) {
            if (retId != null && !"".equals(retId)) {
                urlParameters.append("<retId>").append(retId).append("</retId>");
            }

            if (pageId != null && !"".equals(pageId)) {
                urlParameters.append("<sPageId>").append(pageId).append("</sPageId>");
            }

        } else if ("Events".equals(module)) {
            if (moduleId != null && !"".equals(moduleId)) {
                urlParameters.append("<eventId>").append(moduleId).append("</eventId>");
            }

        } else if ("Fundraiser".equals(module)) {
            if (moduleId != null && !"".equals(moduleId)) {
                urlParameters.append("<fundId>").append(moduleId).append("</fundId>");
            }

        } else if ("BandSummery".equals(module)) {
            if (moduleId != null && !"".equals(moduleId)) {
                urlParameters.append("<bandId>").append(moduleId).append("</bandId>");
            }
        } else if ("BandEventSummery".equals(module)) {
            if (moduleId != null && !"".equals(moduleId)) {
                urlParameters.append("<bandEventId>").append(moduleId).append("</bandEventId>");
            }

        }
        urlParameters.append("<userId>").append(UrlRequestParams.getUid()).append("</userId>");

        if (null != Constants.getMainMenuId()
                && !"".equals(Constants.getMainMenuId())) {
            urlParameters.append("<mainMenuId>").append(Constants.getMainMenuId()).append
                    ("</mainMenuId>");
        }

        if (tarAddr != null && !"".equals(tarAddr)) {
            urlParameters.append("<tarAddr>").append(tarAddr).append("</tarAddr>");
        }

        urlParameters.append("</UserTrackingData>");

        return urlParameters.toString();
    }

    public String createFaqDetailsParam(String faqId, String faqListId) {
        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("<FAQDetails>");
        urlParameters.append(getTag(Constants.HUBCITI_ID, getHubCityId()));
        urlParameters.append("<faqId>").append(faqId).append("</faqId>");
        urlParameters.append("<platform>" + Constants.PLATFORM + "</platform>");

        if (faqListId != null && !"".equals(faqListId)) {
            urlParameters.append("<faqListId>").append(faqListId).append("</faqListId>");
        }

        urlParameters.append("</FAQDetails>");
        return urlParameters.toString();
    }

    public String getFindSubCategoryParam(String catId, String mItemId,
                                          String mBottomId, String searchKey) {
        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<UserDetails>");

        if (null != UrlRequestParams.getUid()) {
            postDataBuilder.append("<userId>").append(UrlRequestParams.getUid()).append
                    ("</userId>");
        }

        if (null != UrlRequestParams.getHubCityId()) {
            postDataBuilder.append("<hubCitiId>").append(UrlRequestParams.getHubCityId()).append
                    ("</hubCitiId>");
        }

        if (null != catId && !"".equals(catId)) {
            postDataBuilder.append("<catId>").append(catId).append("</catId>");
        }


        if (null != CommonConstants.LATITUDE && !CommonConstants.LATITUDE.isEmpty()) {
            postDataBuilder.append("<latitude>").append(CommonConstants.LATITUDE).append
                    ("</latitude>");
        }

        if (null != CommonConstants.LONGITUDE && !CommonConstants.LONGITUDE.isEmpty()) {
            postDataBuilder.append("<longitude>").append(CommonConstants.LONGITUDE).append
                    ("</longitude>");

        }

        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (null != Constants.getZipCode()
                    && !Constants.getZipCode().isEmpty()) {
                postDataBuilder.append("<postalCode>").append(Constants.getZipCode()).append
                        ("</postalCode>");

            }
        } else {
            if (null != Constants.getZipCode()
                    && !Constants.getZipCode().isEmpty()) {
                postDataBuilder.append("<postalCode>").append(Constants.getZipCode()).append
                        ("</postalCode>");
            }
        }
        if (null != mItemId && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
        }

        if (null != searchKey && !"".equals(searchKey)) {
            postDataBuilder.append("<srchKey>").append(searchKey).append("</srchKey>");
        }

        if (null != mBottomId && !"".equals(mBottomId)) {
            postDataBuilder.append("<bottomBtnId>").append(mBottomId).append("</bottomBtnId>");
        }

        postDataBuilder.append("</UserDetails>");

        return postDataBuilder.toString();
    }

    public String versionCheckParams() throws Exception {

        return "<AuthenticateUser><appVersion>" + Constants.getAppVersion() + "</appVersion>" +
                "<platform>" + Constants.PLATFORM
                + "</platform>" + "<hubCitiKey>" + Properties.HUBCITI_KEY +
                "</hubCitiKey></AuthenticateUser>";
    }

    // Event Appsiteloc method changed from GET to POST
    public String getEventAppSiteLocParams(String eventID, String latitude,
                                           String longitude) throws Exception {

        StringBuilder postDataBuilder = new StringBuilder();
        postDataBuilder.append("<Retailer>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<eventId>").append(eventID).append("</eventId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");

        if (latitude != null && !latitude.isEmpty()) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }

        if (longitude != null && !longitude.isEmpty()) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }

        postDataBuilder.append("</Retailer>");

        return postDataBuilder.toString();
    }

    // Request XML for getting City list in Region App
    public String createCityListParameter(String module, String citiExpID,
                                          String mItemId, String mBottomId, String fundId, String retailerId,
                                          String retailerLocationId, String isRetailerEvents, String radius, String srchKey) {
        StringBuilder postDataBuilder = new StringBuilder();
        postDataBuilder.append("<UserDetails>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<module>").append(module).append("</module>");
        String postalCode = Constants.getZipCode();
        if (module.equalsIgnoreCase("Coupons") || module.equalsIgnoreCase("myaccounts"))
        {
            if(postalCode.isEmpty() || postalCode == null){
                postalCode = CouponsActivty.postalCode;
            }
        }


        if (null != postalCode && !postalCode.isEmpty()) {
            postDataBuilder.append("<postalCode>").append(postalCode).append("</postalCode>");
        }
        if (!"".equals(citiExpID) && null != citiExpID) {
            postDataBuilder.append("<citiExpId>").append(citiExpID).append("</citiExpId>");
        }
        if (!"".equals(radius) && null != radius) {
            postDataBuilder.append("<radius>").append(radius).append("</radius>");
        }
        if (null != srchKey && !"".equals(srchKey)) {
            postDataBuilder.append("<srchKey>").append(srchKey).append("</srchKey>");
        }
        if ("true".equals(isRetailerEvents)) {
            if (retailerId != null && !"".equals(retailerId)) {
                postDataBuilder.append("<retailId>").append(retailerId).append("</retailId>");
            }

            if (retailerLocationId != null && !"".equals(retailerLocationId)) {
                postDataBuilder.append("<retailLocationId>").append(retailerLocationId).append
                        ("</retailLocationId>");
            }
        } else {
            if (!"".equals(fundId) && null != fundId) {
                postDataBuilder.append("<fundId>").append(fundId).append("</fundId>");
            } else {
                if (null != mItemId && !"".equals(mItemId)) {
                    postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
                }

                if (null != mBottomId && !"".equals(mBottomId)) {
                    postDataBuilder.append("<bottomBtnId>").append(mBottomId).append
                            ("</bottomBtnId>");
                }
            }
        }

        postDataBuilder.append("</UserDetails>");
        return postDataBuilder.toString();
    }

    public String createCityListParameterFind(String module, String mItemId,
                                              String mBottomId, String latitude, String longitude,
                                              String catName, String srchKey, String catIds, String subCatId) {

        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<UserDetails>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<module>").append(module).append("</module>");

        if (null != mItemId && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
        }

        if (null != mBottomId && !"".equals(mBottomId)) {
            postDataBuilder.append("<bottomBtnId>").append(mBottomId).append("</bottomBtnId>");
        }

        if (null != latitude && !"".equals(latitude)) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }

        if (null != longitude && !"".equals(longitude)) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }

        if (null != catName && !"".equals(catName)) {
            postDataBuilder.append("<catName>").append(catName).append("</catName>");
        }

        if (null != srchKey && !"".equals(srchKey)) {
            postDataBuilder.append("<srchKey>").append(srchKey).append("</srchKey>");
        }

        if (null != catIds && !"".equals(catIds)) {
            postDataBuilder.append("<catIds>").append(catIds).append("</catIds>");
        }

        if (null != subCatId && !"".equals(subCatId)) {
            postDataBuilder.append("<subCatId>").append(subCatId).append("</subCatId>");
        }

        postDataBuilder.append("</UserDetails>");
        return postDataBuilder.toString();

    }

    public String createCityListParameterSubMenu(String module, String mlinkId,
                                                 String typeId, String deptId, String level, String menuId) {
        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<UserDetails>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<module>").append(module).append("</module>");
        if (null != mlinkId && !"".equals(mlinkId)) {
            postDataBuilder.append("<linkId>").append(mlinkId).append("</linkId>");
        }
        if (null != typeId && !"".equals(typeId)) {
            postDataBuilder.append("<typeId>").append(typeId).append("</typeId>");
        }
        if (null != deptId && !"".equals(deptId)) {
            postDataBuilder.append("<departmentId>").append(deptId).append("</departmentId>");
        }
        if (null != menuId && !"".equals(menuId)) {
            postDataBuilder.append("<linkId>").append(menuId).append("</linkId>");
        }
        if (null != level && !"".equals(level)) {
            postDataBuilder.append("<level>").append(level).append("</level>");
        }
        postDataBuilder.append("</UserDetails>");

        return postDataBuilder.toString();

    }

    public String createFundraiserEventDesc(String fundId, String fundListId) {

        return "<MenuItem>" + "<fundListId>" + fundListId + "</fundListId>" + "<hubCitiId>" +
                getHubCityId() + "</hubCitiId>" + "<fundId>" + fundId + "</fundId>" +
                "<platform>" + Constants.PLATFORM
                + "</platform>" + "</MenuItem>";

    }

    public String createDealsParameter(boolean isSubMenu, String latitude,
                                       String longitude, String zipCode) {
        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<Deal>");

        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<flag>").append(isSubMenu).append("</flag>");
        if ((null != latitude && !"".equals(latitude))
                && (null != longitude && !"".equals(longitude))) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");

        }
        if (!Constants.GuestLoginId.equals(UrlRequestParams.getUid().trim())) {
            if (null != zipCode && !"".equals(zipCode)) {
                postDataBuilder.append("<zipCode>").append(zipCode).append("</zipCode>");
            }

//            } else if ((null != latitude && !"".equals(latitude))
//                    && (null != longitude && !"".equals(longitude))) {
//                postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
//                postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
//
//            }
        } else {
            postDataBuilder.append("<zipCode>").append(Constants.getZipCode()).append
                    ("</zipCode>");
        }

        postDataBuilder.append("</Deal>");

        return postDataBuilder.toString();
    }

    public String createDealsGalleryParameter(boolean isSubMenu, String type) {

        return "<Deal>" + "<userId>" + getUid() + "</userId>" + "<hubCitiId>" + getHubCityId() +
                "</hubCitiId>" + "<flag>" + isSubMenu + "</flag>" + "<type>" + type + "</type>" +
                "</Deal>";
    }

    public String createSortFilterListParameter(String module, String latitude,
                                                String longitude, String citiExpId, String srchKey, String cityIds) {

        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<UserDetails>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<module>").append(module).append("</module>");

        if (null != latitude && !"".equals(latitude)) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }

        if (null != longitude && !"".equals(longitude)) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }

        if (null != citiExpId && !"".equals(citiExpId)) {
            postDataBuilder.append("<citiExpId>").append(citiExpId).append("</citiExpId>");
        }

        if (null != cityIds && !"".equals(cityIds)) {
            postDataBuilder.append("<cityIds>").append(cityIds).append("</cityIds>");
        }

        if (null != srchKey && !"".equals(srchKey)) {
            postDataBuilder.append("<srchKey>").append(srchKey).append("</catIds>");
        }

        postDataBuilder.append("</UserDetails>");
        return postDataBuilder.toString();

    }

    public String createSortFilterListParameterAll(String module,
                                                   String latitude, String longitude, String mItemId, String srchKey,
                                                   String bottomBtnId, String catName, String postalCode,
                                                   String filterId, String menuId, String eventTypeId, int bandId) {

        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<UserDetails>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        postDataBuilder.append("<module>").append(module).append("</module>");

        if (null != eventTypeId && !"".equals(eventTypeId)) {
            postDataBuilder.append("<evtTypeID>").append(eventTypeId).append("</evtTypeID>");
        }
        if (bandId != 0) {
            postDataBuilder.append("<bandID>").append(bandId).append("</bandID>");
        }

        if (null != menuId && !"".equals(menuId)) {
            postDataBuilder.append("<menuId>").append(menuId).append("</menuId>");
        }

        if (null != filterId && !"".equals(filterId)) {
            postDataBuilder.append("<filterId>").append(filterId).append("</filterId>");
        }

        if (null != mItemId && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
        }

        if (null != bottomBtnId && !"".equals(bottomBtnId)) {
            postDataBuilder.append("<bottomBtnId>").append(bottomBtnId).append("</bottomBtnId>");
        }

        if (null != catName && !"".equals(catName)) {
            postDataBuilder.append("<catName>").append(catName).append("</catName>");
        }

        if (null != latitude && !"".equals(latitude) && !latitude.equalsIgnoreCase("null")) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }

        if (null != longitude && !"".equals(longitude) && !longitude.equalsIgnoreCase("null")) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }

        if (null != postalCode && !postalCode.isEmpty()) {
            postDataBuilder.append("<postalCode>").append(postalCode).append("</postalCode>");
        }

        if (null != srchKey && !"".equals(srchKey)) {
            postDataBuilder.append("<srchKey>").append(srchKey).append("</srchKey>");
        }

        postDataBuilder.append("</UserDetails>");
        return postDataBuilder.toString();

    }

    public String createCategoryListParameter(String moduleName,
                                              String cityExpId, String filterId, String mItemId,
                                              String bottomBtnId, String retialId, String retailLocationId,
                                              String fundId, String searchKey, String eventTypeId, int bandId, String radius, String
                                                      latitude, String longitude) {

        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<Filter>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");

        //added by subramanya only for shows screen
        if (moduleName.equalsIgnoreCase("BandEvents"))
        {
            if (radius != null && !radius.equals("")) {
                postDataBuilder.append("<radius>").append(radius).append("</radius>");
            } else {
                postDataBuilder.append("<radius>").append("50").append("</radius>");
            }
        }
        String zipCode = Constants.getZipCode();
        if (moduleName.equalsIgnoreCase("Coupons") || moduleName.equalsIgnoreCase("myaccounts"))
        {
            if(zipCode.isEmpty() || zipCode == null){
                zipCode = CouponsActivty.postalCode;
            }
        }

        if (null != zipCode && !"".equals(zipCode)) {
            postDataBuilder.append("<zipCode>").append(zipCode).append("</zipCode>");
        }
        if (null != latitude && !"".equals(latitude) && !latitude.equalsIgnoreCase("null") &&
                !latitude.equalsIgnoreCase("0.0")) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (null != longitude && !"".equals(longitude) && !longitude.equalsIgnoreCase("null") &&
                !longitude.equalsIgnoreCase("0.0")) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }

        if (null != cityExpId && !"".equals(cityExpId)) {
            postDataBuilder.append("<citiExpId>").append(cityExpId).append("</citiExpId>");
        }
        if (null != eventTypeId && !"".equals(eventTypeId)) {
            postDataBuilder.append("<evtTypeID>").append(eventTypeId).append("</evtTypeID>");
        }
        if (bandId != 0) {
            postDataBuilder.append("<bandID>").append(bandId).append("</bandID>");
        }

        if (null != filterId && !"".equals(filterId)) {
            postDataBuilder.append("<filterId>").append(filterId).append("</filterId>");
        }
        if (null != mItemId && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
        }
        if (null != bottomBtnId && !"".equals(bottomBtnId)) {
            postDataBuilder.append("<bottomBtnId>").append(bottomBtnId).append("</bottomBtnId>");
        }

        if (null != searchKey && !"".equals(searchKey)) {
            postDataBuilder.append("<searchKey>").append(searchKey).append("</searchKey>");
        }

        if (null != retialId && !"".equals(retialId)) {
            postDataBuilder.append("<retailId>").append(retialId).append("</retailId>");
        }

        if (null != fundId && !"".equals(fundId)) {
            postDataBuilder.append("<fundId>").append(fundId).append("</fundId>");
        }

        if (null != retailLocationId && !"".equals(retailLocationId)) {
            postDataBuilder.append("<retailLocationId>").append(retailLocationId).append
                    ("</retailLocationId>");
        }

        postDataBuilder.append("<moduleName>").append(moduleName).append("</moduleName>");

        postDataBuilder.append("</Filter>");

        return postDataBuilder.toString();
    }

    public String createOptionListParameter(String mItemId, String businessId,
                                            String bottomBtnId, String searchKey, String latitude, String longitude) {
        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<Filter>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        if (null != mItemId && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
        }
        if (null != latitude && !"".equals(latitude) && !latitude.equalsIgnoreCase("null") &&
                !latitude.equalsIgnoreCase("0.0")) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (null != longitude && !"".equals(longitude) && !longitude.equalsIgnoreCase("null") &&
                !longitude.equalsIgnoreCase("0.0")) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }
        if (null != bottomBtnId && !"".equals(bottomBtnId)) {
            postDataBuilder.append("<bottomBtnId>").append(bottomBtnId).append("</bottomBtnId>");
        }
        if (null != businessId && !"".equals(businessId)) {
            postDataBuilder.append("<businessId>").append(businessId).append("</businessId>");
        }
        if (null != searchKey && !"".equals(searchKey)) {
            postDataBuilder.append("<searchKey>").append(searchKey).append("</searchKey>");
        }
        postDataBuilder.append("<spType>" + "options" + "</spType>");
        postDataBuilder.append("</Filter>");

        return postDataBuilder.toString();
    }

    public String createInterestsListParameter(String mItemId, String catName,
                                               String spType, String cityExpId, String searchKey, String btmId, String latitude,
                                               String longitude) {
        StringBuilder postDataBuilder = new StringBuilder();

        postDataBuilder.append("<Filter>");
        postDataBuilder.append("<userId>").append(getUid()).append("</userId>");
        postDataBuilder.append("<hubCitiId>").append(getHubCityId()).append("</hubCitiId>");
        if (null != cityExpId && !"".equals(cityExpId)) {
            postDataBuilder.append("<citiExpId>").append(cityExpId).append("</citiExpId>");
        }
        if (null != catName && !"".equals(catName)) {
            postDataBuilder.append("<fCategoryName>").append(catName).append("</fCategoryName>");
        }
        if (null != mItemId && !"".equals(mItemId)) {
            postDataBuilder.append("<mItemId>").append(mItemId).append("</mItemId>");
        }
        if (null != latitude && !"".equals(latitude) && !latitude.equalsIgnoreCase("null") &&
                !latitude.equalsIgnoreCase("0.0")) {
            postDataBuilder.append("<latitude>").append(latitude).append("</latitude>");
        }
        if (null != longitude && !"".equals(longitude) && !longitude.equalsIgnoreCase("null") &&
                !longitude.equalsIgnoreCase("0.0")) {
            postDataBuilder.append("<longitude>").append(longitude).append("</longitude>");
        }
        if (null != btmId && !"".equals(btmId)) {
            postDataBuilder.append("<bottomBtnId>").append(btmId).append("</bottomBtnId>");
        }

        if (null != searchKey && !"".equals(searchKey)) {
            postDataBuilder.append("<searchKey>").append(searchKey).append("</searchKey>");
        }

        postDataBuilder.append("<spType>").append(spType).append("</spType>");
        postDataBuilder.append("</Filter>");

        return postDataBuilder.toString();
    }

    // ******* GOV QA REQUESTS *********//
    public JSONObject createGovQaLoginParam(String emailId, String password) {
        JSONObject mainJsonObj = new JSONObject();

        try {

            JSONObject header = new JSONObject();
            header.put("authKey", Constants.GOV_QA_AUTH_KEY);
            header.put("customerEmail", emailId);
            header.put("password", password);

            mainJsonObj.put("customer", header);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObj;

    }

    public JSONObject createGovQaCustomFieldParam(int typeNo) {
        JSONObject mainJsonObj = new JSONObject();

        try {

            JSONObject header = new JSONObject();
            header.put("authKey", Constants.GOV_QA_AUTH_KEY);
            header.put("typeNo", typeNo);

            mainJsonObj.put("govqa", header);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObj;

    }

    public JSONObject createGovQaSubmitParam(int typeNo, String email_id,
                                             ArrayList<HashMap<String, String>> fieldValues, String name,
                                             String title, String addressOne, String addressTwo, String city,
                                             String state, String zip) {
        JSONObject mainJsonObj = new JSONObject();

        try {

            mainJsonObj.put("id", typeNo);
            mainJsonObj.put("name", name);
            mainJsonObj.put("title", title);
            mainJsonObj.put("contactEmail", email_id);
            mainJsonObj.put("addressOne", addressOne);
            mainJsonObj.put("addressTwo", addressTwo);
            mainJsonObj.put("city", city);
            mainJsonObj.put("state", state);
            mainJsonObj.put("zipCode", zip);

            JSONArray header = new JSONArray();

            for (int i = 0; i < fieldValues.size(); i++) {
                JSONObject subJsonObj = new JSONObject();

                if (fieldValues.get(i).containsKey("fldNo")) {
                    subJsonObj.put("fldNo", fieldValues.get(i).get("fldNo"));
                }

                if (fieldValues.get(i).containsKey("name")) {
                    subJsonObj.put("name", fieldValues.get(i).get("name"));
                }

                if (fieldValues.get(i).containsKey("prompt")) {
                    subJsonObj.put("prompt", fieldValues.get(i).get("prompt"));
                }

                if (fieldValues.get(i).containsKey("required")) {
                    subJsonObj.put("required",
                            fieldValues.get(i).get("required"));
                }

                if (fieldValues.get(i).containsKey("fldValue")) {
                    subJsonObj.put("fldValue",
                            fieldValues.get(i).get("fldValue"));
                }

                header.put(subJsonObj);

            }

            mainJsonObj.put("customFieldData", header);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObj;

    }

    public JSONObject createGovQaMakeRequestparam(String groupBy, String sortBy) {
        JSONObject mainJsonObject = new JSONObject();
        try {

            mainJsonObject.put("groupBy", groupBy);
            mainJsonObject.put("sortBy", sortBy);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObject;
    }

    public JSONObject createGovQaViewMyRequestparam(String email, String filterName) {
        JSONObject mainJsonObject = new JSONObject();
        try {

            mainJsonObject.put("email", email);
            mainJsonObject.put("filterName", filterName);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObject;
    }

    public JSONObject getGovQaViewMyRequestDetailsParam(String refNo) {
        JSONObject mainJsonObject = new JSONObject();
        try {

            mainJsonObject.put("referNum", refNo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObject;
    }

    public JSONObject getGovQaViewMyRequestDetailsUpdateParam(String refNo,
                                                              String key, String value) {
        JSONObject mainJsonObject = new JSONObject();
        try {

            mainJsonObject.put("referNum", refNo);
            mainJsonObject.put("fldName", key);
            mainJsonObject.put("fldValue", value);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObject;
    }

    public JSONObject createGovQaFindInfoCatparam(String authKey) {
        JSONObject mainJsonObject = new JSONObject();
        try {

            mainJsonObject.put("authKey", authKey);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObject;
    }

    public JSONObject createGovQaFindInfoSubCatparam(String authKey,
                                                     int parentID) {
        JSONObject mainJsonObject = new JSONObject();
        try {

            mainJsonObject.put("authKey", authKey);
            mainJsonObject.put("parentID", parentID);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObject;
    }

    public JSONObject createGovQaFindInfoListparam(String authKey,
                                                   String filterA, String filterB, int totalToReturn, String keywords,
                                                   String sortBy) {
        JSONObject mainJsonObject = new JSONObject();
        try {

            mainJsonObject.put("authKey", authKey);
            mainJsonObject.put("filterA", filterA);
            mainJsonObject.put("filterB", filterB);
            mainJsonObject.put("totalToReturn", totalToReturn);
            mainJsonObject.put("keywords", keywords);
            mainJsonObject.put("sortBy", sortBy);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObject;
    }

    public JSONObject createGovQaUserDetailsParam(String email_id) {
        JSONObject mainJsonObj = new JSONObject();

        try {

            JSONObject header = new JSONObject();
            header.put("authKey", Constants.GOV_QA_AUTH_KEY);
            header.put("customerEmail", email_id);

            mainJsonObj.put("customer", header);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainJsonObj;

    }

    public JSONObject createGovQaCreateAccSubmitParam(
            HashMap<String, String> fieldValues) {

        JSONObject mainJsonObj = new JSONObject();

        try {

            JSONObject header = new JSONObject();

            header.put("authKey", Constants.GOV_QA_AUTH_KEY);
            header.put("customerEmail", fieldValues.get("email_id"));
            header.put("customerFirstName", fieldValues.get("fname"));
            header.put("customerLastName", fieldValues.get("lname"));
            header.put("phone", fieldValues.get("phone"));
            header.put("address1", fieldValues.get("home_add"));
            header.put("address2", fieldValues.get("apt_num"));
            header.put("city", fieldValues.get("city"));
            header.put("state", fieldValues.get("state"));
            header.put("zip", fieldValues.get("zipcode"));
            header.put("password", fieldValues.get("password"));

            mainJsonObj.put("customer", header);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mainJsonObj;

    }

    public JSONObject createGovQaEditAccSubmitParam(
            HashMap<String, String> fieldValues) {

        JSONObject mainJsonObj = new JSONObject();

        try {

            JSONObject header = new JSONObject();

            header.put("authKey", Constants.GOV_QA_AUTH_KEY);
            header.put("loginEmail", fieldValues.get("email_id"));
            header.put("password", fieldValues.get("password"));
            header.put("firstName", fieldValues.get("fname"));
            header.put("lastName", fieldValues.get("lname"));
            header.put("phone", fieldValues.get("phone"));
            header.put("groupAccessName", fieldValues.get("groupAccessName"));
            header.put("custCFName1", fieldValues.get("custCFName1"));
            header.put("custCFValue1", fieldValues.get("custCFValue1"));
            header.put("custCFName2", fieldValues.get("custCFName2"));
            header.put("custCFValue2", fieldValues.get("custCFValue2"));
            header.put("custCFName3", fieldValues.get("custCFName3"));
            header.put("custCFValue3", fieldValues.get("custCFValue3"));
            header.put("custCFName4", fieldValues.get("custCFName4"));
            header.put("custCFValue4", fieldValues.get("custCFValue4"));
            header.put("custCFName5", fieldValues.get("custCFName5"));
            header.put("custCFValue5", fieldValues.get("custCFValue5"));

            mainJsonObj.put("customerInfo", header);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mainJsonObj;

    }

    @SuppressWarnings("deprecation")
    public HashMap<String, String> getGPSValues(Activity activity) {
        HashMap<String, String> gpsValues = new HashMap<>();
        LocationManager locationManager = (LocationManager) activity
                .getSystemService(Context.LOCATION_SERVICE);
        String provider = Settings.Secure.getString(
                activity.getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps")) {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    CommonConstants.MINIMUM_TIME_BETWEEN_UPDATES,
                    CommonConstants.MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                    new ScanSeeLocListener());
            Location locNew = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (locNew != null) {

                gpsValues.put("latitude", String.valueOf(locNew.getLatitude()));
                gpsValues.put("longitude",
                        String.valueOf(locNew.getLongitude()));

            } else {
                // N/W Tower Info Start
                locNew = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (locNew != null) {
                    gpsValues.put("latitude",
                            String.valueOf(locNew.getLatitude()));
                    gpsValues.put("longitude",
                            String.valueOf(locNew.getLongitude()));
                } else {
                    gpsValues.put("latitude", CommonConstants.LATITUDE);
                    gpsValues.put("longitude", CommonConstants.LONGITUDE);

                }
            }
        }

        return gpsValues;
    }

    public static UserInfoRequestModel getUserRequest() {
        Log.d("UrlRequestParams", "####### userId ## : " + getUid() + " ## hubcity id ##" +
                getHubCityId());
        return new UserInfoRequestModel(getUid(), getHubCityId());
    }

    private static void setAppVersion(Activity activity) {
        try {
            SharedPreferences preference = activity.getSharedPreferences(Constants
                    .APP_INFO_PREFERENCE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preference.edit();
            editor.putString(Constants.APP_VERSION, Constants.getAppVersion());
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static BookMarkModel getBookMarkDetails(String isBkMark, String linkID, String level) {
        return new BookMarkModel(getHubCityId(), getUid(), isBkMark, linkID, level);
    }

    public static ScrollingPageModel getscroltemplate(String catName, int lowerLimit, String
            isSidebar, String
                                                              dateCreated, String level, String linkId) {
        return new ScrollingPageModel(getUid(), getHubCityId(), catName, lowerLimit, isSidebar,
                dateCreated,
                level, linkId);
    }

    public static SideMenuModel getSideMenuDetails(String mLinkID, String mLevel, String sideNaviPersonalizatn) {
        return new SideMenuModel(getHubCityId(), getUid(), sideNaviPersonalizatn);
    }

    public static UpdateModel getUpdateDetails(String bkMarkOrder, String navigOrder) {
        return new UpdateModel(getHubCityId(), getUid(), bkMarkOrder, navigOrder);
    }

    public static NewsSideMenu getSideMenu(String linkID, String level, String sideNaviPersonalizatn) {
        return new NewsSideMenu(getHubCityId(), getUid(), linkID, level, sideNaviPersonalizatn);
    }


    public static CombinationalTempRequest getComboTemplate(int lowerLimit, String dateCreated,
                                                            String linkID, String level) {
        return new CombinationalTempRequest(getUid(), getHubCityId(), lowerLimit, dateCreated,
                linkID, level);
    }


    public static SubPageModel getnewsubcategories(String catName) {
        return new SubPageModel(getUid(), catName);
    }

    public TileTemplateRequestModel getTileTempRequestObject(String dateCreated, String linkID,
                                                             String level) {
        return new TileTemplateRequestModel(getUid(), getHubCityId(), dateCreated, linkID, level);

    }

    // request method for Band Event and bandEventEmail

    public String createBandEventSummeryParam(String eventId) {
        StringBuilder postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ShareProductInfo>");
        postDataBuilder.append("<platform>Android</platform>");
        postDataBuilder.append("<eventId>" + eventId + "</eventId>");
        postDataBuilder.append("<hubCitiId>" + getHubCityId() + "</hubCitiId>");
        postDataBuilder.append("<userId>" + getUid() + "</userId>");
        postDataBuilder.append("</ShareProductInfo>");
        return postDataBuilder.toString();
    }

    // request method for band summery createHotDealsShareParam
    public String createBandSummeryParam(String bandId) {
        StringBuilder postDataBuilder = new StringBuilder();
        postDataBuilder.append("<ShareProductInfo>");
        postDataBuilder.append("<platform>Android</platform>");
        postDataBuilder.append("<bandId>" + bandId + "</bandId>");
        postDataBuilder.append("<hubCitiId>" + getHubCityId() + "</hubCitiId>");
        postDataBuilder.append("<userId>" + getUid() + "</userId>");
        postDataBuilder.append("</ShareProductInfo>");
        return postDataBuilder.toString();

    }

    //claim Bussiness
    public static ClaimModel sendClaimRequest(String loginEmail, String retailLocationID, String business, String typeBusiness, String website, String address, String address1,
                                              String city, String state, String postalCode, String country, String phone, String custInfo, String custEmail,
                                              String keyword, String mailOptnAdd1, String mailOptnAdd2, String mailOptnCity, String mailOptnState, String mailOptnPostalCode,
                                              String mailOptnCountry, String mailOptnPhone, String isMailAddress) {
        return new ClaimModel(loginEmail, getHubCityId(), retailLocationID, business, typeBusiness, website, address, address1, city, state,
                postalCode, country, phone, custInfo, custEmail, getUid(), keyword, mailOptnAdd1, mailOptnAdd2, mailOptnCity, mailOptnState,
                mailOptnPostalCode, mailOptnCountry, mailOptnPhone,isMailAddress);
    }

    public static ClaimGetDetails getClaimBusiness(String retailLocationID) {
        return new ClaimGetDetails(retailLocationID,
                getUid(), getHubCityId());
    }

    //multiple bands
    public static MultipleBandModel sendMultipleBands(String bEvtId, int lowerLimit) {
        return new MultipleBandModel(getUid(), getHubCityId(), bEvtId, lowerLimit);
    }

//Coupon
    public static CouponModel sendCoupon(String sortColumn, String isFeatOrNonFeat, String lastVisitedNo, String sortOrder, String searchKey, String latitude,
                                         String longitude, String catIds, String cityIds) {
        String zipcode;
        if(Constants.getZipCode().isEmpty() || Constants.getZipCode() == null)
        {
            zipcode = CouponsActivty.postalCode;;
        }
        else
        {
            zipcode = Constants.getZipCode();
        }
        return new CouponModel(getUid(), getHubCityId(), sortColumn,zipcode,isFeatOrNonFeat,lastVisitedNo,sortOrder,searchKey,latitude,longitude,catIds,cityIds);
    }
    public static MyCouponModel sendMyCoupon(String sortColumn, String type, String lastVisitedNo, String sortOrder, String searchKey, String latitude,
                                             String longitude, String catIds, String cityIds) {
        String zipcode;
        if(Constants.getZipCode().isEmpty() || Constants.getZipCode() == null)
        {
            zipcode = CouponsActivty.postalCode;;
        }
        else
        {
            zipcode = Constants.getZipCode();
        }
        return new MyCouponModel(getUid(), getHubCityId(), sortColumn,zipcode,type,lastVisitedNo,sortOrder,searchKey,latitude,longitude,catIds,cityIds);
    }

    public static MapModel sendMapReq(String couponIds) {
        String zipcode;
        if(Constants.getZipCode().isEmpty() || Constants.getZipCode() == null)
        {
            zipcode = CouponsActivty.postalCode;
        }
        else
        {
            zipcode = Constants.getZipCode();
        }
        return new MapModel(getUid(),getHubCityId(),zipcode,couponIds);
    }

    public static RedeemGetDetails getRedeemBusiness(String couponId) {
        return new RedeemGetDetails(couponId,getUid());

    }

    public static AddCouponDetails getAddCouponsBusiness(String couponId) {
        return new AddCouponDetails(couponId,getUid());

    }

    public static CouponDetails getCouponsDetails(String couponId,String couponListId) {
        return new CouponDetails(getUid(),couponId, couponListId,getHubCityId());
    }
}
