package com.hubcity.android.commonUtil;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;


import com.scansee.hubregion.R;
public class ImageLoaderAsync extends AsyncTask<String, Void, Bitmap> {
	ImageView bmImage;
	private RotateAnimation anim;
	boolean isImageFitXY = false;

	public ImageLoaderAsync(ImageView bmImage) {
		this.bmImage = bmImage;
		anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);
		this.bmImage.setAnimation(null);
		this.bmImage.setImageResource(R.drawable.loading_button);
		this.bmImage.startAnimation(anim);
		this.bmImage.setAnimation(anim);
	}

	public ImageLoaderAsync(ImageView bmImage, boolean isImageFitXY) {
		this.isImageFitXY = isImageFitXY;
		this.bmImage = bmImage;
		anim = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);
		this.bmImage.setAnimation(null);
		this.bmImage.setImageResource(R.drawable.loading_button);
		this.bmImage.startAnimation(anim);
		this.bmImage.setAnimation(anim);
	}

	protected Bitmap doInBackground(String... urls) {
	
		String urldisplay = urls[0];
		Log.v("","IMAGE URL : "+urldisplay);
		Bitmap mIcon11 = null;
		if(urldisplay != null)
		{
			urldisplay = urldisplay.replaceAll(" ", "%20");

			
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
				if (mIcon11 == null) {
					URL url = new URL(urldisplay);
					HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
					ucon.setInstanceFollowRedirects(false);
					URL secondURL = new URL(ucon.getHeaderField("Location"));
					mIcon11 = BitmapFactory.decodeStream(secondURL.openConnection().getInputStream());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		

		return mIcon11;
	}

	protected void onPostExecute(Bitmap result) {
		bmImage.setImageBitmap(result);
		bmImage.setAnimation(null);
		if (this.isImageFitXY) {
			bmImage.setScaleType(ScaleType.FIT_XY);
		}
	}
}
