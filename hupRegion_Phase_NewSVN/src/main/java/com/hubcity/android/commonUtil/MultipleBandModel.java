package com.hubcity.android.commonUtil;

/**
 * Created by subramanya.v on 9/20/2016.
 */
public class MultipleBandModel
{
	private final String userId;
	private final String hubCitiId;
	private final String bEvtId;
	private final int lowerLimit;

	public MultipleBandModel(String userId, String hubCitiId, String bEvtId, int lowerLimit)
	{
		this.userId = userId;
		this.hubCitiId = hubCitiId;
		this.bEvtId = bEvtId;
		this.lowerLimit = lowerLimit;
	}
}
