package com.hubcity.android.commonUtil;

/**
 * Created by subramanya.v on 8/23/2016.
 */
@SuppressWarnings("DefaultFileTemplate")
public class ClaimModel {


    private final String loginEmail;
    private final String hubCitiId;
    private final String retLocationId;
    private final String name;
    private final String type;
    private final String website;
    private final String retAddress;
    private final String retAddress2;
    private final String city;
    private final String state;
    private final String postalCode;
    private final String country;
    private final String phone;
    private final String custInfo;
    private final String custEmail;
    private final String userId;
    private final String keywords;
    private final String mailAddress;
    private final String mailAddress2;
    private final String mailCity;
    private final String mailState;
    private final String mailPostalCode;
    private final String mailCountry;
    private final String mailPhone;
    private final String isMailAddress;

    public ClaimModel(String loginEmail, String hubCitiId, String retailLocationID, String business, String typeBusiness, String website, String address,
                      String address1, String city, String state, String postalCode, String country, String phone, String custInfo, String custEmail,
                      String userId, String Keywords, String mailAddress, String mailAddress2, String mailCity, String mailState, String mailOptnPostalCode,
                      String mailOptnCountry, String mailPhone, String isMailAddress) {
        this.loginEmail = loginEmail;
        this.hubCitiId = hubCitiId;
        this.retLocationId = retailLocationID;
        this.name = business;
        this.type = typeBusiness;
        this.website = website;
        this.retAddress = address;
        this.retAddress2 = address1;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
        this.phone = phone;
        this.custInfo = custInfo;
        this.custEmail = custEmail;
        this.userId = userId;

        this.keywords = Keywords;
        this.mailAddress = mailAddress;
        this.mailAddress2 = mailAddress2;
        this.mailCity = mailCity;
        this.mailState = mailState;
        this.mailPostalCode = mailOptnPostalCode;
        this.mailCountry = mailOptnCountry;
        this.mailPhone = mailPhone;
        this.isMailAddress = isMailAddress;


    }
}
