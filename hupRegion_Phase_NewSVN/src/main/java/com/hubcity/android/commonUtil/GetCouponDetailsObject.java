package com.hubcity.android.commonUtil;

/**
 * Created by shruthi.n on 12/6/2016.
 */
public class GetCouponDetailsObject {
    private String responseCode;
    private String responseText;
    private String viewOnWeb;

    public String getCouponDesc() {
        return couponDesc;
    }

    public void setCouponDesc(String couponDesc) {
        this.couponDesc = couponDesc;
    }

    private String couponDesc;


    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getViewOnWeb() {
        return viewOnWeb;
    }

    public void setViewOnWeb(String viewOnWeb) {
        this.viewOnWeb = viewOnWeb;
    }



    public String getCouponURL() {
        return couponURL;
    }

    public void setCouponURL(String couponURL) {
        this.couponURL = couponURL;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getCouponStartDate() {
        return couponStartDate;
    }

    public void setCouponStartDate(String couponStartDate) {
        this.couponStartDate = couponStartDate;
    }

    public String getCouponExpireDate() {
        return couponExpireDate;
    }

    public void setCouponExpireDate(String couponExpireDate) {
        this.couponExpireDate = couponExpireDate;
    }

    public String getCouponImagePath() {
        return couponImagePath;
    }

    public void setCouponImagePath(String couponImagePath) {
        this.couponImagePath = couponImagePath;
    }

    public String getRetName() {
        return retName;
    }

    public String getTermAndConditions() {
        return termAndConditions;
    }

    public void setTermAndConditions(String termAndConditions) {
        this.termAndConditions = termAndConditions;
    }

    public void setRetName(String retName) {
        this.retName = retName;
    }



    private String termAndConditions;
    private String usedFlag;

    public String getBannerName() {
        return bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    private String bannerName;
    private String couponURL;
    private String couponId;
    private String couponName;
    private String longDescription;
    private String couponStartDate;
    private String couponExpireDate;
    private String couponImagePath;
    private String retName;
    private String expireFlag;

    public String getProdFlag() {
        return prodFlag;
    }

    public void setProdFlag(String prodFlag) {
        this.prodFlag = prodFlag;
    }

    public String getExpireFlag() {
        return expireFlag;
    }

    public void setExpireFlag(String expireFlag) {
        this.expireFlag = expireFlag;
    }

    public String getUsedFlag() {
        return usedFlag;
    }

    public void setUsedFlag(String usedFlag) {
        this.usedFlag = usedFlag;
    }

    public String getLocatnFlag() {
        return locatnFlag;
    }

    public void setLocatnFlag(String locatnFlag) {
        this.locatnFlag = locatnFlag;
    }

    private String prodFlag;
    private String locatnFlag;

}
