package com.hubcity.android.govqa;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

/**
 * @author Subramanya Padiyar V
 *         <p/>
 *         This page list down the request
 */
public class GovQaMakeRequest extends CustomTitleBar {

    Activity activity;
    LinearLayout linearLayout = null;
    BottomButtons bb = null;
    JSONObject bbJsonObj;
    ArrayList<GovQaMakeReqObj> arrSortAndFilter;
    GovQaMakeReqAdapter listAdapterview;
    ExpandableListView expandable_list;
    ListView listView;

    String groupby = "type";
    String sortby = "name";
    String flag = "type";
    String email_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.govqa_make_req);

            Bundle bundle = getIntent().getExtras();
            String Header = bundle.getString("ListHeader");

            if (getIntent().hasExtra("email_id")) {
                email_id = getIntent().getExtras().getString("email_id");
            }

            // Title of the screen
            title.setText(Header);

            listView = (ListView) findViewById(R.id.makereq_list);

            // visible the sort button
            rightImage.setBackgroundResource(R.drawable.ic_action_sort_by_size);
            rightImage.setTag("Sort");

            // Getting parameter through asynchronous task
            new GetMakeRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            // onclick on listview of makerequest
            listView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    if (!arrSortAndFilter.get(position).isHeader()) {

                        Intent intent = new Intent(GovQaMakeRequest.this,
                                GovQaDetailScreen.class);
                        if (getIntent().hasExtra("BottomBtnJsonObj")) {
                            intent.putExtra("BottomBtnJsonObj", getIntent()
                                    .getExtras().getString("BottomBtnJsonObj"));
                        }
                        intent.putExtra("email_id", getIntent().getExtras()
                                .getString("email_id"));

                        intent.putExtra("customer_id", getIntent().getExtras()
                                .getString("customer_id"));
                        intent.putExtra("typeNo", arrSortAndFilter.get(position)
                                .getId());
                        intent.putExtra("serviceType",
                                arrSortAndFilter.get(position).getFilterName());
                        intent.putExtra("description",
                                arrSortAndFilter.get(position).getNotes());

                        startActivity(intent);
                    }

                }
            });

            // onclick on sort button of makerequest
            rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if ("Sort".equals(rightImage.getTag())) {

                        Intent intent = new Intent(GovQaMakeRequest.this,
                                GovQaMakeRequestSort.class);
                        intent.putExtra("flag", flag);
                        intent.putExtra("groupBy", groupby);
                        startActivityForResult(intent, 2);// Activity is started
                        // with requestCode 2
                    }

                }
            });

            // Back action
            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    finish();
                }
            });
            BottomButton();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 2) {
            String flagmsg = data.getExtras().getString("flag");
            String groupmsg = data.getStringExtra("groupBy");
            String sortmsg = data.getStringExtra("sortBy");

            groupby = groupmsg;
            sortby = sortmsg;
            flag = flagmsg;

            new GetMakeRequest()
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }
    }

    @SuppressLint("ResourceAsColor")
    private void BottomButton() {
        /*** Implementation of Bottom buttons ***/
        activity = GovQaMakeRequest.this;
        linearLayout = (LinearLayout) findViewById(R.id.makereq_list_parent);
        linearLayout.setBackgroundColor(R.color.light_gray);

        // Initiating Bottom button class
        bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
        // Add screen name when needed
        bb.setActivityInfo(activity, "Make Request");

        if (getIntent().hasExtra("BottomBtnJsonObj")
                && getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

            try {
                bbJsonObj = new JSONObject(getIntent().getExtras().getString(
                        "BottomBtnJsonObj"));

                ArrayList<BottomButtonBO> bottomButtonList = bb
                        .parseForBottomButton(bbJsonObj);
                if (bottomButtonList != null) {
                    BottomButtonListSingleton.clearBottomButtonListSingleton();
                    BottomButtonListSingleton
                            .getListBottomButton(bottomButtonList);
                } else {
                    if (MenuAsyncTask.bottomButtonList != null) {
                        BottomButtonListSingleton
                                .clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(MenuAsyncTask.bottomButtonList);
                    }
                }

                bb.createbottomButtontTab(linearLayout, false);

                linearLayout.setVisibility(View.VISIBLE);

            } catch (Exception e) {
                e.printStackTrace();
                linearLayout.setVisibility(View.GONE);
            }

        } else {
            linearLayout.setVisibility(View.GONE);
        }

    }

    private class GetMakeRequest extends AsyncTask<String, Void, String> {

        JSONObject jsonObjectGovQaMakeReq;
        ServerConnections mServerConnections = new ServerConnections();
        UrlRequestParams urlRequestParams = new UrlRequestParams();

        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = ProgressDialog.show(GovQaMakeRequest.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.equalsIgnoreCase("true")) {
                    listAdapterview = new GovQaMakeReqAdapter(
                            GovQaMakeRequest.this, arrSortAndFilter);
                    listView.setAdapter(listAdapterview);
                    mDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "false";
            String url = Properties.url_local_server
                    + Properties.hubciti_version + "govqa/makerequest";
            arrSortAndFilter = new ArrayList<>();
            try {
                JSONObject urlParameters = urlRequestParams
                        .createGovQaMakeRequestparam(groupby, sortby);
                jsonObjectGovQaMakeReq = mServerConnections
                        .getUrlJsonPostResponse(url, urlParameters);

                try {
                    boolean isJsonTableGroup = isJSONArray(
                            jsonObjectGovQaMakeReq, "tableGroup");
                    if (isJsonTableGroup) {
                        JSONArray jsontableGroup = jsonObjectGovQaMakeReq
                                .getJSONArray("tableGroup");

                        for (int index = 0; index < jsontableGroup.length(); index++) {
                            JSONObject jsonGroupobj = jsontableGroup
                                    .getJSONObject(index);
                            GovQaMakeReqObj GovQaMakeReqObj = new GovQaMakeReqObj();
                            GovQaMakeReqObj.setFilterName(jsonGroupobj
                                    .getString("groupContent"));
                            GovQaMakeReqObj.setHeader(true);
                            arrSortAndFilter.add(GovQaMakeReqObj);

                            boolean isJsonTableGroup1 = isJSONArray(
                                    jsonGroupobj, "arServiceReqTypeList");
                            if (isJsonTableGroup1) {
                                JSONArray mainelems = jsonGroupobj
                                        .getJSONArray("arServiceReqTypeList");
                                for (int subindex = 0; subindex < mainelems
                                        .length(); subindex++) {

                                    JSONObject elems = mainelems
                                            .getJSONObject(subindex);
                                    if (elems != null) {
                                        GovQaMakeReqObj filterArdSort = new GovQaMakeReqObj();
                                        filterArdSort.setHeader(false);
                                        filterArdSort.setFilterName(elems
                                                .getString("name"));
                                        filterArdSort.setNotes("");
                                        filterArdSort.setId(elems
                                                .getInt("typeNo"));
                                        arrSortAndFilter.add(filterArdSort);
                                    }
                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                result = "true";

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // checking the object is an Array
    public boolean isJSONArray(JSONObject jsonResponse, String value) {
        boolean isArray = false;

        JSONObject isJSONObject = jsonResponse.optJSONObject(value);
        if (isJSONObject == null) {
            JSONArray isJSONArray = jsonResponse.optJSONArray(value);
            if (isJSONArray != null) {
                isArray = true;
            }
        }
        return isArray;
    }

    @Override
    protected void onResume() {

        super.onResume();
        // Add screen name when needed
        if (null != bb) {
            bb.setActivityInfo(activity, "Make Request");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
