package com.hubcity.android.govqa;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.hubregion.R;

/**
 * @author supriya.m This is static screen from where user can navigate to View
 *         My Requests and Edit Account screen and also they can logout from
 *         this screen
 */
public class GovQaViewMyRequest extends CustomTitleBar {

	Activity activity;
	LinearLayout linearLayout = null;
	BottomButtons bb = null;
	JSONObject bbJsonObj;
	String email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gov_qa_view_my_request);

		try {
			/******* Setting the screen title ********/
			title.setText("View My Requests");

			/****** Initializing of Bottom buttons ******/
			bottomButton();

			/****** creating the click listener ******/
			setListener();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setListener() {

		/******* Back action *******/
		backImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		rightImage.setVisibility(View.GONE);

		email = getIntent().getExtras().getString("email");

		findViewById(R.id.view_my_req_item_layout)
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(GovQaViewMyRequest.this,
								GovQaViewMyReqDetails.class);

						if (bbJsonObj != null && bbJsonObj.length() > 0) {
							intent.putExtra("BottomBtnJsonObj",
									bbJsonObj.toString());
						}
						intent.putExtra("email", email);
						startActivity(intent);
					}
				});
		findViewById(R.id.view_my_req_edit_item_layout)
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(GovQaViewMyRequest.this,
								GovQaCreateEditAccount.class);

						if (bbJsonObj != null && bbJsonObj.length() > 0) {
							intent.putExtra("BottomBtnJsonObj",
									bbJsonObj.toString());
						}

						// For Testing
						intent.putExtra("title", "Edit Account Info");
						intent.putExtra("email_id", email);
						startActivity(intent);
					}
				});
		findViewById(R.id.view_my_req_logout_item_layout)
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Constants.GOV_QA_DONT_ALLOW_LOGIN = true;
						callMainMenu();
					}
				});
	}

	public void callMainMenu() {

		if (SortDepttNTypeScreen.subMenuDetailsList != null) {
			SortDepttNTypeScreen.subMenuDetailsList.clear();
			SubMenuStack.clearSubMenuStack();
			SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
		}
		LoginScreenViewAsyncTask.bIsLoginFlag = true;
		callingMainMenu();

	}

	private void bottomButton() {
		/****** Implementation of Bottom buttons ******/
		activity = GovQaViewMyRequest.this;
		linearLayout = (LinearLayout) findViewById(R.id.govqa_view_my_req_parent_layout);
		// linearLayout.setBackgroundColor(R.color.light_gray);

		// Initiating Bottom button class
		bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
		// Add screen name when needed
		bb.setActivityInfo(activity, "View My Request");

		if (getIntent().hasExtra("BottomBtnJsonObj")
				&& getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

			try {
				bbJsonObj = new JSONObject(getIntent().getExtras().getString(
						"BottomBtnJsonObj"));

				ArrayList<BottomButtonBO> bottomButtonList = bb
						.parseForBottomButton(bbJsonObj);
				if (bottomButtonList != null) {
					BottomButtonListSingleton.clearBottomButtonListSingleton();
					BottomButtonListSingleton
							.getListBottomButton(bottomButtonList);
				} else {
					if (MenuAsyncTask.bottomButtonList != null) {
						BottomButtonListSingleton
								.clearBottomButtonListSingleton();
						BottomButtonListSingleton
								.getListBottomButton(MenuAsyncTask.bottomButtonList);
					}
				}

				bb.createbottomButtontTab(linearLayout, false);

				linearLayout.setVisibility(View.VISIBLE);

			} catch (Exception e) {
				e.printStackTrace();
				linearLayout.setVisibility(View.GONE);
			}

		} else {
			linearLayout.setVisibility(View.GONE);
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// Clearing Memory
		linearLayout = null;
		bb = null;
		bbJsonObj = null;
		email = null;
	}

}
