package com.hubcity.android.govqa;

import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.HubCityContext;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;

/**
 * Get User details
 * 
 * @author rekha_p
 * 
 */
public class GovQAUserDetailsAsyncTask extends
		AsyncTask<Void, Void, HashMap<String, String>> {
	private ProgressDialog mDialog;
	String responseText = "";
	String responseCode = "";
	String email_id = "";

	UrlRequestParams urlRequestParams = new UrlRequestParams();
	ServerConnections mServerConnections = new ServerConnections();

	HashMap<String, String> customer_info = new HashMap<>();
	Activity activity;

	public GovQAUserDetailsAsyncTask(Activity activity, String email_id) {
		this.activity = activity;
		this.email_id = email_id;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mDialog = ProgressDialog.show(activity, "", Constants.DIALOG_MESSAGE,
				true);
		mDialog.setCancelable(false);
	}

	@Override
	protected HashMap<String, String> doInBackground(Void... params) {
		try {
			String gov_qa_customer_details = Properties.url_local_server
					+ Properties.hubciti_version + "govqa/customerinfo";
			JSONObject urlParameters = urlRequestParams
					.createGovQaUserDetailsParam(email_id);

			customer_info = new HashMap<>();

			JSONObject jsonResponse = mServerConnections.getUrlJsonPostResponse(
					gov_qa_customer_details, urlParameters);

			if (jsonResponse != null) {
				if (jsonResponse.has("response")) {
					JSONObject responseJson = jsonResponse
							.getJSONObject("response");

					if (responseJson.has("responseCode")) {
						responseCode = responseJson.getString("responseCode");
					}

					if (responseJson.has("responseText")) {
						responseText = responseJson.getString("responseText");
					}

					JSONObject customerObject = responseJson
							.getJSONObject("customer");

					if (customerObject.has("id")) {
						customer_info.put("id", customerObject.getString("id"));
					}

					if (customerObject.has("first")) {
						customer_info.put("first",
								customerObject.getString("first"));
					}

					if (customerObject.has("last")) {
						customer_info.put("last",
								customerObject.getString("last"));
					}

					if (customerObject.has("phone")) {
						customer_info.put("phone",
								customerObject.getString("phone"));
					}

					if (customerObject.has("addressOne")) {
						customer_info.put("addressOne",
								customerObject.getString("addressOne"));
					}

					if (customerObject.has("addressTwo")) {
						customer_info.put("addressTwo",
								customerObject.getString("addressTwo"));
					}

					if (customerObject.has("city")) {
						customer_info.put("city",
								customerObject.getString("city"));
					}

					if (customerObject.has("state")) {
						customer_info.put("state",
								customerObject.getString("state"));
					}

					if (customerObject.has("zip")) {
						customer_info.put("zip",
								customerObject.getString("zip"));
					}

					if (customerObject.has("groupName")) {
						customer_info.put("groupName",
								customerObject.getString("groupName"));
					}

					if (customerObject.has("email")) {
						customer_info.put("email",
								customerObject.getString("email"));
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return customer_info;

	}

	@Override
	protected void onPostExecute(HashMap<String, String> result) {
		super.onPostExecute(result);
		try {
			mDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
		HubCityContext.govqa_customer_info = customer_info;
	}

}
