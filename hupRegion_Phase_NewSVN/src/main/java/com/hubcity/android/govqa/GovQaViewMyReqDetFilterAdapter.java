package com.hubcity.android.govqa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;

import java.util.ArrayList;

/**
 * Created by subramanya.v on 1/4/2016.
 */
public class GovQaViewMyReqDetFilterAdapter extends BaseAdapter
{
	Context mContext;
	ArrayList<GovQaViewMyReqFilterObj> mArrFilter;
	public GovQaViewMyReqDetFilterAdapter(GovQaViewMyReqDetailsFilter govQaViewMyReqDetailsFilter, ArrayList<GovQaViewMyReqFilterObj> arrFilter)

	{
		this.mContext=govQaViewMyReqDetailsFilter;
		this.mArrFilter=arrFilter;
	}

	@Override
	public int getCount()
	{
		return mArrFilter.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mArrFilter.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater infalInflater = (LayoutInflater) this.mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TextView filter_name;
		ImageView checkbox;
		if(mArrFilter.get(position).isHeader())
		{
			convertView = infalInflater.inflate(
					R.layout.govqa_viewmy_req_filtr_header, null);
			filter_name = (TextView) convertView
					.findViewById(R.id.view_myreq_filter_header);
		}
		else
		{
			convertView = infalInflater.inflate(
					R.layout.govqa_viewmy_req_filtr_adapter, null);
			filter_name = (TextView) convertView
					.findViewById(R.id.view_myreq_filterby);
			checkbox=(ImageView)convertView.findViewById(R.id.view_myreq_checkox);
			if(mArrFilter.get(position).ischecked()) {
				checkbox
						.setImageResource(R.drawable.ic_done_black_24dp);
			}
		}
		filter_name.setText(mArrFilter.get(position).getFilterBy());
		return convertView;
	}
	public void updateArray(ArrayList<GovQaViewMyReqFilterObj> arrSortAndFilter) {
		this.mArrFilter = arrSortAndFilter;
	}
}
