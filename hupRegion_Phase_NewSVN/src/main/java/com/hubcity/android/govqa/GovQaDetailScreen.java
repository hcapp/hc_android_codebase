package com.hubcity.android.govqa;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

/**
 * Class to display custom fields in Make a Request Details screen
 * 
 * @author rekha_p
 * 
 */
public class GovQaDetailScreen extends CustomTitleBar {

	ArrayList<HashMap<String, String>> fieldValues = new ArrayList<>();

	DetailsAsyncTask detailsAsyncTask;
	SubmitAsyncTask submitAsyncTask;

	String serviceType = "", description = "", email_id = "";
	int typeNo;
	boolean isFirst;

	LinearLayout main_layout;
	Activity activity;
	LinearLayout linearLayout;
	JSONObject bbJsonObj;
	BottomButtons bb = null;

	Spinner serviceState;
	EditText serviceAdd1;
	EditText serviceAdd2;
	EditText serviceCity;
	EditText serviceZipCode;

	String state = "", addr1 = "", addr2 = "", city = "", zipcode = "";

	ArrayList<String> submitValues = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.govqa_make_req_details);

		try {
			isFirst = true;

			hideKeyboard();

			if (getIntent().hasExtra("serviceType")) {
                serviceType = getIntent().getExtras().getString("serviceType");
            }

			if (getIntent().hasExtra("description")) {
                description = getIntent().getExtras().getString("description");
            }

			if (getIntent().hasExtra("email_id")) {
                email_id = getIntent().getExtras().getString("email_id");
            }

			if (getIntent().hasExtra("typeNo")) {

                typeNo = getIntent().getExtras().getInt("typeNo");
            }

			title.setText(serviceType);

			rightImage.setVisibility(View.GONE);

			// Instantiating Fields
			serviceState = (Spinner) findViewById(R.id.service_state_value);
			serviceState.setSelection(43);

			serviceAdd1 = (EditText) findViewById(R.id.service_add_value);
			serviceAdd2 = (EditText) findViewById(R.id.service_add_2_value);
			serviceCity = (EditText) findViewById(R.id.service_city_value);
			serviceZipCode = (EditText) findViewById(R.id.service_zip_value);

			// Back Action
			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    cancelAsyncTask();
                    finish();
                }
            });

			main_layout = (LinearLayout) findViewById(R.id.custom_layout);
			main_layout.setVisibility(View.INVISIBLE);

			/*** Implementation of Bottom buttons ***/
			activity = GovQaDetailScreen.this;
			linearLayout = (LinearLayout) findViewById(R.id.bottom_ll);

			// Initiating Bottom button class
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "Detailscreen");

			if (getIntent().hasExtra("BottomBtnJsonObj")
                    && getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

                try {
                    bbJsonObj = new JSONObject(getIntent().getExtras().getString(
                            "BottomBtnJsonObj"));

                    ArrayList<BottomButtonBO> bottomButtonList = bb
                            .parseForBottomButton(bbJsonObj);
                    if (bottomButtonList != null) {
                        BottomButtonListSingleton.clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                    } else {
                        if (MenuAsyncTask.bottomButtonList != null) {
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(MenuAsyncTask.bottomButtonList);
                        }
                    }

                    bb.createbottomButtontTab(linearLayout, false);

                    linearLayout.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    linearLayout.setVisibility(View.GONE);
                }

            } else {
                linearLayout.setVisibility(View.GONE);
            }

			callDetailsAsyncTask();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Call for Details AsyncTask
	private void callDetailsAsyncTask() {

		if (isFirst) {
			detailsAsyncTask = new DetailsAsyncTask();
			detailsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			isFirst = false;
			return;
		}

		if (detailsAsyncTask != null) {
			if (!detailsAsyncTask.isCancelled()) {
				detailsAsyncTask.cancel(true);
			}

			detailsAsyncTask = null;
		}

		detailsAsyncTask = new DetailsAsyncTask();
		detailsAsyncTask.execute();

	}

	// Cancel Async Task
	private void cancelAsyncTask() {
		if (detailsAsyncTask != null && !detailsAsyncTask.isCancelled()) {
			detailsAsyncTask.cancel(true);
		}

		detailsAsyncTask = null;
	}

	/**
	 * Details Async Task. Gets details of Make a Request item
	 * 
	 * @author rekha_p
	 * 
	 */
	public class DetailsAsyncTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog mDialog;
		private JSONObject jsonResponse = null;
		String responseText = "";
		String responseCode = "";

		UrlRequestParams urlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mDialog = ProgressDialog.show(GovQaDetailScreen.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				String gov_qa_custom_fields = Properties.url_local_server
						+ Properties.hubciti_version
						+ "govqa/servicereqcustomfields";

				JSONObject urlParameters = urlRequestParams
						.createGovQaCustomFieldParam(typeNo);

				jsonResponse = mServerConnections.getUrlJsonPostResponse(
						gov_qa_custom_fields, urlParameters);

				if (jsonResponse != null) {

					if (jsonResponse.has("response")) {
						JSONObject jsonValues = jsonResponse
								.getJSONObject("response");

						if (jsonValues.has("responseCode")) {
							responseCode = jsonValues.getString("responseCode");
						}

						if (jsonValues.has("responseText")) {
							responseText = jsonValues.getString("responseText");
						}

						if (jsonValues.has("customFieldList")) {
							JSONArray customArr = new JSONArray();

							try {
								customArr.put(jsonValues
										.getJSONObject("customFieldList"));

							} catch (Exception e) {
								customArr = jsonValues
										.getJSONArray("customFieldList");

							}

							fieldValues = new ArrayList<>();

							for (int i = 0; i < customArr.length(); i++) {
								JSONObject customJsonObj = customArr
										.getJSONObject(i);

								if (customJsonObj.has("customfield")) {

									JSONArray customFieldArr = new JSONArray();

									try {
										customFieldArr.put(customJsonObj
												.getJSONObject("customfield"));
									} catch (Exception e) {
										customFieldArr = customJsonObj
												.getJSONArray("customfield");
									}

									for (int j = 0; j < customFieldArr.length(); j++) {
										JSONObject fieldsJson = customFieldArr
												.getJSONObject(j);
										HashMap<String, String> fieldsHash = new HashMap<>();

										if (fieldsJson.has("fldNo")) {
											fieldsHash.put("fldNo", fieldsJson
													.getString("fldNo"));
										}

										if (fieldsJson.has("name")) {
											fieldsHash.put("name", fieldsJson
													.getString("name"));
										}

										if (fieldsJson.has("prompt")) {
											fieldsHash.put("prompt", fieldsJson
													.getString("prompt"));
										}

										if (fieldsJson.has("required")) {
											fieldsHash
													.put("required",
															fieldsJson
																	.getString("required"));
										}

										if (fieldsJson.has("fldValue")) {
											fieldsHash
													.put("fldValue",
															fieldsJson
																	.getString("fldValue"));
										}

										fieldValues.add(fieldsHash);
									}
								}
							}

						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();

			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			if ("10000".equals(responseCode)) {
				setLayoutValues();
			}
			try {
				mDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		// Dynamically creating views
		private void setLayoutValues() {
			LinearLayout service_layout = (LinearLayout) findViewById(R.id.service_type_layout);
			LinearLayout description_layout = (LinearLayout) findViewById(R.id.description_layout);

			if (serviceType != null && !"".equals(serviceType)) {
				service_layout.setVisibility(View.VISIBLE);
				TextView service_view = (TextView) findViewById(R.id.service_type_value);
				service_view.setText(serviceType);

			} else {
				service_layout.setVisibility(View.GONE);
			}

			if (description != null && !"".equals(description)) {
				description_layout.setVisibility(View.VISIBLE);
				TextView description_view = (TextView) findViewById(R.id.description_value);
				description_view.setText(Html.fromHtml(description));

			} else {
				description_layout.setVisibility(View.GONE);
			}

			TextView email_view = (TextView) findViewById(R.id.email_value);
			email_view.setText(email_id);

			if (fieldValues.size() > 0) {

				for (int i = 0; i < fieldValues.size(); i++) {

					// Fields layout Params
					LinearLayout.LayoutParams layout_params = new LinearLayout.LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.WRAP_CONTENT);
					layout_params.setMargins(5, 5, 5, 0);
					layout_params.weight = 100;

					// Fields layout
					LinearLayout field_ll = new LinearLayout(
							GovQaDetailScreen.this);
					field_ll.setLayoutParams(layout_params);
					field_ll.setOrientation(LinearLayout.HORIZONTAL);

					// Text params
					LinearLayout.LayoutParams txt_params = new LinearLayout.LayoutParams(
							0, LayoutParams.WRAP_CONTENT);
					txt_params.gravity = Gravity.CENTER_VERTICAL;

					// TextView for text
					TextView text = new TextView(GovQaDetailScreen.this);
					text.setLayoutParams(txt_params);
					text.setTextColor(Color.BLACK);
					text.setTypeface(null, Typeface.BOLD);

					StringBuffer sb = new StringBuffer();

					if ("true".equals(fieldValues.get(i).get("required"))) {
						LinearLayout.LayoutParams mand_txt_params = new LinearLayout.LayoutParams(
								0, LayoutParams.WRAP_CONTENT);
						mand_txt_params.setMargins(5, 5, 0, 0);

						TextView mand_text = new TextView(
								GovQaDetailScreen.this);
						mand_text.setLayoutParams(mand_txt_params);
						mand_text.setTextColor(Color.RED);
						mand_text.setTypeface(null, Typeface.BOLD);
						mand_text.setText("*");

						txt_params.setMargins(0, 5, 5, 5);
						txt_params.weight = 35;
						mand_txt_params.weight = 5;

						field_ll.addView(mand_text);
					} else {
						txt_params.setMargins(5, 5, 5, 5);
						txt_params.weight = 40;
					}

					sb.append(fieldValues.get(i).get("name"));

					text.setText(sb.toString());

					field_ll.addView(text);

					// Values params
					LinearLayout.LayoutParams values_params = new LinearLayout.LayoutParams(
							0, LayoutParams.WRAP_CONTENT);
					values_params.setMargins(5, 5, 5, 5);
					values_params.weight = 60;

					// EditText with values
					EditText value = new EditText(GovQaDetailScreen.this);
					value.setLayoutParams(values_params);
					value.setTextColor(Color.BLACK);
					value.setTextSize(14);
					field_ll.addView(value);

					main_layout.addView(field_ll);

				}

			}

			// Buttons Layout params
			LinearLayout.LayoutParams buttons_layout_params = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			buttons_layout_params.setMargins(5, 5, 5, 5);

			// Buttons Layout
			LinearLayout buttonsLayout = new LinearLayout(
					GovQaDetailScreen.this);
			buttonsLayout.setLayoutParams(buttons_layout_params);
			buttonsLayout.setOrientation(LinearLayout.VERTICAL);

			// Buttons params
			LinearLayout.LayoutParams buttons_params = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			buttons_params.setMargins(5, 5, 5, 5);

			// Submit Button
			Button submitBtn = new Button(GovQaDetailScreen.this);
			submitBtn.setText("Submit");
			submitBtn.setTextColor(Color.BLACK);
			submitBtn.setBackgroundResource(R.drawable.govqa_rounded_button);
			submitBtn.setTypeface(null, Typeface.BOLD);
			submitBtn.setLayoutParams(buttons_params);

			buttonsLayout.addView(submitBtn);

			// Cancel Button
			Button cancelBtn = new Button(GovQaDetailScreen.this);
			cancelBtn.setText("Cancel");
			cancelBtn.setTextColor(Color.BLACK);
			cancelBtn.setBackgroundResource(R.drawable.govqa_rounded_button);
			cancelBtn.setTypeface(null, Typeface.BOLD);
			cancelBtn.setLayoutParams(buttons_params);

			buttonsLayout.addView(cancelBtn);

			setClickActionListener(submitBtn, cancelBtn);

			main_layout.addView(buttonsLayout);

			main_layout.setVisibility(View.VISIBLE);

		}

		// Action listener for
		private void setClickActionListener(Button submitBtn, Button cancelBtn) {

			submitBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// Call Webservice
					submitValues = new ArrayList<>();

					for (int i = 0; i < main_layout.getChildCount(); i++) {

						try {
							View view = main_layout.getChildAt(i);

							View subViewKey = ((LinearLayout) view)
									.getChildAt(0);

							View subViewValue = null;

							String values = "", key = "";

							if (subViewKey instanceof TextView) {
								key = ((TextView) subViewKey).getText()
										.toString();

								if (key.startsWith("*")) {
									subViewValue = ((LinearLayout) view)
											.getChildAt(2);
								} else {
									subViewValue = ((LinearLayout) view)
											.getChildAt(1);
								}
							}

							if (serviceAdd1.equals("")) {
								displayAlert("Service Address1 is mandatory",
										false);
								return;
							}

							if (subViewValue instanceof EditText) {
								values = ((EditText) subViewValue).getText()
										.toString();
								submitValues.add(values);
							}

							if ("".equals(values) && key.startsWith("*")) {
								displayAlert("Please Enter Mandatory Fields",
										false);
								submitValues.clear();
								return;
							}

							Log.i("submitValues", "" + submitValues);

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					setSubmitValues(submitValues);
					submitAsyncTask = new SubmitAsyncTask();
					submitAsyncTask.execute();
				}

			});

			cancelBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					cancelAsyncTask();
					finish();
				}
			});
		}
	}

	private void displayAlert(String message, final boolean isFinish) {
		AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
				GovQaDetailScreen.this);
		notificationAlert.setMessage(message).setNeutralButton(
				android.R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						if (isFinish) {
							dialog.dismiss();
							finish();
						} else {
							dialog.cancel();
						}
					}
				});
		notificationAlert.create().show();
	}

	public void setSubmitValues(ArrayList<String> submitValues) {
		try {

			Log.i("fieldValues", "" + fieldValues);
			Log.i("submitValues", "" + submitValues);

			for (int i = 0; i < submitValues.size(); i++) {
				fieldValues.get(i).put("fldValue", submitValues.get(i));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getDetails() {

		addr1 = serviceAdd1.getText().toString();
		addr2 = serviceAdd2.getText().toString();
		city = serviceCity.getText().toString();
		zipcode = serviceZipCode.getText().toString();

		state = serviceState.getSelectedItem().toString();

	}

	public class SubmitAsyncTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog mDialog;
		private JSONObject jsonResponse = null;
		String responseText = "";
		String responseCode = "";
		String referenceNo = "";

		UrlRequestParams urlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mDialog = ProgressDialog.show(GovQaDetailScreen.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected Void doInBackground(Void... params) {
			getDetails();

			String gov_qa_submit_custom_fields = Properties.url_local_server
					+ Properties.hubciti_version + "govqa/createservicereq";

			JSONObject urlParameters = urlRequestParams.createGovQaSubmitParam(
					typeNo, email_id, fieldValues, serviceType, serviceType,
					serviceAdd1.getText().toString(), serviceAdd2.getText()
							.toString(), serviceCity.getText().toString(),
					serviceState.getSelectedItem().toString(), serviceZipCode
							.getText().toString());

			jsonResponse = mServerConnections.getUrlJsonPostResponse(
					gov_qa_submit_custom_fields, urlParameters);

			if (jsonResponse != null) {
				try {
					if (jsonResponse.has("response")) {
						JSONObject jsonValues = jsonResponse
								.getJSONObject("response");

						if (jsonValues.has("responseCode")) {
							responseCode = jsonValues.getString("responseCode");
						}

						if (jsonValues.has("responseText")) {
							responseText = jsonValues.getString("responseText");
						}

						if (jsonValues.has("reference_no")) {
							referenceNo = jsonValues.getString("reference_no");
						}
					} else {
						if (jsonResponse.has("responseCode")) {
							responseCode = jsonResponse
									.getString("responseCode");
						}

						if (jsonResponse.has("responseText")) {
							responseText = jsonResponse
									.getString("responseText");
						}

						if (jsonResponse.has("reference_no")) {
							referenceNo = jsonResponse
									.getString("reference_no");
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			StringBuffer sb = new StringBuffer();
			boolean isSuccess;

			if ("10000".equals(responseCode)) {

				sb.append("Reference No: ");
				sb.append(referenceNo);
				sb.append("\n");
				sb.append("Contact E-Mail: ");
				sb.append(email_id);
				sb.append("\n");
				sb.append("Thank you.");

				isSuccess = true;

			} else {
				sb.append(responseText);
				isSuccess = false;
			}

			displayAlert(sb.toString(), isSuccess);

			mDialog.dismiss();

		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		// Add screen name when needed
		if (null != bb) {
			bb.setActivityInfo(activity, "Detailscreen");
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		cancelAsyncTask();
	}

	// Hides keyboard
	public void hideKeyboard() {
		GovQaDetailScreen.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
}
