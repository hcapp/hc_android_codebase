package com.hubcity.android.govqa;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.hubcity.android.businessObjects.FilterArdSortScreenBO;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.hubregion.R;

/**
 * @author Subramanya Padiyar V
 * 
 *         This page to sort the list of make request page
 */
public class GovQaMakeRequestSort extends CustomTitleBar implements
		OnClickListener {
	ArrayList<FilterArdSortScreenBO> arrSortAndFilter;
	ListView listView;
	LinearLayout ll;
	LinearLayout mainLayout;
	Context context = this;
	TextView groupByText, atozGroup, typeGroup, nameSort;
	CheckBox atozGroupCheck, typeGroupCheck, nameSortCheck, distanceSortCheck;
	TableRow mTableRow2GroupAtoZ, mTableRow2GroupType, mTableRowSortName,
			mTableRowCitySort, mTableRowCityList;

	String groupby = "atoz";
	String sortby = "name";
	String flag = "type";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.govqa_make_req_sort);

		try {
			ll = (LinearLayout) findViewById(R.id.city_ll);
			mainLayout = (LinearLayout) findViewById(R.id.main_layout);

			groupByText = (TextView) findViewById(R.id.event_textView_group_title);
			atozGroup = (TextView) findViewById(R.id.event_textView_group_atoz);
			typeGroup = (TextView) findViewById(R.id.event_textView_group_type);

			atozGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox2);
			atozGroupCheck.setOnClickListener(this);

			typeGroupCheck = (CheckBox) findViewById(R.id.event_group_checkBox3);
			typeGroupCheck.setOnClickListener(this);

			nameSort = (TextView) findViewById(R.id.event_textView_sort_name);

			nameSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox2);
			nameSortCheck.setOnClickListener(this);
			distanceSortCheck = (CheckBox) findViewById(R.id.event_sort_checkBox3);

			mTableRow2GroupAtoZ = (TableRow) findViewById(R.id.find_single_category_tableRow3);
			mTableRow2GroupType = (TableRow) findViewById(R.id.find_single_category_tableRow4);
			mTableRowSortName = (TableRow) findViewById(R.id.find_single_category_tableRow7);

			mTableRowCityList = (TableRow) findViewById(R.id.city_list_tableRow);

			this.title.setText("Group & Sort");

			// set the background for right and left button
			this.rightImage.setBackgroundResource(R.drawable.ic_action_accept);
			this.backImage.setBackgroundResource(R.drawable.ic_action_cancel);

			// Back action
			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    finish();
                }
            });

			rightImage.setTag("Accept");
			// onclick on right button
			rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (!(typeGroupCheck.isChecked() || atozGroupCheck.isChecked())) {
                        flag = "uncheck";
                        groupby = "atoz";

                    }
                    Intent intent = new Intent();
                    intent.putExtra("flag", flag);
                    intent.putExtra("groupBy", groupby);
                    intent.putExtra("sortBy", sortby);
                    setResult(2, intent);
                    finish();// finishing activity

                }
            });
			// Name checkbox
			nameSortCheck.setChecked(true);
			nameSortCheck.setClickable(false);

			// getting from intent
			Bundle bundle = getIntent().getExtras();
			flag = bundle.getString("flag");
			groupby = bundle.getString("groupBy");

			if (flag.equalsIgnoreCase("atoz")) {
                atozGroupCheck.setChecked(true);
                typeGroupCheck.setChecked(false);
            } else if (flag.equalsIgnoreCase("uncheck")) {
                atozGroupCheck.setChecked(false);
                typeGroupCheck.setChecked(false);
            } else {
                atozGroupCheck.setChecked(false);
                typeGroupCheck.setChecked(true);

            }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	// onclick on checkboxes
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.event_group_checkBox2:
			typeGroupCheck.setChecked(false);
			groupby = "atoz";
			flag = "atoz";
			break;

		case R.id.event_group_checkBox3:
			groupby = "type";
			flag = "type";
			atozGroupCheck.setChecked(false);
			break;

		case R.id.event_sort_checkBox2:
			break;

		default:
			break;
		}

	}
}
