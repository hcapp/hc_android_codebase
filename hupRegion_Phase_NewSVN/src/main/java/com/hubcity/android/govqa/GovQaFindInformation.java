package com.hubcity.android.govqa;

import java.util.ArrayList;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

/**
 * @author Subramanya Padiyar V
 * 
 *         This page will display the list of Informations to find
 */
public class GovQaFindInformation extends CustomTitleBar {
	Activity activity;
	LinearLayout linearLayout;
	JSONObject bbJsonObj;
	BottomButtons bb = null;
	ListView listView;
	LinkedList<GovQaFindInfoObj> findInfoList = null;
	LinkedList<GovQaFindInfoObj> findInfoListAdapter = new LinkedList<>();

	String authKey = "ifF34jauK;";
	String filterA = null;
	String filterB = null;
	int totalToReturn = 20;
	String keywords = null;
	String sortBy = null;
	GovQaFindInfoAdapter listAdapterview;
	EditText search;
	Button cancel;
	boolean isfirst = true;
	boolean isLoadMore = true;
	View moreResultsView;
	boolean name = false;
	boolean isFirstLoad;
	int catLoc;
	int subCatLoc;
	ArrayList<String> category = null;
	ArrayList<String> subCategory = null;
	ArrayList<Integer> categoryID = null;
	int isNext = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.govqa_findinformation);
			isFirstLoad = false;
			catLoc = 0;
			subCatLoc = 0;

			moreResultsView = getLayoutInflater().inflate(
                    R.layout.govqa_findinfo_pagination, listView, true);

			listView = (ListView) findViewById(R.id.govqa_findinfo_listview);
			search = (EditText) findViewById(R.id.govqa_findinfo_search_text);
			cancel = (Button) findViewById(R.id.govqa_findinfo_search);

			// getting Find Information Header
			Bundle bundle = getIntent().getExtras();
			String Header = bundle.getString("FindInfoHeader");

			// setting Title of the screen
			title.setText(Header);

			// setting rightbutton image

			rightImage.setBackgroundResource(R.drawable.ic_action_sort_by_size);

			// onclick on backbutton
			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    finish();

                }
            });
			// onclick on Cancelbutton
			cancel.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    search.setText("");
                    hideKeyboardItem();

                }
            });
			// onclick on Searchbutton
			search.setOnEditorActionListener(new OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId,
                        KeyEvent event) {
                    keywords = search.getText().toString();
                    isLoadMore = true;
                    callPagination(isNext, findInfoList);
                    hideKeyboardItem();
                    return true;
                }
            });

			// onclick on Listview
			listView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {

                    if (position != findInfoListAdapter.size()) {
                        Intent intent = new Intent(GovQaFindInformation.this,
                                GovQaRequestDetails.class);
                        if (getIntent().hasExtra("BottomBtnJsonObj")) {
                            intent.putExtra("BottomBtnJsonObj", getIntent()
                                    .getExtras().getString("BottomBtnJsonObj"));
                        }
                        intent.putExtra("Question",
                                findInfoListAdapter.get(position).getQuestion());
                        intent.putExtra("Solution",
                                findInfoListAdapter.get(position).getSolution());
                        intent.putExtra("Title", findInfoListAdapter.get(position)
                                .getSummary());
                        startActivity(intent);
                    }
                }
            });
			// Loading value and pagination
			callPagination(isNext, findInfoList);

			// calling Bottom button
			bottomButton();

			// onclick on sortbutton
			rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GovQaFindInformation.this,
                            GovQaFindInformationSort.class);
                    intent.putExtra("Name", name);
                    intent.putExtra("CatLoc", catLoc);
                    intent.putExtra("SubCatLoc", subCatLoc);
                    intent.putExtra("isFirstLoad", isFirstLoad);
                    if (category == null && subCategory == null
                            && categoryID == null) {
                        category = null;
                        subCategory = null;
                        categoryID = null;

                    }
                    intent.putStringArrayListExtra("Category",
                             category);
                    intent.putStringArrayListExtra("SubCategory",
                             subCategory);
                    intent.putIntegerArrayListExtra("CategoryID",
                             categoryID);

                    startActivityForResult(intent, 2);// Activity is started
                                                        // with requestCode 2

                }
            });
			hideKeyBoard();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void callPagination(int isNext,
			LinkedList<GovQaFindInfoObj> findInfoList) {
		this.isNext = isNext;
		if (this.isNext == 0) {
			totalToReturn = 20;
			new getFindInfo().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		} else {
			totalToReturn = totalToReturn + 20;
			new getFindInfo().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		}

	}

	private void hideKeyBoard() {
		GovQaFindInformation.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private void hideKeyboardItem() {
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 2) {
			search.setText("");
			isLoadMore = true;
			filterA = data.getExtras().getString("filterA");
			filterB = data.getStringExtra("filterB");
			sortBy = data.getStringExtra("sortBy");
			name = data.getExtras().getBoolean("Name");
			isFirstLoad = data.getExtras().getBoolean("isFirstLoad");
			isFirstLoad = true;
			catLoc = data.getExtras().getInt("CatLoc");
			subCatLoc = data.getExtras().getInt("SubCatLoc");
			category = data.getStringArrayListExtra("Category");
			subCategory = data.getStringArrayListExtra("SubCategory");
			categoryID = data.getIntegerArrayListExtra("CategoryID");

			keywords = null;
			totalToReturn = 20;
			new getFindInfo().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
	}

	private class getFindInfo extends AsyncTask<String, Void, String> {
		private ProgressDialog mDialog;
		ServerConnections mServerConnections = new ServerConnections();
		UrlRequestParams urlRequestParams = new UrlRequestParams();
		JSONObject jsonObjectGovQaFindInfo;

		@Override
		protected void onPreExecute() {
			if (isLoadMore) {
				mDialog = ProgressDialog.show(GovQaFindInformation.this, "",
						Constants.DIALOG_MESSAGE, true);
				mDialog.setCancelable(false);
				isLoadMore = false;
			}
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			String url = Properties.url_local_server
					+ Properties.hubciti_version + "govqa/faqs";
			findInfoList = new LinkedList<>();

			try {
				JSONObject urlParameters = urlRequestParams
						.createGovQaFindInfoListparam(authKey, filterA,
								filterB, totalToReturn, keywords, sortBy);
				jsonObjectGovQaFindInfo = mServerConnections
						.getUrlJsonPostResponse(url, urlParameters);
				if (jsonObjectGovQaFindInfo.has("isNext")) {
					isNext = jsonObjectGovQaFindInfo.getInt("isNext");
				}

				try {
					boolean isFaq = isJSONArray(jsonObjectGovQaFindInfo, "faqs");
					if (isFaq) {
						JSONArray FaqPar = jsonObjectGovQaFindInfo
								.getJSONArray("faqs");
						for (int index = 0; index < FaqPar.length(); index++) {
							JSONObject FaqObj = FaqPar.getJSONObject(index);
							GovQaFindInfoObj FindInfo = new GovQaFindInfoObj();
							FindInfo.setSummary(FaqObj.getString("summary"));
							FindInfo.setQuestion(FaqObj.getString("question"));
							FindInfo.setSolution(FaqObj.getString("solution"));
							findInfoList.add(FindInfo);

						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				result = "true";
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {
			if (result.equalsIgnoreCase("true")) {

				listAdapterview = new GovQaFindInfoAdapter(
						GovQaFindInformation.this, findInfoList, isNext);
				listView.setAdapter(listAdapterview);
				listAdapterview.notifyDataSetChanged();
				try {
					if (mDialog != null)
						mDialog.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (isNext == 0) {
					listView.removeFooterView(moreResultsView);
					moreResultsView.setVisibility(View.GONE);
				} else {
					listView.removeFooterView(moreResultsView);
					listView.addFooterView(moreResultsView);
					moreResultsView.setVisibility(View.VISIBLE);

				}
				if (findInfoList.size() == 0) {
					AlertDialog alertDialog = new AlertDialog.Builder(
							GovQaFindInformation.this).create();

					// Setting Dialog Message
					alertDialog.setMessage("No Result Found");

					// Setting OK Button
					alertDialog.setButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

								}
							});

					// Showing Alert Message
					alertDialog.show();
				}
				findInfoListAdapter = new LinkedList<>();
				findInfoListAdapter = findInfoList;
			}

		}

	}

	// checking the object is an Array
	public boolean isJSONArray(JSONObject jsonResponse, String value) {
		boolean isArray = false;

		JSONObject isJSONObject = jsonResponse.optJSONObject(value);
		if (isJSONObject == null) {
			JSONArray isJSONArray = jsonResponse.optJSONArray(value);
			if (isJSONArray != null) {
				isArray = true;
			}
		}
		return isArray;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Add screen name when needed
		if (null != bb) {
			bb.setActivityInfo(activity, "Find Information");
		}
	}

	@SuppressLint("ResourceAsColor")
	private void bottomButton() {
		/*** Implementation of Bottom buttons ***/
		activity = GovQaFindInformation.this;
		linearLayout = (LinearLayout) findViewById(R.id.findinfo_list_parent);
		// linearLayout.setBackgroundColor(R.color.light_gray);

		// Initiating Bottom button class
		bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
		// Add screen name when needed
		bb.setActivityInfo(activity, "Find Information");

		if (getIntent().hasExtra("BottomBtnJsonObj")
				&& getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

			try {
				bbJsonObj = new JSONObject(getIntent().getExtras().getString(
						"BottomBtnJsonObj"));

				ArrayList<BottomButtonBO> bottomButtonList = bb
						.parseForBottomButton(bbJsonObj);
				if (bottomButtonList != null) {
					BottomButtonListSingleton.clearBottomButtonListSingleton();
					BottomButtonListSingleton
							.getListBottomButton(bottomButtonList);
				} else {
					if (MenuAsyncTask.bottomButtonList != null) {
						BottomButtonListSingleton
								.clearBottomButtonListSingleton();
						BottomButtonListSingleton
								.getListBottomButton(MenuAsyncTask.bottomButtonList);
					}
				}

				bb.createbottomButtontTab(linearLayout, false);

				linearLayout.setVisibility(View.VISIBLE);

			} catch (Exception e) {
				e.printStackTrace();
				linearLayout.setVisibility(View.GONE);
			}

		} else {
			linearLayout.setVisibility(View.GONE);
		}

	}

}
