package com.hubcity.android.govqa;

import java.io.Serializable;

public class GovQaFindInfoSortobj implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int filterID;
	String description;
	
	public int getFilterID() {
		return filterID;
	}
	public void setFilterID(int filterID) {
		this.filterID = filterID;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
