package com.hubcity.android.govqa;

import java.util.ArrayList;

import javax.crypto.spec.SecretKeySpec;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.EncryptDecryptAlgorithmAES;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.android.ScanseeBrowserActivity;
import com.scansee.hubregion.R;

public class GovQaLogin extends CustomTitleBar {

    private CheckBox rememberMeCb;
    private boolean mStateChecked;
    Activity activity;

    TextView forgot_pwd_txt;
    Button submit_btn, create_acc_btn;
    JSONObject bbJsonObj;
    BottomButtons bb = null;

    LinearLayout linearLayout = null;
    EditText email_id_et;
    EditText password_et;
    SharedPreferences pref;

    private String email_id_val;
    private String password_val;

    EncryptDecryptAlgorithmAES algoAES;
    SecretKeySpec key;

    boolean isLoginSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.govqa_login);

        try {
            title.setText("GovQA Login");

            // Handling Title bar buttons
            rightImage.setVisibility(View.GONE);
            backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    backAction();
                }
            });

            // Generates key for Encrypting password
            algoAES = new EncryptDecryptAlgorithmAES();
            algoAES.generateKey();

            pref = getSharedPreferences(Constants.PREFERENCE_HUB_CITY, 0);

            email_id_et = (EditText) findViewById(R.id.govqa_email_et);
            password_et = (EditText) findViewById(R.id.govqa_pwd_et);

            hideKeyboard();

            setButtonActions();

            /*** Implementation of Bottom buttons ***/
            activity = GovQaLogin.this;
            linearLayout = (LinearLayout) findViewById(R.id.bottom_ll);
try {
    // Initiating Bottom button class
    bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
    // Add screen name when needed
    bb.setActivityInfo(activity, "GovQaLogin");
}catch (Exception e)
{

}

            if (getIntent().hasExtra("BottomBtnJsonObj")
                    && getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

                try {
                    bbJsonObj = new JSONObject(getIntent().getExtras().getString(
                            "BottomBtnJsonObj"));

                    ArrayList<BottomButtonBO> bottomButtonList = bb
                            .parseForBottomButton(bbJsonObj);
                    if (bottomButtonList != null) {
                        BottomButtonListSingleton.clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                    } else {
                        if (MenuAsyncTask.bottomButtonList != null) {
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(MenuAsyncTask.bottomButtonList);
                        }
                    }
                    bb.createbottomButtontTab(linearLayout, false);

                    linearLayout.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    linearLayout.setVisibility(View.GONE);
                }

            } else {
                linearLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Sets Actions to the buttons in Login screen
    private void setButtonActions() {
        // Forgot Password click Action
        forgot_pwd_txt = (TextView) findViewById(R.id.govqa_forgot_pwd);
        forgot_pwd_txt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Intent intent = new Intent(GovQaLogin.this,
                // GovQaForgotPassword.class);
                // if (bbJsonObj != null && bbJsonObj.length() > 0) {
                // intent.putExtra("BottomBtnJsonObj", bbJsonObj.toString());
                // }
                // startActivity(intent);
                Intent intent = new Intent(GovQaLogin.this,

                        ScanseeBrowserActivity.class);
                intent.putExtra(CommonConstants.URL,
                        "http://mygovhelp.com/ADDISONTX/_cs/ForgotPassword.aspx");
                intent.putExtra(CommonConstants.TAG_PAGE_TITLE,
                        "Forget Password");

                startActivity(intent);
            }
        });

        // Submit button click Action
        submit_btn = (Button) findViewById(R.id.govqa_submit_btn);
        submit_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                startLogInAsynkTask();
            }
        });

        // Create Account Action
        create_acc_btn = (Button) findViewById(R.id.govqa_create_acc_btn);
        create_acc_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GovQaLogin.this,
                        GovQaCreateEditAccount.class);

                if (bbJsonObj != null && bbJsonObj.length() > 0) {
                    intent.putExtra("BottomBtnJsonObj", bbJsonObj.toString());
                }

                intent.putExtra("title", "Create Account");

                startActivity(intent);
            }
        });

        // Remember me action
        rememberMeCb = (CheckBox) findViewById(R.id.govqa_rememberme);
        mStateChecked = pref.getBoolean(Constants.GOV_QA_REMEMBER, false);
        rememberMeCb.setChecked(mStateChecked);
        rememberMeCb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean state) {
                saveLoginCred(state);
            }
        });
    }

    // Remember me functionality
    private void saveLoginCred(boolean state) {

        Editor prefEditor = pref.edit();
        prefEditor.putString(Constants.GOV_QA_REMEMBER_NAME, null);
        prefEditor.putString(Constants.GOV_QA_REMEMBER_PASSWORD, null);
        if (state) {
            prefEditor.putString(Constants.GOV_QA_REMEMBER_NAME, email_id_et
                    .getText().toString().trim());

            prefEditor.putString(Constants.GOV_QA_REMEMBER_PASSWORD,
                    password_et.getText().toString().trim());

        }
        prefEditor.putBoolean(Constants.GOV_QA_REMEMBER, state);
        prefEditor.apply();
    }

    /*
     * Login Async Task
     */
    public class LoginAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog mDialog;
        private JSONObject jsonResponse = null;
        String responseText = "";
        String responseCode = "";

        UrlRequestParams urlRequestParams = new UrlRequestParams();
        ServerConnections mServerConnections = new ServerConnections();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = ProgressDialog.show(GovQaLogin.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                String gov_qa_login = Properties.url_local_server
                        + Properties.hubciti_version + "govqa/authenticateuser";

                JSONObject urlParameters = urlRequestParams
                        .createGovQaLoginParam(email_id_val, password_val);

                jsonResponse = mServerConnections.getUrlJsonPostResponse(
                        gov_qa_login, urlParameters);

                if (jsonResponse != null) {

                    if (jsonResponse.has("response")) {

                        JSONObject jsonResponseDetail = jsonResponse
                                .getJSONObject("response");

                        responseCode = jsonResponseDetail
                                .getString("responseCode");
                        responseText = jsonResponseDetail
                                .getString("responseText");

                        if (jsonResponseDetail.has("sessionId")) {
                            SharedPreferences pref = getApplicationContext()
                                    .getSharedPreferences(
                                            Constants.PREFERENCE_HUB_CITY,
                                            Context.MODE_PRIVATE);
                            SharedPreferences.Editor e = pref.edit().putString(
                                    Constants.GOV_QA_SESSION_ID,
                                    jsonResponseDetail.getString("sessionId"));
                            e.apply();
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if ("10000".equals(responseCode)) {

                isLoginSuccess = true;

                // Encrypt password and save it in preference
                Editor prefEditor = pref.edit();

                prefEditor.putString(Constants.GOV_QA_PASSWORD, Base64
                        .encodeToString(algoAES.encodeData(password_val),
                                Base64.DEFAULT));

                prefEditor.apply();

                mStateChecked = pref.getBoolean(Constants.GOV_QA_REMEMBER,
                        false);

                if (mStateChecked) {
                    saveLoginCred(mStateChecked);
                }

                Intent intent = new Intent(GovQaLogin.this, GovQaMainPage.class);

                if (bbJsonObj != null && bbJsonObj.length() > 0) {
                    intent.putExtra("BottomBtnJsonObj", bbJsonObj.toString());
                }

                intent.putExtra("email_id", email_id_et.getText().toString());

                startActivity(intent);

            } else {

                isLoginSuccess = false;

                AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
                        GovQaLogin.this);
                notificationAlert.setMessage(responseText).setNeutralButton(
                        android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                notificationAlert.create().show();

            }

            try {
                mDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    // Start Login Async Task
    private void startLogInAsynkTask() {
        String userName = email_id_et.getText().toString();
        if (userName.trim() != null) {
            email_id_val = userName;
        }

        String password = password_et.getText().toString();
        if (password.trim() != null) {
            password_val = password;
        }

        if (password_val != null && email_id_val != null) {
            new LoginAsyncTask().execute();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            bb.setActivityInfo(activity, "GovQaLogin");

            mStateChecked = pref.getBoolean(Constants.GOV_QA_REMEMBER, false);

            if (mStateChecked && !Constants.GOV_QA_DONT_ALLOW_LOGIN) {

                email_id_et.setText(pref.getString(Constants.GOV_QA_REMEMBER_NAME,
                        null));

                email_id_et.setSelection(email_id_et.getText().length());

                String password = pref.getString(
                        Constants.GOV_QA_REMEMBER_PASSWORD, "");
                password_et.setText(password);

                password_et.setSelection(password_et.getText().length());
                startLogInAsynkTask();

            } else if (mStateChecked) {

                email_id_et.setText(pref.getString(Constants.GOV_QA_REMEMBER_NAME,
                        null));
                email_id_et.setSelection(email_id_et.getText().length());
                String password = pref.getString(
                        Constants.GOV_QA_REMEMBER_PASSWORD, "");
                password_et.setText(password);
                password_et.setSelection(password_et.getText().length());
            } else {
                email_id_et.setText("");
                password_et.setText("");
                rememberMeCb.setChecked(false);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backAction();
    }

    // Handles back press
    private void backAction() {
        if (!isLoginSuccess) {
            Editor prefEditor = pref.edit();
            prefEditor.putString(Constants.GOV_QA_REMEMBER_NAME, null);
            prefEditor.putString(Constants.GOV_QA_REMEMBER_PASSWORD, null);
            prefEditor.putBoolean(Constants.GOV_QA_REMEMBER, false);
            prefEditor.apply();
        }

        Constants.GOV_QA_DONT_ALLOW_LOGIN = false;
        finish();
    }

    // Hides keyboard
    public void hideKeyboard() {
        GovQaLogin.this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

}
