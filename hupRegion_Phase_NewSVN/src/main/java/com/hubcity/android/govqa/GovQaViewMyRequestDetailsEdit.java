package com.hubcity.android.govqa;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

/**
 * @author supriya.m
 * 
 */
public class GovQaViewMyRequestDetailsEdit extends CustomTitleBar {

	boolean isFirst;
	String email, serviceType, description, serviceAddress1, serviceAddress2,
			city, state, zip;
	ArrayList<CustomFields> arrCustomFields;
	BottomButtons bb = null;
	LinearLayout main_layout;
	Activity activity;
	LinearLayout linearLayout;
	JSONObject bbJsonObj;
	TextView serviceState;
	TextView serviceAdd1;
	TextView serviceAdd2;
	TextView serviceCity;
	TextView serviceZipCode;
	String referenceNmbr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gov_qa_view_my_request_details_edit_screen);
		// creating the view and listener
		populateView();

	}

	private void populateView() {
		try {
			// Hiding Keyboard on initial activity load
			hideKeyboard();
			// Instantiating Fields
			serviceState = (TextView) findViewById(R.id.service_state_value);

			serviceAdd1 = (TextView) findViewById(R.id.service_add_value);
			serviceAdd2 = (TextView) findViewById(R.id.service_add_2_value);
			serviceCity = (TextView) findViewById(R.id.service_city_value);
			serviceZipCode = (TextView) findViewById(R.id.service_zip_value);
			serviceAddress1 = getIntent().getExtras().getString("Address1");
			serviceAddress2 = getIntent().getExtras().getString("Address2");
			city = getIntent().getExtras().getString("City");
			state = getIntent().getExtras().getString("State");
			zip = getIntent().getExtras().getString("Zip");

			// Setting the page title
			title.setText(getIntent().getExtras().getString("title"));
			referenceNmbr = getIntent().getExtras().getString("refNmbr");
			email = getIntent().getExtras().getString("email");
			serviceType = getIntent().getExtras().getString("title");
			// Back Action
			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    finish();
                }
            });
			rightImage.setVisibility(View.GONE);
			divider.setVisibility(View.GONE);
			main_layout = (LinearLayout) findViewById(R.id.custom_layout);
			main_layout.setVisibility(View.INVISIBLE);

			// Implementation of Bottom buttons
			activity = GovQaViewMyRequestDetailsEdit.this;
			linearLayout = (LinearLayout) findViewById(R.id.bottom_ll);

			// Initiating Bottom button class
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "View My Request Details Edit");

			if (getIntent().hasExtra("BottomBtnJsonObj")
                    && getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

                try {
                    bbJsonObj = new JSONObject(getIntent().getExtras().getString(
                            "BottomBtnJsonObj"));

                    ArrayList<BottomButtonBO> bottomButtonList = bb
                            .parseForBottomButton(bbJsonObj);
                    if (bottomButtonList != null) {
                        BottomButtonListSingleton.clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                    } else {
                        if (MenuAsyncTask.bottomButtonList != null) {
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(MenuAsyncTask.bottomButtonList);
                        }
                    }

                    bb.createbottomButtontTab(linearLayout, false);

                    linearLayout.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    linearLayout.setVisibility(View.GONE);
                }

            } else {
                linearLayout.setVisibility(View.GONE);
            }
			// calling webservice to fetch details as per reference number
			new getDetailsFromReferenceNumber()
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Hides keyboard
	public void hideKeyboard() {
		GovQaViewMyRequestDetailsEdit.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private class getDetailsFromReferenceNumber extends
			AsyncTask<Void, Void, String> {
		private ProgressDialog mDialog;
		private JSONObject jsonResponse = null;
		String responseCode = "";

		UrlRequestParams urlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mDialog = ProgressDialog.show(GovQaViewMyRequestDetailsEdit.this,
					"", Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(Void... params) {

			String gov_qa_viewrequest_details = Properties.url_local_server
					+ Properties.hubciti_version + "govqa/getreqbyreferno";

			JSONObject urlParameters = urlRequestParams
					.getGovQaViewMyRequestDetailsParam(referenceNmbr);
			jsonResponse = mServerConnections.getUrlJsonPostResponse(
					gov_qa_viewrequest_details, urlParameters);
			if (jsonResponse != null) {
				try {
					if (jsonResponse.has("responseCode")) {
						responseCode = jsonResponse.getString("responseCode");
						if (responseCode.equals("10000")) {
							if (jsonResponse.has("email"))
								email = jsonResponse.getString("email");
							if (jsonResponse.has("description"))
								serviceType = jsonResponse
										.getString("description");
							// If custom fields are there we will get the array
							// of custom fields as Table from webservice
							if (jsonResponse.has("Table")) {
								arrCustomFields = new ArrayList<>();
								JSONArray jsonArray = jsonResponse
										.getJSONArray("Table");
								for (int i = 0; i < jsonArray.length(); i++) {
									CustomFields customFields = new CustomFields();
									customFields.setFieldName(jsonArray
											.getJSONObject(i).getString(
													"fldName"));
									customFields.setFieldValue(jsonArray
											.getJSONObject(i).getString(
													"fldValue"));
									arrCustomFields.add(customFields);
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				if ("10000".equals(responseCode)) {
                    setLayoutValues();
                }
				mDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	// Creating custom field Class
	private class CustomFields {
		private String fieldName;
		private String fieldValue;

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public String getFieldValue() {
			return fieldValue;
		}

		public void setFieldValue(String fieldValue) {
			this.fieldValue = fieldValue;
		}
	}

	// Dynamically creating views
	public void setLayoutValues() {
		LinearLayout service_layout = (LinearLayout) findViewById(R.id.service_type_layout);
		LinearLayout description_layout = (LinearLayout) findViewById(R.id.description_layout);
		if (serviceType != null && !"".equals(serviceType)) {
			service_layout.setVisibility(View.VISIBLE);
			TextView service_view = (TextView) findViewById(R.id.service_type_value);
			service_view.setText(serviceType);

		} else {
			service_layout.setVisibility(View.GONE);
		}

		if (state != null && !state.equals(""))
			serviceState.setText(state);
		else
			findViewById(R.id.service_state_layout)
					.setVisibility(View.GONE);
		if (city != null && !city.equals(""))
			serviceCity.setText(city);
		else
			findViewById(R.id.service_city_layout)
					.setVisibility(View.GONE);
		if (zip != null && !zip.equals(""))
			serviceZipCode.setText(zip);
		else
			findViewById(R.id.service_zip_layout)
					.setVisibility(View.GONE);
		if (serviceAddress1 != null && !serviceAddress1.equals(""))
			serviceAdd1.setText(serviceAddress1);
		else
			findViewById(R.id.service_add_layout)
					.setVisibility(View.GONE);

		if (serviceAddress2 != null && !serviceAddress2.equals(""))
			serviceAdd2.setText(serviceAddress2);
		else
			findViewById(R.id.service_add_2_layout)
					.setVisibility(View.GONE);

		if (description != null && !"".equals(description)) {
			description_layout.setVisibility(View.VISIBLE);
			TextView description_view = (TextView) findViewById(R.id.description_value);
			description_view.setText(Html.fromHtml(description));

		} else {
			description_layout.setVisibility(View.GONE);
		}

		((TextView) findViewById(R.id.reference_value)).setText(referenceNmbr);

		TextView email_view = (TextView) findViewById(R.id.email_value);
		email_view.setText(email);

		if (arrCustomFields != null && arrCustomFields.size() > 0) {
			for (int i = 0; i < arrCustomFields.size(); i++) {
				// Fields layout Params
				LinearLayout.LayoutParams layout_params = new LinearLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				layout_params.setMargins(5, 5, 5, 0);
				layout_params.weight = 100;

				// Fields layout
				LinearLayout field_ll = new LinearLayout(
						GovQaViewMyRequestDetailsEdit.this);
				field_ll.setLayoutParams(layout_params);
				field_ll.setOrientation(LinearLayout.HORIZONTAL);

				// Text params
				LinearLayout.LayoutParams txt_params = new LinearLayout.LayoutParams(
						0, LayoutParams.WRAP_CONTENT);
				txt_params.gravity = Gravity.CENTER_VERTICAL;

				// TextView for text
				TextView text = new TextView(GovQaViewMyRequestDetailsEdit.this);
				text.setLayoutParams(txt_params);
				text.setTextColor(Color.BLACK);
				text.setTypeface(null, Typeface.BOLD);
				txt_params.setMargins(5, 5, 5, 5);
				txt_params.weight = 40;

				text.setText(arrCustomFields.get(i).getFieldName());

				field_ll.addView(text);

				// Values params
				LinearLayout.LayoutParams values_params = new LinearLayout.LayoutParams(
						0, LayoutParams.WRAP_CONTENT);
				values_params.setMargins(5, 5, 5, 5);
				values_params.weight = 45;
				values_params.gravity = Gravity.CENTER_VERTICAL;
				// EditText with values
				EditText value = new EditText(
						GovQaViewMyRequestDetailsEdit.this);
				value.setLayoutParams(values_params);
				value.setTextColor(Color.BLACK);
				value.setTextSize(14);
				value.setSingleLine(true);
				value.setText(arrCustomFields.get(i).getFieldValue());
				field_ll.addView(value);

				// button params
				LinearLayout.LayoutParams button_params = new LinearLayout.LayoutParams(
						0, LayoutParams.MATCH_PARENT);
				button_params.setMargins(5, 5, 5, 5);
				button_params.weight = 15;

				// Buttons to update
				Button updateButton = new Button(
						GovQaViewMyRequestDetailsEdit.this);
				updateButton.setLayoutParams(button_params);
				updateButton.setBackgroundResource(R.drawable.update);
				updateButton.setTag(i);
				field_ll.addView(updateButton);
				setClickListener(updateButton);
				main_layout.addView(field_ll);
			}
			main_layout.setVisibility(View.VISIBLE);
		}
	}

	private void setClickListener(Button updateButton) {
		updateButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int position = (Integer) v.getTag();
				// for (int i = 0; i < main_layout.getChildCount(); i++) {

				try {
					View view = main_layout.getChildAt(position);

					View subViewKey = ((LinearLayout) view).getChildAt(0);

					View subViewValue = null;

					String values = "", key = "";

					if (subViewKey instanceof TextView) {
						key = ((TextView) subViewKey).getText().toString();

						if (key.startsWith("*")) {
							subViewValue = ((LinearLayout) view).getChildAt(2);
						} else {
							subViewValue = ((LinearLayout) view).getChildAt(1);
						}
					}

					if (subViewValue instanceof EditText) {
						values = ((EditText) subViewValue).getText().toString();
						// submitValues.add(values);
					}

					new UpdateValueAsync(key, values)
							.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			// }
		});
	}

	// Updates value for each dynamic field
	private class UpdateValueAsync extends AsyncTask<Void, Void, Void> {
		String key, value;
		private JSONObject jsonResponse = null;
		String responseText;
		private ProgressDialog mDialog;
		UrlRequestParams urlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		public UpdateValueAsync(String key, String value) {
			this.key = key;
			this.value = value;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mDialog = ProgressDialog.show(GovQaViewMyRequestDetailsEdit.this,
					"", Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected Void doInBackground(Void... params) {
			JSONObject urlParameters = null;
			String gov_qa_viewrequest_details_update = Properties.url_local_server
					+ Properties.hubciti_version + "govqa/updatereqcustfield";
			urlParameters = urlRequestParams
					.getGovQaViewMyRequestDetailsUpdateParam(referenceNmbr,
							key, value);

			jsonResponse = mServerConnections.getUrlJsonPostResponse(
					gov_qa_viewrequest_details_update, urlParameters);
			if (jsonResponse != null) {

				if (jsonResponse.has("responseCode")) {
					try {
						jsonResponse.getString("responseCode");
						responseText = jsonResponse.getString("responseText");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			try {
				mDialog.dismiss();
				if (responseText.equalsIgnoreCase("Success"))
                    Toast.makeText(GovQaViewMyRequestDetailsEdit.this,
                            "Updated Successfully", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(GovQaViewMyRequestDetailsEdit.this,
                            responseText, Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
