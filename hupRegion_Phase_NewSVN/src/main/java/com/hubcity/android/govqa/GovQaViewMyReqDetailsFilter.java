package com.hubcity.android.govqa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.scansee.hubregion.R;


import java.util.ArrayList;


/**
 * Created by subramanya.v on 1/4/2016.
 */
public class GovQaViewMyReqDetailsFilter extends CustomTitleBar {

	private ArrayList<GovQaViewMyReqFilterObj> mFilterList = new ArrayList<>();

	private  String[] mArrayList = null ;

	private int mListPosition;
	private GovQaViewMyReqDetFilterAdapter mListAdapterView;
	private ListView mListView;
	private String mFilterName;
	private String mEmail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.govqa_view_my_req_filter);

		try {
			bindValue();
			bindView();
			setListener();
			getIntentData();

			this.title.setText(R.string.govqa_view_my_request_filter);
			this.backImage.setBackgroundResource(R.drawable.ic_action_cancel);
			this.rightImage.setBackgroundResource(R.drawable.ic_action_accept);

			insertValue(mListPosition);

			mListAdapterView = new GovQaViewMyReqDetFilterAdapter(GovQaViewMyReqDetailsFilter.this,
                    mFilterList);
			mListView.setAdapter(mListAdapterView);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void bindValue() {
		mArrayList = getResources().getStringArray(R.array.arrayList);
	}

	private void bindView() {
		mListView = (ListView) findViewById(R.id.viewmy_req_filter_list);
	}

	private void getIntentData() {
		Intent intent = getIntent();
		if (intent != null) {
			if (intent.hasExtra(Constants.LIST_POSITION)) {
				mListPosition = intent.getExtras().getInt(Constants.LIST_POSITION);
			}
			if (getIntent().hasExtra(Constants.FILTER_NAME)) {
				mFilterName = intent.getExtras().getString(Constants.FILTER_NAME);
			}
			if (getIntent().hasExtra(Constants.EMAIL)) {
				mEmail = intent.getExtras().getString(Constants.EMAIL);
			}
		}


	}

	private void setListener() {
		rightImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.putExtra(Constants.LIST_POSITION, mListPosition);
				intent.putExtra(Constants.FILTER_NAME, mFilterName);
				intent.putExtra(Constants.EMAIL, mEmail);
				setResult(2, intent);
				finish();

			}
		});
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//clicked list cannot be header to perform action in checkbox
				if (!mFilterList.get(position).isHeader()) {
					for (int i = 0; i < mArrayList.length; i++) {
						if (!mFilterList.get(i).isHeader()) {
							mFilterList.get(i).setIschecked(false);
						}
					}

					if (position == 1) {
						mFilterName =getResources().getString(R.string.govqa_view_my_request_filtr_none);
					} else {
						mFilterName = mArrayList[position];
					}
					mFilterList.get(position)
							.setIschecked(true);

					mListPosition = position;


					mListAdapterView
							.updateArray(mFilterList);
					mListAdapterView
							.notifyDataSetChanged();


				}
			}
		});

	}


	private void insertValue(int position) {

		for (int i = 0; i < mArrayList.length; i++) {
			GovQaViewMyReqFilterObj FilterObj = new GovQaViewMyReqFilterObj();
			if (i == 0) {
				FilterObj.setIsHeader(true);
			} else {
				FilterObj.setIsHeader(false);
			}

			if (position == i) {
				FilterObj.setIschecked(true);
			} else {
				FilterObj.setIschecked(false);
			}

			FilterObj.setFilterBy(mArrayList[i]);
			mFilterList.add(FilterObj);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mListAdapterView = null;
	}
}
