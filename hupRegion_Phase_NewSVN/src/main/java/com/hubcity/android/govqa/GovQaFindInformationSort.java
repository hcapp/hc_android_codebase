package com.hubcity.android.govqa;

import java.util.ArrayList;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.scansee.hubregion.R;

/**
 * @author Subramanya Padiyar V
 * 
 *         This page to select sort and filter item
 */
public class GovQaFindInformationSort extends CustomTitleBar implements
		OnItemSelectedListener {
	ArrayList<GovQaFindInfoSortobj> arrSortAndFilter_Category;
	ArrayList<GovQaFindInfoSortobj> arrSortAndFilter_SubCategory;
	ArrayList<String> category;
	ArrayList<Integer> categoryID;
	ArrayList<String> subCategory;
	Spinner category_Spin, subCategory_Spin;
	String authKey = "ifF34jauK;";
	int parentID;
	String filterB = null;
	String filterA = null;
	CheckBox nameChk;
	boolean name = false;
	boolean isFirstLoad;
	int catLoc;
	int subCatLoc;
	String sortBy;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.govqa_findinformation_sort);
			// Getting Data through intent
			if (getIntent().hasExtra("Name")) {
                name = getIntent().getExtras().getBoolean("Name");
            }
			if (getIntent().hasExtra("isFirstLoad")) {
                isFirstLoad = getIntent().getExtras().getBoolean("isFirstLoad");
            }

			if (getIntent().hasExtra("CatLoc")) {
                catLoc = getIntent().getExtras().getInt("CatLoc");
            }

			if (getIntent().hasExtra("SubCatLoc")) {
                subCatLoc = getIntent().getExtras().getInt("SubCatLoc");
            }
			if (getIntent().hasExtra("Category")) {
                category = (ArrayList<String>) getIntent().getSerializableExtra(
                        "Category");
            }
			if (getIntent().hasExtra("SubCategory")) {
                subCategory = (ArrayList<String>) getIntent().getSerializableExtra(
                        "SubCategory");
            }
			if (getIntent().hasExtra("CategoryID")) {
                categoryID = (ArrayList<Integer>) getIntent().getSerializableExtra(
                        "CategoryID");
            }

			// Getting the instance of Spinner and applying OnItemSelectedListener
			// on it
			nameChk = (CheckBox) findViewById(R.id.govqa_findinfo_sort_alphabetic_chkbox);
			category_Spin = (Spinner) findViewById(R.id.govqa_findinfo_sort_catogory_spinner);
			subCategory_Spin = (Spinner) findViewById(R.id.govqa_findinfo_sort_subcatogory_spinner);
			category_Spin.setOnItemSelectedListener(this);
			subCategory_Spin.setOnItemSelectedListener(this);
			// setting right and left button image
			rightImage.setBackgroundResource(R.drawable.ic_action_accept);
			backImage.setBackgroundResource(R.drawable.ic_action_cancel);

			// setting text of header
			title.setText("Filter and Sort ");
			if (name) {
                nameChk.setChecked(name);
            } else {
                nameChk.setChecked(name);
            }
			// onclick on checkBox
			nameChk.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (nameChk.isChecked()) {
                        name = true;
                        sortBy = "name";
                    } else {
                        sortBy = null;
                        name = false;
                    }

                }
            });

			// onclick on CancelButton
			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    finish();
                }
            });
			// onclick on Done Button
			rightImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("filterA", filterA);
                    intent.putExtra("filterB", filterB);
                    intent.putExtra("Name", name);
                    intent.putExtra("CatLoc", catLoc);
                    intent.putExtra("SubCatLoc", subCatLoc);
                    intent.putExtra("isFirstLoad", isFirstLoad);
                    if (nameChk.isChecked()) {
                        sortBy = "name";
                    } else {
                        sortBy = null;
                    }
                    intent.putExtra("sortBy", sortBy);
                    intent.putExtra("Category", category);
                    intent.putExtra("SubCategory", subCategory);
                    intent.putExtra("CategoryID", categoryID);

                    setResult(2, intent);
                    finish();// finishing activity

                }
            });
			// checking status that to load data statically or dynamically
			if (isFirstLoad) {
                // Loading Category
                loadingCatStatic(catLoc);

            } else {
                // Getting parameter through asynchronous task
                new getcategory().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void loadingCatStatic(int catLoc) {

		// Creating the ArrayAdapter instance having the Category list
		ArrayAdapter<String> Category_adapter = new ArrayAdapter<>(
				getApplicationContext(), R.layout.spinner_category, category);
		Category_adapter
				.setDropDownViewResource(R.layout.spinner_category_dropdown_item);
		// Setting the ArrayAdapter data on the Spinner
		category_Spin.setAdapter(Category_adapter);
		category_Spin.setSelection(catLoc);
		if (catLoc != 0) {
			filterA = category.get(catLoc);
		} else {
			filterA = null;
		}

	}

	private class getcategory extends AsyncTask<String, Void, String> {
		private ProgressDialog mDialog;
		ServerConnections mServerConnections = new ServerConnections();
		UrlRequestParams urlRequestParams = new UrlRequestParams();

		JSONObject jsonObjectGovQaFindInfoCat;

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(GovQaFindInformationSort.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			String url = Properties.url_local_server
					+ Properties.hubciti_version + "govqa/filtera";
			arrSortAndFilter_Category = new ArrayList<>();
			category = new ArrayList<>();
			categoryID = new ArrayList<>();
			try {
				JSONObject urlParameters = urlRequestParams
						.createGovQaFindInfoCatparam(authKey);
				jsonObjectGovQaFindInfoCat = mServerConnections
						.getUrlJsonPostResponse(url, urlParameters);

				try {
					if (jsonObjectGovQaFindInfoCat.getString("responseCode")
							.equalsIgnoreCase("10000")) {
						boolean iscategory = isJSONArray(
								jsonObjectGovQaFindInfoCat, "categories");
						if (iscategory) {
							GovQaFindInfoSortobj GovQaFindInfoParentObj = new GovQaFindInfoSortobj();
							JSONArray jsonArrayFindInfoCat = jsonObjectGovQaFindInfoCat
									.getJSONArray("categories");
							category.add("All Items");
							categoryID.add(0);
							GovQaFindInfoParentObj.setFilterID(0);
							GovQaFindInfoParentObj.setDescription("All Items");
							arrSortAndFilter_Category
									.add(GovQaFindInfoParentObj);
							for (int index = 0; index < jsonArrayFindInfoCat
									.length(); index++) {

								GovQaFindInfoSortobj GovQaFindInfoObj = new GovQaFindInfoSortobj();
								JSONObject jsonCatobj = jsonArrayFindInfoCat
										.getJSONObject(index);
								GovQaFindInfoObj.setFilterID(jsonCatobj
										.getInt("filterID"));
								GovQaFindInfoObj.setDescription(jsonCatobj
										.getString("description"));
								arrSortAndFilter_Category.add(GovQaFindInfoObj);
								category.add(jsonCatobj
										.getString("description"));
								categoryID.add(jsonCatobj.getInt("filterID"));
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				result = "true";

			} catch (Exception e) {
				e.printStackTrace();

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equalsIgnoreCase("true")) {
				// Creating the ArrayAdapter instance having the Category list
				ArrayAdapter<String> Category_adapter = new ArrayAdapter<>(
						getApplicationContext(), R.layout.spinner_category,
						category);
				Category_adapter
						.setDropDownViewResource(R.layout.spinner_category_dropdown_item);
				// Setting the ArrayAdapter data on the Spinner
				category_Spin.setAdapter(Category_adapter);
				category_Spin.setSelection(catLoc);

				try {
					mDialog.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	// checking the object is an Array
	public boolean isJSONArray(JSONObject jsonResponse, String value) {
		boolean isArray = false;

		JSONObject isJSONObject = jsonResponse.optJSONObject(value);
		if (isJSONObject == null) {
			JSONArray isJSONArray = jsonResponse.optJSONArray(value);
			if (isJSONArray != null) {
				isArray = true;
			}
		}
		return isArray;
	}

	// click on item and loading data to spinner
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {

		if (isFirstLoad) {
			loadingsubCatStatic(subCatLoc);
			isFirstLoad = false;
			return;
		}

		Spinner spinner = (Spinner) parent;
		if (spinner.getId() == R.id.govqa_findinfo_sort_catogory_spinner) {

			if (isFirstLoad) {
				isFirstLoad = true;

			} else {
				subCatLoc = 0;
				loadingsubCat(position);
			}

		} else {

			if (position >= 1) {
				subCatLoc = position;
				filterB = subCategory.get(position);

			} else {
				subCatLoc = 0;
				filterB = null;
			}
		}

	}

	// LOading Subcatagory
	private void loadingsubCatStatic(int position) {

		// Creating the ArrayAdapter instance having the Category list
		ArrayAdapter<String> SubCategory_adapter = new ArrayAdapter<>(
				getApplicationContext(), R.layout.spinner_category, subCategory);
		SubCategory_adapter
				.setDropDownViewResource(R.layout.spinner_category_dropdown_item);
		// Setting the ArrayAdapter data on the Spinner
		subCategory_Spin.setAdapter(SubCategory_adapter);
		subCategory_Spin.setSelection(subCatLoc);
		if (subCatLoc != 0) {
			filterB = subCategory.get(subCatLoc);
		} else {
			filterB = null;
		}
	}

	// Loading Subcategory dynamically
	private void loadingsubCat(int position) {
		if (position >= 1) {
			catLoc = position;
			filterA = category.get(position);

		} else {
			catLoc = position;
			filterA = null;
		}

		String item = category.get(position);
		if (!Objects.equals(item, "All Items")) {
			// Getting parameter through asynchronous task
			parentID = categoryID.get(position);

			new GetSubCategory()
					.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		} else {
			subCategory = new ArrayList<>();
			subCategory.add("All Items");
			// Creating the ArrayAdapter instance having the Category list
			ArrayAdapter<String> SubCategory_adapter = new ArrayAdapter<>(
					getApplicationContext(), R.layout.spinner_category,
					subCategory);
			SubCategory_adapter
					.setDropDownViewResource(R.layout.spinner_category_dropdown_item);
			// Setting the ArrayAdapter data on the Spinner
			subCategory_Spin.setAdapter(SubCategory_adapter);
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	private class GetSubCategory extends AsyncTask<String, Void, String> {
		private ProgressDialog mDialog;
		JSONObject jsonObjectGovQaFindInfoSubCat;
		ServerConnections mServerConnections = new ServerConnections();
		UrlRequestParams urlRequestParams = new UrlRequestParams();

		@Override
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(GovQaFindInformationSort.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "false";
			String url = Properties.url_local_server
					+ Properties.hubciti_version + "govqa/filterb";
			arrSortAndFilter_SubCategory = new ArrayList<>();
			subCategory = new ArrayList<>();

			try {
				JSONObject urlParameters = urlRequestParams
						.createGovQaFindInfoSubCatparam(authKey, parentID);
				jsonObjectGovQaFindInfoSubCat = mServerConnections
						.getUrlJsonPostResponse(url, urlParameters);

				try {
					if (jsonObjectGovQaFindInfoSubCat.getString("responseCode")
							.equalsIgnoreCase("10000")) {
						boolean iscategory = isJSONArray(
								jsonObjectGovQaFindInfoSubCat, "categories");
						if (iscategory) {
							JSONArray jsonArrayFindInfoSubCat = jsonObjectGovQaFindInfoSubCat
									.getJSONArray("categories");
							subCategory.add("All Items");

							GovQaFindInfoSortobj GovQaFindInfoParentObj = new GovQaFindInfoSortobj();
							GovQaFindInfoParentObj.setFilterID(0);
							GovQaFindInfoParentObj.setDescription("All Items");
							arrSortAndFilter_SubCategory
									.add(GovQaFindInfoParentObj);
							for (int index = 0; index < jsonArrayFindInfoSubCat
									.length(); index++) {
								GovQaFindInfoSortobj GovQaFindInfoObj = new GovQaFindInfoSortobj();

								JSONObject jsonCatobj = jsonArrayFindInfoSubCat
										.getJSONObject(index);
								GovQaFindInfoObj.setFilterID(jsonCatobj
										.getInt("filterID"));
								GovQaFindInfoObj.setDescription(jsonCatobj
										.getString("description"));
								arrSortAndFilter_SubCategory
										.add(GovQaFindInfoObj);
								subCategory.add(jsonCatobj
										.getString("description"));
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				result = "true";

			} catch (Exception e) {
				e.printStackTrace();

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				if (result.equalsIgnoreCase("true")) {
                    // Creating the ArrayAdapter instance having the Category list
                    ArrayAdapter<String> SubCategory_adapter = new ArrayAdapter<>(
                            getApplicationContext(), R.layout.spinner_category,
                            subCategory);
                    SubCategory_adapter
                            .setDropDownViewResource(R.layout.spinner_category_dropdown_item);
                    // Setting the ArrayAdapter data on the Spinner
                    subCategory_Spin.setAdapter(SubCategory_adapter);
                    subCategory_Spin.setSelection(subCatLoc);
                    mDialog.dismiss();

                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
