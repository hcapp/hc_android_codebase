package com.hubcity.android.govqa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CommonConstants;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.EncryptDecryptAlgorithmAES;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

public class GovQaCreateEditAccount extends CustomTitleBar implements
		OnClickListener {

	LinearLayout main_layout;
	Activity activity;
	LinearLayout linearLayout;
	JSONObject bbJsonObj;
	BottomButtons bb = null;
	boolean isFirst;

	LinearLayout home_add_layout, apt_suite_number_layout, city_layout,
			state_prov_layout, zip_post_layout;

	EditText email_value, password_value, confirm_pwd_value, fname_value,
			lname_value, home_add_value, apt_num_value, city_value,
			zipcode_value, phone_value;
	TextView email_txt, email_edit_value, email_mand_txt;

	Button submit_btn, cancel_btn;
	Spinner stateSpinner;
	boolean isCreateAcc;
	String titleStr;

	String email_id = "";
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.govqa_create_edit_account);

		try {
			mContext = GovQaCreateEditAccount.this;
			isFirst = true;

			hideKeyboard();

			titleStr = getIntent().getExtras().getString("title");
			title.setText(titleStr);

			rightImage.setVisibility(View.GONE);

			submit_btn = (Button) findViewById(R.id.submit_btn);
			submit_btn.setOnClickListener(this);

			cancel_btn = (Button) findViewById(R.id.cancel_btn);
			cancel_btn.setOnClickListener(this);

			stateSpinner = (Spinner) findViewById(R.id.state_prov_value);
			stateSpinner.setSelection(43);

			// Layouts
			home_add_layout = (LinearLayout) findViewById(R.id.home_add_layout);
			apt_suite_number_layout = (LinearLayout) findViewById(R.id.apt_suite_number_layout);
			city_layout = (LinearLayout) findViewById(R.id.city_layout);
			state_prov_layout = (LinearLayout) findViewById(R.id.state_prov_layout);
			zip_post_layout = (LinearLayout) findViewById(R.id.zip_post_layout);

			// EditText
			email_value = (EditText) findViewById(R.id.email_add_value);
			password_value = (EditText) findViewById(R.id.password_value);
			confirm_pwd_value = (EditText) findViewById(R.id.confirm_password_value);
			fname_value = (EditText) findViewById(R.id.fname_value);
			lname_value = (EditText) findViewById(R.id.lname_value);
			home_add_value = (EditText) findViewById(R.id.home_add_value);
			apt_num_value = (EditText) findViewById(R.id.apt_suite_number_value);
			city_value = (EditText) findViewById(R.id.city_value);
			zipcode_value = (EditText) findViewById(R.id.zip_post_value);
			phone_value = (EditText) findViewById(R.id.phone_value);

			// TextViews
			email_edit_value = (TextView) findViewById(R.id.email_add_edit_value);
			email_txt = (TextView) findViewById(R.id.email_add_txt);
			email_mand_txt = (TextView) findViewById(R.id.email_mand_txt);

			LayoutParams params = new LayoutParams(0,
                    LayoutParams.WRAP_CONTENT);

			activity = GovQaCreateEditAccount.this;
			if (getIntent().hasExtra("email_id")) {
                email_id = getIntent().getExtras().getString("email_id");
            }

			// Check if The Screen is Create Account or Edit Account
			if (titleStr.startsWith("Create")) {
                isCreateAcc = true;
                email_value.setVisibility(View.VISIBLE);
                email_edit_value.setVisibility(View.GONE);

                email_mand_txt.setVisibility(View.VISIBLE);
                params.weight = 37;

                home_add_layout.setVisibility(View.VISIBLE);
                apt_suite_number_layout.setVisibility(View.VISIBLE);
                city_layout.setVisibility(View.VISIBLE);
                state_prov_layout.setVisibility(View.VISIBLE);
                zip_post_layout.setVisibility(View.VISIBLE);

            } else {
                isCreateAcc = false;
                email_edit_value.setVisibility(View.VISIBLE);
                email_value.setVisibility(View.GONE);

                email_mand_txt.setVisibility(View.GONE);
                params.weight = 40;

                home_add_layout.setVisibility(View.GONE);
                apt_suite_number_layout.setVisibility(View.GONE);
                city_layout.setVisibility(View.GONE);
                state_prov_layout.setVisibility(View.GONE);
                zip_post_layout.setVisibility(View.GONE);

                getUserDetails();

            }

			email_txt.setLayoutParams(params);

			/*** Implementation of Bottom buttons ***/
			linearLayout = (LinearLayout) findViewById(R.id.bottom_ll);

			// Initiating Bottom button class
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "GovQaCreateEditAcc");

			if (getIntent().hasExtra("BottomBtnJsonObj")
                    && getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

                try {
                    bbJsonObj = new JSONObject(getIntent().getExtras().getString(
                            "BottomBtnJsonObj"));

                    ArrayList<BottomButtonBO> bottomButtonList = bb
                            .parseForBottomButton(bbJsonObj);
                    if (bottomButtonList != null) {
                        BottomButtonListSingleton.clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                    } else {
                        if (MenuAsyncTask.bottomButtonList != null) {
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(MenuAsyncTask.bottomButtonList);
                        }
                    }

                    bb.createbottomButtontTab(linearLayout, false);

                    linearLayout.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    linearLayout.setVisibility(View.GONE);
                }

            } else {
                linearLayout.setVisibility(View.GONE);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getUserDetails() {
		try {

			HashMap<String, String> values = new HashMap<>();

			GovQAUserDetailsAsyncTask userDetails = new GovQAUserDetailsAsyncTask(
					activity, email_id);
			values = userDetails.execute().get();

			if (values != null && !values.isEmpty()) {
				setFieldValues(values);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// If Edit Account pre-populate the customer info
	private void setFieldValues(HashMap<String, String> values) {

		try {

			if (isCreateAcc) {
				email_value.append(values.get("email"));
			} else {
				email_edit_value.append(values.get("email"));
			}

			fname_value.append(values.get("first"));
			lname_value.append(values.get("last"));
			phone_value.append(values.get("phone"));

			// Decode the encrypted password and set to Password field
			// Generates key for Encrypting password
			EncryptDecryptAlgorithmAES algoAES = new EncryptDecryptAlgorithmAES();
			SharedPreferences pref = getSharedPreferences(
					Constants.PREFERENCE_HUB_CITY, 0);

			String password = algoAES.decodeData(Base64.decode(
					pref.getString(Constants.GOV_QA_PASSWORD, ""),
					Base64.DEFAULT));

			password_value.append(password);
			confirm_pwd_value.append(password);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		bb.setActivityInfo(activity, "GovQaCreateEditAcc");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	// Validating Mandatory fields
	private boolean validateFields() {
		boolean isValidateSuccess = true;

		String password = password_value.getText().toString().trim();
		String confirm_password = confirm_pwd_value.getText().toString().trim();
		if (titleStr.startsWith("Create")) {
			
			if ("".equals(password) || "".equals(confirm_password)
					|| "".equals(email_value.getText().toString().trim())) {
				displayAlert("Enter Mandatory Fields", false);
				isValidateSuccess = false;

			} else if (!password.equals(confirm_password)) {
				displayAlert("Password and Confirm Password mismatch", false);
				isValidateSuccess = false;

			} else if (isCreateAcc
					&& !validateEmailAddress(email_value.getText().toString()
							.trim())) {
				displayAlert("Invalid Email Address", false);
				isValidateSuccess = false;

			} else {
				if ("".equals(phone_value.getText().toString())) {
					displayAlert("Enter Mandatory Fields", false);
					isValidateSuccess = false;

				} else if (phone_value.length() < 10) {
					displayAlert("Invalid Phone Number", false);
					isValidateSuccess = false;

				}
			}
		} else {
			if ("".equals(password) || "".equals(confirm_password)) {
				displayAlert("Enter Mandatory Fields", false);
				isValidateSuccess = false;

			} else if (!password.equals(confirm_password)) {
				displayAlert("Password and Confirm Password mismatch", false);
				isValidateSuccess = false;

			} else if (isCreateAcc
					&& !validateEmailAddress(email_value.getText().toString()
							.trim())) {
				displayAlert("Invalid Email Address", false);
				isValidateSuccess = false;

			} else {
				if ("".equals(phone_value.getText().toString())) {
					displayAlert("Enter Mandatory Fields", false);
					isValidateSuccess = false;

				} else if (phone_value.length() < 10) {
					displayAlert("Invalid Phone Number", false);
					isValidateSuccess = false;

				}
			}
		}

		return isValidateSuccess;

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit_btn:
			if (validateFields()) {
				new SubmitAsyncTask().execute();
			}

			break;

		case R.id.cancel_btn:
			finish();

			break;

		default:
			break;
		}
	}

	public class SubmitAsyncTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog mDialog;
		private JSONObject jsonResponse = null;
		String responseText = "";
		String responseCode = "";

		UrlRequestParams urlRequestParams = new UrlRequestParams();
		ServerConnections mServerConnections = new ServerConnections();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mDialog = ProgressDialog.show(GovQaCreateEditAccount.this, "",
					Constants.DIALOG_MESSAGE, true);
			mDialog.setCancelable(false);
		}

		@Override
		protected Void doInBackground(Void... params) {

			HashMap<String, String> field_values = getFieldValues();
			JSONObject urlParameters = new JSONObject();

			if (isCreateAcc) {
				String gov_qa_submit_create_acc = Properties.url_local_server
						+ Properties.hubciti_version + "govqa/createaccount";

				urlParameters = urlRequestParams
						.createGovQaCreateAccSubmitParam(field_values);

				jsonResponse = mServerConnections.getUrlJsonPostResponse(
						gov_qa_submit_create_acc, urlParameters);
			} else {
				String gov_qa_submit_edit_acc = Properties.url_local_server
						+ Properties.hubciti_version + "govqa/editaccountinfo";
				urlParameters = urlRequestParams
						.createGovQaEditAccSubmitParam(field_values);

				jsonResponse = mServerConnections.getUrlJsonPostResponse(
						gov_qa_submit_edit_acc, urlParameters);
			}

			if (jsonResponse != null) {
				try {
					JSONObject jsonValues = jsonResponse
							.getJSONObject("response");

					if (jsonValues.has("responseCode")) {
						responseCode = jsonValues.getString("responseCode");
					}

					if (jsonValues.has("responseText")) {
						responseText = jsonValues.getString("responseText");
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			try {
				mDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean isSuccess;

			isSuccess = "10000".equals(responseCode);

			displayAlert(responseText, isSuccess);
		}

		private HashMap<String, String> getFieldValues() {
			HashMap<String, String> fieldValues = new HashMap<>();

			if (isCreateAcc) {
				fieldValues.put("email_id", email_value.getText().toString());
				fieldValues.put("confirm_password", confirm_pwd_value.getText()
						.toString());
				fieldValues
						.put("home_add", home_add_value.getText().toString());
				fieldValues.put("apt_num", apt_num_value.getText().toString());
				fieldValues.put("city", city_value.getText().toString());
				fieldValues.put("state", stateSpinner.getSelectedItem()
						.toString());
				fieldValues.put("zipcode", zipcode_value.getText().toString());

			} else {
				fieldValues.put("email_id", email_edit_value.getText()
						.toString());

				// Rest of the fields
				fieldValues.put("groupAccessName", "");
				fieldValues.put("custCFName1", "");
				fieldValues.put("custCFValue1", "");
				fieldValues.put("custCFName2", "");
				fieldValues.put("custCFValue2", "");
				fieldValues.put("custCFName3", "");
				fieldValues.put("custCFValue3", "");
				fieldValues.put("custCFName4", "");
				fieldValues.put("custCFValue4", "");
				fieldValues.put("custCFName5", "");
				fieldValues.put("custCFValue5", "");
			}

			fieldValues.put("password", password_value.getText().toString());
			fieldValues.put("fname", fname_value.getText().toString());
			fieldValues.put("lname", lname_value.getText().toString());

			fieldValues.put("phone", phoneFormatToString());

			return fieldValues;
		}

		private String phoneFormatToString() {
			String phoneStr = "";
			Pattern pattern = Pattern.compile("\\d+");
			Matcher matcher = pattern.matcher(phone_value.getText().toString());

			while (matcher.find()) {
				phoneStr = phoneStr + matcher.group(0);
			}

			return phoneStr;

		}
	}

	private void displayAlert(String message, final boolean isFinish) {
		AlertDialog.Builder notificationAlert = new AlertDialog.Builder(
				GovQaCreateEditAccount.this);
		notificationAlert.setMessage(message).setNeutralButton(
				android.R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						if (isFinish) {

							if (isCreateAcc) {
								CommonConstants.clearLoginCredentials(mContext);
							}

							dialog.dismiss();
							finish();
						} else {
							dialog.cancel();
						}
					}
				});
		notificationAlert.create().show();
	}


	// Hides keyboard
	public void hideKeyboard() {
		GovQaCreateEditAccount.this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	// Validating email id
	private boolean validateEmailAddress(String emailAddress) {
		// String expression =
		// "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		String expression = "^[\\w\\-]([\\.\\w])+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = emailAddress;
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		return matcher.matches();
	}
}
