package com.hubcity.android.govqa;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.Properties;
import com.hubcity.android.commonUtil.ServerConnections;
import com.hubcity.android.commonUtil.UrlRequestParams;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

/**
 * @author supriya.m This page will list down all request for a particular User
 */
public class GovQaViewMyReqDetails extends CustomTitleBar {

    Activity activity;
    LinearLayout linearLayout = null;
    BottomButtons bb = null;
    JSONObject bbJsonObj;
    ListView listView;
    String email;
    private String mFilterName = "NONE";
    ArrayList<GovQaViewReqDetailsObj> arrGovQaViewReqDetailsObjs;
    private int mListPosition = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gov_qa_view_my_req_details);

        try {
            /******* Setting the screen title ********/
            title.setText("My Requests");

            /****** Initializing of Bottom buttons ******/
            bottomButton();

            /****** creating the view and listener ******/
            populateView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateView() {

        /******* Back action *******/
        backImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rightImage.setBackgroundResource(R.drawable.ic_action_filter_by_size);
        rightImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent filterScreen = new Intent(GovQaViewMyReqDetails.this,
                        GovQaViewMyReqDetailsFilter.class);
                filterScreen.putExtra(Constants.LIST_POSITION, mListPosition);
                filterScreen.putExtra(Constants.FILTER_NAME, mFilterName);
                filterScreen.putExtra(Constants.EMAIL, email);
                startActivityForResult(filterScreen, 2);

            }
        });
        listView = (ListView) findViewById(R.id.govqa_view_req_details_list);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(GovQaViewMyReqDetails.this,
                        GovQaViewMyRequestDetailsEdit.class);

                if (bbJsonObj != null && bbJsonObj.length() > 0) {
                    intent.putExtra("BottomBtnJsonObj", bbJsonObj.toString());
                }
                intent.putExtra("refNmbr",
                        arrGovQaViewReqDetailsObjs.get(position)
                                .getReferenceNumber());
                intent.putExtra("title",
                        arrGovQaViewReqDetailsObjs.get(position)
                                .getDescription());
                intent.putExtra("email", email);
                intent.putExtra("City", arrGovQaViewReqDetailsObjs
                        .get(position).getCity());
                intent.putExtra("State",
                        arrGovQaViewReqDetailsObjs.get(position).getState());
                intent.putExtra("Zip", arrGovQaViewReqDetailsObjs.get(position)
                        .getZip());
                intent.putExtra("Address1",
                        arrGovQaViewReqDetailsObjs.get(position).getAddress1());
                intent.putExtra("Address2",
                        arrGovQaViewReqDetailsObjs.get(position).getAddress2());

                startActivity(intent);

            }
        });
        email = getIntent().getExtras().getString("email");
        /****** calling webservice to fetch request details ******/
        new getRequestDetails()
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        divider.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 2) {
            email = data.getExtras().getString(Constants.EMAIL);
            mFilterName = data.getExtras().getString(Constants.FILTER_NAME);
            mListPosition = data.getExtras().getInt(Constants.LIST_POSITION);

            new getRequestDetails()
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }
    }

    private void bottomButton() {
        /****** Implementation of Bottom buttons ******/
        activity = GovQaViewMyReqDetails.this;
        linearLayout = (LinearLayout) findViewById(R.id.govqa_view_my_req_details_parent_layout);
        // linearLayout.setBackgroundColor(R.color.light_gray);

        // Initiating Bottom button class
        bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
        // Add screen name when needed
        bb.setActivityInfo(activity, "View My Request Details");

        if (getIntent().hasExtra("BottomBtnJsonObj")
                && getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

            try {
                bbJsonObj = new JSONObject(getIntent().getExtras().getString(
                        "BottomBtnJsonObj"));

                ArrayList<BottomButtonBO> bottomButtonList = bb
                        .parseForBottomButton(bbJsonObj);
                if (bottomButtonList != null) {
                    BottomButtonListSingleton.clearBottomButtonListSingleton();
                    BottomButtonListSingleton
                            .getListBottomButton(bottomButtonList);
                } else {
                    if (MenuAsyncTask.bottomButtonList != null) {
                        BottomButtonListSingleton
                                .clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(MenuAsyncTask.bottomButtonList);
                    }
                }

                bb.createbottomButtontTab(linearLayout, false);

                linearLayout.setVisibility(View.VISIBLE);

            } catch (Exception e) {
                e.printStackTrace();
                linearLayout.setVisibility(View.GONE);
            }

        } else {
            linearLayout.setVisibility(View.GONE);
        }

    }

    private class getRequestDetails extends AsyncTask<Void, Void, String> {
        private ProgressDialog mDialog;
        JSONObject jsonObjectGovQaViewMyRequest;
        ServerConnections mServerConnections = new ServerConnections();
        UrlRequestParams urlRequestParams = new UrlRequestParams();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = ProgressDialog.show(GovQaViewMyReqDetails.this, "",
                    Constants.DIALOG_MESSAGE, true);
            mDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "false";

            String url = Properties.url_local_server
                    + Properties.hubciti_version
                    + "govqa/getallrequestsbycustmr";

            arrGovQaViewReqDetailsObjs = new ArrayList<>();
            JSONObject urlParameters = urlRequestParams
                    .createGovQaViewMyRequestparam(email, mFilterName);
            jsonObjectGovQaViewMyRequest = mServerConnections
                    .getUrlJsonPostResponse(url, urlParameters);
            try {
                String responseText = jsonObjectGovQaViewMyRequest
                        .getString("responseText");


                if (responseText.equals("Success")) {
                    JSONArray jsonArray = jsonObjectGovQaViewMyRequest
                            .optJSONArray("Table");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        GovQaViewReqDetailsObj detailsObj = new GovQaViewReqDetailsObj();
                        detailsObj.setDescription(jsonArray.getJSONObject(i)
                                .getString("description"));
                        detailsObj.setReferenceNumber(jsonArray
                                .getJSONObject(i).getString("referNum"));
                        detailsObj.setStatus(jsonArray.getJSONObject(i)
                                .getString("status"));
                        if (jsonArray.getJSONObject(i).has("address1")) {
                            detailsObj.setAddress1(jsonArray.getJSONObject(i)
                                    .getString("address1"));
                        }
                        if (jsonArray.getJSONObject(i).has("address2")) {
                            detailsObj.setAddress2(jsonArray.getJSONObject(i)
                                    .getString("address2"));
                        }
                        if (jsonArray.getJSONObject(i).has("city")) {
                            detailsObj.setCity(jsonArray.getJSONObject(i)
                                    .getString("city"));
                        }
                        if (jsonArray.getJSONObject(i).has("state")) {
                            detailsObj.setState(jsonArray.getJSONObject(i)
                                    .getString("state"));
                        }
                        if (jsonArray.getJSONObject(i).has("zipCode")) {
                            detailsObj.setZip(jsonArray.getJSONObject(i)
                                    .getString("zipCode"));
                        }
                        arrGovQaViewReqDetailsObjs.add(detailsObj);
                    }
                    result = "true";
                } else {
                    result = responseText;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                mDialog.dismiss();

                if (!result.equals("true")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(
                            GovQaViewMyReqDetails.this).create();

                    // Setting Dialog Message
                    alertDialog.setMessage(result);


                    // Setting OK Button
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();

                }
                GovQaViewMyReqDetailsAdapter detailsAdapter = new GovQaViewMyReqDetailsAdapter(
                        GovQaViewMyReqDetails.this, arrGovQaViewReqDetailsObjs,
                        bbJsonObj, email);
                listView.setAdapter(detailsAdapter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Clearing Memory
        linearLayout = null;
        bb = null;
        bbJsonObj = null;
        email = null;
        arrGovQaViewReqDetailsObjs = null;

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Add screen name when needed
        if (null != bb) {
            bb.setActivityInfo(activity, "View My Request Details");
        }
    }

}
