package com.hubcity.android.govqa;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.Constants;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.commonUtil.SubMenuStack;
import com.hubcity.android.screens.BottomButtons;
import com.hubcity.android.screens.LoginScreenViewAsyncTask;
import com.hubcity.android.screens.SortDepttNTypeScreen;
import com.scansee.hubregion.R;

/**
 * Class to Display Report a Problem screen
 * 
 * @author rekha_p
 * 
 */
public class GovQaMainPage extends CustomTitleBar {

	// public HashMap<String, String> customer_info = new HashMap<String,
	// String>();

	String report_prob_arr[] = { "Make a Request", "Find Information",
			"View My Requests" };

	int report_prob_img_arr[] = { R.drawable.makerequest,
			R.drawable.findinformation, R.drawable.viewmyrequests };

	ListView listView;
	ListAdapter adapter;

	JSONObject bbJsonObj;
	BottomButtons bb = null;
	Activity activity;
	LinearLayout linearLayout = null;

	String email_id = "";
	String customer_id = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.govqa_menu_list);

		try {
			// Title of the screen
			title.setText("Report a Problem");

			if (getIntent().hasExtra("email_id")) {
                email_id = getIntent().getExtras().getString("email_id");
            }


			listView = (ListView) findViewById(R.id.menu_list);

			// List view item click listener
			listView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                        int position, long id) {

                    String selectedItem = view.getTag().toString();
                    Log.i("Selected Item", selectedItem);

                    if ("Make a Request".equalsIgnoreCase(selectedItem)) {
                        Intent intent = new Intent(GovQaMainPage.this,
                                GovQaMakeRequest.class);
                        intent.putExtra("email_id", getIntent().getExtras()
                                .getString("email_id"));

                        intent.putExtra("customer_id", customer_id);

                        if (getIntent().hasExtra("BottomBtnJsonObj")) {
                            intent.putExtra("BottomBtnJsonObj", getIntent()
                                    .getExtras().getString("BottomBtnJsonObj"));
                        }

                        intent.putExtra("ListHeader", selectedItem);

                        startActivity(intent);

                    } else if ("View My Requests".equalsIgnoreCase(selectedItem)) {

                        Intent intent = new Intent(GovQaMainPage.this,
                                GovQaViewMyRequest.class);

                        if (bbJsonObj != null && bbJsonObj.length() > 0) {
                            intent.putExtra("BottomBtnJsonObj",
                                    bbJsonObj.toString());
                        }
                        intent.putExtra("email", email_id);
                        startActivity(intent);

                    } else if ("Find Information".equalsIgnoreCase(selectedItem)) {
                        Intent intent = new Intent(GovQaMainPage.this,
                                GovQaFindInformation.class);

                        if (getIntent().hasExtra("BottomBtnJsonObj")) {
                            intent.putExtra("BottomBtnJsonObj", getIntent()
                                    .getExtras().getString("BottomBtnJsonObj"));
                        }

                        intent.putExtra("FindInfoHeader", selectedItem);

                        startActivity(intent);

                    }

                }
            });

			// Set adapter for List view
			adapter = new ListAdapter(this);
			listView.setAdapter(adapter);

			rightImage.setVisibility(View.GONE);

			// Back action
			backImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Constants.GOV_QA_DONT_ALLOW_LOGIN = false;
                    callMainMenu();
                }
            });

			/*** Implementation of Bottom buttons ***/
			activity = GovQaMainPage.this;
			linearLayout = (LinearLayout) findViewById(R.id.menu_list_parent);
			linearLayout.setBackgroundColor(getResources().getColor(R.color.light_gray));

			// Initiating Bottom button class
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;

			// Add screen name when needed
			bb.setActivityInfo(activity, "Report_problem");

			if (getIntent().hasExtra("BottomBtnJsonObj")
                    && getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

                try {
                    bbJsonObj = new JSONObject(getIntent().getExtras().getString(
                            "BottomBtnJsonObj"));

                    ArrayList<BottomButtonBO> bottomButtonList = bb
                            .parseForBottomButton(bbJsonObj);
                    if (bottomButtonList != null) {
                        BottomButtonListSingleton.clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                    } else {
                        if (MenuAsyncTask.bottomButtonList != null) {
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(MenuAsyncTask.bottomButtonList);
                        }
                    }

                    bb.createbottomButtontTab(linearLayout, false);

                    linearLayout.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                    linearLayout.setVisibility(View.GONE);
                }

            } else {
                linearLayout.setVisibility(View.GONE);
            }

			getCustomerId();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void getCustomerId() {
		try {
			GovQAUserDetailsAsyncTask userDetails = new GovQAUserDetailsAsyncTask(
					activity, email_id);
			HashMap<String, String> values = userDetails.execute().get();
			customer_id = values.get("id");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Add screen name when needed
		if (null != bb) {
			bb.setActivityInfo(activity, "Report_problem");
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	/**
	 * Adapter for the list
	 * 
	 * @author rekha_p
	 * 
	 */
	class ListAdapter extends BaseAdapter {

		Activity context;

		public ListAdapter(Activity _context) {
			context = _context;
		}

		class ViewHolder {
			ImageView icon;
			ImageView chevron;
			TextView text;

			LinearLayout menuListTemplateParent;
		}

		@Override
		public int getCount() {
			return report_prob_arr.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@SuppressLint("ViewHolder")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater lInflator = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = lInflator.inflate(R.layout.template_list_menu_row,
					parent, false);

			ViewHolder view = new ViewHolder();

			view.menuListTemplateParent = (LinearLayout) convertView
					.findViewById(R.id.menu_list_template_parent);

			view.icon = (ImageView) convertView
					.findViewById(R.id.imageView_row_icon);

			view.text = (TextView) convertView.findViewById(R.id.tv_row_header);

			view.chevron = (ImageView) convertView
					.findViewById(R.id.imageView_disclosure);

			view.menuListTemplateParent
					.setBackgroundResource(R.color.light_gray);

			view.text.setText(report_prob_arr[position]);
			view.text.setTextColor(Color.parseColor("#000000"));
			view.text.setTypeface(null, Typeface.BOLD);

			convertView.setTag(report_prob_arr[position]);

			view.icon.setBackgroundResource(report_prob_img_arr[position]);

			return convertView;
		}

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Constants.GOV_QA_DONT_ALLOW_LOGIN = false;
		callMainMenu();
	}

	public void callMainMenu() {

		if (SortDepttNTypeScreen.subMenuDetailsList != null) {
			SortDepttNTypeScreen.subMenuDetailsList.clear();
			SubMenuStack.clearSubMenuStack();
			SortDepttNTypeScreen.subMenuDetailsList = new ArrayList<>();
		}
		LoginScreenViewAsyncTask.bIsLoginFlag = true;
		callingMainMenu();

	}

}
