package com.hubcity.android.govqa;

import java.util.ArrayList;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scansee.hubregion.R;

/**
 * @author supriya.m
 *
 */
public class GovQaViewMyReqDetailsAdapter extends BaseAdapter {

	ArrayList<GovQaViewReqDetailsObj> arrDetailsObjs;
	Context mContext;
	JSONObject bbJsonObj;
	String email;

	public GovQaViewMyReqDetailsAdapter(Context context,
			ArrayList<GovQaViewReqDetailsObj> arrDetailsObjs,
			JSONObject bbJsonObj, String email) {
		this.mContext = context;
		this.arrDetailsObjs = arrDetailsObjs;
		this.bbJsonObj = bbJsonObj;
		this.email = email;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrDetailsObjs.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) this.mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(
				R.layout.gov_qa_view_my_req_detail_adapter, null);

	/*	SpannableString content = new SpannableString(arrDetailsObjs.get(
				position).getReferenceNumber());
		content.setSpan(new UnderlineSpan(), 0, arrDetailsObjs.get(position)
				.getReferenceNumber().length(), 0);*/
		((TextView) convertView
				.findViewById(R.id.govqa_req_details_reference_txt))
				.setText(arrDetailsObjs.get(position).getReferenceNumber());

		((TextView) convertView
				.findViewById(R.id.govqa_req_details_description_txt))
				.setText(arrDetailsObjs.get(position).getDescription());
		((TextView) convertView
				.findViewById(R.id.govqa_req_details_status_txt))
				.setText(arrDetailsObjs.get(position).getStatus());

/*		((TextView) convertView
				.findViewById(R.id.govqa_req_details_reference_txt))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(mContext,
								GovQaViewMyRequestDetailsEdit.class);

						if (bbJsonObj != null && bbJsonObj.length() > 0) {
							intent.putExtra("BottomBtnJsonObj",
									bbJsonObj.toString());
						}
						intent.putExtra("refNmbr", arrDetailsObjs.get(position)
								.getReferenceNumber());
						intent.putExtra("title", arrDetailsObjs.get(position)
								.getDescription());
						intent.putExtra("email", email);

						mContext.startActivity(intent);
					}
				});*/

		return convertView;
	}

}
