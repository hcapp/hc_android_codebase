package com.hubcity.android.govqa;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;

public class GovQaMakeReqAdapter extends BaseAdapter {
	Context mContext;
	ArrayList<GovQaMakeReqObj> arrSortAndFilter;

	public GovQaMakeReqAdapter(GovQaMakeRequest govQaMakeRequest,
			ArrayList<GovQaMakeReqObj> arrSortAndFilter) {
		this.mContext = govQaMakeRequest;
		this.arrSortAndFilter = arrSortAndFilter;

	}

	@Override
	public int getCount() {
		return arrSortAndFilter.size();
	}

	@Override
	public Object getItem(int position) {
		return arrSortAndFilter.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater infalInflater = (LayoutInflater) this.mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TextView filter_name;
		ImageView icon;

		if (arrSortAndFilter.get(position).isHeader()) {
			convertView = infalInflater.inflate(
					R.layout.gov_qa_makereq_header_layout, parent,false);
			filter_name = (TextView) convertView
					.findViewById(R.id.gov_qa_makereq_header_text);
		} else {
			convertView = infalInflater.inflate(R.layout.gov_qa_makereq, parent,false);
			filter_name = (TextView) convertView
					.findViewById(R.id.row_header_govqa_makeReq);
			icon = (ImageView) convertView
					.findViewById(R.id.imageView_row_icon_govqa__makeReq);
			icon.setBackgroundResource(R.drawable.grey_image);

		}

		filter_name.setText(arrSortAndFilter.get(position).getFilterName());

		return convertView;
	}

}
