package com.hubcity.android.govqa;

import java.util.LinkedList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scansee.hubregion.R;

public class GovQaFindInfoAdapter extends BaseAdapter {
	Context mContext;
	LinkedList<GovQaFindInfoObj> findInfoList;

	int isNext;

	public GovQaFindInfoAdapter(GovQaFindInformation govQaFindInformation,
			LinkedList<GovQaFindInfoObj> findInfoList, int isNext) {
		this.mContext = govQaFindInformation;
		this.findInfoList = findInfoList;
		this.isNext = isNext;

	}

	@Override
	public int getCount() {
		return findInfoList.size();
	}

	@Override
	public Object getItem(int position) {
		return findInfoList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" })
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (((GovQaFindInformation) mContext).isNext == 1) {
			if (position == ((GovQaFindInformation) mContext).findInfoList
					.size() - 1) {

				((GovQaFindInformation) mContext).callPagination(isNext,
						findInfoList);
			}
		}
		LayoutInflater infalInflater = (LayoutInflater) this.mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TextView filter_name;
		ImageView icon;
		convertView = infalInflater.inflate(R.layout.gov_qa_findinfo, null);
		filter_name = (TextView) convertView
				.findViewById(R.id.row_header_govqa_findinfo);
		icon = (ImageView) convertView
				.findViewById(R.id.imageView_row_icon_govqa__findinfo);
		icon.setBackgroundResource(R.drawable.grey_image);
		filter_name.setText(findInfoList.get(position).getSummary());
		return convertView;
	}

}
