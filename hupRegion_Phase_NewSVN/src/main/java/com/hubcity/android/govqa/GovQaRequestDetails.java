package com.hubcity.android.govqa;

import java.util.ArrayList;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

/**
 * @author Subramanya Padiyar V
 * 
 *         This page to display question and answer
 */
public class GovQaRequestDetails extends CustomTitleBar {
	Activity activity;
	LinearLayout linearLayout;
	JSONObject bbJsonObj;
	BottomButtons bb;
	String question;
	String solution;
	String screenTitle;
	TextView questionView, answerView, questionText, answerText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.govqa_find_request_details);

		rightImage.setVisibility(View.GONE);
		divider.setVisibility(View.GONE);
		if (getIntent().hasExtra("Question")) {
			question = getIntent().getExtras().getString("Question");
		}
		if (getIntent().hasExtra("Solution")) {
			solution = getIntent().getExtras().getString("Solution");
		}
		if (getIntent().hasExtra("Title")) {
			screenTitle = getIntent().getExtras().getString("Title");
		}
		questionText = (TextView) findViewById(R.id.question_text);
		answerText = (TextView) findViewById(R.id.answer_text);
		questionView = (TextView) findViewById(R.id.question);
		answerView = (TextView) findViewById(R.id.answer);

		questionView.setText(Html.fromHtml(question));
		answerView.setText(Html.fromHtml(solution));
		title.setText(Html.fromHtml(screenTitle));
		// onclick on backbutton
		backImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				finish();

			}
		});
		// calling Bottom button
		BottomButton();
	}

	@SuppressLint("ResourceAsColor")
	private void BottomButton() {
		// *** Implementation of Bottom buttons ***//
		activity = GovQaRequestDetails.this;
		linearLayout = (LinearLayout) findViewById(R.id.bottom_ll);
		linearLayout.setBackgroundColor(R.color.light_gray);

		// Initiating Bottom button class
		bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
		// Add screen name when needed
		bb.setActivityInfo(activity, "Request Details");

		if (getIntent().hasExtra("BottomBtnJsonObj")
				&& getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

			try {
				bbJsonObj = new JSONObject(getIntent().getExtras().getString(
						"BottomBtnJsonObj"));

				ArrayList<BottomButtonBO> bottomButtonList = bb
						.parseForBottomButton(bbJsonObj);
				if (bottomButtonList != null) {
					BottomButtonListSingleton.clearBottomButtonListSingleton();
					BottomButtonListSingleton
							.getListBottomButton(bottomButtonList);
				} else {
					if (MenuAsyncTask.bottomButtonList != null) {
						BottomButtonListSingleton
								.clearBottomButtonListSingleton();
						BottomButtonListSingleton
								.getListBottomButton(MenuAsyncTask.bottomButtonList);
					}
				}

				bb.createbottomButtontTab(linearLayout, false);

				linearLayout.setVisibility(View.VISIBLE);

			} catch (Exception e) {
				e.printStackTrace();
				linearLayout.setVisibility(View.GONE);
			}

		} else {
			linearLayout.setVisibility(View.GONE);
		}

	}

	@Override
	protected void onResume() {
		super.onResume();

		// Add screen name when needed
		if (null != bb) {
			bb.setActivityInfo(activity, "Request Details");
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
