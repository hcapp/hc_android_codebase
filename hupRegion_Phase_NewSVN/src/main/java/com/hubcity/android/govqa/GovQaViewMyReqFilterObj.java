package com.hubcity.android.govqa;

/**
 * Created by subramanya.v on 1/4/2016.
 */
public class GovQaViewMyReqFilterObj
{
	private String filterBy;
	private boolean isHeader;
	private boolean ischecked;

	public boolean isHeader()
	{
		return isHeader;
	}

	public void setIsHeader(boolean isHeader)
	{
		this.isHeader = isHeader;
	}


	public String getFilterBy()
	{
		return filterBy;
	}

	public void setFilterBy(String filterBy)
	{
		this.filterBy = filterBy;
	}


	public boolean ischecked()
	{
		return ischecked;
	}

	public void setIschecked(boolean ischecked)
	{
		this.ischecked = ischecked;
	}


}
