package com.hubcity.android.govqa;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.hubcity.android.businessObjects.BottomButtonBO;
import com.hubcity.android.businessObjects.BottomButtonInfoSingleton;
import com.hubcity.android.businessObjects.BottomButtonListSingleton;
import com.hubcity.android.commonUtil.CustomTitleBar;
import com.hubcity.android.commonUtil.MenuAsyncTask;
import com.hubcity.android.screens.BottomButtons;
import com.scansee.hubregion.R;

public class GovQaForgotPassword extends CustomTitleBar {

	JSONObject bbJsonObj;
	BottomButtons bb = null;
	Activity activity;
	LinearLayout linearLayout = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.govqa_forgot_pwd);

		try {
			// Title of the screen
			title.setText("Forgot Password");

			// Handling Title bar buttons
			rightImage.setVisibility(View.GONE);

			// Cancel Button action
			Button cancelBtn = (Button) findViewById(R.id.govqa_cancel_btn);
			cancelBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    finish();
                }
            });

			/*** Implementation of Bottom buttons ***/
			activity = GovQaForgotPassword.this;
			linearLayout = (LinearLayout) findViewById(R.id.main_layout);

			// Initiating Bottom button class
			bb = BottomButtonInfoSingleton.getBottomButtonsSingleton().bottomButtons;
			// Add screen name when needed
			bb.setActivityInfo(activity, "");

			if (getIntent().hasExtra("BottomBtnJsonObj")
                    && getIntent().getExtras().getString("BottomBtnJsonObj") != null) {

                try {
                    bbJsonObj = new JSONObject(getIntent().getExtras().getString(
                            "BottomBtnJsonObj"));

                    ArrayList<BottomButtonBO> bottomButtonList = bb
                            .parseForBottomButton(bbJsonObj);
                    if (bottomButtonList != null) {
                        BottomButtonListSingleton.clearBottomButtonListSingleton();
                        BottomButtonListSingleton
                                .getListBottomButton(bottomButtonList);
                    } else {
                        if (MenuAsyncTask.bottomButtonList != null) {
                            BottomButtonListSingleton
                                    .clearBottomButtonListSingleton();
                            BottomButtonListSingleton
                                    .getListBottomButton(MenuAsyncTask.bottomButtonList);
                        }
                    }

                    bb.createbottomButtontTab(linearLayout, false);

                    linearLayout.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    linearLayout.setVisibility(View.GONE);
                }

            } else {
                linearLayout.setVisibility(View.GONE);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
